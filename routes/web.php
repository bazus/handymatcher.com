<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('index');

Route::get('/directory', 'SiteController@directory')->name('directory');

Route::get('/pro', 'SiteController@pro')->name('pro');

Route::get('/partners', 'SiteController@partners')->name('partners');

Route::post('/sendPartners', 'LeadsController@create')->name('leads.create');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
