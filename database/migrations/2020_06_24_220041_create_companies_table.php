<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('companyname', 64);
            $table->string('firstname', 53);
            $table->string('lastname', 45);
            $table->string('state', 27);
            $table->string('description', 255);
            $table->string('phone', 21);
            $table->string('email', 41);
            $table->integer('usdot');
            $table->string('isDeleted', 18);
            $table->string('website', 49);
            $table->string('url', 255);
            $table->string('youtube', 202);
            $table->string('logo', 99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
