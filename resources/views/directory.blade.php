@extends('template.page')

@push('links')
    <link rel="stylesheet" href="https://handymatcher.com/css/directory.css">
@endpush

@push('content')

    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <div class="input-group">
                        <input type="text" id="search" placeholder="Search For..." class="form-control">
                        <div class="input-group-append">
                            <button onclick="search()" class="btn btn-white"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group" id="errorDiv">

                    </div>
                </div>
            </div>
            <div class="row" id="companies">
                <?php foreach($companies as $client){ /*if(!$client['url']){continue;}*/?>
                <div class="col-md-12">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3">
                                    <?php if(isset($client['logo']) && $client['logo']){ ?>
                                    <img style="max-height:74px;max-width: -webkit-fill-available;margin: 0 auto;display: block;" src="<?= $client['logo'] ?>" alt="<?= $client['companyname'] ?> Logo">
                                    <?php }else{ ?>
                                    <img style="max-height:74px;max-width: -webkit-fill-available;margin: 0 auto;display: block;" src="https://handymatcher.com/images/defaultLogo.jpg" alt="<?= $client['companyname'] ?> Logo">
                                    <?php } ?>
                                </div>
                                <div class="col-9">
                                    <h5 class="card-title"><?= $client['companyname'] ?></h5>
                                    <p class="card-text"><?php if ($client['phone']){echo "Phone: ".$client['phone'];} ?><?php if ($client['email']){echo " Email: ".$client['email'];} ?>
                                        <?php if(isset($client['url'])){?>
                                        <a href="https://handymatcher.com/company/<?= $client['url'] ?>" class="btn btn-default" style="background:goldenrod;color:black;float: right;width: fit-content;">More Info</a>
                                        <?php }else{?>
                                        <a href="https://handymatcher.com/moving/Company-<?= $client['id'] ?>" class="btn btn-default" style="background:goldenrod;color:black;float: right;width: fit-content;">Get an estimate</a>
                                        <?php } ?></p>
                                <!--                                    <p class="card-text"><?php if ($client['phone']){echo "Phone: ".$client['phone'];} ?><?php if ($client['email']){echo " Email: ".$client['email'];} ?><?php if(isset($client['id'])){?><a href="https://handymatcher.com/company/<?= $client['companyname'] ?>" class="btn btn-default" style="background:goldenrod;color:black;float: right;width: fit-content;">More Info</a><?php }?></p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="col-md-12" style="cursor: pointer;" id="showMore" onclick="showMore(this)">
                    <div class="card mb-3">
                        <div class="card-body text-center">
                            <nav style="width: fit-content;display: block;margin: 0 auto;">
                               {{ $companies->links()  }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endpush
