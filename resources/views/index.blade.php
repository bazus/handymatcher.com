@extends('template.page')

@push('links')
    <link rel="stylesheet" href="https://handymatcher.com/css/style.css">
@endpush

@push('content')
<main>
    <div class="container">
        <div class="row">
            <div class="col-12 heroContainer">
                <div class="hero">
                    <h2>Find trusted local pros for any home project.</h2>
                    <div class="custom-container">
                        <div class="row">
                            <div class="col-9 mobileBig">
                                <select class="form-control" id="service" @if(Agent::isMobile()) onchange="openService(this.value)" @endif>
                                    <option value="" selected="" disabled="">What service do you need?</option>
                                    <option value="1">Local Moving</option>
                                    <option value="2">Long Distance Moving</option>
                                    <option value="3">Office Moving</option>
                                    <option value="5">Auto Transport</option>
                                    <option value="6">Plumbing</option>
                                    <option value="7">Flooring</option>
                                    <option value="8">Roofing</option>
                                    <option value="9">Cleaning</option>
                                </select>
                            </div>
                            <div class="col-3 mobileHide">
                                <button class="btn btn-block btn-custom" onclick="openService($('#service').val())">Find Pros</button>
                            </div>
                        </div>
                        <div class="row mobileShow">
                            <div class="col-12" style="color:#fff;background: rgba(0,0,0,0.5);margin-top: 15px;">
                                <a style="color:#fff;" href="https://handymatcher.com/partners">Are you a quality pro? Join our network.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div id="arrow"></div>
<div id="servicesTop" class="container">
    <h5 style="padding-left:20px; padding-top:15px;">Popular projects</h5>
    <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
    >
        <div class="d-flex">
            <div class="col-2 service-icon">
                <img
                    srcset="https://handymatcher.com/images/icons/handyman-tools.svg"
                    class="service-icon"
                />
            </div>
            <div class="col-10 service-text">
                <h5>Handymen</h5>
            </div>
        </div>
    </div>
    <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
    >
        <div class="d-flex">
            <div class="col-2 service-icon">
                <img
                    srcset="https://handymatcher.com/images/icons/plug.svg"
                    class="service-icon"
                />
            </div>
            <div class="col-10 service-text">
                <h5>Electrical</h5>
            </div>
        </div>
    </div>
    <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
    >
        <div class="d-flex">
            <div class="col-2 service-icon">
                <img
                    srcset="https://handymatcher.com/images/icons/ac.svg"
                    class="service-icon"
                />
            </div>
            <div class="col-10 service-text">
                <h5>
                    Heating <br />
                    & Cooling
                </h5>
            </div>
        </div>
    </div>
    <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
    >
        <div class="d-flex">
            <div class="col-2 service-icon">
                <img
                    srcset="https://handymatcher.com/images/icons/pipeline.svg"
                    class="service-icon"
                />
            </div>
            <div class="col-10 service-text">
                <h5>Plumbing</h5>
            </div>
        </div>
    </div>
    <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
    >
        <div class="d-flex">
            <div class="col-2 service-icon">
                <img
                    srcset="https://handymatcher.com/images/icons/roof.svg"
                    class="service-icon"
                />
            </div>
            <div class="col-10 service-text">
                <h5>Roofing</h5>
            </div>
        </div>
    </div>
    <div
        class="col-lg-2 col-md-6 col-sm-6 col-xs-6 service-block"
        onclick="moveToForm()"
    >
        <div class="d-flex">
            <div class="col-2 service-icon">
                <img
                    srcset="https://handymatcher.com/images/icons/carpet.svg"
                    class="service-icon"
                />
            </div>
            <div class="col-10 service-text">
                <h5>Flooring</h5>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-12">
                <div class="service-bottom-buttom"><a href="https://handymatcher.com/directory.php">View All Categories</a></div>
            </div>
        </div>
    </div>
</div>
<br><br>
<br><br><br>
<!-- BOXES -->
<section id="home-icons">
    <div class="container-fluid myRows">
        <div class="grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mb-4 text-center" id="aka-1">
                        <br><br>
                        <h5 class="h-5">Tell us about your project</h5>
                        <p class="p-1">
                            Select a project category that best matches your home repair or improvement need
                            We'll ask you a few important questions to ensure we're able to match you to the right pro for your job
                            HandyMatcher has over 70k service professionals specializing in over 100 categories
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img class="center-image img-fluid" src="images/1.jpg">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img class="img-fluid center-image lazy" src="images/2.jpg">
                </div>
                <div class="col-md-6 mb-4  text-center" id="aka-1">
                    <br><br>
                    <h5 class="h-5">Get Matched by Our Team</h5>
                    <p class="p-1">
                        You'll receive information for up to four pre-screened, local home improvement pros
                        Your matched pros provide the specific service you need and are available right now

                    </p>
                </div>
            </div>
        </div>
        <br><br><br>
        <div class="grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mb-4 text-center" id="aka-1">
                        <br><br>
                        <h5 class="h-5">Get Connected to the Best Pros</h5>
                        <p class="p-1">
                            As soon as your request is processed, we send your information to your matched pros
                            Shortly after receiving your service request, they'll contact you to discuss your project
                            If you prefer, you can contact them at your convenience
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img class="img-fluid center-image lazy" src="images/3.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container tabletTop">
    <section>
        <div class="conatiner">
            <div class="snd-div">
                <h5>
                    Tell us what you need. Get prices on the spot. Book risk free. Save a lot.
                </h5>
            </div>
        </div>
        <br><br>
    </section>

    <br>
    <br>
    <section>
        <div class="container">
            <div class="row text-center">
                <div class="col-4" id="">
                    <img src="images/i1.png" alt="" class="img-2">
                    <br>
                    <h6>Real Prices. Instantly.</h6>
                    <p>Get custom quotes on the spot</p>
                </div>
                <div class="col-4">
                    <img src="images/i2.png" alt="" class="img-2">
                    <br>
                    <h6>Risk Free Booking</h6>
                    <p>Order your job with zero down</p>
                </div>
                <div class="col-4">
                    <img src="images/i3.png" alt="" class="img-2">
                    <br>
                    <h6>Big Savings</h6>
                    <p>Save up to 40% on any project</p>
                </div>
            </div>
        </div>
    </section>
</div>
<br><br><br><br><br>
@endpush
