<?php
if (isset($_GET['r'])){
    $ref = $_GET['r']." | Handy Matcher";
}else{
    $ref = "Organic"." | Handy Matcher";
}
?>
    <!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php
    $title = "Handy Matcher | Moving and Home Improvement";
    $description = "Handy Matcher | Moving and Home Improvement. It is the simplest way to find and book top-rated local home services. Connect with trusted home repair and improvement contractors.";

    ?>
    @include('template.apps.head')
    @stack('links')
</head>

<body>
@include('template.apps.header')

@stack('content')

@include('template.apps.footer')
<script src="https://use.fontawesome.com/7e5b532868.js"></script>
<script>
    function moveToForm(){
        window.location.href = "https://handymatcher.com/moving";
    }
    function openService(id){
        switch(id){
            case "":
                break;
            case '1':
                window.location.href = "https://handymatcher.com/localMoving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            case '2':
                window.location.href = "https://handymatcher.com/longDistanceMoving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            case '3':
                window.location.href = "https://handymatcher.com/moving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            case '4':
                window.location.href = "https://handymatcher.com/moving/<?= str_replace("|", "-", str_replace(" ", "-", $ref)) ?>";
                break;
            default:
                alert("in development check back soon!");
                break;
        }
    }
</script>

@stack('scripts')
</body>
</html>
