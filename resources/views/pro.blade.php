@extends('template.page')

@push('links')
    <link rel="stylesheet" href="https://handymatcher.com/css/pro.css">
@endpush

@push('content')
    <main>
        <div class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-12 heroContainer">
                        <div class="hero">
                            <h6>INTRODUCING HANDYMATCHER</h6>
                            <h1>
                                We help you run your <br />
                                business better
                            </h1>
                            <h5>
                                Handy Matcher is the all-in-one marketing and management
                                solution built <br />
                                exclusively for home remodeling and design professionals.
                            </h5>
                            <h3>
                                <div class="button-container">
                                    <div class="row">
                                        <div class="col-3">
                                            <a
                                                class="btn btn-primary"
                                                href="https://handymatcher.com/partners.php"
                                                role="button"
                                            >Create account</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- BOXES -->

    <section id="home-icons">
        <div class="container-fluid myRows">
            <div class="container section2">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h6>WHY HANDYMATCHER</h6>
                                <h4>Tools custom-built for your business</h4>
                                <h6>
                                    We know how you work. So we designed software and services
                                    to help industry professionals from <br />
                                    designers to contractors manage and grow their businesses
                                    with ease.
                                </h6>
                            </div>
                        </div>
                    </div>
                    <br /><br /><br />
                </section>
            </div>

            <div class="container section3">
                <div class="row">
                    <div class="col-md-6">
                        <img
                            class="img-fluid center-image lazy"
                            src="https://handymatcher.com/images/1o.jpg"
                        />
                    </div>
                    <div class="col-md-6 mb-4 text-center" id="aka-1">
                        <h6>TARGETED LOCAL MARKETING</h6>
                        <h4>
                            Let us introduce you to your<br />
                            next client
                        </h4>
                        <h6>
                            Reach homeowners in your desired markets <br />
                            with the help of our targeted advertising <br />
                            initiatives. Build name recognition, <br />
                            showcase your finished projects, and get <br />
                            matched with prospective clients based on <br />
                            budget, style, and location.
                        </h6>
                        <h6 class="start-now-button">Start Now</h6>
                    </div>
                </div>
            </div>

            <div class="container section4">
                <div class="row">
                    <div class="col-md-6">
                        <h6>ALL IN ONE ACCESS</h6>
                        <h4>
                            Everything your business <br />
                            needs in one central place
                        </h4>
                        <h6>
                            Our innovative easy-to-use tools live in just <br />
                            one platform, saving your time and <br />
                            streamlining your work. Make admin tasks <br />
                            more efficient, get paid faster and <br />
                            collaborate more seamlessly.
                        </h6>
                        <h6 class="start-now-button">Start Now</h6>
                    </div>
                    <div class="col-md-6 mb-4" id="aka-1">
                        <img
                            class="img-fluid center-image lazy"
                            src="https://handymatcher.com/images/1o.jpg"
                        />
                    </div>
                </div>
            </div>

            <div class="container section5">
                <div class="row">
                    <div class="col-md-6">
                        <img
                            class="img-fluid center-image lazy"
                            src="https://handymatcher.com/images/1o.jpg"
                        />
                    </div>
                    <div class="col-md-6 mb-4 text-center" id="aka-1">
                        <h6>LEAD MANAGEMENT THAT LANDS CLIENTS</h6>
                        <h4>
                            An easier way to turn more<br />
                            leads into clients
                        </h4>
                        <h6>
                            We understand that leads don't pay the bills<br />
                            - clients do. Our lead management, <br />
                            collaboration and tracking software helps<br />
                            you grow your client roster through easy-to-<br />
                            use tools your team can use.
                        </h6>
                        <h6 class="start-now-button">Start Now</h6>
                    </div>
                </div>
            </div>

            <div class="container section6">
                <div class="row">
                    <div class="col-md-6">
                        <h6>REAL, LIVE EXPERT SUPPORT</h6>
                        <h4>
                            We've got your back
                        </h4>
                        <h6>
                            Our friendly team of industry specialists will<br />
                            help you make the most of our business<br />
                            tools and marketing services. We're focused <br />
                            on your success!
                        </h6>
                        <h6 class="start-now-button">Start Now</h6>
                    </div>
                    <div class="col-md-6 mb-4" id="aka-1">
                        <img
                            class="img-fluid center-image lazy"
                            src="https://handymatcher.com/images/1o.jpg"
                        />
                    </div>
                </div>
            </div>

            <div class="grey section7">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 mb-4 text-center" id="aka-1">
                            <h6>WHY HANDYMATCHER?</h6>
                            <h3>
                                Learn why industry leaders <br />
                                choose HandyMatcher
                            </h3>
                            <p></p>
                            <div class="bottom-video-container">
                                <iframe
                                    class="bottom-video"
                                    src="https://www.youtube.com/embed/owsIP71dQb4"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen=""
                                ></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="darkBlue section8">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 mb-4 text-center" id="aka-1">
                            <h6>LET'S CHAT</h6>
                            <h3>
                                Ready to get started?
                            </h3>
                            <p></p>
                            <div class="bottom-demo-button-container">
                                <a
                                    href="https://handymatcher.com/pro.php#"
                                    class="bottom-demo-button"
                                    role="button"
                                    target="_blank"
                                    type="button"
                                >Get Free Demo</a
                                >
                            </div>
                            <h6>Have question? Give us a call!</h6>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img
                    class="img-fluid center-image lazy"
                    src="./Handy Matcher _ Moving and Home Improvement_files/2o.jpg"
                  />
                </div>
                <div class="col-md-6 mb-4 text-center" id="aka-1">
                  <h5 class="h-5">Go beyond one-size-fits-all-business software</h5>
                  <p class="p-1">
                    You know the difference between cookie-cutter and custom. Select
                    a project category that best matches your home repair or
                    improvement need We'll ask you a few important questions to
                    ensure we're able to match you to the right pro for your job
                    HandyMatcher has over 70k service professionals specializing in
                    over 100 categories
                  </p>
                </div>
              </div>
            </div>
            <div class="grey">
              <div class="container">
                <div class="row">
                  <div class="col-md-6 mb-4 text-center" id="aka-1">
                    <h5 class="h-5">
                      Everything your business needs in one central place
                    </h5>
                    <p class="p-1">
                      Our innovative, easy-to-use tools live in just one platform.
                      Select a project category that best matches your home repair
                      or improvement need We'll ask you a few important questions to
                      ensure we're able to match you to the right pro for your job
                      HandyMatcher has over 70k service professionals specializing
                      in over 100 categories
                    </p>
                  </div>
                  <div class="col-md-6">
                    <img
                      class="center-image img-fluid"
                      src="./Handy Matcher _ Moving and Home Improvement_files/1o.jpg"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img
                    class="img-fluid center-image lazy"
                    src="./Handy Matcher _ Moving and Home Improvement_files/2o.jpg"
                  />
                </div>
                <div class="col-md-6 mb-4 text-center" id="aka-1">
                  <h5 class="h-5">Go beyond one-size-fits-all-business software</h5>
                  <p class="p-1">
                    You know the difference between cookie-cutter and custom. Select
                    a project category that best matches your home repair or
                    improvement need We'll ask you a few important questions to
                    ensure we're able to match you to the right pro for your job
                    HandyMatcher has over 70k service professionals specializing in
                    over 100 categories
                  </p>
                </div>
              </div>
            </div>
            <div class="grey">
              <div class="container">
                <div class="row">
                  <div class="col-md-6 mb-4 text-center" id="aka-1">
                    <h5 class="h-5">Let us introduce you to your next client</h5>
                    <p class="p-1">
                      Reach homeowners in your desired markets with the help of our
                      targeted advertising. As soon as your request is processed, we
                      send your information to your matched pros Shortly after
                      receiving your service request, they'll contact you to discuss
                      your project If you prefer, you can contact them at your
                      convenience
                    </p>
                  </div>
                  <div class="col-md-6">
                    <img
                      class="img-fluid center-image lazy"
                      src="./Handy Matcher _ Moving and Home Improvement_files/3o.jpg"
                    />
                  </div>
                </div>
              </div>
            </div> -->
        </div>
    </section>

@endpush
