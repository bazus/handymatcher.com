@extends('template.page')

@push('links')
    <link rel="stylesheet" href="{{ asset('css/site.css') }}">
@endpush

@push('content')
    <section id="progressBarContainer">
        <div class="row">
            <div class="col-11">
                <div class="progress">
                    <div id="progressBar" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div class="col-1">
                <div id="percentage">0%</div>
            </div>
        </div>
    </section>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="myBox">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h2>Become a Partner</h2>
                                <br><br><br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-center">
                                <h5>Please fill the info below so we can get you more clients in your area!</h5>
                            </div>
                        </div>
                        <div class="stepContainer">
                            <div class="row customBox" id="step1">
                                <div class="col-12 text-center">
                                    <h2>What kind of Service do you supply?</h2>
                                    <hr>
                                    <h5>Service</h5>
                                    <select class="my-custom-control" id="serviceType" onchange="selectServiceType(this.value)">
                                        <option selected="" disabled="" value="">Select Service</option>
                                        <option value="Local Moving">Local Moving</option>
                                        <option value="Long Distance Moving">Long Distance Moving</option>
                                        <option value="Local & Long Distance Moving">Local & Long Distance Moving</option>
                                        <option value="Auto transport">Auto transport</option>
                                        <option value="Cleaning">Cleaning</option>
                                        <option value="Plumbing">Plumbing</option>
                                        <option value="Flooring">Flooring</option>
                                        <option value="Roofing">Roofing</option>
                                        <option value="" disabled="">===========</option>
                                        <option value="0">Other</option>
                                    </select>
                                    <hr>
                                    <input type="text" placeholder="Enter Reason" class="my-custom-control" style="display:none;" id="serviceTypeOther">
                                    <button disabled id="firstNext" class="btn btn-custom btn-lg" onclick="moveToStep2()">Next</button>
                                </div>
                            </div>
                            <div class="row customBox" id="step2" style="display:none;">
                                <div class="col-12 text-center">
                                    <h2>In what states your business operate?</h2>
                                    <hr>
                                    <h5>States</h5>
                                    <input id="states" onkeyup="checkStep2(this.value)" type="text" class="my-custom-control" placeholder="Enter States e.g. NY,CA,OH,...">
                                    <hr>
                                    <button disabled id="secondNext" class="btn btn-custom btn-lg" onclick="moveToStep3()">Next</button>
                                    <button class="btn btn-default btn-lg" onclick="moveToStep1()">Previous</button>
                                </div>
                            </div>
                            <div class="row customBox" id="step3" style="display:none;">
                                <div class="col-12 text-center">
                                    <h2>How can we reach you?</h2>
                                    <hr>
                                    <h5>Personal Name</h5>
                                    <input type="text" id="fullName" class="my-custom-control" placeholder="Enter Full Name">
                                    <hr>
                                    <h5>Company Name</h5>
                                    <input type="text" id="companyName" class="my-custom-control" placeholder="Enter Company Name">
                                    <hr>
                                    <h5>Phone Number</h5>
                                    <input type="text" id="phone" class="my-custom-control" placeholder="Enter Phone Number">
                                    <hr>
                                    <h5>Email Address</h5>
                                    <input type="text" id="email" class="my-custom-control" placeholder="Enter Email Address">
                                    <hr>
                                    <button class="btn btn-custom btn-lg" id="sendBtn" onclick="sendClientRequest()">Send</button>
                                    <button class="btn btn-default btn-lg" onclick="moveToStep3()">Previous</button>
                                </div>
                            </div>
                        </div>

                        <div class="section1">
                            <div class="container">
                                <div class="head text-center">
                                    <h3>Attract new customers every day with contractors leads</h3>
                                    <p>We're generating leads specifically for your needs, all our leads must prove that they are genuine and authentic in order to be sent.</p>
                                </div>
                                <br><br><br>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 box">
                                        <div class="box-inner">
                                            <img src="images/3.png" class="img-fluid d-block mx-auto">
                                            <h4>100% “live” customers</h4>
                                            <p>You will get the contact info of a people who are looking for your service.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 box">
                                        <div class="box-inner">
                                            <img src="images/2.png" class="img-fluid d-block mx-auto">
                                            <h4>In less than a moment</h4>
                                            <p>Average time from the moment of request to your account.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 box">
                                        <div class="box-inner">
                                            <img src="images/1.png" class="img-fluid d-block mx-auto">
                                            <h4>Flexible Filtering</h4>
                                            <p>Receive only leads you are looking for. Our filters are fully customizable.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br><br><br>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endpush


@push('scripts')
    <script>
        var clientData = [];
        clientData['service'] = "";
        clientData['states'] = "";

        function sendClientRequest(){
            $("sendBtn").attr("disabled",true);
            $("sendBtn").text("Sending");

            var sentData = [];
            sentData['name'] = document.getElementById('fullName').value;
            sentData['companyName'] = document.getElementById('companyName').value;
            sentData['phone'] = document.getElementById('phone').value;
            sentData['email'] = document.getElementById('email').value;
            sentData['stats'] = clientData['status'];
            sentData['service'] = clientData['service'];

            if(sentData){
                $.ajax({
                    url: "sendPartners",
                    data:{
                        name: document.getElementById("fullName").value,
                        companyName: document.getElementById('companyName').value,
                        phone: document.getElementById('phone').value,
                        email: document.getElementById('email').value,
                        states: document.getElementById('states').value,
                        service: document.getElementById('serviceType').value
                    },
                    dataType:"JSON",
                    method:"POST",
                    async: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data) {
                            window.location.href("https://handymathcer.com/thankYouPartners/"+document.getElementById('fullName').value);
                        }else{
                            alert("oops we got into a problem, please try again later");
//                          window.location.reload();
                        }
                    }catch (e) {
                        alert(data);
                        console.log(data);
                        console.log(e);
                    }
                });
            }
        }

        function selectServiceType(val){
            if(val != 0 && val != "0"){
                $("#serviceTypeOther").hide();
                clientData['service'] = val;
                $("#firstNext").removeAttr("disabled");
                $("#progressBar").width('50%');
                $("#percentage").text("50%");
            }else{
                $("#progressBar").width('0%');
                $("#percentage").text("0%");
                $("#firstNext").attr("disabled",true);
                $("#serviceTypeOther").show();
            }
        }
        window.onscroll = function() {checkForSticky()};
        var progressBarContainer = document.getElementById("progressBarContainer");
        var sticky = progressBarContainer.offsetTop;
        function checkForSticky() {
            if (window.pageYOffset > sticky) {
                progressBarContainer.classList.add("sticky");
            } else {
                progressBarContainer.classList.remove("sticky");
            }
        }
        function moveToStep1(){
            $('html, body').animate({scrollTop:$('#step1').position().top}, 'slow');
        }
        function moveToStep2(){
            $("#step2").show();
            $('html, body').animate({scrollTop:$('#step2').position().top}, 'slow');
        }
        function checkStep2(val){
            if(val){
                clientData['states'] = val;
                $("#secondNext").removeAttr("disabled");
                $("#progressBar").width('90%');
                $("#percentage").text("90%");
            }else{
                $("#secondNext").attr("disabled",true);
                $("#progressBar").width('50%');
                $("#percentage").text("50%");
                $("#step3").hide();
            }
        }
        function moveToStep3(){
            $("#step3").show();
            $('html, body').animate({scrollTop:$('#step3').position().top}, 'slow');
        }

    </script>
@endpush
