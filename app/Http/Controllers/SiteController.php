<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function directory(){
        $companies = Company::paginate(15);

        return view('directory')->with([
            'companies' => $companies
        ]);
    }

    public function pro()
    {
        return view('pro');
    }

    public function partners(){
        return view('partners');
    }
}
