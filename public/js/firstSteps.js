window.onscroll = function() {checkForSticky()};
var progressBarContainer = document.getElementById("progressBarContainer");
var sticky = progressBarContainer.offsetTop;
function checkForSticky() {
  if (window.pageYOffset > sticky) {
    progressBarContainer.classList.add("sticky");
  } else {
    progressBarContainer.classList.remove("sticky");
  }
}
function moveToStep1(){
    $('html, body').animate({scrollTop:$('#step1').position().top}, 'slow');
}
function getDetailsFromZip(zip,type){
    $.ajax({
        url: "https://handymatcher.com/actions/getDetailsFromZip.php",
        method: "POST",
        data: {
            zip:zip
        },
        async: true
    }).done(function (data) {
        try{
            data = JSON.parse(data);
            if(data.CityMixedCase && data.State){
                if(type == 1){
                    var container = document.getElementById("zipInfo");
                }else if(type == 2){
                    var container = document.getElementById("zipInfo2");
                }
                container.innerHTML = "";

                var hr = document.createElement("hr");
                container.appendChild(hr);

                var string = data.CityMixedCase+", "+data.State+" "+zip;
                var p = document.createElement("p");
                p.innerText = string;
                container.appendChild(p);

                if(type == 1){
                    $("#firstNext").removeAttr("disabled");
                    $("#progressBarContainer").show();
                    $("#theProgressBar").show();
                    $("#progressBar").width('12.5%');
                    $("#percentage").text("13%");
                }else if(type == 2){
                    $("#secondNext").removeAttr("disabled");
                    $("#progressBar").width('25%');
                    $("#percentage").text("25%");
                }
            }else{
                if(type == 1){
                    var container = document.getElementById("zipInfo");
                }else if(type == 2){
                    var container = document.getElementById("zipInfo2");
                }
                container.innerHTML = "Not Found";
            }
            
        }catch(e){
            
        }
    });
}
function checkStep1(val){
    if(val.length == 5){
        getDetailsFromZip(val,1);
    }else{
        $("#firstNext").attr("disabled",true);
        $("#progressBar").width('0%');
        $("#percentage").text("0%");
        $("#step2").hide();
    }
}
function moveToStep2(){
    $("#step2").show();
    $('html, body').animate({scrollTop:$('#step2').position().top}, 'slow');
}
function checkStep2(val){
    if(val.length == 5){
        getDetailsFromZip(val,2);
    }else{
        $("#secondNext").attr("disabled",true);
        $("#progressBar").width('12.5%');
        $("#percentage").text("13%");
        $("#step3").hide();
    }
}
function moveToStep3(){
    $("#step3").show();
    $('html, body').animate({scrollTop:$('#step3').position().top}, 'slow');
}
$(document).ready(function(){var $input = $("#movingDate").pickadate({format:'mm/dd/yyyy'});})

$("#movingDate").on('change',function(){
    if($(this).val()){
        $("#progressBar").width('37.5%');
        $("#percentage").text("38%");
        $("#thirdNext").removeAttr("disabled");
    }else{
        $("#progressBar").width('25%');
        $("#percentage").text("25%");
        $("#step4").hide();
        $("#thirdNext").attr("disabled",true);
    }
});
function moveToStep4(){
    $("#step4").show();
    $('html, body').animate({scrollTop:$('#step4').position().top}, 'slow');
}
function checkStep4(val){
    if(val){
        $("#fourthNext").removeAttr("disabled");
        $("#progressBar").width('50%');
        $("#percentage").text("50%");
    }else{
        $("#fourthNext").attr("disabled",true);
        $("#progressBar").width('37.5%');
        $("#percentage").text("38%");
        $("#step5").hide();
    }
}
function moveToStep5(){
    $("#progressBar").width('67.5%');
    $("#percentage").text("68%");
    $("#step5").show();
    $('html, body').animate({scrollTop:$('#step5').position().top}, 'slow');
//    asyncLoading();
}
function checkStep5(val){
    if(val){
        $("#fithNext").removeAttr("disabled");
        $("#progressBar").width('75%');
        $("#percentage").text("75%");
    }else{
        $("#fifthNext").attr("disabled",true);
        $("#progressBar").width('67.5%');
        $("#percentage").text("68%");
        $("#step6").hide();
    }
}
function moveToStep6(){
    $("#progressBar").width('87.5%');
    $("#percentage").text("88%");
    $("#step6").show();
    $('html, body').animate({scrollTop:$('#step6').position().top}, 'slow');
//    asyncLoading();
}
async function asyncLoading() {
    let promise = new Promise((res, rej) => {
        setTimeout(() => res(), 7000)
    });

    // wait until the promise returns us a value
    let result = await promise; 
    runStep6();
}
function runStep7(){
    $("#formFromZip").val($("#fromZip").val());
    $("#formToZip").val($("#toZip").val());
    $("#formRooms").val($("#movingSize").val());
    $("#formMoveDate").val($("#movingDate").val());
    $("#formTypeOfMove").val($("#typeOfMove").val());
    $("#progressBar").width('95%');
    $("#percentage").text("95%");
    $("#step7").show();
    $('html, body').animate({scrollTop:$('#step7').position().top}, 'slow');
}
function checkForm(){
        if(document.getElementById("info").checked === true){
            if($("#phone").val()){
                if($("#email").val()){
                    $("#estimate").removeAttr("disabled");
                }
            }
        }else{
            $("#estimate").attr("disabled",true);
        }
    }
    function runStep2(){
        $("#firstStep").hide();
        $("#lastStep").show();
        $("#firstStepBtn").hide();
        $("#lastStepBtn").show();
    }
    function runStep1(){
        $("#firstStep").show();
        $("#lastStep").hide();
        $("#firstStepBtn").show();
        $("#lastStepBtn").hide();
    }