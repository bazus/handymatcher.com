<?php
if (!isset($loadFooterHelp)){$loadFooterHelp = true;}
?>

<?php if($bouncer["userSettingsData"]["helpMode"] == "1" && (!isset($isWelcomeMode) || $isWelcomeMode == false) && !$GLOBALS['mobileDetect']["isMobile"]){ ?>

    <?php // Usersnap (no longer using it)
          /*
    <script src="//api.usersnap.com/load/b9713c4b-36e1-42f9-8313-60a7b2b6bace.js" async></script>
          */
          ?>
<?php } ?>
<style>
    .need-help-popover{
        padding: 0;
    }
    .need-help-popover .popover-title{
        white-space: nowrap;
        background-color: #1ab394;
        border-color: #1ab394;
        color: #FFFFFF;
    }
    .need-help-popover .popover-content{
        padding: 0;
    }
    .need-help-popover .helpItem{
        padding: 7px 13px;
    }
    .need-help-popover .helpItem:hover{
        background-color: #f3f3f3;
        color:#676a6c;
        cursor: pointer;
    }
    .need-help-popover .helpItem a{
        color: #333;
    }
    .callUsSwal .form-control{
        margin-bottom: 15px;
    }
    .callUsSwal .btn-default:hover{
        background: white;
    }
</style>
<div class="footer">
    <div class="pull-right" style="display: none;">
        <?php echo $bouncer["userData"]["fullName"]; ?> - <strong><?php echo $bouncer["userData"]["organizationName"]; ?></strong>
    </div>
    <div>
        <strong>Copyright</strong> Network Leads © <?php echo date("Y",strtotime("now")); ?>
    </div>
</div>

<style>
    .btn-default:hover{
        background:#F8F8F8 !important;
    }
    .btn-warning:hover{
        background:#ECA455 !important;
    }

    #needHelp{
        position: absolute;
        bottom: 46px;
        right: 15px;
        z-index: 99999;
    }
    .contactBtn:hover {
        background: #ed5565 !important;
        border-color: #ed5565 !important;
    }

    @media only screen and (max-width: 480px) {
        .footer{
            margin-top: 10px;
        }
    }
    @media only screen and (max-width: 780px) {
        #needHelp{
            position: fixed !important;
            width: 100% !important;
            bottom: 0px !important;
            right: 0 !important;
        }
        #needhelpbtn{
            border-radius: 0 !important;
            width: 100% !important;
            padding: 9px !important;
        }
        .need-help-popover{
            max-width: 100%;
            width: -webkit-fill-available;
            left: 0px !important;
            margin-left: 10px;
            margin-right: 10px;
            position: fixed; !important;
        }
    }
</style>

<?php
$showNeedHelpByUrl = false;

$url = explode("console/",$_SERVER["REQUEST_URI"]);

$eraselastpage = explode("?",$url[count($url)-1]);
$url[count($url)-1] = $eraselastpage[0];

if(isset($url[1])) {
    $URI = explode("/", $url[1]);

    if ($URI[0] == "index.php" || $URI[0] == "") {
        $showNeedHelpByUrl = true;
    }
}

if($loadFooterHelp == true && $bouncer["userSettingsData"]["helpMode"] == "1" && $showNeedHelpByUrl == true){ ?>
    <div id="needHelp">
        <button id="needhelpbtn" type="button" class="btn btn-primary btn-rounded need-help-popverover" data-container="body" data-toggle="popover" data-placement="top">
            Need Help?
        </button>
    </div>
<?php } ?>
<script>
    $(function () {

        <?php if(isset($_GET["needhelp"])){?>
                $( "#needhelpbtn" ).click();
        <?php } ?>

        function getPopoverCustomTemplate(className) {
            return '<div class="popover ' + className + '" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>';
        }

        var contactOptions = document.createElement("div");

        var chat = document.createElement("div");
        chat.setAttribute("id","triggerIntercom");
        chat.classList.add("helpItem");
        
        var chatIcon = document.createElement("i");
        chatIcon.classList.add("fa");
        chatIcon.classList.add("fa-commenting");

        var ChatButton = document.createElement("a");
        ChatButton.innerHTML = " Chat with us";
        chat.appendChild(chatIcon);
        chat.appendChild(ChatButton);

        var call = document.createElement("div");
        call.setAttribute("onclick","needHelpController.talkToUs('','',0)");
        call.classList.add("helpItem");

        var callIcon = document.createElement("i")
        callIcon.classList.add("fa");
        callIcon.classList.add("fa-phone");

        var callButton = document.createElement("a");
        callButton.innerHTML = " Have us call you";
        call.appendChild(callIcon);
        call.appendChild(callButton);

        var email = document.createElement("div");
        email.setAttribute("onclick","needHelpController.sendEmail()");
        email.classList.add("helpItem");

        var emailIcon = document.createElement("i")
        emailIcon.classList.add("fa");
        emailIcon.classList.add("fa-envelope");

        var emailButton = document.createElement("a");
        emailButton.innerHTML = " Send us an email";
        email.appendChild(emailIcon);
        email.appendChild(emailButton);

        var helpCenter = document.createElement("div");
        helpCenter.setAttribute("onclick","needHelpController.openHelpCenter()");
        helpCenter.classList.add("helpItem");

        var helpCenterIcon = document.createElement("i")
        helpCenterIcon.classList.add("fa");
        helpCenterIcon.classList.add("fa-question-circle");

        var helpCenterButton = document.createElement("a");
        helpCenterButton.innerHTML = " Help center";
        helpCenter.appendChild(helpCenterIcon);
        helpCenter.appendChild(helpCenterButton);

        contactOptions.appendChild(chat);
        contactOptions.appendChild(call);
        contactOptions.appendChild(email);
        contactOptions.appendChild(helpCenter);

        $('.need-help-popverover').popover({
            html: true,
            title: "HELP & FEEDBACK",
            content: contactOptions,
            template: getPopoverCustomTemplate('need-help-popover')
        })

        $(document).on('click', function(e) {
            $('.need-help-popverover').each(function() {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide').data('bs.popover').inState.click = false // fix for BS 3.3.6
                }

            });
        });
    })

    var l;

    var needHelpController = {

        sendEmail: function() {
            $(document).ready(function () {
                l = $('#buttonSend').ladda();
            });
            var e = document.createElement("div");
            e.classList.add("text-left");
            var t = document.createElement("div"),
                n = document.createElement("label"),
                a = document.createTextNode("Full Name");
            n.appendChild(a), t.appendChild(n), (i = document.createElement("input")).type = "text", i.value = "<?= $bouncer['userData']['fullName'] ?>", i.id = "fullname", i.classList.add("swal-content__input"), i.setAttribute("placeholder", "Type In Your Name"), i.setAttribute("required", "required"), t.appendChild(i), e.appendChild(t);
            var i, d = document.createElement("div");
            d.style.marginTop = "13px", n = document.createElement("label"), a = document.createTextNode("Email"), n.appendChild(a), d.appendChild(n), (i = document.createElement("input")).type = "text", i.value = "<?= $bouncer['userData']['email'] ?>", i.id = "email", i.classList.add("swal-content__input"), i.setAttribute("placeholder", "Enter Your Email Address"), i.setAttribute("required", "required"), d.appendChild(i), e.appendChild(d);
            var s = document.createElement("div");
            s.style.marginTop = "13px", n = document.createElement("label"), a = document.createTextNode("Message"), n.appendChild(a), s.appendChild(n), (m = document.createElement("textarea")).classList.add("swal-content__input"), m.style.resize = "vertical", m.style.height = "200px", m.id = "content", m.setAttribute("placeholder", "Enter your message.."), m.setAttribute("required", "required"), s.appendChild(m), e.appendChild(s);
            var o = document.createElement("br"),
                r = document.createElement("br"),
                c = document.createElement("small"),
                u = document.createTextNode("* Here you can send us an Email if you need any kind of Assistant or Information");
            c.style.fontSize = "60%", c.appendChild(u), c.appendChild(o), e.appendChild(c);
            c = document.createElement("div");
            c.appendChild(m), c.appendChild(r), e.appendChild(c);
            var y = document.createElement("div");
            y.style.width = "100%", y.style.marginBottom = "5%", y.style.marginTop = "2%";
            var v = document.createElement("small");
            v.id = "errorSmall", v.classList.add("pull-left"), v.style.fontSize = "60%", v.style.color = "red", y.appendChild(v);
            var f = document.createElement("button");
            f.classList.add("btn"), f.classList.add("btn-default"), f.classList.add("pull-right"), f.setAttribute("onclick", "swal.close();"), f.style.marginBottom = "5%", f.style.marginTop = "2%";
            var g = document.createTextNode("Cancel");
            f.appendChild(g);
            var E = document.createElement("button");
            E.setAttribute("onclick", "needHelpController.acceptedEmail();"), E.setAttribute("data-style", "zoom-out"), E.style.marginLeft = "3px", E.style.marginRight = "3px", E.classList.add("btn"), E.classList.add("btn-success"), E.classList.add("pull-right"), E.id = "buttonSend", E.style.marginBottom = "5%", E.style.marginTop = "2%", g = document.createTextNode("Send"), E.appendChild(g), y.appendChild(E), y.appendChild(f), e.appendChild(y), swal({
                title: !1,
                content: e,
                buttons: !1,
                icon: !1
            });
        },
        acceptedEmail: function() {
            l = $('#buttonSend').ladda();
            l.ladda('start');
            var e = $("#fullname").val(),
                t = $("#email").val(),
                n = $("#content").val();
            "" == e.trim() || "" == t.trim() || "" == n.trim() ? (l.ladda("stop"), $("#errorSmall").text("* Please Fill All Fields")) : (needHelpController.sendEmailToSystem(e, t, n))
        },
        sendEmailToSystem: function(e, t, n) {

            var a = BASE_URL + "/actions/mail/sendEmailToSupport.php";
            return jQuery.ajax({
                url: a,
                method: "post",
                data: {
                    name: e,
                    email: t,
                    content: n
                },
                success: function (e) {
                },
                async: 1
            }).done(function (e) {
                l.ladda("stop");
                try {
                    if (e == "true") {
                        swal("Email Sent", "Thank you for contacting us. One of our representatives will be in contact with you ‌shortly. If you ever have any questions that require immediate assistance, please call us at (866) 277-2073.", "success");
                    } else {
                        toastr.error("Email not sent, please try again later", "Failed");
                    }
                } catch (e) {
                    toastr.error("Email not sent, please try again later", "Failed");
                }
            }), !1
        },
        talkToUs: function(phoneNumber, reason, error) {
            var content = document.createElement("div");

            var input = document.createElement("input");
            input.setAttribute("type", "tel");
            input.value = phoneNumber;
            input.id = "phoneNumber";
            input.placeholder = "Contact Phone Number";
            input.classList.add("form-control");
            if (error == 1) {
                input.style.border = "1px solid red";
            }

            content.appendChild(input);

            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.value = reason;
            input.id = "reason";
            input.placeholder = "Call Reason";
            input.classList.add("form-control");
            if (error == 2) {
                input.style.border = "1px solid red";
            }

            content.appendChild(input);

            var buttonsContainer = document.createElement("div");
            buttonsContainer.classList.add("swal-footer");
            buttonsContainer.style.padding = "0";

            var button = document.createElement("button");
            button.setAttribute("onclick", "swal.close()");
            button.style.marginRight = "5px";
            button.classList.add("btn");
            button.classList.add("btn-default");

            var buttonText = document.createTextNode("Cancel");

            button.appendChild(buttonText);
            buttonsContainer.appendChild(button);

            var button = document.createElement("button");
            button.setAttribute("onclick", "needHelpController.prepareTicket()");
            button.classList.add("btn");
            button.classList.add("laddaButton");
            button.classList.add("btn-success");

            var buttonText = document.createTextNode("Send");

            button.appendChild(buttonText);
            buttonsContainer.appendChild(button);

            content.appendChild(buttonsContainer);

            swal({
                title: "Have us call you",
                content: content,
                buttons: false,
                className: "callUsSwal"
            });
        },
        prepareTicket: function() {
            var l = $(".laddaButton").ladda();
            l.ladda("start");

            var phoneNumber = document.getElementById("phoneNumber").value;
            var reason = document.getElementById("reason").value;
            if (phoneNumber.replace(/ /g, "") != "" && reason.replace(/ /g, "") != "") {
                needHelpController.sendCallTicket(phoneNumber, reason);
            } else {
                if (phoneNumber.replace(/ /g, "") == "") {
                    toastr.error("All fields are required", "Fail");
                    l.ladda("stop");
                    needHelpController.talkToUs(phoneNumber, reason, 1);
                    return false;
                }
                if (reason.replace(/ /g, "") == "") {
                    toastr.error("All fields are required", "Fail");
                    l.ladda("stop");
                    needHelpController.talkToUs(phoneNumber, reason, 2);
                    return false;
                }
            }
        },
        sendCallTicket: function(p, r) {
            var l = $(".laddaButton").ladda();
            var a = BASE_URL + "/console/actions/system/sendCallTicket.php";
            return jQuery.ajax({
                url: a,
                method: "post",
                data: {
                    phone: p,
                    reason: r
                },
                async: 1
            }).done(function (e) {
                try {
                    e = JSON.parse(e);
                    if (e == true) {
                        toastr.success("We got your ticket and will call you soon", "Ticket received");
                        swal.close();
                        return 1;
                    } else {
                        toastr.error("There was an error sending your ticket, please try again later", "failed");
                        l.ladda("stop");
                        return !1
                    }
                } catch (e) {
                    l.ladda("stop");
                    toastr.error("There was an error sending your ticket, please try again later", "failed");
                    return !1
                }
            }), !1
        },
        openHelpCenter: function() {
            window.open("http://help.network-leads.com/en/", "_blank");
        }
    }
</script>