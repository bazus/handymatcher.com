<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/index.css?v=1.2" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/firstSteps.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/firstSteps.mobile.css" rel="stylesheet">

    <script>var BASE_URL = "<?php $_SERVER['LOCAL_NL_URL'] ?>";var expireDate = "<?= date('') ?>"</script>

</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<?php if ($showAddProviders == true){ ?>
    <span id="noProviders" class="noProviders" onclick="location.href = '<?= $_SERVER["LOCAL_NL_URL"] ?>/console/categories/leads/leadProviders.php?addProvider=1'">
            <p>You do not have any lead providers set up yet</p>
            <p>Click here to add a lead provider</p>
        </span>
<?php } ?>
<div id="wrapper">
    <?php require_once("side.php"); ?>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once("top.php"); ?>
        <div class="wrapper wrapper-content">


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="firstStep infoStep">
                <div class="stepStatus hideMobile">
                    <i class="fa fa-check-circle"></i>
                </div>
                <div class="stepContent">
                    <h2>First steps</h2>
                    <span>What to do after you register</span>
                </div>
                <div class="stepButton">
                    <img class="hideMobile" style="height: 63px;" src="<?= $_SERVER["LOCAL_NL_URL"] ?>/console/images/icons/tasks.png">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="firstStep">
                <div class="stepStatus">
                    <i class="fa fa-check fa-checked"></i>
                </div>
                <div class="stepContent">
                    <h3 class="checked">Create an account</h3>
                    <span>You did it! Now let's dive in.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="firstStep">
                <div class="stepStatus">
                    <i class="fa fa-check <?php if ($userDoneStep['leadProvider']){ echo "fa-checked";} ?>"></i>
                </div>
                <div class="stepContent">
                    <h3 <?php if ($userDoneStep['leadProvider']){ echo "class='checked'";} ?>>Add your first lead provider</h3>
                    <span>And start receiving leads.</span><br>
                    <a href="http://help.network-leads.com/set-up/how-add-a-lead-providerlead-source" target="_blank">How to add a lead provider?</a>
                </div>
                <div class="stepButton">
                    <button onclick="location.href = '<?= $_SERVER["LOCAL_NL_URL"] ?>/console/categories/leads/leadProviders.php?addProvider=1'" class="btn btn-toDoBtn" <?php if ($userDoneStep['leadProvider']){ echo "disabled";} ?>>Add <span class="hideMobile">Lead </span>Provider</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="firstStep">
                <div class="stepStatus">
                    <i class="fa fa-check <?php if ($userDoneStep['mailInbox']){ echo "fa-checked";} ?>"></i>
                </div>
                <div class="stepContent">
                    <h3 <?php if ($userDoneStep['mailInbox']){ echo "class='checked'";} ?>>Connect your Email account</h3>
                    <span>Send templates and estimates straight from Network Leads.</span><br>
                    <a href="http://help.network-leads.com/set-up/how-add-a-lead-providerlead-source" target="_blank">How to connect an email account?</a>
                </div>
                <div class="stepButton">
                    <button onclick="location.href = '<?= $_SERVER["LOCAL_NL_URL"] ?>/console/categories/mail/mailSettings.php?addAccount=1'" class="btn btn-toDoBtn" <?php if ($userDoneStep['mailInbox']){ echo "disabled";} ?>>Add <span class="hideMobile">Mail </span>Inbox</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="firstStep">
                <div class="stepStatus">
                    <i class="fa fa-check <?php if ($userDoneStep['manuallyLead']){ echo "fa-checked";} ?>"></i>
                </div>
                <div class="stepContent">
                    <h3 <?php if ($userDoneStep['manuallyLead']){ echo "class='checked'";} ?>>Insert a lead manually</h3>
                    <span>Start tracking potential revenue</span><br>
                    <a href="http://help.network-leads.com/set-up/how-to-add-a-lead-manually" target="_blank">How to insert a lead manually?</a>
                </div>
                <div class="stepButton">
                    <button onclick="location.href = '<?= $_SERVER["LOCAL_NL_URL"] ?>/console/categories/leads/leads.php?addManuallyLead=1'" class="btn btn-toDoBtn" <?php if ($userDoneStep['manuallyLead']){ echo "disabled";} ?>>Add <span class="hideMobile">Manually </span>Lead</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-center" style="font-size: 13px;margin-top: 15px;">Already familiar with Network Leads? <a onclick="skipFirstSteps();">Click Here</a> to skip get started.</p>
        </div>
    </div>
</div>
<script>
    function skipFirstSteps(){
        swal({
            title: "Are you sure?",
            text: "We highly recommended to complete all the steps above",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                setCookie("hideFirstSteps",1,14);
                location.href = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/";
            }
        });
    }
    function setCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
</script>

</div>
<?php require_once("footer.php"); ?>
</div>

<?php require_once("rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Flot -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.time.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/index.min.js"></script>
<?php
//if($bouncer['isFromMobile'] == false) {
//    if ($bouncer["userSettingsData"]["firstTutorial"] == "1" && $bouncer["isUserAnAdmin"] == "1") {
//
//        // Admin Tutorial - Stage 1
//        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/admin/adminTutorial_stage1.php");
//
//    } elseif ($bouncer["userSettingsData"]["firstTutorial"] == "1" && $bouncer["isUserAnAdmin"] == "0") {
//        // Non-Admin Tutorial - Stage 1
//        //require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/adminTutorial_stage1.php");
//    }
//}
?>


</body>
</html>