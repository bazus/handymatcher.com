<?php

// track stats only in the production environment
if($_SERVER["ENVIRONMENT"] == "prod") {
    if (isset($analytics)) {
        /*
        if (in_array("mixpanel", $analytics)) {
            require_once($_SERVER['LOCAL_NL_PATH'] . "/console/services/analytics/mixpanel.php");
        }
        */
        if (in_array("google", $analytics)) {
            require_once($_SERVER['LOCAL_NL_PATH'] . "/console/services/analytics/google.php");
        }
        /*
        if (in_array("firebase", $analytics)) {
            require_once($_SERVER['LOCAL_NL_PATH'] . "/console/services/analytics/firebase.php");
        }
        */
        if (in_array("adwords", $analytics)) {
            require_once($_SERVER['LOCAL_NL_PATH'] . "/console/services/analytics/adwords.php");
        }
    }
}