<!-- Global site tag (gtag.js) - Google Ads: 762011834 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-762011834"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'AW-762011834');
</script>