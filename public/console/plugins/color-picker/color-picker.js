function createPicker(obj){
    var colorArr = [
        "#000000",
        "#ffffff",
        "#d54e21",
        "#78a300",
        "#0e76a8",
        "#9cc2cb",
        "#73716e",
        "#ff7700",
        "#ffcc00",
        "#ff66b5"
    ];
    var container = document.createElement("div");
    container.id = "mcp-container";

    var div = document.createElement("div");
    div.id = "customId";
    div.setAttribute("onclick","initColorPicker()");
    div.classList.add("mcp-color");
    div.style.backgroundColor = colorArr[0];

    container.appendChild(div);

    for (var i = 0;i<colorArr.length;i++){
        var color = colorArr[i];
        var div = document.createElement("div");
        div.id = "mcp-"+color;
        div.classList.add("mcp-color");
        div.style.backgroundColor = color;
        div.setAttribute("onclick","pickColor(this)");
        if (i==0){
            div.classList.add("active");
        }

        container.appendChild(div);
    }

    var hiddenInput = document.createElement("input");
    hiddenInput.setAttribute("type","hidden");
    hiddenInput.id = "mcp-color-result";
    container.appendChild(hiddenInput);

    var hiddenColorPicker = document.createElement("input");
    hiddenColorPicker.id = "mcp-hidden-color-picker";
    hiddenColorPicker.setAttribute("type",'color');
    hiddenColorPicker.style.opacity = 0;
    hiddenColorPicker.style.height = "0";
    hiddenColorPicker.style.width = "0";
    hiddenColorPicker.style.position = "absoulte";

    container.appendChild(hiddenColorPicker);

    obj.appendChild(container);
    $(function () {
        $("#mcp-hidden-color-picker").on("change",function () {
            $("#customId").css("background-color",$("#mcp-hidden-color-picker").val());
            $("#mcp-color-result").css("background-color",$("#mcp-hidden-color-picker").val());
            $("#mcp-color-result").val($("#mcp-hidden-color-picker").val());
        });
    });
}
function setCustomColor(color){
    $("#customId").css("background-color",color);
    $("#mcp-color-result").val(color);
    $("#mcp-hidden-color-picker").val(color);
}
function pickColor(obj){
    $(".mcp-color").filter(".active").each(function () {
        $(this).removeClass("active");
    });
    $("#customId").css("background-color",obj.id.substr(4,obj.id.length));
    $(obj).addClass("active");
    $("#mcp-color-result").val(obj.id.substr(4,obj.id.length));
    $("#mcp-hidden-color-picker").val(obj.id.substr(4,obj.id.length));
}
function initColorPicker() {
    $('#mcp-hidden-color-picker').click();
}
function getColor() {
    if($("#mcp-color-result").val() == ""){return "#000000";}
    return $("#mcp-color-result").val();
}