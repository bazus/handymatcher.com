function initCusmod(modalId,settings) {

    var cusmod = $(modalId);
    cusmod.html("");
    if(settings.template !== undefined) {
        cusmod.attr("class","cusmod "+settings.template);
    }else{
        cusmod.attr("class","cusmod");
    }

    var cusmodWrapper = document.createElement("div");

    var cusmodBack = document.createElement("div");
    cusmodBack.className = "cusmod-back";

    var cusmodDialog = document.createElement("div");
    cusmodDialog.className = "cusmod-dialog";

    var cusmodContent = document.createElement("div");
    if(settings.animation !== undefined) {
        cusmodContent.className = "cusmod-content animated "+settings.animation;
    }else{
        cusmodContent.className = "cusmod-content animated fadeIn";
    }

    if(settings.hideCloseBTN === undefined || settings.hideCloseBTN != true) {
        var cusmodClose = document.createElement("a");
        cusmodClose.className = "cusmod-close";
        cusmodClose.innerHTML = "x";
        cusmodClose.setAttribute("data-dismiss", "cusmod");

        cusmodContent.appendChild(cusmodClose);
    }

    var cusmodBody = document.createElement("div");
    cusmodBody.className = "cusmod-body";


    // set header image
    if(settings.headerImage !== undefined) {
        var cusmodHeadImage = document.createElement("div");
        cusmodHeadImage.className = "cusmod-headImage";

        cusmodHeadImage.style.backgroundImage = "url("+settings.headerImage+")";

        cusmodBody.appendChild(cusmodHeadImage);
    }

    // set title
    if(settings.header !== undefined) {

       var cusmodHeader;
       if((settings.header instanceof Element)){
           // if the settings.header is an object
           cusmodHeader = document.createElement("div");
           cusmodHeader.className = "cusmod-header";
           $(cusmodHeader).html(settings.header);

       }else{
           // if the settings.header is just text
           cusmodHeader = document.createElement("h2");
           cusmodHeader.className = "cusmod-header";
           cusmodHeader.innerHTML = settings.header;
       }

       cusmodBody.appendChild(cusmodHeader);
    }

    // set subtitle
    if(settings.subHeader !== undefined) {

        var cusmodSubTitle = document.createElement("h3");
        cusmodSubTitle.className = "cusmod-subTitle";
        cusmodSubTitle.innerHTML = "<strong>"+settings.subHeader+"</strong>"

        cusmodBody.appendChild(cusmodSubTitle);
    }

    // set content
    if(settings.content !== undefined) {

        var cusmodText;
        if((settings.content instanceof Element)){
            cusmodText = document.createElement("div");
            cusmodText.className = "cusmod-text";
            $(cusmodText).html(settings.content);
        }else {
            cusmodText = document.createElement("p");
            cusmodText.className = "cusmod-text";
            cusmodText.innerHTML = settings.content;
        }

        cusmodBody.appendChild(cusmodText);
    }


    var cusmodFooter = document.createElement("div");
    cusmodFooter.className = "cusmod-footer";


    if((settings.footer instanceof Element)){
        if (settings.footer !== undefined){
            $(cusmodFooter).html(settings.footer);
        }
    }else {
        // set buttons
        if (settings.footer !== undefined) {

            var entries = Object.entries(settings.footer);
            for (var i = 0; i < entries.length; i++) {
                var singleBtn = entries[i];

                var btnText = singleBtn[0];
                var btnAction = singleBtn[1];

                var cusmodButton = document.createElement("a");
                cusmodButton.className = "cusmod-button";
                cusmodButton.innerHTML = btnText;

                if (btnAction !== null) {
                    cusmodButton.setAttribute("onClick", btnAction);
                } else {
                    cusmodButton.className = "cusmod-button-close";
                    cusmodButton.setAttribute("data-dismiss", "cusmod");
                }

                cusmodFooter.appendChild(cusmodButton);
            }

        }
    }
    cusmodBody.appendChild(cusmodFooter);

    cusmodContent.appendChild(cusmodBody);

    cusmodDialog.appendChild(cusmodContent);

    cusmodWrapper.appendChild(cusmodBack);
    cusmodWrapper.appendChild(cusmodDialog);

    cusmod.html(cusmodWrapper);

    // show the modal
    cusmod.css("display","block");

    // close the modal if clicked
    $("[data-dismiss='cusmod']").on("click", function(event){
        $(event.target).closest(".cusmod").css("display","none");
    });

    $(".cusmod-dialog").on("click", function(event){
        event.stopPropagation(); //this is important! If removed, you'll get both alerts
    });

    $(".cusmod").on("click", function(event){
        $(event.target).closest(".cusmod").css("display","none");
    });


}

