<?php
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/search.php");

$showWelcomeBackMsg = false;

$searchq = "";
$search_results = array();
$counter = [];
if (isset($_GET['top-search']) && $_GET['top-search']){

    $searchq = trim($_GET['top-search']);

    $search = new search();
    $search_results = $search->searchResult($bouncer["credentials"]["orgId"],$searchq,$bouncer["credentials"]["userId"],$bouncer['isUserAnAdmin']);
    if ($search_results) {
        $counter['users'] = count($search_results['users']);
        $counter['leads'] = count($search_results['leads']);
        $counter['leadsProvider'] = count($search_results['leadsProvider']);

        $counter['count'] = $counter['users'] + $counter['leads'] + $counter['leadsProvider'];
    }
}
function highlightStr($haystack, $needle) {
    preg_match_all("/$needle+/i", $haystack, $matches);
    if (is_array($matches[0]) && count($matches[0]) >= 1) {
        foreach ($matches[0] as $match) {
            $haystack = str_replace($match, '<b>'.$match.'</b>', $haystack);
        }
    }
    return $haystack;
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/search.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <style>
        .my-btn {
            margin-right: 10px;
            color:grey;
        }
        .btn-success{
            color:white !important;
        }
        #buttonsGroup{
            margin-bottom: 14px;
        }
        #buttonsGroup label{
            margin-bottom: 2px;
            margin-right: 10px;
        }
        .btn-default.active{
            box-shadow: none !important;
        }
        @media only screen and (max-width: 480px) {
            #showOnly{
                display: none;
            }
            #buttonsGroup input{
                margin-right: 5px;
            }
        }
    </style>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">

        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li  class="active">
                        <strong>Search Page</strong>
                    </li>
                    <li>
                        <small><i>Search results for: </i><strong><?= $searchq ?></strong></small>
                    </li>
                </ol>
            </div>
        </div>
        <?php if($search_results) { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="input-group" id="buttonsGroup">
                    <label id="showOnly">Show Only:</label>
                    <input type="button" onclick="showOnly('all')" class="btn btn-success btn-xs my-btn btn-all" value="(<?=$counter['count']?>) All">
                    <?php if (isset($search_results['users']) && count($search_results['users']) > 0){?>
                    <input type="button" onclick="showOnly('users')" class="btn btn-default btn-xs my-btn btn-users" value="(<?=$counter['users']?>) Users"><?php } ?>
                    <?php if (isset($search_results['leads']) && count($search_results['leads']) > 0){?>
                    <input type="button" onclick="showOnly('leads')" class="btn btn-default btn-xs my-btn btn-leads" value="(<?=$counter['leads']?>) Leads"><?php } ?>
                    <?php if (isset($search_results['leadsProvider']) && count($search_results['leadsProvider']) > 0){?>
                    <input type="button" onclick="showOnly('providers')" class="btn btn-default btn-xs my-btn btn-providers" value="(<?=$counter['leadsProvider']?>) Leads Providers"><?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <?php if($search_results && $counter['count'] > 0) { ?>
                        <?php foreach ($search_results['users'] as $user) {?>
                        <a href="#" target="" style="cursor: default;">
                            <div class="search-result search-users">
                                <h3><i class="fa fa-user"></i> <?= $user['fullName'] ?></h3><span class="label label-primary pull-right">User</span>
                                <p> <?php if ($user['fullName']){ ?>Name: <?= highlightStr($user['fullName'],$searchq); ?><?php }  if ($user['email']){ ?>| Email: <?= highlightStr($user['email'],$searchq) ?> <?php }  if ($user['phoneNumber']){ ?>| Phone: <?= highlightStr($user['phoneNumber'],$searchq) ?> <?php }  if ($user['phoneCountryCodeState']){ ?> | <?= $user['phoneCountryCodeState'] ?> <?php }  if ($user['jobTitle']){ ?>| Job Title: <?= $user['jobTitle'] ?><?php } ?></p>
                                <div class="hr-line-dashed"></div>
                            </div>
                        </a>
                        <?php } ?>
                            <?php foreach ($search_results['leads'] as $lead) {?>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/leads/lead.php?leadId=".$lead['id']; ?>" target="_blank">
                            <div class="search-result search-leads">
                                <h3><i class="fa fa-address-card-o "></i> <?= $lead['firstname']." ".$lead['lastname'] ?></h3><span class="label label-success pull-right">Lead</span>
                                <p>Job #<?php if($lead["jobNumber"] == NULL || $lead["jobNumber"] == ""){ echo highlightStr($lead['id'],$searchq); }else{ echo highlightStr($lead['jobNumber'],$searchq); }?> <?php if ($lead['firstname'] && $lead["lastname"]){ ?>| Full Name: <?= highlightStr($lead['firstname'],$searchq)." ".highlightStr($lead['lastname'], $searchq) ?> <?php }  if ($lead['email']){ ?>| Email: <?= highlightStr($lead['email'],$searchq) ?><?php }  if ($lead['phone']){ ?> | Phone: <?php echo highlightStr($lead['phone'],$searchq);} if(isset($lead['phone2']) && $lead['phone2'] !== ""){echo " | Phone2: ".highlightStr($lead['phone2'],$searchq)."";} ?></p>
                                <div class="hr-line-dashed"></div>
                            </div>
                        </a>
                        <?php } ?>
                            <?php foreach ($search_results['leadsProvider'] as $provider) {?>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/leads/leadProviders.php?providerId=".$provider['id']; ?>" target="_blank">
                            <div class="search-result search-providers">
                                <h3><i class="fa fa-handshake-o"></i> <?= $provider['providerName'] ?></h3><span class="label label-info pull-right">Lead Provider</span>
                                <p><?php if ($provider['providerName']){ ?>Name: <?= highlightStr($provider['providerName'],$searchq) ?><?php } if ($provider['providerEmail']){ ?> | Email: <?= highlightStr($provider['providerEmail'],$searchq) ?><?php } if ($provider['providerPhone']){ ?> | Phone: <?= highlightStr($provider['providerPhone'],$searchq) ?><?php } if ($provider['providerFax']){ ?> | Fax: <?= highlightStr($provider['providerFax'],$searchq) ?><?php } if ($provider['providerWebsite']){ ?> | Website: <?= highlightStr($provider['providerWebsite'],$searchq) ?><?php } if ($provider['providerAddress']){ ?> | Address: <?= highlightStr($provider['providerAddress'],$searchq) ?> <?php } if ($provider['providerContactName']){ ?>| ContactName: <?= highlightStr($provider['providerContactName'],$searchq) ?><?php } ?></p>
                                <div class="hr-line-dashed"></div>
                            </div>
                        </a>
                        <?php } ?>
                        <?php }else{ ?>
                            <a href="#" target="" style="cursor: default;">
                                <div class="search-result search-users">
                                    <h3>No Results Found</h3><span class="label label-danger pull-right">No Results</span>
                                    <p>No Results For: <?= $searchq ?></p>
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<?php if($showWelcomeBackMsg){ ?>
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Network Leads', 'Welcome back');

            }, 1300);
        });
    </script>
<?php } ?>
<script>
    function showOnly(search){
        if(search !== "all"){
            $(".search-result").each(function(){
                this.style.display = "none";
            });
            var searchKey = ".search-"+search;
            $(searchKey).each(function(){
                this.style.display = "";
            });
        }else{
            $(".search-result").each(function(){
                this.style.display = "";
            });
        }
        $(".my-btn").each(function(){
            if($(this).hasClass("btn-success")){
                this.classList.remove("btn-success");
                this.classList.remove("active");
                this.classList.add("btn-default");
            }else{
                if($(this).hasClass("btn-"+search)){
                    this.classList.add("btn-success");
                    this.classList.add("active");
                }
            }
        });
    }
</script>
</body>
</html>

