<?php

$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/notifications.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/actions/init/timeElapsed.php");

$notification = new notifications($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$bouncer["userData"]["isAdmin"]);
$totalNotification = $notification->getTotalPendingNotifications();
if($totalNotification > 0){

    $allNotification = $notification->getAllNotification();

}
$allSeenNotification = $notification->getAllSeenNotification();
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>
        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        <a>User</a>
                    </li>
                    <li class="active">
                        <strong>Notification</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-top: 13px;">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 animated fadeInRight">
                <div class="mail-box-header">
                    <h2>
                        Unseen Notifications (<span style="color:#28B294;"><?= $totalNotification ?></span>)
                    </h2>
                    <div class="mail-tools tooltip-demo m-t-md">
                    </div>
                </div>
                <div class="mail-box">
                    <?php if(isset($allNotification)){ ?>
                    <table class="table table-striped table-mail">
                        <thead>
                        <tr>
                            <th style="width: 20%;">From</th>
                            <th style="width: 40%;">Title</th>
                            <th style="width: 20%;">Notification Type</th>
                            <th style="width: 20%;" class="text-right">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($allNotification as $notification){ ?>
                            <tr class="unread">
                                <td class="mail-contact"><?= $notification['fullName'] ?></td>
                                <?php if($notification['n_type'] == 1){ // redirect to users iframe?>
                                <td class="mail-subject">
                                    <a style="color:#28B294;" onclick="showCustomModal('categories/iframes/notifications/user.php?id='+<?= $notification['id'] ?>)">
                                        <?= $notification['title'] ?>
                                    </a>
                                </td>
                                <td><i class="fa fa-user fa-fw text-primary"></i>User</td>
                                <?php }elseif($notification['n_type'] == 2){ // redirect to organizations iframe?>
                                <td class="mail-subject">
                                    <a style="color:#28B294;" onclick="showCustomModal('categories/iframes/notifications/org.php?id='+<?= $notification['id'] ?>)">
                                        <?= $notification['title'] ?>
                                    </a>
                                </td>
                                <td><i class="fa fa-users fa-fw text-primary"></i>Organization</td>
                                <?php }elseif($notification['n_type'] == 3){ // redirect to users level iframe?>
                                <td class="mail-subject">
                                    <a style="color:#28B294;" onclick="showCustomModal('categories/iframes/notifications/level.php?id='+<?= $notification['id'] ?>)">
                                    <?= $notification['title'] ?>
                                    </a>
                                </td>
                                <td><i class="fa fa-user-secret fa-fw text-primary"></i>Admin</td>
                                <?php }?>
                                <td class="text-right mail-date"><?= time_elapsed_string($notification['created_at'])?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 animated fadeInRight">
                <div class="mail-box-header">
                    <h2>
                        Seen Notifications
                    </h2>
                    <div class="mail-tools tooltip-demo m-t-md">
                    </div>
                </div>
                <div class="mail-box">
                    <?php if(isset($allSeenNotification) && $allSeenNotification){ ?>
                    <table class="table table-hover table-mail">
                        <thead>
                        <tr>
                            <th style="width: 20%;">From</th>
                            <th style="width: 40%;">Title</th>
                            <th style="width: 20%;">Notification Type</th>
                            <th style="width: 20%;" class="text-right">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($allSeenNotification as $notification){ ?>
                            <tr class="unread">
                                <td class="mail-ontact"><?= $notification['fullName'] ?></td>
                                <?php if($notification['n_type'] == 1){ // redirect to users iframe?>
                                    <td class="mail-subject">
                                        <a style="color:#28B294;" onclick="showCustomModal('categories/iframes/notifications/user.php?id='+<?= $notification['id'] ?>)">
                                          <small class="text-dark"><?= $notification['title'] ?></small>
                                        </a>
                                    </td>
                                    <td><i class="fa fa-user fa-fw text-primary"></i>User</td>
                                <?php }elseif($notification['n_type'] == 2){ // redirect to organizations iframe?>
                                    <td class="mail-subject">
                                        <a style="color:#28B294;" onclick="showCustomModal('categories/iframes/notifications/org.php?id='+<?= $notification['id'] ?>)">
                                            <small><?= $notification['title'] ?></small>
                                        </a>
                                    </td>
                                    <td><i class="fa fa-users fa-fw text-primary"></i>Organization</td>
                                <?php }elseif($notification['n_type'] == 3){ // redirect to users level iframe?>
                                    <td class="mail-subject">
                                        <a style="color:#28B294;" onclick="showCustomModal('categories/iframes/notifications/level.php?id='+<?= $notification['id'] ?>)">
                                            <small><?= $notification['title'] ?></small>
                                        </a>
                                    </td>
                                    <td><i class="fa fa-user-secret fa-fw text-primary"></i>Admin</td>
                                <?php }?>
                                <td class="text-right mail-date"><?= time_elapsed_string($notification['created_at'])?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

</body>
</html>

