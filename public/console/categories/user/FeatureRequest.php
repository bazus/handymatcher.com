
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<style>
    #featureInfoBtn:focus {
        outline: none;
        box-shadow: none;
    }
    body{
        background-color:#ffffff !important;
    }
</style>
<div id="featureRequestContainer" style="padding: 20px;">
    <h1 style="text-align: center">Feature Request</h1>
    <div class="alert alert-info" style="font-size: 15px;text-align: center;">
        Need a feature in the software?<br> Let us know and we will do what we can to make it happen🙂 - The whole system was built for you!
    </div>
    <div>
        <form style="margin-bottom: 25px;" id="featureRequestForm" enctype="multipart/form-data" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/setFeatureRequest.php" method="post">
            <div class="form-group">
                <textarea maxlength="4000" class="form-control form-control-sm" rows="8" style="resize:none;" id="featureDescription" name="featureDescription" placeholder="Description.."></textarea>
                <small id="DescriptionError" style="float:left;display:none;margin: 7px 0;" class="text-danger">*Description is required</small>
            </div>
            <div class="form-group" style="position: relative">
                <button type="button" id="featureInfoBtn" style="position: absolute; right:2px;bottom:4px;background: none;border:none;" data-toggle="popover" role="button" data-trigger="hover" data-placement="top" data-content="We always want what we build to be perfect, please allow us to reach out to you directly to understand more.">
                    <span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></button>
                <input class="form-control form-control-sm" type="tel" id="featurePhone" name="featurePhone" placeholder="Contact Phone Number (optional)" style="padding-right: 30px;">
            </div>
            <div class="custom-file">
                <input onchange="showAttachedFile()" style="width: 100%;" type="file" multiple class="custom-file-input" id="inputGroupFile01" name="featureRequestsFiles[]">
                <label class="custom-file-label" style="height: unset;border: 1px solid #e5e6e7;font-size: .875rem;color: inherit;" for="inputGroupFile01" id="inputGroupFileLabel01">Add files (optional)</label>
            </div>
        </form>
        <div style="text-align: right;position: absolute; bottom: 13px;right: 19px">
            <button type="button" class="swal-button swal-button--cancel" onclick="parent.swal.close()">Close</button>
            <button onclick="$('#featureRequestForm').submit()" id="btnSubmit" style="margin-left:9px" class="swal-button ladda-button ladda-button-demo">Send</button>
        </div>
    </div>
</div>

<script src="<?= $_SERVER['CDN'] ?>/home/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/popper.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script>
$(function() {
    l = $('.ladda-button-demo').ladda();
    $("[data-toggle=popover]").popover();
});

$('#featureRequestForm').ajaxForm({
    beforeSubmit: beforeSubmit,  // pre-submit callback
    success: afterSubmit  // post-submit callback
});


function beforeSubmit(data) {
    $('#DescriptionError').hide();

    if(!$('#featureDescription').val()){
        $('#DescriptionError').show();
        return false;
    }
    l.ladda( 'start' );
}

function afterSubmit(resp,status,other) {
    
    try{
        var data = JSON.parse(resp);
        if(data.status == true){
            sendFeatureRequestEmailToUs($('#featureDescription').val(),$('#featurePhone').val());
            swal({title:"Success!", text:"Your feature request has been sent",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then(function(isConfirm){
                if (isConfirm) {
                    parent.swal.close();
                }
            });
        }else{
            swal({title:"Oops", text:"Your feature request has not been sent, please try again later",icon: "error",
                buttons: {
                    ok:"Ok"
                }
            })
        }
    }catch (e) {
        console.log(e);
        swal({title:"Oops", text:"Your feature request has not been sent, please try again later",icon: "error",
            buttons: {
                ok:"Ok"
            }
        })
    }
    l.ladda('stop');

}
function sendFeatureRequestEmailToUs(featureDescription,featurePhone){
        var strUrl ="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/sendFeatureRequestMail.php", strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                featureDescription:featureDescription,
                featurePhone:featurePhone
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try{
                data = JSON.parse(data);
                if(data == true){
                    console.log("success");
                }else{
                    return false;
                }
            }catch(e){
                return false;
            }
        });

    }
function showAttachedFile(){
    var input = document.getElementById('inputGroupFile01');
    var label = document.getElementById('inputGroupFileLabel01');
    label.innerHTML = input.files.item(0).name;
    updateFile();
}
function updateFile() {

    var input = document.getElementById('inputGroupFile01');
    var output = document.getElementById('inputGroupFileLabel01');
    output.innerHTML = "Choose files";

    var ul = document.createElement("ul");
    ul.setAttribute("style", "margin-bottom:0px;padding-left: 0;list-style: none;");

    if (input.files.length > 3) {
        swal("Maximum 3 files allowed", "", "error");
        output.innerHTML = "Choose file";
        input.value = "";
        return;
    } else {
        output.innerHTML = "";
        for (var i = 0; i < input.files.length; ++i) {
            var li = document.createElement("li");
            li.setAttribute("style", "width:220;white-space:nowrap;overflow:hidden;text-overflow:ellipsis");
            li.innerHTML += input.files.item(i).name;

            ul.appendChild(li);
        }
        output.appendChild(ul);
    }
}

</script>