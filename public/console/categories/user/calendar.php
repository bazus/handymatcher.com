<?php
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

require_once($_SERVER['LOCAL_NL_PATH'] . "/devs/maorDebugger.php");

$pagePermissions = array(false,true,true,array(["calendar",1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/googleAPI.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/googlecalendar.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendarSettings.php");

$organization = new organization($bouncer["credentials"]["orgId"]);
$departments = $organization->getDepartments();


$calendarSettings = new calendarSettings($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
$calendarSettings->checkIfSettingsExists(); // If settings don't exist - create them
$calendarSettingsData = $calendarSettings->getData();
// ================= (START) GOOGLE CALENDAR =================
$googlecalendars = [];
$isAuthorized = false;
$googleCalendarSyncAccount = unserialize($calendarSettingsData["googleCalendarSyncAccount"]);
if($googleCalendarSyncAccount != NULL && $googleCalendarSyncAccount != false){
    // Check the token

    $googleAPI = new googleAPI($googleCalendarSyncAccount["refresh_token"]);
    $isAuthorized = $googleAPI->isAuthorized();

    if($isAuthorized == true) {
        $service = new Google_Service_Calendar($googleAPI->client);

        $googlecalendar = new googlecalendar($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"], $service);
        $googlecalendars = $googlecalendar->getCalendars();
    }
}
// ================= (START) GOOGLE CALENDAR =================

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads - Calendar</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <?php //=============Calendar=============// ?>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print'>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/calendar.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/calendar.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?php echo $_SERVER['LOCAL_NL_URL']; ?>";
    </script>

    <style>
        .holidayEvent{
            background-color: #0080FF !important;
            border-color: #0080FF !important;
        }
        .mainRow {
            padding-bottom: 50px;
        }

        @media only screen and (max-width: 1450px) {
            .mainRow{
                display: -webkit-flex;
                display: flex;
                flex-wrap: wrap;
            }
            .panel1 {
                order: 2;
                width: 100%;
            }
            .panel2 {
                order: 1;
                width: 100%;
            }
        }
         .card-primary{
             background-color:#f3f3f4;
         }
        .fc-event, fc-content{
            word-wrap:break-word;
        }
        .checkbox>label:before{
            background-color: var(--bac) !important;
            border-color: var(--boc) !important;
            outline: none !important;
        }
        .checkbox>label:after{
            color: var(--c) !important;
        }
        input:focus {outline:0;}

        .googleCalendar{
            /*background-color: var(--bac) !important;*/
            /*border-color: var(--boc) !important;*/
        }
        .calendar-move-info{
            display: inline-block;
        }
        iframe{
            width:100%;
            height: 250px;
            position: relative;
        }
        .popover{
            border: 1px darkgray solid;
        }
        .fc-day-grid-event{
            margin-bottom: 2px;
        }
    </style>
</head>

<body class="">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH'] . "/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashboard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH'] . "/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL'] . "/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li class="active">
                        <strong>Calendar</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row rower mainRow">
            <div class="col-md-2 panel1" style="padding:0;padding-right: 7px;">
                <div class="ibox-content" style="padding: 15px 4px 0px 4px;">
                    <div class="feed-activity-list">
                        <div class="feed-element">
                            <div id="miniDatePicker"></div>
                        </div>
                        <div class="feed-element">
                            <div class="form-group">
                                <label>My Calendars</label>


                                <div class="checkbox checkbox-default calendarCheckbox">
                                    <input type="checkbox" onchange="calendarController.getEvents(this.id,'regular')" name="personalCheckbox" id="personalCheckbox" checked>
                                    <label for="personalCheckbox" id="personalCheckbox_label" style="margin-left: 5px;"><?php echo $bouncer["userData"]["fullName"]; ?> (Private)</label>
                                    <div class="ibox-tools">
                                        <a class="dropdown-toggle calendarDropdownLink" data-toggle="dropdown" href="#">
                                            <i class="fa fa-ellipsis-v calendarDotsMenu" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a onclick="calendarController.pickCalendarColor('personalCheckbox')" href="#" class="dropdown-item calendarDropdownLink">Change color</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="checkbox checkbox-default calendarCheckbox">
                                    <input type="checkbox" onchange="calendarController.getEvents(this.id,'regular')" name="organizationCheckbox" id="organizationCheckbox" checked>
                                    <label for="organizationCheckbox" id="organizationCheckbox_label" style="margin-left: 5px;"><?php echo $bouncer["organizationData"]["organizationName"]; ?></label>
                                    <div class="ibox-tools">
                                        <a class="dropdown-toggle calendarDropdownLink" data-toggle="dropdown" href="#">
                                            <i class="fa fa-ellipsis-v calendarDotsMenu" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a onclick="calendarController.pickCalendarColor('organizationCheckbox')" href="#" class="dropdown-item calendarDropdownLink">Change color</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="checkbox checkbox-default calendarCheckbox">
                                    <input type="checkbox" onchange="calendarController.getEvents(this.id,'regular')" name="operationsCheckbox" id="operationsCheckbox" checked>
                                    <label for="operationsCheckbox" id="operationsCheckbox_label" style="margin-left: 5px;">Operations</label>
                                    <div class="ibox-tools">
                                        <a class="dropdown-toggle calendarDropdownLink" data-toggle="dropdown" href="#">
                                            <i class="fa fa-ellipsis-v calendarDotsMenu" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a onclick="calendarController.pickCalendarColor('operationsCheckbox')" href="#" class="dropdown-item">Change color</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="checkbox checkbox-default calendarCheckbox">
                                    <input type="checkbox" onchange="calendarController.getEvents(this.id,'regular')" name="holidays" id="holidays" checked>
                                    <label for="holidays" id="holidays_label" style="margin-left: 5px;">Holidays</label>
                                    <div class="ibox-tools">
                                        <a class="dropdown-toggle calendarDropdownLink" data-toggle="dropdown" href="#">
                                            <i class="fa fa-ellipsis-v calendarDotsMenu" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a onclick="calendarController.pickCalendarColor('holidays')" href="#" class="dropdown-item">Change color</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div id="googleCalendarList">
                                <?php

                                if(count($googlecalendars) > 0){
                                    ?>
                                    <label>Google Calendars</label>
                                    <?php
                                    foreach ($googlecalendars as $googlecalendar){
                                        if($googlecalendar["name"] == "Network Leads"){continue;} // <- do not display the 'Network leads' calendar that we generated and he subscribed to
                                        ?>
                                        <div class="checkbox checkbox-default">
                                            <input type="checkbox" onchange="calendarController.getEvents(this.id,'google')" name="googlecalendar[]" value="<?php echo $googlecalendar["id"]; ?>" id="<?php echo $googlecalendar["id"]; ?>" checked>
                                            <label id="<?php echo $googlecalendar["id"]; ?>_label" style="--c: #fff;--bac: <?php echo $googlecalendar["backgroundColor"]; ?>;--boc: <?php echo $googlecalendar["backgroundColor"]; ?>;margin-left: 5px;" for="<?php echo $googlecalendar["id"]; ?>"><?php echo $googlecalendar["name"]; ?></label>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $connectedToGoogleAsync = false;
                        if ($isAuthorized != NULL && $isAuthorized != false) {
                            $connectedToGoogleAsync = true;
                        }
                        if($connectedToGoogleAsync){
                            ?>
                            <div class="feed-element" id="googleAsyncConnected">
                                <div id="googleAsyncConnectedWrap">
                                <div style="text-align: center;">
                                    <div id="googleSyncButtonsWrapper"><button id="removeGoogleCalendar" onclick="calendarController.removeGoogleCalendarSync()" class="btn btn-white btn-rounded btn-block" style="width: 36px;border-top-right-radius: 0;border-bottom-right-radius: 0;display: inline-table;">X</button><a style="color:unset;" href="<?php  echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/API/authorizeGoogleAPI.php?app=calendar" target="_blank"><button id="changeGoogleAcountButton" class="btn btn-white btn-rounded btn-block" style="border-top-left-radius: 0;border-bottom-left-radius: 0;display: inline-table;margin-top: 0;">Change <span style="color:#4285f4;">G</span><span style="color:#e94235;">o</span><span style="color:#fbbc05;">o</span><span style="color:#4285f4;">g</span><span style="color:#34a853">l</span><span style="color:#e94235;">e</span> Account</button></a></div>
                                    <small style="font-size: 9px;text-align: center;display: block;margin-top: 5px;color: #828282;">Synced: <?php echo $googleCalendarSyncAccount['email']; ?></small>
                                </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="feed-element">
                            <div id="googleAsyncNotConnected" style="<?php if($connectedToGoogleAsync){echo 'display:none;';} ?>">
                                <a style="color:unset;" href="<?php  echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/API/authorizeGoogleAPI.php?app=calendar">
                                    <button class="btn btn-white btn-rounded btn-block">Enable <span style="color:#4285f4;">G</span><span style="color:#e94235;">o</span><span style="color:#fbbc05;">o</span><span style="color:#4285f4;">g</span><span style="color:#34a853">l</span><span style="color:#e94235;">e</span> Sync</button>
                                </a>
                            </div>
                            <div style="margin-top: 7px;">
                                <a style="color:unset;" onclick="openSuncCalendarModal()">
                                    <button class="btn btn-white btn-rounded btn-block">Subscribe to calendar</button>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-10 panel2" style="padding:2px;">
                <div class="sidebar-layout">
                    <div class="sidebar-layout__main" style="font-size:14px">
                        <div id="calendar" class="calendar-example fc fc-cosmo fc-ltr table-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php require_once($_SERVER['LOCAL_NL_PATH'] . "/console/footer.php"); ?>

    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH'] . "/console/rightside.php"); ?>

</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moment.js">//2.22.2</script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Calendar -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/fullcalendar/fullcalendar.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBk4Hg32ReYFDrq57-UGKDf6pNr6iJfJM&libraries=places" type="text/javascript"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<!-- Custom Script -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/calendar.min.js?v=1.3.7.6"></script>

<script>
    //prevent the a tag going to top of page on click
    $('.dropdown-item').click(function(e) {
        e.preventDefault();
    });

    $("#miniDatePicker").datepicker({
        todayBtn: true,

        todayHighlight: true,
    });
    
    $('#miniDatePicker').on('changeDate', function() {
        var val = $('#miniDatePicker').datepicker('getFormattedDate');
        calendarController.calendar.gotoDate(moment(val));
    });

    function setEventTitle(start, end) {
        <?php $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["calendar", 3]));
        if($checkAuths == true) { ?>
        start = start.format("YYYY-MM-DD HH:mm:ss");
        end = end.format("YYYY-MM-DD HH:mm:ss");
        showCustomModal('categories/iframes/addEvent.php?type=1&start=' + start + '&end=' + end);
        <?php } ?>
    }

    function deleteEvent(id){
        <?php $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["calendar", 3]));
        if($checkAuths == true) { ?>
        var eventId = id;
        swal({
            title: "Delete Event",
            text: "You will not be able to recover this event!",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then((isConfirm)=>{
            if (isConfirm) {
                var strUrl = BASE_URL + '/console/actions/system/calendar/deleteEvent.php', strReturn = "";

                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        eventId: eventId,
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function () {
                    calendarController.getEvents();
                });
            }
        });
        <?php } ?>
    }
    calendarController.init();

    <?php if (isset($_GET['eventId'])){ ?>
    showCustomModal('categories/iframes/showEvent.php?eventId='+<?= $_GET['eventId'] ?>);
    <?php } ?>
    
    
    function openSuncCalendarModal() {
        var content = document.createElement("div");
        content.setAttribute("style","text-align: center");

        var pre1 = document.createElement("pre");
        pre1.setAttribute("style","color:#337ab7;cursor:pointer;margin-bottom: 25px;margin-top: 15px;width: 49px;display: inline-block;margin-right:3px;");
        pre1.setAttribute("onClick","copyText('https://www.network-leads.com/api/calendar.php?k=<?php echo $bouncer["userData"]["secretKey"]; ?>')");
        pre1.innerHTML = "Copy";

        var pre2 = document.createElement("pre");
        pre2.setAttribute("style","white-space: nowrap;overflow: scroll;color:#337ab7;cursor:pointer;margin-bottom: 25px;margin-top: 15px;width: 60%;display: inline-block;");
        pre2.setAttribute("onClick","copyText('https://www.network-leads.com/api/calendar.php?k=<?php echo $bouncer["userData"]["secretKey"]; ?>')");
        pre2.innerHTML = "https://www.network-leads.com/api/calendar.php?k=<?php echo $bouncer["userData"]["secretKey"]; ?>";


        content.appendChild(pre1);
        content.appendChild(pre2);

        var footer = document.createElement("div");

        var closeBTN = document.createElement("a");
        closeBTN.className = "cusmod-button";
        closeBTN.setAttribute("data-dismiss","cusmod");
        closeBTN.innerHTML = "Done";

        footer.appendChild(closeBTN);

        var settings = {
            animation:"fadeInUp",
            //template: "cusmod-blue",
            hideCloseBTN:false,
            headerImage: BASE_URL+"/console/images/modals/example3.jpg",
            content: content,
            footer: footer
        };
        initCusmod("#cusmod", settings);
    }

    function copyText(str) {
        var container = document.getElementById("wrapper");

        var el = document.createElement('textarea');
        el.value = str;
        container.appendChild(el);
        el.select();
        document.execCommand('copy');
        container.removeChild(el);

        toastr.success("Copied","Link Copied");
    }
</script>
</body>
</html>

