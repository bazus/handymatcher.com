<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,array(["users",2]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/classes/register.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

$register = new register();
$lastActivationRow = $register->getLastActivationRow($bouncer["credentials"]["userId"]);


$user = new user($_GET['id']);
?>
<!--<link href="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">-->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


<style>
    .passRulesList{
        margin-bottom: 0px;
        list-style: none;
        margin-left: -36px;
    }
</style>
<input type="hidden" name="userId" id="userId" value="<?= $_GET['id'] ?>">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" id="passwordGroup">
                    <label for="password">Choose a new password</label>
                    <input id="password" onkeyup="checkPassesRules();checkErrors=false;" type="password" class="form-control" placeholder="New Password" />
                </div>
                <div class="form-group" id="repasswordGroup">
                    <label for="repassword">Re-enter new password</label>
                    <input id="repassword" onkeyup="checkMatch=true;checkErrors=false;checkPassesRules();" onchange="checkErrors=true;checkPassesRules();" type="password" class="form-control" placeholder="New Password" />
                    <small class="text-danger" id="passwordsdonotmatch" style="display: none;font-size: 15px;">Passwords do not match</small>
                </div>

                <ul class="passRulesList">
                    <li id="isLengthValid">password must be at least 8 characters</li>
                    <li id="isOneLowerCaseValid">password must contain at least one lowercase letter</li>
                    <li id="isOneUpperCaseValid">password must contain at least one uppercase letter</li>
                <ul>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="saveNewPassword()" id="saveNewPassword" disabled>Update Password</button>
            </div>
        </div>
    </div>
</div>
<!--<script src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>-->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    function saveNewPassword(){
        l.ladda( 'start' );

        var password = document.getElementById('password').value;
        var repassword = document.getElementById('repassword').value;
        var userId = document.getElementById('userId').value;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/changeUserPassword.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                password:password,
                repassword:repassword,
                userId:userId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            data = JSON.parse(data);
            $('#myCustomModal').modal('hide');
            l.ladda('stop');
            swal({
                title: "Success!",
                text: "You changed the password",
                icon: "success",
                buttons: {
                    ok:"Ok"
                }
            });
        });
    };

    var checkMatch = false;
    var checkErrors = false;
    function checkPassesRules(){
        var password = document.getElementById('password').value;
        var repassword = document.getElementById('repassword').value;

        var isMatch = false;
        if(checkMatch) {
            if (checkErrors) {
                if (repassword != "" && (password != repassword)) {
                    // Passwords do not match
                    document.getElementById('passwordGroup').className = "form-group has-error";
                    document.getElementById('repasswordGroup').className = "form-group has-error";
                    document.getElementById('passwordsdonotmatch').style.display = '';
                } else {
                    isMatch = true;
                    document.getElementById('passwordGroup').className = "form-group";
                    document.getElementById('repasswordGroup').className = "form-group";
                    document.getElementById('passwordsdonotmatch').style.display = 'none';
                }
            }else{
                if (repassword != "" && (password == repassword)) {
                    isMatch = true;
                }
            }
        }

        var isLengthValid = false;
        var isOneLowerCaseValid = false;
        var isOneUpperCaseValid = false;

        // is length 8
        if(password.length >= 8){
            isLengthValid = true;
        }

        // is lower & upper case
        isOneLowerCaseValid = (/[a-z]/.test(password));
        isOneUpperCaseValid = (/[A-Z]/.test(password));

        if(isLengthValid) {
            document.getElementById('isLengthValid').className = 'text-success';
        }else{
            document.getElementById('isLengthValid').className = '';
        }

        if(isOneLowerCaseValid){
            document.getElementById('isOneLowerCaseValid').className = 'text-success';
        }else{
            document.getElementById('isOneLowerCaseValid').className = '';
        }

        if(isOneUpperCaseValid){
            document.getElementById('isOneUpperCaseValid').className = 'text-success';
        }else{
            document.getElementById('isOneUpperCaseValid').className = '';
        }

        if(isLengthValid && isOneLowerCaseValid && isOneUpperCaseValid && isMatch){
            document.getElementById('saveNewPassword').disabled = false;
            document.getElementById('passwordGroup').className = "form-group";
            document.getElementById('repasswordGroup').className = "form-group";
            document.getElementById('passwordsdonotmatch').style.display = 'none';
            return true;
        }else{
            document.getElementById('saveNewPassword').disabled = true;
        }

        return false;
    }

</script>