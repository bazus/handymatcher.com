<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
if ($bouncer['isUserAnAdmin'] == true) {
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

    $organization = new organization($bouncer["credentials"]["orgId"]);
    $usersOfMyCompany = $organization->getUsersOfMyCompany($bouncer["credentials"]["userId"]);
}
?>
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Network Leads - File Manager</title>

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/dropzone/dropzone.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/iCheck/custom.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/contextMenu/style.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/fileManager.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
        <script>
            var BASE_URL = "<?php echo $_SERVER['LOCAL_NL_URL']; ?>";
            var destDir = "";
        </script>
        <style>
            a.sideFolders:hover button{
                display: block !important;
            }
            .file-box:hover button.deleteBtn{
                display: block !important;
            }
            @media only screen and (max-width: 1199px) {
                .spanText {
                    max-width: none !important;
                }
            }
            @media only screen and (max-width: 768px) {
                .spanText {
                    max-width: 150px !important;
                }
            }
        </style>
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/fileManager.css" rel="stylesheet">
    </head>

    <body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>" ondrop="resetPosition()">
    <div id="wrapper">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

        <div id="page-wrapper" class="gray-bg dashboard-1">
            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

            <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
                <div class="col-lg-10">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                        </li>
                        <li>
                            <a>User</a>
                        </li>
                        <li  class="active">
                            <strong>File Manager</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="file-manager">
                                <?php /*
                                <h5>Show:</h5>
                                <a href="#" class="file-control active">Ale</a>
                                <a href="#" class="file-control">Documents</a>
                                <a href="#" class="file-control">Audio</a>
                                <a href="#" class="file-control">Images</a>
                                */ ?>
                                <div class="hr-line-dashed"></div>
                                <button class="btn btn-primary btn-block" id="uploadButton" onclick="$('#fileUpload').click();">Upload Files</button>
                                <form action="<?= $_SERVER['LOCAL_NL_URL']?>/console/actions/user/uploadFile.php" method="post" id="uploadForm" enctype="multipart/form-data" style="display: none;">
                                    <input type="file" name="file" id="fileUpload" onchange="checkFileBeforeUpload(this);">
                                    <input type="hidden" name="folderId" id="folderId" value="0">
                                    <input type="hidden" name="userIdSelected" id="userIdSelected" value="0">
                                    <input type="submit" id="submitUpload">
                                </form>
                                <div class="hr-line-dashed"></div>
                                <h5>Folders</h5>
                                <ul class="folder-list" id="folderContainer" style="padding: 0">

                                </ul>
                                <span style="cursor: pointer;" onclick="createFolder();">+ Create Folder</span><br>
                                <?php /*
                                <h5 class="tag-title">Tags</h5>
                                <ul class="tag-list" style="padding: 0">
                                    <li><a href="">Family</a></li>
                                    <li><a href="">Work</a></li>
                                    <li><a href="">Home</a></li>
                                    <li><a href="">Children</a></li>
                                    <li><a href="">Holidays</a></li>
                                    <li><a href="">Music</a></li>
                                    <li><a href="">Photography</a></li>
                                    <li><a href="">Film</a></li>
                                </ul>
                                    */ ?>
                                <?php if ($bouncer['isUserAnAdmin'] == true){?>
                                    <hr>
                                    <select id="userToShow" class="form-control" onchange="showSelectedUser(this)">
                                        <option value="">My Files</option>
                                        <?php foreach ($usersOfMyCompany as $user){ ?>
                                            <option value="<?= $user['id'] ?>"><?= $user['fullName'] ?> Files</option>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                                <div class="clearfix"></div>
                                <div class="hr-line-dashed"></div>
                                <small style="font-size: 9px;color:grey;">* Your files are private and only you and your administrator can see them</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="items-container" ondrop="resetPosition()"></div>
                        </div>
                    </div>
                </div>
            </div>


            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
        </div>
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>

    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jasny -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- Toastr -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

    <!-- Ladda -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

    <!-- SweetAlerts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Context Menu -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/contextMenu/script.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/contextMenu/position.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/jquery-touch.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/upload.min.js"></script>

    <?php if (isset($_GET['error']) && $_GET['error'] == 1){ ?>
    <script>
        $(document).ready(function(){
            toastr.error("There Was An Error Uploading Your File/File Size is Too large, Max 10MB","Error");
        });
    </script>
    <?php } ?>
    <?php if ($bouncer['isUserAnAdmin'] == true){?>
        <script>
            function showSelectedUser(obj){
                if (obj.value){
                    userIdSelected = obj.value;
                } else{
                    userIdSelected = null;
                }
                reloader();
            }
        </script>
    <?php } ?>
    </body>
</html>

