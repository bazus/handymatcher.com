<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,array(["users",3]));

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userAuthorization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/userTypes.php");
$userTypes = new userTypes();
$getUserTypes = $userTypes->getData();

$organization = new organization($bouncer["credentials"]["orgId"]);
$organizationData = $organization->getData();
$departments = $organization->getDepartments();
$totalUsersInOrganization = $organization->getTotalUsersInOrganization();

$canAddMoreUsers = $organization->canAddUsers();
if (!isset($isWelcomeMode)){
    $isWelcomeMode = false;
}
$userAuthorization = new userAuthorization();
$getAuthorizations = $userAuthorization->getAuthorizations();

?>
<!--<link href="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">-->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/intl-tel-input/css/intlTelInput.min.css" rel="stylesheet">

<style>
    .passRulesList{
        margin-bottom: 0px;
        list-style: none;
        margin-left: -36px;
    }
    .dropdown-toggle{
        height: 33px;
    }
    .popover-content{
        width: 200px !important;
    }
    #countryCode button{
        width: 100%;
    }

    #country-listbox{
        z-index: 9999;
    }
    .intl-tel-input{
        width: 100%;
    }
    .flag-container{
        z-index: 9998;
    }
    .disabled{
        pointer-events: none;
    }
    .disabled:hover{
        cursor: not-allowed;
    }
    .has_error::placeholder{ color: #cc5965;}

    .form-control{
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <form id="AddUserForm" class="modal-content animated fadeIn">

            <div class="modal-header">
                <h4 class="modal-title">Add User</h4>
                <small>Create a user for your organization</small>
            </div>
            <?php if($canAddMoreUsers){ ?>
            <div class="modal-body">
                <div class="form-group">
                    <input id="fullName" name="fullName" type="text" class="form-control" placeholder="Full Name" value="" />
                    <div id="fullNameErrorDiv"></div>
                </div>
                <div class="form-group">
                    <div class="input-group" style="width: 100%">
                        <input name="email" id="email" type="email" class="form-control" placeholder="Email to send invitation" value="" />
                    </div>
                    <div id="emailErrorDiv"></div>
                </div>
                <div class="form-group">
                    <div class="input-group tooltip-div">
                        <input name="jobTitle" id="jobTitle" type="text" class="form-control" placeholder="Job Title (Employee,Office Manager,Helper,Driver,etc)" value="" />
                        <span class="input-group-addon" style="cursor: pointer;" data-toggle="popover" data-placement="auto bottom" data-content="This data will be visible to the user"><i class="fa fa-question"></i></span>
                    </div>
                    <div id="jobTitleErrorDiv"></div>
                </div>
                <div class="form-group">
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control" placeholder="Phone Number" name="phoneNumber" id="phoneNumber">
                        <input type="hidden" id="country" name="country" value="US">
                    </div>
                    <div id="phoneErrorDiv"></div>
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" name="message" id="message" placeholder="Add a message to invitation.."></textarea>
                </div>
                <?php if ($getUserTypes){?>
                <div class="form-group input-group">
                    <select id="userTypes" name="userTypes" class="form-control">
                        <option value="1">Select User Type</option>
                    <?php foreach ($getUserTypes as $type){?>
                        <option value="<?= $type['id'] ?>"><?= $type['title'] ?></option>
                    <?php }?>
                    </select>
                    <span class="input-group-addon" style="cursor: pointer;" onclick="showUserTypeInfo()"><i class="fa fa-question"></i></span>
                </div>
                <?php } ?>
                <?php if ($departments){?>
                    <div class="form-group">
                        <select id="department" name="department" class="form-control">
                            <option value="">Select Department (Optional)</option>
                            <?php foreach ($departments as $dep){?>
                                <option value="<?= $dep['id'] ?>"><?= $dep['title'] ?></option>
                            <?php }?>
                        </select>
                    </div>
                <?php } ?>

        <hr>
                <div class="form-group" id="authorization">
                    <div id="authorizationDiv" style="cursor: pointer;">
                        <i class="fa fa-arrow-down pull-right" style="font-size: 12px;" id="toggleAuthorization"></i><span class="pull-right" style="font-size: 12px;margin-top: -2px;">Click To Edit</span>
                        <label for="message" style="cursor: pointer;">User Authorizations</label>
                    </div>
                    <div id="authorizationForm" style="display: none;">
                    <?php
                    foreach($getAuthorizations as $authorization){
                        ?>
                            <div class="input-group m-b">
                                <span class="input-group-addon" style="min-width: 139px;text-align: left;"><i style="margin-right: 6px;background-color: #cacaca8a;padding: 3px;border-radius: 29px;cursor: pointer;" class="fa fa-question" data-toggle="popover" data-placement="auto left" data-content="<?php echo $authorization["description"]; ?>" data-original-title="" title=""></i><?php echo $authorization["title"]; ?></span>
                                <select class="form-control" id="<?php echo $authorization["name"]; ?>" name="<?php echo $authorization['name']; ?>">

                                    <?php
                                    foreach($authorization["levels"] as $level=>$levelText){
                                        ?>
                                        <option value="<?php echo $level; ?>" <?php if($level == $authorization["value"]){echo "selected";} ?> <?php if($level == "1"){echo "selected";} ?>><?php echo $levelText; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                    <?php } ?>
                </div>

                </div>
                <?php }else{ ?>
                <div class="modal-body">
                    <h2 style="text-align: center;">Please try again later</h2>
                </div>
                <?php } ?>

        </div>
            <div class="modal-footer" style="<?php if ($isWelcomeMode == true) {echo 'display: none;';}?>">
                <?php if($canAddMoreUsers){ ?>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <input type="submit" value="Send Invitation" class="ladda-button ladda-button-demo btn btn-primary " data-style="zoom-in" id="createNewUserButton">
                <?php }else{ ?>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <?php } ?>
            </div>
        </form>

    </div>
</div>

<!--<script async src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>-->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/intl-tel-input/js/intlTelInput.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/intl-tel-input/js/intlTelInput-jquery.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/validate/jquery.validate.min.js"></script>


<?php if($canAddMoreUsers){ ?>
<script>
    var countryCode = "US";
    var input = $("#phoneNumber").intlTelInput({
        allowDropdown: true,
        separateDialCode: true,
        formatOnDisplay: true,
        preferredCountries: ["us"]
    });

    input.on("countrychange", function() {
        var flag = $(".iti-flag").attr("class").split(/\s+/);
        countryCode = flag[1].toUpperCase();
        document.getElementById("country").value = flag[1].toUpperCase();
    });

    var invites = 0;
    // var countryCodeState = "US";
    // $('#countryCode').flagStrap({
    //     countries: {
    //         "AF": "Afghanistan +93",
    //         "AL": "Albania +355",
    //         "DZ": "Algeria +213",
    //         "AS": "American Samoa +1",
    //         "AD": "Andorra +376",
    //         "AO": "Angola +244",
    //         "AI": "Anguilla +1",
    //         "AG": "Antigua and Barbuda +1",
    //         "AR": "Argentina +54",
    //         "AM": "Armenia +374",
    //         "AW": "Aruba +297",
    //         "AU": "Australia +61",
    //         "AT": "Austria +43",
    //         "AZ": "Azerbaijan +994",
    //         "BS": "Bahamas +1",
    //         "BH": "Bahrain +973",
    //         "BD": "Bangladesh +880",
    //         "BB": "Barbados +1",
    //         "BY": "Belarus +375",
    //         "BE": "Belgium +32",
    //         "BZ": "Belize +501",
    //         "BJ": "Benin +229",
    //         "BM": "Bermuda +1",
    //         "BT": "Bhutan +975",
    //         "BO": "Bolivia, Plurinational State of +591",
    //         "BA": "Bosnia and Herzegovina +387",
    //         "BW": "Botswana +267",
    //         "BR": "Brazil +55",
    //         "IO": "British Indian Ocean Territory +246",
    //         "BN": "Brunei Darussalam +673",
    //         "BG": "Bulgaria +359",
    //         "BF": "Burkina Faso +226",
    //         "BI": "Burundi +257",
    //         "KH": "Cambodia +855",
    //         "CM": "Cameroon +237",
    //         "CA": "Canada +1",
    //         "CV": "Cape Verde +238",
    //         "KY": "Cayman Islands +1",
    //         "CF": "Central African Republic +236",
    //         "TD": "Chad +235",
    //         "CL": "Chile +56",
    //         "CN": "China +86",
    //         "CO": "Colombia +57",
    //         "KM": "Comoros +269",
    //         "CK": "Cook Islands +682",
    //         "CR": "Costa Rica +506",
    //         "CI": "Côte d'Ivoire +225",
    //         "HR": "Croatia +385",
    //         "CU": "Cuba +53",
    //         "CW": "Curaçao +599",
    //         "CY": "Cyprus +357",
    //         "CZ": "Czech Republic +420",
    //         "DK": "Denmark +45",
    //         "DJ": "Djibouti +253",
    //         "DM": "Dominica +1",
    //         "DO": "Dominican Republic +1",
    //         "EC": "Ecuador +593",
    //         "EG": "Egypt +20",
    //         "SV": "El Salvador +503",
    //         "GQ": "Equatorial Guinea +240",
    //         "ER": "Eritrea +291",
    //         "EE": "Estonia +372",
    //         "ET": "Ethiopia +251",
    //         "FK": "Falkland Islands (Malvinas) +500",
    //         "FO": "Faroe Islands +298",
    //         "FJ": "Fiji +679",
    //         "FI": "Finland +358",
    //         "FR": "France +33",
    //         "GF": "French Guiana +594",
    //         "PF": "French Polynesia +689",
    //         "GA": "Gabon +241",
    //         "GM": "Gambia +220",
    //         "GE": "Georgia +995",
    //         "DE": "Germany +49",
    //         "GH": "Ghana +233",
    //         "GI": "Gibraltar +350",
    //         "GR": "Greece +30",
    //         "GL": "Greenland +299",
    //         "GD": "Grenada +1",
    //         "GP": "Guadeloupe +590",
    //         "GU": "Guam +1",
    //         "GT": "Guatemala +502",
    //         "HT": "Haiti +509",
    //         "HN": "Honduras +504",
    //         "HK": "Hong Kong +852",
    //         "HU": "Hungary +36",
    //         "IS": "Iceland +354",
    //         "IN": "India +91",
    //         "ID": "Indonesia +62",
    //         "IR": "Iran +98",
    //         "IQ": "Iraq +964",
    //         "IE": "Ireland +353",
    //         "IL": "Israel +972",
    //         "IT": "Italy +39",
    //         "JM": "Jamaica +1",
    //         "JP": "Japan +81",
    //         "JO": "Jordan +962",
    //         "KZ": "Kazakhstan +7",
    //         "KE": "Kenya +254",
    //         "KI": "Kiribati +686",
    //         "KW": "Kuwait +965",
    //         "KG": "Kyrgyzstan +996",
    //         "LV": "Latvia +371",
    //         "LB": "Lebanon +961",
    //         "LS": "Lesotho +266",
    //         "LR": "Liberia +231",
    //         "LY": "Libya +218",
    //         "LI": "Liechtenstein +423",
    //         "LT": "Lithuania +370",
    //         "LU": "Luxembourg +352",
    //         "MO": "Macao +853",
    //         "MK": "Macedonia (FYROM) +389",
    //         "MG": "Madagascar +261",
    //         "MW": "Malawi +265",
    //         "MY": "Malaysia +60",
    //         "MV": "Maldives +960",
    //         "ML": "Mali +223",
    //         "MT": "Malta +356",
    //         "MH": "Marshall Islands +692",
    //         "MQ": "Martinique +596",
    //         "MR": "Mauritania +222",
    //         "MU": "Mauritius +230",
    //         "MX": "Mexico +52",
    //         "FM": "Micronesia +691",
    //         "MD": "Moldova +373",
    //         "MC": "Monaco +377",
    //         "MN": "Mongolia +976",
    //         "ME": "Montenegro +382",
    //         "MS": "Montserrat +1",
    //         "MA": "Morocco +212",
    //         "MZ": "Mozambique +258",
    //         "MM": "Myanmar (Burma) +95",
    //         "NA": "Namibia +264",
    //         "NR": "Nauru +674",
    //         "NP": "Nepal +977",
    //         "NL": "Netherlands +31",
    //         "NC": "New Caledonia +687",
    //         "NZ": "New Zealand +64",
    //         "NI": "Nicaragua +505",
    //         "NE": "Niger +227",
    //         "NG": "Nigeria +234",
    //         "NU": "Niue +683",
    //         "NF": "Norfolk Island +672",
    //         "NO": "Norway +47",
    //         "OM": "Oman +968",
    //         "PK": "Pakistan +92",
    //         "PW": "Palau +680",
    //         "PA": "Panama +507",
    //         "PG": "Papua New Guinea +675",
    //         "PY": "Paraguay +595",
    //         "PE": "Peru +51",
    //         "PH": "Philippines +63",
    //         "PL": "Poland +48",
    //         "PT": "Portugal +351",
    //         "PR": "Puerto Rico +1",
    //         "QA": "Qatar +974",
    //         "RE": "Réunion +262",
    //         "RO": "Romania +40",
    //         "RU": "Russian Federation +7",
    //         "RW": "Rwanda +250",
    //         "WS": "Samoa +685",
    //         "SM": "San Marino +378",
    //         "ST": "Sao Tome and Principe +239",
    //         "SA": "Saudi Arabia +966",
    //         "SN": "Senegal +221",
    //         "RS": "Serbia +381",
    //         "SC": "Seychelles +248",
    //         "SL": "Sierra Leone +232",
    //         "SG": "Singapore +65",
    //         "SX": "Sint Maarten +1",
    //         "SK": "Slovakia +421",
    //         "SI": "Slovenia +386",
    //         "SB": "Solomon Islands +677",
    //         "SO": "Somalia +252",
    //         "ZA": "South Africa +27",
    //         "SS": "South Sudan +211",
    //         "ES": "Spain +34",
    //         "LK": "Sri Lanka +94",
    //         "SD": "Sudan +249",
    //         "SR": "Suriname +597",
    //         "SZ": "Swaziland +268",
    //         "SE": "Sweden +46",
    //         "CH": "Switzerland +41",
    //         "SY": "Syria +963",
    //         "TW": "Taiwan +886",
    //         "TJ": "Tajikistan +992",
    //         "TZ": "Tanzania +255",
    //         "TH": "Thailand +66",
    //         "TL": "Timor-Leste +670",
    //         "TG": "Togo +228",
    //         "TK": "Tokelau +690",
    //         "TO": "Tonga +676",
    //         "TT": "Trinidad and Tobago +1",
    //         "TN": "Tunisia +216",
    //         "TR": "Turkey +90",
    //         "TM": "Turkmenistan +993",
    //         "TC": "Turks and Caicos Islands +1",
    //         "TV": "Tuvalu +688",
    //         "UG": "Uganda +256",
    //         "UA": "Ukraine +380",
    //         "AE": "United Arab Emirates +971",
    //         "GB": "United Kingdom +44",
    //         "US": "United States +1",
    //         "UY": "Uruguay +598",
    //         "UZ": "Uzbekistan +998",
    //         "VU": "Vanuatu +678",
    //         "VE": "Venezuela +58",
    //         "VN": "Vietnam +84",
    //         "WF": "Wallis and Futuna +681",
    //         "YE": "Yemen +967",
    //         "ZM": "Zambia +260",
    //         "ZW": "Zimbabwe +263"
    //     },
    //     placeholder: {
    //         value: "",
    //         text: "Country Code"
    //     },
    //     buttonSize: "btn-sm",
    //     labelMargin: "10px",
    //     scrollableHeight: "350px",
    //     onSelect: function (value, element) {
    //         countryCodeState = value;
    //         setTimeout("fixFlag()","0")
    //     }
    // });
    //
    // function fixFlag() {
    //     var selectedFlag = document.getElementById('countryCode').childNodes[1].childNodes[0].childNodes[0];

    //     var myNode = document.getElementById('countryCode').childNodes[1].childNodes[0];
    //     while (myNode.firstChild) {
    //         myNode.removeChild(myNode.firstChild);
    //     }
    //     document.getElementById('countryCode').childNodes[1].childNodes[0].appendChild(selectedFlag);
    // }
    // fixFlag();

    // function checkNewUser(){

        // var fullName = document.getElementById('fullName').value;
        // var jobTitle = document.getElementById('jobTitle').value;
        // var phoneNumber = document.getElementById('phoneNumber').value;
        //
        // if(fullName != "" && jobTitle != "" && phoneNumber!= ""){
        //     document.getElementById('createNewUserButton').classList.remove("disabled");
        //     if (document.getElementById('createNewUserButtonFromWelcome') !== null){
        //         document.getElementById('createNewUserButtonFromWelcome').classList.remove("disabled");
        //     }
        // }else{
        //     document.getElementById('createNewUserButton').classList.add("disabled");
        //     if (document.getElementById('createNewUserButtonFromWelcome') !== null){
        //         document.getElementById('createNewUserButtonFromWelcome').classList.add("disabled");
        //     }
        // }
    // }

    // function checkEmail(){
    //     var email = document.getElementById('email').value;
    //     if(email == ""){return false;}
    //
    //     if(validateEmail(email)){
    //         checkNewUser();
    //     }
    // }
    //
    // function validateEmail(email) {
    //     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //     return re.test(String(email).toLowerCase());
    // }

    var l;
    $(document).ready(function () {

        l = $('.ladda-button-demo').ladda();


        $('#AddUserForm').validate({

            errorPlacement: function(error, element) {
                if (element.attr("name") == "fullName" )
                    error.insertAfter("#fullNameErrorDiv");
                else if  (element.attr("name") == "email" )
                    error.insertAfter("#emailErrorDiv");
                else if  (element.attr("name") == "jobTitle" )
                    error.insertAfter("#jobTitleErrorDiv");
                else if (element.attr("name") == "phoneNumber" )
                    error.insertAfter("#phoneErrorDiv");
            },

            rules: {
                fullName: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                        remote: {
                            url:"<?php echo $_SERVER['LOCAL_NL_URL'];?>/console/actions/user/checkEmailExist.php",
                            type: "post"
                        }
                },
                jobTitle: {
                    required: true,
                },
                phoneNumber: {
                    required: true,
                    number: true
                }
            },
            messages:{
                    email:{
                        remote: "Email already in use!"
                    }
            },

            submitHandler: function (form) {

                sendInvitation();
                return false;
            }

         });

    });
    
    function sendInvitation(){
        l.ladda( 'start' );

        var fullName = document.getElementById('fullName').value;
        var email = document.getElementById('email').value;
        var jobTitle = document.getElementById('jobTitle').value;
        var phoneNumber = document.getElementById('phoneNumber').value;
        var message = document.getElementById('message').value;
        var form = $("#authorizationForm").find("select");
        var formData = form.serialize();
        var formData = formData;
        var userTypes = document.getElementById("userTypes").value;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/inviteNewUser.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                fullName:fullName,
                email:email,
                jobTitle:jobTitle,
                <?php if ($departments){?>
                department: document.getElementById("department").value,
                <?php }?>
                <?php if ($getUserTypes){?>
                userTypes: userTypes,
                <?php }?>
                countryCodeState:countryCode,
                phoneNumber:phoneNumber,
                message:message,
                auth: formData,
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                l.ladda('stop');
                $('#myCustomModal').modal('hide');
                if (data == true) {
                    swal({
                        title: "Success!",
                        text: "An invitation sent to " + email,
                        icon: "success",
                        buttons: {
                            ok:"Ok"
                        },
                        timer: 3000
                    }).then( (value) => {
                        window.location="?tab=users";
                    });
                } else {
                    swal({
                        title: "Oops",
                        text: "We got an error. Please try again later or contact our support team at support@network-leads.com",
                        icon: "error",
                        buttons: {
                            go: "Continue"
                        }
                    }).then( (value) => {
                        window.location="?tab=users";
                    });;
                }
            }catch (e) {
                l.ladda('stop');
                swal({
                    title: "Oops",
                    text: "We got an error. Please try again later or contact our support team at support@network-leads.com",
                    icon: "error",
                    buttons: {
                        go: "Continue"
                    },
                }).then( (value) => {
                    window.location="?tab=users";
                });;
            }

        });
    }

    function sendInvitationFromWelcome(){
        // var laddatButton = $('.myLaddaButton').ladda();
        // laddatButton.ladda( 'start' );

        var fullName = document.getElementById('fullName').value;
        var email = document.getElementById('email').value;
        var jobTitle = document.getElementById('jobTitle').value;
        var phoneNumber = document.getElementById('phoneNumber').value;
        var message = document.getElementById('message').value;
        var form = $("#authorizationForm");
        var formData = form.serialize();
        var userTypes = document.getElementById("userTypes").value;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/inviteNewUser.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                fullName:fullName,
                email:email,
                jobTitle:jobTitle,
                countryCodeState:countryCode,
                phoneNumber:phoneNumber,
                message:message,
                <?php if ($getUserTypes){?>
                userTypes: userTypes,
                <?php }?>
                auth: formData,
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {


            data = JSON.parse(data);
            laddatButton.ladda('stop');
            if(data == true){


                //window.location = BASE_URL+"/console/welcome.php?page=3&email="+email;

                var container = "Invitation sent!";

                swal({
                    title: "Invitation sent!",
                    content: container,
                    buttons: {
                        addmore: {text:"Add another user",className:"btn-default"},
                        finish: "Finish"
                    },
                    icon: false,
                }).then((x)=> {

                    if(x == "addmore"){
                        document.getElementById('fullName').value = "";
                        document.getElementById('email').value = "";
                        document.getElementById('jobTitle').value = "";
                        document.getElementById('phoneNumber').value = "";
                        document.getElementById('message').value = "";
                        document.getElementById("userTypes").value = 1;
                        <?php foreach($getAuthorizations as $authorization){?>
                        document.getElementById("<?= $authorization['name'] ?>").value = 1;
                        <?php } ?>
                    }
                    if(x == "finish"){
                        window.location = BASE_URL+"/console/";
                    }

                });


            }else{
                swal({
                    title: "Oops",
                    text: "We got an error. Please try again later or contact our support team at support@network-leads.com",
                    icon: "error",
                    buttons: {
                        ok:"Ok"
                    }
                });
            }

        });
    }

    $( document ).ready(function() {
        $("[data-toggle=popover]")
            .popover();
    });

    $("#authorizationDiv").on("click",function(){
         $("#authorizationForm").slideToggle(350);
         if ($("#toggleAuthorization").hasClass("fa-arrow-down")){
             $("#toggleAuthorization").removeClass("fa-arrow-down");
             $("#toggleAuthorization").addClass("fa-arrow-up");
         }else{
             $("#toggleAuthorization").removeClass("fa-arrow-up");
             $("#toggleAuthorization").addClass("fa-arrow-down");
         }
    });

    function showUserTypeInfo(){
        var content = document.createElement("div");
        content.classList.add("text-left");

        var h2 = document.createElement("h2");
        h2.style.fontSize = "21px";
        h2.style.fontWeight = "bold";
        var h2Text = document.createTextNode("Standard");

        var p = document.createElement("p");
        p.style.fontSize = "13px";
        var pText = document.createTextNode("Standard accounts are limited to basic software functionality with no access to change or adapt account settings.");

        p.appendChild(pText);
        h2.appendChild(h2Text);
        content.appendChild(h2);
        content.appendChild(p);

        var h2 = document.createElement("h2");
        h2.style.fontSize = "21px";
        h2.style.fontWeight = "bold";
        var h2Text = document.createTextNode("Sales");

        var p = document.createElement("p");
        p.style.fontSize = "13px";
        var pText = document.createTextNode("Sales accounts can access basic statistics and functions relevant to sales and lead management.");

        p.appendChild(pText);
        h2.appendChild(h2Text);
        content.appendChild(h2);
        content.appendChild(p);

        var h2 = document.createElement("h2");
        h2.style.fontSize = "21px";
        h2.style.fontWeight = "bold";
        var h2Text = document.createTextNode("Manager");

        var p = document.createElement("p");
        p.style.fontSize = "13px";
        var pText = document.createTextNode("Manager accounts may access all aspects of the software and enjoy all administrative capabilities.");

        p.appendChild(pText);
        h2.appendChild(h2Text);
        content.appendChild(h2);
        content.appendChild(p);

        swal({
            content: content,
            buttons: {
                skip:{
                    text:"Got It",
                }
            }
        });
    }


</script>
<?php } ?>