<?php
$pagePermissions = array(false,true,true,array(["organization",1],['departments',1],['users',1]));

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/department.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userAuthorization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/userTypes.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

$userTypes = new userTypes();
$getUserTypes = $userTypes->getData();
//require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
//require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";
//
//$s3 = new s3();
$organization = new organization($bouncer["credentials"]["orgId"]);
$organizationData = $organization->getData();
$totalUsersInOrganization = $organization->getTotalUsersInOrganization();
$usersOfMyCompany = $organization->getUsersOfMyCompany($bouncer["credentials"]["userId"]);
if($bouncer["userData"]["organizationTypeId"] == "1") {
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingCompany.php");

    $movingCompany = new movingCompany($bouncer["credentials"]["orgId"]);
    $movingLicenses = $movingCompany->getLicenses();

    if(!$movingLicenses){
        $movingLicenses = array();
        $movingLicenses["DOT"] = "";
        $movingLicenses["ICCMC"] = "";
        $movingLicenses["registration"] = "";
    }
}

$canAddMoreUsers = $organization->canAddUsers();

$checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",3]));
if($checkAuths == false) {
    $canAddMoreUsers = false;
}

$maxUsersAllowd = "1";
if($bouncer['organizationData']["organizationPackage"] == 3){
    $maxUsersAllowd = "unlimited";
}

$departments = $organization->getDepartments();

$tab = "main";
if(isset($_GET['tab'])){
    if($_GET['tab'] == "users"){$tab = "users";}
    if($_GET['tab'] == "departments"){$tab = "departments";}
}
$loadFooterHelp = false;
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads - User Management</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/management.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/management.css" rel="stylesheet">
    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>
    <?php
    $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",2]));
    if($checkAuths == false){ ?>
        <style>
            .inputForEditAuthorization{
                background-color: #eee;
                opacity: 1;
                cursor: not-allowed;
                pointer-events: none;
            }
        </style>
    <?php } ?>
    
    <style>
        .swal-organization-key-info{
            width: 600px !important;
            overflow-y: hidden;
        }

        .companyKeyStatus{
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
        }
        .companyKeyStatus:hover,.companyKeyStatus:active,.companyKeyStatus:visited{
            color:#fff !important;
        }
        .social-network-form-wrap span {
            font-size: 20px;
        }
        .social-network-form-wrap span i{
            vertical-align: middle;
        }
        #organizationDataForm .form-group {
            margin-bottom: 5px;
        }
        @media only screen and (max-width: 992px) {
            #cityStateZipInputs{
                flex-wrap: wrap;
            }
            #cityStateZipInputs input[type="text"]{
                margin-right: 0!important;
                margin-bottom: 5px;
            }
        }
        @media only screen and (max-width: 767px) {
            #errorOrganizationNameWrapper label{
                display: none;
            }
            #errorOrganizationKeyWrapper label{
                display: none;
            }
        }

        .hr-line-dashed{
            margin: 15px 0;
        }
        @media (max-width: 1200px) {
            .main-col, .side-col{
                width: 100%;
            }
        }
        @media (min-width: 1200px) and (max-width: 1434px) {
            .remove-org-logo{
               float: left!important;
                margin-bottom: 5px;
                margin-right: 70px;
            }
        }
    </style>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li  class="active">
                        <strong>Organization Management</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-9 main-col">
                <div class="ibox">
                    <div class="ibox-content">
                            <div id="organizationTopRight" style="display: none;">
                                <?php if($bouncer["isUserAnAdmin"]){ ?>
                                    <a href="../organization/billing.php" id="organization_billing" class="btn btn-sm btn-primary" style="float: right;"><i class="fa fa-credit-card"></i> Billing</a>
                                <?php } ?>
                            </div>
                            <div id="usersTopRight" style="display: none;float: right;">

                                <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="inviteUserToOrg()"> Add New User</button>
                            </div>
                        <?php
                        $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["departments",3]));
                        if($checkAuths == true) {
                        ?>
                            <div id="depTopRight" style="display: none;float: right;">
                                <?php if($bouncer["isNotFreeUser"]){ ?>
                                    <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="showCustomModal('categories/iframes/addNewDep.php')"> Add New Department</button>
                                <?php }else{ ?>
                                    <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="limitReached('package')"> Add New Department</button>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="clients-list" id="managementTabs" style="margin-top: 34px;">
                            <ul class="nav nav-tabs" id="organization_tabs">
                                <?php
                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",1]));
                                if($checkAuths == true) {
                                ?>
                                    <li class="<?php if($tab == "main"){ ?>active<?php } ?>"><a data-toggle="tab" href="#mainTab" onclick="changedTab('main')"><i class="fa fa-bars"></i> <span id="myOrg"</span></a></li>
                                <?php }
                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",1]));
                                if($checkAuths == true) {
                                ?>
                                <li class="<?php if($tab == "users"){ ?>active<?php } ?>"><a data-toggle="tab" href="#usersTab" onclick="changedTab('users')"><i class="fa fa-users"></i> Users</a></li>
                                <?php }
                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["departments",1]));
                                if($checkAuths == true) {
                                ?>
                                <li class="<?php if($tab == "departments"){ ?>active<?php } ?>"><a data-toggle="tab" href="#departmentsTab" onclick="changedTab('departments')"><i class="fa fa-building"></i> Departments</a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">

                                <?php
                                    $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",1]));
                                    if($checkAuths == true) {
                                ?>
                                <div id="mainTab" class="tab-pane <?php if($tab == "main"){ ?>active<?php } ?>">
                                    
                                    <form class="form-horizontal" id="organizationDataForm">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Company Name</label>
                                            <div class="col-sm-10" style="display: flex; justify-content: initial">
                                                <input  style="margin-right: 5px;" id="companyName" value="<?php echo $organizationData["organizationName"]; ?>" name="organizationName" type="text" class="form-control inputForEditAuthorization">
                                                <input  value="<?php echo $organizationData["organizationType"]; ?>" type="text" class="form-control" disabled placeholder="Company Type">
                                            </div>
                                    </div>
                                        <div class="form-group" style="margin:0; display: none" id="errorOrganizationNameWrapper">
                                            <label class="col-sm-2 control-label" style="padding-top:0;"></label>
                                            <div class="col-sm-10" style="padding:0;">
                                                <div id="errorOrganizationName"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" style="padding-right: 6px;">Company Key<span onclick="showCompanyKeyInfo(event)"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px; vertical-align: middle;" class="fa fa-info-circle"></i></span></span></label>
                                            <div class="col-sm-10">

                                                <div class="input-group" style="width: 100%;">
                                                <input  id="companyKey" value="<?php echo $organizationData["organizationNameKey"]; ?>" name="organizationNameKey" type="text" class="form-control inputForEditAuthorization">
                                                    <span style="display: none" class="input-group-btn input-group-append companyKeyStatus" id="companyKeyNotTaken"><button class="btn btn-white bootstrap-touchspin-up  bg-primary" type="button"><i class="fa fa-check" aria-hidden="true"></i></button></span>
                                                    <span style="display: none" class="input-group-btn input-group-append companyKeyStatus" id="companyKeyTaken"><button class="btn btn-white bootstrap-touchspin-up bg-danger" type="button"><i class="fa fa-times" aria-hidden="true"></i></button></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin:0; display: none" id="errorOrganizationKeyWrapper">
                                            <label class="col-sm-2 control-label"  style="padding-top:0;"></label>
                                            <div class="col-sm-10" style="padding:0;">
                                                <div id="errorOrganizationKey"></div>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Address</label>
                                            <div class="col-sm-10"><input value="<?php echo $organizationData["address"]; ?>" name="address" placeholder="Company Address" type="text" class="form-control inputForEditAuthorization"></div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group"><label class="col-sm-2 control-label">Location</label>
                                                <div id="cityStateZipInputs" class="col-sm-10" style="display: flex; justify-content: initial">
                                                    <input style="margin-right:5px" value="<?php echo $organizationData["city"]; ?>" name="city" placeholder="Company City" type="text" class="form-control inputForEditAuthorization">
                                                    <input style="margin-right:5px" value="<?php echo $organizationData["state"]; ?>" name="state" placeholder="Company State" type="text" class="form-control inputForEditAuthorization">
                                                    <input value="<?php echo $organizationData["zip"]; ?>" name="zip" placeholder="Company Zip Code" type="text" class="form-control inputForEditAuthorization">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Phone</label>
                                            <div class="col-sm-10"><input value="<?php echo $organizationData["phone"]; ?>" name="phone" placeholder="Company Phone Number" type="text" class="form-control inputForEditAuthorization"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Website</label>
                                            <div class="col-sm-10"><input value="<?php echo $organizationData["website"]; ?>" name="website" placeholder="Company Website" type="text" class="form-control inputForEditAuthorization"></div>
                                        </div>
                                        <?php  if($bouncer["userData"]["organizationTypeId"] == "1"){?>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group"><label class="col-sm-2 control-label">Licenses</label>
                                                <div class="col-sm-10" style="display: flex; justify-content: initial">
                                                    <input style="margin-right:5px" value="<?php echo $movingLicenses['DOT']; ?>" name="usdot" placeholder="Company USDOT" type="text" class="form-control inputForEditAuthorization">
                                                    <input style="margin-right:5px" value="<?php echo $movingLicenses['ICCMC']; ?>" name="mcc" placeholder="Company MC" type="text" class="form-control inputForEditAuthorization">
                                                    <input value="<?php echo $movingLicenses['registration']; ?>" name="registration" placeholder="Company Registration #" type="text" class="form-control inputForEditAuthorization">
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="hr-line-dashed"></div>
                                            <div class="form-group social-network-form-wrap"><label class="col-sm-2 control-label"><span><i class="fa fa-facebook-official" aria-hidden="true"></i></span></label>
                                                <div class="col-sm-10">
                                                    <input placeholder="Facebook Link" id="facebook" value="<?php echo $organizationData["facebookLink"]; ?>" name="facebook" type="text" class="form-control inputForEditAuthorization">
                                                </div>
                                            </div>
                                            <div class="form-group social-network-form-wrap"><label class="col-sm-2 control-label"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></label>
                                                <div class="col-sm-10">
                                                    <input placeholder="Instagram Link" id="instagram" value="<?php echo $organizationData["instagramLink"]; ?>" name="instagram" type="text" class="form-control inputForEditAuthorization">
                                                </div>
                                            </div>
                                            <div class="form-group social-network-form-wrap"><label class="col-sm-2 control-label"><span id="twitterSpan"><i class="fa fa-twitter" aria-hidden="true"></i></span></label>
                                                <div class="col-sm-10">
                                                    <input placeholder="Twitter Link" id="twitter" value="<?php echo $organizationData["twitterLink"]; ?>" name="twitter" type="text" class="form-control inputForEditAuthorization">
                                                </div>
                                            </div>
                                            <div class="form-group social-network-form-wrap"><label class="col-sm-2 control-label"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></label>
                                                <div class="col-sm-10">
                                                    <input placeholder="LinkedIn Link" id="linkedin" value="<?php echo $organizationData["linkedInLink"]; ?>" name="linkedin" type="text" class="form-control inputForEditAuthorization">
                                                </div>
                                            </div>
                                        <br>

                                        <?php
                                        $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",2]));
                                        if($checkAuths == true){
                                            ?>
                                            <div class="form-group" style="margin-bottom: 0px; margin-right: 0px;">
                                                <input type="submit" value="Save Changes" class="pull-right ladda-button btn btn-primary" id="saveChanges"  data-style="zoom-in">
                                            </div>
                                        <?php } ?>

                                    </form>

                                        <div class="hr-line-dashed"></div>

                                </div>
                                <?php }
                                    $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",1]));
                                    if($checkAuths == true) {
                                ?>
                                <div id="usersTab" class="tab-pane <?php if($tab == "users"){ ?>active<?php } ?>">
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                                            <div class="table-responsive" style="height: 100%;">
                                                <table class="table table-hover usersTable" style="margin-bottom: 65px;">
                                                    <thead>
                                                        <th>#</th>
                                                        <th class="mob"></th>
                                                        <th>Name</th>
                                                        <th>User Type</th>
                                                        <th>
                                                            <select class="form-control" style="width: 132px;display: inline-table;padding: 1px;height: 23px;font-size: 13px;" onchange="showUsersFromDep(this.value)">
                                                                <option value="">All Departments</option>
                                                                <?php foreach ($departments as $dep){?>
                                                                    <option value="<?= $dep['id'] ?>"><?= $dep['title'] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </th>
                                                        <th class="mob"></th>
                                                        <th>Email</th>
                                                        <th class="mob">State</th>
                                                        <th>Phone</th>
                                                        <th>Status</th>
                                                        <th style="text-align: center">Actions</th>
                                                    </thead>
                                                    <tbody id="usersTableBody">
                                                    <?php
                                                    if (count($usersOfMyCompany) > 0){
                                                    foreach ($usersOfMyCompany as $user){
                                                        $userClass = new user($user['id'],true);

                                                        $department = new department($user["depId"],$bouncer["credentials"]["orgId"]);
                                                        $departmentData = $department->getData(); ?>
                                                        <tr id="user-<?= $user["id"] ?>" onclick="showUserTab('<?= $user["id"] ?>')" class="<?php if($user["isDeleted"] == "1"){ ?> deletedUser <?php } ?> <?php if($user["depId"] != NULL){echo "department_".$user["depId"];} ?>">
                                                            <td>
                                                                <div class="checkbox checkbox-primary">
                                                                    <input class="styled" type="checkbox" onclick="toggleEmailUser('<?= $user["id"] ?>','<?= $user["fullName"] ?>')">
                                                                    <label></label>
                                                                </div>
                                                            </td>
                                                            <td class="client-avatar mob">
                                                                <?php if($user['profilePicture']){ ?>
                                                                    <img alt="image" src="<?php try{ echo $user['profilePicture'];}catch (Exception $e){echo $_SERVER['LOCAL_NL_URL']."/console/img/icons/user.png";} ?>">
                                                                <?php }else{ ?>
                                                                    <img alt="image" src="<?= $_SERVER['LOCAL_NL_URL']."/console/img/icons/user.png" ?>">
                                                                <?php } ?>
                                                            </td>
                                                            <td><a data-toggle="tab" href="#contact-<?= $user["id"] ?>" id="contact-<?= $user["id"] ?>-trigger" class="client-link"><?= $user['fullName'] ?></a></td>
                                                            <td><?= $user['typeTitle'] ?></td>
                                                            <td><?= $departmentData['title'] ?></td>
                                                            <td class="contact-type mob"><i class="fa fa-envelope"></i></td>
                                                            <td><?= ($user['email']) ? $user['email'] : "<b>Pending</b>"; ?></td>
                                                            <td class="contact-type mob"><i class="fa fa-phone"> <?= $user['phoneCountryCodeState'] ?></i></td>
                                                            <td><?= $user['phoneNumber'] ?></td>
                                                            <td class="client-status">
                                                                <?php if($user["isDeleted"] == "1"){ ?>
                                                                    <span class="label label-danger center-block">Deleted</span>
                                                                <?php }else{ ?>
                                                                    <?php if($user["isActive"] == "1"){ ?>
                                                                        <span class="label label-primary center-block">Active</span>
                                                                        <?php if($user["isAdmin"] == "1"){ ?>
                                                                            <span class="label label-default center-block">Admin</span>
                                                                        <?php } ?>
                                                                    <?php }else{ ?>
                                                                        <button type="button" class="btn btn-outline btn-xs btn-default" onclick="resendInvitationEmail('<?= $user["id"]; ?>',this)">Resend Invitation</button>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </td>
                                                            <td style="text-align: center">
                                                                <?php
                                                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",2]));
                                                                if($checkAuths == true) {
                                                                    ?>
                                                                    <?php if ($user['id'] == $organizationData['userIdCreated']){ ?>
                                                                        <span class="label label-default">Owner</span>
                                                                    <?php }else{ ?>
                                                                    <?php if($bouncer["isNotFreeUser"]){ ?>
                                                                        <div class="dropdown">
                                                                            <a data-toggle="dropdown" id="menu<?= $user['id'] ?>" class="dropdown-toggle">
                                                                                <i class="fa fa-gear"></i>
                                                                            </a>
                                                                            <ul class="dropdown-menu" role="menu<?= $user['id'] ?>" aria-labelledby="menu<?= $user['id'] ?>" style="margin-left: -102px;">
                                                                                <li role="presentation"><a role="menuitem" style="color:black" onclick="showCustomModal('categories/user/changeUserPassword.php?id='+<?= $user['id'] ?>)">Change Password</a></li>
                                                                                <?php
                                                                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",4]));
                                                                                if($checkAuths == true) {
                                                                                    ?>
                                                                                    <li role="presentation">
                                                                                        <?php if($user["isDeleted"] == "1"){ ?>
                                                                                            <?php if($canAddMoreUsers){ ?>
                                                                                                <a role="menuitem" class="label-default" style="color:white;" onclick="revokeUser(<?= $user["id"]?>,0,this)">Un-revoke</a>
                                                                                            <?php }else{ ?>
                                                                                                <a role="menuitem" class="label-default" style="color:white;" title="You have reached the maximum users allowed" disabled="disabled">Un-revoke</a>
                                                                                            <?php }
                                                                                        }else{ ?>
                                                                                            <a role="menuitem" class="label-danger" style="color:white;" onclick="revokeUser(<?= $user["id"]?>,1,this)">Revoke</a>
                                                                                        <?php } ?>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        </div>
                                                                    <?php }else{ ?>
                                                                        <a id="menu<?= $user['id'] ?>" onclick="limitReached('package')" class="dropdown-toggle">
                                                                            <i class="fa fa-gear"></i>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <?php }?>
                                                                <?php }else{ ?>
                                                                    <span class="label label-default">Disabled</span>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php }}else{ ?>
                                                        <tr>
                                                            <td colspan="8" style="text-align: center;">No Users In Organization Yet</td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr id="noUsers" style="display: none;">
                                                        <td colspan="8" style="text-align: center;">No Users In Organization Yet</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <span class="pull-right small text-muted" style="margin-top: 15px;"><?= $totalUsersInOrganization ?> Of <?php echo $maxUsersAllowd; ?> Users In Organization</span><br>
                                        </div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                </div>
                                <?php } ?>
                                <?php
                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["departments",1]));
                                if($checkAuths == true) {
                                ?>
                                <div id="departmentsTab" class="tab-pane <?php if($tab == "departments"){ ?>active<?php } ?>">
                                    <div class="table-responsive" style="height: 100%;">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>ZIP</th>
                                            <th>Phone</th>
                                            <th>Owner Name</th>
                                            </thead>
                                            <tbody>
                                            <?php if (count($departments) > 0){ foreach ($departments as $dep){?>
                                                <tr onclick="showDepartmentTab('<?= $dep["id"] ?>')" style="cursor:pointer;">
                                                    <td><a data-toggle="tab" href="#department-<?= $dep["id"] ?>" id="department-<?= $dep["id"] ?>-trigger" class="client-link"><?= $dep['title'] ?></a></td>
                                                    <td><?= $dep['description'] ?></td>
                                                    <td><?= $dep['address'] ?></td>
                                                    <td><?= $dep['city'] ?></td>
                                                    <td><?= $dep['state'] ?></td>
                                                    <td><?= $dep['zip'] ?></td>
                                                    <td><?= $dep['phone'] ?></td>
                                                    <td><?= $dep['owner'] ?></td>
                                                </tr>
                                            <?php }}else{ ?>
                                            <tr><td colspan="8" style="text-align: center;">No Departments Yet</td></tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 side-col" style="padding-bottom: 25px;">
                <div class="ibox ">

                    <div class="ibox-content" id="organization_rightside_ibox">
                        <div class="tab-content" id="sideTabs">
                            <?php
                            $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",1]));
                            if($checkAuths == true) {
                            ?>
                            <div id="organizationSideTab" class="tab-pane active">
                                <h4>Company Logo</h4>

                                <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 100%">
                                    <?php if($organizationData["logoPath"] != ""){ ?>
                                        <div class="fileinput-new thumbnail" style="width: 100%; max-height: 1000px;">
                                            <img src="<?php echo $organizationData["logoPath"]; ?>" alt="<?php echo $organizationData["organizationName"]; ?>'s Logo">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%; max-height: 150px;"></div>
                                        <button class="btn btn-warning pull-right remove-org-logo" onclick="removeOrgLogo();">Remove Logo</button>
                                    <?php }else{ ?>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%; max-height: 150px;"></div>
                                    <?php }
                                    $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",2]));
                                    if($checkAuths == true){
                                    ?>
                                    <div>
                                        <form action="<?php echo $_SERVER['LOCAL_NL_URL']."/console/actions/system/organization/uploadLogo.php"; ?>" method="post" enctype="multipart/form-data">

                                        <button class="btn btn-default fileinput-exists" type="submit">Save</button>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Change Image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="fileToUpload">
                                        </span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash-o"></i></a>
                                        </form>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php
                            foreach ($usersOfMyCompany as $user){
                                ?>
                                <div id="contact-<?= $user["id"] ?>" class="tab-pane">
                                    <div class="row m-b-lg">
                                        <div class="col-lg-4 text-center">
                                            <h2><?= $user['fullName'] ?></h2>

                                            <div class="m-b-sm">
                                                <?php if($user['profilePicture']){ ?>
                                                    <img alt="image" class="img-circle" src="<?php try{ echo $user['profilePicture'];}catch (Exception $e){echo $_SERVER['LOCAL_NL_URL']."/console/img/icons/user.png";} ?>" style="width: 62px">
                                                <?php }else{ ?>
                                                    <img alt="image" class="img-circle" src="<?= $_SERVER['LOCAL_NL_URL']."/console/img/icons/user.png" ?>" style="width: 62px">
                                                <?php } ?>
                                            </div>
                                            <?php if($user["isAdmin"] == "1"){ ?>
                                                <span class="label label-default">Admin</span>
                                            <?php } ?>
                                        </div>
                                        <div class="col-lg-8">
                                            <strong>
                                                Comments
                                            </strong>
                                            <p>
                                                <textarea style="height: 103px;" class="form-control" id="user_comment_<?= $user['id']?>"><?= $user['comment'] ?></textarea>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="client-detail" style="height: 100%;">


                                        <div class="clients-list" style="margin-top: 34px;">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#userDetailsTab-<?= $user["id"] ?>"> Information</a></li>
                                                <li class=""><a data-toggle="tab" href="#userAuthorizationsTab-<?= $user["id"] ?>"> Authorizations</a></li>
                                                <li class=""><a data-toggle="tab" href="#userLoginsTab-<?= $user["id"] ?>"> Log-ins</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="userDetailsTab-<?= $user["id"] ?>" class="tab-pane active">

                                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">

                                                            <br>
                                                            <ul class="list-group clear-list" style="margin-top: 3px;">
                                                                <li class="list-group-item fist-item">
                                                                    <input class="form-control" type="text" id="user_fullName_<?= $user['id']?>" value="<?= $user['fullName']?>" placeholder="Full Name" />
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <input class="form-control" type="text" id="user_email_<?= $user['id']?>" value="<?= $user['email']?>" placeholder="Email" disabled />
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="input-group">
                                                                                <span class="input-group-btn"><div id="countryCodeState_<?php echo $user["id"]; ?>" class="countryCodeState" data-input-name="country" data-selected-country="<?php echo $user["phoneCountryCodeState"]; ?>" style="display: inline-table;vertical-align: top;"></div></span>
                                                                                <input class="form-control" type="text" id="user_phone_<?= $user['id']?>" value="<?= $user['phoneNumber']?>" placeholder="Phone Number" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <input class="form-control" type="text" id="user_jobTitle_<?= $user['id']?>" value="<?= $user['jobTitle']?>" placeholder="Job Title (Supervisor, Salseperson, manager..)" />
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <label>Department</label>
                                                                    <select class="form-control" id="user_department_<?= $user['id']?>" placeholder="Department">
                                                                        <option value="">Not Set</option>
                                                                        <?php
                                                                        foreach($departments as $department){
                                                                            ?>
                                                                            <option value="<?= $department["id"] ?>" <?php if($department["id"] == $user["depId"]){?>selected<?php } ?>><?php echo $department["title"]; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <label>User Type</label>
                                                                    <select class="form-control" onchange="checkUserType(<?= $user['id']?>)" id="user_userType_<?= $user['id']?>" placeholder="User Type">
                                                                        <option value="">Not Set</option>
                                                                        <?php
                                                                        foreach($getUserTypes as $type){
                                                                            ?>
                                                                            <option value="<?= $type["id"] ?>" <?php if($type["id"] == $user["userTypeId"]){?>selected<?php } ?>><?php echo $type["title"]; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </li>
                                                                <div id="salesmanBonus-<?= $user['id'] ?>">
                                                                    <?php if ($user["userTypeId"] == 3){ ?>
                                                                    <li class="list-group-item">
                                                                        <label>Sales Bonus Percentage</label>
                                                                        <input class="form-control" type="number" min="0" max="100" id="user_salesBonusPerc_<?= $user['id'] ?>" value="<?= $user['salesBonusPerc'] ?>">
                                                                    </li>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                                $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",2]));
                                                                if($checkAuths == true) {
                                                                ?>
                                                                <li class="list-group-item">
                                                                    <?php if($bouncer["isNotFreeUser"]){ ?>
                                                                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="updateUserData('<?= $user['id']?>')"><i class="fa fa-save"></i> Save Changes</button>
                                                                    <?php }else{ ?>
                                                                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="limitReached('package')"><i class="fa fa-save"></i> Save Changes</button>
                                                                    <?php } ?>
                                                                </li>
                                                                <?php } ?>
                                                            </ul>

                                                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 353.634px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>


                                                </div>
                                                <div id="userAuthorizationsTab-<?= $user["id"] ?>" class="tab-pane" style="padding-top: 19px;">
                                                    <?php if ($user['id'] != $organizationData['userIdCreated']){ ?>
                                                    <?php if($user["isAdmin"] == "0"){ ?>
                                                        <small>* With each promotion of access level, the user gains access to additional areas of the system, including all the access of previous levels.</small>
                                                        <form id="userAuthorizationsForm-<?= $user["id"] ?>" style="margin-top: 3px;">
                                                        <?php
                                                            $singleUserAuthorization = new userAuthorization($user["id"]);
                                                            $userAuthorizations = $singleUserAuthorization->getUserAuthorizations();

                                                            foreach($userAuthorizations as $authorization){
                                                        ?>
                                                                    <div class="input-group m-b">
                                                                        <span class="input-group-addon" style="min-width: 139px;text-align: left; padding: 4px 8px;"><i style="margin-right: 6px;background-color: #cacaca8a;padding: 5px;border-radius: 4px;cursor: pointer;" class="fa fa-question" data-toggle="popover-hover" data-placement="auto left" data-content="<?php echo $authorization["description"]; ?>" data-original-title="" title=""></i><?php echo $authorization["title"]; ?></span>
                                                                        <select class="form-control" name="<?php echo $authorization["name"]; ?>">

                                                                            <?php
                                                                                foreach($authorization["levels"] as $level=>$levelText){
                                                                            ?>
                                                                                    <option value="<?php echo $level; ?>" <?php if($level == $authorization["value"]){echo "selected";} ?>><?php echo $levelText; ?></option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                        <?php } ?>
                                                        </form>
                                                    <?php }else{
                                                        ?>
                                                        <div align="center">
                                                            <h2>USER IS AN ADMINISTRATOR</h2>
                                                            <small>Adminstrators have access to all features</small>
                                                        </div>
                                                    <?php } ?>

                                                    <?php
                                                    $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["users",2]));
                                                    if($checkAuths == true) {
                                                    ?>
                                                    <div style="text-align: right" align="right" class="mydivi">
                                                        <?php
                                                            if($user["isAdmin"]){
                                                                ?>
                                                                <?php if($bouncer["isNotFreeUser"]){ ?>
                                                                    <button type="button" class="btn btn-sm btn-info" onclick="toggleAdminPrivilages('<?= $user["id"] ?>',false)"> Remove Admin Privilages</button>
                                                                <?php }else{ ?>
                                                                    <button type="button" class="btn btn-sm btn-info" onclick="limitReached('package')"> Remove Admin Privilages</button>
                                                                <?php } ?>
                                                                <?php
                                                            }else{
                                                                ?>
                                                                <?php if($bouncer["isNotFreeUser"]){ ?>
                                                                    <button type="button" class="btn btn-sm btn-info" onclick="toggleAdminPrivilages('<?= $user["id"] ?>',true)"> Add Admin Privilages</button>
                                                                    <button type="button" class="btn btn-sm btn-primary" onclick="saveUserAuthorizations('<?= $user["id"] ?>')"> Save</button>
                                                                <?php }else{ ?>
                                                                    <button type="button" class="btn btn-sm btn-info" onclick="limitReached('package')"> Add Admin Privilages</button>
                                                                    <button type="button" class="btn btn-sm btn-primary" onclick="limitReached('package')"> Save</button>
                                                                <?php } ?>

                                                                <?php
                                                            }
                                                        ?>
                                                    </div>
                                                    <?php } ?>
                                                    <?php }else{ ?>
                                                        <div align="center">
                                                            <h2>USER IS AN ADMINISTRATOR</h2>
                                                            <small>Adminstrators have access to all features</small>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div id="userLoginsTab-<?= $user["id"] ?>" class="tab-pane" style="padding-top: 19px;">
                                                    <table class="table table-striped">
                                                        <tbody>
                                                        <?php
                                                            $userLogins = $organization->userLogins($user['id']);
                                                            if($userLogins){
                                                            foreach($userLogins as $login){
                                                        ?>
                                                            <tr>
                                                                <td><?= $login["loginTime"] ?></td>
                                                            </tr>
                                                        <?php }}else{?>
                                                            <tr>
                                                                <td>No Logins</td>
                                                            </tr>
                                                        <?php }?>
                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                            foreach ($departments as $dep){
                                ?>
                                <div id="department-<?= $dep["id"] ?>" class="tab-pane">
                                    <div class="client-detail" style="height: 100%;">
                                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                                                <ul class="list-group clear-list" style="margin-top: 3px;">
                                                    <li class="list-group-item fist-item">
                                                        <label>Department Description</label>
                                                        <textarea style="height: 103px;" class="form-control" id="department_description_<?= $dep['id']?>"><?= $dep['description'] ?></textarea>
                                                    </li>
                                                    <li class="list-group-item fist-item">
                                                        <label>Name</label>
                                                        <input class="form-control" type="text" id="department_title_<?= $dep['id']?>" value="<?= $dep['title']?>" placeholder="Department Name" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label>Address</label>
                                                        <input class="form-control" type="text" id="department_address_<?= $dep['id']?>" value="<?= $dep['address']?>" placeholder="Address" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label>City</label>
                                                        <input class="form-control" type="text" id="department_city_<?= $dep['id']?>" value="<?= $dep['city']?>" placeholder="City" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label>State</label>
                                                        <input class="form-control" type="text" id="department_state_<?= $dep['id']?>" value="<?= $dep['state']?>" placeholder="State" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label>Zip</label>
                                                        <input class="form-control" type="text" id="department_zip_<?= $dep['id']?>" value="<?= $dep['zip']?>" placeholder="Zip" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label>Phone</label>
                                                        <input class="form-control" type="text" id="department_phone_<?= $dep['id']?>" value="<?= $dep['phone']?>" placeholder="Phone" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <label>Owner Name</label>
                                                        <input class="form-control" type="text" id="department_owner_<?= $dep['id']?>" value="<?= $dep['owner']?>" placeholder="Owner Name" />
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="row">
                                                            <?php
                                                            $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["departments",4]));
                                                            if($checkAuths == true) {
                                                            ?>
                                                            <div class="col-md-6">
                                                                <?php if($bouncer["isNotFreeUser"]){ ?>
                                                                    <button type="button" class="btn btn-danger btn-sm btn-block" onclick="deleteDepartment('<?= $dep['id']?>')"><i class="fa fa-remove"></i> Delete Department</button>
                                                                <?php }else{ ?>
                                                                    <button type="button" class="btn btn-danger btn-sm btn-block" onclick="limitReached('package')"><i class="fa fa-remove"></i> Delete Department</button>
                                                                <?php } ?>
                                                            </div>
                                                            <?php } ?>
                                                            <?php
                                                            $checkAuths = $bouncer["userAuthorization"]->checkAuthorizations(array(["departments",2]));
                                                            if($checkAuths == true) {
                                                            ?>
                                                            <div class="col-md-6">
                                                                <?php if($bouncer["isNotFreeUser"]){ ?>
                                                                    <button type="button" class="btn btn-primary btn-sm btn-block" onclick="updateDepartmentData('<?= $dep['id']?>')"><i class="fa fa-save"></i> Save Changes</button>
                                                                <?php }else{ ?>
                                                                    <button type="button" class="btn btn-primary btn-sm btn-block" onclick="limitReached('package')"><i class="fa fa-save"></i> Save Changes</button>
                                                                <?php } ?>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </li>
                                                </ul>

                                            </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 353.634px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
<!--Jquery validate-->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/validate/jquery.validate.min.js"></script>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/management.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<script>
    $(function () {
        $('[data-toggle="popover-hover"]').popover({
            trigger: 'hover',
        })


        $('#organizationDataForm').validate({

        errorPlacement: function(error, element) {
                if (element.attr("name") == "organizationName" ) {
                    $('#errorOrganizationNameWrapper').hide();
                    $('#errorOrganizationNameWrapper').show();
                    error.insertAfter("#errorOrganizationName");
                } else if (element.attr("name") == "organizationNameKey" ) {
                    $('#errorOrganizationKeyWrapper').hide();
                    $('#errorOrganizationKeyWrapper').show();
                    error.insertAfter("#errorOrganizationKey");
                }
            },

            rules: {
                organizationName: {
                    required: true,
                },
                organizationNameKey: {
                    required: true,
                    remote: {
                        url: "<?php echo $_SERVER['LOCAL_NL_URL'];?>/console/actions/user/checkCompanyKeyExist.php",
                        type: "post"
                    }
                }
            },
            messages:{
                organizationNameKey:{
                    remote: "Company key already in use!"
                }
            },
            submitHandler: function (form) {

                saveOrganizationForm();
            }
        });

    });

    function inviteUserToOrg(){
        <?php
            if($canAddMoreUsers == true){
        ?>
            showCustomModal('categories/user/addNewUser.php');
        <?php }else{ ?>
            limitReached("user");
        <?php } ?>
    }

    var tab = "<?php echo $tab; ?>";
    <?php if($tab == "users" && isset($_GET['userSelected'])){ ?>
    $( document ).ready(function() {
        showUserTab('<?= $_GET['userSelected'] ?>');
    });
    <?php } ?>
    <?php if(isset($_GET['showUsersModal'])){ ?>
    $( document ).ready(function() {
        inviteUserToOrg();
    });
    <?php } ?>
    <?php if($tab == "departments" && isset($_GET['departmentSelected'])){ ?>
    $( document ).ready(function() {
        showDepartmentTab('<?= $_GET['departmentSelected'] ?>');
    });
    <?php } ?>
    <?php if($tab == "users"){?>
    document.getElementById('usersTopRight').style.display = '';
    <?php } ?>
    <?php if($tab == "departments"){?>
    document.getElementById('depTopRight').style.display = '';
    <?php } ?>


    <?php foreach ($usersOfMyCompany as $user){ ?>
        ccstate("<?php echo $user["id"]; ?>","<?php echo $user["phoneCountryCodeState"]; ?>");
    <?php } ?>



    <?php
    if (count($usersOfMyCompany) > 0){
    foreach ($usersOfMyCompany as $user){ ?>
    $(document).ready(function() {

        $('#fromDate-<?= $user["id"] ?> span').html(moment().format('MMMM D, YYYY') + ' - ' + moment().add(1,"weeks").format('MMMM D, YYYY'));
        $('#fromDate-<?= $user["id"] ?>').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(),
            endDate: moment().add(1,"weeks"),
            minDate: moment(),
            maxDate: '12/31/2020',
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Go',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {

            $('#fromDate-<?= $user["id"] ?> span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            theStartDate[<?= $user["id"] ?>] = start.format('MMMM D, YYYY');
            theEndDate[<?= $user["id"] ?>] = end.format('MMMM D, YYYY');

        });

            theStartDate[<?= $user["id"] ?>] = "Today";
            theEndDate[<?= $user["id"] ?>] = "+1 week";
    });
    <?php }}?>

    function showCompanyKeyInfo(e){
        e.preventDefault();

        var content = document.createElement("div");

        var span = document.createElement("span");
        span.setAttribute("style","font-size: 15px;");
        span.innerHTML = 'This will be used in the Job Board and the custom company URL<br><br>Job board: <a href="https://jobboard.network-leads.com/<?php echo $organizationData["organizationNameKey"]; ?>" target="_blank">https://jobboard.network-leads.com/<?php echo $organizationData["organizationNameKey"]; ?></a><br>Custom login: <a href="https://www.network-leads.com/<?php echo $organizationData["organizationNameKey"]; ?>" target="_blank">https://www.network-leads.com/<?php echo $organizationData["organizationNameKey"]; ?></a>';

        content.appendChild(span);

        swal({
            content: content,
            buttons: {
                skip:{
                    text:"Got It"
                }
            },
            className: "swal-organization-key-info"
        });
    }
</script>
<?php
//if($bouncer["userSettingsData"]["firstTutorial"] == "1" && $bouncer['isUserAnAdmin'] == true) {
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/admin/adminTutorial_stage2.php");
//}else if ($bouncer["userSettingsData"]['firstTutorial'] == "1" && $bouncer['isUserAnAdmin'] == false){
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/user/userTutorial_stage2.php");
//}

if(!isset($_GET['tab'])){
   ?>
<script>
    changedTab("main");
</script>
<?php
}
?>
</body>
</html>

