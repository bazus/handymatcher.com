<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organizationTwilio.php");

$organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
$userPhoneNumber = $organizationTwilio->unPrettifyPhone($bouncer["userData"]["phoneCountryCode"].$bouncer["userData"]["phoneNumber"]);
?>
<!--<link href="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">-->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


<style>
    .passRulesList{
        margin-bottom: 0px;
        list-style: none;
        margin-left: -36px;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>

                <p>
                    A verification code will be sent to your phone number (<b><?php echo $userPhoneNumber; ?></b>) as a text message.
                </p>

                <div style="display: none;" id="enterCode">
                    <div class="form-group">
                        <label for="password">6 Digit Code</label>
                        <div class="input-group">
                            <input id="verificationCode" maxlength="6" type="text" class="form-control" placeholder="Enter Code">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" onclick="submitCode()">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="wrongCode" style="display: none;">
                    Wrong Verification Code.
                    <br><br>
                </div>
                <div align="center">
                    <button type="button" class="btn btn-white" data-dismiss="modal" onclick="backToProfile()">Back</button>
                    <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="sendCode()" id="sendCodeButton" >Send Code</button>
                    <button type="button" class="btn btn-primary" disabled id="countDownButton" style="display: none;">Send Code</button>
                </div>

                <div id="error" style="display: none;">
                    <br>
                    An error occurred.<br>
                    Please wait a full 3 minutes before trying again.<br>
                    You may contact us at <a href="mailto:support@network-leads.com">support@network-leads.com</a>.
                </div>

            </div>
        </div>
    </div>
</div>

<!--<script src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>-->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>

    function backToProfile(){
        showCustomModal('categories/user/profile.php');
    }

    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    function sendCode(){
        l.ladda( 'start' );


        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/sendVerificationCodeToPhone.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda('stop');

            try{
                data = JSON.parse(data);
                if(data == true){
                    document.getElementById('error').style.display = 'none';
                    document.getElementById('enterCode').style.display = '';
                    startCountDown();
                }else{
                    document.getElementById('error').style.display = '';
                }
            }catch (e){
                // Error
                document.getElementById('error').style.display = '';
            }


        });
    };

    function submitCode() {
        var verificationCode = document.getElementById('verificationCode').value;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/checkVerificationCodeToPhone.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            data:{
                code:verificationCode
            },
            method: "POST",
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{
                data = JSON.parse(data);
                if(data == true){
                    $('#myCustomModal').modal('hide');
                    swal({title:"Success!", text:"Your phone has been verified.",icon: "success",
                        buttons: {
                            ok:"Ok"
                        }
                    }).then((isConfirm)=>{
                        if (isConfirm) {
                            showCustomModal('categories/user/profile.php');
                        }
                    });

                }else{
                    document.getElementById('wrongCode').style.display = '';
                }
            }catch (e){
                // Error
                document.getElementById('wrongCode').style.display = '';
            }


        });
    }


    var minutes = 2;
    var seconds = 59;
    function startCountDown() {
        minutes = 2;
        seconds = 59;
        document.getElementById('sendCodeButton').style.display = 'none';
        document.getElementById('countDownButton').style.display = '';
        countDown();
    }
    function countDown(){
        if(minutes != 0 && seconds == 0){
            minutes--;
            seconds = 59;
        }
        if(seconds != 0){
            seconds--;
        }
        if(minutes == 0 && seconds == 0){
            document.getElementById('countDownButton').style.display = 'none';
            document.getElementById('sendCodeButton').style.display = '';
        }else{
            secondsText = seconds;
            if(seconds < 10){
                secondsText = "0"+seconds;
            }
            document.getElementById('countDownButton').innerHTML = "Resend code in "+minutes+":"+secondsText;
            setTimeout("countDown()",1000);
        }
    }

</script>