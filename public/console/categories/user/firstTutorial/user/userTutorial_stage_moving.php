<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>
<script>
    $("#movingMenu").parent().parent().css('min-height','2500px');

    var options = {
        "showSkip" : false,
        "scrollAnimationSpeed" : 500
    }

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something
            location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/leads.php?step=moving");
        }
    });

    <?php if($bouncer["organizationData"]["organizationPackage"] == "3" || $bouncer["organizationData"]["organizationPackage"] == "2"){ ?>
    //simple config.
    var enjoyhint_script_steps = [
        {
            'click .menu-full:first-child' : 'Click on the "Trucks" option to open its panel',
            "showSkip" : false
        },
        {
            'next #newTruck' : 'You can add new trucks, set its purchase date, milage at purchase etc',
            "showSkip" : false
        },
        {
            'next #trucksNames' : 'Here you can find your trucks. By clicking on its row in the table you can update, remove and mark the specific truck as sold',
            "showSkip" : false
        },
        {
            'click .menu-full:nth-child(2)' : 'Click on the "Storage" option to open its panel',
            "showSkip" : false
        },
        {
            'next #newStorage' : 'You can add new storage options, set their prices, tax, address, contact name and more',
            "showSkip" : false
        },
        {
            'next #storagesNames' : 'Here you can find your available storage options. By clicking on its row in the table you can update and remove a specific storage option',
            "showSkip" : false
        },
        {
            'click .menu-full:nth-child(3)' : 'Click on the "Inventory" option to open its panel',
            "showSkip" : false
        },
        {
            'next #addGroupBtn' : 'You can add new item groups, so you can keep your list organized by name and type',
            "showSkip" : false
        },
        {
            'next #groupSearch' : 'You can search the groups by their names',
            "showSkip" : false
        },
        {
            'click #gorup-1' : 'As you go, you get some groups to use by default, lets try the "Appliances" group',
            "showSkip" : false
        },
        {
            'next tbody#inventoryItemsTbody' : 'These are all the items under the "Appliances" group ordered by its "Item Name" to its "Cubic Feet". This group is not editable, if needed you can just copy the necessary items to your custom group',
            "showSkip" : false,
            "timeout" : 200,
        },
        {
            'click .menu-full:nth-child(4)' : 'Click on the "Materials" option to open its panel',
            "showSkip" : false
        },
        {
            'click .newMaterial' : 'Click here to insert a new material line',
            "showSkip" : false
        },
        {
            'next #materialsNamesTableBody' : 'Set the material name and costs ordered in the table',
            "showSkip" : false
        },
        {
            'next #saveBtn' : 'Save the changes made in the page (deleting materials are automatically saved)',
            "showSkip" : false
        },
        {
            'next #performanceTab' : 'Here you can find some information about your the performance of your salesman, leads handeled and jobs booked. you also can find more information about your lead providers in the advertising performance',
            "showSkip" : false
        },
        {
            'click .menu-full:nth-child(6)' : 'Click on the "Settings" option to open its panel',
            "showSkip" : false
        },
        {
            'next .tab-pane>fieldset' : 'Here you can set some important settings about the way the software will work, to allow duplicate leads based on phone or email, send a specific template to every incoming lead from your mail account so the lead can reply, and more',
            "showSkip" : false
        },
    ];

    <?php }else{ ?>

    var enjoyhint_script_steps = [
        {
            'next .menu-full:first-child' : 'You can add new trucks, set its purchase date, milage at purchase etc  * This feature is not available on your plan',
            "showSkip" : false
        },
        {
            'next .menu-full:nth-child(2)' : 'You can add new storage options, set their prices, tax, address, contact name and more  * This feature is not available on your plan',
            "showSkip" : false
        },
        {
            'next .menu-full:nth-child(3)' : 'You can add new item groups, so you can keep your list organized by name and type  * This feature is not available on your plan',
            "showSkip" : false
        },
        {
            'next .menu-full:nth-child(4)' : 'You can set the material name and costs ordered in the table  * This feature is not available on your plan',
            "showSkip" : false
        },
        {
            'next #performanceTab' : 'Here you can find some information about your the performance of your salesman, leads handeled and jobs booked. you also can find more information about your lead providers in the advertising performance  * This feature is not available on your plan',
            "showSkip" : false
        },
        {
            'click .menu-full:nth-child(6)' : 'Click on the "Settings" option to open its panel',
            "showSkip" : false
        },
        {
            'next .tab-pane>fieldset' : 'Here you can set some important settings about the way the software will work, to allow duplicate leads based on phone or email, send a specific template to every incoming lead from your mail account so the lead can reply, and more',
            "showSkip" : false
        },
    ];
    <?php }?>

    $(".enjoyhint_close_btn").click(function() {
        location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/leads.php?step=moving");
    });


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();
    // "nextButton": {text:"Continue"}
</script>