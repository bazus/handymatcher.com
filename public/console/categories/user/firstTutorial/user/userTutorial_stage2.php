<?php
$checkAuthsOrg = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",1],['departments',1],['users',1]));
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>

<script>
    var options = {
        "showSkip" : false
    }

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something

            swal({title: "You are all set to go", text: "You finished the quick tutorial!",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{
                turnOffTutorialMode();
            });

        }
    });

    //simple config.
    var enjoyhint_script_steps = [
        <?php if($bouncer["organizationData"]["organizationPackage"] >= 3 && $checkAuthsOrg){ ?>
        {
            'next #organization_tabs' : 'This is essentially your company\'s official information. You can update your organization\'s data, manage users and departments',
            "showSkip" : false
        },
        <?php }elseif($bouncer["organizationData"]["organizationPackage"] > 1){ ?>
        {
            'next #organization_tabs' : 'This is essentially your company\'s official information. You can update your organization\'s data and manage users',
            "showSkip" : false
        },
        <?php }else{ ?>
        {
            'next #organization_tabs' : 'This is essentially your company\'s official information. You can update your organization\'s data',
            "showSkip" : false
        },
        <?php } ?>
    ];

    var isCancelled = false;
    $(".enjoyhint_close_btn").click(function(e) {
        if (isCancelled){
            swal({title: "You are all set to go", text: "You finished the quick tutorial!",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{
                turnOffTutorialMode();
            });
        }else{
            isCancelled = true;
            quitTutorial();
        }
    });
    function quitTutorial(){
        swal({
            text:"Leaving so soon!? We highly recommend completing the full tutorial before beginning to use our software - We don't want you to miss out on any of our cool features.",
            buttons: {
                continue: {
                    text:"Continue Tutorial",
                    value:"continue"
                },
                skip:{
                    text:"Quit Tutorial",
                    value:"skip",
                    className:"btn-danger"
                }
            }
        }).then((val) => {
            if (val == "skip"){
                $(".enjoyhint_close_btn").click();
            }else{
                if (currentStep > 0){
                    for(var i = 0;i<currentStep;i++){
                        enjoyhint_script_steps.shift();
                    }
                }

                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();

                isCancelled = false;
            }
        });
    }

    function turnOffTutorialMode(){

        var strUrl = BASE_URL+'/console/actions/user/turnOffFirstTutorial.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {},
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            window.location = BASE_URL+'/console/categories/leads/leads.php';
        });
    }


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();

</script>