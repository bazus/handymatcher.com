<?php
if (isset($_GET['step'])){$step = $_GET['step'];}else{$step = "";}
$isUserAManager = $bouncer["userAuthorization"]->isUserTypeMatch(array(2));
$checkAuths_mailcenter = $bouncer["userAuthorization"]->checkAuthorizations(array(["mailcenter",1]));
$checkAuthsOrg = $bouncer["userAuthorization"]->checkAuthorizations(array(["organization",1],['departments',1],['users',1]));
$isPackageValid_chat = $bouncer["isNotFreeUser"];
if ($checkAuths_calendar = $bouncer["userAuthorization"]->checkAuthorizations(array(["calendar",1]))){
    $checkAuthsCalendar = true;
}else{
    $checkAuthsCalendar = false;
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>

<style>
    .enjoy_hint_label{
        z-index: 20000;
        font-size: 20px;
    }
    .skipButton:hover{
        color:white;
        background-color: #65ACCE !important;
    }
    .learnMore{
        pointer-events: all;
        display:block;
        position: absolute;
        box-sizing: content-box;
        border: 2px solid #007bff;
        -webkit-border-radius: 40px;
        border-radius: 40px;
        color: rgba(255,255,255,1);
        background: #007bff;
        -webkit-transition: background-color .3s cubic-bezier(0,0,0,0),color .3s cubic-bezier(0,0,0,0),width .3s cubic-bezier(0,0,0,0),border-width .3s cubic-bezier(0,0,0,0),border-color .3s cubic-bezier(0,0,0,0);
        -moz-transition: background-color .3s cubic-bezier(0,0,0,0),color .3s cubic-bezier(0,0,0,0),width .3s cubic-bezier(0,0,0,0),border-width .3s cubic-bezier(0,0,0,0),border-color .3s cubic-bezier(0,0,0,0);
        -o-transition: background-color .3s cubic-bezier(0,0,0,0),color .3s cubic-bezier(0,0,0,0),width .3s cubic-bezier(0,0,0,0),border-width .3s cubic-bezier(0,0,0,0),border-color .3s cubic-bezier(0,0,0,0);
        transition: background-color .3s cubic-bezier(0,0,0,0),color .3s cubic-bezier(0,0,0,0),width .3s cubic-bezier(0,0,0,0),border-width .3s cubic-bezier(0,0,0,0),border-color .3s cubic-bezier(0,0,0,0);
        margin-left: 10%;
        margin-top: 1.5%;
        width: 100px;
        height: 40px;
        font: normal normal normal 17px/40px "Advent Pro",Helvetica,sans-serif;
        cursor: pointer;
        text-align: center;
        letter-spacing: 1px;
    }
    .learnMoreLabel{
        font-size: 0.8em;
        background: rgba(11,62,111,0.5);
        color: #fff;
        padding: 5px;
        border-radius: 5px;
        letter-spacing: 1px;
        cursor: pointer;
        pointer-events: all;
    }
    .endTutorial{
        color:black;
    }

</style>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script>
    if ($('body').hasClass('mini-navbar')){
        $('body').removeClass('mini-navbar');
    }
    var options = {
        "showSkip" : true
    };

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onSkip: function () {
            swal({title: "You are all set to go", text: "You finished the quick tutorial!",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{
                turnOffTutorialMode();
            });
        }
    });
    var leadLink = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/leads.php";
    var movingLink = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/moving/home.php";
    var emailLink = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/mailSettings.php";
    var currentStep = 0;
    <?php if($bouncer["organizationData"]["organizationPackage"] == "1"){ ?>
    //simple config.
    var enjoyhint_script_steps = [
        <?php
        if ($step == "moving") {

    }else{?>
        {
            'next .profile-element' : 'You can access your profile data and settings here and upload a profile picture',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 0;
            }
        },
        {
            'next #side_leads' : 'This is the lead page where you can see and manage all of your leads, insert leads manually and manage your lead providers',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 1;
            }
        },
        {
            'next #reportrange' : 'You can choose the date range that you wish to see the list of leads from. By default it is set to the current day',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 2;
            }
        },
        {
            'next a[href$="leadProviders.php"]' : 'You can manage your lead providers, add, edit and remove providers',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 3;
            }
        },
        {
            'next a.btn.btn-success.btn-sm' : 'If you\'ve got a potential customer on the line, you can insert that lead manually and manage it like any other lead',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 4;
            }
        },
        {
            'click button#theBTN' : 'Filter the leads table by clicking here',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 5;
            }
        },
        {
            'next div#filterBTN' : 'These are the available filters to use, you can choose multiply filters to fit your needs',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 6;
            }
        },
        {
            'next tbody#leadsTableHead' : 'Here you can find all of the incoming leads that\'s being assigned to you',
            "showSkip" : false,
            onBeforeStart: function(){
                currentStep = 7;
                $('.backBTN').click();
                var x = 0;
                if ($('#leadsTableHead tr')){
                    $("#leadsTableHead tr").each(function(){
                        if (x > 0){
                            $(this).remove();
                        }else{
                            x ++;
                        }
                    });
                }
            }
        },
        <?php if ($isUserAManager){ ?>
        {
            'next #side_moving' : 'Here you can access more management features (i.e. trucks, inventory, materials, storage etc)<br><span class="learnMoreLabel" onclick="location.replace(movingLink)">Click on the highlighted left menu "Moving" link to learn more</span>',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 8;
            }
        },<?php }} ?>
        {
            'next #side_tools' : 'These are your working tools, such as your <b>file manager</b>, which is private for each user. <?php if ($checkAuthsCalendar){ ?>There is also a <b>calendar</b> where you can manage your organization\'s operations and manage/create events<?php } ?>',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 9;
            }
        },
        {
            'next .topSiteNavBar' : 'Use the search bar to find anything you need from the software - I mean anything!',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 10;
                $(".enjoyhint_close_btn").hide();
            }
        },
        {
            'click #rightside_toolbar_open' : 'Click Here to open the side bar',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 11;
            }
        },
        <?php if ($checkAuthsCalendar){ ?>
        {
            'next .sidebar-container' : 'You can view your next events in your calendar here',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 12;
                $(".enjoyhint_close_btn").show();
            }
        },
        <?php } ?>
        {
            'next #rightside_toolbar_chat' : 'In the chat tab you can see users in your organization and chat with them privately',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 13;
                $(".enjoyhint_close_btn").show();
            }
        },
        <?php if ($checkAuthsOrg){ ?>
        {
            'click #side_organization' : 'Click here to manage your organization',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 14;
            }
        }
        <?php }else{ ?>
        {
            'next body' : '<span class="endTutorial">That\'s it you are ready to go</span>',
            'showSkip' : false,
            'nextButton':{text:'Finish'},
            onBeforeStart:function(){
                turnOffTutorialMode();
                $("#rightside_toolbar_open a").click();
            }

        }
        <?php } ?>
    ];
    <?php } ?>

    <?php if($bouncer["organizationData"]["organizationPackage"] == "3" || $bouncer["organizationData"]["organizationPackage"] == "2"){
        ?>
    //simple config.
    var enjoyhint_script_steps = [
        <?php
        if ($step == "email") {

        }else{
        if ($step == "moving") {

        }else{?>
        {
            'next .profile-element' : 'You can access your profile data and settings here and upload a profile picture',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 0;
            }
        },
        {
            'next #side_leads' : 'This is the lead page where you can see and manage all of your leads, insert leads manually and manage your lead providers',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 1;
            }
        },
        {
            'next #reportrange' : 'You can choose the date range that you wish to see the list of leads from. By default it is set to the current day',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 2;
            }
        },
        {
            'next a[href$="leadProviders.php"]' : 'You can manage your lead providers, add, edit and remove providers',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 3;
            }
        },
        {
            'next a.btn.btn-success.btn-sm' : 'If you\'ve got a potential customer on the line, you can insert that lead manually and manage it like any other lead',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 4;
            }
        },
        {
            'click button#theBTN' : 'Filter the leads table by clicking here',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 5;
            }
        },
        {
            'next div#filterBTN' : 'These are the available filters to use, you can choose multiply filters to fit your needs',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 6;
            }
        },
        {
            'next tbody#leadsTableHead' : 'Here you can find all of the incoming leads that\'s being assigned to you',
            "showSkip" : false,
            onBeforeStart: function(){
                currentStep = 7;
                $('.backBTN').click();
                var x = 0;
                if ($('#leadsTableHead tr')){
                    $("#leadsTableHead tr").each(function(){
                        if (x > 0){
                            $(this).remove();
                        }else{
                            x ++;
                        }
                    });
                }
            }
        },
        <?php if ($isUserAManager){ ?>
        {
            'next #side_moving' : 'Here you can access more management features (i.e. trucks, inventory, materials, storage etc)<br><span class="learnMoreLabel" onclick="location.replace(movingLink)">Click on the highlighted left menu "Moving" link to learn more</span>',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 8;
            }
        },<?php }} ?>
        <?php if ($checkAuths_mailcenter){ ?>
        {
            'next #side_mail' : 'Here you can manage your email accounts, create and manage marketing templates, see your outgoing emails and compose new emails<br><button class="learnMore" onclick="location.replace(emailLink)">Learn More</a>',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 9;
            }
        },<?php }} ?>
        {
            'next #side_tools' : 'These are your working tools, such as your <b>file manager</b>, which is private for each user. <?php if ($checkAuthsCalendar){ ?>There is also a <b>calendar</b> where you can manage your organization\'s operations and manage/create events\',<?php } ?>',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 10;
            }
        },
        {
            'next .topSiteNavBar' : 'Use the search bar to find anything you need from the software - I mean anything!',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 11;
                $(".enjoyhint_close_btn").hide();
            }
        },
        {
            'click #rightside_toolbar_open' : 'Click Here to open the side bar',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 12;
            }
        },
        <?php if ($checkAuthsCalendar){ ?>
        {
            'next .sidebar-container' : 'You can view your next events in your calendar here',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 13;
                $(".enjoyhint_close_btn").show();
            }
        },<?php } ?>
        {
            'next #rightside_toolbar_chat' : 'In the chat tab you can see users in your organization and chat with them privately',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 14;
                $(".enjoyhint_close_btn").show();
            }
        },
        <?php if ($checkAuthsOrg){ ?>
        {
            'click #side_organization' : 'Click here to manage your organization',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 15;
                $(".enjoyhint_close_btn").show();
            }
        }
        <?php }else{ ?>
        {
            'next body' : '<span class="endTutorial">That\'s it you are ready to go</span>',
            "showSkip" : false,
            'nextButton':{text:'Finish'},
            onBeforeStart:function(){
                currentStep = 16;
                turnOffTutorialMode();
                $("#rightside_toolbar_open a").click();
            }

        }
        <?php } ?>
    ];
    <?php } ?>

    var isCancelled = false;
    $(".enjoyhint_close_btn").click(function(e) {
        if (isCancelled){
            swal({title: "You are all set to go", text: "You finished the quick tutorial!",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{
                turnOffTutorialMode();
            });
        }else{
            isCancelled = true;
            quitTutorial();
        }
    });
    function quitTutorial(){
        swal({
            text:"Leaving so soon!? We highly recommend completing the full tutorial before beginning to use our software - We don't want you to miss out on any of our cool features.",
            buttons: {
                continue: {
                    text:"Continue Tutorial",
                    value:"continue"
                },
                skip:{
                    text:"Quit Tutorial",
                    value:"skip",
                    className:"btn-danger"
                }
            }
        }).then((val) => {
            if (val == "skip"){
                $(".enjoyhint_close_btn").click();
            }else{
                if (currentStep > 0){
                    for(var i = 0;i<currentStep;i++){
                        enjoyhint_script_steps.shift();
                    }
                }

                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();

                isCancelled = false;
            }
        });
    }

    function turnOffTutorialMode(){

        var strUrl = BASE_URL+'/console/actions/user/turnOffFirstTutorial.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {},
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

        });
    }
    <?php if (!$step){ ?>
    var content = document.createElement("div");
    content.setAttribute("style","text-align: left;font-size: 13px");
    content.innerHTML = "In the few steps before you, we will go through the basics.<br>At any point feel free to contact us for help or visit our <a href=\"http://help.network-leads.com\" target=\"_blank\" title=\"Network Leads Help Center\">Support Page</a>";

    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    swal({
        title: "Welcome!",
        content: content,
        buttons: {
            skip:{
                text:"Skip",
                className:"btn-default skipButton",
                value:"skip",
            },
            confirm:{
                text:"Start Tutorial",
                value:"start",
            },

        }
    }).then((value) => {
        if(value == "skip"){
            turnOffTutorialMode();
        }else{
            startTutorial();
        }
    });
    <?php }else{ ?>
    enjoyhint_instance.set(enjoyhint_script_steps);
    startTutorial();
    <?php } ?>


    function startTutorial(){
        //run Enjoyhint script
        enjoyhint_instance.run();
    }

</script>