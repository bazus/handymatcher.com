<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>
<script>
    if ($('body').hasClass('mini-navbar')){
        $('body').removeClass('mini-navbar');
    }
    var options = {
        "showSkip" : false
    }

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something
            location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/leads.php?step=email");
        }
    });

    //simple config.
    var enjoyhint_script_steps = [
        {
            'next #composeNewEmail' : 'Here you can send emails using the system from your own email account',
            "showSkip" : false
        },
        {
            'next .table-mail' : 'Here you can see all of the outgoing emails and you can see the email content by click its row',
            "showSkip" : false,
            onBeforeStart: function(){
                var x = 0;
                if ($('#emailsContainer tr')){
                    $("#emailsContainer tr").each(function(){
                        if (x > 0){
                            $(this).remove();
                        }else{
                            x ++;
                        }
                    });
                }
            }
        },
    ];

    $(".enjoyhint_close_btn").click(function() {
        location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/leads.php?step=email");
    });


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();

</script>