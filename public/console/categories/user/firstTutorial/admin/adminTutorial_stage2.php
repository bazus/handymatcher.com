<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>
<style>
    .btn-danger {
        background-color: #ed5565 !important;
        border-color: #ed5565 !important;
        color: #FFFFFF !important;
    }
    .btn-danger:hover{
        background-color: #ec4758 !important;
        border-color: #ec4758 !important;
        color: #FFFFFF !important;
    }
</style>
<script>
    var options = {
        "showSkip" : false
    };

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something

            swal({title: "You are all set to go", text: "You finished the quick tutorial!",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{
                turnOffTutorialMode();
            });

        }
    });
    $("#organization_billing").attr("disabled","disabled");
    $("#organization_billing").attr("href","#");
    //simple config.
    var currentStep = 0;
    var enjoyhint_script_steps = [
        <?php if($bouncer["organizationData"]["organizationPackage"] == 3){ ?>
        {
            'next #organization_tabs' : 'This is essentially your company\'s official information. You can update your organization\'s data, manage users and departments',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 0;
            }
        },
        <?php }elseif($bouncer["organizationData"]["organizationPackage"] > 1){ ?>
        {
            'next #organization_tabs' : 'This is essentially your company\'s official information. You can update your organization\'s data and manage users',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 0;
            }
        },
        <?php }else{ ?>
        {
            'next #organization_tabs' : 'This is essentially your company\'s official information. You can update your organization\'s data',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 0;
            }
        },
        <?php } ?>
        {
            'next #organization_billing' : 'You can always manage and update your package and billing information too',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 1;
            }
        },
        {
            'next #organization_rightside_ibox' : 'You can upload your company logo here',
            "showSkip" : false,
            onBeforeStart:function () {
                currentStep = 2;
            }
        }
    ];
    var isCancelled = false;
    $(".enjoyhint_close_btn").click(function(e) {
        if (isCancelled){
            swal({title: "You are all set to go", text: "You finished the quick tutorial!",icon: "success",
                buttons: {
                    ok:"Ok"
                },
            }).then((isConfirm)=>{
                turnOffTutorialMode();
            });
        }else{
            isCancelled = true;
            quitTutorial();
        }
    });
    function quitTutorial(){
        swal({
            text:"Leaving so soon!? We highly recommend completing the full tutorial before beginning to use our software - We don't want you to miss out on any of our cool features.",
            buttons: {
                continue: {
                    text:"Continue Tutorial",
                    value:"continue"
                },
                skip:{
                    text:"Quit Tutorial",
                    value:"skip",
                    className:"btn-danger"
                }
            }
        }).then((val) => {
            if (val == "skip"){
                $(".enjoyhint_close_btn").click();
            }else{
                if (currentStep > 0){
                    for(var i = 0;i<currentStep;i++){
                        enjoyhint_script_steps.shift();
                    }
                }

                enjoyhint_instance.set(enjoyhint_script_steps);
                enjoyhint_instance.run();

                isCancelled = false;
            }
        });
    }
    function turnOffTutorialMode(){

        var strUrl = BASE_URL+'/console/actions/user/turnOffFirstTutorial.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {},
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            window.location = BASE_URL+'/console/';
        });
    }


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();

</script>