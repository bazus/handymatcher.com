<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>
<script>
    if ($('body').hasClass('mini-navbar')){
        $('body').removeClass('mini-navbar');
    }
    var options = {
        "showSkip" : false
    }

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something
            location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/templates.php");
        }
    });

    //simple config.
    var enjoyhint_script_steps = [
        {
            'next #addAccount' : 'Here you can add a new email account, all information is encrypted and stored securely, we will not share any of that information and its only for your own use',
            "showSkip" : false
        },
        {
            'next .accountsTable' : 'Manage your organization email accounts, set email accounts as private and don\'t forget to disable in on vacations :)',
            "showSkip" : false
        },
    ];

    $(".enjoyhint_close_btn").click(function() {
        location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/index.php?step=email");
    });


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();

</script>