<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>
<script>
    if ($('body').hasClass('mini-navbar')){
        $('body').removeClass('mini-navbar');
    }
    var options = {
        "showSkip" : false
    }

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something
            location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/outgoing.php");
        }
    });

    //simple config.
    var enjoyhint_script_steps = [
        {
            'next #preDefinedTemplatesBody' : 'Here you can add, choose pre defined templates or create a new one with a blank page to fit your needs best. These templates can be used in number of ways. Inside the template editor you can find few placeholders to use from the lead\'s first name to the lead\' "to" ZIP code',
            "showSkip" : false
        },
    ];

    $(".enjoyhint_close_btn").click(function() {
        location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/index.php?step=email");
    });


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();

</script>