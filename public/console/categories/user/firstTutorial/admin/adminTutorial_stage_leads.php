<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/enjoyhint/enjoyhint.min.js"></script>
<script>
    var options = {
        "showSkip" : false
    }

    //initialize instance
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            //do something
            location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/index.php?step=leads");
        }
    });

    //simple config.
    var enjoyhint_script_steps = [
        {
            'next #reportrange' : 'You can choose the date range that you wish to see the list of leads from. By default it is set to the current day',
            "showSkip" : false
        },
        {
            'next a[href$="leadProviders.php"]' : 'You can manage your lead providers, add, edit and remove providers',
            "showSkip" : false
        },
        {
            'next a.btn.btn-success.btn-sm' : 'If you\'ve got a potential customer on the line, you can insert that lead manually and manage it like any other lead',
            "showSkip" : false
        },
        {
            'click button#theBTN' : 'Filter the leads table by clicking here',
            "showSkip" : false
        },
        {
            'next div#filterBTN' : 'These are the available filters to use, you can choose multiply filters to fit your needs',
            "showSkip" : false
        },
        {
            'next tbody#leadsTableHead' : 'Here you can find all of the incoming leads after <b>successfully</b> setting a lead provider and start receiving leads, you can see the lead status, priority and more information, you also can assign the lead to a specific salesman',
            "showSkip" : false,
            onBeforeStart: function(){
                $('.backBTN').click();
                var x = 0;
                if ($('#leadsTableHead tr')){
                    $("#leadsTableHead tr").each(function(){
                        if (x > 0){
                            $(this).remove();
                        }else{
                            x ++;
                        }
                    });
                }
            }
        },
    ];

    $(".enjoyhint_close_btn").click(function() {
        location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console/index.php?step=leads");
    });


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);
    enjoyhint_instance.run();

</script>