<?php if ($bouncer['isUserAnAdmin'] == true && !$GLOBALS['mobileDetect']["isMobile"]){ ?>
<input type="hidden" id="chatAlert">
<?php }?>

<?php
    if($_SERVER["ENVIRONMENT"] != "local"){
?>

<?php if (!$GLOBALS['mobileDetect']["isMobile"]){ ?>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/chat.min.js"></script>
<?php } ?>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/node_modules/socket.io-client/dist/socket.io.js"></script>
<script>
    var reminderSoundAlert = false;
    function initSoundsOnBodyClick() {
        reminderSoundAlert = new Audio('https://s3.amazonaws.com/networkleads/sounds/alert1.mp3');
    }
    document.body.addEventListener('click', initSoundsOnBodyClick, true);


    var socket = io("chat.thenetworkleads.com");

    socket.on('connect', function() {
        socket.emit('connectme', {myuserid:"<?php echo $bouncer["credentials"]["userId"]; ?>"});
    });


    socket.on('newSMS', function(data) {

        var singleLeadId = data.leadIds[data.leadIds.length-1];
    <?php
            if($URI[0] == "categories" && $URI[1] == "leads" && $URI[2] == "lead.php"){
            ?>
            if(singleLeadId == leadId){

                document.getElementById("sms-side-iframe").contentWindow.getConversation();

                if($('#sms-side').hasClass("open-sms-side") == true){
                    // if sms side is open - mark new msg as read
                    document.getElementById("sms-side-iframe").contentWindow.markRead();
                }else{
                    // if sms side is closed - show the red blinker
                    document.getElementById("sms-side-iframe").contentWindow.getTotalUnread();
                }
            }
            <?php }else{ ?>

            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 10000
            };
            toastr.options.onclick = function () {
                window.open(BASE_URL+"/console/categories/leads/lead.php?leadId="+singleLeadId,"_blank");
            };

            toastr.info('click to open', 'New SMS',{iconClass:"toast-newSMS"});
            <?php } ?>


    });

    socket.on('newLeadArrived', function(data){

        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.options.onclick = function () {
            window.open(BASE_URL+"/console/categories/leads/lead.php?leadId="+data.leadId,"_blank");
        };

        toastr.info('click to open', 'New Lead Arrived');

    });

    socket.on('newReminder', function(data){

        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.options.onclick = function () {
            window.open(BASE_URL+"/console/categories/leads/lead.php?leadId="+data.leadId,"_blank");
        };

        toastr.info(data.eventIn, data.title,{iconClass:"toast-newEvent"});

        if(reminderSoundAlert != false) {
            reminderSoundAlert.play();
        }
    });

    socket.on('leadlogupdated', function(data){
        getLeadsFollowups(data.logId,true,data.doNotAlert);
    });

    socket.on('message', function(data){
        socket.emit('setMSGasread', data.msg);

        var d = new Date();
        var nowTime = d.getHours() + ":" + d.getMinutes();

        var chatWithUserId = data.fromUserId;

        var doesExist = false;
        for(var i = 0;i<rooms.length;i++){
            if(rooms[i] == chatWithUserId){
                toggleChat(chatWithUserId,true);
                doesExist = true;
            }
        }
        if(doesExist == false){
            checkForRoomWithUser(chatWithUserId);
            var audio = new Audio('<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/sounds/plucky.mp3');
            audio.play();
        }else{
            addMSG(chatWithUserId,data.msg.mymsg, nowTime, "left");
        }
    });

    function checkForUnreadMsgs(){
        var strUrl = BASE_URL+'/console/actions/chat/checkForUnreadMsgs.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{
                data = JSON.parse(data);
                for(var i = 0;i<data.length;i++){
                    checkForRoomWithUser(data[i],true);
                }
            }catch (e){

            }

        });
    }

    checkForUnreadMsgs();
</script>
<?php if (!$GLOBALS['mobileDetect']["isMobile"]){ ?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/chat.css" rel="stylesheet">
    <div style="height: 327px;position: fixed;bottom: 0px;right: 0px;z-index:2" id="chatRooms">
</div>
<?php } ?>
<?php } ?>