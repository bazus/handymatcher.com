<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/classes/register.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/phoneVerification.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");

$register = new register();
$lastActivationRow = $register->getLastActivationRow($bouncer["credentials"]["userId"]);

$phoneVerification = new phoneVerification($bouncer["credentials"]["userId"]);
$isPhoneVerified = $phoneVerification->isPhoneVerified($bouncer["userData"]["phoneNumber"]);

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"]);
$doesHaveActiveMailAccount = $mailaccounts->doesHaveActiveAccount();

$profilePicture = "";
if(isset($bouncer['userData']['profilePicture']) && $bouncer['userData']['profilePicture'] !== null && $bouncer['userData']['profilePicture'] !== "") {
    $profilePicture = $bouncer['userData']['profilePicture'];
}

$birthDay = false;
$birthMonth = false;
$birthYear = false;

if ($bouncer["userData"]["birthDate"]) {
    $birthDay = date("d", strtotime($bouncer["userData"]["birthDate"]));
    $birthMonth = date("m", strtotime($bouncer["userData"]["birthDate"]));
    $birthYear = date("Y", strtotime($bouncer["userData"]["birthDate"]));
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/intl-tel-input/css/intlTelInput.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/cropper/cropper.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/profileModal.min.css" rel="stylesheet">
<style>
    .alert-custom{
        color: #3e3e3e;
        background-color: #8080800d;
        border-color: #80808054;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog" style="width: 709px;padding: 0px;">

        <div class="modal-content animated fadeIn">

            <div style="float: left;height: 570px;width: 102px;background-color: #00b494;display: inline-table;">
                <ul class="nav metismenu" id="side-menu">

                    <li id="profileTabButton" class="active">
                        <a onclick="changeProfileTab('profileTab')"> <span class="nav-label">Profile</span></a>
                    </li>
                    <li id="profileSettingsTabButton">
                        <a onclick="changeProfileTab('profileSettingsTab')"> <span class="nav-label">Settings</span></a>
                    </li>

                </ul>

            </div>

            <div id="profileTab" style="display: inline-table; height: 570px;width: 594px;background-color: #f8fafb;">
                <div class="modal-body">
                    <div id="changeImage">
                        <?php if($bouncer['userData']['profilePicture'] && getimagesize($profilePicture)){ ?>
                            <img onclick="showChangeProfilePic()" alt="image" id="profilePicture" class="img-circle circle-border m-b-md" src="<?= $profilePicture ?>" />
                        <?php }else{ ?>
                            <img onclick="showChangeProfilePic()" alt="image" id="profilePicture" class="img-circle circle-border m-b-md" src="<?= $_SERVER['LOCAL_NL_URL']; ?>/console/img/icons/user.png" />
                        <?php } ?>
                        <div class="form-group">
                            <div class="half">
                                <label for="fullName">Full Name</label>
                                <input id="fullName" type="text" class="form-control" placeholder="Your Full Name" value="<?php echo $bouncer["userData"]["fullName"]; ?>">
                            </div>
                            <div class="half">
                                <label for="birthDay" id="birthDayLabel">Date Of Birth</label>
                                <select class="form-control birthday" id="birthMonth">
                                    <option value="">Month</option>
                                    <?php for ($i=1;$i<=12;$i++){ ?>
                                        <option value="<?= $i ?>" <?php if ($birthMonth == $i){echo "selected";} ?> ><?= $i ?></option>
                                    <?php } ?>
                                </select>
                                <select class="form-control birthday" id="birthDay">
                                    <option value="">Day</option>
                                    <?php for ($i=1;$i<=31;$i++){ ?>
                                        <option value="<?= $i ?>" <?php if ($birthDay == $i){echo "selected";} ?> ><?= $i ?></option>
                                    <?php } ?>
                                </select>
                                <select class="form-control birthday" id="birthYear">
                                    <option value="">Year</option>
                                    <?php for ($i=1900;$i<=date("Y",strtotime("NOW"));$i++){ ?>
                                        <option value="<?= $i ?>" <?php if ($birthYear == $i){echo "selected";} ?> ><?= $i ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <input id="email" type="email" class="form-control" placeholder="Email" value="<?php echo $bouncer["userData"]["email"]; ?>" disabled>
                                <span class="input-group-btn"> <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="changePassword()">Change Password</button> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="organizationName">Organization Name <i id="popoverme" class="fa fa-info-circle" style="cursor: pointer;"></i></label>
                            <input id="organizationName" type="text" class="form-control" placeholder="Organization Name" value="<?php echo $bouncer["userData"]["organizationName"]; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="countryCodeState">Phone Number</label><br>
                            <!--                        <div id="countryCodeState" data-input-name="country" data-selected-country="--><?php //echo $bouncer["userData"]["phoneCountryCodeState"]; ?><!--" style="display: inline-table;width: 70px;vertical-align: top;"></div>-->
                            <input type=hidden name="country" id="countryCodeState" value="<?php echo $bouncer["userData"]["phoneCountryCodeState"]; ?>">
                            <div class="input-group" style="display: inline-table;">
                                <input id="phoneNumber" type="text" class="form-control" placeholder="Phone Number" value="<?php echo $bouncer["userData"]["phoneNumber"]; ?>" style="height: 38px;">
                                <?php if($isPhoneVerified){ ?>
                                    <span class="input-group-addon" style="color: #1ab394;"> Verified</span>
                                <?php }else{ ?>
                                    <span style="cursor:pointer;" class="input-group-addon" data-dismiss="modal" onclick="verifyPhone()"> Verify Phone</span>
                                <?php } ?>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div id="changeProfilePicture" style="display: none;">
                        <div id="body-overlay"><div><img src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/images/loading/loading.gif" width="64px" height="64px"/></div></div>
                        <div class="bgColor center-block">
                            <form id="uploadForm" action="upload.php" method="post">
                                <div id="targetOuter">
                                    <div id="targetLayer"></div>
<!--                                    <img src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/images/loading/photo.png"  class="icon-choose-image" />-->
                                    <span class="icon-choose-text">Click Here</span>
                                    <div class="icon-choose-image" >
                                        <input name="file" id="userImage" type="file" class="inputFile" onChange="showPreview(this);" />
                                    </div>
                                </div>
                                <div>
                                    <input type="submit" value="Crop & Upload Photo" class="btnSubmit" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="bottom: 0px;position: fixed;right: 1px;width: 600px;" id="profileFooter">
                    <div id="closeOrSaveChanges-section">
                        <small class="text-navy" style="float:  left;margin-top: 9px;display: none;" id="updatesSuccess">Your information has been updated.</small>
                        <small class="text-danger" style="float:  left;margin-top: 9px;display: none;" id="updatesError">Changes not saved. Please try again later.</small>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="saveChanges()">Save changes</button>
                    </div>
                    <div id="back-section" style="display: none">
                        <button type="button" class="btn btn-white" onclick="showProfile()">Back</button>
                    </div>
                </div>
            </div>
            <div id="profileSettingsTab" style="height: 570px;width: 594px;background-color: #f8fafb; display: none;">
                <div class="modal-body">

                    <?php if($bouncer["isUserAnAdmin"] == true){ ?>

                    <div class="alert alert-custom">
                        <?php if(!$isPhoneVerified){ ?>
                            To receive sms when a new lead arrives, please <a data-dismiss="modal" onclick="verifyPhone()">verify your phone</a> first.
                        <?php } ?>
                        <div class="checkbox checkbox-success" style="margin-bottom: 0px;">
                            <input id="smsnewlead" name="smsnewlead" <?php if(!$isPhoneVerified){ ?> disabled <?php } ?> type="checkbox" <?php if($bouncer["userSettingsData"]["receiveSMSWhenNewLeadArrives"] == "1"){ ?> checked <?php } ?>>
                            <label for="smsnewlead">
                                I would like to receive <span style="font-weight: bold">SMS</span> when a new lead arrives.
                                <?php if($isPhoneVerified){ ?>
                                    <br>
                                    Verified phone number: <i><span id="countryCodeNumber"></span> <?= $bouncer["userData"]["phoneNumber"]; ?></i>
                                <?php } ?>
                            </label>
                        </div>
                    </div>

                    <div class="alert alert-custom">
                        <div class="checkbox checkbox-success" style="margin-bottom: 0px;margin-top: 6px;">
                            <input id="emailnewlead" name="emailnewlead" placeholder="Email Address" onchange="toggleEmailInput(this)" type="checkbox" <?php if($bouncer["userSettingsData"]["receiveEMAILWhenNewLeadArrives"] == "1"){ ?> checked <?php } ?>>
                            <label for="emailnewlead">
                                I would like to receive an <b>Email</b> when a new lead arrives.
                            </label>
                            <input type="email" class="form-control" name="emailnewlead_email" id="emailnewlead_email"<?php if($bouncer["userSettingsData"]["receiveEMAILWhenNewLeadArrives"] != "1"){ ?> disabled <?php } ?> value="<?php echo $bouncer["userSettingsData"]["EMAILWhenNewLeadArrives"]; ?>" />
                        </div>
                    </div>
                    <?php } ?>


                    <div class="alert alert-custom">
                        <div class="checkbox checkbox-success" style="margin-bottom: 0px;margin-top: 6px;">
                            <input id="enableTutorial" name="enableTutorial" placeholder="Enable Tutorial" type="checkbox" <?php if ($bouncer["userSettingsData"]['helpMode'] == 1){echo "checked";} ?>>
                            <label for="enableTutorial">
                                Enable Help Mode
                            </label>
                        </div>
                        <div class="checkbox checkbox-success" style="margin-bottom: 0px;margin-top: 6px;">
                            <input id="showGoogleMaps" name="enableTutorial" placeholder="Lead Google Maps" type="checkbox" <?php if ($bouncer["userSettingsData"]['showMapsInLeads'] == 1){echo "checked";} ?>>
                            <label for="showGoogleMaps">
                                Display map in lead page
                            </label>
                        </div>
                        <?php if($bouncer['isUserAnAdmin']){ ?>
                            <div class="checkbox checkbox-success" style="margin-bottom: 0px;margin-top: 6px;">
                                <input id="weeklyEmail" name="weeklyEmail" type="checkbox" <?php if ($bouncer["userSettingsData"]['receiveWeeklyEmail'] == 1){echo "checked";} ?>>
                                <label for="weeklyEmail">
                                    Receive Weekly Report By Email
                                </label>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="alert alert-custom">
                        <label for="localTimeZone">
                            Local Timezone
                        </label>
                        <select class="form-control" id="localTimeZone" name="localTimeZone">
                            <option value="">Select Timezone</option>
                            <option value="America/Los_Angeles" <?php if ($bouncer["userSettingsData"]['localTimeZone'] == "America/Los_Angeles"){echo "selected";} ?>>Pacific Standard Time</option>
                            <option value="America/Denver" <?php if ($bouncer["userSettingsData"]['localTimeZone'] == "America/Denver"){echo "selected";} ?>>Mountain Standard Time</option>
                            <option value="America/Chicago" <?php if ($bouncer["userSettingsData"]['localTimeZone'] == "America/Chicago"){echo "selected";} ?>>Central Standard Time</option>
                            <option value="America/New_York" <?php if ($bouncer["userSettingsData"]['localTimeZone'] == "America/New_York"){echo "selected";} ?>>Eastern Standard Time</option>
                        </select>
                    </div>
<!--                    <hr>-->
<!--                    <div class="form-group" style="margin-bottom: 0px;margin-top: 6px;">-->
<!--                        <input id="enableFirstTutorial" name="enableFirstTutorial" type="button" class="btn btn-xs btn-primary" onclick="toggleFirstTutorial()" value="Restart First Tutorial" --><?php //if($bouncer["userSettingsData"]["firstTutorial"] == "1"){ ?><!-- disabled="disabled" --><?php //} ?><!--
                    </div>-->
                </div>
                <div class="modal-footer" style="bottom: 0px;position: fixed;right: 1px;width: 600px;">
                    <small class="text-navy" style="float:  left;margin-top: 9px;display: none;" id="settingsUpdatesSuccess">Your settings has been updated.</small>
                    <small class="text-danger" style="float:  left;margin-top: 9px;display: none;" id="settingsUpdatesError">Changes not saved. Please try again later.</small>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="saveSettingsChanges()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/intl-tel-input/js/intlTelInput.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/intl-tel-input/js/intlTelInput-jquery.min.js"></script>
<!--<script src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/js/jquery.flagstrap.min.js"></script>-->
<!-- Cropper -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/cropper/cropper.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/cropper/jquery-cropper.min.js"></script>

<script>
    $("#popoverme").popover({
        container: "#myCustomModal",
        html: true,
        content: "You can manage your organization in the 'My Organization' Panel",
        trigger:"hover",
        placement: 'auto right'
    });
    var countryCodeState = '<?php echo $bouncer["userData"]["phoneCountryCodeState"]; ?>';
    var input = $("#phoneNumber").intlTelInput({
        allowDropdown: true,
        separateDialCode: true,
        formatOnDisplay: true,
        preferredCountries: ["us"],
        initialCountry: '<?php echo $bouncer["userData"]["phoneCountryCodeState"]; ?>'
    });
    input.on("countrychange", function() {
        var flag = $(".iti-flag").attr("class").split(/\s+/);
        document.getElementById("countryCodeState").value = flag[1].toUpperCase();
        countryCodeState = flag[1].toUpperCase();
    });

    function changeProfileTab(tabId){
        document.getElementById('profileTab').style.display = 'none';
        document.getElementById('profileSettingsTab').style.display = 'none';
        document.getElementById('profileTabButton').className = '';
        document.getElementById('profileSettingsTabButton').className = '';

        document.getElementById(tabId+'Button').className = 'active';
        document.getElementById(tabId).style.display = 'inline-table';
    }

    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
        $("#countryCodeNumber").text($(".selected-dial-code").text());
    });

    function toggleEmailInput(obj){
        if(obj.checked){
            document.getElementById('emailnewlead_email').disabled = false;
        }else{
            document.getElementById('emailnewlead_email').disabled = true;
        }
    }

    function saveSettingsChanges() {

        var localTimeZone = document.getElementById("localTimeZone").value;
        var helpMode = document.getElementById("enableTutorial").checked;
        if (helpMode === true){
            helpMode = 1;
        }else{
            helpMode = 0;
        }
        var leadGoogleMaps = document.getElementById("showGoogleMaps").checked;
        if (leadGoogleMaps === true){
            leadGoogleMaps = 1;
        }else{
            leadGoogleMaps = 0;
        }

        var weeklyEmail = 0;
        if(document.getElementById("weeklyEmail")){
            weeklyEmail = document.getElementById("weeklyEmail").checked;
            if (weeklyEmail === true){
                weeklyEmail = 1;
            }else{
                weeklyEmail = 0;
            }
        }

        <?php if($bouncer["isUserAnAdmin"] == true){ ?>
            var smsnewlead = document.getElementById('smsnewlead').checked;
            var emailnewlead = document.getElementById('emailnewlead').checked;
            var emailnewlead_email = document.getElementById('emailnewlead_email').value;

            theData = {
                smsnewlead:smsnewlead,
                emailnewlead:emailnewlead,
                emailnewlead_email:emailnewlead_email,
                helpMode:helpMode,
                leadGoogleMaps: leadGoogleMaps,
                weeklyEmail: weeklyEmail,
                localTimeZone:localTimeZone,
            };
        <?php }else{ ?>
        theData = {
            helpMode:helpMode,
            leadGoogleMaps: leadGoogleMaps,
            localTimeZone:localTimeZone
        };
        <?php } ?>

        l.ladda( 'start' );

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/updateProfileSettings.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:theData,
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            data = JSON.parse(data);
            if(data){
                document.getElementById('settingsUpdatesSuccess').style.display = '';
            }else{
                document.getElementById('settingsUpdatesError').style.display = '';
            }

            setTimeout(function(){
                document.getElementById('settingsUpdatesSuccess').style.display = 'none';
                document.getElementById('settingsUpdatesError').style.display = 'none';
            }, 3000);

            l.ladda('stop');

        });

    }

        function saveChanges(){
        l.ladda( 'start' );

        var fullName = document.getElementById('fullName').value;
        var phoneNumber = document.getElementById('phoneNumber').value;
        var day = document.getElementById('birthDay').value;
        var month = document.getElementById('birthMonth').value;
        var year = document.getElementById('birthYear').value;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/updateProfileData.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                fullName:fullName,
                countryCodeState:countryCodeState,
                phoneNumber:phoneNumber,
                day:day,
                month:month,
                year:year
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            data = JSON.parse(data);
            if(data){
                document.getElementById('updatesSuccess').style.display = '';
            }else{
                document.getElementById('updatesError').style.display = '';
            }

            setTimeout(function(){
                document.getElementById('updatesSuccess').style.display = 'none';
                document.getElementById('updatesError').style.display = 'none';
            }, 3000);

            l.ladda('stop');

        });
    };

    function changePassword(){
        showCustomModal('categories/user/changePassword.php');
    }

    function verifyPhone(){
        saveChanges();
        showCustomModal('categories/user/verifyPhone.php');
    }
    // Profile Picture

    var $image;
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });
    function showPreview(objFileInput) {

        if (objFileInput.files[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                $("#targetLayer").html('<img src="'+e.target.result+'" width="200px" height="200px" style="max-width:100%;" class="upload-preview" />');
                $("#targetLayer").css('opacity','0.8');
                $(".icon-choose-image").css('display','none');
                $(".icon-choose-text").css('display','none');
            };
            fileReader.readAsDataURL(objFileInput.files[0]);
        }
        loader();
    }
    function loader(){
        setTimeout(function(){
            $image = $("#targetLayer img");
            $image.cropper({
                aspectRatio: 1 / 1,
                crop: function(event) {

                }
            });
        },200);
    }
    $(document).ready(function (e) {
        $("#uploadForm").on('submit',(function(e) {
            e.preventDefault();
            try {
                var cropper = $image.data('cropper');
                cropper.getCroppedCanvas().toBlob((blob) => {

                    const formData = new FormData();
                    formData.append('file', blob);

                    $.ajax({
                        url: "<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/changeProfilePicture.php",
                        type: "POST",
                        data: formData,
                        beforeSend: function () {
                            $("#body-overlay").show();
                        },
                        contentType: false,
                        processData: false,
                    }).done(function (data) {
                        try {
                            data = JSON.parse(data);
                            if (data.success == true) {
                                toastr.success("Profile Picture Updated","Updated");
                                $("#profilePicture").attr("src", data.destination);
                                $("#execModal").click();
                            } else {
                                for (var i = 0; i < data.error.length; i++) {
                                    toastr.error(data.error[i]);
                                }
                                $("#execModal").click();
                            }
                        } catch (e) {
                            toastr.error("Error Uploading Your Picture");
                            $("#execModal").click();
                        }
                    });
                });
            }catch (e) {
                $("#userImage").click();
            }
        }));
    });
    function showChangeProfilePic(){
        $('#changeProfilePicture').show();
        $('#changeImage').hide();
        $("#closeOrSaveChanges-section").hide();
        $("#back-section").show();
        $('#userImage').click();
    }

    function showProfile(){
        $('#changeProfilePicture').hide();
        $('#changeImage').show();
        $("#closeOrSaveChanges-section").show();
        $("#back-section").hide();
    }

    function toggleFirstTutorial(){

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/updateFirstTutorial.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                enable:1
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            data = JSON.parse(data);
            if (data == true){
                location.replace("<?= $_SERVER['LOCAL_NL_URL'] ?>/console");
            }else{
                toastr.error("Temporary Disabled");
            }
        });

    }
</script>