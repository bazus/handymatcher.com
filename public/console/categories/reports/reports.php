<?php
$pagePermissions = array(true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");


?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads - Reports</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/leads.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/leads.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.css" rel="stylesheet">


    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>

    <style>
        .pace {
            -webkit-pointer-events: none;
            pointer-events: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
            position: fixed;
            height: 140px;
            width: 140px;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        .pace-inactive {
            display: none;
        }

        .pace .pace-activity {
            display: block;
            position: fixed;
            z-index: 2000;
            width: 140px;
            height: 140px;
            border: solid 2px transparent;
            border-top-color: #29d;
            border-left-color: #29d;
            border-radius: 70px;
            -webkit-animation: pace-spinner 400ms linear infinite;
            -moz-animation: pace-spinner 400ms linear infinite;
            -ms-animation: pace-spinner 400ms linear infinite;
            -o-animation: pace-spinner 400ms linear infinite;
            animation: pace-spinner 400ms linear infinite;
        }

        @-webkit-keyframes pace-spinner {
            0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
        }
        @-moz-keyframes pace-spinner {
            0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
            100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
        }
        @-o-keyframes pace-spinner {
            0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
            100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
        }
        @-ms-keyframes pace-spinner {
            0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
            100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
        }
        @keyframes pace-spinner {
            0% { transform: rotate(0deg); transform: rotate(0deg); }
            100% { transform: rotate(360deg); transform: rotate(360deg); }
        }
        #tabsWrapper{
            padding: 0;
        }
        #navTab{
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        #iframeWrapper{
        }
        #iframeHolder{
            height: 100%;
        }

        iframe{
            width: 100%;
        }
        #navTab > li.active > a {
            color: #1ab394;
            background-color: white;
            border: none;
        }
        #navTab > li.active {
            border: none;
        }


         .list-group{
             display: flex;
             width: 100%;
             justify-content: space-evenly;
         }
        .list-group .list-group-item{
            width: 100%;
            background-color: #fff;
            cursor: pointer;
            padding: 0px !important;
            text-align: center;
        }
        .list-group .list-group-item:first-child{
            margin-bottom: 0;
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
            border-top-left-radius: 4px !important;
            border-bottom-left-radius: 4px !important;
            border-right: none !important;
        }
        .list-group .list-group-item:last-child{
            margin-bottom: 0;
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
            border-top-right-radius: 4px !important;
            border-bottom-right-radius: 4px !important;
            border-left: none !important;
        }
        .list-group .list-group-item:hover{
            color: #555;
            text-decoration: none;
        }
        .list-group .list-group-item a{
            padding: 9px !important;
        }
        .list-group .list-group-item a:focus,.list-group .list-group-item a:hover{
            background-color: unset !important;
        }
        .list-group .list-group-item.active{
            background-color: #1ab394;
            border: 1px solid #e7eaec !important;
            color: #FFFFFF;
            z-index: 2;
        }

    </style>

</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
        <div id="wrapper">
            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li  class="active">
                        <strong>Reports</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div>


            <ul class="list-group nav" style="display: flex;">
                <li class="list-group-item active">
                    <a data-toggle="tab"  onclick="reportsController.setIframeTabSrc('financial')" class="nav-link nav-item active financial-tab">Financials</a>
                </li>
                <li class="list-group-item">
                    <a data-toggle="tab" onclick="reportsController.setIframeTabSrc('sales')" class="nav-link sales-tab nav-item">Sales</a>
                </li>
            </ul>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div id="dateRangeReports" class="form-control"
                     style="float: left;margin-bottom: 10px; max-width: fit-content;">
                    <i class="fa fa-calendar"></i>
                    <span></span> <b class="caret"></b>
                </div>

                <div id="iframeWrapper"  style="margin-bottom: 10px; margin-right: 6px;">
                    <div id="iframeHolder">

                    </div>
                </div>

            </div>

        </div>
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
</div>
<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/moment.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.min.js"></script>


<script>
    var currentTab = "financial";
    var theStartDate;
    var theEndDate;
    var theLabel = "This Month";

    $(function(){
        Pace.on("hide",  function(pace){
            document.getElementById("iframeHolder").style.visibility = "";
        });
        reportsController.setIframeTabSrc("financial");

            $('#dateRangeReports span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#dateRangeReports').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().startOf('month'),
                endDate: moment(),
                minDate: '01/01/2018',
                maxDate: '12/31/2020',
                dateLimit: { days: 365 },
                showDropdowns: true,
                showWeekNumbers: false,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last Week': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Go',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                theLabel = label;
                $(".spanner").each(function(){
                    if (label == "Custom"){
                        $(this).text("Custom Date");
                    } else{
                        $(this).text(label);
                    }
                });
                if (label != "Custom") {
                    $('#dateRangeReports span').html(label);
                }else{
                    $('#dateRangeReports span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                theStartDate = start.format('DD-MM-YYYY');
                theEndDate = end.format('DD-MM-YYYY');

                reportsController.initIframe();
            });
            theStartDate = moment().startOf('month').format('DD-MM-YYYY');
            theEndDate = moment().format('DD-MM-YYYY');

        reportsController.initIframe();

    });

    var reportsController = {
            setIframeTabSrc: function (tabNameSelected) {
                if(!tabNameSelected){currentTab = "financial";}
                currentTab = tabNameSelected;

                var iframeHolder =  document.getElementById("iframeHolder");
                iframeHolder.innerHTML = "";
                var iframe = document.createElement("iframe");

                iframe.setAttribute("frameBorder","0");
                iframe.setAttribute("id","iframe");
                iframe.setAttribute("name","stats-iframe");
                iframe.setAttribute("src","<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/iframes/reports/" + currentTab + ".php");
                iframe.onload = function() {
                    reportsController.initIframe();
                };

                iframeHolder.appendChild(iframe);
            },
            iframeHeight: function () {
                var iFrameID = document.getElementById('iframe');
                if(iFrameID) {
                    iFrameID.height = "";
                    iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
                }

            },
            initIframe:function () {
                if (typeof window.frames['stats-iframe'].init === "function") {
                    // safe to use the function
                    window.frames['stats-iframe'].init();
                    reportsController.iframeHeight();
                }
            }
        }
    function showCustomerPaymentsInfo(){
        swal({
            text: 'Payments are calculated by the user that added the payment, not by the user that handles the lead.',
            buttons: {
                skip:{
                    text:"Got It",
                }
            }
        });
    }

</script>

</body>
</html>

