<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/services/htmlpurifier/library/HTMLPurifier.auto.php");

// Set the HTML purify, disable cache (it will cause performance hit)
/*
$HTMLpurifyconfig = HTMLPurifier_Config::createDefault();
$HTMLpurifyconfig->set('Core.DefinitionCache', null);
$purifier = new HTMLPurifier($HTMLpurifyconfig);
*/

$pagePermissions = array(false,[1],true,true,[2]);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingSettings.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

$movingSettingsData = [];
$movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
if($movingSettings->checkAndSetMovingSettings()){
    $movingSettingsData = $movingSettings->getData(true);
}
$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$organizationMailAccounts = $mailaccounts->getMyAccounts(true,true);

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$preEmailsData = $preEmails->getData(NULL,true);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Network Leads - Moving Section</title>

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/iCheck/custom.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Angular_Seed_Project/js/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/summernote/summernote.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">


        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/movingHome.css" rel="stylesheet">

        <script>
            var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
        </script>

        <link rel="stylesheet" href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/css/moving.css">
        <link rel="stylesheet" href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/mobile/moving.css">
        <style>
            .swal-tariff{
                width: 300px;
            }
            .swal-tariff .swal-content {
                padding: 0 1rem;
                margin-top: 1rem;
            }
            .swal-tariff  .swal-footer{
                padding: 1rem;
            }
            .customSwitch .onoffswitch{
                width: 100%;
                height: 33px;
            }
            .customSwitch .onoffswitch-label{
                height: 33px;
            }
            .customSwitch .onoffswitch-inner:before{
                height: 33px;
                text-align: center;
                font-size: 19px;
                line-height: 30px;
                content: "Activated";
            }
            .customSwitch .onoffswitch-switch{
                right: 94%;
            }
            .customSwitch .onoffswitch-inner:after{
                height: 33px;
                text-align: center;
                font-size: 19px;
                line-height: 30px;
                content: "Disabled";
            }

            .crewTable td{
                vertical-align: middle !important;
            }
            #movingMenu .menu-item{
                white-space: nowrap;
                text-overflow: ellipsis;
                overflow: hidden;
            }
            .swal-crew{
                margin: 0;
                height: 700px;
            }
            .swal-crew .swal-content{
                margin: 0;
                padding: 0;
            }

            .swal-trucks{
                margin: 0;
                height: 760px;
            }
            .swal-trucks .swal-content{
                margin: 0;
                padding: 0;
            }
            .swal-carrier{
                margin: 0;
                height: 800px;
            }
            .swal-carrier .swal-content{
                margin: 0;
                padding: 0;
            }
            .swal-update-carrier{
                margin: 0;
                height: 800px;
            }
            .swal-update-carrier .swal-content{
                margin: 0;
                padding: 0;
            }
            .swal-group{
                margin: 0;
                height: 255px;
            }
            .swal-group .swal-content{
                margin: 0;
                padding: 0;
            }
            .swal-item{
                margin: 0;
                height: 340px;
            }
            .swal-item .swal-content{
                margin: 0;
                padding: 0;
            }
            .swal-storage{
                margin: 0;
                height: 660px;
            }
            .swal-storage .swal-content{
                margin: 0;
                padding: 0;



            .ui-widget-content{
                border: 1px solid #969696 !important;
            }


        </style>
    </head>

    <body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
        <div id="wrapper">
            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

                <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
                    <div class="col-lg-10">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                            </li>
                            <li class="active">
                                <strong>Moving</strong>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="ibox" id="movingMenu">
                            <div class="ibox-content" style="">
                                <div class="menu-full">
                                    <div id="btn-trucks" class="btn btn-large" onclick="chooseTab('trucks');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -10px -10px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Trucks</span><span class="menu-sub">Co-ordinate Truck activity.</span></div>
                                </div>
                                <div class="menu-full">
                                    <div id="btn-crew" class="btn btn-large" onclick="chooseTab('crew');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -110px -10px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Laborers/Foreman/Drivers</span><span class="menu-sub">Add and manage team members to<br>add and notify about Jobs and updates.</span></div>
                                </div>
                                <div class="menu-full">
                                    <div id="btn-carriers" class="btn btn-large" onclick="chooseTab('carriers');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -10px -100px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Carriers</span><span class="menu-sub">Add and manage your carriers.</span></div>
                                </div>
                                <div class="menu-full">
                                    <div id="btn-inventory" class="btn btn-large" onclick="chooseTab('inventory');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -110px -100px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Inventory</span><span class="menu-sub">Update and customize inventory options.</span></div>
                                </div>
                                <div class="menu-full">
                                    <div id="btn-storage" class="btn btn-large" onclick="chooseTab('storage');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -210px -10px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Storage Facilities</span><span class="menu-sub">Manage Storage space, availability<br> and more.</span></div>
                                </div>
                                <div class="menu-full">
                                    <div id="btn-materials" class="btn btn-large" onclick="chooseTab('materials');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -210px -100px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Materials</span><span class="menu-sub">What materials does your company<br> currently offer and use?</span></div>
                                </div>

                                <div class="menu-full">
                                    <div id="btn-fullvalueprotection" class="btn btn-large" onclick="chooseTab('fullvalueprotection');"><span style="background: url('<?= $_SERVER['CDN'] ?>/images/icons/moving_fvp.png');height: 70px;width: 80px;float: left;"></span><span class="menu-item">Full Value Protection</span><span class="menu-sub" style="white-space: break-spaces !important;">Setup your company's deductible amounts for move protection</span></div>
                                </div>

                                <div class="menu-full">
                                    <div id="btn-movingSettings" class="btn btn-large" onclick="chooseTab('movingSettings');"><span style="background: url('<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/icons/moving_sprites.png') -10px -190px;height: 70px;width: 80px;float: left;"></span><span class="menu-item">Settings</span><span class="menu-sub">Move Management Settings</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div class="ibox">
                            <div class="ibox-content secondBox">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <main id="mainContent">
                                    <div id="trucks" style="display:none;">
                                        <div id="inventoryTopRight" style="padding-bottom: 20px">
                                            <h2 style="float: left;">Trucks</h2>
                                            <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="addNewTruck()" id="newTruck"> Add New Truck</button>
                                        </div>
                                        <div class="inventory-group-list" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th>Truck Name</th>
                                                    <th>Active</th>
                                                </tr>
                                                </thead>
                                                <tbody id="trucksNames">

                                                </tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        <div style="padding-bottom: 20px">
                                            <div class="col-sm-6" style="padding-left: 0;"><h2 id="truckDetail" style="display: none;float: left;">Truck Details <span id="titleSpan"></span></h2></div><div class="col-sm-6" id="truckRightButtons"></div>
                                        </div>
                                        <div class="inventory-group-list" style="margin-top: 38px;">
                                            <ul class="list-group clear-list" style="margin-top: 3px;">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12" id="trucksLeftDetails">
                                                    </div>
                                                    <div class="col-md-6 col-sm-12" id="trucksRightDetails">
                                                    </div>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="carriers" style="display:none;">
                                        <div id="inventoryTopRight" style="padding-bottom: 20px">
                                            <h2 style="float: left;">Carriers</h2>
                                            <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="addNewCarrier()"> Add New Carrier</button>
                                        </div>
                                        <div class="inventory-group-list" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th>Carrier Name</th>
                                                    <th style="width: 212px;"></th>
                                                </tr>
                                                </thead>
                                                <tbody id="carriersTable">

                                                </tbody>
                                            </table>
                                            <hr>

                                            <div class="form-group" style="border-top: 1px dotted #c2c2c2;margin-top: 60px;">
                                                <label style="text-align: left;margin-top: 15px;padding: 0px;" class="col-sm-8 control-label">Job Acceptance Terms and Agreements <?php if ($bouncer["userSettingsData"]['helpMode'] == 1){?><span onclick="showJobAcceptanceInfo(event)"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span> <?php } ?></label>
                                                <div id="summernote2"><?= $movingSettingsData['jobAcceptanceTerms'] ?></div>
                                            </div>
                                            <div class="row buttonsSettings">
                                                <div class="col-md-9"></div>
                                                <div class="col-md-3">
                                                    <button onclick="saveJobAcceptanceTerms()" class="btn btn-block btn-success">Save Changes</button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div id="crew" style="display:none;">
                                        <div id="inventoryTopRight" style="padding-bottom: 20px">
                                            <h2 style="float: left;">Laborers/Foreman/Drivers</h2>
                                            <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="addNewCrewMember()"> Add New Crew Member</button>
                                        </div>
                                        <div class="inventory-group-list" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th>Crew Member Name</th>
                                                    <th>Member Type</th>
                                                    <th style="width: 212px;">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="crewTable" class="crewTable">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="storage" style="display: none;">
                                        <div id="inventoryTopRight" style="padding-bottom: 20px">
                                            <h2 style="float: left;">Storages</h2>
                                            <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="addNewStorage()" id="newStorage"> Add New Storage</button>
                                        </div>
                                        <div class="inventory-group-list" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th>Facility Name</th>
                                                    <th>Active</th>
                                                </tr>
                                                </thead>
                                                <tbody id="storagesNames">
                                                    <tr id="emptystorage">
                                                        <td colspan="5" style="background-color: #e7eaec36;text-align: center;">No Storage Yet</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        <div id="storageDetails">
                                            <div style="padding-bottom: 20px">
                                                <div class="col-sm-7" style="padding-left: 0;"><h2 style="float: left;">Storage Details <span id="titleSpanStorage"></span></h2></div><div class="col-sm-5" id="storageRightButtons"></div>
                                            </div>
                                            <div class="inventory-group-list" style="margin-top: 38px;">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12" id="divTopStorageLeft"><ul class="list-group clear-list" style="margin-top: 3px;" id="StorageLeftDetails"></ul></div>
                                                    <div class="col-md-6 col-sm-12" id="divTopStorageRight"><ul class="list-group clear-list" style="margin-top: 3px;" id="StorageRightDetails"></ul></div>
                                                </div>
                                                <div class="row">
                                                    <hr>
                                                    <h3 class="text-center" style="display: none;">Price & Details</h3>
                                                    <div class="col-md-6 col-sm-12" id="divBottomStorageLeft"><ul class="list-group clear-list" style="margin-top: 3px;" id="storageBottomLeftDetails"></ul></div>
                                                    <div class="col-md-6 col-sm-12" id="divBottomStorageRight"><ul class="list-group clear-list" style="margin-top: 3px;" id="storageBottomRightDetails"></ul></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="inventory" style="display: none;">
                                        <div id="inventoryTopRight" style="padding-bottom: 20px">
                                            <h2 style="float: left;">Inventory Groups</h2>
                                            <button type="button" class="btn btn-sm btn-primary" id="addGroupBtn" onclick="addNewGroup()"> Add New Group</button>
                                        </div>
                                        <div class="row" style="margin-top: 32px;">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="groupSearch" id="groupSearch" placeholder="Search In Groups">
                                                    <span class="input-group-btn">
                                                        <button onclick="clearGroupSearch()" type="button" class="btn btn-default">Clear</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="inventory-group-list" style="margin-top: 32px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <th style="width: 72%">Group Name</th>
                                                <th>Delete</th>
                                                </thead>
                                                <tbody id="inventoryGroups">

                                                </tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        <h2 id="h2Span" style="display: none;">Group Items <span id="titleSpan"></span></h2>
                                        <div id="buttonsContainer" style="padding-bottom: 20px">
                                            <button type="button" class="btn btn-sm btn-primary" id="itemBTN" style="display:none;float: right;" onclick="addNewItem(groupId)"> Add New Item</button>
                                            <button type="button" class="btn btn-sm btn-success" id="itemSaveBTN" style="display: none;float: right; margin-right: 10px;" disabled="true" onclick="updateItems()">Save</button>
                                        </div>
                                        <div class="inventory-group-list">
                                            <div class="input-group" id="itemsSearch" style="display: none;margin-bottom: 20px;">
                                                <input class="form-control" type="text" name="itemSearch" id="itemSearch" placeholder="Search In Items">
                                                <span class="input-group-btn">
                                                    <button onclick="clearItemSearch()" type="button" class="btn btn-default">Clear</button>
                                                </span>
                                            </div>
                                            <div id="inventoryItems">

                                            </div>
                                        </div>
                                    </div>
                                    <div id="materials" style="display: none;">
                                        <div id="materialsTopRight" style="padding-bottom: 20px">
                                            <h2 style="float: left;">Materials</h2>
                                            <button type="button" class="btn btn-sm btn-primary newMaterial" style="float: right;" onclick="addMaterial()"> Add New Material</button>
                                            <button id="saveBtn" style="float: right;margin-right: 10px;" class="btn btn-primary btn-sm" onclick="updateMaterials()" disabled="true">Save</button>
                                        </div>
                                        <div class="materials-group-list table-responsive" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th class="inputHead">Name</th>
                                                    <th class="inputHead">Cost</th>
                                                    <th class="inputHead">Local Price</th>
                                                    <th class="inputHead">Long Distance Price</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody id="materialsNamesTableBody">
                                                <tr id="emptyMat"> <td colspan="5" style="background-color: #e7eaec36;text-align: center;">No Materials Yet</td> </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div id="materialsTopRight" style="padding-bottom: 20px">
                                            <button type="button" class="btn btn-sm btn-primary" style="float: right;" onclick="addMaterial()"> Add New Material</button>
                                            <button id="saveBtnBottom" style="float: right;margin-right: 10px;" class="btn btn-primary btn-sm" onclick="updateMaterials()" disabled="true">Save</button>
                                        </div>
                                    </div>
                                    <div id="fullvalueprotection" style="display: none;">
                                        <div id="fullvalueprotectionWarpper" style="display: flex; flex-direction: column">
                                            <div style="padding-bottom: 20px;display: flex;flex-wrap: wrap;">
                                                <h2 style="float: left;">Full Value Protection</h2>
                                                <button id="openCalculatorBtn" style="margin-left:auto;" class="btn btn-primary" onclick="FVPcontroller.openRateCalculator();">Open Rate Calculator</button>
                                            </div>
                                            <div id="ValuationChargeRateCalculator"  class="ui-widget-content" style="cursor:pointer;display: none;position:fixed;padding: 10px 20px;z-index: 9999;top: 50%;left: 30%;background-color:#f5f5f6;">
                                                <button type="button" onclick="FVPcontroller.closeRateCalculator()" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                <div class="hidden-xs" style="float:right;margin-right: 13px;margin-top: 1px;"><i class="fa fa-arrows"></i> Click and hold to drag</div>

                                                <div style="padding-bottom: 20px">
                                                    <h3 style="float: left;">Valuation Charge Rate Calculator</h3>
                                                </div>
                                                <div id="valuationChargeRateCalculator" class="col-sm-10" style="display: flex; justify-content: initial; width: 100%;padding: 0;margin-top:1rem;">
                                                    <div class="valuationChargeRateCalculatorInputDiv">
                                                        <input id="valuationCharge" style="margin-right:5px; margin-top: auto" value="" name="valuationCharge" placeholder="Valuation Charge" type="tel" min="0" class="form-control form-control-sm valuation-charge-rate-calculator-input fvp-input">
                                                    </div>
                                                    <div class="valuationChargeRateCalculatorInputDiv">
                                                        <input id="amountOfLiability" style="margin-right:5px; margin-top: auto" value="" name="amountOfLiability" placeholder="Amount of Liability" type="tel" min="0" class="form-control  form-control-sm valuation-charge-rate-calculator-input fvp-input">
                                                    </div>
                                                    <div class="" style="display: flex;">
                                                        <input id="anchorNumber" style="margin-right:5px;margin-top:auto;width:60px;" value="1000" name="" type="tel" min="0" class="form-control form-control-sm valuation-charge-rate-calculator-input fvp-input" disabled>
                                                    </div>
                                                    <div class="" style="display: flex;" id="valuationChargeRateCalculatorButtonDiv">
                                                        <button id="calculateValuation" style="margin-right:5px;margin-top:auto; width: 85px; display: block" class="btn btn-primary valuation-charge-rate-calculator-input fvp-input" onclick="FVPcontroller.valuationChargeFormula();">Calculate</button>
                                                    </div>
                                                    <div class="valuationChargeRateCalculatorInputDiv">
                                                        <input id="rate" style="margin-right:5px; margin-top: auto" value="" name="rate" placeholder="Rate" type="tel" min="0" class="form-control form-control-sm valuation-charge-rate-calculator-input fvp-input">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="protection-group-list table-responsive" style="margin-top: 15px;">
                                                <form id="fvpForm" action="<?php echo $_SERVER["LOCAL_NL_URL"]; ?>/console/actions/moving/fvp/saveData.php" method="post" style="width: fit-content">
                                                    <div style="background-color: #f5f5f6; text-align: center;font-size: 13px;font-weight: bold;border:1px solid #EBEBEB;border-bottom: none;">Deductible Levels</div>

                                                    <div style="display: flex;flex-wrap: nowrap;">
                                                        <table class="table table-bordered main-table" id="fvpTable" style="margin-bottom: 0px;margin-bottom: 0px;justify-content: space-between;">
                                                            <thead id="fvpTableHeader"></thead>
                                                            <tbody id="fvpTableBody"></tbody>
                                                        </table>
                                                        <div style="background-color: #f5f5f6;display:flex;margin-bottom: 0px;padding: 9px;justify-content: space-between;border: 1px solid #e7eaec;border-left: 0;">
                                                           <div style="display: flex;flex-direction: column;margin-top: auto;margin-bottom: auto;">
                                                                <button class="btn btn-default btn-xs" style="margin-bottom: 13px;" type="button" onclick="FVPcontroller.removeCol();"><i class="fa fa-minus"></i></button><br>
                                                                <button class="btn btn-default btn-xs" type="button" onclick="FVPcontroller.addCol();"><i class="fa fa-plus"></i></button>
                                                           </div>
                                                        </div>
                                                    </div>
                                                    <div style="background-color: #f5f5f6;border: 1px solid #e7eaec;border-top: none;padding: 9px;text-align: center;">
                                                        <button class="btn btn-default btn-xs" style="margin-right: 13px;" type="button" onclick="FVPcontroller.removeRow();"><i class="fa fa-minus"></i></button>
                                                        <button class="btn btn-default btn-xs" type="button" onclick="FVPcontroller.addRow();"><i class="fa fa-plus"></i></button>
                                                    </div>
    <!--                                                <button type="submit" class="btn btn-sm btn-primary" style="float: right; margin-top: 13px;"> Update</button>-->

                                                </form>

                                            </div>
                                            <div class="row" style="padding:0 15px">
                                                <button type="submit" onclick="$('#fvpForm').submit()" class="updateFvpBtn ladda-button btn btn-sm btn-primary" data-style="zoom-in" style="float: right; margin-top: 13px;">
                                                    <span class="ladda-label">Save</span>
                                                    <span class="ladda-spinner"></span>
                                                </button>
                                                <button id="resetFvpToDefault" onclick="FVPcontroller.resetToDefault();" class="resetFvpBtn ladda-button btn btn-sm btn-default" data-style="zoom-in" style="float: left; margin-top: 13px;">
                                                    <span class="ladda-label">Reset To Default</span>
                                                    <span class="ladda-spinner"></span>
                                                </button>
                                            </div>
                                            <div class="row">
                                                <div class="alert alert-info" style="margin: 19px;display: block;">
                                                    Valuation Charge Formula:<br>
                                                    Valuation Charge= Rate x Amount of Liability / 1000<br><br>
                                                    Example:<br>
                                                    Valuation Charge: $2,100.00<br>
                                                    Amount of Liability: $70,000.00<br>
                                                    Valuation Charge Calculation: 2100 / 70000 * 1000 = $30.00<br>
                                                    Rate: $30.00<br><br>

                                                    For information about Valuation Charge Rate visit <a href="https://www.movinginsurance.com/" target="_blank">movinginsurance.com</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="salePerformance" style="display: none;">
                                        <div style="padding-bottom: 20px">
                                            <h2 style="float: left;">Salesman Performance</h2>
                                            <div id="reportrange" class="form-control" style="float: right;margin-bottom: 10px; max-width: fit-content;">
                                                <i class="fa fa-calendar"></i>
                                                <span></span> <b class="caret"></b>
                                            </div>
                                        </div>
                                        <div class="protection-group-list table-responsive" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Booked Jobs</th>
                                                    <th>Total Leads Handled</th>
                                                    <th>Sent Emails</th>
                                                    <th>Customer Payments<?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?><span onclick="showCustomerPaymentsInfo()"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span><?php } ?></th>
                                                    <th>Sales Bonus</th>
                                                </tr>
                                                </thead>
                                                <tbody id="performance">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="advertisingPerformance" style="display: none;">
                                        <div style="padding-bottom: 20px">
                                            <h2 style="float: left;">Advertising Performance</h2>
                                            <div id="reportrangeAdvert" class="form-control" style="float: right;margin-bottom: 10px; max-width: fit-content;">
                                                <i class="fa fa-calendar"></i>
                                                <span></span> <b class="caret"></b>
                                            </div>
                                        </div>
                                        <div class="protection-group-list table-responsive" style="margin-top: 38px;">
                                            <table class="table table-bordered main-table">
                                                <thead>
                                                <tr>
                                                    <th>Provider Name</th>
                                                    <th>Total Leads</th>
                                                    <th>Booked Jobs</th>
                                                    <th>Booked</th>
                                                    <th>Booked Estimates</th>
                                                    <th>Customer Payments</th>
                                                </tr>
                                                </thead>
                                                <tbody id="advertPerformance">

                                                </tbody>
                                                <tfoot id="advertFooter">

                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="movingSettings" style="display: none;">
                                        <div class="tab-pane">
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="checkbox checkbox-primary" style="padding-bottom: 0px;">
                                                            <?php if (isset($preEmailsData) && $preEmailsData && isset($organizationMailAccounts) && $organizationMailAccounts){ ?>
                                                                <input onchange="toggleSendEstimate(this)" class="settingsForm" id="sendEstimateEmail" type="checkbox" <?php if($movingSettingsData["sendEstimateEmail"] == 1){echo "checked";} ?>>
                                                            <?php  }else{ ?>
                                                                <input onchange="userCantSendAutoEmail()" id="sendEstimateEmail" type="checkbox">
                                                            <?php  }?>
                                                            <label for="sendEstimateEmail" style="text-align: left;" class="control-label">Send automatic email to new incoming leads <?php if ($bouncer["userSettingsData"]['helpMode'] == 1){?><span onclick="showEstimateEmailInfo(event)"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span> <?php } ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <select class="pull-right form-control settingsForm" id="estimateEmailId" style="display: inline-table;width: 32%;margin-left: 3px;" <?php if($movingSettingsData["sendEstimateEmail"] == 0){echo "disabled";}  ?>>
                                                            <option value="0">Email template</option>
                                                            <?php
                                                            foreach ($preEmailsData as $preEmailData){ ?>
                                                                <option value="<?php echo $preEmailData['id']; ?>" <?php if($preEmailData['id'] == $movingSettingsData["estimateEmailId"]){echo "selected";} ?>><?php echo $preEmailData['title']; ?></option>
                                                            <?php
                                                            } ?>
                                                        </select>
                                                        <select class="pull-right form-control settingsForm" id="estimateEmailAddressId" style="display: inline-table;width: 32%;" <?php if($movingSettingsData["sendEstimateEmail"] == 0){echo "disabled";}  ?>>
                                                            <option value="">Select account</option>
                                                            <?php
                                                            foreach($organizationMailAccounts as $organizationMailAccount){?>
                                                                <option value="<?php echo $organizationMailAccount['id']; ?>" <?php if($organizationMailAccount['id'] == $movingSettingsData["estimateEmailAddressId"]){echo "selected";} ?>><?php echo $organizationMailAccount['email']; ?></option>
                                                            <?php
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="checkbox checkbox-primary">
                                                    <input class="settingsForm" id="blockDuplicatesLeadsBasedPhone" type="checkbox"  <?php if($movingSettingsData["blockDuplicatesLeadsBasedPhone"] == 1){echo "checked";} ?>>
                                                    <label for="blockDuplicatesLeadsBasedPhone" style="text-align: left;" class="control-label">Block Duplicate Leads (based on phone)</label>
                                                </div>
                                                <div class="checkbox checkbox-primary">
                                                    <input class="settingsForm" id="blockDuplicatesLeadsBasedEmail" type="checkbox"  <?php if($movingSettingsData["blockDuplicatesLeadsBasedEmail"] == 1){echo "checked";} ?>>
                                                    <label for="blockDuplicatesLeadsBasedEmail" style="text-align: left;" class="control-label">Block Duplicate Leads (based on email)</label>
                                                </div>
                                            </fieldset>

                                            <div class="form-group"><label style="text-align: left;" class="col-sm-8 control-label">Set Default Fuel Surcharge</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group m-b">
                                                        <input id="calculateFuel" type="number" min="0" max="100" class="form-control settingsForm" value="<?=$movingSettingsData["calculateFuel"] ?>">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label style="text-align: left;" class="col-sm-8 control-label">Set Default % Coupon Discount</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group m-b">
                                                        <input id="couponDiscount" type="number" min="0" max="100" class="form-control settingsForm" value="<?=$movingSettingsData["couponDiscount"] ?>">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label style="text-align: left;" class="col-sm-8 control-label">Set Default % Of Senior Citizen Discount</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group m-b">
                                                        <input id="seniorDiscount" type="number" min="0" max="100" class="form-control settingsForm" value="<?=$movingSettingsData["seniorDiscount"] ?>">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="text-align: left;" class="col-sm-8 control-label">Set Default % Of Veteran Citizen Discount</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group m-b">
                                                        <input type="number" placeholder="Username" min="0" max="100" class="form-control settingsForm" id="veteranDiscount" value="<?=$movingSettingsData["veteranDiscount"] ?>">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="text-align: left;" class="col-sm-8 control-label">Cf / Lbs Ratio</label>
                                                <div class="col-sm-4">
                                                    <div class="m-b">
                                                        <input type="number" min="0" placeholder="7" class="form-control settingsForm" id="cflbsratio" value="<?=$movingSettingsData["cflbsratio"] ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="height: 53px;">
                                                <label style="text-align: left;" class="col-sm-8 control-label">Calculate Estimate By</label>
                                                <div class="col-sm-4">
                                                    <div class="m-b">
                                                        <select class="pull-right form-control settingsForm" id="calculateBy" style="float: none !important;">
                                                            <option value="0" <?php if($movingSettingsData['calculateBy'] == 0){echo "selected";} ?>>LBS</option>
                                                            <option value="1" <?php if($movingSettingsData['calculateBy'] == 1){echo "selected";} ?>>Cubic Feet</option>
                                                            <option value="2" <?php if($movingSettingsData['calculateBy'] == 2){echo "selected";} ?>>Hours</option>
                                                            <option value="3" <?php if($movingSettingsData['calculateBy'] == 3){echo "selected";} ?>>USD</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="height: 53px;">
                                                <label style="text-align: left;margin-top:8px !important;" class="col-sm-8 control-label">Tariff</label>
                                                <div class="col-sm-4">
                                                    <div class="m-b">
                                                        <button onclick="openTariff()" class="btn btn-block btn-default">Open Tariff data</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="height: 53px;">
                                                <label style="text-align: left;margin-top:8px !important;" class="col-sm-8 control-label">Job Board</label>
                                                <div class="col-sm-4">
                                                    <div class="m-b">
                                                        <div class="switch customSwitch settingsForm" align="center">
                                                            <div class="onoffswitch">
                                                                <input id="jobBoardFeature" class="onoffswitch-checkbox" <?php if($movingSettingsData["jobBoardFeature"] == "1"){ ?> checked <?php } ?> type="checkbox" name="jobBoardFeature" />
                                                                <label class="onoffswitch-label" for="jobBoardFeature">
                                                                    <span class="onoffswitch-inner" style="text-align:left"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="border-top: 1px dotted #c2c2c2;margin-top: 60px;">
                                                <label style="text-align: left;margin-top: 15px;" class="col-sm-8 control-label">Estimate terms of use <?php if ($bouncer["userSettingsData"]['helpMode'] == 1){?><span onclick="showEstimateTermsInfo(event)"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span> <?php } ?></label>
                                                <div id="summernote"><?= $movingSettingsData['estimateTerms'] ?></div>
                                            </div>
                                            <div class="form-group" style="border-top: 1px dotted #c2c2c2;margin-top: 60px;">
                                                <label style="text-align: left;margin-top: 15px;" class="col-sm-8 control-label">Full value protection terms of use <?php if ($bouncer["userSettingsData"]['helpMode'] == 1){?><span onclick="showFVPInfo(event)"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span> <?php } ?></label>
                                                <div id="summernoteFVP"><?= $movingSettingsData['FVPTerms'] ?></div>
                                            </div>
                                            <div class="form-group" style="border-top: 1px dotted #c2c2c2;margin-top: 60px;">
                                                <label style="text-align: left;margin-top: 15px;" class="col-sm-8 control-label">Credit card authorization terms of use <?php if ($bouncer["userSettingsData"]['helpMode'] == 1){?><span onclick="showEstimateTermsCreditCardInfo(event)"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span> <?php } ?></label>
                                                <div id="summernote3"><?= $movingSettingsData['estimateTermsCreditCard'] ?></div>
                                            </div>

                                        </div>
                                        <div class="row buttonsSettings">
                                            <div class="col-md-9"></div>
                                            <div class="col-md-3">
                                                <button onclick="saveData()" class="btn btn-block btn-default" id="price-send" disabled="true">Save Changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </main>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
            </div>
            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
        </div>

        <!-- Mainly scripts -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

        <!-- jQuery UI -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

        <!-- Toastr -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

        <!-- SweetAlerts -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

        <!-- Touchspin -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

        <!-- Ladda -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

        <!-- Jasny -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

        <!-- Date range use moment.js same as full calendar plugin -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
        <!-- Date range picker -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Data picker -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/datapicker/bootstrap-datepicker.js"></script>

        <!-- == Moving Section == -->
        <!-- I-Checks -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/iCheck/icheck.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/summernote/summernote.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">

        <script src="<?php echo $_SERVER['CDN']; ?>/home/js/plugins/ajaxForm/jquery.form.js"></script>

        <script>
            function chooseTab(id){
                setButtons(id);
                $('#mainContent').children("div").each(function(){
                    $(this).hide();
                });
                $("#"+id).show();
                switch (id) {
                    case 'trucks':
                        getTrucks();
                        break;
                    case 'carriers':
                        getCarriers();
                        break;
                    case 'crew':
                        getCrew();
                        break;
                    case 'storage':
                        getStorages();
                        break;
                    case 'inventory':
                        getGroups();
                        break;
                    case 'materials':
                        getMaterials();
                        break;

                    case 'fullvalueprotection':
                        FVPcontroller.getData();
                        break;
                }
            }
            function setButtons(id){
                $("#movingMenu .menu-full div").each(function(){
                    if ($(this).hasClass('active')){
                        $(this).removeClass('active');
                    }
                });
                $('#btn-'+id).addClass('active');
            }
            function showCustomerPaymentsInfo(){
                swal({
                    text: 'Payments are calculated by the user that added the payment, not by the user that handles the lead.',
                    buttons: {
                        skip:{
                            text:"Got It",
                        }
                    }
                });
            }
        </script>


        <!-- Trucks -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/trucks.min.js"></script>
        <!-- Carriers -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/carriers.min.js"></script>
        <!-- Crew -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/crew.min.js"></script>
        <!-- Storage -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/storage.min.js"></script>
        <!-- Inventory -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/inventory.min.js"></script>
        <!-- Materials -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/materials.min.js"></script>
        <!-- Moving Settings -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/movingSettings.min.js"></script>
        <!-- FVP -->
        <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/fvp.min.js"></script>

        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

    <?php if (isset($_GET['truck'])){ ?>
        <script>
            $(document).ready(function(){
                var id = <?= $_GET['truck'] ?>;
                chooseTab('trucks');
                getTruck(id);
            });
        </script>
    <?php } ?>
    <?php if (isset($_GET['showTab']) && $_GET['showTab'] == "materials"){ ?>
    <script>
        $(document).ready(function(){
            chooseTab('materials');
        });
    </script>
<?php } ?>

    <script>
        function openTariff() {

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");
            iframe.src = BASE_URL + "/console/categories/iframes/moving/tariff.php";
            iframe.setAttribute("style", "border:none;width: 100%;height:600px");
            iframe.setAttribute("name", "tariffIframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: {
                    cancel: "cancel",
                    Save: "Save"
                },
                className: "swal-tariff"
            }).then(data => {
                if (data == "Save") {
                    window.tariffIframe.saveData();
                }
            });
        }
        function addNewCrewMember(crewId){

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");
            if(crewId != undefined){
                iframe.src = BASE_URL+"/console/categories/iframes/moving/addCrew.php?crewId="+crewId;
            }else{
                iframe.src = BASE_URL+"/console/categories/iframes/moving/addCrew.php";
            }

            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-crew-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-crew"
            });
        }
        function addNewTruck(){

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");

            iframe.src = BASE_URL+"/console/categories/iframes/moving/addTruck.php";


            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-trucks-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-trucks"
            });
        }
        function addNewCarrier(){

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");

            iframe.src = BASE_URL+"/console/categories/iframes/moving/addCarrier.php";


            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-carrier-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-carrier"
            });
        }
        function updateCarrier(carrierId){

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");

            iframe.src = BASE_URL+"/console/categories/iframes/moving/editCarrier.php?carrierId="+carrierId;


            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-update-carrier-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-update-carrier"
            });
        }
        function addNewGroup(){

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");

            iframe.src = BASE_URL+"/console/categories/iframes/moving/inventory/addGroup.php";


            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-group-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-group"
            });
        }
        function addNewItem(groupId){
            console.log(groupId);
            var content = document.createElement("div");

            var iframe = document.createElement("iframe");

            iframe.src = BASE_URL+"/console/categories/iframes/moving/inventory/addItem.php?id="+groupId;


            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-item-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-item"
            });
        }
        function addNewStorage(){

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");

            iframe.src = BASE_URL+"/console/categories/iframes/moving/addStorage.php";


            iframe.setAttribute("style","border:none;width: 100%;height:100%;");
            iframe.setAttribute("name","swal-storage-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-storage"
            });
        }



    </script>
        <?php
        //FVP style
        //This style didnt work when it was at the top of the page
        ?>
    <style>

        #fvpTableBody td{
            padding: 0 !important;
        }
        #fvpTableBody input{
            border: none !important;
        }
        @media only screen and (max-width: 768px) {
            #valuationChargeRateCalculator {
                flex-wrap: wrap;
            }
            #valuationChargeRateCalculatorButtonDiv, .valuationChargeRateCalculatorInputDiv{
                margin-top: 5px;
            }
            #valuationChargeRateCalculatorButtonDiv{
                margin-left: auto;
            }
        }
        .valuationChargeRateCalculatorInputDiv{
            width:100%;
            padding-right: 5px;
            display: flex;
            flex-direction: column;
        }
        @media (max-width: 576px) {
            #ValuationChargeRateCalculator{
                left: 0 !important;
            }
        }
        @media (max-width: 1600px) {
            #fvpTableHeader .fvp-input {
                width: 90px !important;
            }
            #fvpTableBody .fvp-input {
                width: 90px !important;
            }
        }
    </style>
    </body>
</html>

