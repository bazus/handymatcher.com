<?php
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
if ($bouncer['organizationData']['isNMpartner'] != 1){
    header("location: ".$_SERVER['LOCAL_NL_URL']."/console/");
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Network Leads - Network Moving Partners</title>

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/iCheck/custom.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <script>
            var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
            var NLid = "<?= $bouncer['organizationData']['id']; ?>";
        </script>

        <style>
            dl,dd{
                margin-bottom: 3px !important;
            }
            .tabler th{
                text-align: center;
            }
            .panel-body {
                padding: 0 15px 15px 15px;
            }
            .reasons{
                text-overflow: ellipsis;
                overflow: hidden;
                max-width: 100px;
                white-space: nowrap;
            }
            .nav-tabs > li > a{
                color: #676767 !important;
            }
        </style>
    </head>

    <body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
        <div id="wrapper">
            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

                <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
                    <div class="col-lg-10">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                            </li>
                            <li>
                                Moving
                            </li>
                            <li class="active">
                                <strong>Network Moving Partners</strong>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div id="mainTab" class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInUp">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="m-b-md">
                                                <h2 id="orgName"></h2>
                                            </div>

                                        </div>
                                    </div>
                                    <?php if($bouncer["isUserAnAdmin"]){ ?>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <dl class="row mb-0">
                                                <div class="col-sm-4 text-sm-right"><dt>Status:</dt> </div>
                                                <div class="col-sm-8 text-sm-left"><dd class="mb-1"><span class="label" id="isActive"></span></dd></div>
                                            </dl>
                                            <dl class="row mb-0">
                                                <div class="col-sm-4 text-sm-right"><dt>Owner:</dt> </div>
                                                <div class="col-sm-8 text-sm-left"><dd class="mb-1" id="contactName"></dd> </div>
                                            </dl>
                                            <dl class="row mb-0">
                                                <div class="col-sm-4 text-sm-right"><dt>Personal Rep:</dt> </div>
                                                <div class="col-sm-8 text-sm-left"> <dd class="mb-1"><a href="#" class="text-navy" id="personalRep"></a> </dd></div>
                                            </dl>

                                        </div>
                                        <div class="col-xs-6" id="cluster_info">

                                            <dl class="row mb-0">
                                                <div class="col-sm-4 text-sm-right">
                                                    <dt>Joined:</dt>
                                                </div>
                                                <div class="col-sm-8 text-sm-left">
                                                    <dd class="mb-1" id="joinDate"></dd>
                                                </div>
                                            </dl>
                                            <dl class="row mb-0">
                                                <div class="col-sm-4 text-sm-right">
                                                    <dt>Type:</dt>
                                                </div>
                                                <div class="col-sm-8 text-sm-left">
                                                    <dd class="mb-1" id="orgType"></dd>
                                                </div>
                                            </dl>
                                        </div>
                                    </div>
                                    <?php }else{ ?>
                                    <div class="row">
                                        <div class="col-xs-12 m-l-md">
                                            <h3>
                                                My requests
                                            </h3>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="row m-t-sm">
                                        <div class="col-lg-12">
                                            <div class="panel blank-panel">
                                                <div class="panel-heading">
                                                    <div class="panel-options">
                                                        <ul class="nav nav-tabs">
                                                            <?php if($bouncer["isUserAnAdmin"]){ ?>
                                                            <li class="active"><a class="nav-link active" href="#tab-1" data-toggle="tab">Campaigns</a></li>
                                                            <?php } ?>
                                                            <li <?php if(!$bouncer["isUserAnAdmin"]){ ?>class="active"<?php } ?>><a class="nav-link <?php if(!$bouncer["isUserAnAdmin"]){ ?>active<?php } ?>" href="#tab-2" data-toggle="tab">Requests</a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <div class="tab-content">
                                                        <?php if($bouncer["isUserAnAdmin"]){ ?>
                                                        <div class="tab-pane active table-responsive" id="tab-1">
                                                            <table class="table table-bordered">
                                                                <thead class="tabler">
                                                                <th>Campaign Name</th>
                                                                <th>Budget</th>
                                                                <th>Filters</th>
                                                                <th>Post Methods</th>
                                                                <th>Last Lead</th>
                                                                <th>Leads Today</th>
                                                                <th>Status</th>
                                                                </thead>
                                                                <tbody id="campaignContent">
                                                                    <tr>
                                                                        <td colspan="7" id="noCampaigns" style="text-align: center">No Campaigns</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <?php } ?>
                                                        <div class="tab-pane table-responsive<?php if(!$bouncer["isUserAnAdmin"]){ ?> active<?php } ?>" id="tab-2">

                                                            <table class="table table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th>Status</th>
                                                                    <th>Lead ID</th>
                                                                    <th>Open Date</th>
                                                                    <th>Reasons</th>
                                                                    <th>Response</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="requests">
                                                                    <tr>
                                                                        <td colspan="5" id="noRequests" style="text-align: center">No Requests</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="filesTab" class="col-lg-3" style="display: none;">
                        <div class="wrapper wrapper-content project-manager">
                            <h4>Project description</h4>
                            <p class="small">
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look
                                even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing
                            </p>
                            <p class="small font-bold">
                                <span><i class="fa fa-circle text-warning"></i> High priority</span>
                            </p>
                            <h5>Project tag</h5>
                            <ul class="tag-list" style="padding: 0">
                                <li><a href=""><i class="fa fa-tag"></i> Zender</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Lorem ipsum</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Passages</a></li>
                                <li><a href=""><i class="fa fa-tag"></i> Variations</a></li>
                            </ul>
                            <h5>Project files</h5>
                            <ul class="list-unstyled project-files">
                                <li><a href=""><i class="fa fa-file"></i> Project_document.docx</a></li>
                                <li><a href=""><i class="fa fa-file-picture-o"></i> Logo_zender_company.jpg</a></li>
                                <li><a href=""><i class="fa fa-stack-exchange"></i> Email_from_Alex.mln</a></li>
                                <li><a href=""><i class="fa fa-file"></i> Contract_20_11_2014.docx</a></li>
                            </ul>
                            <div class="text-center m-t-md">
                                <a href="#" class="btn btn-xs btn-primary">Add files</a>
                                <a href="#" class="btn btn-xs btn-primary">Report contact</a>

                            </div>
                        </div>
                    </div>
                </div>

                <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
            </div>
            <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
        </div>

        <!-- Mainly scripts -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

        <!-- jQuery UI -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

        <!-- Toastr -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

        <!-- SweetAlerts -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

        <!-- Touchspin -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

        <!-- Ladda -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

        <!-- Jasny -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

        <!-- Date range use moment.js same as full calendar plugin -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
        <!-- Date range picker -->
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
        <?php if($bouncer["isUserAnAdmin"]){ ?>
            <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/partners.min.js"></script>
        <?php }else{ ?>
            <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moving/partners.user.min.js"></script>
        <?php } ?>

    </body>
</html>

