<?php
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

$jobBoardBaseURL = "";
if($_SERVER["ENVIRONMENT"] == "prod"){
    $jobBoardBaseURL = "https://jobboard.network-leads.com";
}elseif($_SERVER["ENVIRONMENT"] == "stage"){
    $jobBoardBaseURL = "https://jobboard.thenetworkleads.com";
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads - Job Board</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">


    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/leads.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/leads.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/dataTables/datatables.min.css" rel="stylesheet">


    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>

    <style>
        .jobBoardTable th{
            text-align: center;
        }
        .jobBoardTable td{
            text-align: center;
            cursor: pointer;
            vertical-align: middle !important;
        }
        .jobBoardTable>tbody td{
            vertical-align: middle;
            padding: 5px;
        }

        .jobBoardTable>tbody tr>td:last-child>div{
            display: flex;
            justify-content: space-evenly;
            border: none;
        }

        .disabledJob>td{
            background-color: #dd344521;
        }
        .dataTables_paginate{
            margin-top: 13px !important;
        }
    </style>

</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li  class="active">
                        <strong>Job Board</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">


                <div style="margin-bottom: 13px;">
                    <div class="dropdown settings-dropdown" style="display: inline-table">
                        <button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="dropdownMenu330">
                            <i class="fa fa-cog" aria-hidden="true" ></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu330">
                            <li><a href="#" onclick="jobBoardController.copyBoardLink();">Copy board link</a></li>
                            <li><a href="#" onclick="jobBoardController.sendBoardLink();">Send board link</a></li>
                        </ul>
                    </div>
                    <button class="btn btn-sm btn-default" style="display:inline-table;margin-left: 5px;padding-top: 4px;padding-bottom: 5px;" onclick="jobBoardController.openBoardLink();">Open Board</button>
                </div>

                <div class="ibox-content" style="margin-bottom: 10px; margin-right: 6px;">

                    <table class="table table-hover table-bordered jobBoardTable">
                        <thead>
                        <tr>
                            <th class="th-jobNumber" style="text-align:center;width: 73px;">Job #</th>
                            <th class="th-from">From</th>
                            <th class="th-to">To</th>
                            <th class="th-size">Size</th>
                            <th class="th-moveDate">Move Date</th>
                            <th class="th-price">Price</th>
                            <th class="th-signatures">Signatures</th>
                            <th class="th-action" style="width:230px;">Action</th>
                        </tr>
                        </thead>
                        <tbody id="jobsRows">
                            <tr><td style="text-align:center;" colspan="8">No Jobs</td></tr>
                        </tbody>
                    </table>

                    <div class="alert alert-info">
                        All jobs in board will be available <a href="<?php echo $jobBoardBaseURL."/".$bouncer["organizationData"]["organizationNameKey"]; ?>" class="alert-link" target="_blank">here</a>.<br>
                        Disabled jobs will not appear on the board.<br>
                        You can share your link with anyone you want.<br>
                        Signed jobs will appear under your 'job acceptance forms' in the 'operation' tab inside the lead.
                    </div>
                </div>

            </div>

        </div>
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
</div>
<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/dataTables/datatables.min.js"></script>

<script>

    var jobBoardController = {
        getJobs:function() {


            var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/moving/jobs/getJobs.php";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {},
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    jobBoardController.setJobTable(data);
                }catch (e) {

                }
            });
        },
        openJob:function(leadId){
            window.open("<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/leads/lead.php?leadId="+leadId,"_blank");
        },
        setJobTable:function(data) {

            if($('.dataTable').length){
                var table = $('.jobBoardTable').DataTable();
                table.destroy();
            }

            $("#jobsRows").html("");

            if(data.length == 0){
                var tr = document.createElement("tr");

                var td = document.createElement("td");
                td.setAttribute("style","text-align:center;");
                td.innerHTML = "No Jobs";
                td.setAttribute("colspan","8");

                tr.appendChild(td);
                document.getElementById("jobsRows").appendChild(tr);
            }

            for (var i = 0; i < data.length; i++) {

                var row = document.createElement("tr");
                if(data[i].isActive == "0") {
                    row.className = "disabledJob";
                }

                row.setAttribute("onClick","jobBoardController.openJob('"+data[i].leadId+"')");

                var jobNumber = document.createElement("td");
                jobNumber.innerHTML = data[i].leadData.jobNumber;

                var From = document.createElement("td");
                From.innerText = data[i].leadData.from;

                var To = document.createElement("td");
                To.innerText = data[i].leadData.to;

                var size = document.createElement("td");
                size.innerText = data[i].estimateData.size;
                size.setAttribute("data-order", data[i].estimateData.sizeNumber);

                var date = document.createElement("td");
                date.innerText = data[i].leadData.pickupDate;
                date.setAttribute("data-order", data[i].leadData.pickupDateTimestamp);

                var price = document.createElement("td");
                price.innerText = "$"+data[i].carrierBalance;

                var signed =  document.createElement("td");
                signed.setAttribute("data-order", data[i].totalSignatures);

                if(data[i].totalSignatures == "0"){
                    signed.innerText = "No signatures";
                }else{
                    var signedBadge =  document.createElement("span");
                    signedBadge.className = "label label-success";
                    if(data[i].totalSignatures == "1"){
                        signedBadge.innerHTML = "Signed " + data[i].totalSignatures + " time";
                    }else{
                        signedBadge.innerHTML = "Signed " + data[i].totalSignatures + " times";
                    }
                    signed.appendChild(signedBadge);
                }

                var action = document.createElement("td");
                var actionDiv = document.createElement("div");

                var deleteBtn = document.createElement("deleteBtn");

                deleteBtn.className = "btn btn-sm btn-outline btn-danger";

                deleteBtn.setAttribute("onClick", "jobBoardController.deleteJob(event,"+data[i].id+")");
                deleteBtn.innerHTML = "Remove";

                actionDiv.appendChild(deleteBtn);
                action.appendChild(actionDiv);

                row.appendChild(jobNumber);
                row.appendChild(From);
                row.appendChild(To);
                row.appendChild(size);
                row.appendChild(date);
                row.appendChild(price);
                row.appendChild(signed);
                row.appendChild(action);

                document.getElementById("jobsRows").appendChild(row);
            }
            startTableSort();

        },
        disableJob:function(jobId){

            var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/moving/jobs/disableJob.php";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    jobId: jobId
                },
                async: true
            }).done(function (data) {
                jobBoardController.getJobs();
            });
        },
        reactivateJob:function(jobId){

            var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/moving/jobs/reactivateJob.php";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    jobId: jobId
                },
                async: true
            }).done(function (data) {
                jobBoardController.getJobs();
            });
        },
        deleteJob:function(e,jobId){
            e.preventDefault();
            e.stopPropagation();

            swal({
                title: "Are you sure?",
                text: "This will remove the job from your board",
                icon: "warning",
                dangerMode: true,
                buttons: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/moving/jobs/deleteJob.php";
                    jQuery.ajax({
                        url: strUrl,
                        method: "POST",
                        data: {
                            jobId: jobId
                        },
                        async: true
                    }).done(function (data) {
                        jobBoardController.getJobs();
                    });
                }
            });
        },
        openBoardLink:function () {
            window.open("<?php echo $jobBoardBaseURL."/".$bouncer["organizationData"]["organizationNameKey"]; ?>");
        },
        copyBoardLink:function () {
            var string= "<?php echo $jobBoardBaseURL."/".$bouncer["organizationData"]["organizationNameKey"]; ?>";
            var el = document.createElement('textarea');
            el.value = string;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);

            parent.swal("Link Copied","", "success");
        },
        sendBoardLink:function () {
            parent.sendSMScontroller.init("",undefined,"<?php echo $bouncer["organizationData"]["organizationName"]; ?> is inviting you to view their job board: <?php echo $jobBoardBaseURL."/".$bouncer["organizationData"]["organizationNameKey"]; ?>\r\n \r\nPlease do not reply to this text");
        },
        addJobToBoard:function (leadId, carrierBalance) {
            var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/moving/jobs/addToJobBoard.php";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    leadId: leadId,
                    carrierBalance:carrierBalance
                },
                async: true
            }).done(function (data) {
                try{
                    data = JSON.parse(data);
                    if(data.status == false && data.reason != ""){
                        parent.swal("Oops", data.reason , "error");
                    }
                }catch (e) {

                }


                jobBoardController.getJobs();
            });
        }
    }

    $( document ).ready(function() {
        jobBoardController.getJobs();
    });
    function startTableSort() {

        var dTable = $('.jobBoardTable').DataTable({
            "searching": true,
            "responsive": false,
            "autoWidth": false,
            "fixedColumns": true,
            "aaSorting": [],
            "oLanguage": {
                "sInfo": ""
            },
            "bFilter": false,
            "sScrollX": "100%",
            "pageLength": 11,
            "lengthChange": false,
            "columnDefs": [ {
                "targets": ["th-jobNumber","th-action"],
                "orderable": false,
                "visible": true
            } ],
            "language": {
                "paginate": {
                    "previous": "Previous ",
                    "next": " Next"
                }
            }
        });

        var info = dTable.page.info();
        if(info.pages == 1){
            $('.dataTables_paginate').hide();
        }

        $('.jobBoardTable').DataTable();
        $('.dataTables_length').addClass('bs-select');

        $('thead').css("background-color","#F5F5F6");
    }

</script>

</body>
</html>

