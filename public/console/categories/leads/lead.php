<?php
$pagePermissions = array(false,array(1),true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/userManagement.php");

$userManagement = new userManagement($bouncer["credentials"]["orgId"]);
$orgData = $userManagement->getOrganizationData();
$orgName = $orgData['organizationName'];

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$folders = $preEmails->getFoldersData();

if (isset($_GET['leadId'])) {
    $leadId = $_GET['leadId'];
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    if ($bouncer['isUserAnAdmin'] == true){
        if ($lead->isLeadFromOrg() == false){
            header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
        }
    }else{
        if ($lead->isLeadFromOrg(true,$bouncer["credentials"]["userId"]) == false){
            header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
        }
    }

}else{
    header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Lead #<?=$leadId?></title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/jQueryUI/jquery-ui.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/contextMenu/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/lead.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/movingEstimate.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.css" rel="stylesheet">



    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
        var BASE_YMQ_URL = "<?= $_SERVER['YMQ_URL'] ?>";
        var leadId = "<?= $leadId ?>";
        var helpMode = <?= $bouncer["userSettingsData"]["helpMode"] ?>;
        var leadGoogleMaps = <?= $bouncer["userSettingsData"]["showMapsInLeads"] ?>;
        var selectedFolder = null;
        var folders = <?php echo json_encode($folders); ?>;
        function iframeLoaded(){return false;}
    </script>

    <style>

        .squareSelect{
            padding: 0;
            padding-left: 3px;
            -webkit-appearance: none;
            -moz-appearance: none;
            background-position: right 50%;
            background-repeat: no-repeat;
            background-image: url('data:image/svg+xml;utf8,<?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1"><path d="M4 8L0 4h8z"/></svg>');
        }
        #estimateChangeStatusDropdownList>li.disabled{
            background-color: #f3f3f3;
        }

        #createEstimateUL.disabled li a{
            background-color: #efefef;
            pointer-events: none;
            color: #8e8e8e;
        }

        .dropdown-estimate-tab > li > a:hover{
            color: #262626 !important;
            text-decoration: none !important;
            background-color: #f5f5f5 !important;
        }

        .closeIcon{
            float: right;
            font-size: 21px;
            font-weight: 700;
            line-height: 1;
            cursor: pointer;
        }
        .closeIcon.disabled{
            display: none;
        }


        .CarrierSmsOrEmailSwal .swal-footer{
            display: flex;
            justify-content: space-evenly;
        }
        .CarrierSmsOrEmailSwal .swal-button{
            background-color: #1ab394;
        }    .CarrierSmsOrEmailSwal .swal-button:hover{
            background-color: #1ab394;
        }
        .swal-button:focus {
            overflow: hidden;
        }
        #extraCharges>:first-child{
            margin-top: 13px;
        }
        #extraCharges>:last-child{
            margin-bottom: 13px;
        }
        #movingMaterials>:first-child{
            margin-top: 13px;
        }
        #movingMaterials>:last-child{
            margin-bottom: 13px;
        }
        .updatedBtnAndSmallWrapper{
            display: flex;
            justify-content: space-between;
        }
        .updatedBtnAndSmall{
            display: flex;
            justify-content: flex-end;
            overflow: auto;
        }
        @media only screen and (max-width: 992px) {
            .signItem{
                width: 100%;
                margin-top: 15px !important;
            }
        }

        .swal-move-board{
            width: 90% !important;
            height: 94% !important;
            overflow-y: hidden;
        }
        .swal-move-board .swal-content{
            margin: 0px !important;
        }

        .swal-lead-create-claim{
            width: 600px !important;
            height: 800px !important;
            overflow-y: hidden;
        }
        .swal-lead-create-claim .swal-content{
            margin: 0px !important;
            padding: 0px !important;
        }



        @media only screen and (max-width: 600px) {
            .swal-lead-create-claim{
                width: 100% !important;
            }
            .swal-responsive-on-mobile{
                width: 100% !important;
            }
        }

        .swal-operations-assign{
            overflow-y: hidden;
            height: 631px;
        }
        .swal-operations-assign .swal-content{
            margin: 0px !important;
            padding: 0 !important;
            height: 631px !important;
        }
        .swal-add-payment{
            margin: 0;
        }

        .swal-add-payment .swal-content{
            margin: 0;
            padding: 0;
        }
        .swal-add-payment .swal-footer{
            justify-content: space-around;
            display: flex;
        }
        #claimsTable td{
            vertical-align: middle;
        }
        .activity-details-list{
            padding-top:3px;
        }

        .lead-status-warning>span{
            /* label label-warning */
            background-color: #f8ac59 !important;
            border-color: #f8ac59 !important;
            color: #FFFFFF !important;
        }
        .lead-status-info>span{
            /* label label-info */
            background-color: #23c6c8 !important;
            border-color: #23c6c8 !important;
            color: #FFFFFF !important;
        }
        .lead-status-primary>span{
            /* label label-primary */
            background-color: #1ab394 !important;
            border-color: #1ab394 !important;
            color: #FFFFFF !important;
        }
        .lead-status-success>span{
            /* label label-success */
            background-color: #1c84c6 !important;
            border-color: #1c84c6 !important;
            color: #FFFFFF !important;
        }
        .lead-status-danger>span{
            /* label label-danger */
            background-color: #ed5565 !important;
            border-color: #ed5565 !important;
            color: #FFFFFF !important;
        }

        .stream {
            padding: 6px 0 !important;
            word-wrap: break-word;
        }
        .stream-badge i{
            font-size: 10px !important;
        }
        .stream .stream-panel {
            margin-left: 43px !important;
            padding: 3px 0px 0px 0px !important;
        }

        .swal-update-carrier-balance .swal-footer{
            display: flex;
            justify-content: space-between;
        }

        @media (max-width: 767px){
            #estimateChangeStatusDropdownList{
                position: absolute;
                right: 0;
                left: auto;
                background-color: #fff !important;
            }

            .estimateActionButtons{
                display: flex;
                flex-direction: column;
            }

            .estimateActionButtons>form{
                width: 100%;
            }
            .estimateActionButtons>button,.estimateActionButtons>form>button{
                margin-bottom: 6px;
                width: 100%;
            }
        }

        .fvpTable input{
            width: 79px !important;
        }

        .resetFVPAOL_POPUP{
            width: 300px;
        }
        .resetFVPAOL_POPUP .popover-content{
            width: 100%;
        }


        .divFeatureDisabled>*::after{
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display: block;
            z-index: 3;
        }
        .swal-lead-create-claim{
            margin:0;
        }
        @media (max-width: 576px) {
            .swal-lead-create-claim{
                height: 100% !important;
            }
            #itemsTable .bootstrap-touchspin span{
                display: none;
            }
            /* remove input number arrows */
            #itemsTable .bootstrap-touchspin input::-webkit-outer-spin-button,
            #itemsTable .bootstrap-touchspin input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            /* remove input number arrows */
            /* Firefox */
            #itemsTable .bootstrap-touchspin input[type=number] {
                -moz-appearance: textfield;
            }
        }
        @media (max-width: 992px) {
            #totalUnPackingCostsWrapper,#totalPackingCostsWrapper,#startInitialWrapper{
                display: none;
            }
            .estimateInputContainer{
                display:flex;
            }
            .initalPriceInputs{
                width:50% !important;
            }
            .packingInputWarpper{
                width:33.3% !important;
            }
            #fuelSurchargeWrapper{
                width:190px !important;
            }
            #estimatesTab{
                padding: 15px 10px 20px 10px;
            }
            .packingBtnSpan{
                padding:6px 3px;
            }
        }
        .editLeadDetailsInputLabel{
            position: relative;
        }
        @media (max-width: 576px) {

            #leadDetailsColsWrapper{
                flex-direction: column;
            }
            #leadDetailsColsWrapper #left, #right{
                display: flex;
                flex-direction: column;
                width: 100% !important;
            }
            #leadDetailsFromAddress{
                order: -1;
            }
            #leadDetailsFromApartment{
                order: 0;
            }
            #leadDetailsFromApartmentType{
                order: 1;
            }
            #leadDetailsFromFloor{
                order: 2;
            }
            #leadDetailsFromLevel{
                order: 3;
            }
            #leadDetailsFromCity{
                order: 4;
            }
            #leadDetailsFromState{
                order: 5;
            }
            #leadDetailsFromZip{
                order: 6;
            }
            #leadDetailsToAddress{
                order: -1;
            }
            #leadDetailsToApartment{
                order: 0;
            }
            #leadDetailsToApartmentType{
                order: 1;
            }
            #leadDetailsToFloor{
                order: 2;
            }
            #leadDetailsToLevel{
                order: 3;
            }
            #leadDetailsToCity{
                order: 4;
            }
            #leadDetailsToState{
                order: 5;
            }
            #leadDetailsToZip{
                order: 6;
            }
            #moveEditBtn,#leadEditBtn{
                width: 100%;
            }
        }


    </style>
</head>

<body <?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>class="mini-navbar"<?php } ?>>
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>
        <div class="row" style="margin-top: 13px;">
            <div class="col-lg-4 col-md-12">
                <div class="ibox" style="margin-bottom: 11px;">
                    <div class="ibox-content" style="padding: 0px;">
                        <div class="padIt" style="display: flex; justify-content: space-between;flex-wrap:wrap;flex-direction: row-reverse;">


                            <div style="display: flex; flex-direction: row-reverse;margin-left:auto;height:max-content;">
                                <div class="input-group" id="estimateStatusWrapper" style="margin-top:3px;float: right;margin-left: 3px;width: -webkit-min-content;">
                                    <span id="estimateStatus" style="float:right;font-size: 13px;padding: 7px;" class="label">New</span>
                                    <span class="input-group-addon" style="background-color: #d1dade;color: #5e5e5e;border-color: #d1dade;padding: 0;border-left: 1px solid #ffffff !important;border-bottom-right-radius: 3px;border-top-right-radius: 3px;">
                                         <ul class="nav navbar-nav navbar-right" style="margin: 0px">
                                            <li class="dropdown">
                                              <span href="#" id="estimateChangeStatusDropdown" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 7px 15px 8px 13px;"><span class="caret"></span></span>
                                              <ul class="dropdown-menu" style="margin-top: 13px;width: 211px;color: #000;" id="estimateChangeStatusDropdownList">
                                              </ul>
                                            </li>
                                          </ul>
                                    </span>
                                </div>

                                <span id="estimateVIP" style="margin-top:3px;float: right; margin-left: 5px;"></span>
                                <span id="badLeadReasons" onclick="leadController.lead.badLead()" style="float: right;font-size: 13px;padding: 7px;margin-left: 5px;margin-top:3px;cursor: pointer;" class="label label-danger"></span>
                            </div>

                            <div style="display: inline-table;margin-right:auto;">
                                <h2 id="leadTopName"></h2>
                                <span style="display: block"><i id="emailIcon" class="fa fa-envelope-o"></i> <span id="leadTopEmail"></span></span>
                                <span style="display: block"><span id="leadTopPhone"></span></span>
                            </div>
                        </div>
                        <div align="center" style="text-align: center;width:100%;border-bottom: 1px solid #f1f1f1d1;border-top: 1px solid #f1f1f1d1;display: flex;justify-content: space-evenly;" class="btn-group menuBtns">
                            <div class="menuBtn tabMenu1 active" onclick="leadController.page.showTabById('1')" id="tabMenu1">Details</div>
                            <div class="menuBtn tabMenu3" onclick="leadController.page.showTabById('2')" id="tabMenu3">Settings</div>
                        </div>
                        <div class="feed-activity-list">

                            <div class="tab" id="tab-1" style="display: block;">
                                <div class="feed-element padIt">
                                    <div class="activity-details-list">
                                        <small class="pull-right text-navy" id="jobNumber"></small>
                                        <div>Job #</div>
                                    </div>
                                    <div class="activity-details-list">
                                        <small class="pull-right text-navy" id="userHandle"></small>
                                        <div>User Handling</div>
                                    </div>
                                    <div class="activity-details-list">
                                        <small class="pull-right text-navy"><span id="distanceMiles"></span></small>
                                        <div>Distance</div>
                                    </div>
                                    <div class="activity-details-list">
                                        <small class="pull-right text-navy" id="dateReceived"><i class="fa fa-clock-o"></i></small>
                                        <div>Received</div>
                                    </div>
                                    <div class="activity-details-list" id="operationsLeftSide" style="display: none;">
                                        <span id="operationTabTotalAssigned" style="display: none;cursor: pointer;" onclick="leadController.moving.operations.assign();" class="label pull-right">Nothing Assigned</span>
                                        <span id="operationTabTotalTrucks" class="label label-primary pull-right" style="cursor:pointer;" onclick="leadController.moving.operations.assign()"></span>
                                        <span id="operationTabTotalCarriers" class="label label-primary pull-right" style="cursor:pointer;margin-right: 3px;" onclick="leadController.moving.operations.assign()"></span>
                                        <span id="operationTabTotalCrew" class="label label-primary pull-right" style="cursor:pointer;margin-right: 3px;" onclick="leadController.moving.operations.assign()"></span>
                                        <div>Operation</div>
                                    </div>
                                    <div class="activity-details-list" id="leadTags">

                                    </div>
                                </div>
                                <div style="cursor: pointer;padding: 15px 20px 15px 20px;" onclick="leadController.page.showSectionById('estimateDetails')">
                                    <small class="pull-right">
                                        <i id="moveDetailsIcon" class="fa fa-arrow-down"></i>
                                    </small>
                                    <div id="moveDetailsSpan">More Details</div>
                                </div>

                                <div id="estimateDetails" class="padIt" style="display: none;">
                                    <div class="feed-element" style="border-bottom: none;">
                                        <div style="margin-bottom: 15px;">
                                            <small class="pull-right">
                                                <select id="userHandelingSelect" class="form-control inputer" <?php if($bouncer["isUserAnAdmin"] != "1"){ ?> disabled <?php } ?>>
                                                    <option value=""></option>
                                                </select>
                                            </small>
                                            <div>User Handeling</div>
                                        </div>
                                        <div style="margin-bottom: 15px;">
                                            <small class="pull-right">
                                                <select id="department" class="form-control inputer">
                                                    <option value="">All Departments</option>
                                                </select>
                                            </small>
                                            <div>Department</div>
                                        </div>
                                        <div style="margin-bottom: 15px;">
                                            <small class="pull-right">
                                                <select id="priority" class="form-control inputer">
                                                    <option value="1">Lowest</option>
                                                    <option value="2">Low</option>
                                                    <option value="3">Medium</option>
                                                    <option value="4">High</option>
                                                    <option value="5">Highest</option>
                                                </select>
                                            </small>
                                            <div>Priority</div>
                                        </div>
                                        <div style="margin-bottom: 15px;">
                                            <small class="pull-right">
                                                <div class="input-group inputer" style="padding: 0;">
                                                    <input type="text" id="reference" value="" class="form-control inputer">
                                                </div>
                                            </small>
                                            <div>Reference</div>
                                        </div>
                                    </div>
                                    <div class="feed-element" style="border: none;margin-top: 0px;">
                                        <div class="mobileOrder">
                                            <small class="pull-right mobileOrder2">
                                                <div id="boxDatePicker" class="form-control dater inputer">
                                                    <i class="fa fa-calendar"></i>
                                                    <span id="boxDate"></span><b class="caret"></b>
                                                </div>
                                            </small>
                                            <div class="mobileOrder1">
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" name="includeBoxDeliveryDate" id="includeBoxDeliveryDate">
                                                    <label for="includeBoxDeliveryDate" style="font-size: 12px;">Box Delivery Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mobileOrder">
                                            <small class="pull-right mobileOrder2">
                                                <div id="pickupDatePicker" class="form-control dater inputer">
                                                    <i class="fa fa-calendar"></i>
                                                    <span id="pickupDate"></span><b class="caret"></b>
                                                </div>
                                            </small>
                                            <div class="mobileOrder1">
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" name="includePickupDate" id="includePickupDate">
                                                    <label for="includePickupDate" style="font-size: 12px;">Pickup Date</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mobileOrder">
                                            <small class="pull-right mobileOrder2">
                                                <div id="requestedDeliveryDateStartPicker" class="form-control dater inputer">
                                                    <i class="fa fa-calendar"></i>
                                                    <span id="requestedDeliveryDateStart"></span><b class="caret"></b>
                                                </div>
                                            </small>
                                            <div class="mobileOrder1">
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" name="includeRequestDeliveryDate" id="includeRequestDeliveryDate">
                                                    <label for="includeRequestDeliveryDate" class="desktopLongText" style="font-size: 12px;">Requested Delivery Date</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feed-element" style="margin-top: 0px;border-bottom: none;">
                                        <div class="input-group" style="width: 100%;">
                                            <div>Notes <small style="font-size: 70%;color: #a0a0a0;float: right;">* Visible Only To Users In Organization</small></div>
                                            <textarea style="width: 100%;height: 71px;resize: vertical;margin-top: 13px;" id="leadNotes" placeholder="Notes.." class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="feed-element" style="padding-top: 18px;margin-top: 0;border: none;">
                                        <div style="text-align: center">
                                            <button id="saveLeftSide" class="btn btn-primary ladda-button" data-style="expand-right">Save Estimate Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab" id="tab-2">
                                <div class="feed-activity-list padIt">
                                    <div class="feed-element">
                                        <div>
                                            <small class="pull-right text-navy">
                                                <div class="switch">
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" name="vipLead" class="onoffswitch-checkbox" id="vipLead" onchange="leadController.lead.toggleVip()">
                                                        <label class="onoffswitch-label" for="vipLead">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </small>
                                            <div>Hot Lead</div>
                                        </div>
                                        <div>

                                        </div>
                                    </div>
                                    <div class="feed-element" style="border:none;">
                                        <small class="center-block">
                                            <div class="row">
                                                <div class="col-lg-12" id="leadSettingsButtons">

                                                </div>
                                            </div>
                                        </small>
                                    </div>
                                    <div class="feed-element" style="margin-top:0px; padding: 0px; border:none;">
                                        <small class="center-block">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-md-6 col-sm-12">
                                                        <button class="btn btn-primary btn-sm btn-block m-b-xs" onclick="leadController.tags.showTagsModal()">Lead Tags</button>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <button class="btn btn-success btn-sm btn-block m-b-xs" onclick="leadController.page.showReminderModal()">Follow Up Reminder</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </small>
                                    </div>
                                    <div class="feed-element" style="border: none;padding: 0;display: none;" id="leadRawPostDataDiv">
                                        <a style="color: unset;" onclick="leadController.lead.showRawPostData()"><small class="block text-muted"><i class="fa fa-code"></i> Show raw post data</small></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content" style="padding: 0;" id="leftPager">
                        <div class="myLinks active" id="left-1" onclick="leadController.page.setLeftLinks(1);">
                            Lead & Moving Details
                        </div>
                        <div class="padIt myLinks" id="left-2" onclick="leadController.page.setLeftLinks(2);">
                            Contact Client <span id="emailsTotal"></span>
                        </div>
                        <div class="padIt myLinks" id="left-3" onclick="leadController.page.setLeftLinks(3);">
                            Client Inventory <span id="inventoryTotal"></span><span id="inventoryCFLabel"></span>
                        </div>
                        <div class="padIt myLinks" id="left-4" onclick="leadController.page.setLeftLinks(4);">
                            Move Estimate <span id="estimateTotal"></span> <span id="estimateSign"></span>
                        </div>
                        <div class="padIt myLinks" id="left-9" onclick="leadController.page.setLeftLinks(9);">
                            Payments <span id="paymentsCount"></span>
                        </div>
                        <div class="padIt myLinks" id="left-5" onclick="leadController.page.setLeftLinks(5);" style="display: none;">
                            Client Updated Info <span id="newUpdatedData" class="label label-primary blinker pull-right" style="display: none;">Updated</span>
                        </div>
                        <div class="padIt myLinks" id="left-6" onclick="leadController.page.setLeftLinks(6);">
                            Files & Documents <span id="filesTotal" class="label label-primary pull-right"></span>
                        </div>
                        <div class="padIt myLinks" id="left-8" onclick="leadController.page.setLeftLinks(8);">
                            Claims <span id="claimsTotal" class="label label-primary pull-right"></span>
                        </div>
                        <div class="padIt myLinks" id="left-7" onclick="leadController.page.setLeftLinks(7);" style="display: none;">
                            Operations
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content" id="leadLogStreamContainer" style="padding: 13px;position: relative;min-height: 50px;max-height: 400px;overflow-y: scroll;">
                        <button id="addCustomLog" style="border-radius: 18px;top: 0px;float: right;right: 0;z-index: 99;position: sticky;" type="button" class="btn btn-default btn-sm" onclick="leadController.lead.addCustomLog()"><i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                        <div class="activity-stream" id="leadLogStream"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12" id="pageContainer">
                <div id="page-1">
                    <div id="map" style="display: none;"></div>
                    <!-- Lead Details -->
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row" id="leadDetails">

                                <h4 class="tabHeader" style="margin-left: 15px;">Lead Details</h4>

                                <span style="float: right;margin-top: -26px;margin-right: 9px;"><a onclick="$('#leadDetails').hide();$('#leadDetailsEdit').show();">Edit</a></span>

                                <div class="col-md-6" style="padding-left: 0px !important;">

                                    <div class="feed-element padIt">
                                        <div>
                                            <small id="leadFullName" class="pull-right text-navy shortenText"></small>
                                            <div class="text-bold">Full Name</div>
                                        </div>
                                        <div>
                                            <small id="leadPhone" class="pull-right text-navy shortenText"></small>
                                            <div class="text-bold">Phone Number</div>
                                        </div>
                                        <div>
                                            <small id="leadSecondPhone" class="pull-right text-navy shortenText"><span style="color: gray;"></span></small>
                                            <div class="text-bold">Secondary Phone</div>
                                        </div>
                                        <div>
                                            <small id="leadEmail" class="pull-right text-navy shortenText"></small>
                                            <div class="text-bold">Email</div>
                                        </div>
                                        <div>
                                            <small id="leadProvider" class="pull-right text-navy shortenText"></small>
                                            <div class="text-bold">Provider</div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6" style="padding-left: 0px !important;">

                                    <div class="feed-element padIt">
                                        <div>
                                            <small id="leadIp" class="pull-right text-navy"></small>
                                            <div class="text-bold">IP</div>
                                        </div>
                                        <div>
                                            <div class="text-bold">Lead Comments</div>
                                            <div id="leadComment" class="well"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="leadDetailsEdit" class="col-md-12 marginRow" style="float: none;padding-top: 19px;margin-top: 13px;display:none">

                                <span style="float: right;margin-top: -33px;margin-right: -19px;"><a onclick="$('#leadDetails').show();$('#leadDetailsEdit').hide();">Back</a></span>

                                <div class="row">
                                    <div class="col-md-6 left-details">
                                        <div class="form-group">
                                            <input type="text" id="leadEditFirstname" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="leadEditFirstname">First Name</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 right-details">
                                        <div class="form-group">
                                            <input type="text" id="leadEditLastname" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="leadEditLastname">Last Name</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 left-details">
                                        <div class="form-group">
                                            <input type="text" id="leadEditPhone" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="leadEditPhone">Phone Number</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 right-details">
                                        <div class="form-group">
                                            <input type="text" id="leadEditPhone2" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="leadEditPhone2">Secondary Phone</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <input type="email" id="leadEditEmail" onkeyup="this.setAttribute('value', this.value);" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="leadEditEmail">Email</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <input type="text" id="leadEditProviderName" class="form-control changable labelInput" value="" onkeyup="$(this).val('Manual')" disabled="disabled">
                                            <label class="labelInputPlaceholder" for="leadEditProviderName" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Provider</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <input type="text" id="leadEditIpAddress" class="form-control changable labelInput" value="" disabled="disabled">
                                            <label class="labelInputPlaceholder" for="leadEditIpAddress">IP</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <textarea id="leadEditComments" class="form-control changable labelInput" style="height: 93px;resize: none;" required=""></textarea>
                                            <label class="labelInputPlaceholder" for="leadEditComments" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Lead Comments</label>
                                            <small style="font-size: 70%;color: #a0a0a0;">* Comments are visible to client</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <button id="leadEditBtn" class="btn btn-primary pull-right btn-m ladda-button">Save Lead Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Lead Details -->

                    <!-- Move Details -->
                    <div class="ibox">
                        <div class="ibox-content">
                            <div id="moveDetails">
                                <h4 class="tabHeader">Move Details</h4>

                                <span style="float: right;margin-top: -26px;margin-right: 9px;"><a onclick="$('#moveDetails').hide();$('#moveDetailsEdit').show();">Edit</a></span>

                                <div class="row">
                                    <div class="col-md-6" style="padding-left: 0px !important;">

                                        <div class="feed-element padIt">
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingFrom"></small>
                                                <div class="text-bold">Moving From</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingFromAddress"></small>
                                                <div class="text-bold">Moving From Address</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingFromLevel"></small>
                                                <div class="text-bold">Moving From Level</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingFromFloor"></small>
                                                <div class="text-bold">Moving From Floor</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingFromApt"></small>
                                                <div class="text-bold">Moving From Apartment</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingFromAptType"></small>
                                                <div class="text-bold">Moving From Apartment Type</div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6" style="padding-left: 0px !important;">

                                        <div class="feed-element padIt">
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingTo"></small>
                                                <div class="text-bold">Moving To</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingToAddress"></small>
                                                <div class="text-bold">Moving To Address</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingToLevel"></small>
                                                <div class="text-bold">Moving To Level</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingToFloor"></small>
                                                <div class="text-bold">Moving To Floor</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingToApt"></small>
                                                <div class="text-bold">Moving To Apartment</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingToAptType"></small>
                                                <div class="text-bold">Moving To Apartment Type</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr style="margin: 1px;">
                                <div class="row">
                                    <div class="col-md-12" style="padding-left: 0px !important;">

                                        <div class="feed-element padIt">
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingDate"></small>
                                                <div class="text-bold">Pickup Date</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingType"></small>
                                                <div class="text-bold">Type Of Move</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingSizeNumber"></small>
                                                <div class="text-bold" id="moveSizeNumberLabel">Move Size</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingSize"></small>
                                                <div class="text-bold">Move Size Text</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr style="margin: 1px;">
                                <div class="row">
                                    <div class="col-md-12" style="padding-left: 0px !important;">

                                        <div class="feed-element padIt">
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="movingAutoTransport"></small>
                                                <div class="text-bold">Auto transport</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="onSiteEstimator"></small>
                                                <div class="text-bold">On site estimator</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="bindingText"></small>
                                                <div class="text-bold">Binding Type</div>
                                            </div>
                                            <div>
                                                <small class="pull-right text-navy shortenText" id="needStorageText"></small>
                                                <div class="text-bold">Need storage</div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                            <div id="moveDetailsEdit" class="col-md-12 marginRow" style="float: none;padding-top: 19px;margin-top: 13px;display:none">
                                <span style="float: right;margin-top: -33px;margin-right: -19px;"><a onclick="$('#moveDetailsEdit').hide();$('#moveDetails').show();">Back</a></span>
                                <div id="leadDetailsColsWrapper" style="display: flex;justify-content: space-between;">
                                    <div id="left" style="width:48%;">

                                        <div id="leadDetailsFromCity" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromCity" class="form-control changable labelInput" value="" required="" autocomplete="false">
                                            <label class="labelInputPlaceholder" for="moveEditFromCity">From City</label>
                                        </div>
                                        <div id="leadDetailsFromState" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromState" class="form-control changable labelInput" value="" required="" autocomplete="false">
                                            <label class="labelInputPlaceholder" for="moveEditFromState">From State</label>
                                        </div>
                                        <div id="leadDetailsFromZip" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromZip" class="form-control changable labelInput" value="" required="" autocomplete="false">
                                            <label class="labelInputPlaceholder" for="moveEditFromZip">From Zip</label>
                                        </div>
                                        <div id="leadDetailsFromAddress" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromAddress" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditFromAddress">From Address</label>
                                        </div>
                                        <div id="leadDetailsFromLevel" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromLevel" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditFromLevel">From Level</label>
                                        </div>
                                        <div id="leadDetailsFromFloor" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromFloor" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditFromFloor">From Floor</label>
                                        </div>
                                        <div id="leadDetailsFromApartment" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditFromApartment" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditFromApartment">From Apartment</label>
                                        </div>
                                        <div id="leadDetailsFromApartmentType" class="form-group editLeadDetailsInputLabel">
                                            <select class="form-control changable labelInput" id="moveEditFromApartmentType">
                                                <option></option>
                                                <option value="Apartment building">Apartment building</option>
                                                <option value="Private House">Private House</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            <label class="labelInputPlaceholder" for="moveEditFromApartmentType">From Apartment Type</label>
                                        </div>


                                    </div>
                                    <div id="right" style="width:48%;">

                                        <div id="leadDetailsToCity" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToCity" class="form-control changable labelInput" value="" required="" autocomplete="false">
                                            <label class="labelInputPlaceholder" for="moveEditToCity">To City</label>
                                        </div>
                                        <div id="leadDetailsToState" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToState" class="form-control changable labelInput" value="" required="" autocomplete="false">
                                            <label class="labelInputPlaceholder" for="moveEditToState">To State</label>
                                        </div>
                                        <div id="leadDetailsToZip" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToZip" class="form-control changable labelInput" value="" required="" autocomplete="false">
                                            <label class="labelInputPlaceholder" for="moveEditToZip">To Zip</label>
                                        </div>
                                        <div id="leadDetailsToAddress" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToAddress" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditToAddress">To Address</label>
                                        </div>
                                        <div id="leadDetailsToLevel" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToLevel" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditToLevel">To Level</label>
                                        </div>
                                        <div id="leadDetailsToFloor" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToFloor" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditToFloor">To Floor</label>
                                        </div>
                                        <div id="leadDetailsToApartment" class="form-group editLeadDetailsInputLabel">
                                            <input type="text" id="moveEditToApartment" class="form-control changable labelInput" value="" required="">
                                            <label class="labelInputPlaceholder" for="moveEditToApartment">To Apartment</label>
                                        </div>
                                        <div id="leadDetailsToApartmentType" class="form-group editLeadDetailsInputLabel">
                                            <select class="form-control changable labelInput" id="moveEditToApartmentType">
                                                <option></option>
                                                <option value="Apartment building">Apartment building</option>
                                                <option value="Private House">Private House</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            <label class="labelInputPlaceholder" for="moveEditToApartmentType">To Apartment Type</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <div style="margin-bottom: 15px;">
                                                <div id="moveEditDatePicker" class="form-control changable labelInput">
                                                    <i class="fa fa-calendar"></i>
                                                    <span id="moveEditDate"></span><b class="caret"></b>
                                                </div>
                                                <label class="labelInputPlaceholder" for="moveEditDate" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Pickup Date</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <select id="moveEditType" class="form-control changable labelInput">
                                                <option value="0">Local Moving</option>
                                                <option value="1">Long Distance</option>
                                            </select>
                                            <label class="labelInputPlaceholder" for="moveEditType" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Type Of Move</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <input type="number" id="moveEditSizeNumber" min="0" class="form-control changable labelInput" value="">
                                            <label class="labelInputPlaceholder" for="moveEditSizeNumber" id="moveEditSizeNumberLabel" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Move Size</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <input type="text" id="moveEditSize" class="form-control changable labelInput" value="">
                                            <label class="labelInputPlaceholder" for="moveEditSize" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Move Size Text</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <input type="text" id="onSiteEstimatorEdit" class="form-control changable labelInput" value="">
                                            <label class="labelInputPlaceholder" for="onSiteEstimatorEdit" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">On Site Estimator</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <select id="binding" class="form-control changable labelInput">
                                                <option value="">Not Selected</option>
                                                <option value="0">Non-Binding</option>
                                                <option value="1">Binding</option>
                                            </select>
                                            <label class="labelInputPlaceholder" for="binding" style="font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;margin-left: -11px;margin-top: -4px;">Binding Type</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 left-details">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-success">
                                                <input type="checkbox" name="needStorage" id="needStorage">
                                                <label for="needStorage">Need Storage</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <form id="autoTransportForm">
                                    <div id="autoTransport"></div>
                                </form>
                                <div class="row" style="margin-top: 9px;">
                                    <div class="col-md-12">
                                        <input type="button" class="btn btn-default btn-block" onclick="leadController.autoTransport.addVehicle()" value="Add Auto Transport">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 3%;">
                                    <div class="col-md-12">
                                        <button id="moveEditBtn" class="btn btn-primary btn-md ladda-button pull-right">Save Moving Details</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="page-2" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content" id="contactClientContent" style="display: none;">
                            <div id="newEmailTab" style="display: none;">

                            </div>
                            <div id="previewEmail" style="margin-top: 9px;display: none">
                                <input type="text" disabled id="previewSubject" class="form-control center-block">
                                <iframe id="previewContent" onload="iframeLoaded()" style="overflow-x: auto;width: 100%;border: 1px solid #d8d8d8;"></iframe>
                                <small id="emailPreviewData"></small>
                            </div>
                            <div id="emailErrorDiv" class="pull-left text-danger"></div>
                        </div>
                        <div class="ibox-footer" style="text-align: right;">
                            <div id="buttonsPage1" style="text-align: left; display: flex; flex-wrap: wrap; justify-content: space-between">
                                <div>
                                    <button class="btn btn-primary" id="newSMSButtonLabel" onclick="$('#open-sms-side').click();">Send SMS</button>
                                    <button class="btn btn-primary" id="newEmailButtonLabel" onclick="sendEmailsModal([leadId],null,null,leadController.contact.getLeadEmailsData)">Send Email</button>
                                </div>
                                <a onclick="copyUpdateInventoryLink()" style="margin-top: 7px;">Copy 'update inventory' link</a>
                            </div>
                            <div id="buttonsPage2" style="display: none;">
                                <button class="btn btn-white" onclick="leadController.page.change(1)">Go Back</button>
                            </div>
                        </div>
                    </div>
                    <div class="ibox" id="tableTab">
                        <div class="ibox-content">
                            <h2>Emails</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <th>Sent Time</th>
                                        <th>Subject</th>
                                        <th>To Email</th>
                                        <th>User Sent</th>
                                        <th>Opened</th>
                                        <th>Preview</th>
                                    </thead>
                                    <tbody id="tableBody">

                                    </tbody>
                                </table>
                            </div>
                            <p class="font-bold  alert alert-warning m-b-sm" id="emailHasBeenUnsubscribed" style="display: none">
                                Note: the email associated with this lead<span id="leadEmailThatUnsubscribed" style="font-weight: bold; text-decoration: underline"></span> has been unsubscribed and will not be able to receive any further emails from you.
                            </p>
                        </div>
                    </div>
                </div>
                <div id="page-3" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content" style="overflow-x: scroll;">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="tabHeader" style="margin-bottom: 15px;">Inventory</h4>
                                    <table class="table table-bordered">
                                        <thead>
                                            <th>Title</th>
                                            <th>Cubic Feet</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </thead>
                                        <tbody id="itemsTable">
                                            <tr id="noItems">
                                                <td colspan="4" style="text-align: center;">No Items Yet</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4" id="tableTotalCf"></td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div id="updateInventoryEstimateBTNDiv" class="pull-right" style="display: none;">
                                        Do you want to update the estimate size? <a onclick="leadController.moving.inventory.updateEstimateWeightByInventory()">Yes</a> | <a onclick="leadController.moving.inventory.askToUpdateEstimateInventory(false)">No</a>
                                    </div>
                                    <div id="updateInventoryBTNDiv" class="pull-right">
                                        <button class="btn btn-primary" onclick="leadController.moving.inventory.updateInventoryEstimate()">Save Inventory</button>
                                    </div>
                                    <div id="clearInventoryBTNDiv" class="pull-left">
                                        <button class="btn btn-danger" onclick="leadController.moving.inventory.clearInventoryEstimate()">Clear Inventory</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="tabHeader" style="margin-bottom: 15px;">Add items to inventory</h4>
                                </div>
                                <div class="col-md-12" id="selectorGroup">
                                    <input onkeyup="leadController.moving.inventory.searchItem(this)" class="form-control" type="text" name="itemSearch" id="itemSearch" placeholder="Search In Items">
                                    <select id="inventoryGroup" class="form-control" style="margin-top: 5px;">
                                        <option value="0">Select Items Group</option>
                                    </select>
                                    <div id="inventoryItemsList">

                                    </div>
                                    <div id="inventoryItemsSearch" style="margin-top: 4px;">

                                    </div>
                                    <a class="pull-right" onclick="leadController.moving.inventory.changeView(2)" style="margin-bottom: 15px;margin-top: 15px;position: relative;z-index: 99;" id="addItem">Cannot find your item? Add manually</a>
                                </div>
                                <div class="col-md-12" id="addNewItems" style="display: none;">
                                    <div class="form-group">
                                        <input type="text" id="newItemTitle" placeholder="Item Name" class="form-control" style="margin-bottom: 5px;">
                                        <input type="number" id="newItemCF" placeholder="Item Cubic Feet" class="form-control" style="margin-bottom: 5px;">
                                        <small id="errorSmall" class="text-danger"></small>
                                        <div class="pull-right">
                                            <input type="button" id="backToInventory" onclick="leadController.moving.inventory.changeView(1)" value="Back" class="btn btn-default">
                                            <input type="button" id="saveItem" onclick="leadController.moving.inventory.addNewItem()" value="Add New Item" class="btn btn-primary">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="page-4" style="display: none;">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist" id="estimatesTabs">
                        </ul>
                        <ul class="nav nav-tabs" role="tablist" style="float: right;position: absolute;top: -1px;right: 13px;" id="createEstimateUL">
                            <li class="active" style="float: right;"><a style="cursor: pointer;" aria-expanded="false" onclick="leadController.moving.estimate.createNewEstimateTab(null);"> <i id="moveDetailsIcon" class="fa fa-plus"></i> </a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" id="tab-1" class="tab-pane active">
                                <div class="panel-body" style="padding: 0px;margin-bottom: 30px;">


                                    <div class="ibox" style="border: none;margin-bottom: 0px;">
                                        <div class="ibox-content" id="estimatesTab">
                                            <div class="estimateRow" id="estimateCalcTab1">
                                                <div class="alert alert-danger" id="unActiveEstimateAlert" style="display: none;">
                                                    This estimate is disabled.<br>
                                                    The client can still see it, but not sign it.
                                                </div>
                                                
                                                <div style="margin-left: 0px;padding-top: 9px;">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Initial Price</label>
                                                        <div class="col-sm-9 estimateInputContainer">
                                                            <div class="input-group m-b initalPriceInputs" style="width: 34%;display: inline-table;">
                                                                <input class="form-control estimateChangable" id="totalCF" name="totalCF" type="number" value="" step="1" min="0" onchange="leadController.moving.estimate.fvp.setFVPAOL()">
                                                                <span class="input-group-addon" style="padding: 0">
                                                    <select id="estimateCForLBS" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;" onchange="leadController.moving.estimate.fvp.setFVPAOL()">
                                                        <option value="0">LBS</option>
                                                        <option value="1">CF</option>
                                                        <option value="2">Hours</option>
                                                        <option value="3">USD</option>
                                                    </select>
                                                </span>
                                                            </div>
                                                            <div class="input-group m-b initalPriceInputs" style="width: 34%;display: inline-table;" id="perCFdiv">
                                                                <input class="form-control estimateChangable" id="perCF" name="perCF" type="number" value="0" step="0.1" min="0">
                                                                <span class="input-group-addon" id="estimateCForLBS2">$ per cf</span>
                                                            </div>

                                                            <div id="startInitialWrapper" style="width: 28%;float: right;margin-right: 3px;">
                                                                <input type="text" class="form-control estimateChangable keepDisabled" id="startInitial" disabled="disabled" name="startInitial" style="background-color: #efefef; display: inline-table; width: 70px;float: right; " min="0" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table;">$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Fuel Surcharge</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" id="fuelSurchargeWrapper" style="width: 34%;">
                                                                <input class="form-control estimateChangable" id="fuelSurcharge" name="fuelSurcharge" type="number" step="0.1" min="0" max="100" value="">
                                                                <span class="input-group-addon" style="padding: 0;">
                                                    <select id="estimateFuelType" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;">
                                                        <option value="0">%</option>
                                                        <option value="1">USD</option>
                                                    </select>

                                                </span>
                                                            </div>
                                                            <div style="width: 35%;float: right;margin-top: -46px;margin-right: 3px;">
                                                                <input type="text" class="form-control estimateChangable keepDisabled" id="totalFuelSurcharge" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">$</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="hr-line-dashed" style="margin-top: 0px;margin-bottom: 0px;"></div>
                                                    <div style="background-color: #eaeaea75;height: 21px;">
                                        <span class="checkbox checkbox-success" style="float:left;display: inline-flex;margin-left: 5px;">
                                            <input type="checkbox" id="packersToggle" onchange="leadController.moving.estimate.togglePackers()" style="height: 12px;margin-right: 5px;"><label for="packersToggle">Add Packers</label>
                                        </span>
                                                        <label style="float: right;margin-right: 13px;color: #4CAF50;">Subtotal : <span id="subTotal1"></span></label>
                                                    </div>
                                                    <div class="hr-line-dashed" style="margin-top: 0px;"></div>
                                                    <div class="row" id="packingRowDiv">
                                                        <label class="col-sm-3 control-label">Packing</label>
                                                        <div class="col-sm-9 estimateInputContainer">
                                                            <div class="input-group m-b packingInputWarpper" style="width: 25%;display: inline-table;">
                                                                <input class="form-control estimateChangable" style="padding: 4px 5px;" id="packers" disabled min="0" name="packers" type="number" step="1" value="0">
                                                                <span class="input-group-addon packingBtnSpan" style="font-size: 11px;">Packers</span>
                                                            </div>
                                                            <div class="input-group m-b packingInputWarpper" style="width: 25%;display: inline-table;">
                                                                <input class="form-control estimateChangable" style="padding: 4px 5px;" id="packersHrs" disabled min="0" name="packersHrs" type="number" step="0.5" value="0">
                                                                <span class="input-group-addon packingBtnSpan" style="font-size: 11px;">hrs</span>
                                                            </div>
                                                            <div class="input-group m-b packingInputWarpper" style="width: 25%;display: inline-table;">
                                                                <input class="form-control estimateChangable" style="padding: 4px 5px;" id="packersPerHrs" disabled min="0" name="packersPerHrs" type="number" step="0.1" value="0">
                                                                <span class="input-group-addon packingBtnSpan" style="font-size: 11px;">$ per hrs	</span>
                                                            </div>
                                                            <div id="totalPackingCostsWrapper" style="width: 23%;float: right; margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalPackingCosts" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="unpackingRowDiv">
                                                        <label class="col-sm-3 control-label">Unpacking</label>
                                                        <div class="col-sm-9 estimateInputContainer">
                                                            <div class="input-group m-b packingInputWarpper" style="width: 25%;display: inline-table;">
                                                                <input class="form-control estimateChangable" style="padding: 4px 5px;" id="unpackers" disabled min="0" name="unpackers" type="number" step="1" value="0">
                                                                <span class="input-group-addon packingBtnSpan" style="font-size: 11px;">Packers</span>
                                                            </div>
                                                            <div class="input-group m-b packingInputWarpper" style="width: 25%;display: inline-table;">
                                                                <input class="form-control estimateChangable" style="padding: 4px 5px;" id="unpackersHrs" disabled min="0" name="unpackersHrs" type="number" step="0.5" value="0">
                                                                <span class="input-group-addon packingBtnSpan" style="font-size: 11px;">hrs</span>
                                                            </div>
                                                            <div class="input-group m-b packingInputWarpper" style="width: 25%;display: inline-table;">
                                                                <input class="form-control estimateChangable" style="padding: 4px 5px;" id="unpackersPerHrs" disabled min="0" name="unpackersPerHrs" type="number" step="0.1" value="0">
                                                                <span class="input-group-addon packingBtnSpan" style="font-size: 11px;">$ per hrs	</span>
                                                            </div>
                                                            <div id="totalUnPackingCostsWrapper" style="width: 23%;float: right; margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalUnPackingCosts" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hr-line-dashed" style="margin-top: 0px;margin-bottom: 0px;"></div>
                                                    <div style="background-color: #eaeaea75;height: 21px;">
                                                        <label style="float: right;margin-right: 13px;color: #4CAF50;">Subtotal : <span id="subTotal2"></span></label>
                                                    </div>
                                                    <div class="hr-line-dashed" style="margin-top: 0px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Agent Fee</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" style="width: 190px;display: inline-table;">
                                                                <input class="form-control estimateChangable" id="agentFee" name="agentFee" type="number" step="0.5" min="0" max="100" value="0">
                                                                <span class="input-group-addon" style="padding: 0;">
                                                                  <select id="agentFeeType" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;">
                                                                    <option value="0">%</option>
                                                                    <option value="1">USD</option>
                                                                </select>
                                                                </span>
                                                            </div>
                                                            <div style="width: 93px;float: right;margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalAgentFee" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hr-line-dashed" style="margin: 0px;"></div>
                                                    <div class="row" style="margin: 0px;">
                                                        <div id="extraCharges"></div>
                                                        <div id="movingMaterials"></div>
                                                    </div>

                                                    <div class="hr-line-dashed" style="margin-top: 0px;margin-bottom: 0px;"></div>
                                                    <div style="background-color: #eaeaea75;height: 21px;">
                                                         <span class="checkbox checkbox-success" style="float:left;display: inline-flex;margin-left: 5px;">
                                                             <input type="checkbox" id="fvpToggle" onchange="leadController.moving.estimate.fvp.toggleFVP()" style="height: 12px;margin-right: 5px;"><label for="fvpToggle">Add Value Protection</label>
                                                         </span>
                                                        <label style="float: right;margin-right: 13px;color: #4CAF50;">Subtotal : <span id="subTotal3"></span></label>
                                                    </div>

                                                    <div class="hr-line-dashed" style="margin-top: 0px;"></div>
                                                    <div class="row divFeatureDisabled" id="FVProwDiv">
                                                        <div class="col-sm-3">
                                                            <div class="radio">
                                                                <input class="estimateChangable" type="radio" name="FVPoption" id="valueProtectionType1" value="true" checked="" onchange="leadController.moving.estimate.fvp.changeFVPType()">
                                                                <label for="valueProtectionType1">
                                                                    Full Value Protection
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <input class="estimateChangable" type="radio" name="FVPoption" id="valueProtectionType2" value="false" checked="" onchange="leadController.moving.estimate.fvp.changeFVPType()">
                                                                <label for="valueProtectionType2">
                                                                    Release Value
                                                                    <span style="display: block" class="small">(60-cents option)</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-9" style="display: flex;flex-wrap:wrap;">
                                                            <div style="overflow: scroll">
                                                            <div class="input-group m-b" style="width: 190px;display: inline-table;">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Amount of Liability</th>
                                                                        <th>Deductible Level</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon">$</span>
                                                                                <input type="tel" value="0.00" id="valueProtectionAOL" name="valueProtectionAOL" class="form-control input-sm estimateChangable" style="width: 100px;" />
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" id="resetFVPAOL_POPUP" class="btn btn-default btn-sm" onclick="leadController.moving.estimate.fvp.setFVPAOL()" style="line-height: 1.45;"  data-toggle="popover" data-container="body" data-placement="auto top" data-trigger="hover" data-content="This will re-calculate the amount of liability based on the estimate initial CF/LBS"><i class="fa fa-undo"></i></button>
                                                                                </span>
                                                                            </div>



                                                                        </td>
                                                                        <td><div id="FVPdaList" class="" style="display: flex"></div></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                            <div style="width: 93px;float: right;margin-right: 3px;display: flex;flex-direction: row-reverse;margin-left:auto;">
                                                                <input type="text" class="form-control keepDisabled" id="valueProtectionCharge" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="background-color: #eaeaea75;height: 21px;">
                                                         <span class="checkbox checkbox-success" style="float:left;display: inline-flex;margin-left: 5px;">
                                                            <input type="checkbox" id="discountsToggle" onchange="leadController.moving.estimate.toggleDiscounts(this)" style="height: 12px;margin-right: 5px;"><label for="discountsToggle">Add Discounts</label>
                                                        </span>
                                                        <label style="float: right;margin-right: 13px;color: #4CAF50;">Subtotal : <span id="subTotal4"></span></label>
                                                    </div>
                                                    <div class="hr-line-dashed" style="margin-top: 0px;"></div>
                                                    <div class="row" id="generalDiscountRowDiv">
                                                        <label class="col-sm-3 control-label">Discount</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" style="width: 190px;display: inline-table;">
                                                                <input class="form-control estimateChangable" id="generalDiscount" disabled name="generalDiscount" type="number" step="0.5" min="0" max="100" value="0">
                                                                <span class="input-group-addon" style="padding: 0;">
                                                                    <select id="generalDiscountType" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;">
                                                                        <option value="0">%</option>
                                                                        <option value="1">USD</option>
                                                                    </select>
                                                                </span>

                                                            </div>
                                                            <div style="width: 93px;float: right;margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalGeneralDiscount" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">-$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="couponDiscountRowDiv">
                                                        <label class="col-sm-3 control-label">Coupon Discount</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" style="width: 190px;">
                                                                <input class="form-control estimateChangable" id="couponDiscount" disabled name="couponDiscount" type="number" step="0.5" min="0" max="100" value="0">
                                                                <span class="input-group-addon" style="padding: 0;">
                                                                     <select id="couponDiscountType" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;">
                                                                        <option value="0">%</option>
                                                                        <option value="1">USD</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                            <div style="width: 93px;float: right;margin-top: -46px;margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalCouponDiscount" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">-$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="seniorDiscountRowDiv">
                                                        <label class="col-sm-3 control-label">Senior Citizen Discount</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" style="width: 190px;">
                                                                <input class="form-control estimateChangable" type="number" id="seniorDiscount" disabled name="seniorDiscount" step="0.5" min="0" max="100" value="0">
                                                                <span class="input-group-addon" style="padding: 0;">
                                                                      <select id="seniorDiscountType" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;">
                                                                        <option value="0">%</option>
                                                                        <option value="1">USD</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                            <div style="width: 93px;float: right;margin-top: -46px;margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalSeniorDiscount" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">-$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="veteranDiscountRowDiv">
                                                        <label class="col-sm-3 control-label">Veteran Discount</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" style="width: 190px;">
                                                                <input class="form-control estimateChangable" type="number" id="veteranDiscount" disabled name="veteranDiscount" step="0.5" min="0" max="100" value="0">
                                                                <span class="input-group-addon" style="padding: 0;">
                                                                      <select id="veteranDiscountType" class="estimateChangable" style="border: unset;background: transparent;height: 27px;font-size: 12px;">
                                                                        <option value="0">%</option>
                                                                        <option value="1">USD</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                            <div style="width: 93px;float: right;margin-top: -46px;margin-right: 3px;">
                                                                <input type="text" class="form-control keepDisabled" id="totalVeteranDiscount" disabled="disabled" style="background-color: #efefef; display: inline-table; width: 70px;float: right;" />
                                                                <label style="float: right;margin-right: 3px;margin-top: 1px;font-size: 19px;font-weight: normal; display: inline-table; float: right;">-$</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="background-color: #eaeaea75;height: 21px;">
                                                        <label style="float: right;margin-right: 13px;color: #4CAF50;">Subtotal : <span id="subTotal5"></span></label>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Additional info</label>
                                                            <textarea class="form-control" id="estimateComments" name="estimateComments" placeholder="Additional info.." style="height: 73px;max-height: 130px;resize: vertical;"></textarea>
                                                        </div>
                                                    </div>
                                                    <div id="estimateAttachedFileDiv" style="margin-top: 13px;padding-top: 20px;border-top: 1px dashed #e7eaec;">
                                                        <label>Attached files</label>
                                                        <div id="estimateAttachedFile"></div>
                                                    </div>
                                                    <div class="hr-line-dashed" style=""></div>
                                                    <div class="row" style="display:none" id="needStorageEstimate">
                                                        <label class="col-sm-3 control-label">Monthly Storage Fee</label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group m-b" style="width: 190px;display: inline-table;">
                                                                <input class="form-control estimateChangable" id="monthlyStorageFee" name="monthlyStorageFee" type="number" step="1" value="0">
                                                                <span class="input-group-addon">$</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hr-line-dashed" style="margin-top: 0px;margin-bottom: 0px;"></div>
                                                    <div style="background-color: #eaeaea75;height: 40px;">
                                                        <label style="float: right;margin-top: 11px;margin-right: 13px;color: #4CAF50;">Total Estimate : <span id="TotalEstimate"></span></label>
                                                    </div>
                                                    <div class="row" style="margin-right: 3px;" id="signArea"></div>
                                                    <div class="hr-line-dashed" style="margin-top: 15px;margin-bottom: 15px;"></div>
                                                    <div class="row" id="ipArea" style="padding: 0 0 20px 20px;"></div>
                                                    <div class="estimateActionButtons">
                                                        <button type="button" class="btn btn-sm btn-default" style="order:1;" onclick="sendEmailsModal([leadId],null,[1,leadId,movingLeadData.estimates[currentEstimate].id],leadController.contact.getLeadEmailsData)">Send Estimate</button>
                                                        <button type="button" class="btn btn-sm btn-default" style="order:2;" onclick="openUpdateInventoryLink()" id="openEstimateBTN">Open Estimate</button>
                                                        <button type="button" class="btn btn-sm btn-default" style="order:3;" onclick="leadController.moving.estimate.createExtra()" id="extraChargeBtn">Add Extra Charge</button>
                                                        <button type="button" class="btn btn-sm btn-default" style="order:4;" onclick="leadController.moving.estimate.addMaterial()" id="addMaterialBtn">Add Material</button>
                                                        
                                                        <form method="post" id="estimateFileForm" enctype="multipart/form-data" style="order:5;display: inline-table">
                                                             <button type="button" onclick="$('#estimateFile').click();" class="btn btn-sm btn-default btn-file ladda-button" id="attachFileBTN" data-spinner-color="#676a6c" data-style="expand-right">Attach file</button>
                                                            <input style="display: none;" type="file" name="estimateFile" id="estimateFile" onchange="$('#estimateFileForm').submit();" />
                                                        </form>

                                                        <button type="button" class="btn btn-sm btn-default" style="order:6;" onclick="sendEmailsModal([leadId],null,[3,leadId,movingLeadData.estimates[currentEstimate].id],leadController.contact.getLeadEmailsData)" id="sendInvoiceBTN">Send Invoice</button>

                                                        <button type="button" class="btn btn-primary pull-right" style="order:0;margin-right: 3px;" onclick="leadController.moving.estimate.saveCalc()" id="saveBtn">Save</button>

                                                    </div>
                                                </div>
                                            </div>
                                            <div id="estimateCalcTab" style="display: none;"></div>
                                        </div>
                                        <div onclick="$('#estimateSettingsIcon').toggleClass('fa-chevron-down');$( '#estimateSettingsDiv' ).slideToggle();" style="cursor:pointer;background-color: whitesmoke;border-top: 1px solid gainsboro;padding: 7px 20px 7px 20px;/* border-bottom: 1px solid gainsboro; */text-align: right;">
                                            <div class="row">
                                                <div class="col-md-12" style="text-align: left">
                                                    <h3 style="margin: 4px;">Estimate Settings
                                                        <i class="fa fa-chevron-down fa-chevron-up pull-right" id="estimateSettingsIcon" style="cursor:pointer;"></i>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="estimateSettingsDiv" style="display:none;background-color: whitesmoke;border-top: 1px solid gainsboro;padding: 7px 20px 7px 20px;border-bottom: 1px solid gainsboro;text-align: right;">
                                            <div class="row">
                                                <div class="col-md-12" style="text-align: left">
                                                    <span class="checkbox checkbox-success" style="display: flex;margin: 0px;">
                                                        <input type="checkbox" id="showEstimateTerms" checked style="height: 12px;margin-right: 5px;">
                                                        <label for="showEstimateTerms" class="regularCheck">Show terms and conditions section in estimate page</label>
                                                        <span style="cursor:pointer;margin-left: 3px;" onclick="leadController.page.showTermsInEstimatePageInfo()"><i style="font-size: 16px;margin-top: 2px;"class="fa fa-info-circle"></i></span>
                                                    </span>
                                                    <span class="checkbox checkbox-success" style="display: flex;margin: 0px;margin-top: 3px;">
                                                            <input type="checkbox" id="showEstimateSign" checked style="height: 12px;margin-right: 5px;">
                                                            <label for="showEstimateSign" class="regularCheck">Allow E-signature in estimate page</label>

                                                            <span style="cursor:pointer;margin-left: 3px;" onclick="leadController.page.showESignatueInEstimatePageInfo()"><i style="font-size: 16px;margin-top: 2px;" class="fa fa-info-circle"></i></span>
                                                    </span>
                                                    <span class="checkbox checkbox-success" style="display: flex;margin: 0px;margin-top: 3px;">
                                                            <input type="checkbox" id="showEstimateInventory" checked style="height: 12px;margin-right: 5px;">
                                                            <label for="showEstimateInventory" class="regularCheck">Display inventory in estimate page</label>
                                                            <span  style="cursor:pointer;margin-left: 3px;" onclick="leadController.page.showInventoryInEstimatePageInfo()"><i style="font-size: 16px;margin-top: 2px;" class="fa fa-info-circle"></i></span>
                                                    </span>
                                                    <span class="checkbox checkbox-success" style="display: flex;margin: 0px;margin-top: 3px;">
                                                            <input type="checkbox" id="showEstimatePayments" checked style="height: 12px;margin-right: 5px;">
                                                            <label for="showEstimatePayments" class="regularCheck">Display payments in estimate page</label>
                                                            <span style="cursor:pointer;margin-left: 3px;" onclick="leadController.page.showPaymentsInEstimatePageInfo()"><i style="font-size: 16px;margin-top: 2px;" class="fa fa-info-circle"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>





                </div>
                <div id="page-5" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <h4 class="tabHeader" style="margin-left: 15px;">Updated Lead Details</h4>
                                <div class="col-md-6" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Firstname</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFirstname"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFirstname">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Phone</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updatePhone"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdatePhone">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Lastname</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateLastname"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateLastname">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Email</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateEmail"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateEmail">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Comments</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateComments"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateComments">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-left: 0px !important;"></div>
                                <div class="col-md-6" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div>
                                            <button class="btn btn-success pull-right btn-update" onclick="leadController.lead.updateLeadInfo()" id="btnUpdateLeadDetails">Update All Lead Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <h4 class="tabHeader" style="margin-left: 15px;">Updated Move Details</h4>
                                <div class="col-md-6" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From State</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromState"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFState">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From City</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromCity"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFCity">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From Zip</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromZip"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFZip">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From Address</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromAddress"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFAddress">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From Level</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromLevel"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFLevel">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From Floor</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromFloor"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFFloor">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>From Apartment</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateFromApt"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateFApt">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Move Date</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateMoveDate"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateMoveDate">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To State</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToState"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTState">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To City</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToCity"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTCity">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To Zip</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToZip"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTZip">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To Address</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToAddress"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTAddress">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To Level</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToLevel"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTLevel">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To Floor</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToFloor"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTFloor">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>To Apartment</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateToApt"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateTApt">Update</button>
                                            </div>
                                        </div>
                                        <div class="updatedBtnAndSmallWrapper">
                                            <div>Need Storage</div>
                                            <div class="updatedBtnAndSmall">
                                                <small class="pull-right text-navy shortenText" id="updateNeedStorage"></small>
                                                <button class="btn btn-default pull-right btn-update" id="btnUpdateNeedStorage">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="    row">
                                <div class="    col-md-6" style="padding-left: 0px !important;"></div>
                                <div class="col-md-6" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt">
                                        <div>
                                            <button class="btn btn-success pull-right btn-update" onclick="leadController.lead.updateMovingInfo()" id="btnUpdateMovingDetails">Update All Moving Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row" id="updateInventory">
                                <h4 class="tabHeader" style="margin-left: 15px;">Updated Inventory</h4>
                                <div class="col-md-12" style="padding-left: 0px !important;">
                                    <div class="feed-element padIt table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <th>Item Name</th>
                                                <th>Cubic Feet</th>
                                                <th>Quantity</th>

                                            </thead>
                                            <tbody id="updateInventoryTable">

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3"><button onclick="leadController.moving.inventory.addToInventory();" class="btn btn-primary pull-right">Add Items To Inventory</button></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="page-6" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row" style="display: flex; padding: 0 15px;">

                                <h4 class="tabHeader" style="width: 100%;">Files & Documents</h4>
                                <div id="fileAndInfo" style="display: flex; width: 100%; justify-content: flex-end;">
                                    <button class="btn btn-primary pull-right" id="uploadBtn" onclick="$('#uploadFile').click(); $('#uploadBtn').text('Choosing a file');">Upload File</button>
                                    <form enctype="multipart/form-data" method="post" action="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/leads/uploadFile.php" id="uploadForm" style="display: none">
                                        <input type="file" id="uploadFile" name="file">
                                        <input type="hidden" id="leadId" name="leadId" value="<?= $leadId; ?>">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox">
                        <div class="ibox-footer">
                            <div class="row">
                                <div class="alert alert-info" style="margin: 19px;display: block;">These files will not be visible to your customer. If you would like a customer to see a file, you can attach it to the estimate.</div>
                                <div style="margin: 19px;" id="leadFiles"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="page-8" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content" style="overflow-x: scroll;">
                            <div class="row" style="padding: 0px 15px 15px 0px;">
                                <h4 class="tabHeader" style="margin-left: 15px; float:left;">Claims</h4>

                                <div class="dropdown" style="float: right;">
                                    <button class="btn btn-default" type="button" onclick="leadController.lead.claims.createClaim();">Create a claim</button>
                                </div>
                                <button style="display:none; color: white; margin-right: 1rem; float:right" class="btn btn-danger" id="deleteSelectedClaims" onclick="leadController.lead.claims.deleteSelected();"><i class="fa fa-trash-o"></i></button>
                            </div>
                            <div class="row" style="padding: 0px 15px 0px 15px;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 25px;"><input id="checkAllClaims" type="checkbox"></th>
                                        <th>Name</th>
                                        <th>Files</th>
                                        <th style="width: 123px;text-align: center">Status</th>
                                        <th style="width: 123px;text-align: center">Actions</th>
                                        <th style="width: 123px;text-align: center">View</th>
                                    </tr>
                                    </thead>
                                    <tbody id="claimsTable"></tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="page-7" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content" style="overflow-x: scroll;">
                            <div class="row" style="padding: 0px 15px 15px 0px;">
                                <h4 class="tabHeader" style="margin-left: 15px; float:left;">Job acceptance forms</h4>


                                <div class="dropdown" style="float: right;">
                                    <button class="btn btn-default" type="button" onclick="leadController.moving.operations.jobAcceptance.getSingleJobByLeadId();">Add this job to board</button>
                                </div>
                                <button style="display:none; color: white; margin-right: 1rem; float:right" class="btn btn-danger" id="deleteSelectedJobCarriers" onclick="leadController.moving.operations.jobAcceptance.deleteCarrierJob();"><i class="fa fa-trash-o"></i></button>
                            </div>
                            <div class="row" style="padding: 0px 15px 0px 15px;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 25px;"><input id="checkAllcarriers" type="checkbox"></th>
                                        <th>Carrier</th>
                                        <th style="width: 123px;text-align: center">Carrier Balance</th>
                                        <th style="width: 123px;text-align: center">Status</th>
                                        <th style="width: 123px;text-align: center">Actions</th>
                                        <th style="width: 123px;text-align: center">View</th>
                                    </tr>
                                    </thead>
                                    <tbody id="jobAcceptanceForms"></tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="page-9" style="display: none;">
                    <div class="ibox">
                        <div class="ibox-content" style="overflow-x: scroll;">

                            <div id="ccaf">
                                <div id="ccafTableWarpper" class="table-responsive">
                                    <h3>Credit card authorizations</h3>
                                    <table class="table table-bordered">
                                        <thead class="small">
                                        <th style="width: 73px;">Amount</th>
                                        <th style="width: 129px;">Name</th>
                                        <th style="width: 129px;">Date</th>
                                        <th></th>
                                        </thead>
                                        <tbody id="ccafTable"></tbody>
                                    </table>
                                </div>

                                <div id="payments" class="table-responsive">
                                    <h3>Payments</h3>
                                    <table class="table table-bordered">
                                        <thead class="small">
                                        <th style="width: 73px;">Amount</th>
                                        <th style="width: 129px;">Description</th>
                                        <th style="width: 129px;">Date</th>
                                        <th></th>
                                        </thead>
                                        <tbody id="paymentTable"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="feed-element" style="text-align: center;">
                                <button id="sendCCAF" class="btn btn-block btn-default" onclick="sendEmailsModal([leadId],null,[2,leadId],leadController.contact.getLeadEmailsData)" style="width: 47%;display: inline-table">Send CC Authorization</button>
                                <button id="addPayment" class="btn btn-block btn-primary" style="width: 47%;display: inline-table; margin-top: 0px;" onclick="leadController.lead.payments.addPayment()">Add Payment</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- =========== ESTIMATE BOTTOM SETTINGS MODALS ===========-->


    <div id="estimateSettingsShowTerms" style="display: none; text-align: left;max-width: 600px;">
        <div class="alert alert-info" style="margin-top: 20px;font-size: 14px;">
            By removing this selection the client will not be able to see the terms of use of this estimate
        </div>
        <img src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/images/other/estimateTerms-min.jpg" style="width: 100%;" />
    </div>

    <div id="estimateSettingsAllowEsignature" style="display: none; text-align: left;max-width: 600px;">
        <div class="alert alert-info" style="margin-top: 20px;font-size: 14px;">
            By removing this selection, the client will not be able to sign this estimate digitally
        </div>
        <img src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/images/other/estimateSignature-min.jpg" style="width: 100%;" />
    </div>

    <div id="estimateSettingsShowInventory" style="display: none; text-align: left;max-width: 600px;">
        <div class="alert alert-info" style="margin-top: 20px;font-size: 14px;">
            By removing this selection the client will not see the inventory list in the estimate
        </div>
        <img src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/images/other/estimateInventory-min.jpg" style="width: 100%;" />
    </div>

    <div id="estimateSettingsShowPayments" style="display: none; text-align: left;max-width: 600px;">
        <div class="alert alert-info" style="margin-top: 20px;font-size: 14px;">
            By removing this selection payments will not be displayed upon your sent estimates
        </div>
        <img src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/images/other/estimatePayments-min.jpg" style="width: 100%;" />
    </div>


    <!-- =========== ESTIMATE BOTTOM SETTINGS MODALS ===========-->


    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- System Functions -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<!-- Touchspin -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGYD6DNH69y897uXLi6cDvdjkQFh6DhjU&region=US&language=en"> </script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/vehicles.js"></script>

<!-- Context Menu -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/contextMenu/script.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/contextMenu/position.js"></script>

<!-- Lead -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/leads/emailTags.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/leads/lead.min.js?v=1.3.8.2"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/moment.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.min.js"></script>

<script>
    var orgName =  <?php echo "'" . $orgName . "'"; ?>;

     $("#estimateFileForm").submit(function(e) {
        e.preventDefault();

        var attachFileBTN = $("#attachFileBTN").ladda();
        attachFileBTN.ladda("start");

        var formData = new FormData(this);


        jQuery.ajax({
            url: '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leads/lead/uploadFileToEstimate.php',
            data: formData,
            type:"POST",
            error: function(xhr, textStatus, errorThrown){
                // Clear the file input
                $("#estimateFile").val("");
                attachFileBTN.ladda("stop");
                toastr.error("Couldn't add file","Failed");
            },
            async: true,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {

            // Clear the file input
            $("#estimateFile").val("");
            attachFileBTN.ladda("stop");

            // Handle the file uploaded
            try{
                data = JSON.parse(data);

                if(data.status == true){
                    for(var i = 0;i<data.files.length;i++){
                        leadController.moving.estimate.addFile(data.files[i]);
                    }
                }
                if(data.status == false){
                    // Handle errors

                    if(data.error == 1 || data.error == 4 || data.error == 5 || data.error == 6 || data.error == 7){
                        // general error
                        toastr.error("Couldn't add file","Failed");
                    }
                    if(data.error == 2){
                        // Sorry, file size too big
                        toastr.error("Sorry, file size too big","Failed");
                    }
                    if(data.error == 3){
                        // Sorry, only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed
                        toastr.error("Only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed","Failed");
                    }
                }
            }catch (e) {
                toastr.error("Couldnt add file","Failed");
            }
        });


        /*
        $.ajax({
            url: '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leads/lead/uploadFileToEstimate.php',
            type: 'POST',
            data: formData,
            success: function (data) {
                // Clear the file input
                $("#estimateFile").val("");
                attachFileBTN.ladda("stop");


                // Handle the file uploaded
                try{
                    data = JSON.parse(data);

                    if(data.status == true){
                        for(var i = 0;i<data.files.length;i++){
                            leadController.moving.estimate.addFile(data.files[i]);
                        }
                    }
                    if(data.status == false){
                        // Handle errors

                        if(data.error == 1 || data.error == 4 || data.error == 5 || data.error == 6){
                            // general error
                            toastr.error("Couldn't add file","Failed");
                        }
                        if(data.error == 2){
                            // Sorry, file size too big
                            toastr.error("Sorry, file size too big","Failed");
                        }
                        if(data.error == 3){
                            // Sorry, only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed
                            toastr.error("Only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed","Failed");
                        }
                    }
                }catch (e) {
                    toastr.error("Couldnt add file","Failed");
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
*/


    });

    $( document ).ready(function() {


        //Job acceptance forms delete
        $(document).on("change", "input[name='carrierCheckbox']", function () {
            var carrierJobCheckboxes = [];
            $.each($(".carrierCheckbox:checked"), function(){
                carrierJobCheckboxes.push($(this).val());
            });

            if(carrierJobCheckboxes.length == 0){
                $("#deleteSelectedJobCarriers").hide();
            }else {
                $("#deleteSelectedJobCarriers").show();
            }
        });

        $('#checkAllcarriers').change(function() {
            if(this.checked) {
                $( ".carrierCheckbox" ).each(function( index ) {
                    $(".carrierCheckbox").prop("checked", true);

                    $("#deleteSelectedJobCarriers").show();
                });
            }
            else if(this.checked == false) {
                $( ".carrierCheckbox" ).each(function( index ) {
                    $(".carrierCheckbox").prop("checked", false);
                    $("#deleteSelectedJobCarriers").hide();
                });
            }
        });


        $(document).on("change", "input[name='claimCheckbox']", function () {
            var claimsCheckboxes = [];
            $.each($(".claimCheckbox:checked"), function(){
                claimsCheckboxes.push($(this).val());
            });

            if(claimsCheckboxes.length == 0){
                $("#deleteSelectedClaims").hide();
            }else {
                $("#deleteSelectedClaims").show();
            }
        });

        $('#checkAllClaims').change(function() {
            if(this.checked) {
                $( ".claimCheckbox" ).each(function( index ) {
                    $(".claimCheckbox").prop("checked", true);

                    $("#deleteSelectedClaims").show();
                });
            }
            else{
                $( ".claimCheckbox" ).each(function( index ) {
                    $(".claimCheckbox").prop("checked", false);
                    $("#deleteSelectedClaims").hide();
                });
            }
        });
        //================================

        function initAutoComplete(id,type,input){
            var q = document.getElementById(id).value;
            $('#'+id).devbridgeAutocomplete({
                serviceUrl: '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leads/autoCompleteLocation.php?q='+q+"&type="+type+"&input="+input,
                onSelect: function (suggestion) {

                    if(type == "from"){

                        document.getElementById('moveEditFromState').value = suggestion.data.state;

                        if(input == "zip" || input == "city") {
                            document.getElementById('moveEditFromZip').value = suggestion.data.zip;
                            document.getElementById('moveEditFromCity').value = suggestion.data.city;
                        }

                    }
                    if(type == "to"){
                        document.getElementById('moveEditToState').value = suggestion.data.state;

                        if(input == "zip" || input == "city") {
                            document.getElementById('moveEditToZip').value = suggestion.data.zip;
                            document.getElementById('moveEditToCity').value = suggestion.data.city;
                        }

                    }

                    $('#'+id).devbridgeAutocomplete().disable();
                    document.getElementById(id).blur();
                    $('#'+id).devbridgeAutocomplete().enable();

                },
                noCache:true,
                preventBadQueries:true,
                forceFixPosition:true,
                showNoSuggestionNotice:true
            });
        }

        initAutoComplete('moveEditFromCity','from',"city");
        initAutoComplete('moveEditFromState','from',"state");
        initAutoComplete('moveEditFromZip','from',"zip");

        initAutoComplete('moveEditToCity','to',"city");
        initAutoComplete('moveEditToState','to',"state");
        initAutoComplete('moveEditToZip','to',"zip");
    });

    $("#includeBoxDeliveryDate").on("change",function () {
        if (document.getElementById("includeBoxDeliveryDate").checked == true){
            $("#boxDatePicker").removeClass("disabled-picker");
        }else{
            $("#boxDatePicker").addClass("disabled-picker");
        }
    });
    $("#includePickupDate").on("change",function () {
        if (document.getElementById("includePickupDate").checked == true){
            $("#pickupDatePicker").removeClass("disabled-picker");
        }else{
            $("#pickupDatePicker").addClass("disabled-picker");
        }
    });
    $("#includeRequestDeliveryDate").on("change",function () {
        if (document.getElementById("includeRequestDeliveryDate").checked == true){
            $("#requestedDeliveryDateStartPicker").removeClass("disabled-picker");
        }else{
            $("#requestedDeliveryDateStartPicker").addClass("disabled-picker");
        }
    });

    function copyUpdateInventoryLink() {

        var container = document.getElementsByTagName("BODY")[0];
        var el = document.createElement('textarea');
        el.value = "<?php echo $_SERVER["YMQ_URL"]; ?>/"+leadData.leadData.secretKey;

        container.appendChild(el);
        el.select();
        document.execCommand('copy');
        container.removeChild(el);

        toastr.success("Copied","Link Copied");
    }

    function openUpdateInventoryLink() {

        var link = "<?php echo $_SERVER["YMQ_URL"]; ?>/leads.php?key="+leadData.leadData.secretKey+"&page=estimates";

        window.open(link);
    }

    $("#resetFVPAOL_POPUP").popover({
        template: '<div class="popover resetFVPAOL_POPUP" role="tooltip"><div class="arrow" style="left: 50%;"></div><h3 class="popover-title" style="display: none;"></h3><div class="popover-content"></div></div>'
    });
</script>


<?php
    require_once($_SERVER['LOCAL_NL_PATH']."/console/categories/leads/sms.php");
?>
</body>
</html>

