
<style>
    #sms-side{
        right: 0;
        background-color: #f1f5f8;
        border-left: 1px solid rgb(208, 208, 208);
        border-top: 1px solid rgb(208, 208, 208);
        position: fixed;
        top: 60px;
        width: 260px !important;
        z-index: 999;
        bottom: 0;
        margin-right: -260px;
    }
    #sms-side.sms-top {
        top: 0;
        border-top: none;
    }

    .sms-side-pinch {
        bottom: 31px;
        display: -webkit-flex;
        display: flex;
        height: 63px;
        overflow: hidden;
        position: fixed;
        margin-left: -36px;
        width: 35px;
        z-index: 99999999999999999999999999999;
        font-size: 19px;
        position: absolute;
    }

    .sms-side-pinch .pinch{
        font-size: 26px;
        cursor: pointer;
        background-color: #19b393;
        color: #fff !important;
        border-bottom-right-radius: 0;
        border-top-right-radius: 0;
        height: 50px;
        right: 0;
        margin: 8px 0 8px 8px;
        top: 0;
        padding-right: 6px;
        position: absolute;
        border-top-left-radius: 11px;
        border-bottom-left-radius: 11px;
        border: 1px solid gainsboro;
        border-right: none;
        color: #525252;
    }

    .sms-arrow{
        transform: rotate(180deg);
        float: left;
        fill: #fff;
        padding-right: 6px;
        width: 25px;
    }

    .unreadSMSBlink{
        background-color: #ed5565 !important;
        color: #FFFFFF !important;
    }

    .unreadSMSBlink .sms-arrow{
        fill: #fff !important;
    }
</style>

<div id="sms-side" class="closed-sms-side">

    <div class="sms-side-pinch" id="sms-side-pinch">
        <div class="pinch" id="open-sms-side">

            <svg class="sms-arrow" width="20px" height="50px" viewBox="0 0 24 24">
                <path d="M8.59,16.59L13.17,12L8.59,7.41L10,6l6,6l-6,6L8.59,16.59z"></path>
            </svg>
            <div style="float: left; height: 55px;line-height: 50px;"><span id="totalUnreadSMS"></span></div>
        </div>
    </div>

    <div class="sms-content-div" style="position: relative; overflow: hidden; width: auto; height: 100%;">
        <div class="sidebar-container" style="overflow: hidden; width: auto; height: 100%;">
            <iframe src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/iframes/leads/leadSMS.php?leadId=<?php echo $leadId; ?>" id="sms-side-iframe" style="width: 260px;height: 100%;border: none;"></iframe>
        </div>
    </div>

</div>

<script>
    // Move right sms sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#sms-side').addClass('sms-top');
            //$('.sms-content-div').css("margin-top","60px");
        } else {
            $('#sms-side').removeClass('sms-top');
            //$('.sms-content-div').css("margin-top","0px");

        }
    });


    $("#open-sms-side").on("click", function() {
        if(document.getElementById('sms-side').classList.contains('closed-sms-side')){

            $('#sms-side').removeClass("open-sms-side");
            $('#sms-side').removeClass("closed-sms-side");

            $("#sms-side").animate({marginRight:'0px'},350);
            $('#sms-side').addClass("open-sms-side");

            AnimateRotate(0);

            document.getElementById("sms-side-iframe").contentWindow.markRead();

        }else{

            $('#sms-side').removeClass("open-sms-side");
            $('#sms-side').removeClass("closed-sms-side");

            $("#sms-side").animate({marginRight:'-260px'},350);
            //$(".sms-arrow").animate({transform:'rotate(180deg);'},350);

            AnimateRotate(180);

            $('#sms-side').addClass("closed-sms-side");
        }
    });

    function AnimateRotate(angle) {
        // caching the object for performance reasons
        var $elem = $('.sms-arrow');

        // we use a pseudo object for the animation
        // (starts from `0` to `angle`), you can name it as you want
        $({deg: 0}).animate({deg: angle}, {
            duration: 400,
            step: function(now) {
                // in the step-callback (that is fired each step of the animation),
                // you can use the `now` paramter which contains the current
                // animation-position (`0` up to `angle`)
                    var paddingRight = 0;
                    var paddingLeft = 0;
                    if (now == 0) {
                        paddingRight = 0;
                        paddingLeft = "6px";
                    }else{
                        paddingRight = "6px";
                        paddingLeft = 0;
                    }
                    $elem.css({
                        transform: 'rotate(' + now + 'deg)',
                        paddingRight: paddingRight,
                        paddingLeft: paddingLeft
                    });
                }
            }
        )
    }
</script>