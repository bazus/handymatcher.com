<?php
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadProviders.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leads.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadTags.php");

$showBooked = false;
$showEstimates = false;
$showSaleTarget = false;

$organization = new organization($bouncer['userData']["organizationId"]);
$departments = $organization->getDepartments();

$leads = new leads($bouncer['userData']["organizationId"]);
$leadsUsers = $leads->getLeadsUsers();

$leadProviders = new leadProviders($bouncer["userData"]["organizationId"]);
$providers = $leadProviders->getProviders();

$leadTags = new leadTags($bouncer["userData"]["organizationId"]);
$leadTagsData = $leadTags->getData();

$totalProviders = $leadProviders->getTotalProvidersActive();
function escapeJavaScriptText($str)
{
    $new_str = '';

    $str_len = strlen($str);
    for($i = 0; $i < $str_len; $i++) {
        $new_str .= '\\x' . sprintf('%02x', ord(substr($str, $i, 1)));
    }

    return $new_str;
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads - Leads Management</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/leads.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/leads.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
        var orgUsers = [];
    </script>

    <style>
        .btn-danger-outline{
            background-color: unset;
            color: #ed5565;
            border-color: #ed5565;
        }
        .btn-danger-outline:hover{
            background-color: #ec4758 !important;
            border-color: #ec4758;
            color: #FFFFFF !important;
        }
        #leadsTableHead tr{
            height: 41px !important;
        }
        #leadsTableHead tr td.leadTdWhiteSpace{
            width: fit-content;
            overflow: hidden;
            white-space: nowrap;
        }

        .orangeBorder{
            border: 1px solid #f8ac59 !important;
            margin-top: 0 !important;
        }
        .blueBorder{
            border: 1px solid #1ab394 !important;
        }

        .getLeadsBy{
            border: none;
            padding-right: 23px;
            background: url(data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0Ljk1IDEwIj48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6I2ZmZjt9LmNscy0ye2ZpbGw6IzQ0NDt9PC9zdHlsZT48L2RlZnM+PHRpdGxlPmFycm93czwvdGl0bGU+PHJlY3QgY2xhc3M9ImNscy0xIiB3aWR0aD0iNC45NSIgaGVpZ2h0PSIxMCIvPjxwb2x5Z29uIGNsYXNzPSJjbHMtMiIgcG9pbnRzPSIxLjQxIDQuNjcgMi40OCAzLjE4IDMuNTQgNC42NyAxLjQxIDQuNjciLz48cG9seWdvbiBjbGFzcz0iY2xzLTIiIHBvaW50cz0iMy41NCA1LjMzIDIuNDggNi44MiAxLjQxIDUuMzMgMy41NCA1LjMzIi8+PC9zdmc+) no-repeat 95% 50%;
            -moz-appearance: none;
            -webkit-appearance: none;
            appearance: none;
        }

        .borderDownForTitle{
            border-bottom: 1px solid lightgray;
            padding-bottom: 10px;
        }

        .buttonFilterSections{
            float: inherit;
            margin-right: 5px;
        }

        #leadsTableHeader th {
            vertical-align: middle;
        }

        .LT_leadid{
            min-width: 80px;
        }
        .LT_fullname{
            min-width: 93px;
        }
        .LT_email{
            min-width: 230px;
        }
        .LT_phone{
            min-width: 180px;
        }
        .LT_datereceived{
            min-width: 190px;
        }
        .LT_moveDate{
            min-width: 105px;
        }
        .LT_movedate{
            min-width: 150px;
        }
        .LT_provider{
            min-width: 132px;
        }
        .LT_status{
            min-width: 63px;
        }
        .LT_priority{
            min-width: 20px;
        }
        .LT_assign{
            min-width: 171px;
        }
        .selectAllLeadsCheckbox {
            width: 17px;
            height: 17px;
            border: 1px solid #cccccc;
            border-radius: 3px;
            background-color: #fff;
            -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
            -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
            transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        }
        .dataTables_paginate{
            margin-top: 13px !important;
        }
    </style>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<button onclick="topFunction()" id="topBTN" title="Go to top"><i class="fa fa-arrow-up"></i></button>
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li  class="active">
                        <strong>Leads</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <?php if($totalProviders == 0 && $bouncer["isUserAnAdmin"] == true){ ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-info-circle"></i> Add Lead Providers
                    </div>
                    <div class="panel-body">
                        <p>
                            You do not have any lead providers API set up yet.<br>
                            In order to start receiving leads you need to set up a new lead provider first.<br>
                            <br>
                            <a href="leadProviders.php" role="button" class="btn btn-info btn-sm">Add Lead Provider</a>
                        </p>
                    </div>
                </div>
                <?php } ?>
                <div id="datePicker" style="padding:6px 12px !important;float: left;margin-bottom: 10px;margin-right: 5px; width: fit-content;">
                    <button id="backDay" class="btn btn-default inline"><i class="fa fa-arrow-left"></i></button>
                    <div id="reportrange" class="reportrange">
                        <i style="margin: 3px;" class="fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                    <button id="nextDay" class="btn btn-default inline"><i class="fa fa-arrow-right"></i></button>

                    <?php if(isset($_COOKIE["getLeadsByMoveDate"]) && $_COOKIE["getLeadsByMoveDate"] == "1") {}  ?>
                    <div class="reportrange" style="padding: 0px;">
                        <select class="form-control getLeadsBy" id="getLeadsBy" onchange="getLeads()">
                            <option value="0">By Received Date</option>
                            <option value="1" <?php if(isset($_COOKIE["getLeadsByMoveDate"]) && $_COOKIE["getLeadsByMoveDate"] == "1") { ?> selected <?php }  ?>>By Move Date</option>
                        </select>
                    </div>

                </div>
                <div class="pull-right" id="leadsButtons" style="margin-top: 8px;">
                    <a href="leadProviders.php" role="button" class="btn btn-primary btn-sm"><i class="fa fa-handshake-o"></i> Lead Providers</a>
                    <a onclick="showCustomModal('categories/iframes/leads/addManualLead.php')" role="button" class="btn btn-success btn-sm"><i class="fa fa-address-card"></i> Insert Lead Manually</a>
                    <?php if($bouncer["isUserAnAdmin"] == "1"){ ?>
                    <button onclick="showCustomModal('categories/iframes/leads/settings.php')" role="button" class="btn btn-success btn-sm">Settings</button>
                    <?php } ?>
                </div>

                <br><br>

                <div class="ibox-content" style="margin-bottom: 10px; margin-right: 6px;">

                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div style="padding-left: 0px;">
                        <span class="totalLeadsSpan" style="padding-top: 3px; padding-bottom: 6px;">Total Leads : <span id="totalLeads"></span></span>
                        <span><button onclick="getLeads();" style="vertical-align: middle;margin-left: 3px;" id="refreshLead" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button></span>
                        <div class="ibox-tools" style="float: right; display: flex">

                            <button onclick="deleteSelectedLeads();" style="display:none; margin-right: 5px;" id="deleteSelectedLeads" class="btn btn-danger-outline btn-sm btn-mobile-block" title="Delete selected leads"><i class="fa fa-trash-o"></i></button>
                            <button onclick="sendEmailToLead();" style="display:none; margin-right: 5px;" id="emailToLeads" class="btn btn-default btn-sm btn-mobile-block" title="Send email to selected leads"><i class="fa fa-envelope-o"></i></button>
                            <button onclick="sendSMSToLead()" style="display:none;margin-right: 5px;" id="smsToLeads" class="btn btn-default btn-sm btn-mobile-block" title="Send sms to selected leads"><i class="fa fa fa-comment-o"></i></button>
                            <button onclick="checkLeadsCountBeforeOpen()" style="display:none;margin-right: 5px;" id="openLeads" class="btn btn-default btn-sm btn-mobile-block" title="Open selected leads"><i class="fa fa-window-restore"></i></button>

                            <button id="theBTN" onclick="document.getElementById('filterBTN').style.display='';this.style.display='none';" class="btn btn-default btn-sm" style="margin: 0; height: 30px">Filters</button>

                            <div id="filterBTN" style="display: none; float: right;">
                                <select class="form-control orangeBorder" style="margin:0" onchange="getLeadsByFilter('priority',this,$('#filterSelected_priority option:selected').html())" id="filterSelected_priority">
                                    <option value="">Filter by priority</option>
                                    <option value="1">1 - Lowest</option>
                                    <option value="2">2 - Low</option>
                                    <option value="3">3 - Medium</option>
                                    <option value="4">4 - High</option>
                                    <option value="5">5 - Highest</option>
                                </select>
                                <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                                    <select class="form-control orangeBorder" onchange="getLeadsByFilter('user',this,$('#filterSelected_user option:selected').html())" id="filterSelected_user">
                                        <option value>Filter by users</option>
                                        <option value="null">Not assigned</option>
                                        <?php foreach ($leadsUsers as $user){?>
                                            <option value="<?= $user['id'] ?>"><?= $user['fullName'] ?></option>
                                        <?php }?>
                                    </select>
                                <?php }?>
                                <select class="form-control orangeBorder" onchange="getLeadsByFilter('tags',this,$('#filterSelected_tags option:selected').html())" id="filterSelected_tags">
                                    <option value="">Filter by tags</option>
                                    <?php foreach ($leadTagsData as $leadTag){?>
                                        <option value="<?php echo $leadTag["id"]; ?>"><?php echo $leadTag["name"]; ?></option>
                                    <?php }?>
                                </select>
                                <select class="form-control orangeBorder" onchange="getLeadsByFilter('provider',this,$('#filterSelected_provider option:selected').html())" id="filterSelected_provider">
                                    <option value="">Filter by providers</option>
                                    <option value="manual">Manual</option>
                                    <?php
                                    foreach ($providers as $provider){
                                        ?>
                                        <option value="<?= $provider['id'] ?>"><?= $provider['providerName'] ?></option>
                                    <?php }?>
                                </select>
                                <select class="form-control orangeBorder" onchange="getLeadsByFilter('vip',this,$('#filterSelected_vip option:selected').html())" id="filterSelected_vip">
                                    <option value="">Leads Filter</option>
                                    <option value="1">Hot Leads</option>
                                </select>
                                <select class="form-control orangeBorder" onchange="getLeadsByFilter('status',this,$('#filterSelected_status option:selected').html())" id="filterSelected_status">
                                    <option value="">Status Filter</option>
                                    <option value="0">New</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Quoted</option>
                                    <option value="3">Booked</option>
                                    <option value="4">In progress</option>
                                    <option value="5">Storage/Transit</option>
                                    <option value="6">Completed</option>
                                    <option value="7">Cancelled</option>
                                </select>
                                <button onclick="document.getElementById('theBTN').style.display='';document.getElementById('filterBTN').style.display='none';" class="btn btn-sm btn-default backBTN" style="margin-top: 0">Back</button>
                            </div>

                            <div class="dropdown buttonFilterSections" style="margin-left:3px;float: right;">

                                <button class="dropdown-toggle count-info btn btn-default btn-sm" data-toggle="dropdown" href="#" aria-expanded="true">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <div class="dropdown-menu dropdown-alerts" id="leadsTableColumnPicker" style="left: -130px;">
                                    <h4 class="borderDownForTitle">Column Selections</h4>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="fullNameCheckBox" id="fullNameCheckBox">
                                        <label for="fullNameCheckBox">
                                            Full Name
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="emailCheckBox" id="emailCheckBox">
                                        <label for="emailCheckBox">
                                            Email
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="phoneCheckBox" id="phoneCheckBox">
                                        <label for="phoneCheckBox">
                                            Phone
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="dateRecCheckBox" id="dateRecCheckBox">
                                        <label for="dateRecCheckBox">
                                            Data Received
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="moveDateCheckBox" id="moveDateCheckBox">
                                        <label for="moveDateCheckBox">
                                            Move Date
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="providerCheckBox" id="providerCheckBox">
                                        <label for="providerCheckBox">
                                            Provider
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="statusCheckBox" id="statusCheckBox">
                                        <label for="statusCheckBox">
                                            Status
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" checked="" name="priorityCheckBox" id="priorityCheckBox">
                                        <label for="priorityCheckBox">
                                            Priority
                                        </label>
                                    </div>
                                    <?php if ($bouncer['isUserAnAdmin'] == true){ ?>

                                        <div class="checkbox checkbox-primary">
                                            <input type="checkbox" checked="" name="assignCheckBox" id="assignCheckBox">
                                            <label for="assignCheckBox">
                                                Assign
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="fromCityCheckBox" id="fromCityCheckBox">
                                        <label for="fromCityCheckBox">
                                            From City
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="fromStateCheckBox" id="fromStateCheckBox">
                                        <label for="fromStateCheckBox">
                                            From State
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="fromZipCheckBox" id="fromZipCheckBox">
                                        <label for="fromZipCheckBox">
                                            From Zip
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="toCityCheckBox" id="toCityCheckBox">
                                        <label for="toCityCheckBox">
                                            To City
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="toStateCheckBox" id="toStateCheckBox">
                                        <label for="toStateCheckBox">
                                            To State
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="toZipCheckBox" id="toZipCheckBox">
                                        <label for="toZipCheckBox">
                                            To Zip
                                        </label>
                                    </div>


                                    <button type="submit" style="width: 100%;" class="ladda-button ladda-button-demo btn btn-primary btn-sm" data-style="slide-up" onclick="leadsTdsController.updateLeadsTableColumnsLocalStorage();">Apply</button>
                                    <hr style="margin-top: 9px;margin-bottom: 4px;" />
<!--                                    <div style="text-align: center;"><a style="color: #337ab7;" onclick="leadsTdsController.exportLeadsToCsv()">export to csv</a></div>-->
                                    <div id="exportCsvBtn" style="text-align: center;"><a style="color: #337ab7;">export to csv</a></div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <div id="filterContainer" style="margin-top:15px;width: 100%;">
                            <div id="selectedFilters" class="pull-right inputer" style="display: none;margin-bottom: 9px;">

                            </div>
                        </div>
                    </div>
                <div class="table-responsive leadsTable">
                    <table class="table table-bordered dataTables-leads">
                        <thead class="thead-dark" id="leadsTableHeader">
                        </thead>
                        <tbody id="leadsTableHead">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
    <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
        <div id="leadsAssign" style="display: none;">
        <?php foreach ($organization->getUsersOfMyCompany() AS $user){ ?>
            <script>orgUsers.push({"id":'<?=$user['id']?>',"fullName":'<?php if ($user['id'] == $bouncer['credentials']["userId"]){echo "Assign To Me";}else{echo "Assign To ".escapeJavaScriptText($user['fullName']);}?>'});</script>
        <?php }?>
        </div>
    <?php } ?>
</div>
<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/datetimepicker/jquery.datetimepicker.js"></script>

<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Date range picker -->

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/moment.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/leads/script.min.js?v=1.3.7.8"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/dataTables/datatables.min.js"></script>

    <?php
//    if($userSettingsData["firstTutorial"] == "1" && $bouncer['isUserAnAdmin'] == true) {
//        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/admin/adminTutorial_stage_leads.php");
//    }else if ($userSettingsData['firstTutorial'] == "1" && $bouncer['isUserAnAdmin'] == false){
//        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/user/userTutorial_stage1.php");
//    }
    ?>

    <?php
        if(isset($_GET['showLeadSettings']) && $_GET['showLeadSettings'] == "true"){
            ?>
            <script>
                showCustomModal('categories/iframes/leads/settings.php?show=tags')
            </script>
            <?php
        }
    ?>


    <?php if (isset($_GET['addManuallyLead'])){ ?>
        <script>
            $(document).ready(function () {
                showCustomModal('categories/iframes/leads/addManualLead.php')
            });
        </script>
    <?php } ?>

    <script>
        // ============== LEADS TABLE COLUMN FILTER (START) ==============
        var leadsTdsController = {

            getLeadsTableColumns:function(){
                var getCheckboxChoices = JSON.parse(localStorage.getItem("userCheckboxChoices"));

                // if no customized columns - set these default columns
                if(getCheckboxChoices == null){
                    getCheckboxChoices = [];
                    getCheckboxChoices[0] = {name:"fullNameCheckBox",value:true};
                    getCheckboxChoices[1] = {name:"emailCheckBox",value:true};
                    getCheckboxChoices[2] = {name:"phoneCheckBox",value:true};
                    getCheckboxChoices[3] = {name:"dateRecCheckBox",value:true};
                    getCheckboxChoices[4] = {name:"moveDateCheckBox",value:true};
                    getCheckboxChoices[5] = {name:"providerCheckBox",value:true};
                    getCheckboxChoices[6] = {name:"statusCheckBox",value:true};
                    getCheckboxChoices[7] = {name:"priorityCheckBox",value:true};
                    getCheckboxChoices[8] = {name:"assignCheckBox",value:true};
                    getCheckboxChoices[9] = {name:"fromCityCheckBox",value:false};
                    getCheckboxChoices[10] = {name:"fromStateCheckBox",value:false};
                    getCheckboxChoices[11] = {name:"fromZipCheckBox",value:false};
                    getCheckboxChoices[12] = {name:"toCityCheckBox",value:false};
                    getCheckboxChoices[13] = {name:"toStateCheckBox",value:false};
                    getCheckboxChoices[14] = {name:"toZipCheckBox",value:false};
                }

                if(getCheckboxChoices[0] == null || getCheckboxChoices[0] == undefined){getCheckboxChoices[0] = {name:"fullNameCheckBox",value:true};}
                if(getCheckboxChoices[1] == null || getCheckboxChoices[1] == undefined){getCheckboxChoices[1] = {name:"emailCheckBox",value:true};}
                if(getCheckboxChoices[2] == null || getCheckboxChoices[2] == undefined){getCheckboxChoices[2] = {name:"phoneCheckBox",value:true};}
                if(getCheckboxChoices[3] == null || getCheckboxChoices[3] == undefined){getCheckboxChoices[3] = {name:"dateRecCheckBox",value:true};}
                if(getCheckboxChoices[4] == null || getCheckboxChoices[4] == undefined){getCheckboxChoices[4] = {name:"moveDateCheckBox",value:true};}
                if(getCheckboxChoices[5] == null || getCheckboxChoices[5] == undefined){getCheckboxChoices[5] = {name:"providerCheckBox",value:true};}
                if(getCheckboxChoices[6] == null || getCheckboxChoices[6] == undefined){getCheckboxChoices[6] = {name:"statusCheckBox",value:true};}
                if(getCheckboxChoices[7] == null || getCheckboxChoices[7] == undefined){getCheckboxChoices[7] = {name:"priorityCheckBox",value:true};}
                if(getCheckboxChoices[8] == null || getCheckboxChoices[8] == undefined){getCheckboxChoices[8] = {name:"assignCheckBox",value:true};}
                if(getCheckboxChoices[9] == null || getCheckboxChoices[9] == undefined){getCheckboxChoices[9] = {name:"fromCityCheckBox",value:false};}
                if(getCheckboxChoices[10] == null || getCheckboxChoices[10] == undefined){getCheckboxChoices[10] = {name:"fromStateCheckBox",value:false};}
                if(getCheckboxChoices[11] == null || getCheckboxChoices[11] == undefined){getCheckboxChoices[11] = {name:"fromZipCheckBox",value:false};}
                if(getCheckboxChoices[9] == null || getCheckboxChoices[12] == undefined){getCheckboxChoices[12] = {name:"toCityCheckBox",value:false};}
                if(getCheckboxChoices[10] == null || getCheckboxChoices[13] == undefined){getCheckboxChoices[13] = {name:"toStateCheckBox",value:false};}
                if(getCheckboxChoices[11] == null || getCheckboxChoices[14] == undefined){getCheckboxChoices[14] = {name:"toZipCheckBox",value:false};}

                return getCheckboxChoices;
            },
            updateLeadsTableColumnsLocalStorage:function(){
                // When changing the leads TDS columns - update the local storage

                // start the ladda
                var ldbtn = $('.ladda-button-demo').ladda();
                ldbtn.ladda("start");

                // Add the checked columns to local storage
                var sList = [];
                $('#leadsTableColumnPicker input[type=checkbox]').each(function () {
                    sList.push({name:this.name, value:this.checked});
                });
                localStorage.setItem("userCheckboxChoices", JSON.stringify(sList));

                // Set the ladda effect and re-draw table
                setTimeout(function(){
                    ldbtn.ladda("stop");
                    $('.buttonFilterSections').removeClass('open');
                    setLeadsTable();
                }, 300);
            },
            createLeadsTableHeader:function(){

                var userTableHeader = document.getElementById('leadsTableHeader');
                userTableHeader.innerHTML = "";

                var leadsTableColumns = leadsTdsController.getLeadsTableColumns();

                var tr = document.createElement("tr");

                var th = document.createElement("th");
                th.className = 'LT_tags'
                tr.append(th);

                var th = document.createElement("th");
                th.className = 'LT_leadid'
                th.setAttribute("style","text-align:center");
                th.innerHTML = '<input type="checkbox" class="selectAllLeadsCheckbox" onchange="leadsTdsController.toggleSelectAllLeads(this)" />';
                tr.append(th);



                if(leadsTableColumns[0].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_fullname';
                    th.append('Full Name');
                    tr.append(th);
                }

                if(leadsTableColumns[1].value == true){
                    var th = document.createElement("th");
                    th.className = 'LT_email';
                    th.append('Email');
                    tr.append(th);
                }

                if(leadsTableColumns[2].value == true){
                    var th = document.createElement("th");
                    th.className = 'LT_phone';
                    th.append('Phone');
                    tr.append(th);
                }

                if(leadsTableColumns[3].value == true){
                    var th = document.createElement("th");
                    th.className = 'LT_datereceived';
                    th.append('Date Received');
                    tr.append(th);
                }

                if(leadsTableColumns[4].value == true){
                    var th = document.createElement("th");
                    th.className = 'LT_moveDate';
                    th.append('Move Date');
                    tr.append(th);
                }

                if(leadsTableColumns[5].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_provider';
                    th.append('Provider');
                    tr.append(th);
                }

                if(leadsTableColumns[6].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_status';
                    th.append('Status');
                    tr.append(th);
                }

                if(leadsTableColumns[7].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_priority';
                    th.append('Priority');
                    tr.append(th);
                }

                <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                if(leadsTableColumns[8].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_assign';
                    th.append('Assign');
                    tr.append(th);
                }
                <?php } ?>

                if(leadsTableColumns[9].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_fromCity';
                    th.append('From City');
                    tr.append(th);
                }

                if(leadsTableColumns[10].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_fromState';
                    th.append('From State');
                    tr.append(th);
                }

                if(leadsTableColumns[11].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_fromZip';
                    th.append('From Zip');
                    tr.append(th);
                }

                if(leadsTableColumns[12].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_toCity';
                    th.append('To City');
                    tr.append(th);
                }

                if(leadsTableColumns[13].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_toState';
                    th.append('To State');
                    tr.append(th);
                }

                if(leadsTableColumns[14].value == true) {
                    var th = document.createElement("th");
                    th.className = 'LT_toZip';
                    th.append('To Zip');
                    tr.append(th);
                }

                userTableHeader.append(tr);
            },
            createLeadsTableRow:function(lead){

                // get leads columns to show
                var leadsTableColumns = leadsTdsController.getLeadsTableColumns();

                var container = document.getElementById("leadsTableHead");

                var tr = document.createElement("tr");
                tr.setAttribute("onclick","window.open('"+BASE_URL+"/console/categories/leads/lead.php?leadId="+lead.id+"','_blank')");
                tr.id = "leadRow"+lead.id;

                if (lead.status >= 3 && lead.status <= 5){
                    tr.classList.add("bookedRow");
                }

                if (lead.status == 6){
                    tr.classList.add("finishedRow");
                }

                if (lead.isBadLead == "1" || lead.status == 7){
                    tr.style.backgroundColor = "rgba(237,85,101,0.4)";
                    tr.style.color = "white";
                }

                var td = document.createElement("td");
                td.setAttribute("style","text-align:center;padding: 0px;width:20px");
                td.setAttribute("data-value", "");

                var eachWidth = 100/lead.tagsData.length;
                for(var i = 0;i<lead.tagsData.length;i++) {

                    var tag = document.createElement("div");
                    tag.style.height = "40px";
                    tag.style.width = eachWidth+"%";
                    tag.style.cssFloat = "left";
                    tag.style.backgroundColor = lead.tagsData[i].color;
                    tag.title = lead.tagsData[i].name;

                    td.appendChild(tag);
                }
                tr.appendChild(td);

                var td = document.createElement("td");
                td.style.textAlign = "center";

                var idBTN = document.createElement("button");

                if (lead.isVIP == "1"){
                    idBTN.classList.add("hotLead");
                }
                idBTN.setAttribute("onClick","event.stopPropagation();toggleLeadRow('"+lead.id+"')");
                idBTN.setAttribute("style","color:#676a6c");
                idBTN.setAttribute("data-leadid",lead.id);
                idBTN.id = "selectLead-"+lead.id;
                idBTN.classList.add("selectLeadBTN");
                idBTN.classList.add("btn");
                idBTN.classList.add("btn-white");
                idBTN.classList.add("btn-sm");

                if(lead.jobNumber != null && lead.jobNumber != ""){
                    idBTN.innerHTML = lead.jobNumber;
                    td.setAttribute("data-value", lead.jobNumber);
                }else{
                    idBTN.innerHTML = lead.id;
                    td.setAttribute("data-value", lead.id);
                }

                td.appendChild(idBTN);
                tr.appendChild(td);

                if(leadsTableColumns[0].value == true){
                    var td = leadsTdsController.getFullnameTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[1].value == true){
                    var td = leadsTdsController.getEmailTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[2].value == true){
                    var span = leadsTdsController.getPhoneSMSTD(lead);

                    var td = leadsTdsController.getPhoneTD(lead);

                    td.insertBefore(span, td.childNodes[1]);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[3].value == true){
                    var td = leadsTdsController.getDateReceiveTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[4].value == true){
                    var td = leadsTdsController.getMoveDateTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[5].value == true){
                    var td = leadsTdsController.getProviderTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[6].value == true){
                    var td = leadsTdsController.getStatusTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[7].value == true){
                    var td = leadsTdsController.getPriorityTD(lead);
                    tr.appendChild(td);
                }

                <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                if(leadsTableColumns[8].value == true){
                    var td = leadsTdsController.getAssignTD(lead);
                    tr.appendChild(td);
                }
                <?php } ?>

                if(leadsTableColumns[9].value == true){
                    var td = leadsTdsController.getFromCityTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[10].value == true){
                    var td = leadsTdsController.getFromStateTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[11].value == true){
                    var td = leadsTdsController.getFromZipTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[12].value == true){
                    var td = leadsTdsController.getToCityTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[13].value == true){
                    var td = leadsTdsController.getToStateTD(lead);
                    tr.appendChild(td);
                }

                if(leadsTableColumns[14].value == true){
                    var td = leadsTdsController.getToZipTD(lead);
                    tr.appendChild(td);
                }

                container.appendChild(tr);

            },
            exportLeadsToCsv:function(){

                // get leads columns to show
                var leadsTableColumns = leadsTdsController.getLeadsTableColumns();

                var filteredLeads = filterLeads();
                var allDataToExport = [];
                var headersToExport = [];
                headersToExport["Lead"] = true;

                for(var i = 0;i<filteredLeads.length;i++){
                    var lead = filteredLeads[i];
                    var dataToExport = [];
                    dataToExport.push(lead.id);

                    if(leadsTableColumns[0].value == true){
                        headersToExport["Full Name"] = true;
                        dataToExport.push(lead.firstname+" "+lead.lastname);
                    }

                    if(leadsTableColumns[1].value == true){
                        headersToExport["Email"] = true;
                        dataToExport.push(lead.email);
                    }

                    if(leadsTableColumns[2].value == true){
                        headersToExport["Phone"] = true;
                        dataToExport.push(lead.phone);
                    }

                    if(leadsTableColumns[3].value == true){
                        headersToExport["Date Received"] = true;
                        var dateReceived = lead.dateReceived.replace(/,/g, "");

                        dataToExport.push(dateReceived);
                    }

                    if(leadsTableColumns[4].value == true){
                        headersToExport["Move Date"] = true;
                        var moveDateText = lead.moveDateText.replace(/,/g, "");

                        dataToExport.push(moveDateText);
                    }

                    if(leadsTableColumns[5].value == true){
                        headersToExport["Provider Name"] = true;
                        var providerName = lead.providerName.replace(/,/g, "");

                        dataToExport.push(providerName);
                    }

                    if(leadsTableColumns[6].value == true){
                        headersToExport["Status"] = true;
                        bookedText = lead.statusText;

                        var bookedText = bookedText.replace(/,/g, "");

                        dataToExport.push(bookedText);
                    }

                    if(leadsTableColumns[7].value == true){
                        headersToExport["Priority"] = true;
                        switch (lead.priority){
                            case '1':
                                var priority = "Lowest";
                                break;
                            case '2':
                                var priority = "Low";
                                break;
                            case '3':
                                var priority = "Medium";
                                break;
                            case '4':
                                var priority = "High";
                                break;
                            case '5':
                                var priority = "Highest";
                                break;
                        }
                        dataToExport.push(priority);
                    }


                    <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                    if(leadsTableColumns[8].value == true){
                        headersToExport["Assign"] = true;
                        var assignedUser = "";
                        for (var x = 0; x < orgUsers.length; x++) {
                            if (orgUsers[x].id == lead.userIdAssigned) {
                                assignedUser = orgUsers[x].fullName;
                            }
                        }
                        dataToExport.push(assignedUser);
                    }
                    <?php } ?>

                    if(leadsTableColumns[9].value == true){
                        headersToExport["From City"] = true;
                        dataToExport.push(lead.fromCity);
                    }

                    if(leadsTableColumns[10].value == true){
                        headersToExport["From State"] = true;
                        dataToExport.push(lead.fromState);
                    }

                    if(leadsTableColumns[11].value == true){
                        headersToExport["From Zip"] = true;
                        dataToExport.push(lead.fromZip);
                    }
                    if(leadsTableColumns[12].value == true){
                        headersToExport["To City"] = true;
                        dataToExport.push(lead.toCity);
                    }

                    if(leadsTableColumns[13].value == true){
                        headersToExport["To State"] = true;
                        dataToExport.push(lead.toState);
                    }

                    if(leadsTableColumns[14].value == true){
                        headersToExport["To Zip"] = true;
                        dataToExport.push(lead.toZip);
                    }

                    allDataToExport.push(dataToExport);

                }




                var headers = (Object.keys(headersToExport)).join(",");

                        var csvContent = "data:text/csv;charset=utf-8,"+headers+"\n";

                        allDataToExport.forEach(function(rowArray) {
                            var row = rowArray.join(",");
                            csvContent += row + "\r\n";
                        });
                        var encodedUri = encodeURI(csvContent);

                        var downloadLink = document.createElement("a");
                        downloadLink.href = encodedUri;
                        downloadLink.download = "NetworkLeads.csv";

                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);

                        //window.open(encodedUri);

            },
            getFullnameTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.firstname+" "+lead.lastname);
                var tdNode = document.createTextNode(lead.firstname+" "+lead.lastname);

                td.appendChild(tdNode);
                return td;
            },
            getEmailTD:function (lead) {
                var duplicateEmail = document.createElement("span");
                duplicateEmail.style.marginLeft = "3px";

                duplicateEmail.classList.add("label");
                duplicateEmail.classList.add("label-danger");
                duplicateEmail.style.padding = "4px 8px";
                duplicateEmail.style.cssFloat = "right";
                duplicateEmail.setAttribute("onclick","event.stopPropagation();showDuplicateEmail('"+lead.id+"')");
                var duplicateText = document.createTextNode("Duplicate");
                duplicateEmail.appendChild(duplicateText);

                var td = document.createElement("td");
                td.classList.add("leadTdWhiteSpace");
                td.setAttribute("data-value", lead.email);

                var tdSpan  = document.createElement("span");
                if(lead.duplicateEmail == true){
                    td.setAttribute("title",lead.email);
                    tdSpan.style.width = "150px";
                    tdSpan.style.whiteSpace = "nowrap";
                    tdSpan.style.overflow = "hidden";
                    tdSpan.style.textOverflow = "ellipsis";
                    tdSpan.style.display = "inline-block";
                }

                var tdSpanNode = document.createTextNode(lead.email);

                tdSpan.appendChild(tdSpanNode);
                td.appendChild(tdSpan);
                if(lead.duplicateEmail == true){
                    td.appendChild(duplicateEmail);
                }
                return td;
            },
            getPhoneTD:function (lead) {
                var duplicatePhone = document.createElement("span");
                duplicatePhone.style.marginRight = "7px";

                duplicatePhone.classList.add("label");
                duplicatePhone.classList.add("label-danger");
                duplicatePhone.style.padding = "4px 8px";
                duplicatePhone.style.cssFloat = "right";
                duplicatePhone.setAttribute("onclick","event.stopPropagation();showDuplicatePhone('"+lead.id+"')");
                var duplicateText = document.createTextNode("Duplicate");
                duplicatePhone.appendChild(duplicateText);


                var td = document.createElement("td");
                td.classList.add("leadTdWhiteSpace");
                td.setAttribute("data-value", lead.phone);

                var tdSpan  = document.createElement("span");
                if(lead.duplicatePhone == true) {
                    td.setAttribute("title",lead.phone);
                    tdSpan.style.width = "78px";
                    tdSpan.style.overflow = "hidden";
                    tdSpan.style.whiteSpace = "nowrap";
                    tdSpan.style.textOverflow = "ellipsis";
                    tdSpan.style.display = "inline-block";
                }

                var tdSpanNode = document.createTextNode(lead.phone);

                tdSpan.appendChild(tdSpanNode);
                td.appendChild(tdSpan);

                if(lead.duplicatePhone == true){
                    td.appendChild(duplicatePhone);
                }
                return td;
            },
            getPhoneSMSTD:function (lead) {

                var span = document.createElement("span");
                span.setAttribute("style","float: right;");


                var spanI = document.createElement("i");
                spanI.className = "fa fa-commentsfa fa-comments";
                spanI.setAttribute("style","width: 19px;");

                if (lead.unreadsms > 0) {
                    if (lead.unreadsms == 999){lead.unreadsms = "999+";}

                    var label = document.createElement("span");
                    label.className = "badge badge-warning float-right";
                    label.setAttribute("style","top: -9px;position: relative;left: -9px;padding:3px 5px 3px 5px");
                    label.innerHTML = lead.unreadsms;

                    spanI.appendChild(label);
                }

                span.appendChild(spanI);

                return span;
            },
            getDateReceiveTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute('title',lead.dateReceivedAgo);
                var tdNode = document.createTextNode(lead.dateReceived);
                td.setAttribute("data-order", lead.dateReceivedTimestamp);
                td.setAttribute("data-value", lead.dateReceived);
                td.appendChild(tdNode);
                return td;
            },
            getMoveDateTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute('title',lead.moveDate);
                td.setAttribute("data-order", lead.moveDateTimestamp);
                td.setAttribute("data-value", lead.moveDateText);
                var tdNode = document.createTextNode(lead.moveDateText);

                td.appendChild(tdNode);
                return td;
            },
            getProviderTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.providerName);

                var tdNode = document.createTextNode(lead.providerName);
                if (lead.isNM == 1){
                    var icon = document.createElement("i");
                    icon.classList.add("icon-NM");
                    td.appendChild(icon);
                }
                td.appendChild(tdNode);
                return td;
            },
            getStatusTD:function (lead) {
                var td = document.createElement("td");

                if (lead.isBadLead == '1'){
                    td.setAttribute("data-value", "Bad Lead");

                    var isBadLead = document.createElement("span");
                    isBadLead.style.maxWidth = "150px";
                    isBadLead.classList.add("center-block");
                    isBadLead.classList.add("label");
                    isBadLead.classList.add("label-danger");
                    isBadLead.setAttribute("style","padding: 6px;");

                    var isBadLeadText = document.createTextNode("Bad Lead");
                    isBadLead.appendChild(isBadLeadText);
                    td.appendChild(isBadLead);
                }else {
                    td.setAttribute("data-value", lead.statusText);

                    var booked = document.createElement("span");
                    booked.style.maxWidth = "150px";
                    booked.classList.add("center-block");
                    booked.classList.add("label");
                    booked.setAttribute("style", "padding: 6px;");
                    booked.classList.add("label");

                    var bookedText = document.createTextNode(lead.statusText);

                    switch (lead.status) {
                        case '1':
                            booked.classList.add("label-warning");
                            break;
                        case '2':
                            booked.classList.add("label-info");
                            break;
                        case '3':
                            booked.classList.add("label-primary");
                            break;
                        case '4':
                            booked.classList.add("label-primary");
                            break;
                        case '5':
                            booked.classList.add("label-primary");
                            break;
                        case '6':
                            booked.classList.add("label-success");
                            break;
                        case '7':
                            booked.classList.add("label-danger");
                            break;
                    }
                    booked.appendChild(bookedText);
                    td.appendChild(booked);
                }

                return td;
            },
            getPriorityTD:function (lead) {
                var td = document.createElement("td");

                var i = document.createElement("i");
                i.classList.add("fa");
                switch (lead.priority){
                    case '1':
                        var tdNode = document.createTextNode(" Lowest");
                        td.setAttribute("data-value", "Lowest");

                        i.classList.add("fa-arrow-down");
                        i.style.color = "rgb(85, 165, 87)";
                        td.setAttribute("data-order", 1);
                        break;
                    case '2':
                        var tdNode = document.createTextNode(" Low");
                        td.setAttribute("data-value", "Low");

                        i.classList.add("fa-arrow-down");
                        i.style.color = "rgb(42, 135, 53)";
                        td.setAttribute("data-order", 2);
                        break;
                    case '3':
                        var tdNode = document.createTextNode(" Medium");
                        td.setAttribute("data-value", "Medium");

                        i.classList.add("fa-arrow-up");
                        i.style.color = "rgb(234, 125, 36)";
                        td.setAttribute("data-order", 3);
                        break;
                    case '4':
                        var tdNode = document.createTextNode(" High");
                        td.setAttribute("data-value","High");

                        i.classList.add("fa-arrow-up");
                        i.style.color = "rgb(234, 68, 68)";
                        td.setAttribute("data-order", 4);
                        break;
                    case '5':
                        var tdNode = document.createTextNode(" Highest");
                        td.setAttribute("data-value", "Highest");

                        i.classList.add("fa-arrow-up");
                        i.style.color = "rgb(206,0,0)";
                        td.setAttribute("data-order", 5);
                        break;
                    default:
                        var tdNode = document.createTextNode(" Lowest");
                        td.setAttribute("data-value", "Lowest");

                        i.classList.add("fa-arrow-down");
                        i.style.color = "rgb(85, 165, 87)";
                        td.setAttribute("data-order", 6);
                        break;
                }
                td.appendChild(i);
                td.appendChild(tdNode);
                return td;
            },
            getAssignTD:function (lead) {
                if (document.getElementById("leadsAssign") !== null){
                    var td = document.createElement("td");


                    var select = document.createElement("select");

                    select.classList.add("form-control", "assignUserSelect");
                    select.style.color = "#676A6C";
                    select.style.height = "23px";
                    select.setAttribute("onclick","event.stopPropagation();");
                    select.setAttribute("onChange","assignUser("+lead.id+",this.value)");

                    var option = document.createElement("option");
                    option.value = "";

                    var optionText = document.createTextNode("Select User");
                    td.setAttribute("data-value","Unassigned");
                    option.appendChild(optionText);
                    select.appendChild(option);
                    for (var x = 0; x < orgUsers.length; x++) {
                        var option = document.createElement("option");
                        option.value = orgUsers[x].id;
                        if (orgUsers[x].id == lead.userIdAssigned) {
                            option.setAttribute("selected","true");
                            td.setAttribute("data-value",orgUsers[x].fullName);
                        }
                        var optionText = document.createTextNode(orgUsers[x].fullName);
                        option.appendChild(optionText);
                        select.appendChild(option);
                    }
                    td.appendChild(select);
                    return td;
                }else{

                }

            },
            getFromCityTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.fromCity);

                var tdNode = document.createTextNode(lead.fromCity);

                td.appendChild(tdNode);
                return td;
            },
            getFromStateTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.fromState);

                var tdNode = document.createTextNode(lead.fromState);

                td.appendChild(tdNode);
                return td;
            },
            getFromZipTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.fromZip);

                var tdNode = document.createTextNode(lead.fromZip);

                td.appendChild(tdNode);
                return td;
            },
            getToCityTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.toCity);

                var tdNode = document.createTextNode(lead.toCity);

                td.appendChild(tdNode);
                return td;
            },
            getToStateTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.toState);

                var tdNode = document.createTextNode(lead.toState);

                td.appendChild(tdNode);
                return td;
            },
            getToZipTD:function (lead) {
                var td = document.createElement("td");
                td.setAttribute("data-value", lead.toZip);

                var tdNode = document.createTextNode(lead.toZip);

                td.appendChild(tdNode);
                return td;
            },
            toggleSelectAllLeads:function (obj) {

                if(obj.checked){
                    $( ".selectLeadBTN" ).each(function( index ) {
                        toggleLeadRow($(this).attr("data-leadid"),1);
                    });
                }else{
                    $( ".selectLeadBTN" ).each(function( index ) {
                        toggleLeadRow($(this).attr("data-leadid"),0);
                    });
                }
            }
        }


        $('#leadsTableColumnPicker').on('click', function (e) {
            e.stopPropagation();
        });

        // Set the leads table column checkboxes in the setting menu
        $(document).ready(function() {
            var leadsTableColumns = leadsTdsController.getLeadsTableColumns();

            $( leadsTableColumns).each(function( index ) {
                $('#' + $( this )[0].name).prop('checked', $( this )[0].value);
            });
        });
        // ============== LEADS TABLE COLUMN FILTER (END) ==============
        function startTableSort() {

            var dTable = $('.dataTables-leads').DataTable({
                "aaSorting": [],
                "oLanguage": {
                    "sInfo": ""
                },
                "bFilter": false,
                "sScrollX": "100%",
                "pageLength": 300,
                "lengthChange": false,
                "columnDefs": [ {
                    "targets": ["LT_tags","LT_leadid","LT_fullname","LT_email","LT_phone","LT_provider","LT_assign","LT_fromZip","LT_toZip"],
                    "orderable": false,
                    "visible": true
                } ],
                buttons: [
                    {
                        extend: 'csv',
                        exportOptions: {
                            format: {
                                body: function (data, row, column, node) {
                                    return node.getAttribute("data-value");
                                }
                            }
                        }
                    }
                ]
            });
            var info = dTable.page.info();
            if(info.pages == 1){
                $('.dataTables_paginate').hide();
            }
            
            $("#exportCsvBtn").on("click", function() {
                dTable.button( '.buttons-csv' ).trigger();
            });
            $('.dataTables_length').addClass('bs-select');
            $('.LT_tags').removeClass('sorting_asc');
            $('.LT_tags').addClass('sorting_disabled');
            $('thead').css("background-color","rgb(245, 245, 246);");

        }
    </script>

</body>
</html>

