<?php
// This file is not in use since 16.1.20 (niv)
/*
require_once($_SERVER['LOCAL_NL_PATH']."/console/connect/connect.php");
$database = new connect();

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/lead.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingLead.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/estimateCalculation.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingSettings.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingCompany.php");

$bouncer["credentials"]["userId"] = $_GET['userId'];
$bouncer["credentials"]['orgId'] = $_GET['orgId'];

$movingCompany = new movingCompany($bouncer["credentials"]["orgId"]);
$organizationCompanyData = $movingCompany->getLicenses();

$organization = new organization($bouncer["credentials"]['orgId']);
$organizationData = $organization->getData();
$organizationPicture = $organizationData['logoPath'];

$movingSettingsData = [];
$movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
if($movingSettings->checkAndSetMovingSettings()){
    $movingSettingsData = $movingSettings->getData(true);
}

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$organizationMailAccounts = $mailaccounts->getMyAccounts(true);

if (isset($_GET['leadId'])){
    $leadId = $_GET['leadId'];
}

$estimateCalculation = new estimateCalculation($leadId);
if ($estimateCalculation->estimateHasCalculation) {
    $data = $estimateCalculation->getData();
}

$lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
$leadData = $lead->getData();

$movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
$movingLeadData = $movingLead->getData();
$totalCF = $movingLead->getInventoryTotalWeight();
if ($movingLeadData['typeOfMove'] == 1){
    $typeOfMove = "Long Distance";
}else{
    $typeOfMove = "Local Moving";
}
if ($movingLeadData['moveInventory']) {
    $movingLeadData['moveInventory'] = unserialize($movingLeadData['moveInventory']);
}
?>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
    #estimate *{
        font-family: 'Montserrat', sans-serif;;
    }
    .table-bordered {
        border: 1px solid #EBEBEB;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > thead > tr > td {
        background-color: #F5F5F6;
        border-bottom-width: 1px;
    }
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #e7e7e7;
    }
    .table > thead > tr > th {
        border-bottom: 1px solid #DDDDDD;
        vertical-align: bottom;
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td,
    .table > tfoot > tr > td {
        border-top: 1px solid #e7eaec;
        line-height: 1.42857;
        padding: 8px;
        vertical-align: top;
    }
</style>
<div style="width: 100%" id="estimate">
    <div style="width: 80%;margin: 0 auto;">
        <?php if (false && $organizationPicture && getimagesize($organizationPicture) > 0){ ?>
        <div style="margin-bottom: 25px;">
            <div style="margin-bottom: 15px;">
                <img style="height: 60px;" src="<?= $organizationPicture ?>" alt="<?= $organizationData['organizationName'] ?>">
            </div>
        </div>
        <?php } ?>
        <div style="margin-bottom: 60px;">
            <div style="text-align: center;">
                <div style="text-align: left">
                    <span style="font-size: 30px;margin-bottom: -65px;">Moving Estimate</span>
                    <span style="margin-top:-30px;float: right;">
                        <?php if ($organizationData['organizationName']) {
                            echo $organizationData['organizationName']."<br>";
                        }else{
                            echo "<br>";
                        }?>
                        <?php if ($organizationData['address']) {
                            echo $organizationData['address']."<br>";
                        }else{
                            echo "<br>";
                        }?>
                        <?php if ($organizationData['phone']) {
                            echo $organizationData['phone']."<br>";
                        }else{
                            echo "<br>";
                        }?>
                        <span style="margin-top: 10px;color: rgb(82,126,187)">
                            Job No: <?= $_GET['leadId'] ?>
                        </span>
                        <?php
                        if ($organizationCompanyData && isset($organizationCompanyData['DOT']) && $organizationCompanyData['DOT']) {
                            echo "<br> USDOT:" . $organizationCompanyData['DOT'];
                        }
                        if ($organizationCompanyData && isset($organizationCompanyData['ICCMC']) && $organizationCompanyData['ICCMC']) {
                            echo "<br> MCC:".$organizationCompanyData['ICCMC'];
                        }?>
                    </span>
                </div>
            </div>
        </div>
        <hr style="margin-bottom: 15px;">
        <div style="margin-bottom: 15px;">
            <?php if (isset($leadData['firstname']) && isset($leadData['lastname'])){ ?>
                <span style="font-weight: bold;font-size:16px;display:block;margin-bottom: 3px;">Full Name: <?php  echo $leadData['firstname']." ".$leadData['lastname'] ?></span>
            <?php } ?>
            <?php if (isset($leadData['phone'])){ ?>
                <span style="font-weight: bold;font-size:16px;display:block;margin-bottom: 3px;">Phone: <?php  echo $leadData['phone']?></span>
            <?php } ?>

            <?php if (isset($leadData['email'])){ ?>
                <span style="font-weight: bold;font-size:16px;display:block;margin-bottom: 3px;">Email: <?php  echo $leadData['email']?></span>
            <?php } ?>
        </div>
        <?php if ((isset($movingLeadData['fromAddress']) && $movingLeadData['fromAddress'] != "") || (isset($movingLeadData['fromCity']) && $movingLeadData['fromCity'] != "") || (isset($movingLeadData['fromState']) && $movingLeadData['fromState'] != "") || (isset($movingLeadData['fromZip']) && $movingLeadData['fromZip'] != "") || (isset($movingLeadData['toAddress']) && $movingLeadData['toAddress'] != "") || (isset($movingLeadData['toCity']) && $movingLeadData['toCity'] != "") || (isset($movingLeadData['toState']) && $movingLeadData['toState'] != "") || (isset($movingLeadData['toZip']) && $movingLeadData['toZip'] != "")){ ?>
        <hr style="margin-bottom: 15px;">
        <div>
            <div style="margin-top: 15px;margin-right: 15px;width: 48%;display: inline-block;">
                <h3>Moving From</h3>
                <?php if (isset($movingLeadData['fromAddress']) && $movingLeadData['fromAddress'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">Address: <?php  echo $movingLeadData['fromAddress'] ?></span>
                <?php } ?>
                <?php if (isset($movingLeadData['fromCity']) && $movingLeadData['fromCity'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">City: <?php  echo $movingLeadData['fromCity'] ?></span>
                <?php } ?>
                <?php if (isset($movingLeadData['fromState']) && $movingLeadData['fromState'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">State: <?php  echo $movingLeadData['fromState'] ?></span>
                <?php } ?>
                <?php if (isset($movingLeadData['fromZip']) && $movingLeadData['fromZip'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">ZIP Code: <?php  echo $movingLeadData['fromZip'] ?></span>
                <?php } ?>
            </div>
            <?php if ((isset($movingLeadData['toAddress']) && $movingLeadData['toAddress'] != "") || (isset($movingLeadData['toCity']) && $movingLeadData['toCity'] != "") || (isset($movingLeadData['toState']) && $movingLeadData['toState'] != "") || (isset($movingLeadData['toZip']) && $movingLeadData['toZip'] != "")){ ?>
            <div style="width: 48%;display: inline-block;">
                <h3>Moving To</h3>
                <?php if (isset($movingLeadData['toAddress']) && $movingLeadData['toAddress'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">Address: <?php  echo $movingLeadData['toAddress'] ?></span>
                <?php } ?>
                <?php if (isset($movingLeadData['toCity']) && $movingLeadData['toCity'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">City: <?php  echo $movingLeadData['toCity'] ?></span>
                <?php } ?>
                <?php if (isset($movingLeadData['toState']) && $movingLeadData['toState'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">State: <?php  echo $movingLeadData['toState'] ?></span>
                <?php } ?>
                <?php if (isset($movingLeadData['toZip']) && $movingLeadData['toZip'] != ""){ ?>
                    <span style="font-size:16px;display: block;margin-bottom: 3px;">ZIP Code: <?php  echo $movingLeadData['toZip'] ?></span>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        <hr style="margin-bottom: 15px;">
        <div>
            <div style="margin-top: 35px;margin-right: 15px;width: 48%;display: inline-block;">
                <h3>Relocation Details</h3>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Job No: <span style="float: right;"><?php  echo $_GET['leadId'] ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Estimate Date: <span style="float: right;"><?php  echo date("m/d/Y",strtotime($movingLeadData['requestedDeliveryDateStart'])) ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Move Type: <span style="float: right;"><?php  echo $typeOfMove ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Estimated Volume: <span style="float: right;"><?php  echo $estimateCalculation->calculationData['totalCF'] ?> <?php if($movingSettingsData['calculateBy'] == 0){echo "LBS";}else if($movingSettingsData['calculateBy'] == 1){echo "CF";}else if($movingSettingsData['calculateBy'] == 2){echo "Hours";} ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Move Date: <span style="float: right;"><?php  echo date("m/d/Y",strtotime($movingLeadData['moveDate'])) ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Box Delivery: <span style="float: right;"><?php  echo date("m/d/Y",strtotime($movingLeadData['boxDeliveryDate'])) ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Pickup Day: <span style="float: right;"><?php  echo date("m/d/Y",strtotime($movingLeadData['pickupDate'])) ?></span></span>
                <span style="font-size:16px;display: block;margin-bottom: 3px;">Delivery: <span style="float: right;"><?php  echo date("m/d/Y",strtotime($movingLeadData['requestedDeliveryDateStart'])) ?></span></span>
            </div>
            <div style="width: 48%;display: inline-block;vertical-align: top;margin-top: -50px;">
                <h3>Relocation Estimate</h3>
                <?php

                echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Initial Price: <span style=\"float:right;\">$".number_format($estimateCalculation->calculationData['initPrice'],2, '.', ',')."</span></span>";
                if (isset($estimateCalculation->calculationData['fuel']) && $estimateCalculation->calculationData['fuel'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Fuel (".$data['fuel']."%): <span style=\"float:right;\">$".number_format($estimateCalculation->calculationData['fuel'],2, '.', ',')."</span></span>";
                }
                if (isset($estimateCalculation->calculationData['coupon']) && $estimateCalculation->calculationData['coupon'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Coupon (".$data['coupon']."%): <span style=\"float:right;\">-$".number_format($estimateCalculation->calculationData['coupon'],2, '.', ',')."</span></span>";
                }
                if (isset($estimateCalculation->calculationData['senior']) && $estimateCalculation->calculationData['senior'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Senior Discount (".$data['senior']."%): <span style=\"float:right;\">-$".number_format($estimateCalculation->calculationData['senior'],2, '.', ',')."</span></span>";
                }
                if (isset($estimateCalculation->calculationData['veteran']) && $estimateCalculation->calculationData['veteran'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Veteran Discount (".$data['veteranDiscount']."%): <span style=\"float:right;\">-$".number_format($estimateCalculation->calculationData['veteran'],2, '.', ',')."</span></span>";
                }
                if ($estimateCalculation->calculationData['totalPacking'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Packing + Unpacking: <span style=\"float:right;\">$".number_format($estimateCalculation->calculationData['totalPacking'],2, '.', ',')."</span></span>";
                }
                if (isset($estimateCalculation->calculationData['discount']) && $estimateCalculation->calculationData['discount'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Discount (".$data['discount']."%): <span style=\"float:right;\">-$".number_format($estimateCalculation->calculationData['discount'],2, '.', ',')."</span></span>";
                }
                if (isset($estimateCalculation->calculationData['extraCharges'])){
                    foreach ($estimateCalculation->calculationData['extraCharges'] as $charge){
                        echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">".$charge['title'].": <span style=\"float:right;\">$".number_format($charge['price'],2, '.', ',')."</span></span>";
                    }
                }
                if (isset($estimateCalculation->calculationData['agentFee']) && $estimateCalculation->calculationData['agentFee'] > 0){
                    echo "<span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Agent Fee (".$data['agentFee']."%): <span style=\"float:right;\">$".number_format($estimateCalculation->calculationData['agentFee'],2, '.', ',')."</span></span>";
                }
                echo "<hr style='margin-bottom: 15px;'>";
                echo "<div style='margin-top: 15px;'><span style=\"font-size:16px;display: block;margin-bottom: 3px;\">Total Estimate: <span style=\"float:right;\">$".number_format($data['totalEstimate'],2, '.', ',')."</span></span></div>";
                ?>
            </div>
        </div>
        <?php if (isset($movingSettingsData['estimateTerms']) && $movingSettingsData['estimateTerms']){?>
        <hr style="margin-bottom: 15px;">
        <div>
            <h3 class="text-center">Understanding Your Estimate</h3>
            <p>
                <?php if (isset($movingSettingsData['estimateTerms']) && $movingSettingsData['estimateTerms']){ echo $movingSettingsData['estimateTerms']; } ?>
            </p>
        </div>
        <?php } ?>
        <?php if ($movingLeadData['moveInventory']){?>
        <div style="page-break-before: always;">
            <h2>Inventory Items</h2>
            <div style="min-height: .01%;overflow-x: auto;">
                <table style="width: 100%;" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Item Name</th>
                        <th>Cubic feet</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($movingLeadData['moveInventory'] as $invetory){?>
                        <tr>
                            <td><?php  echo $invetory['amount'] ?></td>
                            <td><?php  echo $invetory['name'] ?></td>
                            <td><?php  echo $invetory['cf'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><b>Total</b></td>
                            <td></td>
                            <td id="totalCuf"><b><?php  echo $totalCF ?></b></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <?php } ?>
        <div style="margin-top:25px;width: 100%;position: absolute;bottom: -20px;text-align: center;">
            <div style="display: inline-block;width: 30%;margin-right: 5px;">
                <span style="border-top: 1px solid black;display: block;width: 100%;"></span>
                Customer Name
            </div>
            <div style="display: inline-block;width: 30%;margin-right: 5px;">
                <span style="border-top: 1px solid black;display: block;width: 100%;"></span>
                Customer Signature
            </div>
            <div style="display: inline-block;width: 30%;margin-right: 5px;">
                <span style="border-top: 1px solid black;display: block;width: 100%;"></span>
                Date
            </div>
        </div>
    </div>
</div>
*/