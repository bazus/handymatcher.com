<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,array(["calendar",1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");
$calendar = new calendar($bouncer["credentials"]["userId"]);
$event = $calendar->getEventByID($_GET['eventId'],$bouncer["credentials"]["orgId"]);
$checkAuths_calendarEdit = $bouncer["userAuthorization"]->checkAuthorizations(array(["calendar",2]));
$checkAuths_calendarDelete = $bouncer["userAuthorization"]->checkAuthorizations(array(["calendar",4]));
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
    .text-navy{
        font-weight: bold;
    }
    .pac-container{
        z-index: 99999;
    }
    .daterangepicker{
        z-index: 9999;
    }
</style>
<script>
    var BASE_URL = "<?php echo $_SERVER['LOCAL_NL_URL']; ?>";
</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">

            <div class="modal-body" style="padding: 15px;">
                <div class="form-group">
                    <label for="eventType">Choose Calendar</label>
                    <select id='eventType' class="form-control" required name='eventType'>
                        <?php if($event['orgId'] == NULL) {?>
                            <option value="1" selected><?php echo $bouncer["userData"]["fullName"]; ?> (Private)</option>
                            <option value="2"><?php echo $bouncer["organizationData"]["organizationName"]; ?></option>
                        <?php }else{?>
                            <option value="1"><?php echo $bouncer["userData"]["fullName"]; ?> (Private)</option>
                            <option value="2" selected><?php echo $bouncer["organizationData"]["organizationName"]; ?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="eventTitle">Title</label>
                    <input required class="form-control" type="text" name="eventTitle" value="<?= $event['title'] ?>" id="eventTitle" placeholder="Event Title">
                </div>
                <div class="form-group">
                    <label for="eventDescription">Description</label>
                    <textarea class="form-control" style="resize: vertical;" name="eventDescription" id="eventDescription" placeholder="Event Description"><?= $event['description'] ?></textarea>
                </div>
                <div class="form-group">
                    <label for="eventPlace">Location</label>
                    <input class="form-control" type="text" name="eventPlace" value="<?= $event['location'] ?>"  id="eventPlace" placeholder="Event Location">
                </div>
                <div class="form-group">
                    <label for="dateS">Time</label>
                    <div id="dateS" class="form-control" style="">
                        <i class="fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                </div>
                <small id="spanError" style="color:red;"></small>
            </div>
            <div class="modal-footer">
                <?php if ($checkAuths_calendarDelete){ ?>
                <button type="button" class="ladda-button ladda-button-demo btn btn-danger pull-left" onclick="deleteEvent(<?= $_GET['eventId'] ?>)" data-dismiss="modal" data-style="zoom-in">Delete Event</button>
                <?php } ?>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <?php if ($checkAuths_calendarEdit){ ?>
                <button type="button" class="ladda-button ladda-button-demo btn btn-primary" onclick="saveEditEvent()" data-dismiss="modal" data-style="zoom-in" id="eventer">Save</button>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/geocomplete/jquery.geocomplete.min.js"></script>
<script>
    var l;
    var theStartDate;
    var theEndDate;

    $(document).ready(function() {

        $('#dateS span').html(moment("<?= $event['startDate'] ?>").format('MMMM D, YYYY HH:mm') + ' - ' + moment("<?= $event['endDate'] ?>").format('MMMM D, YYYY HH:mm'));

        $('#dateS').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment("<?= $event['startDate'] ?>"),
            endDate: moment("<?= $event['endDate'] ?>"),
            minDate: '01/01/2018',
            maxDate: '12/31/2020',
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: true,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Set',
                cancelLabel: 'Cancel',
                fromLabel: 'From Date',
                toLabel: 'To Date',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {

            $('#dateS span').html(start.format('MMMM D, YYYY HH:mm') + ' - ' + end.format('MMMM D, YYYY HH:mm'));
            theStartDate = start.format('DD-MM-YYYY HH:mm');
            theEndDate = end.format('DD-MM-YYYY HH:mm');
            document.getElementById('spanError').innerHTML = "";
            document.getElementById('eventer').disabled = false;


        });
        theStartDate = moment("<?= $event['startDate'] ?>").format('DD-MM-YYYY HH:mm');
        theEndDate = moment("<?= $event['endDate'] ?>").format('DD-MM-YYYY HH:mm');

    });
    $(document).ready(function () {
        l = $('#eventer').ladda();

        /*
        $("#eventPlace").geocomplete();
        $("#examples a").click(function(){
            $("#eventPlace").val($(this).text()).trigger("geocode");
            return false;
        });
         */
    });

    function saveEditEvent(){
        l.ladda( 'start' );

        var title = document.getElementById("eventTitle").value;

        if(title == "" || title == " "){

            document.getElementById('spanError').innerHTML = "* The Title can't be empty";

        }else{
            document.getElementById('spanError').innerHTML = "";

            var eventType = document.getElementById("eventType").value;
            console.log(eventType);
            var description = document.getElementById("eventDescription").value;
            var place = document.getElementById("eventPlace").value;

            var strUrl = BASE_URL+'/console/actions/system/calendar/updateEvent.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    id : "<?= $_GET['eventId'] ?>",
                    eventType: eventType,
                    title: title,
                    description: description,
                    place: place,
                    dateS : theStartDate,
                    dateE: theEndDate
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true,
            }).done(function(data){

                data = JSON.parse(data);
                if (data == true){

                    $('#myCustomModal').modal('hide');
                    calendarController.getEvents();

                }else{

                    document.getElementById('spanError').innerHTML = "* Error Saving Data, Please try again later";
                }
            });
        }

    }

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {

        l.ladda('stop');
        $('#myCustomModal').modal('hide');

        calendarController.getEvents();

    }

</script>