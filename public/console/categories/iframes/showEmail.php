<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,false,array(["mailcenter",1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
$mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);
$email = $mailCenter->getEmail($_GET['id']);
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<script>
    var BASE_URL = "<?php echo $_SERVER['LOCAL_NL_URL']; ?>";
</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">

            <div class="modal-body" style="padding: 15px;">
                <h2 style="margin-top: 0px;">Subject: <?= $email['subject'] ?></h2>
                <h5>From Email: <?= $email['fromEmail'] ?></h5>
                <h5>To Email: <?= $email['toEmail'] ?></h5>
                <h5>Send Date: <?= date("F j, Y H:i",strtotime($email['sentDate'])) ?></h5>
                <?php if ($email['didOpen'] == 1){echo "<h5>Opened At: ".date("F j, Y H:i",strtotime($email['openDate']))."</h5>";}?>
                <textarea id="emailContent"><?= $email['content'] ?></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!-- Date range use moment.js same as full calendar plugin -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Froala Editor -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/froala_editor.min.js"></script>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/table.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/font_family.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/font_size.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/colors.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/inline_style.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/paragraph_style.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/paragraph_format.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/align.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/quote.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/link.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/quick_insert.min.js"></script>
<script>
    $(function() {
        // init froala
        $('textarea').froalaEditor({
            key: 'nD5E2E1A1wG1G1B2C1B1D7C4E1E4H3jcpB-13tE-11hjqmkmgbg1vH4dgd==',
            height: 450,
            heightMax: 1500,
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', '|', 'fontFamily', 'fontSize', 'colors', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote','insertDropDown', '-', 'insertLink', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'],
        });
        $('textarea').froalaEditor('edit.off');
    });
    var l;

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {

        l.ladda('stop');
        $('#showEmailModal').modal('hide');

        getActiveEvents();

    }

</script>