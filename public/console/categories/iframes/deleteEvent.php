<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,false,array(["calendar",4]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

$calendar = new calendar($bouncer["credentials"]["userId"]);
$event = $calendar->getEventByID($_GET['id'],$bouncer["credentials"]["orgId"]);

?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Delete Event</h4>
                <small>Delete Event Title "<?= $event['title'] ?>".</small>
            </div>
            <form id="setEventForm" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/calendar/deleteEvent.php">
                <input type="hidden" name="eventId" value="<?= $_GET['id'] ?>">
                <div class="modal-body">
                    <h2>
                        Are You Sure You Want To Delete Event Title <br>"<?= $event['title'] ?>" ?
                    </h2>
                </div>
                <div class="modal-footer">
                    <p class="pull-left"><small>* <u>24 Hours Format</u></small></p>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-danger" data-style="zoom-in">Delete Event</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#setEventForm').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        l.ladda( 'stop' );
        $('#myCustomModal').modal('hide');

        if(statusText == "success"){
            var data = responseText;
            data = JSON.parse(data);
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };

            if(data == true){



            }else{
                // Failed
                toastr.error('Your request was not sent, please try again later.','Error');
            }
        }else{
            // Failed
            toastr.error('2Your request was not sent, please try again later.','Error');
        }
    }
</script>