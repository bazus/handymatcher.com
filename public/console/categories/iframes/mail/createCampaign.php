<?php
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadProviders.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

$organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);

$phoneNumbers = $organizationTwilio->getPhoneNumbers();
foreach($phoneNumbers as &$phoneNumber){
    $phoneNumber["prettyNumber"] = $organizationTwilio->prettifyPhone($phoneNumber["number"]);
}

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$organizationMailAccounts = $mailaccounts->getMyAccounts(true,true);

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$preEmailsData = $preEmails->getData(NULL,true);


$leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);

$leadProvidersList = $leadProviders->getProviders(true);
?>

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia-new/HTML5_Full_Version/css/animate.css" rel="stylesheet">

<style>
    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }


    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }

   .tab-content{
       display: block;
       padding: .75rem 1.25rem;
       margin-bottom: -1px;
       background-color: #fff;
       border: 1px solid rgba(0,0,0,.125);
       border-radius: 9px;
   }

    .bd-callout-info{
        border-left-color: #5BC0E1 !important;
        padding: 1.25rem;
        margin-top: 1.25rem;
        margin-bottom: 1.25rem;
        border: 1px solid #eee;
        border-left-width: .25rem;
        border-radius: .25rem;
    }

    .swal-BTN-confirm{
        background-color: #7cd1f9;
        color: #fff;
        border: none;
        box-shadow: none;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        padding: 10px 24px;
        margin: 0;
        cursor: pointer;
    }
    .swal-BTN:active {
        background-color: #70bce0 !important;
    }
    .swal-BTN:not([disabled]):hover {
        background-color: #78cbf2 !important;
    }
    .swal-BTN:focus {
        outline: none;
        box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(43, 114, 165, .29);
    }
    .swal-BTN[disabled] {
        opacity: .5;
        cursor: default;
    }

    .emailTypeListGroup{
        text-align: center;
    }
    .emailTypeListGroup>a{
        padding: 3px;
    }

    .list-group-item.list-group-item-action.active>span{
        background-color: #fff !important;
        color: #007bff !important;
    }

    .badge-secondary-custom {
        background-color: #d1dade;
        color: #5e5e5e;
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-size: 11px;
        font-weight: 600;
        padding-bottom: 4px;
        padding-left: 6px;
        padding-right: 6px;
        text-shadow: none;
    }

    #templatesArea{
        height: 434px !important;
        padding: 0px !important;
    }

    #page2>footer>button:first-child{
        left:9px !important;
    }
    #page2>footer>button:last-child{
        right:9px !important;
    }

</style>
<div id="wrapper" style="padding:20px;visibility: hidden;">
    <form id="campaignForm" name="campaignForm" method="post" style="margin-top: 13px">

        <div class="row">
            <div class="col-4">
                <div class="list-group" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-campaign-new" data-toggle="list" href="#campaign-new" role="tab" aria-controls="new">Campaign details <span class="badge badge-primary badge-pill" style="float: right;display: none;background-color: #1ab394;" id="campaignDetailsBagdeOK"><i class="fa fa fa-check"></i></span><span class="badge badge-primary badge-pill" style="float: right;display: none;background-color: #ed5565;" id="campaignDetailsBagdeFAIL"><i class="fa fa fa-times"></i></span></a>
                    <a class="list-group-item list-group-item-action" id="list-campaign-message" data-toggle="list" href="#campaign-message" role="tab" aria-controls="message">Message <span class="badge badge-primary badge-pill" style="float: right;display: none;background-color: #1ab394;margin-left: 4px;" id="campaignMessageBagdeOK"><i class="fa fa fa-check"></i></span><span class="badge badge-primary badge-pill" style="float: right;display: none;background-color: #ed5565;margin-left: 4px;" id="campaignMessageBagdeFAIL"><i class="fa fa fa-times"></i></span> <span class="badge badge-primary badge-pill" style="float: right;display: none; background-color: #1ab394;margin-left: 4px;" id="campaignTypeBagde"></span></a>
                    <a class="list-group-item list-group-item-action" id="list-campaign-schedule" data-toggle="list" href="#campaign-schedule" role="tab" aria-controls="schedule">Schedule your message <span class="badge badge-primary badge-pill" style="float: right;display: none;background-color: #1ab394;margin-left: 4px;" id="campaignScheduleBagdeOK"><i class="fa fa fa-check"></i></span><span class="badge badge-primary badge-pill" style="float: right;display: none;background-color: #ed5565;margin-left: 4px;" id="campaignScheduleBagdeFAIL"><i class="fa fa fa-times"></i></span></a>
                    <a class="list-group-item list-group-item-action" id="list-campaign-audience" data-toggle="list" href="#campaign-audience" role="tab" aria-controls="audience" style="max-height: 305px;overflow-y: scroll;">Choose your audience</a>
                </div>
            </div>
            <div class="col-8">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="campaign-new" role="tabpanel" aria-labelledby="list-campaign-new">


                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control input-sm" onkeyup="createCampaignController.check.details();" id="campaignName" name="campaignName" placeholder="Campaign Name">
                            </div>
                        </div>



                    </div>
                    <div class="tab-pane fade show" id="campaign-message" role="tabpanel" aria-labelledby="list-campaign-message">

                        <div class="row">
                            <div class="col-md-12">


                                <div class="row">
                                    <div class="col-12">
                                        <div class="list-group list-group-horizontal emailTypeListGroup" role="tablist">
                                            <a class="list-group-item list-group-item-action" id="list-sms-list" data-toggle="list" href="#list-sms" role="tab" aria-controls="sms" onclick="createCampaignController.selectedCampaignType(1)">SMS</a>
                                            <a class="list-group-item list-group-item-action" id="list-email-list" data-toggle="list" href="#list-email" role="tab" aria-controls="email" onclick="createCampaignController.selectedCampaignType(2)">Email</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12" style="padding: 0px;">
                                        <div class="tab-content" style="border: none;padding: 0px;padding-left: 9px;padding-right: 9px;">
                                            <div class="tab-pane fade" id="list-sms" role="tabpanel" aria-labelledby="list-home-list">



                                                <div id="page1" style="margin-top: 11px;padding:4px;width: 100%;height: 483px; overflow-x: hidden;overflow-y: hidden;">
                                                    <h3 style="text-align: left;font-size: 13px;">From phone number</h3>
                                                    <select id="sendSMSModalFromPhoneNumberID" name="fromPhoneNumberId" onchange="createCampaignController.check.message();" class="form-control" style="margin-bottom: 13px;" <?php if(count($phoneNumbers) == 0){ ?> disabled <?php } ?>>
                                                        <?php
                                                        if(count($phoneNumbers) == 0){
                                                            ?>
                                                            <option value="">No phone numbers</option>
                                                            <?php
                                                        }else{
                                                            foreach ($phoneNumbers as &$phoneNumber){
                                                                ?>
                                                                <option value="<?= $phoneNumber["id"] ?>"><?= $phoneNumber["prettyNumber"] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>

                                                    <?php if(count($phoneNumbers) == 0){ ?>
                                                        <div style="background-color: rgb(254, 250, 227);padding: 17px;border: 1px solid rgb(240, 225, 161);display: block;margin-top: 13px;margin-bottom: 13px;text-align: center;color: rgb(97, 83, 78);">
                                                            <a target="_blank" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/mailSettings.php" style="color: unset;text-decoration: underline;">Click here to assign a new phone number</a>
                                                        </div>
                                                    <?php } ?>

                                                    <h3 style="text-align: left;font-size: 13px;">
                                                        Message
                                                        <a href="#" onclick="SMSTemplatesController.showTemplates()" style="font-size: 13px;float: right;color: cornflowerblue;text-decoration: none;">Apply template</a>
                                                    </h3>

                                                    <textarea id="sendSMSModalMessage" name="smsTextMessage" class="form-control" onkeyup="createCampaignController.check.message();" style="text-align: left;font-size: 81%;padding: 11px;height: 100px;" placeholder="Enter text.."></textarea>

                                                    <div style="text-align: left;margin-top: 13px;margin-bottom: 13px;">
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[FirstName]');">First name</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[LastName]');">Last name</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[CompanyName]');">Company Name</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[inventorylink]');">Update inventory link</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[FromState]');">From state</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[ToState]');">To state</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[FromCity]');">From city</span>
                                                        <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[ToCity]');">To city</span>
                                                    </div>

                                                </div>
                                                <div id="page2" class="animated fadeOut" style="margin-top: 11px;padding:0px;width: 100%;height: 483px; overflow-x: hidden; display: none;">
                                                    <?php
                                                        require_once( $_SERVER['LOCAL_NL_PATH']."/console/categories/iframes/system/sendSMS_templates.php");
                                                    ?>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="list-email" role="tabpanel" aria-labelledby="list-profile-list" style="padding: 9px;">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="emailAccountId">From email account</label>
                                                        <select class="pull-right form-control settingsForm" id="emailAccountId" name="emailAccountId" onchange="createCampaignController.check.message();">
                                                            <option value="">Select account</option>
                                                            <?php
                                                            foreach($organizationMailAccounts as $organizationMailAccount){?>
                                                                <option value="<?php echo $organizationMailAccount['id']; ?>"><?php echo $organizationMailAccount['email']; ?></option>
                                                                <?php
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 9px;">
                                                    <div class="col-md-12">
                                                        <label for="estimateEmailId">Email Template</label>
                                                        <select class="pull-right form-control settingsForm" id="emailTemplateId" name="emailTemplateId" onchange="createCampaignController.check.message();">
                                                            <option value="">Select template</option>
                                                            <?php
                                                            foreach ($preEmailsData as $preEmailData){ ?>
                                                                <option value="<?php echo $preEmailData['id']; ?>"><?php echo $preEmailData['title']; ?></option>
                                                                <?php
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>



                    </div>
                    <div class="tab-pane fade" id="campaign-schedule" role="tabpanel" aria-labelledby="list-campaign-schedule">


                        <div class="row">
                            <div class="col-md-12">
                                <label for="actionType">When?</label>
                                <select class="form-control input-sm" id="actionType" name="actionType" onchange="createCampaignController.toggleDaysAfter(this);createCampaignController.check.schedule();">
                                    <option value="">Select schedule</option>
                                    <option value="1">When new lead arrives</option>
                                    <option value="2">[X] days after new lead arrives</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 13px;">
                            <div class="col-md-12" style="display: none" id="daysAfterDiv">
                                <label>Days after</label>

                                <div class="input-group mb-3">
                                    <input type="number" class="form-control input-sm" id="daysAfter" name="daysAfter" placeholder="" onkeyup="createCampaignController.check.schedule();">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2"> Days after lead arrives</span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="campaign-audience" role="tabpanel" aria-labelledby="list-campaign-audience">


                        <p>All leads that match the <code class="highlighter-rouge">following rules</code> will receive your message.</p>

                       <div id="audienceRules"></div>
                        <div style="text-align: center;margin-top: 13px;">
                            <a href=#" onclick="createCampaignController.addAudienceRule();">Add rule</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="bd-callout-info" style="bottom: 49px;width: 95%;position: absolute;">
            <p>The animation effect of this component is dependent on the <code class="highlighter-rouge">prefers-reduced-motion</code> media query. See the <a href="/docs/4.3/getting-started/accessibility/#reduced-motion">reduced motion section of our accessibility documentation</a>.</p>
        </div>

    </form>
    <footer style="bottom: 13px;right: 13px;position: absolute;">
        <button class="swal-BTN swal-BTN-confirm" onclick="createCampaignController.submitCampaign();">Activate Campaign</button>
    </footer>
</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/popper.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<script>
    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });

    // Override the default functions
    SMSTemplatesController.showTemplates = function () {
        $('#page2').css("display", "");
        $('#page2').removeAttr('class').attr('class', '');
        $('#page2').addClass('animated');
        $('#page2').addClass("fadeIn");

        $('#page1').removeAttr('class').attr('class', '');
        $('#page1').addClass('animated');
        $('#page1').addClass("fadeOut");
        $('#page1').css("display", "none");

    };
    SMSTemplatesController.backFromTemplates = function () {
        $('#page2').removeAttr('class').attr('class', '');
        $('#page2').addClass('animated');
        $('#page2').addClass("fadeOut");
        $('#page2').css("display", "none");

        setTimeout(function () {
            $('#page2').css("display", "none");
        }, 1000);

        $('#page1').removeAttr('class').attr('class', '');
        $('#page1').addClass('animated');
        $('#page1').addClass("fadeIn");
        $('#page1').css("display", "");

    };
    SMSTemplatesController.setTemplate = function (data) {
        var txtarea = document.getElementById("sendSMSModalMessage");
        txtarea.innerHTML = "";
        txtarea.value = "";

        SMScontroller.updateMessageArea(data);
        SMSTemplatesController.backFromTemplates();
    }

    var SMScontroller = {
        updateMessageArea: function(text) {
            var txtarea = document.getElementById("sendSMSModalMessage");
            if (!txtarea) {
                return;
            }

            var scrollPos = txtarea.scrollTop;
            var strPos = 0;
            var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
                "ff" : (document.selection ? "ie" : false));
            if (br == "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                strPos = range.text.length;
            } else if (br == "ff") {
                strPos = txtarea.selectionStart;
            }

            var front = (txtarea.value).substring(0, strPos);
            var back = (txtarea.value).substring(strPos, txtarea.value.length);
            txtarea.value = front + text + back;
            strPos = strPos + text.length;
            if (br == "ie") {
                txtarea.focus();
                var ieRange = document.selection.createRange();
                ieRange.moveStart('character', -txtarea.value.length);
                ieRange.moveStart('character', strPos);
                ieRange.moveEnd('character', 0);
                ieRange.select();
            } else if (br == "ff") {
                txtarea.selectionStart = strPos;
                txtarea.selectionEnd = strPos;
                txtarea.focus();
            }

            txtarea.scrollTop = scrollPos;
        }
    }

    var createCampaignController = {
        rulesCount:0,
        campaignType:null,
        selectedCampaignType:function(type) {
            createCampaignController.campaignType = type;
            document.getElementById("campaignTypeBagde").style.display = "";
            if(type == 1){
                document.getElementById("campaignTypeBagde").innerHTML = "SMS";
            }
            if(type == 2){
                document.getElementById("campaignTypeBagde").innerHTML = "Email";
            }

            createCampaignController.check.message();
        },
        addAudienceRule:function(){

            createCampaignController.rulesCount++;

            var audienceRules = document.getElementById("audienceRules");

            var div = document.createElement("div");
            div.className = "input-group ruleGroup";


            var div1 = document.createElement("div");
            div1.className = "input-group-prepend";

            // Selecter
            var select = document.createElement("select");
            select.className = "form-control";
            select.name = "ruleSelector[]";
            select.id = "ruleSelector"+createCampaignController.rulesCount;
            select.setAttribute("style","-webkit-appearance: none;width: 130px;");
            select.setAttribute("onChange","createCampaignController.changeRuleApply(this,"+createCampaignController.rulesCount+")");

            select.innerHTML += "<option value='fromCity'>From City</option>";
            select.innerHTML += "<option value='fromState'>From State</option>";
            select.innerHTML += "<option value='fromZip'>From Zip</option>";
            select.innerHTML += "<option value='toCity'>To City</option>";
            select.innerHTML += "<option value='toState'>To State</option>";
            select.innerHTML += "<option value='toZip'>To Zip</option>";
            select.innerHTML += "<option value='leadProvider'>Lead Provide</option>";

            div1.appendChild(select);

            var div2 = document.createElement("div");
            div2.setAttribute("style","margin-left: 3px;");

            // Apply 1
            var select = document.createElement("select");
            select.className = "form-control";
            select.setAttribute("style","-webkit-appearance: none;width: 130px;");
            select.name = "ruleApply1[]";
            select.id = "ruleApply1"+createCampaignController.rulesCount;

            select.innerHTML += "<option value='1'>is</option>";
            select.innerHTML += "<option value='2'>is not</option>";
            select.innerHTML += "<option value='3'>starts with</option>";
            select.innerHTML += "<option value='4'>ends with</option>";
            select.innerHTML += "<option value='5'>contains</option>";
            select.innerHTML += "<option value='6'>does not contain</option>";

            div2.appendChild(select);

            // Apply 2
            var select = document.createElement("select");
            select.className = "form-control";
            select.setAttribute("style","-webkit-appearance: none; display:none;width: 130px;");
            select.name = "ruleApply2[]";
            select.id = "ruleApply2"+createCampaignController.rulesCount;

            select.innerHTML += "<option value='1'>is</option>";
            select.innerHTML += "<option value='2'>is not</option>";

            div2.appendChild(select);

            var div3 = document.createElement("div");
            div3.className = "input-group-append";
            div3.setAttribute("style","margin-left: 3px;");


            var input = document.createElement("input");
            input.className = "form-control";
            input.type = "text";
            input.name = "ruleValue1[]";
            input.id = "ruleValue1"+createCampaignController.rulesCount;
            input.setAttribute("style","width: 219px;");


            div3.appendChild(input);

            var select = document.createElement("select");
            select.className = "form-control";
            select.setAttribute("style","-webkit-appearance: none; display:none;width: 219px;");
            select.name = "ruleValue2[]";
            select.id = "ruleValue2"+createCampaignController.rulesCount;

            var leadProvidersList = JSON.parse('<?php echo json_encode($leadProvidersList); ?>');

            if(leadProvidersList.length == 0){
                select.disabled = true;
                select.innerHTML += "<option value=''>No providers</option>";
            }else{
                for(var i = 0;i<leadProvidersList.length;i++){
                    select.innerHTML += "<option value='"+leadProvidersList[i].id+"'>"+leadProvidersList[i].providerName+"</option>";
                }
            }

            div3.appendChild(select);

            if(document.getElementsByClassName('ruleGroup').length > 0){
                var div4 = document.createElement("div");
                div4.className = "ruleAnd";
                div4.innerHTML = "AND";
                div4.setAttribute("style","color: #5b5b5b;text-align:center;margin:3px;width: 100%;font-size: 13px;");

                audienceRules.appendChild(div4);
            }

            var deleteBtn = document.createElement("button");
            deleteBtn.setAttribute("style","margin-right: 3px;font-size: 30px;outline: none;");
            deleteBtn.setAttribute("onclick","createCampaignController.removeRule(this)");
            deleteBtn.type = "button";
            deleteBtn.className = "close";
            deleteBtn.innerHTML = '<span aria-hidden="true">×</span><span class="sr-only">Close</span>';

            div.appendChild(deleteBtn);

            div.appendChild(div1);
            div.appendChild(div2);
            div.appendChild(div3);

            audienceRules.appendChild(div);
        },
        removeRule:function(obj) {
            var elem = obj.parentNode;

            var nextSibling = elem.nextSibling;
            elem.parentNode.removeChild(elem);
            if(nextSibling!= null && nextSibling.className == "ruleAnd"){
                nextSibling.parentNode.removeChild(nextSibling);
            }

        },
        changeRuleApply:function(obj,count){

            if(obj.value == "leadProvider"){
                document.getElementById("ruleApply1"+count).style.display = "none";
                document.getElementById("ruleApply2"+count).style.display = "";

                document.getElementById("ruleValue1"+count).style.display = "none";
                document.getElementById("ruleValue2"+count).style.display = "";
            }else{
                document.getElementById("ruleApply2"+count).style.display = "none";
                document.getElementById("ruleApply1"+count).style.display = "";

                document.getElementById("ruleValue2"+count).style.display = "none";
                document.getElementById("ruleValue1"+count).style.display = "";
            }
        },
        toggleDaysAfter:function(obj){
            if(obj.value == "2"){
                document.getElementById("daysAfterDiv").style.display = "";
            }else{
                document.getElementById("daysAfterDiv").style.display = "none";
            }
        },
        submitCampaign:function(){
            if(createCampaignController.check.all()){
                $('#campaignForm').submit();
            }
        },
        check:{
            all:function(){
                console.log(createCampaignController.check.details());
                console.log(createCampaignController.check.message());
                console.log(createCampaignController.check.schedule());
                return (createCampaignController.check.details() && createCampaignController.check.message() && createCampaignController.check.schedule());
            },
            details:function () {
                var campaignName = document.getElementById("campaignName").value;

                var isOk = false;
                if(campaignName != ""){
                    isOk = true;
                }

                var campaignDetailsBagdeOK = document.getElementById("campaignDetailsBagdeOK");
                var campaignDetailsBagdeFAIL = document.getElementById("campaignDetailsBagdeFAIL");

                campaignDetailsBagdeOK.style.display = "none";
                campaignDetailsBagdeFAIL.style.display = "none";
                if(isOk){
                    campaignDetailsBagdeOK.style.display = "";
                }else{
                    campaignDetailsBagdeFAIL.style.display = "";
                }

                return isOk;
            },
            message:function () {
                var isOk = false;

                if(createCampaignController.campaignType == null){
                    isOk = false;
                }else{
                    if(createCampaignController.campaignType == 1){
                        var sendSMSModalFromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
                        var sendSMSModalMessage = document.getElementById("sendSMSModalMessage").value;

                        if(sendSMSModalFromPhoneNumberID != "" && sendSMSModalFromPhoneNumberID != false && sendSMSModalMessage!= "" && sendSMSModalMessage != false){
                            isOk = true;
                        }else{
                            isOk = false;
                        }
                    }

                    if(createCampaignController.campaignType == 2){
                        var emailAccountId = document.getElementById("emailAccountId").value;
                        var emailTemplateId = document.getElementById("emailTemplateId").value;

                        if(emailAccountId != "" && emailAccountId != false && emailTemplateId!= "" && emailTemplateId != false){
                            isOk = true;
                        }else{
                            isOk = false;
                        }
                    }
                }

                var campaignMessageBagdeOK = document.getElementById("campaignMessageBagdeOK");
                var campaignMessageBagdeFAIL = document.getElementById("campaignMessageBagdeFAIL");
                var campaignTypeBagde = document.getElementById("campaignTypeBagde");

                campaignMessageBagdeOK.style.display = "none";
                campaignMessageBagdeFAIL.style.display = "none";
                if(isOk){
                    campaignMessageBagdeOK.style.display = "";
                    campaignTypeBagde.style.backgroundColor = "#1ab394";
                }else{
                    campaignMessageBagdeFAIL.style.display = "";
                    campaignTypeBagde.style.backgroundColor = "#ed5565";
                }
                return isOk;
            },
            schedule:function () {
                var isOk = false;

                var actionType = document.getElementById("actionType").value;
                if(actionType == "" || actionType == false){return false;}

                if(actionType == 1) {
                    isOk = true;
                }
                if(actionType == 2){
                    var daysAfter = document.getElementById("daysAfter").value;
                    if(daysAfter == "" || daysAfter == false){
                        isOk = false;
                    }else{
                        isOk = true;
                    }
                }

                var campaignScheduleBagdeOK = document.getElementById("campaignScheduleBagdeOK");
                var campaignScheduleBagdeFAIL = document.getElementById("campaignScheduleBagdeFAIL");

                campaignScheduleBagdeOK.style.display = "none";
                campaignScheduleBagdeFAIL.style.display = "none";
                if(isOk){
                    campaignScheduleBagdeOK.style.display = "";
                }else{
                    campaignScheduleBagdeFAIL.style.display = "";
                }
                return isOk;
            }
        }
    };

    $("#campaignForm").submit(function(e) {
        e.preventDefault();

        var form = $(this);
        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/campaigns/createCampaign.php';

        console.log(form);

        $.ajax({
            type: "POST",
            url: strUrl,
            data: form.serialize() // serializes the form's elements.
        }).done(function (data) {
            console.log(data);
            parent.swal.close();
            parent.emailCampaignsController.getCampaigns();

        });

    });

</script>