<?php
$pagePermissions = array(false,true,true,true);
// todos change permissions by package
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <style>
        #step1,#step2{
            padding: 13px;
            text-align: center;
        }
        #step2{
            text-align: left;
        }
        .mailBTN{
            width: 100px;
            height: 100px;
            border: 1px solid;
            border-color: rgb(127, 209, 222);
            border-radius: 11px;
            background-color: #fff;
            text-align: center;
            padding: 5px;
            display: inline-table;
            margin-left: 3px;
            margin-right: 3px;
            cursor: pointer;
        }
        .mailBTN:hover{
            box-shadow: rgba(0, 163, 189, 0.3) 0px 0px 0px 1px, rgba(0, 163, 189, 0.3) 0px 0px 12px 0px;
        }
        .mailBTN>img{
            width: 43px;
            border-radius: 6px;
        }
        .mailBTN>span{
            white-space: normal;
            display: block;
            margin-top: 9px;
        }
        .mailBTNS>a{
            color: unset;
        }
        .mailH2{
            font-weight: 400;
            font-size: 21px;
            font-family: Avenir Next W02,Helvetica,Arial,sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
        }
    </style>
</head>
<body style="background-color: #fff;" class="iframeSettingsBody">

<div class="animated fadeIn">
    <div id="step1">
    <h2 class="mailH2">Choose your email provider</h2>
    <div class="mailBTNS">
        <div class="mailBTN" onclick="window.top.location.href = '<?php  echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/API/authorizeGoogleAPI.php?app=gmail';">
            <img alt="Google/Gmail" class="" src="<?php echo $_SERVER["LOCAL_NL_URL"]; ?>/console/images/icons/emails/google.svg">
            <span>
                Google / Gmail
            </span>
        </div>
        <div class="mailBTN" onclick="window.top.location.href = '<?php  echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/API/authorizeMicrosoftAPI.php';">
            <img alt="" class="" src="<?php echo $_SERVER["LOCAL_NL_URL"]; ?>/console/images/icons/emails/office.png">
            <span>
                Office 365
            </span>
        </div>
        <div class="mailBTN" onclick="setEmailSettings(2)">
            <img alt="" class="" src="<?php echo $_SERVER["LOCAL_NL_URL"]; ?>/console/images/icons/emails/yahoo.jpg">
            <span>
                Yahoo
            </span>
        </div>
        <div class="mailBTN" onclick="setEmailSettings(0)">
            <img alt="" class="" src="<?php echo $_SERVER["LOCAL_NL_URL"]; ?>/console/images/icons/emails/other.svg">
            <span>
                Custom
            </span>
        </div>
    </div>
</div>
    <div id="step2" style="display: none;">


    <a onclick="goto_step1()" class="btn btn-default btn-rounded" href="#"><span><i class="fa fa-arrow-left"></i> Back</span></a>
    <div style="border-top: 1px dashed #e7eaec;color: #ffffff;background-color: #ffffff;height: 1px;margin-top: 10px;margin-bottom: 10px"></div>
    <form method="post" class="form-horizontal">
        <div id="mailAddressSettings">
            <div class="form-group">
                <div class="col-sm-8"><input type="email" name="fullName" id="fullName" placeholder="Full Name" class="form-control" onkeyup="checkForm()"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-8"><input type="email" name="emailAddress" id="emailAddress" placeholder="Email Address" class="form-control" onkeyup="checkForm()"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-8"><input type="password" name="emailPassword" id="emailPassword" placeholder="Email Password" class="form-control" onkeyup="checkForm()"></div>
            </div>
        </div>
        <div id="mailServerSettings" style="display: none;">
            <div class="form-group">
                <div class="col-sm-8"><input type="text" name="outgoingmailserver" id="outgoingmailserver" onkeyup="checkForm()" placeholder="Example: smtp.gmail.com" class="form-control"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-8"><input type="text" name="emailPort" id="emailPort" onkeyup="checkForm()" placeholder="Example : 587" class="form-control"></div>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                    <div class="checkbox checkbox-primary">
                        <input type="checkbox" name="requiredSSL" id="requiredSSL" onkeyup="checkForm()">
                        <label for="requiredSSL" style="display: block;"> Use Secured Connection</label>
                        <span style="font-weight: 200;">* check only if the smtp server needs a secured connection (SSL)</span>
                    </div>
                </div>
            </div>
        </div>

        <p id="yahooHelp">
            Make sure the email settings is turned ON for <a href="https://help.yahoo.com/kb/SLN27791.html" target="_blank" title="allow less secure apps" style="text-decoration: underline">Less Secure Apps</a>.<br>
            If you have a business account you need to <a href="https://help.smallbusiness.yahoo.net/s/article/SLN29264" target="_blank" title="Create an app password" style="text-decoration: underline">create an app password</a>.
        </p>



    </form>

    <div style="text-align: center">
        <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="createAccount()" id="createAccount" disabled="">
            <span class="ladda-label">Create</span>
            <span class="ladda-spinner"></span></button>
    </div>
</div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var mailType = null;

    function setEmailSettings(type) {

        mailType = type;
        document.getElementById('mailServerSettings').style.display = "none";
        document.getElementById('yahooHelp').style.display = "none";

        if(type == 2){
            document.getElementById('yahooHelp').style.display = "";
        }
        if(type == 0){
            document.getElementById('mailServerSettings').style.display = "";
        }

        goto_step2();
    }

    function goto_step1(){
        document.getElementById("step1").style.display = "";
        document.getElementById("step2").style.display = "none";
        parent.adjustMailSettingIframeHeight($("#step1").height()+50);
    }
    function goto_step2(){

        // Reset inputs
        document.getElementById('outgoingmailserver').value = "";
        document.getElementById('fullName').value = "";
        document.getElementById('emailAddress').value = "";
        document.getElementById('emailPassword').value = "";
        document.getElementById('emailPort').value = "";

        document.getElementById("step1").style.display = "none";
        document.getElementById("step2").style.display = "";
        parent.adjustMailSettingIframeHeight($("#step2").height()+50);
    }

    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    function createAccount(){
        if(mailType === null){return;}
        l.ladda( 'start' );

        var outgoingmailserver = document.getElementById('outgoingmailserver').value;
        var fullName = document.getElementById('fullName').value;
        var emailAddress = document.getElementById('emailAddress').value;
        var emailPassword = document.getElementById('emailPassword').value;
        var emailPort = document.getElementById('emailPort').value;
        var requiredSSL = document.getElementById('requiredSSL').checked;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/createAccount.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                outgoingmailserver:outgoingmailserver,
                fullName:fullName,
                emailAddress:emailAddress,
                emailPassword:emailPassword,
                emailPort:emailPort,
                accountType:mailType,
                requiredSSL:requiredSSL
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda('stop');

            try{
                data = JSON.parse(data);

                if(data.status == true) {
                    parent.getEmailsAccountData();
                    parent.testMailConnection(data.accountId);
                }else{
                    swal("Your changes were not saved. Please try again later","","error");
                }

            }catch (e){
                swal("Your changes were not saved. Please try again later","","error");
            }

        });
    };

    function checkForm(){
        document.getElementById('createAccount').disabled = true;

        var fullName = document.getElementById('fullName').value;
        var emailAddress = document.getElementById('emailAddress').value;
        var emailPassword = document.getElementById('emailPassword').value;
        var outgoingmailserver = document.getElementById('outgoingmailserver').value;
        var emailPort = document.getElementById('emailPort').value;

        if(mailType == 2){
            if(fullName != "" && emailAddress != "" && emailPassword != ""){
                document.getElementById('createAccount').disabled = false;
            }
        }
        if(mailType == 0){
            if(fullName != "" && emailAddress != "" && emailPassword != "" && outgoingmailserver != "" && emailPort != ""){
                document.getElementById('createAccount').disabled = false;
            }
        }
    }
    checkForm();


    function showFullNameInfo(){
        swal({
            title: "Full Name",
            text: "This is the Name that will feature in your outgoing emails from the software and what recipients will see in their inbox.",
            buttons: {
                skip:{
                    text:"Got It"
                }
            }
        });
    }
</script>
</body>
</html>