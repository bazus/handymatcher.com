<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

?>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 0;
    }
</style>
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn" style="height: 100%;">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Trucks</h4>
                <small>You can create your own Trucks.</small>
            </div>
            <form id="groupForm">
                <div style="padding: 13px;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
                                <div class="input-group">
                                <?php } ?>
                                    <input id="title" class="form-control" onkeyup="setNameError()" type="text" required name="truckTitle" placeholder="Truck Name">
                                <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
                                    <span class="input-group-addon">
                                        <span onclick="showTruckNameInfo()" style="cursor:pointer;">
                                            <i style="font-size: 20px;" class="fa fa-question"></i>
                                        </span>
                                    </span>
                                </div>
                            <?php } ?>
                                <span id="nameError" style="display: none;"><small class="text-danger">* Name Is Required</small></span>
                            </div>
                            <div class="form-group">
                                <input id="description" class="form-control" type="text" name="truckDescription" placeholder="Truck Description">
                            </div>
                            <div class="form-group">
                                <input id="model" class="form-control" type="text" name="truckModel" placeholder="Truck Model">
                            </div>
                            <div class="form-group">
                                <input id="year" class="form-control" type="text" name="truckYear" placeholder="Truck Mfc. Year">
                            </div>
                            <div class="form-group">
                                <input id="vin" class="form-control" type="text" name="truckVIN" placeholder="Truck VIN">
                            </div>
                            <div class="form-group">
                                <input id="plate" class="form-control" type="text" name="truckPlate" placeholder="Truck Plate Number">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="tire" class="form-control" type="text" name="truckTire" placeholder="Truck Tire Size">
                            </div>
                            <div class="form-group">
                                <input id="state" class="form-control" type="text" name="truckState" placeholder="Truck State">
                            </div>
                            <div class="form-group">
                                <input id="capacity" class="form-control" type="number" min="0" name="truckCapacity" placeholder="Truck Capacity (Cubic Feet)">
                            </div>
                            <div class="form-group">
                                <div class="form-group" id="data_1">
                                    <div class="input-group date" style="width: 100%;">
                                        <input  name="truckPurchaseDate" id="purchaseDate" type="text" class="form-control" placeholder="Truck Purchase Date">
                                    </div>
                                </div>
<!--                                <input id="purchaseDate" class="form-control" type="date" name="truckPurchaseDate" placeholder="Enter Date">-->
                            </div>
                            <div class="form-group">
                                <input id="purchasePrice" class="form-control" min="0" type="number" name="truckPurchasePrice" placeholder="Truck Purchase Price">
                            </div>
                            <div class="form-group">
                                <input id="purchaseMiles" class="form-control" min="0" type="number" name="truckPurchaseMiles" placeholder="Truck Purchase Miles">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="position:absolute; bottom: 0; width: 100%;">
                    <button style="float: left" type="button" class="btn btn-white" id="closeBtn" onclick="parent.swal.close()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addTruck()" data-style="zoom-in">Create Truck</button>
                </div>
            </form>

        </div>
    </div>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Data picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>



<script>

    $(function() {
        $("#purchaseDate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10)
            }
        );
    });



    function addTruck(){

        var title = document.getElementById("title").value;
        if (title == "" || title == " "){
            $("#nameError").css("display","");
        }else{
            var description = document.getElementById("description").value;
            var model = document.getElementById("model").value;
            var year = document.getElementById("year").value;
            var vin = document.getElementById("vin").value;
            var plate = document.getElementById("plate").value;
            var tire = document.getElementById("tire").value;
            var state = document.getElementById("state").value;
            var capacity = document.getElementById("capacity").value;
            var purchaseDate = document.getElementById("purchaseDate").value;
            var purchasePrice = document.getElementById("purchasePrice").value;
            var purchaseMiles = document.getElementById("purchaseMiles").value;

            var strUrl = BASE_URL+'/console/actions/moving/trucks/addTruck.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    title:title,
                    description:description,
                    model:model,
                    year:year,
                    vin:vin,
                    plate:plate,
                    tire:tire,
                    state:state,
                    capacity:capacity,
                    purchaseDate:purchaseDate,
                    purchasePrice:purchasePrice,
                    purchaseMiles:purchaseMiles,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                data = JSON.parse(data);
                $("#closeBtn").click();
                parent.getTrucks();
                parent.getTruck(data);
            });
        }

    }
    function setNameError(){
        var title = document.getElementById("title").value;
        if (title == "" || title == " "){
            $("#nameError").css("display","");
        }else{
            $("#nameError").css("display","none");
        }
    }
    function showTruckNameInfo(){
        swal({
            text: 'Giving a name to a truck will help you identify it later',
            buttons: {
                skip:{
                    text:"Got It",
                }
            }
        });
    }

</script>