<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 0;
    }
    @media only screen and (max-width: 380px) {
        #closeBtn{
            float:left;
        }
        #companyDetailsBtn {
            margin-top: 5px !important;
        }
        #city, #state, #zip{
            width: 100% !important;
        }
        #state, #zip{
            margin-top: 15px;
        }
        #right-hr{
            margin-top:110px;
        }
    }
</style>

    <div class="modal-dialog">

        <div class="modal-content animated fadeIn" style="height: 100%;">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add External Storage Facilities Details</h4>
                <small>You can create your own storages.</small>
            </div>
            <form id="groupForm">
                <div>
                    <div class="row" style="display: flex">
                        <div class="col-md-6 modal-body" style="width: 50%; padding-right:5px">
                            <div class="form-group">
                                <input id="title" class="form-control input-sm" onkeyup="setNameError()" required type="text" name="Title" placeholder="Facility Name">
                                <span id="nameError" style="display: none;"><small class="text-danger">* Name Is Required</small></span>
                            </div>
                            <div class="form-group">
                                <input id="address" class="form-control input-sm" onkeyup="setAddressError()" type="text" required name="Address" placeholder="Billing Address">
                                <span id="addressError" style="display: none;"><small class="text-danger">* Billing Address Is Required</small></span>
                            </div>
                            <div class="form-group">
                                <input id="city" class="form-control input-sm" style="width: 32%; display: inline-table;" type="text" name="City" placeholder="City">
                                <input id="state" class="form-control input-sm" style="width: 32%; display: inline-table;" type="text" name="State" placeholder="State">
                                <input id="zip" class="form-control input-sm" style="width: 32%; display: inline-table;" type="text" name="ZIP" placeholder="ZIP">
                            </div>
                            <hr>
                            <div class="form-group input-group">
                                <span class="input-group-addon">$</span>
                                <input id="pricePer100" class="form-control input-sm" type="number" min="0" name="pricePer100" placeholder="Price Per 100 Cuf.">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">$</span>
                                <input id="pricePerCrate" class="form-control input-sm" type="number" min="0" name="pricePerCrate" placeholder="Price Per Crate">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">$</span>
                                <input id="lateFee" class="form-control input-sm" type="number" min="0" name="lateFee" placeholder="Late Fee">
                            </div>
                        </div>
                        <div class="col-md-6 modal-body" style="width: 50%; padding-left:5px;">
                            <div class="form-group">
                                <input id="contact" class="form-control input-sm" type="text" name="Contact" placeholder="Contact Name">
                            </div>
                            <div class="form-group">
                                <input id="email" class="form-control input-sm" type="text" name="Email" placeholder="Contact Email">
                            </div>
                            <div class="form-group">
                                <input id="phone" class="form-control input-sm" type="text" name="Phone" placeholder="Contact Phone">
                            </div>
                            <hr id="right-hr">
                            <div class="form-group input-group">
                                <span class="input-group-addon">$</span>
                                <input id="pricePer1000" class="form-control input-sm" type="number" min="0" name="pricePer1000" placeholder="Price Per 1000 Lbs">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">%</span>
                                <input id="tax" class="form-control input-sm" type="number" min="0" max="100" name="tax" placeholder="Tax">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">$</span>
                                <input id="flatRate" class="form-control input-sm" type="number" min="0" name="flatRate" placeholder="Flat Rate">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-left:20px;">
                        <small>* All prices are per day</small>
                    </div>
                </div>

                <div class="modal-footer" style="position:absolute; bottom: 0; width: 100%;">
                    <button type="button" class="btn btn-white" id="closeBtn" onclick="parent.swal.close()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addStorage()" data-style="zoom-in">Create Storage</button>
                    <button id="companyDetailsBtn" type="button" onclick="insertOrgData(event)" class="btn btn-success pull-left" style="margin: 0;">Use company details</button>
                </div>
            </form>

        </div>
    </div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>

<script>

    function addStorage(){

        var title = document.getElementById("title").value;
        var address = document.getElementById("address").value;
        if (title == "" || title == " "){
            $("#nameError").css("display","");
            return;
        }else {
            $("#nameError").css("display","none");
            if (address == "" || address == " "){
                $("#addressError").css("display","");
                return;
            }
            $("#addressError").css("display","none");
            var city = document.getElementById("city").value;
            var state = document.getElementById("state").value;
            var zip = document.getElementById("zip").value;
            var phone = document.getElementById("phone").value;
            var contact = document.getElementById("contact").value;
            var email = document.getElementById("email").value;
            var pricePer100 = document.getElementById("pricePer100").value;
            var pricePer1000 = document.getElementById("pricePer1000").value;
            var pricePerCrate = document.getElementById("pricePerCrate").value;
            var tax = document.getElementById("tax").value;
            var lateFee = document.getElementById("lateFee").value;
            var flatRate = document.getElementById("flatRate").value;

            var strUrl = BASE_URL + '/console/actions/moving/storage/addStorage.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "post",
                data: {
                    title: title,
                    address: address,
                    city: city,
                    state: state,
                    zip: zip,
                    phone: phone,
                    contact: contact,
                    email: email,
                    pricePer100: pricePer100,
                    pricePer1000: pricePer1000,
                    pricePerCrate: pricePerCrate,
                    tax: tax,
                    lateFee: lateFee,
                    flatRate: flatRate
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);
                $("#closeBtn").click();
                parent.getStorages();
                parent.getStorage(data);

            });
        }

    }
    function setNameError(){
        var title = document.getElementById("title").value;
        if (title == "" || title == " "){
            $("#nameError").css("display","");
        }else{
            $("#nameError").css("display","none");
        }
    }
    function setAddressError(){
        var address = document.getElementById("address").value;
        if (address == "" || address == " "){
            $("#addressError").css("display","");
        }else{
            $("#addressError").css("display","none");
        }
    }
    function insertOrgData(e){
        e.preventDefault();
        var strUrl = BASE_URL + '/console/actions/system/organization/getData.php', strReturn = "";

        jQuery.ajax({
            url: strUrl,
            method: "post"
        }).done(function(data){
            data = JSON.parse(data);
            if (data){
                $("#title").val(data.name+"\'s storage");
                $("#address").val(data.address);
                $("#city").val(data.city);
                $("#state").val(data.state);
                $("#zip").val(data.zip);
                $("#phone").val(data.phone);
            }
        });
    }
</script>