<?php
if(isset($_GET['carrierId'])) {

    $carrierId = $_GET['carrierId'];

    $isCalledFromModal = true;
    $pagePermissions = array(false, array(1), true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

    $carriers = new carriers($bouncer["credentials"]["orgId"]);
    $singleCarrier = $carriers->getSingleCarrier($carrierId);
    ?>
    <script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
    <style>
        .modal-dialog {
            margin: 0;
        }
    </style>
        <div class="modal-dialog">

            <div class="modal-content animated fadeIn" style="height: 100%;">
                <div class="modal-header" style="text-align: left">
                    <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                    <h4 class="modal-title">Update Carrier</h4>
                    <small>You can update your carrier data</small>
                </div>
                <form id="carrierForm" method="post" action="<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/moving/carriers/updateCarrier.php" enctype="multipart/form-data">
                    <input type="hidden" name="carrierId" value="<?php echo $carrierId; ?>" />
                    <div class="modal-body" style="padding: 0px;">
                        <div class="row" style="margin: 0px;display: flex;">
                            <div class="col-md-6 modal-body" style="padding: 13px;padding-bottom: 0px;width: 50%;">
                                <div class="form-group">
                                    <input class="form-control" type="text" required name="carrierName" value="<?= $singleCarrier['name'] ?>" placeholder="Carrier name">
                                    <span style="display: none;"><small class="text-danger">* Name Is Required</small></span>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="contactName" value="<?= $singleCarrier['contactName'] ?>" placeholder="Contact name">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="email" name="carrierEmail" value="<?= $singleCarrier['email'] ?>" placeholder="Email">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierAddress" value="<?= $singleCarrier['address'] ?>" placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierCity" value="<?= $singleCarrier['city'] ?>" placeholder="City">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" name="carrierZip" value="<?= $singleCarrier['zip'] ?>" placeholder="Zip">
                                        </div>
                                        <div class="col-md-6">
                                            <div style="margin-top: 13px;"
                                                 class="visible-xs-block visible-sm-block"></div>
                                            <input class="form-control" type="text" name="carrierState" value="<?= $singleCarrier['state'] ?>" placeholder="State">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <input class="form-control" type="text" name="carrierCountry" value="<?= $singleCarrier['country'] ?>" placeholder="Country">
                                </div>
                            </div>
                            <div class="col-md-6 modal-body" style="padding: 13px;padding-bottom: 0px; width: 50%;">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierDOT" value="<?= $singleCarrier['dot'] ?>" placeholder="D.O.T #">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierICCMC" value="<?= $singleCarrier['iccmc'] ?>" placeholder="I.C.C MC">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" min="0" name="carrierRegistration" value="<?= $singleCarrier['registration'] ?>" placeholder="Registration #">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierPhone1" value="<?= $singleCarrier['phone1'] ?>" placeholder="Phone 1">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierPhone2" value="<?= $singleCarrier['phone2'] ?>" placeholder="Phone 2">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="carrierFax" value="<?= $singleCarrier['fax'] ?>" placeholder="Fax">
                                </div>
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <input class="form-control" type="text" name="carrierWebsite" value="<?= $singleCarrier['website'] ?>" placeholder="Website">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0px;">
                            <div class="col-md-12 modal-body" style="padding: 13px;">
                                <hr>
                                <select class="form-control" name="carrierType">
                                    <option value="0" <?php if($singleCarrier['type'] == "0"){echo " selected ";} ?>>Carrier Type</option>
                                    <option value="1" <?php if($singleCarrier['type'] == "1"){echo " selected ";} ?>>Nationwide</option>
                                    <option value="2" <?php if($singleCarrier['type'] == "2"){echo " selected ";} ?>>Local</option>
                                    <option value="3" <?php if($singleCarrier['type'] == "3"){echo " selected ";} ?>>Long Distance</option>
                                    <option value="4" <?php if($singleCarrier['type'] == "4"){echo " selected ";} ?>>International</option>
                                    <option value="5" <?php if($singleCarrier['type'] == "5"){echo " selected ";} ?>>Auto Transport</option>
                                    <option value="6" <?php if($singleCarrier['type'] == "6"){echo " selected ";} ?>>Storage</option>
                                </select>
                                <br>
                                <textarea class="form-control" name="carrierComments" placeholder="Comments.."><?= $singleCarrier['comments'] ?></textarea>
                            </div>
                        </div>
                        <hr style="margin: 0px !important;">
                        <div class="row" style="margin: 0px;">
                            <div class="col-md-12 modal-body" style="padding: 13px;">
                                <button type="button" title="Upload a new iframe banner" onclick="$('#hiddenFile').click()" id="uploadAgreemtnFile" class="btn btn-xs btn-default"><i class="fa fa-upload"></i> Click here to upload agreement file</button>
                                <input type="file" style="display: none" id="hiddenFile" name="agreemtnFile" onchange="updateList()">
                                <div id="fileList">
                                    <?php
                                    if($singleCarrier["agreementFileURL"] != ""){
                                        ?>
                                        <ul style="margin: 0;padding-left: 0;list-style: none;margin-top: 13px;">
                                            <li>
                                                <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/moving/carriers/getAgreementFileLink.php?key=<?php echo $singleCarrier["agreementFileKey"] ?>" target="_blank" title="Carrier Agreement"><?php echo $singleCarrier["agreementFileName"] ?></a>
                                            </li>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" style="position:absolute; bottom: 0; width: 100%;">
                        <button type="button" class="btn btn-danger pull-left" data-style="zoom-in" onclick="deleteCarrier()">Delete</button>
                        <button type="button" class="btn btn-white pull-left" id="closeBtn"onclick="parent.swal.close()">Close</button>
                        <button type="submit" class="btn btn-primary" data-style="zoom-in">Update</button>
                    </div>
                </form>

            </div>
        </div>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
    <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
    <!-- SweetAlerts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
    <!-- Toastr -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

    <script>
        $('#carrierForm').ajaxForm({
            beforeSubmit: beforeSubmit,  // pre-submit callback
            success: afterSubmit  // post-submit callback
        });

        function beforeSubmit(data) {

        }

        function updateList() {
            var input = document.getElementById('hiddenFile');
            var output = document.getElementById('fileList');
            output.innerHTML = "";

            var ul = document.createElement("ul");
            ul.setAttribute("style","margin: 0;padding-left: 0;list-style: none;margin-top: 13px;");

            for (var i = 0; i < input.files.length; ++i) {
                var li = document.createElement("li");
                li.innerHTML += input.files.item(i).name;

                ul.appendChild(li);
            }
            output.appendChild(ul);
        }


        function afterSubmit(resp, status, other) {
            resp = JSON.parse(resp);
            if (resp == true && status == "success" && other.status == 200) {
                // success
                toastr.success("success","Carrier updated");
            } else {
                // failed
                toastr.error("error","Error saving carrier");
            }

            parent.getCarriers();
            $("#closeBtn").click();
        }
        function deleteCarrier(id){
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this carrier",
                icon: "warning",
                dangerMode: true,
                buttons: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    var strUrl = BASE_URL+'/console/actions/moving/carriers/deleteCarrier.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method: "POST",
                        data: {
                            carrierId: "<?php echo $singleCarrier["id"]; ?>"
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function (data) {

                        try{
                            data = JSON.parse(data);

                            if(data == true){
                                toastr.success("deleted","Carrier deleted");
                            }else{
                                toastr.error("Data not saved", "error");
                            }
                        }catch (e) {
                            toastr.error("Data not saved", "error");
                        }

                        parent.getCarriers();
                        $("#closeBtn").click();

                    });
                }
            });
        }

    </script>
    <?php
}
?>