<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

if(isset($_GET['crewId']) && $_GET['crewId'] != ""){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/crew.php");
    $crewId = $_GET['crewId'];

    $crew = new crew($bouncer["credentials"]["orgId"]);
    $crewData = $crew->getSingleCrew($crewId);
}

?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 0;
    }
    .modal-footer{
        display: flex;
    }
    .confirmForm{
        margin-left: auto !important;
    }
</style>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>

    <div class="modal-dialog" style="height: 100%;">

        <div class="modal-content animated fadeIn" style="height: 100%;">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <?php if(isset($crewId)){ ?>
                    <h4 class="modal-title">Update Crew Member</h4>
                <?php }else{ ?>
                    <h4 class="modal-title">Add Crew Member</h4>
                    <small>Add and manage team members to add and notify about Jobs and updates.</small>
                <?php } ?>
            </div>
            <form id="membersForm">
                <div id="bodyWrapper" class="modal-body" style="padding: 0px;">
                    <div class="row" style="margin: 0px;">
                        <div class="col-md-6 modal-body" style="padding: 13px;padding-bottom: 0px">
                            <div class="form-group">
                                <input id="memberFullname" class="form-control" value="<?php if(isset($crewData["name"])){echo $crewData["name"];} ?>" type="text"  name="mebmerFullname" placeholder="Full name">
                                <span  class="nameMemberError errorMessage" style="display: none;"><small class="text-danger">* Name Is Required</small></span>
                            </div>
                            <div class="form-group">
                                <input id="memberEmail" class="form-control" value="<?php if(isset($crewData["email"])){echo $crewData["email"];} ?>" type="email" name="memberEmail" placeholder="Email">
                            </div>
                            <div class="form-group input-group date"  id="date_picker" style="width:100%;">
                                <input id="memberBirthDate" readonly style="background-color: #fff; opacity: unset;" class="form-control" type="text" value="<?php if(isset($crewData["birthDate"])){echo $crewData["birthDate"];} ?>" name="memberBirthDate" placeholder="Birth Date">
                            </div>
                        </div>
                        <div class="col-md-6 modal-body" style="padding: 13px;padding-bottom: 0px">
                            <div class="form-group">
                                <input id="memberPhone" class="form-control" value="<?php if(isset($crewData["phone"])){echo $crewData["phone"];} ?>" type="tel" name="phone" placeholder="Phone">
                            </div>
                            <div class="form-group">
                                <input id="memberAddress" class="form-control" type="text" value="<?php if(isset($crewData["address"])){echo $crewData["address"];} ?>" name="memberAddress" placeholder="Address">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin: 0px;">
                        <div class="col-md-12 modal-body" style="padding:0 13px 13px 13px;">
                            <hr style="margin-top: 0px;">
                            <div class="form-group">
                                <select id="memberType" class="form-control" name="memberType">
                                    <option value="">Select Member Type</option>
                                    <option <?php if(isset($crewData["type"]) && $crewData["type"] == "1"){echo ' selected ';} ?> value="1">Laborer</option>
                                    <option <?php if(isset($crewData["type"]) && $crewData["type"] == "2"){echo ' selected ';} ?> value="2">Foreman</option>
                                    <option <?php if(isset($crewData["type"]) && $crewData["type"] == "3"){echo ' selected ';} ?> value="3">Driver</option>
                                </select>
                                <span  class="typeMemberError errorMessage" style="display: none;"><small class="text-danger">* Type Is Required</small></span>
                            </div>
                            <br>
                            <div class="form-group">
                                <textarea id="memberComments" style="resize: vertical;" class="form-control" name="memberComments" placeholder="Comments.."><?php if(isset($crewData["comments"])){echo $crewData["comments"];} ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="position:absolute;bottom: 0; width: 100%;">
                    <button type="button" class="btn btn-white" onclick="parent.swal.close()">Close</button>
                    <?php if(isset($crewId)){ ?>
                        <button type="submit" class="ladda-button btn btn-primary confirmForm" data-style="zoom-in">
                            <span class="ladda-label">Update Member</span>
                            <span class="ladda-spinner"></span>
                        </button>
                    <?php }else{ ?>
                        <button type="submit" class="ladda-button btn btn-primary confirmForm" data-style="zoom-in">
                            <span class="ladda-label">Add Member</span>
                            <span class="ladda-spinner"></span>
                        </button>
                    <?php } ?>
                </div>
            </form>

        </div>


    </div>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Data picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- jquery validate -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/validate/jquery.validate.min.js"></script>


<!-- Ladda -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script>

    $(function() {

        $('#memberBirthDate').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10)
            }
        );

        $('#membersForm').validate({
            rules: {
                mebmerFullname: {
                    required: true,
                },
                memberEmail: {
                    email: true,
                },
                memberType: {
                    required: true,
                }
            },
            submitHandler: function (form) {

                <?php if(isset($crewId)){ ?>
                    updateMember();
                <?php }else{ ?>
                    addMember();
                <?php } ?>

            }
        });

    });


    function addMember(){

        var l = $( '.ladda-button' ).ladda();
        l.ladda( 'start' );

        var fullname = $('#memberFullname').val();
        var email = $('#memberEmail').val();
        var birthDate = $('#memberBirthDate').val();
        var phone = $('#memberPhone').val();
        var address = $('#memberAddress').val();
        var type = $('#memberType').val();
        var comments = $('#memberComments').val();

        var strUrl = BASE_URL+'/console/actions/moving/crew/addCrew.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {
                fullname : fullname,
                email : email,
                birthDate : birthDate,
                phone : phone,
                address : address,
                type : type,
                comments : comments
            },
            async: true
        }).done(function (data) {
            l.ladda('stop');

            data = JSON.parse(data);
            if(data != true){
                    parent.swal("Error creating member","","error");
            }
            parent.getCrew();
            parent.swal.close();
        });
    }

    function updateMember(){
        var l = $( '.ladda-button' ).ladda();
        l.ladda( 'start' );

        var fullname = $('#memberFullname').val();
        var email = $('#memberEmail').val();
        var birthDate = $('#memberBirthDate').val();
        var phone = $('#memberPhone').val();
        var address = $('#memberAddress').val();
        var type = $('#memberType').val();
        var comments = $('#memberComments').val();

        var strUrl = BASE_URL+'/console/actions/moving/crew/updateCrew.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {
                crewId: <?php if(isset($crewId)){echo $crewId;}else{echo "null";} ?>,
                fullname : fullname,
                email : email,
                birthDate : birthDate,
                phone : phone,
                address : address,
                type : type,
                comments : comments
            },
            async: true
        }).done(function (data) {
            l.ladda('stop');

            data = JSON.parse(data);
            if(data != true){
                parent.swal("Error creating member","","error");
            }

            parent.getCrew();
            parent.swal.close();
        });
    }
</script>