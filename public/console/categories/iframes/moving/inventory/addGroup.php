<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 0;
    }
</style>
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn" style="height: 100%;">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Groups</h4>
                <small>You can create your own Group.</small>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="groupTitle">Group Title</label>
                    <input id="groupTitle" class="form-control" type="text" name="groupTitle" placeholder="Enter Group Title">
                </div>
            </div>
            <div class="modal-footer" style="position:absolute; bottom: 0; width: 100%;">
                <button type="button" class="btn btn-white" onclick="parent.swal.close()" style="float: left;">Close</button>
                <button type="button" class="btn btn-primary" onclick="addGroup()" data-style="zoom-in">Create Group</button>
            </div>

        </div>
    </div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>

<script>

    function addGroup(){

        var strUrl = BASE_URL+'/console/actions/moving/inventory/addGroup.php', strReturn = "";

        var title = document.getElementById("groupTitle").value;

        jQuery.ajax({
            url: strUrl,
            method:"post",
            data:{
                title:title
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            parent.getGroups(true);
            parent.swal.close();
        });

    }
</script>