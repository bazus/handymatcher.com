
    <script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
    <style>
        .modal-dialog {
            margin: 0;
        }
    </style>
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Items</h4>
                <small>You can create your own Items.</small>
            </div>
            <form id="itemForm">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="itemTitle">Item Title</label>
                        <input id="title" class="form-control" type="text" name="itemTitle"
                               placeholder="Enter Item Title">
                    </div>
                    <div class="form-group">
                        <label for="itemCF">Item Cubic Feet</label>
                        <input id="cf" class="form-control" type="number" min="0" name="itemCF"
                               placeholder="Enter Item Cubic Feet">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" onclick="parent.swal.close()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addItem()" data-dismiss="modal"
                            data-style="zoom-in">Create Item
                    </button>
                    <div id="itemAddedMessage" class="alert alert-success" style="width: 103px;float:left;padding: 7px;text-align: center;display:none;">Item added</div>
                </div>
            </form>

        </div>
    </div>

    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
    <script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
    <!-- Ladda -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>
    <script>

        function addItem() {

            var strUrl = BASE_URL + '/console/actions/moving/inventory/addItem.php', strReturn = "";
            var groupId = "<?= $_GET['id'] ?>";
            var title = document.getElementById("title").value;
            var cf = document.getElementById("cf").value;

            jQuery.ajax({
                url: strUrl,
                method: "post",
                data: {
                    groupId: groupId,
                    title: title,
                    cf: cf
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                parent.getItems(groupId, parent.document.getElementById("group-" + groupId), parent.document.getElementById("titleSpan").innerHTML.replace(" - ", ""));
                showAddItemMessage();
            });

        }
        function showAddItemMessage() {
            $("#itemAddedMessage").show();
            setTimeout(function(){
                $("#itemAddedMessage").fadeOut();
            }, 2000);
            $("#itemForm").trigger("reset");
        }
    </script>
