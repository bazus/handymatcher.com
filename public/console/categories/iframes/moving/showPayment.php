<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

if (isset($_GET['id'])){;
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $id = $_GET['id'];
    $payments = new payments($bouncer["credentials"]['orgId']);

    $payment = $payments->getPayment($id);

    $lead = new lead($payment["leadId"],$bouncer["credentials"]['orgId']);
    $leadData = $lead->getData();

    // calculate VAT
    $vatCalculation = $payments->calculateVAT($payment["amount"],$payment["vatPer"],$payment["vatIsInclusive"]);

    $amountWithoutVat = $payment["amount"];
    if($payment["vatIsInclusive"] == "1"){
        $amountWithoutVat -= $vatCalculation["vatAmount"];
    }

}else{
    header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 30px auto;
    }
    .modal-body{
        padding: 29px;
    }
    .modal-body div{
        margin-bottom: 12px;
    }
    .modal-body .pull-right{
        color: #1ab394;
    }
    .small-control {
        background-color: #FFFFFF;
        background-image: none;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
        color: inherit;
        display: block;
        padding: 6px 8px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        width: 150px;
        height: 26px;
    }
    .smaller-control {
        background-color: #FFFFFF;
        background-image: none;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
        color: inherit;
        display: block;
        padding: 6px 8px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        width: 75px;
        height: 26px;
    }
    #paymentEditDatePicker .fa-calendar{
        margin-right: 5px;
    }
    .topPaymentForm{
        padding: 15px 15px 5px 15px;
    }
    .topPaymentForm div{
        border-bottom: 1px dotted #c7c7c7;
        padding-bottom: 5px;
    }
    .topPaymentForm div:hover{
        border-bottom: 1px solid #919191;
    }
    .amountPayed{
        font-weight: bold;
        font-size: 18px;
    }
    .bottomPaymentForm{
        background: #b9b9b924;
        border-radius: 5px;
        padding: 15px;
        border: 1px solid #c3c3c3;
    }
    .bottomPaymentForm .row .col-md-6 div{
        border-bottom: 1px dotted #c7c7c7;
    }
    .bottomPaymentForm .row .col-md-6 div:hover{
        border-bottom: 1px solid #919191;
    }
    .small-control:focus{
        border-color: #1ab394;
        outline:0;
    }
    #buttonsPaymentsContainer button{
        margin-top:5px;
    }

    #deletePayment{
        margin-right: 5px;
    }
    @media (max-width: 480px) {

        .small-control {
            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            border-radius: 1px;
            color: inherit;
            display: block;
            padding: 6px 8px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
            width: 80%;
            height: 26px;
        }
        .smaller-control {
            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            border-radius: 1px;
            color: inherit;
            display: block;
            padding: 3px 8px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
            width: 50%;
            height: 26px;
        }
        select.small-control{
            padding: 2px;
            width: 118%;
        }
        #paymentEditDatePicker{
            width: 100%;
            overflow:hidden;
            white-space:nowrap;
            text-overflow: ellipsis;
        }
        .btnGroup{
            flex-grow: 1;
            justify-content: space-between;
        }

        #downloadReceipt{
            margin-left: 0;
        }
        .modal-dialog {
            height: 100%;
            margin: 0;
        }
    }
</style>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <button type="button" class="close" data-dismiss="modal" style="margin:5px;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <div class="modal-header" style="padding: 0px 39px;">
                <h2 style="font-weight: bold;text-align: left;margin-top: 15px;margin-bottom: 15px;color: #6d6d6d;"><i class="fa fa-credit-card"></i> Payment details <span class="pull-right" style="font-size: 13px;">Job #<?php if($leadData['jobNumber'] != NULL && $leadData['jobNumber'] != ""){echo $leadData['jobNumber'];}else{echo $leadData['id'];} ?></span></h2>
            </div>
            <div class="modal-body" style="padding: 6px 28px;">

                <div class="topPaymentForm">
                    <div class="amountPayed">Total Amount (USD): <span class='pull-right' id="paymentAmount"><i class="fa fa-dollar"></i><?= number_format($payment['totalAmount'],2,'.',',') ?></span></div>

                    <div>Amount: <span class='pull-right' id="paymentVat">$<?php echo $amountWithoutVat; ?></span></div>
                    <div>Tax (VAT): <span class='pull-right' id="paymentVat">$<?php echo $vatCalculation["vatAmount"]; ?> (%<?php echo $payment['vatPer']; ?>)</span></div>
                    <div>User Handle: <span class='pull-right' id="paymentFullName"><?= ($payment['fullName']) ?></span></div>
                    <div>Payment confirmation number: <span class='pull-right'>#10<?= ($payment['id']) ?></span></div>
                    <div>Method: <span class='pull-right' id="paymentMethod"><?php if ($payment['method'] == 1){echo 'Credit Card';}elseif($payment['method'] == 2){echo "Paypal";}elseif($payment['method'] == 3){echo "Check";}elseif($payment['method'] == 4){echo "Wire Transfer";}elseif($payment['method'] == 5){echo "Cash";} ?></span></div>
                    <div>Payment Date: <span class='pull-right' id="paymentDate"><?php if ( $payment['paymentDate']){echo date("M j Y, H:i",strtotime($payment['paymentDate']));}else{echo "---";} ?></span></div>
                </div>
                <div id="addressInformation" class="bottomPaymentForm addressInformation">
                    <div class="row" style="margin-bottom: 0px !important;">
                        <div class="col-md-6" style="margin-bottom: 0px !important;">
                            <div>Address: <span class='pull-right' id="paymentAddress"><?php if ( $payment['address']){echo $payment['address'];}else{echo "---";} ?></span></div>
                            <div>City: <span class='pull-right' id="paymentCity"><?php if ( $payment['city']){echo $payment['city'];}else{echo "---";} ?></span></div>
                            <div>ZIP: <span class='pull-right' id="paymentZip"><?php if ( $payment['zip']){echo $payment['zip'];}else{echo "---";} ?></span></div>
                        </div>
                        <div class="col-md-6" style="margin-bottom: 0px !important;">
                            <div>State: <span class='pull-right' id="paymentCountry"><?php if ( $payment['country']){echo $payment['country'];}else{echo "---";} ?></span></div>
                            <div>Phone: <span class='pull-right' id="paymentPhone"><?php if ( $payment['phone']){echo $payment['phone'];}else{echo "---";} ?></span></div>
                            <div>Email: <span class='pull-right'><?php if ( $payment['email']){echo $payment['email'];}else{echo "---";} ?></span></div>
                        </div>
                    </div>
                </div>
                <div id="addressInformationEdit" class="bottomPaymentForm addressInformation" style="display: none">
                    <div class="row" style="margin-bottom: 0px !important;">
                        <div class="col-md-6" style="margin-bottom: 0px !important;">
                            <div>Address: <span class='pull-right' id="paymentAddress"><input type='text' style='float: right;' class='small-control' value='<?php if ( $payment['address']){echo $payment['address'];}else{echo "---";} ?>' id='paymentEditAddress'></span></div>
                            <div>City: <span class='pull-right' id="paymentCity"><input type='text' style='float: right;' class='small-control' value='<?php if ( $payment['city']){echo $payment['city'];}else{echo "---";} ?>' id='paymentEditCity'></span></div>
                            <div>ZIP: <span class='pull-right' id="paymentZip"><input type='text' style='float: right;' class='small-control' value='<?php if ( $payment['zip']){echo $payment['zip'];}else{echo "---";} ?>' id='paymentEditZip'></span></div>
                        </div>
                        <div class="col-md-6" style="margin-bottom: 0px !important;">
                            <div>State: <span class='pull-right' id="paymentCountry"><input type='text' style='float: right;' class='small-control' value='<?php if ( $payment['country']){echo $payment['country'];}else{echo "---";} ?>' id='paymentEditCountry'></span></div>
                            <div>Phone: <span class='pull-right' id="paymentPhone"><input type='phone' style='float: right;' class='small-control' value='<?php if ( $payment['phone']){echo $payment['phone'];}else{echo "---";} ?>' id='paymentEditPhone'></span></div>
                            <div>Email: <span class='pull-right'><input type='email' style='float: right;' class='small-control' value='<?php if ( $payment['email']){echo $payment['email'];}else{echo "---";} ?>' id='paymentEditEmail'></span></div>
                        </div>
                    </div>
                </div>

                <?php if ($payment['method'] == 1){ ?>
                <div class="bottomPaymentForm">
                    <div class="row" style="margin-bottom: 0px !important;">
                        <div class="col-md-6" style="margin-bottom: 0px !important;">
                            <div>Card Holder Name: <span class='pull-right' id="paymentCardHolder"><?php if ( $payment['cardHolder']){echo $payment['cardHolder'];}else{echo "---";} ?></span></div>
                            <div>Credit Card Number: <span class='pull-right' id="paymentCardNumber"><?php echo "XXXX-XXXX-XXXX-"; if($payment['creditCardNumber']){ echo $payment['creditCardNumber'];}else{echo 'XXXX';} ?></span></div>
                            <div>Exp Date: <span class='pull-right' id="paymentExpDate"><?= ($payment['expDate']) ?></span></div>
                        </div>
                        <div class="col-md-6" style="margin-bottom: 0px !important;">
                            <div>Security Code: <span class='pull-right' id="paymentCode"><?= ($payment['securityCode']) ?></span></div>
                            <div>Confirmation Number: <span class='pull-right' id="paymentConfirm"><?php if ( $payment['creditCardConfirmation']){echo $payment['creditCardConfirmation'];}else{echo "---";} ?></span></div>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="remarks">Remarks: <textarea class="form-control" id="remarksText" style="margin-top: 3px;" disabled><?= $payment['remarks'] ?></textarea></div>
            </div>
            <div style="display: flex; justify-content: space-between;flex-wrap: wrap;" class="modal-footer" style="margin-top:0;" id="buttonsPaymentsContainer">
                <div class="btnGroup" style="display: flex; margin-right:auto;">
                    <button type="button" class="btn btn-white" id="editBtn">Edit</button>
                    <button type="button" class="btn btn-danger" id="deletePayment">Delete Payment</button>
                </div>
                <div  class="btnGroup" style="display: flex">
                    <button type="button" class="btn btn-success" id="downloadReceipt">Download Receipt</button>
                    <button type="button" class="btn btn-success" id="sendReceipt">Send Receipt</button>
                </div>
            </div>
            <div class="modal-footer" style="margin-top:0;display: none;" id="buttonsPaymentsContainerEdit">
                <button type='button' class='btn btn-primary' id='savePaymentBtn'>Save</button>
                <button type='button' class='btn btn-default pull-left' id='goBackPaymentBtn'>Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Touchspin -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script>
    $( document ).ready(function() {
        $("#savePaymentBtn").on('click',function(){
            savePaymentEdit();
        });
    });

    var paymentEditDate = moment('<?= date("Y-m-d",strtotime($payment['paymentDate'])); ?>').format('MMMM D, YYYY');

    $("#sendReceipt").on("click",function () {
        $("#myCustomModal").modal('hide');
        showCustomModal('categories/iframes/sendPaymentReceiptEmail.php?paymentId=<?php echo $payment["id"]; ?>');
    });

    $("#downloadReceipt").on("click",function () {
        window.location = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/api/leads/downloadPaymentReceiptEmailText.php?paymentId=<?= $id ?>&orgId=<?php echo $bouncer["credentials"]['orgId']; ?>';
    });

    $("#editBtn").on("click",function () {

        // Change To Edit
        document.getElementById('addressInformation').style.display = "none";
        document.getElementById('addressInformationEdit').style.display = "";

        // Set Buttons
        document.getElementById('buttonsPaymentsContainer').style.display = "none";
        document.getElementById('buttonsPaymentsContainerEdit').style.display = "";

        // Payment Remarks
        $( "#remarksText" ).prop( "disabled", false );

        // Set Back Button Click
        $("#goBackPaymentBtn").on("click",function(){

            // Change To Edit
            document.getElementById('addressInformation').style.display = "";
            document.getElementById('addressInformationEdit').style.display = "none";

            // Set Buttons
            document.getElementById('buttonsPaymentsContainer').style.display = "flex";
            document.getElementById('buttonsPaymentsContainerEdit').style.display = "none";

            // Payment Remarks
            $( "#remarksText" ).prop( "disabled", true );


        });

        $(".addressInformation div").each(function () {
            $(this).css('border','unset');
        });


        $('#paymentEditDatePicker #paymentEditDate').html(moment('<?= date("Y-m-d",strtotime($payment['paymentDate'])); ?>').format('MMMM D, YYYY'));

        $('#paymentEditDatePicker').daterangepicker({
            singleDatePicker: true,
            format: 'MM/DD/YYYY',
            startDate: moment('<?= date("Y-m-d",strtotime($payment['paymentDate'])); ?>'),
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Go',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {
            $('#paymentEditDatePicker #paymentEditDate').html(end.format('MMMM D, YYYY'));
            paymentEditDate = end.format('YYYY-MM-DD');
        });

    });

    $("#deletePayment").on("click",function () {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this payment",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then(function(isConfirm){
            if (isConfirm) {

                var strUrl = BASE_URL + '/console/actions/moving/payments/deletePayment.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        id: "<?= $id ?>",
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true) {
                            $("#myCustomModal").modal('hide');
                            leadController.lead.getLeadPaymentDetails();
                            toastr.success("Payment Deleted", "Deleted");
                        } else {
                            toastr.error("Can not delete payment", "Failed");
                        }
                    } catch (e) {
                        toastr.error("Can not delete payment", "Failed");
                    }
                });
            }else{

            }
        });

    });

    function savePaymentEdit(){
        var paymentData = {
            "address": document.getElementById("paymentEditAddress").value,
            "city": document.getElementById("paymentEditCity").value,
            "zip": document.getElementById("paymentEditZip").value,
            "country": document.getElementById("paymentEditCountry").value,
            "phone": document.getElementById("paymentEditPhone").value,
            "email": document.getElementById("paymentEditEmail").value,
            "remarks": document.getElementById("remarksText").value
        };

        var strUrl = BASE_URL + '/console/actions/moving/payments/editPayment.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data: {
                id: "<?= $id ?>",
                data: paymentData
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data == true) {
                    $("#myCustomModal").modal('hide');
                    leadController.lead.getLeadPaymentDetails();
                    toastr.success("Saved", "Saved");
                }else{
                    toastr.error("Cant save details","Failed");
                }
            }catch (e) {
                toastr.error("Cant save details","Failed");
            }
        });
    }

    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }
</script>