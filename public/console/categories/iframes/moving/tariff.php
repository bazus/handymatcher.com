<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");
$movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
$movingSettingsData = $movingSettings->getData();

$tariff = $movingSettingsData["tariff"];

?>
<style>

    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }

    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }
</style>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<div id="wrapper" style="visibility: hidden;">
    <form action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/moving/movingSettings/saveTariff.php" method="post" id="tariffForm">
        <table class="table table-sm table-borderless" style="font-size: 15px;text-align: center;">
            <thead style="border-bottom: 1px solid gainsboro;">
            <tr>
                <th scope="col">From state</th>
                <th scope="col">Price per CF</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $states = [];
                $states[] = "AL";
                $states[] = "AK";
                $states[] = "AZ";
                $states[] = "AR";
                $states[] = "CA";
                $states[] = "CO";
                $states[] = "CT";
                $states[] = "DC";
                $states[] = "DE";
                $states[] = "FL";
                $states[] = "GA";
                $states[] = "HI";
                $states[] = "IA";
                $states[] = "ID";
                $states[] = "IL";
                $states[] = "IN";
                $states[] = "KS";
                $states[] = "KY";
                $states[] = "LA";
                $states[] = "MA";
                $states[] = "MD";
                $states[] = "ME";
                $states[] = "MI";
                $states[] = "MN";
                $states[] = "MO";
                $states[] = "MS";
                $states[] = "MT";
                $states[] = "NC";
                $states[] = "ND";
                $states[] = "NE";
                $states[] = "NH";
                $states[] = "NJ";
                $states[] = "NM";
                $states[] = "NV";
                $states[] = "NY";
                $states[] = "OH";
                $states[] = "OK";
                $states[] = "OR";
                $states[] = "PA";
                $states[] = "RI";
                $states[] = "SC";
                $states[] = "SD";
                $states[] = "TN";
                $states[] = "TX";
                $states[] = "UT";
                $states[] = "VA";
                $states[] = "VT";
                $states[] = "WA";
                $states[] = "WI";
                $states[] = "WV";
                $states[] = "WY";
    
                foreach($states as $state){
                    $defaultPricePerCF = 0;
                    if(isset($tariff[$state]["cf"])){
                        $defaultPricePerCF = $tariff[$state]["cf"];
                    }
                    ?>
                    <tr>
                        <td style="vertical-align: middle;"><span style="display: inline-table;"><?= $state ?></span></td>
                        <td>$<input type="number" class="form-control form-control-sm" value="<?= $defaultPricePerCF ?>" name="states[<?= $state ?>][cfPrice]" style="text-align: center; line-height:normal; height: 20px; display: inline-table;width: 100px;margin-left: 3px;" /></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </form>
</div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>


<script>
    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });
    
    $('#tariffForm').ajaxForm({
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    });

    function beforeSubmit(data) {

    }

    function afterSubmit(resp,status,other) {
        resp = JSON.parse(resp);

        if(resp == true && status == "success" && other.status == 200){
            // success
            parent.window.swal.close();
        }else{
            // failed
            $(parent)[0].swal("Oops", "Your changes were not saved. Please try again later.", "error");

        }

    }

    function saveData() {
        $("#tariffForm").submit();
    }
</script>