<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

if (isset($_GET['id'])){;
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/passwords.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $id = $_GET['id'];
    $payments = new payments($bouncer["credentials"]['orgId']);

    $ccaf = $payments->getCreditCardAuthorization($id);

    $lead = new lead($ccaf["leadId"],$bouncer["credentials"]['orgId']);
    $leadData = $lead->getData();

    $passwords = new passwords();
    $unHashedCardNumber = $passwords->unHashCC($ccaf["cardNumber"]);
}else{
    header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 30px auto;
    }
    .modal-body{
        padding: 29px;
    }
    .modal-body div{
        margin-bottom: 12px;
    }
    .modal-body .pull-right{
        color: #1ab394;
    }
    .small-control {
        background-color: #FFFFFF;
        background-image: none;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
        color: inherit;
        display: block;
        padding: 6px 8px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        width: 150px;
        height: 26px;
    }
    .smaller-control {
        background-color: #FFFFFF;
        background-image: none;
        border: 1px solid #e5e6e7;
        border-radius: 1px;
        color: inherit;
        display: block;
        padding: 6px 8px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        width: 75px;
        height: 26px;
    }
    #paymentEditDatePicker .fa-calendar{
        margin-right: 5px;
    }
    .topPaymentForm{
        padding: 15px 15px 5px 15px;
    }
    .topPaymentForm div{
        border-bottom: 1px dotted #c7c7c7;
        padding-bottom: 5px;
    }
    .topPaymentForm div:hover{
        border-bottom: 1px solid #919191;
    }
    .amountPayed{
        font-weight: bold;
        font-size: 18px;
    }
    .bottomPaymentForm{
        background: #ffffff;
        border-radius: 5px;
        padding: 15px;
        border: 1px solid #e6e6e6;
        font-size: 12px;
        height: 213px;
        overflow-y: scroll;
    }
    .bottomPaymentForm .row .col-md-6 div{
        border-bottom: 1px dotted #c7c7c7;
    }
    .bottomPaymentForm .row .col-md-6 div:hover{
        border-bottom: 1px solid #919191;
    }
    .small-control:focus{
        border-color: #1ab394;
        outline:0;
    }
    #addPaymentBtn{
        background-color: #7cd1f9;
        color: #fff;
        border: none;
        box-shadow: none;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        padding: 10px 24px;
    }

    #closeBtn{
        color: #555;
        background-color: #efefef;
        border: none;
        box-shadow: none;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        padding: 10px 24px;
    }
    @media (max-width: 480px) {
        .small-control {
            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            border-radius: 1px;
            color: inherit;
            display: block;
            padding: 6px 8px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
            width: 80%;
            height: 26px;
        }
        .smaller-control {
            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            border-radius: 1px;
            color: inherit;
            display: block;
            padding: 3px 8px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
            width: 50%;
            height: 26px;
        }
        select.small-control{
            padding: 2px;
            width: 118%;
        }
        #paymentEditDatePicker{
            width: 100%;
            overflow:hidden;
            white-space:nowrap;
            text-overflow: ellipsis;
        }
        .modal-dialog {
            height: 100%;
            margin: 0 !important;
        }
    }
</style>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog" style="max-width: 900px;">
        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="padding: 0px 39px;">
                <h2 style="font-weight: bold;text-align: left;margin-top: 15px;margin-bottom: 15px;color: #6d6d6d;"><i class="fa fa-credit-card"></i> Credit card authorization <span class="pull-right" style="font-size: 13px;">Job #<?php if($leadData['jobNumber'] != NULL && $leadData['jobNumber'] != ""){echo $leadData['jobNumber'];}else{echo $leadData['id'];} ?></span></h2>
            </div>
            <div class="modal-body" style="padding: 6px 28px;">

                <div class="topPaymentForm">
                    <div class="amountPayed">Amount (USD): <span class='pull-right'><i class="fa fa-dollar"></i><?= $ccaf['signedAmount'] ?></span></div>

                    <div>Card Type: <span class='pull-right'><?php echo $ccaf['cardType']; ?></span></div>
                    <div>Cardholder Name: <span class='pull-right'><?php echo $ccaf['cardholderName']; ?></span></div>
                    <div>Card number: <span class='pull-right'><?php echo $unHashedCardNumber; ?></span></div>
                    <div>Exp Date: <span class='pull-right'><?php echo $ccaf['expMonth']."/".$ccaf['expYear']; ?></span></div>
                    <div>CVV: <span class='pull-right'><?php echo $ccaf['CVV']; ?></span></div>
                    <div>Address: <span class='pull-right'><?php echo $ccaf['address']; ?></span></div>
                    <div>City: <span class='pull-right'><?php echo $ccaf['city']; ?></span></div>
                    <div>State: <span class='pull-right'><?php echo $ccaf['state']; ?></span></div>
                    <div>Billing Zip Code: <span class='pull-right'><?php echo $ccaf['zipCode']; ?></span></div>
                    <div>Signed Date: <span class='pull-right'><?php echo date("l, F jS, Y",strtotime($ccaf["signDate"])) ?></span></div>
                </div>
                <div class="bottomPaymentForm">

                    I, <span style="font-weight: bold;text-decoration: underline"><?php echo $ccaf['signedName']; ?></span>, authorize <span style="font-weight: bold;text-decoration: underline"><?php echo $bouncer["organizationData"]["organizationName"]; ?></span> to charge my credit card above for the amount of <span style="font-weight: bold;text-decoration: underline">$<?php echo $ccaf['signedAmount']; ?></span>.<br>
                    <br>

                    <?php
                        if($ccaf['terms'] && $ccaf['terms'] != NULL && $ccaf['terms'] != ""){
                            // New ones - have terms
                            echo $ccaf['terms'];
                        }else{
                            // Old ones - do not have terms (show the standard terms)
                            ?>
                            I agree to <span style="font-weight: bold;text-decoration: underline"><?php echo $bouncer["organizationData"]["organizationName"]; ?></span> cancellation policy as well, which reads: "Deposits are refundable (less 10% for processing fees and internal resources) for reservations cancelled Inside Seven (7) days of signing this estimate.<br>
                            If the pick-up date reflected on this estimate is within Seven (7) business days of signing, the deposit is non-refundable.<br>
                            Moves may be postponed for up to 24 months, if you choose to cancel your move outside of <span style="font-weight: bold;text-decoration: underline"><?php echo $bouncer["organizationData"]["organizationName"]; ?></span> cancellation policy, your deposit may be applicable towards any move in the next 24 months.<br>
                            All cancellation or refund requests MUST be sent in writing via email to <span style="font-weight: bold;text-decoration: underline"><?php echo $bouncer["organizationData"]["organizationName"]; ?></span>.<br>
                            Any cancellation or refund request received after normal business hours, after 6pm Eastern Time, will be deemed received on the next business day (Federal holidays and Weekends excluded)".<br>
                            <br>
                            I agree that any deposit provided to <span style="font-weight: bold;text-decoration: underline"><?php echo $bouncer["organizationData"]["organizationName"]; ?></span> can be utilized for a future move within a 12 month period of placing a reservation, should I request to cancel.<br>

                            <?php
                        }
                    ?>


                </div>
                <div class="bottomPaymentForm" style="text-align: center">

                    <img style="width: 300px;height: 115px;" src="<?php echo $ccaf['signature']; ?>" />

                </div>

            </div>
            <div class="modal-footer" style="margin-top:0;">
                <button type="button" class="btn btn-white pull-left" data-dismiss="modal" id="closeBtn">Close</button>
                <button type="button" class="btn btn-white" onclick="addCCAToPayment()" id="addPaymentBtn">Add to payment</button>
            </div>
        </div>
    </div>
</div>

<!-- Touchspin -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script>
    var signaturePadCCAF;
    var canvasCCAF;
    function initSignatureCCAF() {
        canvasCCAF = document.getElementById("signCanvasCCAF");
        window.onresize = resizeCanvas;
        //resizeCanvas();
        signaturePadCCAF = new SignaturePad(canvasCCAF);
    }

    function addCCAToPayment(){
        var ccData = <?php echo json_encode($ccaf); ?>;

       parent.leadController.lead.payments.addPayment(ccData);
        $('#myCustomModal').modal('hide');

    }
</script>