<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

?>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<style>
    .modal-dialog {
        margin: 0;
    }
</style>
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn" style="height: 100%;">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" onclick="parent.swal.close()"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Carrier</h4>
                <small>You can create your own carrier.</small>
            </div>
            <form id="carrierForm" method="post" action="<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/moving/carriers/addCarrier.php" enctype="multipart/form-data">
                <div class="modal-body" style="padding: 0px;">
                    <div class="row" style="margin: 0px; display: flex;">
                        <div class="col-md-6 modal-body" style="padding: 13px;padding-bottom: 0px; width:50%;">
                            <div class="form-group">
                                <input class="form-control" type="text" required name="carrierName" placeholder="Carrier name">
                                <span  style="display: none;"><small class="text-danger">* Name Is Required</small></span>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="contactName" placeholder="Contact name">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="email" name="carrierEmail" placeholder="Email">
                            </div>
                            <hr>
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierAddress" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierCity" placeholder="City">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="carrierZip" placeholder="Zip">
                                    </div>
                                    <div class="col-md-6">
                                        <div style="margin-top: 13px;" class="visible-xs-block visible-sm-block"></div>
                                        <input class="form-control" type="text" name="carrierState" placeholder="State">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <input class="form-control" type="text" name="carrierCountry" placeholder="Country">
                            </div>
                        </div>
                        <div class="col-md-6 modal-body" style="padding: 13px;padding-bottom: 0px;width:50%;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierDOT" placeholder="D.O.T #">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierICCMC" placeholder="I.C.C MC">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" min="0" name="carrierRegistration" placeholder="Registration #">
                            </div>
                            <hr>
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierPhone1" placeholder="Phone 1">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierPhone2" placeholder="Phone 2">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="carrierFax" placeholder="Fax">
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <input class="form-control" type="text" name="carrierWebsite" placeholder="Website">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin: 0px;">
                        <div class="col-md-12 modal-body" style="padding: 13px;">
                            <hr>
                            <select class="form-control" name="carrierType">
                                <option value="0">Carrier Type</option>
                                <option value="1">Nationwide</option>
                                <option value="2">Local</option>
                                <option value="3">Long Distance</option>
                                <option value="4">International</option>
                                <option value="5">Auto Transport</option>
                                <option value="6">Storage</option>
                            </select>
                            <br>
                            <textarea class="form-control" name="carrierComments" placeholder="Comments.."></textarea>
                        </div>
                    </div>
                    <hr style="margin: 0px !important;">
                    <div class="row" style="margin: 0px;">
                        <div class="col-md-12 modal-body" style="padding: 13px;">
                            <button type="button" title="Upload a new iframe banner" onclick="$('#hiddenFile').click()" id="uploadAgreemtnFile" class="btn btn-xs btn-default"><i class="fa fa-upload"></i> Click here to upload agreement file</button>
                            <input type="file" style="display: none" id="hiddenFile" name="agreemtnFile" onchange="updateList()">
                            <div id="fileList"></div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="position:absolute; bottom: 0; width: 100%;">
                    <button style="float: left" type="button" class="btn btn-white" id="closeBtn" onclick="parent.swal.close()">Close</button>
                    <button type="submit" class="btn btn-primary" data-style="zoom-in">Add Carrier</button>
                </div>
            </form>

        </div>
    </div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<script>
    $('#carrierForm').ajaxForm({
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    });

    function beforeSubmit(data) {

    }

    function afterSubmit(resp,status,other) {
        resp = JSON.parse(resp);
        if(resp.status == true && status == "success" && other.status == 200){
            // success
            toastr.success("success","Carrier added");
        }else{
            // failed
            if(resp.status == true){
                toastr.error("error","Error saving carrier");
            }else{
                toastr.error("error",resp.reason);
            }
        }

        parent.getCarriers();
        $("#closeBtn").click();
    }

    function updateList() {
        var input = document.getElementById('hiddenFile');
        var output = document.getElementById('fileList');
        output.innerHTML = "";

        var ul = document.createElement("ul");
        ul.setAttribute("style","margin: 0;padding-left: 0;list-style: none;margin-top: 13px;");

        for (var i = 0; i < input.files.length; ++i) {
            var li = document.createElement("li");
            li.innerHTML += input.files.item(i).name;

            ul.appendChild(li);
        }
        output.appendChild(ul);
    }



</script>