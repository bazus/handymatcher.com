<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <form id="addFolder" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/addFolder.php">
                <?php if(isset($_GET['folder'])){ ?>
                    <input type="hidden" name="parentFolder" value="<?= $_GET['folder'] ?>">
                <?php } ?>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control" type="text" name="folderName" placeholder="Enter Folder Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in">Create Folder</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#addFolder').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        l.ladda( 'stop' );
        $('#myCustomModal').modal('hide');

        if(statusText == "success"){
            var data = responseText;
            data = JSON.parse(data);
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 3000
            };

            if(data == true){
                toastr.success('Folder Created','Success');
                getFolders(<?= $_GET['folder'] ?>);
            }else{
                // Failed
                toastr.error('Your request was not sent, please try again later.','Error');
            }
        }else{
            // Failed
            toastr.error('Your request was not sent, please try again later.','Error');
        }
    }
</script>