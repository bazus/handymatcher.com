<?php
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
$lead = new lead($_GET['leadId'],$_GET['orgId']);
$leadData = $lead->getData();
?>
Dear <?= $leadData['firstname'] ?>,<br>
Your free moving estimate is ready for you to review.<br>
Please <a href="<?php echo $_SERVER["YMQ_URL"]."/".$leadData['secretKey'] ?>" target="_blank">click here</a> to go over the inclusions and parameters and if you wish, you can e-sign and lock your great price in!<br>
<br>
Sincerely,<br>
<?= $bouncer['organizationData']['organizationName'] ?>