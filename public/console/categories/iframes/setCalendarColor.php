<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/calendarSettings.php");

$calendar = new calendarSettings($bouncer["credentials"]["userId"]);
$calendarColor = $calendar->getCalendarColor( $_GET['calendarId']);
//var_dump($calendarColor[0]);
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
    var calendarId = "<?= $_GET['calendarId']; ?>";
    var calendarColor = "<?= $calendarColor[0] ?>";

</script>
<style>
    .calendarColorModal .modal-body{
        padding: 7px 15px;
    }
    .calendarColorModal .form-group{
        margin-bottom: 10px;
    }
    .calendarColorModal .modal-footer{
        padding: 10px;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>

<div class="modal inmodal calendarColorModal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog" style="max-width: 450px;">

        <div class="modal-content animated fadeIn">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 modal-body">
                        <div class="form-group">
                            <label for="calendarColor">Calendar Color</label>
                            <div id="calendarColor"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top:0;">
                    <button class="btn btn-primary" onclick="setColor()" id="updateColor">Save</button>
                    <button class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.min.js"></script>

<script>
    $(document).ready(function () {


        var mcp = document.getElementById("calendarColor");
        createPicker(mcp);
        setCustomColor(calendarColor);

    });

    function setColor() {

        var color = getColor();

        var strUrl = BASE_URL + '/console/actions/system/calendar/setCalendarColor.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data: {
                calendarId:calendarId,
                color:color
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data == true){
                    parent.calendarController.getCalendarColor(calendarId);
                    $("#myCustomModal").modal('hide');
                }
            } catch (e) {
                toastr.error("Calendar color change failed");
            }
        });
    }


</script>