<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,false,array(["mailcenter",3]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <h4 class="modal-title">Add New Folder</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" name="name" id="name" class="form-control" onkeyup="checkForm()" placeholder="New folder name">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" id="createFolder" disabled data-style="zoom-in">Create Folder</button>
            </div>
        </div>
    </div>
</div>

<script>
    var l;

    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    $(".ladda-button-demo").on("click",function () {
        l.ladda("start");

        var name = $("#name").val();

        var strUrl = BASE_URL+'/console/actions/mail/preEmails/createFolder.php', strReturn = "";

        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                name:name
            },
            success: function (html) {
                strReturn = html;
            },
            async: false
        }).done(function (data) {

            $(document).ready(function () {
                getFoldersData();
            });
            $("#execModal").click();

        });
    });

    function checkForm(){
        if ($("#name") != ""){
            $("#createFolder").attr("disabled",false);
        }else{
            $("#createFolder").attr("disabled",true);
        }
    }

</script>