<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,false,array(["mailcenter",2]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$folders = $preEmails->getFoldersData();
?>
<style>
    .wickedpicker {
        z-index: 9999;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <h4 class="modal-title">Change "<?= $_GET['name'] ?>" Template Folder</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select onchange="checkForm()" class="form-control" id="templateFolder">
                        <option value="">Select Template Folder</option>
                        <option value="NULL">Home Folder</option>
                        <?php foreach ($folders as $folder){ ?>
                        <option value="<?= $folder['id'] ?>"><?= $folder['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" id="changeFolder" disabled data-style="zoom-in">Move to folder</button>
            </div>
        </div>
    </div>
</div>

<script>
    var l;

    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    $("#changeFolder").on("click",function () {
        l.ladda("start");

        var folder = $("#templateFolder").val();

        if (folder && folder != ""){}else{l.ladda("stop");return false;}

        var strUrl = BASE_URL+'/console/actions/mail/preEmails/changeFolder.php', strReturn = "";

        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                folder:folder,
                id:<?= $_GET['id'] ?>
            },
            success: function (html) {
                strReturn = html;
            },
            async: false
        }).done(function (data) {

            $(document).ready(function () {
                selectedFolder = folder;
                getData();
            });
            $("#execModal").click();

        });
    });

    function checkForm(){
        if ($("#templateFolder") != ""){
            $("#changeFolder").attr("disabled",false);
        }else{
            $("#changeFolder").attr("disabled",true);
        }
    }

</script>