<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,false,array(["departments",3]));

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Department</h4>
                <small>You can create your own departments.</small>
            </div>
            <form id="addDep" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/addDep.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title">Name</label>
                        <input class="form-control" type="text" name="title" placeholder="Enter Department Name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input class="form-control" type="text" name="description" placeholder="Enter Department Description">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input class="form-control" type="text" name="address" placeholder="Enter Department Address">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input class="form-control" type="text" name="city" placeholder="Enter Department City">
                    </div>
                    <div class="form-group">
                        <label for="state">State</label>
                        <input class="form-control" type="text" name="state" placeholder="Enter Department State">
                    </div>
                    <div class="form-group">
                        <label for="zip">ZIP</label>
                        <input class="form-control" type="text" name="zip" placeholder="Enter Department ZIP">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" type="text" name="phone" placeholder="Enter Department Phone">
                    </div>
                    <div class="form-group">
                        <label for="owner">Owner Name</label>
                        <input class="form-control" type="text" name="owner" placeholder="Enter Department Owner Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in">Create Department</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#addDep').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        l.ladda( 'stop' );
        $('#myCustomModal').modal('hide');

        if(statusText == "success"){
            var data = responseText;
            data = JSON.parse(data);
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };

            if(data == true){
                toastr.success('Your Department Data was successfully sent.','Department Sent');
                window.location = '?tab=departments';
            }else{
                // Failed
                toastr.error('Your request was not sent, please try again later.','Error');
            }
        }else{
            // Failed
            toastr.error('Your request was not sent, please try again later.','Error');
        }
    }
</script>