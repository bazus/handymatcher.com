<?php
if(isset($_GET['leadId'])) {
    $pagePermissions = array(false, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $organizationMailAccounts = $mailaccounts->getMyAccounts(true, true);

    $leadId = $_GET['leadId'];

    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);

    $leadData = $lead->getData();
    $hasDefaultEmail = false; // used to determine if to add "disabled" attribute the Send button
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
    .estimateContainer *{
        font-family: 'Montserrat', sans-serif;;
    }
    .estimateEmail{
        width: 850px;
        padding-top: 0;
        margin-top: 10px;
    }
    .estimateContainer{
        width: 80%;
        display: block;
        margin: 15px auto;
    }
    h2.bold{
        font-weight: bold;
    }
    .jobNo{
        font-size: 14px;
        color: #2f79ff;
        font-weight: 100;
    }
    .estimateHr{
        border-top: 1px solid #afadad;
        margin-bottom: 15px;
        margin-top: 15px;
    }
    h5 span.pull-right{
        font-weight: 100;
    }
    #myCustomModal .modal-footer{
        margin-top: 0;
    }
    .bottom-line{
        border-top: 1px solid black;
        display: block;
        width: 100%;
    }
    .topSpacer{
        margin-top: 50px;
    }
    .estimateContainer h3{
        margin-bottom: 15px;
    }
    .mailContent .form-group{
        margin-bottom: 5px;
        height: 34px;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog estimateEmail">

        <div class="modal-content animated fadeIn" style="box-shadow: none !important;">
            <div class="modal-head">
                <div class="row">
                    <div class="col-md-12 mailContent" style="padding: 15px 15px 0 15px;">
                        <?php if (count($organizationMailAccounts) > 0){ ?>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">From:</label>
                                <div class="col-sm-11">
                                    <select class="form-control" id="mailAccount">
                                        <option value="">Select Mail Account</option>
                                        <?php
                                        foreach($organizationMailAccounts as $organizationMailAccount){
                                            ?>
                                            <option <?php if (count($organizationMailAccounts) == 1){echo "selected";} ?> <?php if ($organizationMailAccount['id'] == $bouncer['organizationData']['mailAccount']){echo 'selected';$hasDefaultEmail = true;} ?> value="<?php echo $organizationMailAccount['id']; ?>" ><?php echo $organizationMailAccount['email']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">To:</label>
                                <div class="col-sm-11">
                                    <input type="text" id="toEmail" class="form-control cancelTo" placeholder="To Email Address" disabled value="<?= $leadData['email'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">Subject:</label>
                                <div class="col-sm-11">
                                    <input type="text" id="subjectEmail" class="form-control" placeholder="Email Subject" value="<?= "Moving Estimate From ".$bouncer['organizationData']['organizationName'] ?>">
                                </div>
                            </div>
                        <?php }else{ ?>
                            <div class="alert alert-info" style="width: 80%;display: block;margin: 0 auto;margin-bottom: 11px;">
                                No Active Mail Account, <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/mailSettings.php">Create One</a>
                            </div>
                        <?php } ?>
                        <hr style="margin-bottom: 0px;">
                    </div>
                </div>
            </div>

            <div class="modal-body" style="height: 75vh;background: #ffffff !important;padding: 0px;">
                <iframe src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/iframes/sendEstimateEmailText.php?leadId=<?= $_GET['leadId'] ?>&userId=<?= $bouncer["credentials"]["userId"] ?>&orgId=<?= $bouncer["credentials"]['orgId'] ?>" style="width: 100%;border: none;height: 100%;"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="button" <?php if (!$hasDefaultEmail){echo 'disabled';} ?> class="ladda-button ladda-button-demo btn btn-primary" id="sendEstimate" data-style="zoom-in">Send</button>
            </div>
        </div>
    </div>
</div>


<!-- Ladda -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });
    $("#sendEstimate").on('click',function () {
        l.ladda("start");

        var subjectEmail = $("#subjectEmail").val();

        var strUrl = BASE_URL+'/console/actions/mail/sendEstimateToLead.php';

        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                "leadId": '<?= $leadId ?>',
                "emailAccountId": $("#mailAccount").val(),
                subjectEmail: subjectEmail
            },
            async: true
        }).done(function (data) {
            try {
                l.ladda( 'stop' );
                data = JSON.parse(data);
                if (data.status == true){
                    toastr.success("Email Sent Successfully","Sent");
                    $('#myCustomModal').modal('hide');
                } else{
                    for (var i = 0;i<data.errors.length;i++){
                        toastr.error(data.errors[i],"ERROR");
                    }
                }
            }catch (e) {
                l.ladda( 'stop' );
                toastr.error("Email Not Sent","ERROR");
            }

        });
    });
    $("#mailAccount").on("change",function () {
       if ($(this).val() != ""){
           $("#sendEstimate").attr("disabled",false);
       }else{
           $("#sendEstimate").attr("disabled",true);
       }
    });
    <?php if (count($organizationMailAccounts) == 1){?>
        $("#sendEstimate").attr("disabled",false);
    <?php } ?>
</script>