<?php
?>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
    .row{
        flex-wrap: nowrap;
        margin-bottom: 5px;
    }
    label{
        margin-bottom: 0;
    }
    #newPayment{
        padding: 1rem;
         height: 735px;
    }
    #footer{
        display: flex;
        justify-content: space-between;
        position: fixed;
        width: 100%;
        bottom: 0;
        padding: 10px;
    }
    #content{
        padding: 1rem;
    }
    body{
    }
    @media only screen and (max-width: 468px) {
        .row{
            display: block;
            flex-direction: column;
        }
        .row label, .input-group-prepend {
            width: 100%;
        }
        .col-7{
            width:100%;

        }
        .row label{
            padding:0 0 0 1rem;
        }
        #closePayment, #savePayment{
            width: 45% !important;
        }
        #footer{
            position: unset;
        }
    }

    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }


    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }

</style>
<div id="wrapper" style="visibility: hidden;">
    <div id="newPayment" style="padding: 0px;">
    <div id="content">
        <div class="feed-element">
            <div class="alert alert-info" style="font-size: 0.9em;margin-bottom: 13px !important;height: auto;">Payments info are stored securely in our database and helps you keep track of your earnings.</div>
            <div class="form-group row">
                <label for="method" class="col-5 col-form-label col-form-label-sm">Method Of Payment</label>
                <div class="col-7 input-group-sm  input-group-prepend">
                    <select id="method" onchange="setMethodInputs(this.value)" name="method" class="form-control inputer">
                        <option value="1">Credit Card</option>
                        <option value="2">PayPal</option>
                        <option value="3">Check</option>
                        <option value="4">Wire Transfer</option>
                        <option value="5">Cash</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="amount" class="col-5 col-form-label col-form-label-sm">Amount</label>
                <div class="col-7 input-group-sm input-group-prepend">
                    <span class="input-group-text form-control">USD</span>
                    <input type="number" min="0" max="100000" value="0" name="amount" id="amount" class="form-control inputer">
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="vatPer" class="col-5 col-form-label col-form-label-sm">Tax (VAT)</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <span style="height: 31px" class="input-group-text">%</span>
                        <input type="number" min="0" max="100" value="0" name="vatPer" id="vatPer" class="form-control inputer">
                        <select class="form-control squareSelect" id="vatType">
                            <option value="0">exclusive</option>
                            <option value="1">inclusive</option>
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="paymentDescription" class="col-5 col-form-label-sm">Description</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="paymentDescription" id="paymentDescription" placeholder="Description (e.g 'First payment')" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="remarks" class="col-5 col-form-label-sm">Remarks</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <textarea name="remarks" id="remarks" class="form-control inputer" style="resize: none;"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="feed-element">
            <div>
                <div class="form-group row">
                    <label for="address" class="col-5 col-form-label-sm">Address</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="address" id="address" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="city" class="col-5 col-form-label-sm">City</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="city" id="city" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="country" class="col-5 col-form-label-sm">State</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="country" id="country" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="zip" class="col-5 col-form-label-sm">Zip</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="number" name="zip" id="zip" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="phone" class="col-5 col-form-label-sm">Phone</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="phone" id="phone" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="paymentEmail" class="col-5 col-form-label-sm">Email</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="email" name="paymentEmail" id="paymentEmail" class="form-control inputer">
                    </div>
                </div>
            </div>
        </div>
        <div class="feed-element">
            <div>
                <div class="form-group row">
                    <label for="cardHolder" class="col-5 col-form-label-sm">Card Name Holder</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="cardHolder" id="cardHolder" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="creditCardNumber" class="col-5 col-form-label-sm">Credit Card Number</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="creditCardNumber" id="creditCardNumber" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="paymentYear" class="col-5 col-form-label-sm">Expiration Date</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <select id="paymentMonth" class="form-control halfInput">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        <select id="paymentYear" class="form-control halfInput">
                            <?php for ($i=0;$i<=20;$i++){ ?>
                                <option value="<?= date("Y",strtotime("+".$i." years")) ?>"><?= date("Y",strtotime("+".$i." years")) ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="securityCode" class="col-5 col-form-label-sm">Security Code</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="securityCode" id="securityCode" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group row">
                    <label for="confirm" class="col-5 col-form-label-sm">Credit Card Confirmation</label>
                    <div class="col-7 input-group-sm input-group-prepend">
                        <input type="text" name="confirm" id="confirm" class="form-control inputer">
                    </div>
                </div>
            </div>
            <div id="spanPaymentError" style="margin-bottom: 9px;text-align: center;margin-top: 9px;"><small class="text-danger">&zwnj;</small></div>
        </div>
    </div>
    <div id="footer">
        <input id="closePayment" style="display: inline-table;width: 30%" onclick="parent.swal.close();" class="swal-button swal-button--cancel" type="button" value="Close">
        <input id="savePayment" style="display: inline-table;width: 30%;margin-top: 0px;" onclick="paymentController.savePayment()" class="swal-button swal-button--save" type="button" value="Save Payment">
    </div>
</div>
</div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<script>

    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });
    
    var paymentController = {
        savePayment: function () {

            var paymentData = {
                "method": document.getElementById("method").value,
                "description": document.getElementById("paymentDescription").value,
                "amount": document.getElementById("amount").value,
                "vatPer": document.getElementById("vatPer").value,
                "vatIsInclusive": document.getElementById("vatType").value,
                "remarks": document.getElementById("remarks").value,
                "cardHolder": document.getElementById("cardHolder").value,
                "address": document.getElementById("address").value,
                "city": document.getElementById("city").value,
                "zip": document.getElementById("zip").value,
                "country": document.getElementById("country").value,
                "phone": document.getElementById("phone").value,
                "email": document.getElementById("paymentEmail").value,
                "creditCardNumber": document.getElementById("creditCardNumber").value,
                "securityCode": document.getElementById("securityCode").value,
                "expDate": document.getElementById("paymentMonth").value + " - " + document.getElementById("paymentYear").value,
                "creditCardConfirmation": document.getElementById("confirm").value,
            };
            if (document.getElementById("noPaymentRow")) {
                document.getElementById("noPaymentRow").remove();
            }

            if (paymentData.amount == "" || paymentData.amount == "0") {
                setPaymentError();
                setWrongLabel("amount");
                return;
            }

            if (paymentData.method == 1) {

                if (paymentData.cardHolder == "") {
                    setPaymentError();
                    setWrongLabel("cardHolder");
                    return;
                }
                if (paymentData.creditCardNumber == "") {
                    setPaymentError();
                    setWrongLabel("creditCardNumber");
                    return;
                }
                if (document.getElementById("paymentMonth").value == "") {
                    setPaymentError();
                    return;
                }
                if (document.getElementById("paymentYear").value == "") {
                    setPaymentError();
                    return;
                }
                if (paymentData.securityCode == "") {
                    setPaymentError();
                    setWrongLabel("securityCode");
                    return;
                }
            }
            $("#spanPaymentError").children().text("");


            var strUrl = BASE_URL + '/console/actions/moving/payments/savePayment.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "post",
                data: {
                    leadId: parent.leadId,
                    data: paymentData
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data != false) {

                        paymentData.id = data;
                        paymentData.amount = formatMoney(paymentData.amount);

                        parent.leadController.lead.getLeadPaymentDetails();

                        setMethodInputs('1');
                        document.getElementById("method").value = '1';
                        document.getElementById("confirm").value = "";
                        document.getElementById("paymentDescription").value = "";
                        document.getElementById("amount").value = "";
                        document.getElementById("remarks").value = "";
                        document.getElementById("cardHolder").value = "";
                        document.getElementById("address").value = "";
                        document.getElementById("city").value = "";
                        document.getElementById("zip").value = "";
                        document.getElementById("country").value = "";
                        document.getElementById("phone").value = "";
                        document.getElementById("paymentEmail").value = "";
                        document.getElementById("creditCardNumber").value = "";
                        document.getElementById("securityCode").value = "";

                        parent.swal.close();
                        $(parent)[0].swal("Payment added","", "success");

                    } else {
                        $(parent)[0].swal("Oops", "Your payment was not saved. Please try again later.", "error");
                        return false;
                    }
                } catch (e) {
                    $(parent)[0].swal("Oops", "Your payment was not saved. Please try again later.", "error");
                    return false;
                }
            });
        },
        getCCAF:function (ccaf) {
            if(ccaf){
                var strUrl = BASE_URL+'/console/actions/moving/payments/getUnHashCC.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method:"post",
                    data:{
                        card:ccaf.cardNumber
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {

                    try {
                        var unHashedCC = JSON.parse(data);
                        $('#cardHolder').val(ccaf.cardholderName);
                        $('#amount').val(ccaf.signedAmount);
                        $('#creditCardNumber').val(unHashedCC);
                        $('#paymentMonth').val(ccaf.expMonth);
                        $('#paymentYear').val(ccaf.expYear);
                        $('#securityCode').val(ccaf.CVV);
                        $('#zip').val(ccaf.zipCode);
                        $('#address').val(ccaf.address);
                        $('#country').val(ccaf.state);
                        $('#city').val(ccaf.city);
                    }catch (e) {
                        $(parent)[0].swal("Oops", "Your payment was not saved. Please try again later.", "error");
                        return false;
                    }
                });
            }
        }
    }
    function checkWidth(init) {

        if ($(window).width() < 468) {
            $('.input-group-prepend').addClass('col');
            $('.input-group-prepend').removeClass('col-7');
            $('label').removeClass('col-5');
            $('label').addClass('col');
        } else {
            if (!init) {
                $('.input-group-prepend').removeClass('col');
                $('.input-group-prepend').addClass('col-7');
                $('label').removeClass('col');
                $('label').addClass('col-5');
            }
        }
    }

    $(document).ready(function() {
        checkWidth(true);

        $(window).resize(function() {
            checkWidth(false);
        });
    });

    function setWrongLabel(id){

        $("#"+id).addClass("is-invalid");
        setTimeout(function () {
            $("#"+id).removeClass("is-invalid");
        },2000);
    }
    function setPaymentError(){

        $("#spanPaymentError").css("display","");
        $("#spanPaymentError").children().text("* Please Fll All The Inputs");
    }
    function formatMoney(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }
    function setMethodInputs(val){
        switch (val) {
            case '1':
                appear("#confirm",1);
                appear("#cardHolder",1);
                appear("#creditCardNumber",1);
                appear("#paymentMonth",1);
                appear("#securityCode",1);
                break;
            default:
                appear("#confirm",2);
                appear("#cardHolder",2);
                appear("#creditCardNumber",2);
                appear("#paymentMonth",2);
                appear("#securityCode",2);
                break;
        }
    }
    function appear(input,type){
        if (type == 1){
            $(input).parent().parent().css("display", "");
        }else{
            $(input).parent().parent().css("display", "none");
        }
    }
</script>