<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadReminders.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

$organization = new organization($bouncer["credentials"]["orgId"]);
$usersOfMyCompany = $organization->getUsersOfMyCompany($bouncer["credentials"]["userId"]);

$leadReminders = new leadReminders($_GET['leadId'],$bouncer["credentials"]["orgId"]);
$reminder = $leadReminders->getData();

$reminderId = NULL;

if($reminder != false){
    $reminderId = $reminder['reminderData']['id'];
}

$page = "";
if (isset($_GET['calendar']) && $_GET['calendar'] == 1){
    $page = "calendar";
}
?>

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
    var leadId = "<?= $_GET['leadId']; ?>";
</script>
<style>
    .daterangepicker .range_inputs{
        text-align: left;
        margin: 6px;
    }
    .daterangepicker .calendar-time{
        margin: 6px;
    }
    .daterangepicker{
        z-index: 9999;
        padding: 10px;
        text-align: center;
    }
    #reminderStartTimePicker,#reminderEndTimePicker{
        font-size: 12px;
    }
    .reminderModal .modal-body{
        padding: 7px 15px;
    }
    .reminderModal .form-group{
        margin-bottom: 10px;
    }
    .reminderModal .modal-footer{
        padding: 10px;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>

<div class="modal inmodal reminderModal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog" style="max-width: 450px;">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="padding: 2px;">
                <button type="button" class="close" data-dismiss="modal" style="margin-top: 0 !important;margin-right: 3px;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 modal-body">
                        <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                        <div class="form-group">
                            <select class="form-control" id="selectedUser">
                                <?php if ($reminder != false){ ?>
                                    <option <?php if ($reminder['calendarData']['userId'] == $bouncer["credentials"]["userId"]){echo "selected";} ?> value="<?= $bouncer["credentials"]["userId"] ?>">Assign To Me</option>
                                    <?php foreach ($usersOfMyCompany as $user){ ?>
                                        <option <?php if ($reminder['calendarData']['userId'] == $user['id']){echo 'selected';}?> value="<?= $user['id'] ?>">Assign To <?= $user['fullName'] ?></option>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <option value="<?= $bouncer["credentials"]["userId"] ?>">Assign To Me</option>
                                    <?php foreach ($usersOfMyCompany as $user){ ?>
                                        <option value="<?= $user['id'] ?>">Assign To <?= $user['fullName'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="reminderSubject">Subject</label>
                            <?php if ($reminder != false){ ?>
                                <input type="text" value="<?= $reminder['calendarData']['title'] ?>" class="form-control" id="reminderSubject">
                            <?php }else{ ?>
                                <?php
                                $lead = new lead($_GET['leadId'],$bouncer["credentials"]["orgId"]);
                                $leadData = $lead->getData();
                                    $jobID = "";
                                    if($leadData["jobNumber"] != NULL && $leadData["jobNumber"] != ""){
                                        $jobID = $leadData["jobNumber"];
                                    }else{
                                        $jobID = $leadData["id"];
                                    }
                                ?>
                                <input type="text" value="Job #<?= $jobID ?> | Follow Up" class="form-control" id="reminderSubject">
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="reminderStartTime">Start Time</label>
                                    <div id="reminderStartTimePicker" class="form-control">
                                        <i class="fa fa-calendar"></i>
                                        <span id="reminderStartTime"></span><b class="caret"></b>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="reminderEndTime">End Time</label>
                                    <div id="reminderEndTimePicker" class="form-control">
                                        <i class="fa fa-calendar"></i>
                                        <span id="reminderEndTime"></span><b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="firstReminder">Reminder</label>
                                    <select class="form-control" id="firstReminder">
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime'] === NULL ){ echo 'selected';}?> value="">No Reminder (optional)</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 0 ){ echo 'selected';}?> value="0">At time of event</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 5 ){ echo 'selected';}?> value="5">5 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 10 ){ echo 'selected';}?> value="10">10 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 15 ){ echo 'selected';}?> value="15">15 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 30 ){ echo 'selected';}?> value="30">30 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 60 ){ echo 'selected';}?> value="60">1 Hour before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['reminderTime']/60 == 12 ){ echo 'selected';}?> value="120">2 Hours before start time</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="secondReminder">Second Reminder</label>
                                    <select class="form-control" id="secondReminder">
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder'] == NULL ){ echo 'selected';}?> value="">No Reminder (optional)</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 0 ){ echo 'selected';}?> value="0">At time of event</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 5 ){ echo 'selected';}?> value="5">5 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 10 ){ echo 'selected';}?> value="10">10 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 15 ){ echo 'selected';}?> value="15">15 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 30 ){ echo 'selected';}?> value="30">30 Minutes before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 60 ){ echo 'selected';}?> value="60">1 Hour before start time</option>
                                        <option <?php if ($reminder != false && $reminder['reminderData']['secondReminder']/60 == 120 ){ echo 'selected';}?> value="120">2 Hours before start time</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="reminderColor">Reminder Event Color</label>
                            <div id="reminderColor"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top:0;">
                <?php if ($reminder != false){ ?>
                    <button class="btn btn-primary" onclick="sendReminder()" id="updateReminder">Update Reminder</button>
                    <button class="btn btn-danger" onclick="deleteReminder(<?= $reminder['reminderData']['id'] ?>)" id="deleteReminder">Delete Reminder</button>
                    <?php if (isset($reminder['reminderData']) && $page == "calendar") { ?>
                        <span class="pull-left" style="margin-bottom: 5px;">
                            <a class="btn btn-default" style="padding: 9px;font-size: 11px;font-weight: bold;border: 1px solid #b3b3b4;" href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/lead.php?leadId=<?=$_GET['leadId']?>" target="_blank">Open Lead</a>
                        </span>
                    <?php } ?>
                <?php }else{ ?>
                    <button class="btn btn-primary" onclick="sendReminder()" id="addReminder">Add Reminder</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>
<script>
    var startTime;
    var endTime;
    $(document).ready(function () {


        // Start Time
        <?php if ($reminder != false && isset($reminder['calendarData']['startDate'])){ ?>
        $('#reminderStartTimePicker #reminderStartTime').html(moment("<?= $reminder['calendarData']['startDate'] ?>").format('MMMM D, YYYY HH:mm'));
        startTime = moment("<?= $reminder['calendarData']['startDate'] ?>").format("YYYY-MM-DD HH:mm");
        <?php }else{ ?>
        $('#reminderStartTimePicker #reminderStartTime').html(moment().format('MMMM D, YYYY HH:mm'));
        startTime = moment().format("YYYY-MM-DD HH:mm");
        <?php } ?>



        $('#reminderStartTimePicker').daterangepicker({
            singleDatePicker: true,
            format: 'MM/DD/YYYY',
            <?php if ($reminder != false && isset($reminder['calendarData']['startDate'])){ ?>
                startDate: moment("<?= $reminder['calendarData']['startDate'] ?>"),
            <?php }else{ ?>
                startDate: moment(),
            <?php } ?>
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: true,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Go',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {
            $('#reminderStartTimePicker #reminderStartTime').html(end.format('MMMM D, YYYY HH:mm'));
            startTime = start.format("YYYY-MM-DD HH:mm");
        });

        // End Time
        <?php if ($reminder != false && $reminder['calendarData']['endDate']){ ?>
        $('#reminderEndTimePicker #reminderEndTime').html(moment("<?= $reminder['calendarData']['endDate'] ?>").format('MMMM D, YYYY HH:mm'));
        endTime = moment("<?= $reminder['calendarData']['endDate'] ?>").format("YYYY-MM-DD HH:mm");
        <?php }else{ ?>
        $('#reminderEndTimePicker #reminderEndTime').html(moment().format('MMMM D, YYYY HH:mm'));
        endTime = moment().format("YYYY-MM-DD HH:mm");
        <?php } ?>
        $('#reminderEndTimePicker').daterangepicker({
            singleDatePicker: true,
            format: 'MM/DD/YYYY',
            <?php if ($reminder != false && $reminder['calendarData']['endDate']){ ?>
                startDate: moment("<?= $reminder['calendarData']['endDate'] ?>"),
            <?php }else{ ?>
                startDate: moment(),
            <?php } ?>
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: true,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Go',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {
            $('#reminderEndTimePicker #reminderEndTime').html(end.format('MMMM D, YYYY HH:mm'));
            endTime = start.format("YYYY-MM-DD HH:mm");
        });
        var mcp = document.getElementById("reminderColor");
        createPicker(mcp);
        <?php if ($reminder != false && $reminder['reminderData']) { ?>
            setCustomColor("<?= $reminder['reminderData']['color'] ?>");
        <?php } ?>

    });


    function sendReminder() {

        var subject = $("#reminderSubject").val();
        var reminder = $("#firstReminder").val();
        var reminder2 = $("#secondReminder").val();
        var color = getColor();

        var strUrl = BASE_URL + '/console/actions/leads/lead/setReminder.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data: {
                reminderId: "<?php echo $reminderId; ?>",
                leadId: leadId,
                subject:subject,
                dateStart:startTime,
                dateEnd:endTime,
                reminder:reminder,
                reminder2:reminder2,
                user: $("#selectedUser").val(),
                color:color
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data == true){
                    toastr.success("Reminder updated","Updated");

                    <?php if ($reminder != false && isset($reminder['reminderData']) && $page == "calendar") { ?>
                    calendarController.getEvents();
                    <?php } ?>

                    $("#myCustomModal").modal('hide');
                }
            } catch (e) {
                toastr.error("Saving Reminder Failed");
            }
        });
    }

    function deleteReminder(id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Reminder",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then((isConfirm)=> {
            if (isConfirm) {
                var strUrl = BASE_URL + '/console/actions/leads/lead/deleteReminder.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        id: id,
                        leadId: leadId
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true) {
                            toastr.success("Reminder Deleted", "Deleted");
                            $("#myCustomModal").modal('hide');
                        }
                    } catch (e) {
                        toastr.error("Deleting Reminder Failed");
                    }
                });
            }
        });

    }
</script>