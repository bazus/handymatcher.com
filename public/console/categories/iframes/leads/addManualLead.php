<?php

$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");
$leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
$providers = $leadProviders->getProviders(true);
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/autocomplete.css" rel="stylesheet">
<style>
    .col-md-6 label,.col-md-6 input,.col-md-6 select{
        margin-left: -15px;
    }
    .right-side{
        margin-left: 15px !important;
    }
    #movingSpan{
        margin-left: -15px;
        margin-top: 15px;
        display: block;
        cursor: pointer;
    }
    .modal-dialog .row{
        margin-bottom: 7px;
    }
    #movingDetails{
        margin-top: 15px;
        margin-left: -15px;
        display: none;
    }
    #movingDetails .col-md-6 label,#movingDetails .col-md-6 input,#movingDetails .col-md-6 select{
        margin-left: 0;
    }
    #movingDetails .row .col-sm-12{
        margin-bottom: 7px;
    }
    #movingDetails .right-side{
        margin-left: 0;
    }
    .bottomText{
        margin-left: 15px;
        margin-right: 15px;
        width: 97%;
    }
    .bottomLabel{
        margin-left: 15px;
    }
    .full-row{
        margin-top: 15px;
    }
    @media (max-width: 991px) {
        #movingDetails .row{
            display: flex;
            flex-direction: column;
        }
        .right-side{
            margin-left: 0 !important;
        }
        .bottomText{
            margin-left: 0;
            margin-right: 0;
            width: 100%;
        }
        .bottomLabel{
            margin-left: 0;
        }
        .col-md-6 label,.col-md-6 input,.col-md-6 select{
            margin-left: 0 !important;
            width: 100% !important;
        }
        .col-sm-12{
            padding: 0px;
        }
    }
    @media (max-width: 480px) {
        #myCustomModal .pull-right{
            margin-bottom: unset;
            float: right !important;
        }
    }

    .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
    .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
    .autocomplete-selected { background: #F0F0F0; }
    .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
    .autocomplete-group { padding: 2px 5px; }
    .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }

</style>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Insert New Lead</h4>
                <small>You can manually insert leads to the system.</small>
            </div>
            <form id="groupForm">
                <div class="modal-body" style="padding-top: 0px;padding-bottom: 9px;">
                    <div class="row">
                        <div class="col-md-12 modal-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <label for="firstname">Firstname</label>
                                    <input type="text" required="required" class="form-control" id="firstname" name="firstname">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <label class="right-side" for="lastname">Lastname</label>
                                    <input type="text" class="form-control right-side" id="lastname" name="lastname">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <label for="phone">Phone</label>
                                    <input type="text" required="required" class="form-control" id="phone" name="phone">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <label class="right-side" for="phone2">Secondary Phone</label>
                                    <input type="text" class="form-control right-side" id="phone2" name="phone2">
                                </div>
                            </div>
                            <div class="row">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="row">
                                <label for="comments">Comments</label>
                                <textarea class="form-control" style="resize: vertical;" id="comments" name="comments"></textarea>
                            </div>
                            <div class="row">
                                <label for="provider">Select Provider</label>
                                <select class="form-control" id="provider" name="provider">
                                    <option value="0">Manual</option>
                                    <?php foreach ($providers as $provider){ ?>
                                        <option value="<?= $provider['id'] ?>"><?= $provider['providerName'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="row" style="margin-top: 16px;margin-bottom: -11px;">
                                <small class="pull-right text-navy">
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="vipLead" class="onoffswitch-checkbox" id="vipLead">
                                            <label class="onoffswitch-label" for="vipLead">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </small>
                                <label for="vipLead">Hot Lead</label>
                            </div>
                            <hr style="margin-right: -15px;margin-left: -15px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <span id="movingSpan" onclick="toggleMovingDetails()">Moving Details <i id="movingBtn" class="fa fa-arrow-down pull-right"></i></span>
                                </div>
                            </div>
                            <div id="movingDetails">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12" style="order: 1;">
                                        <label for="fromCity">From City</label>
                                        <input type="text" class="form-control" id="fromCity" name="fromCity" autocomplete="off">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 9;">
                                        <label class="right-side" for="toCity">To City</label>
                                        <input type="text" class="form-control right-side" id="toCity" name="toCity" autocomplete="off">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 2;">
                                        <label for="fromState">From State</label>
                                        <input type="text" class="form-control" id="fromState" name="fromState" autocomplete="off">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 10;">
                                        <label class="right-side" for="toState">To State</label>
                                        <input type="text" class="form-control right-side" id="toState" name="toState" autocomplete="off">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 3;">
                                        <label for="fromZip">From Zip</label>
                                        <input type="text" class="form-control" id="fromZip" name="fromZip" autocomplete="off">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 11;">
                                        <label class="right-side" for="toZip">To Zip</label>
                                        <input type="text" class="form-control right-side" id="toZip" name="toZip" autocomplete="off">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 4;">
                                        <label for="fromAddress">From Address</label>
                                        <input type="text" class="form-control" id="fromAddress" name="fromAddress">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 12;">
                                        <label class="right-side" for="toAddress">To Address</label>
                                        <input type="text" class="form-control right-side" id="toAddress" name="toAddress">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 5;">
                                        <label for="fromLevel">From Level</label>
                                        <input type="text" class="form-control" id="fromLevel" name="fromLevel">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 13;">
                                        <label class="right-side" for="toLevel">To Level</label>
                                        <input type="text" class="form-control right-side" id="toLevel" name="toLevel">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 6;">
                                        <label for="fromFloor">From Floor</label>
                                        <input type="text" class="form-control" id="fromFloor" name="fromFloor">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 14;">
                                        <label class="right-side" for="toFloor">To Floor</label>
                                        <input type="text" class="form-control right-side" id="toFloor" name="toFloor">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 7;">
                                        <label for="fromApartment">From Apartment</label>
                                        <input type="text" class="form-control" id="fromApartment" name="fromApartment">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 15;">
                                        <label class="right-side" for="toApartment">To Apartment</label>
                                        <input type="text" class="form-control right-side" id="toApartment" name="toApartment">
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 8;">
                                        <label for="fromApartmentType">From Apartment Type</label>
                                        <select class="form-control" id="fromApartmentType" name="fromApartmentType">
                                            <option></option>
                                            <option value="Apartment building">Apartment building</option>
                                            <option value="Private House">Private House</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-12" style="order: 16;">
                                        <label class="right-side" for="toApartmentType">To Apartment Type</label>
                                        <select class="form-control right-side" id="toApartmentType" name="toApartmentType">
                                            <option></option>
                                            <option value="Apartment building">Apartment building</option>
                                            <option value="Private House">Private House</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <span class="full-row"></span>
                                    <div class="col-sm-12" style="order: 17; padding: 0px;">
                                        <label for="moveDate" class="bottomLabel">Move Date</label>
                                        <input type="text" class="form-control bottomText" id="moveDate" name="moveDate">
                                    </div>
                                    <div class="col-sm-12" style="order: 18; padding: 0px;">
                                        <label for="moveSizeLBS" class="bottomLabel">Move Size (LBS)</label>
                                        <input type="number" min="0" class="form-control bottomText" id="moveSizeLBS" name="moveSizeLBS">
                                    </div>
                                    <div class="col-sm-12" style="order: 19; padding: 0px;">
                                        <label for="moveSize" class="bottomLabel">Move Size Rooms</label>
                                        <select class="form-control bottomText" id="moveSize" name="moveSize">
                                            <option value="">Select Number Of Rooms</option>
                                            <option value="Studio 1500 lbs">Studio 1500 lbs</option>
                                            <option value="1 BR Small 3000 lbs">1 BR Small 3000 lbs</option>
                                            <option value="1 BR Large 4000 lbs">1 BR Large 4000 lbs</option>
                                            <option value="2 BR Small 4500 lbs">2 BR Small 4500 lbs</option>
                                            <option value="2 BR Large 6500 lbs">2 BR Large 6500 lbs</option>
                                            <option value="3 BR Small 8000 lbs">3 BR Small 8000 lbs</option>
                                            <option value="3 BR Large 9000 lbs">3 BR Large 9000 lbs</option>
                                            <option value="4 BR Small 10000 lbs">4 BR Small 10000 lbs</option>
                                            <option value="4 BR Large 12000 lbs">4 BR Large 12000 lbs</option>
                                            <option value="Over 12000 lbs">Over 12000 lbs</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <small id="errorSpan" class="text-danger"></small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="ladda-button btn btn-primary pull-right" type="submit" data-style="zoom-out" onclick="sendLead()">
                        <span class="ladda-label">Insert Lead</span>
                        <span class="ladda-spinner"></span>
                    </button>
                    <button style="margin-right: 5px;" class="btn btn-white" id="closeBtn" data-dismiss="modal">Cancel</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/autocomplete/ac.min.js"></script>

<script>

    function toggleMovingDetails() {
        $("#movingDetails").slideToggle();
        $("#movingBtn").toggleClass("fa-arrow-up");
        $("#movingBtn").toggleClass("fa-arrow-down");
    }

    function sendLead(){
        l.ladda( 'start' );
        let vipLeadState = document.getElementById("vipLead").checked;
        if (vipLeadState){
            vipLeadState = 1;
        }else{
            vipLeadState = 0;
        }
        var data = {
            "firstname":document.getElementById("firstname").value,
            "lastname":document.getElementById("lastname").value,
            "email":document.getElementById("email").value,
            "phone":document.getElementById("phone").value,
            "phone2":document.getElementById("phone2").value,
            "comments":document.getElementById("comments").value,
            "providerId":document.getElementById("provider").value,
            "vipLead":vipLeadState
        };
        if (data.firstname == "" || data.phone == ""){
            l.ladda('stop');
            document.getElementById("errorSpan").innerText = "* Firstname, Phone And Email Are Required";
            return false;
        }else{
            document.getElementById("errorSpan").innerText = "";
        }
        var movingData = {
            "fromCity":document.getElementById("fromCity").value,
            "toCity":document.getElementById("toCity").value,
            "fromState":document.getElementById("fromState").value,
            "toState":document.getElementById("toState").value,
            "fromZip":document.getElementById("fromZip").value,
            "toZip":document.getElementById("toZip").value,
            "fromAddress":document.getElementById("fromAddress").value,
            "toAddress":document.getElementById("toAddress").value,
            "fromLevel":document.getElementById("fromLevel").value,
            "toLevel":document.getElementById("toLevel").value,
            "fromFloor":document.getElementById("fromFloor").value,
            "toFloor":document.getElementById("toFloor").value,
            "fromApartment":document.getElementById("fromApartment").value,
            "toApartment":document.getElementById("toApartment").value,
            "fromApartmentType":document.getElementById("fromApartmentType").value,
            "toApartmentType":document.getElementById("toApartmentType").value,
            "moveDate":document.getElementById("moveDate").value,
            "moveSizeLBS":document.getElementById("moveSizeLBS").value,
            "moveSize":document.getElementById("moveSize").value
        };

        var strUrl = BASE_URL+'/console/actions/leads/sendLead.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{data:data,movingData:movingData},
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try{
                data = JSON.parse(data);
                if(data != false){
                    window.location.replace(BASE_URL+"/console/categories/leads/lead.php?leadId="+data);
                }else{
                    l.ladda('stop');
                    swal("Oops", "Please try again later", "error");
                }
            }catch(e){
                l.ladda('stop');
                swal("Oops", "Please try again later", "error");
            }
        });
    }

    var l;
    $( document ).ready(function() {
        l = $( '.ladda-button' ).ladda();

        $('#moveDate').daterangepicker({
            timepicker:false,
            singleDatePicker: true,
            format: 'MM/DD/YYYY'
        });

        function initAutoComplete(id,type,input){
            var q = document.getElementById(id).value;
            $('#'+id).devbridgeAutocomplete({
                serviceUrl: '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leads/autoCompleteLocation.php?q='+q+"&type="+type+"&input="+input,
                onSelect: function (suggestion) {

                    if(type == "from"){

                        document.getElementById('fromState').value = suggestion.data.state;

                        if(input == "zip" || input == "city") {
                            document.getElementById('fromZip').value = suggestion.data.zip;
                            document.getElementById('fromCity').value = suggestion.data.city;
                        }

                    }
                    if(type == "to"){
                        document.getElementById('toState').value = suggestion.data.state;

                        if(input == "zip" || input == "city") {
                            document.getElementById('toZip').value = suggestion.data.zip;
                            document.getElementById('toCity').value = suggestion.data.city;
                        }

                    }

                    $('#'+id).devbridgeAutocomplete().disable();
                    document.getElementById(id).blur();
                    $('#'+id).devbridgeAutocomplete().enable();

                },
                noCache:true,
                preventBadQueries:true,
                forceFixPosition:true,
                showNoSuggestionNotice:true
            });
        }

        initAutoComplete('fromCity','from',"city");
        initAutoComplete('fromState','from',"state");
        initAutoComplete('fromZip','from',"zip");

        initAutoComplete('toCity','to',"city");
        initAutoComplete('toState','to',"state");
        initAutoComplete('toZip','to',"zip");

    });
</script>