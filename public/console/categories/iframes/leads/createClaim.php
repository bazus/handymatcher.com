<?php

$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

if(isset($_GET['leadId'])){
    $leadId = $_GET['leadId'];

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $leadData = $lead->getData();

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    $movingLeadData = $movingLead->getData();

}else{
    echo "<script>parent.swal.close();</script>";
    exit;
}
?>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>

<style>
    #footer{
        display: flex;
        justify-content: space-between;
        position: absolute;
        width: 100%;
        bottom: 0;
        padding: 10px;
    }
    .content{
        padding: 1rem;
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    fieldset.scheduler-border {
        border: 1px groove #fff !important;
        padding: 0 1rem 1rem 1rem !important;
        margin: 0 0 9px 0 !important;
        -webkit-box-shadow: 0px 0px 0px 0px #000;
        box-shadow: 0px 0px 0px 0px #fff;
        border-bottom: none !important;
        border-left: none !important;
        border-right: none !important;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 9px;
        border-bottom:none;
        color: #5b5b5b;
    }

    .content input.form-control-sm{
        margin-bottom: 3px;
    }

    .custom-file-label:after{
        display: none;
    }
    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }

    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }
    @media (max-width: 576px) {
        #footer{
            position: unset;
        }
    }
</style>

<div id="wrapper" style="visibility:hidden;">

    <div class="content">


        <form id="claimForm" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leads/lead/claims/createClaim.php" method="post">
            <input type="hidden" name="leadId" value="<?php echo $leadId; ?>" />
            <fieldset class="scheduler-border">
                <legend class="scheduler-border" style="top: 12px;position: absolute;right: 42px;background-color: #fff;">
                    <select class="form-control form-control-sm" onchange="changeClaimClientDetails(this.value)">
                        <option value="0">Same as 'From' details</option>
                        <option value="1">Same as 'To' details</option>
                    </select>
                </legend>
                <legend class="scheduler-border" style="width: 139px;">Client details</legend>
                <div class="control-group">
                    <div class="controls bootstrap-timepicker">
                        <input type="text" class="form-control form-control-sm" id="address" name="address" placeholder="Address" />
                        <input type="text" class="form-control form-control-sm" id="city" name="city" placeholder="City" />
                        <input type="text" class="form-control form-control-sm" id="state" name="state" placeholder="State" />
                        <input type="text" class="form-control form-control-sm" id="zip" name="zip" placeholder="Zip" />
                    </div>
                </div>
            </fieldset>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Client contact</legend>
                <div class="control-group">
                    <div class="controls bootstrap-timepicker">
                        <div style="display: flex">
                            <input style="margin-right: 3px;" type="text" name="firstName" class="form-control form-control-sm" value="<?php if(isset($leadData["firstname"])){echo $leadData["firstname"];} ?>" placeholder="First Name" />
                            <input type="text" name="lastName" class="form-control form-control-sm" value="<?php if(isset($leadData["lastname"])){echo $leadData["lastname"];} ?>" placeholder="Last Name" />
                        </div>
                        <input type="email" name="email" class="form-control form-control-sm" value="<?php if(isset($leadData["email"])){echo $leadData["email"];} ?>" placeholder="Email" />
                        <input type="tel" name="phone1" class="form-control form-control-sm" value="<?php if(isset($leadData["phone"])){echo $leadData["phone"];} ?>" placeholder="Phone 1" />
                        <input type="tel" name="phone2" class="form-control form-control-sm" value="<?php if(isset($leadData["phone2"])){echo $leadData["phone2"];} ?>" placeholder="Phone 2" />
                    </div>
                </div>
            </fieldset>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Claim details</legend>
                <div class="control-group">
                    <div class="controls bootstrap-timepicker">

                        <div class="input-group">
                            <div class="input-group-prepend" style="    height: calc(1.5em + 0.5rem + -1px);">
                                <span class="input-group-text" id="basic-addon1">Amount ($)</span>
                            </div>
                            <input type="number" name="amount" class="form-control form-control-sm" placeholder="0.00" />
                        </div>

                        <textarea class="form-control" name="details" id="details" placeholder="Details.." style="height: 150px;"></textarea>
                        <div class="input-group mt-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Attached Files</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" multiple id="inputGroupFile01" name="claimFiles[]" onchange="updateFile()">
                                <label class="custom-file-label" style="height: unset" for="inputGroupFile01" id="inputGroupFileLabel01">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
    <div id="footer">
        <input id="closeClaim" style="display: inline-table;" onclick="parent.swal.close();" class="swal-button swal-button--cancel" type="button" value="Close">
        <input id="createClaim" style="display: inline-table;margin-top: 0px;" onclick="$('#claimForm').submit();" class="ladda-button ladda-button-demo swal-button swal-button--save" type="button" value="Create Claim">
    </div>
</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>


<script>

    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });

    function changeClaimClientDetails(fromDetails) {
        if(fromDetails == "0"){
            $("#address").val("<?php if(isset($movingLeadData["fromAddress"])){echo $movingLeadData["fromAddress"];} ?>");
            $("#city").val("<?php if(isset($movingLeadData["fromCity"])){echo $movingLeadData["fromCity"];} ?>");
            $("#state").val("<?php if(isset($movingLeadData["fromState"])){echo $movingLeadData["fromState"];} ?>");
            $("#zip").val("<?php if(isset($movingLeadData["fromZip"])){echo $movingLeadData["fromZip"];} ?>");
        }else if(fromDetails == "1"){
            $("#address").val("<?php if(isset($movingLeadData["toAddress"])){echo $movingLeadData["toAddress"];} ?>");
            $("#city").val("<?php if(isset($movingLeadData["toCity"])){echo $movingLeadData["toCity"];} ?>");
            $("#state").val("<?php if(isset($movingLeadData["toState"])){echo $movingLeadData["toState"];} ?>");
            $("#zip").val("<?php if(isset($movingLeadData["toZip"])){echo $movingLeadData["toZip"];} ?>");
        }
    }

    $(document).ready(function() {
        changeClaimClientDetails("0");
        l = $('.ladda-button-demo').ladda();
    });

    $('#claimForm').ajaxForm({
    beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    });

    function beforeSubmit(data) {
        l.ladda( 'start' );
    }

    function afterSubmit(resp,status,other) {


        parent.leadController.lead.claims.getClaims();

        try{
            var data = JSON.parse(resp);
            if(data.status == true){
                // success
                parent.swal("Claim created","", "success");
            }else{
                // failed
                parent.swal("Oops",data.reason, "error");
            }
        }catch (e) {
            parent.swal("Oops","Claim was not created", "error");
        }
        l.ladda('stop');

    }


    function updateFile() {

        var input = document.getElementById('inputGroupFile01');
        var output = document.getElementById('inputGroupFileLabel01');
        output.innerHTML = "Choose files";

        var ul = document.createElement("ul");
        ul.setAttribute("style","margin-bottom:0px;padding-left: 0;list-style: none;");

        if(input.files.length > 4){
            swal("Maximum 4 files allowed","", "error");
            output.innerHTML = "Choose file";
            input.value = "";
            return;
        }else {
            output.innerHTML = "";
            for (var i = 0; i < input.files.length; ++i) {
                var li = document.createElement("li");
                li.innerHTML += input.files.item(i).name;

                ul.appendChild(li);
            }
            output.appendChild(ul);
        }
    }

</script>