<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadTags.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

$lead = new lead($_GET['leadId'],$bouncer["credentials"]["orgId"]);
$tags = $lead->getLeadTags();
$leadTags = new leadTags($bouncer["credentials"]["orgId"]);
$leadTagsData = $leadTags->getData();
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
    var leadId = "<?= $_GET['leadId']; ?>";
    var availableTags = <?= json_encode($leadTagsData) ?>;
</script>
<style>
    .modal-content .tagColor{
        cursor: pointer;
        margin-left: 5px;
        display: inline-block;
        background: white;
        border: 1px solid;
    }
    .modal-content .disabledTag{
        opacity: 0.5;
        pointer-events: none;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>

<div class="modal inmodal reminderModal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog" style="max-width: 450px;">

        <div class="modal-content animated fadeIn">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if(count($leadTagsData) > 0){ ?>
                        <h4>Select tags from the list below</h4>
                        <div id="tags-container">
                            <?php foreach ($leadTagsData as $tag){ ?>
                                <span onclick="toggleTag('<?= $tag['id'] ?>')" id="tag-<?= $tag['id'] ?>" class="label tagColor <?php if(in_array($tag["id"],$tags)){ ?>disabledTag<?php } ?>" style="color: <?= $tag['color'] ?>"><?= $tag['name'] ?></span>
                            <?php } ?>
                        </div>
                        <hr>
                        <h4>Selected lead tags</h4>
                        <div id="selected-tags">
                        </div>
                        <?php }else{ ?>
                            <h3>No Tags Available</h3>
                            <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                                <a onclick="openLeadsSettings()" data-dismiss="modal">Go to leads settings and add tags</a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin-top:0;">
                <?php if(count($leadTagsData) == 0 || $leadTagsData == false){ ?>
                <button class="btn btn-primary disabled" data-dismiss="modal">Save</button>
                <?php }else{ ?>
                    <button class="btn btn-primary" onclick="saveLeadTags()">Save</button>
                <?php } ?>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
                <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                    <a class="pull-left" style="font-size: 10px;margin-top: 8px;" onclick="openLeadsSettings()">Manage Tags</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    var selectedTags = [];
    function toggleTag(id){
        if (selectedTags.includes(id)){
            for( var i = 0; i < selectedTags.length; i++){
                if ( selectedTags[i] === id) {
                    selectedTags.splice(i, 1);
                }
            }
        } else{
            selectedTags.push(id);
        }
        $("#tag-"+id).toggleClass("disabledTag");
        selectTags();
    }
    function selectTags() {
        var container = document.getElementById("selected-tags");
        container.innerHTML = "";

        for (var i = 0;i<selectedTags.length;i++){
            var id = selectedTags[i];
            var span = document.createElement("span");
            span.setAttribute("onclick","toggleTag('"+selectedTags[i]+"')");
            span.classList.add("label");
            span.classList.add("tagColor");
            var currentTag = availableTags.find(x => x.id === id);

            span.style.color = currentTag.color;

            var spanText = document.createTextNode(availableTags.find(x => x.id === id).name);

            span.appendChild(spanText);
            container.appendChild(span);
        }
    }
    function saveLeadTags() {
        var strUrl = BASE_URL + '/console/actions/leads/lead/updateLeadTags.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data: {
                leadId: leadId,
                tags:selectedTags
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data == true){
                    toastr.success("Lead tags updated","Updated");
                    leadController.page.showTabById('1');
                    leadController.tags.reload();
                    $("#myCustomModal").modal('hide');
                }
            } catch (e) {
                toastr.error("lead tags updating Failed");
            }
        });
    }

    function openLeadsSettings(){
        window.open(BASE_URL+ "/console/categories/leads/leads.php?showLeadSettings=true");
    }

    <?php if ($tags){ ?>
        <?php foreach ($tags as $tag){ ?>
            selectedTags.push('<?= $tag ?>');
        <?php } ?>
    selectTags();
    <?php } ?>
</script>