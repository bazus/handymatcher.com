<?php

$pagePermissions = array(false,array(1),true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

if (isset($_GET['leadId'])) {
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $leadId = $_GET['leadId'];
    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $leadData = $lead->getData();


    if ($bouncer['isUserAnAdmin'] == true){
        if ($lead->isLeadFromOrg() == false){
            //header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
            exit;
        }
    }else{
        if ($lead->isLeadFromOrg(true,$bouncer["credentials"]["userId"]) == false){
            //header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
            exit;
        }
    }

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $phoneNumbers = $organizationTwilio->getPhoneNumbers();

}else{
    //header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <title>Chat Widget</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>

    <link rel="stylesheet" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/chat-widget/style.css">

    <style>
        .select-form-control{
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;

            background-color: #FFFFFF;
            background-image: none;
            border: 1px solid #e5e6e7;
            color: inherit;
            display: block;
            padding: 4px 9px;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        }

        select:disabled,textarea:disabled{
            background-color: #e6e6e68c;
        }
    </style>
</head>

<body>

<div class="container clearfix">
    <div class="chat">
        <div class="chat-header clearfix" style="background-color: #f1f5f8;top: 0px;position: fixed;width: 260px;z-index: 9999999;">
            <div class="chat-about">
                <div class="chat-with" style="display: inline-table;">Chat with</div>
                <div style="display: inline-table;">
                    <select class="select-form-control" id="sms-selected-number" onchange="getConversation();markRead();">
                    </select>
                </div>
                <div class="chat-num-messages" id="total-sms-msgs"></div>
            </div>
        </div> <!-- end chat-header -->

        <div class="chat-history" id="conversationDiv">
            <ul id="conversationUL">

                <!--
                <li class="clearfix">
                    <div class="message-data align-right">
                        <span class="message-data-time" >10:10 AM, Today</span> &nbsp; &nbsp;
                        <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>

                    </div>
                    <div class="message other-message float-right">
                        Hi Vincent, how are you? How is the project coming along?
                    </div>
                </li>

                <li>
                    <div class="message-data">
                        <span class="message-data-name"><i class="fa fa-circle online"></i></span>
                        <span class="message-data-time">10:12 AM, Today</span>
                    </div>
                    <div class="message my-message">
                        Are we meeting today? Project has been already finished and I have results to show you.
                    </div>
                </li>

                <li class="clearfix">
                    <div class="message-data align-right">
                        <span class="message-data-time" >10:14 AM, Today</span> &nbsp; &nbsp;
                        <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>

                    </div>
                    <div class="message other-message float-right">
                        Well I am not sure. The rest of the team is not here yet. Maybe in an hour or so? Have you faced any problems at the last phase of the project?
                    </div>
                </li>

                <li>
                    <div class="message-data">
                        <span class="message-data-name"><i class="fa fa-circle online"></i></span>
                        <span class="message-data-time">10:20 AM, Today</span>
                    </div>
                    <div class="message my-message">
                        Actually everything was fine. I'm very excited to show this to our team.
                    </div>
                </li>
-->
            </ul>
        </div> <!-- end chat-history -->

        <div class="chat-message clearfix" style="border-top: 2px solid white;width: 260px;background-color: #f1f5f8;bottom: 0px; position: fixed">
            <select class="select-form-control" id="send-sms-from" style="width:100%;margin-bottom: 7px;" <?php if(count($phoneNumbers) == 0){echo "disabled";} ?> onchange="getConversation();markRead();">
                <?php
                    foreach($phoneNumbers as $phoneNumber){
                        $prettyNumber = $organizationTwilio->prettifyPhone($phoneNumber["number"]);
                        echo '<option value="'.$phoneNumber["id"].'">From '.$prettyNumber.'</option>';
                    }
                ?>
            </select>
            <textarea name="message-to-send" id="message-to-send" placeholder ="Type your message" rows="3" <?php if(count($phoneNumbers) == 0){echo "disabled";} ?>></textarea>

            <?php if(count($phoneNumbers) == 0){ ?>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/mailSettings.php" target="_blank" style="color: #337ab7;text-decoration: none;float: left;">Register a phone number</a>
            <?php
                }
            ?>
            <button id="sendSmsMsg" onclick="sendMSG()">Send</button>

        </div> <!-- end chat-message -->

    </div> <!-- end chat -->
</div> <!-- end container -->

<script id="message-template" type="text/x-handlebars-template">
    <li class="clearfix">
        <div class="message-data align-right">
            <span class="message-data-time" >{{time}}, Today</span> &nbsp; &nbsp;
            <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>
        </div>
        <div class="message other-message float-right">
            {{messageOutput}}
        </div>
    </li>
</script>

<script id="message-response-template" type="text/x-handlebars-template">
    <li>
        <div class="message-data">
            <span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
            <span class="message-data-time">{{time}}, Today</span>
        </div>
        <div class="message my-message">
            {{response}}
        </div>
    </li>
</script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.0/handlebars.min.js'></script>

<!--
<script  src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/chat-widget/index.js"></script>
-->

<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";

    function getLeadNumber(){

        var strUrl = BASE_URL+'/console/actions/system/twilio/getLeadNumbers.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            data:{
                leadId:"<?php echo $leadId; ?>"
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            data = JSON.parse(data);

            var smsSelectedNumber = document.getElementById('sms-selected-number');
            smsSelectedNumber.innerHTML = "";

            for(var i = 0;i<data.length;i++){

                var option = document.createElement("option");
                option.innerHTML = data[i].prettyNumber;
                option.value = data[i].number;

                smsSelectedNumber.appendChild(option);
            }

            getConversation();
            getTotalUnread();

        });
    }

    function getTotalUnread(){

        var strUrl = BASE_URL+'/console/actions/system/twilio/getTotalUnread.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            data:{
                leadId:"<?php echo $leadId; ?>"
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            data = JSON.parse(data);

            parent.document.getElementById('totalUnreadSMS').innerHTML = "";

            if(data == 0){
                parent.document.getElementById('open-sms-side').classList.remove("blinker");
                parent.document.getElementById('open-sms-side').classList.remove("unreadSMSBlink");
            }else{
                parent.document.getElementById('open-sms-side').classList.add("blinker");
                parent.document.getElementById('open-sms-side').classList.add("unreadSMSBlink");
            }

        });
    }

    function getConversation(){

        var toNumber = document.getElementById('sms-selected-number').value;
        var fromPhoneNumberId = document.getElementById('send-sms-from').value;


        if (toNumber !== "" && toNumber !== "+" && toNumber !== "+1") {
            $("#message-to-send").removeAttr('disabled');
            $("#send-sms-from").removeAttr('disabled');
            $("#sendSmsMsg").removeAttr('disabled');
            var strUrl = BASE_URL + '/console/actions/system/twilio/getLeadSMS.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                data: {
                    fromPhoneNumberId: fromPhoneNumberId,
                    toNumber: toNumber
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);

                var conversationUL = document.getElementById('conversationUL');
                conversationUL.innerHTML = "";

                if (data.length == 0) {
                    document.getElementById('total-sms-msgs').innerHTML = "No Messages";
                } else {
                    if (data.length == 1) {
                        document.getElementById('total-sms-msgs').innerHTML = "1 Message";
                    } else {
                        document.getElementById('total-sms-msgs').innerHTML = data.length + " Messages";
                    }
                }

                for (var i = 0; i < data.length; i++) {
                    createMsg(data[i]);
                }

            });
        }else{
            document.getElementById('conversationUL').innerHTML = "";
            document.getElementById('total-sms-msgs').innerHTML = "Phone Invalid";
            document.getElementById("message-to-send").setAttribute('disabled',true);
            document.getElementById("send-sms-from").setAttribute('disabled',true);
            document.getElementById("sendSmsMsg").setAttribute('disabled',true);
        }
    }

    function createMsg(data){

        var liClassName = "clearfix";
        var divClassName = "align-right";
        var div2ClassName = "float-right";
        var msgClassName = "other-message";
        if(data.isOutGoing == "1"){
            liClassName = "";
            divClassName = "";
            div2ClassName = "";
            msgClassName = "my-message";
        }

        var conversationUL = document.getElementById('conversationUL');

        var li = document.createElement("li");
        li.className = liClassName;




        // ========== (START) CREATE MSG HEAD ==========
        var div = document.createElement("div");
        div.className = "message-data "+divClassName;

        var span = document.createElement("span");
        span.className = "message-data-time";
        span.innerHTML = data.sentAt;

        div.appendChild(span);

        if(data.status != null && data.status != undefined){
            var span = document.createElement("span");
            span.className = "message-data-status";
            span.innerHTML = data.status[0].toUpperCase() + data.status.slice(1);

            if(data.status == "sent" || data.status == "delivered"){
                span.setAttribute("style","color: #4CAF50");
            }else if(data.status == "undelivered"){
                span.setAttribute("style","color: #F44336");
            }

            div.appendChild(span);
        }

        if(data.isOutGoing == "1") {
            var span = document.createElement("span");
            span.className = "message-data-name";
            span.innerHTML = data.fullName;

            div.appendChild(span);
        }

        li.appendChild(div);
        // ========== (END) CREATE MSG HEAD ==========




        // ========== (START) CREATE MSG BODY ==========
        var div = document.createElement("div");
        div.className = "message "+msgClassName+" "+div2ClassName;
        div.innerHTML = data.msg.replace(/\n/g,'<br>');

        li.appendChild(div);
        // ========== (END) CREATE MSG BODY ==========


        conversationUL.appendChild(li);

        fixSMSChatPosition();
    }

    function fixSMSChatPosition(){
        window.scrollTo(0,document.body.scrollHeight);
    }

    function sendMSG(){
        var msg = document.getElementById('message-to-send').value;
        var toNumber = document.getElementById('sms-selected-number').value;
        var fromPhoneNumberId = document.getElementById('send-sms-from').value;

        var strUrl = BASE_URL+'/console/actions/system/twilio/sendSMSToLead.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                leadId:"<?php echo $leadId; ?>",
                fromPhoneNumberId:fromPhoneNumberId,
                toNumber:toNumber,
                msg:msg
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{
                data = JSON.parse(data);
                if(data.status == true && data.resp == true){

                    document.getElementById('message-to-send').value = "";

                    var msgData = [];
                    msgData["isOutGoing"] = "1";
                    msgData["fullName"] = "Me";
                    msgData["sentAt"] = "Now";
                    msgData["msg"] = msg;

                    createMsg(msgData);
                }else{
                    if (data.resp == "balance"){
                        parent.limitReached("balance");
                    }
                    if (data.resp == 400 || data.resp == "400"){
                        parent.toastr.error("Number '"+toNumber+"' is disconnected or invalid","Failed");
                    }
                }
            }catch(e){
                parent.toastr.error("General error, please try again later","Failed");
            }


        });
    }

    function markRead(){
        var toNumber = document.getElementById('sms-selected-number').value;
        var fromPhoneNumberId = document.getElementById('send-sms-from').value;

        var strUrl = BASE_URL+'/console/actions/system/twilio/markMSGSasRead.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                fromPhoneNumberId:fromPhoneNumberId,
                toNumber:toNumber
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            getTotalUnread();

        });
    }

    getLeadNumber();
</script>
</body>

</html>
