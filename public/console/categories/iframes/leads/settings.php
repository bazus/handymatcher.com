<?php

$isCalledFromModal = true;

$pagePermissions = array(true,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

$show = false;
if(isset($_GET['show'])){
    $show = $_GET['show'];
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.css" rel="stylesheet">
<style>
    .selectedUserRow{
        background-color: #f5f5f5;
    }
    #usersWithRules table th,#usersWithRules table td{
        text-align: center;
        vertical-align: middle;
    }
    .collapse-link{
        cursor: pointer;
    }
    .copyModal{
        min-width: 68%;
    }
</style>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Leads Settings</h4>
            </div>
            <div class="modal-body" style="padding: 0px;padding-bottom: 0px;background-color: #fff !important;">

                <div class="row" style="margin: 0px !important;">

                    <div class="ibox" style="margin-bottom: 0px;">
                        <div class="ibox-title collapse-link" style="border: none;">
                            <h4 style="float: left;margin-top: 0px;">Lead rotation rules</h4>
                            <div class="ibox-tools">
                                <a>
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                                <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
                                <a onclick="showRotationInfo(event)">
                                    <i class="fa fa-question"></i>
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="ibox-content no-padding" style="background-color: #f3f3f380;display: none;">
                            <div style="padding: 11px !important;">

                                <div id="addARuleForUserDiv" style="display: none;">
                                    <div class="input-group" style="width: 100%;display: flex;">
                                        <select class="form-control" id="userSalesId" style="display: inline-table;" onchange="selectedUser(this)">
                                        <?php
                                        /*
                                        foreach($usersWithoutRule as $userId=>$userData){
                                        ?>
                                            <option value="<?php echo $userId; ?>"><?php echo $userData["fullName"]; ?></option>
                                        <?php } */?>
                                        </select>
                                        <span style="display: flex;">
                                            <button type="button" class="btn btn-primary" id="addUserRule" onclick="addUserRule()" disabled> Add</button>
                                        </span>
                                    </div>

                                    <div style="width: 100%;text-align: center"><a onclick="addRule_back()">Back</a></div>
                                </div>

                                <div id="usersWithRules" style="margin-top: 9px;">

                                    <table class="table table-bordered table-hover">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>User</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                        </thead>
                                        <tbody id="usersWithRuleTable">
                                                <?php
                                                /*
                                                    if(count($usersWithRule) > 0){
                                                        foreach($usersWithRule as $userId=>$userData){
                                                            ?>
                                                <tr onclick="getRotationRulesOfUser('<?php echo $userId; ?>')" class="white-bg">
                                                            <td><?= $userData["fullName"]; ?></td>
                                                            <td><?= $userData["fullName"]; ?></td>
                                                            <td><?= $userData["fullName"]; ?></td>
                                                </tr>
                                                            <?php
                                                        }
                                                    }else{
                                                        ?>
                                                        <tr>
                                                            <td colspan="3" style="background-color: #fff; text-align: center;">No Users</td>
                                                        </tr>
                                                        <?php
                                                    }
                                                */
                                                ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" style="text-align: center;" class="white-bg"><a onclick="addRule()">Add Rule</a></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div id="salesRule" style="display: none;">
                                    <hr>
                                    <label>Assign leads from following providers</label>
                                    <div class="row" id="acceptProvidersDiv">

                                    </div>
                                    <br>
                                    <hr>
                                    <label style="margin-bottom: 0px;">Assign leads according to the following schedule</label>
                                    <label style="color: #bbbbbb;font-size: 11px;display: block;">If no schedule is set, user will be able to receive leads all week</label>
                                    <label style="color: #bbbbbb;font-size: 11px;display: block;">All times are by EST (Eastern Standard Time)</label>
                                    <div id="acceptScheduleDiv"></div>
                                    <br>
                                    <div style="text-align: center">
                                        <button class="btn btn-primary" onclick="addRowToSchedule()">
                                            <i class="fa fa-plus"></i> Add
                                        </button>
                                    </div>
                                    <hr>
                                    <div style="height: 40px; width: 100%;">
                                        <span style="color: #ff4462;display: none;" class="pull-left" id="dataNotOk">Please fill all data</span>
                                        <button class="btn btn-primary pull-right" onclick="saveRotationRule()" id="saveBtn">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin: 0px;border-width: 1px;">
                    <div class="ibox" style="margin-bottom: 0px;">
                        <div class="ibox-title collapse-link" style="border: none;">
                            <h4 style="float: left;margin-top: 0px;">Lead Tags</h4>
                            <div class="ibox-tools">
                                <a>
                                    <i class="fa fa-chevron-<?php if($show != "tags"){ ?>down<?php }else{ ?>up<?php } ?>"></i>
                                </a>
                                <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
                                <a onclick="showLeadTagsInfo(event)">
                                    <i class="fa fa-question"></i>
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="ibox-content no-padding" style="background-color: #f3f3f380; <?php if($show != "tags"){ ?>display: none;<?php } ?> width: 100%;">
                            <div style="padding: 11px !important;">
                                <div id="leadTagsTable" class="table-responsive">
                                    <table class="table table-bordered" style="background-color: #fff;">
                                        <thead>
                                            <th style="width: 1%;">Color</th>
                                            <th>Tag Name</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody id="leadTagsBody">
                                            <tr><td colspan="3" class="text-center">No Tags Available</td></tr>
                                        </tbody>
                                        <tfoot>
                                            <tr><td colspan="3" class="text-center"><a onclick="toggleTab(true);">Add Tag</a></td></tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div id="addNewTagContainer" style="display: none;">
                                    <div class="form-group">
                                        <label for="newTagName">Create New Tag</label>
                                        <input type="text" id="newTagName" class="form-control" placeholder="Tag Name">
                                    </div>
                                    <label for="tagColor">Choose Tag Color</label>
                                    <div class="form-group text-center">
                                        <div id="tagColor"></div>
                                    </div>
                                    <div class="form-group" style="height: 25px;">
                                        <div style="position: absolute;right: 15px;">
                                            <button style="margin-right: 5px;" class="btn btn-default btn-sm" onclick="toggleTab(false);">Back</button>
                                            <button class="btn btn-primary btn-sm" onclick="sendTag();">Create Tag</button>
                                        </div>
                                    </div>
                                </div>
                                <div id="editTagContainer" style="display: none;">
                                    <input type="hidden" id="editId">
                                    <div class="form-group">
                                        <label for="editTagName">Tag Name</label>
                                        <input type="text" id="editTagName" class="form-control" placeholder="Tag Name">
                                    </div>
                                    <label for="tagEditColor">Choose Tag Color</label>
                                    <div class="form-group text-center">
                                        <div id="tagEditColor"></div>
                                    </div>
                                    <div class="form-group" style="height: 25px;">
                                        <div style="position: absolute;right: 15px;">
                                            <button style="margin-right: 5px;" class="btn btn-default btn-sm" onclick="toggleEdit(false);">Back</button>
                                            <button class="btn btn-primary btn-sm" onclick="updateTag();">Update Tag</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin: 0px;border-width: 1px;">
                    <div class="ibox" style="margin-bottom: 0px;">
                        <div class="ibox-title collapse-link" style="border: none;">
                            <h4 style="float: left;margin-top: 0px;">Lead Iframes</h4>
                            <div class="ibox-tools">
                                <a>
                                    <i class="fa fa-chevron-<?php if($show != "iframes"){ ?>down<?php }else{ ?>up<?php } ?>"></i>
                                </a>
                                <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
                                    <a onclick="showLeadIframeInfo(event)">
                                        <i class="fa fa-question"></i>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="ibox-content no-padding" style="background-color: #f3f3f380; <?php if($show != "tags"){ ?>display: none;<?php } ?> width: 100%;">
                            <div style="padding: 11px !important;">
                                <div class="table-responsive" id="myIframes">
                                    <table class="table table-bordered" style="background-color: #fff;">
                                        <thead>
                                            <th style="text-align: center;">Name</th>
                                            <th style="text-align: center;width: 92px;">Leads Today</th>
                                            <th style="text-align: center;width: 105px;">Banner Image</th>
                                            <th style="text-align: center;width: 160px;">Actions</th>
                                        </thead>
                                        <tbody id="iframeContainer">
                                            <tr><td class="text-center" colspan="4">No iframes available</td></tr>
                                        </tbody>
                                        <tfoot>
                                        <tr><td colspan="4" class="text-center"><a onclick="showAddIframe(true)">Add iframe</a></td></tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div id="iframeInsert" style="display:none;">
                                    <div class="form-group">
                                        <label for="createIframeName">Create iframe</label>
                                        <input type="text" placeholder="iframe name" class="form-control" id="createIframeName">
                                    </div>
                                    <div class="form-group" style="height: 20px;">
                                        <span class="pull-right">
                                            <button class="btn btn-sm btn-default" style="margin-right: 5px;" onclick="showAddIframe(false)">Back</button>
                                            <button class="btn btn-sm btn-primary" onclick="createIframe()">Create iframe</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Custom and plugin javascript -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/color-picker/color-picker.min.js"></script>
<script>
    // Lead Iframes
    function getIframes(){
        var strUrl = BASE_URL+'/console/actions/leads/leadIframe/getIframesData.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data){
                    var container = document.getElementById("iframeContainer");
                    container.innerHTML = "";
                    for (var i = 0;i<data.length;i++){
                        setIframe(data[i],container);
                    }
                }
            }catch (e) {

            }
        });
    }
    function setIframe(data,container) {

        var tr = document.createElement("tr");

        var td = document.createElement("td");
        td.classList.add("text-center");
        var tdText = document.createTextNode(data.name);

        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        td.classList.add("text-center");
        var tdText = document.createTextNode(data.LeadsRecevied);

        td.appendChild(tdText);
        tr.appendChild(td);

        if (data.bannerUrl != null){
            var td = document.createElement("td");
            td.classList.add("text-center");
            var tdButton = document.createElement("button");
            tdButton.title = "Preview iframe banner";
            tdButton.style.marginRight = "3px";
            tdButton.setAttribute("onclick", "showIframeImage('"+data.bannerUrl+"')");
            tdButton.classList.add("btn");
            tdButton.classList.add("btn-xs");
            tdButton.classList.add("btn-primary");
            var tdButtonI = document.createElement("i");
            tdButtonI.classList.add("fa");
            tdButtonI.classList.add("fa-eye");
            tdButton.appendChild(tdButtonI);

            td.appendChild(tdButton);

            var tdButton = document.createElement("button");
            tdButton.title = "Clear iframe banner";
            tdButton.style.marginRight = "3px";
            tdButton.setAttribute("onclick", "clearIframeImage('"+data.id+"')");
            tdButton.classList.add("btn");
            tdButton.classList.add("btn-xs");
            tdButton.classList.add("btn-warning");
            var tdButtonI = document.createElement("i");
            tdButtonI.classList.add("fa");
            tdButtonI.classList.add("fa-trash");
            tdButton.appendChild(tdButtonI);

            td.appendChild(tdButton);
            tr.appendChild(td);
        }else {
            var td = document.createElement("td");
            td.classList.add("text-center");
            var tdButton = document.createElement("button");
            tdButton.title = "Upload a new iframe banner";
            tdButton.setAttribute("onclick", "$('#" + data.id + "-hiddenFile').click()");
            tdButton.id = data.id + "-uploadFile";
            tdButton.classList.add("btn");
            tdButton.classList.add("btn-xs");
            tdButton.classList.add("btn-default");
            var tdButtonI = document.createElement("i");
            tdButtonI.classList.add("fa");
            tdButtonI.classList.add("fa-upload");
            tdButton.appendChild(tdButtonI);

            td.appendChild(tdButton);
            var hiddenFile = document.createElement("input");
            hiddenFile.setAttribute("onchange", "prepareUpload");
            hiddenFile.id = data.id + "-hiddenFile";
            hiddenFile.type = "file";
            hiddenFile.style.display = "none";
            td.appendChild(hiddenFile);

            tr.appendChild(td);
        }

        var td = document.createElement("td");
        td.classList.add("text-center");
        var tdButton = document.createElement("button");
        tdButton.style.marginRight = "5px";
        tdButton.setAttribute("onclick","copyLink('"+data.uniqueKey+"')");
        tdButton.classList.add("btn");
        tdButton.classList.add("btn-xs");
        tdButton.classList.add("btn-success");
        var tdButtonText = document.createTextNode("Integrations");
        tdButton.appendChild(tdButtonText);

        td.appendChild(tdButton);

        var tdButton = document.createElement("button");
        tdButton.title = "Delete iframe";
        tdButton.setAttribute("onclick","deleteIframe("+data.id+")");
        tdButton.classList.add("btn");
        tdButton.classList.add("btn-xs");
        tdButton.classList.add("btn-danger");
        var tdButtonI = document.createElement("i");
        tdButtonI.classList.add("fa");
        tdButtonI.classList.add("fa-trash-o");
        tdButton.appendChild(tdButtonI);

        td.appendChild(tdButton);
        tr.appendChild(td);

        container.appendChild(tr);

        $("#"+data.id+"-hiddenFile").on('change', prepareUpload);
    }

    function copyLink(key) {
        var token = "<?= $bouncer['organizationData']['secretToken'] ?>";
        var str = "<?php echo $_SERVER["YMQ_URL"]; ?>/estimate.php?key="+key+"&ST="+token;

        try {

            var swalContent = document.createElement("div");

            var swalLinkHeader = document.createTextNode("Direct link");
            swalContent.appendChild(swalLinkHeader);

            var swalLinkContainer = document.createElement("pre");
            swalLinkContainer.style.marginBottom = "25px";
            swalLinkContainer.style.marginTop = "15px";

            var swalLink = document.createElement("a");
            swalLink.setAttribute("onclick","copyText('"+str+"')");

            var swalLinkText = document.createTextNode(str);

            swalLink.appendChild(swalLinkText);

            swalLinkContainer.appendChild(swalLink);
            swalContent.appendChild(swalLinkContainer);


            var swalLinkHeader = document.createTextNode("Iframe Code");
            swalContent.appendChild(swalLinkHeader);

            var swalLinkContainer = document.createElement("pre");
            swalLinkContainer.style.marginTop = "15px";

            var swalLink = document.createElement("a");
            swalLink.setAttribute("onclick","copyText('<iframe style=\"border: none;width: 750px; height: 750px;\" src=\""+str+"\"></iframe>')");
            var swalLinkText = document.createTextNode('<iframe style="border: none;width: 750px; height: 750px;" src=\"'+str+'"\></iframe>');

            swalLink.appendChild(swalLinkText);

            swalLinkContainer.appendChild(swalLink);
            swalContent.appendChild(swalLinkContainer);

            var swalP = document.createElement("p");
            swalP.style.fontSize = "14px";
            swalP.style.textAlign = "left";
            swalP.classList.add("text-muted");
            swalP.innerHTML = "<span style='font-weight: bold'>Iframe Size:</span> 750x750 (pixels)";
            swalContent.appendChild(swalP);

            swal({
                title:"Click to copy",
                content: swalContent,
                className: "CopyModal"
            });
            // toastr.success("Text copied to clipboard", "Copied");
        }catch (e) {
            window.open(str);
        }
    }

    function copyText(str) {
        var container = document.getElementById("myCustomModal");
        var el = document.createElement('textarea');
        el.value = str;
        container.appendChild(el);
        el.select();
        document.execCommand('copy');
        container.removeChild(el);

        toastr.success("Copied","Link Copied");
    }

    function clearIframeImage(id) {
        var strUrl = BASE_URL+'/console/actions/leads/leadIframe/clearIframeImage.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                iframeId:id
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data != true){
                    toastr.error("Changing iframe banner failed","Failed");
                }
            }catch (e) {
                toastr.error("Changing iframe banner failed","Failed");
            }
            getIframes();
        });
    }
    function toggleIframe(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this banner",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                var strUrl = BASE_URL+'/console/actions/leads/leadIframe/clearIframeImage.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method:"POST",
                    data:{
                        id:id
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true){
                            getIframes();
                        }else{
                            toastr.error("Changing iframe status failed","Failed");
                        }
                    }catch (e) {
                        toastr.error("Changing iframe status failed","Failed");
                    }
                });
            }
        });
    }
    function deleteIframe(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this iframe",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                var strUrl = BASE_URL + '/console/actions/leads/leadIframe/deleteIframe.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        id: id
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true) {
                            getIframes();
                        } else {
                            toastr.error("Deleting iframe failed", "Failed");
                        }
                    } catch (e) {
                        toastr.error("Deleting iframe failed", "Failed");
                    }
                });
            }
        });
    }
    function showAddIframe(type) {
        if (type == true){
            $("#myIframes").hide();
            $("#iframeInsert").show();
        }else{
            $("#myIframes").show();
            $("#iframeInsert").hide();
        }
    }
    function createIframe() {
        var name = document.getElementById("createIframeName").value;
        var strUrl = BASE_URL+'/console/actions/leads/leadIframe/createIframe.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                name: name
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data == true){
                    document.getElementById("createIframeName").value = "";
                    showAddIframe(false);
                    getIframes();
                }else{
                    toastr.error("Creating iframe failed","Failed");
                }
            }catch (e) {
                toastr.error("Creating iframe failed","Failed");
            }
        });
    }
    function showLeadIframeInfo(e){
        e.stopPropagation();
        swal({
            text: "Here you can create a custom estimate form to place on your website. This iFrame estimate form will add any organic leads you generate on your own site directly to your Network Leads software.",
            buttons: {
                skip:{
                    text:"Got It"
                }
            }
        });
    }
    function showIframeImage(url) {
        var content = document.createElement("div");

        var img = document.createElement("img");
        img.src = url;
        img.classList.add("img-thumbnail");

        content.appendChild(img);

        swal({content:content,
            buttons: "Close",
        });

    }
    var files;

    function prepareUpload(event) {
        var id = event.currentTarget.id;
        id = id.replace("-hiddenFile","");
        files = event.target.files;
        uploadFiles(id);
    }

    function uploadFiles(id){
        $("#"+id+"-uploadFile").attr("disabled",true);

        var data = new FormData();
        $.each(files, function(key, value)
        {
            data.append(key, value);
        });

        data.append('iframeId', id);

        var strUrl = BASE_URL+'/console/actions/leads/leadIframe/uploadFile.php';

        $.ajax({
            url: strUrl,
            type: "POST",
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR){

            },
            async: true
        }).done(function(data){
            try {
                if (data.success == true){
                    getIframes();
                }else{
                    toastr.error(data.error,"Failed");
                }
            }catch (e) {
                toastr.error("Uploading File failed, please try again later","Failed");
            }
            $("#"+id+"-uploadBtn").attr("disabled",false);
            getIframes();
        });
    }


    getIframes();
</script>
<script>
    var l = $( '.ladda-button' ).ladda();

    var selectedUserId = null;

    function getUsers(){
        var userIdToAdd = document.getElementById('userSalesId').value;

        var strUrl = BASE_URL+'/console/actions/leads/leadSalesRotation/getUsers.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{

            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{
                data = JSON.parse(data);

                if(data == false){
                    swal("Oops", "Please try again later", "error");
                }else{

                    // ===== INITIALIZE USERS WITHOUT RULE =====
                    var userSalesId = document.getElementById('userSalesId');
                    userSalesId.innerHTML = "";

                    var option = document.createElement("option");
                    option.value = "";
                    option.innerHTML = "Choose Salesperson";

                    userSalesId.appendChild(option);

                    for(var i = 0;i<data.usersWithoutRule.length;i++){
                        var userWithoutRule = data.usersWithoutRule[i];

                        var option = document.createElement("option");
                        option.value = userWithoutRule.id;
                        option.innerHTML = userWithoutRule.fullName;

                        userSalesId.appendChild(option);

                    }
                    // ===== INITIALIZE USERS WITHOUT RULE =====

                    // ===== INITIALIZE USERS WITH RULE TABLE =====
                    var usersWithRuleTable = document.getElementById('usersWithRuleTable');
                    usersWithRuleTable.innerHTML = "";

                    if(data.usersWithRule.length == 0){
                        // Empty table
                        var tr = document.createElement("tr");

                        var td = document.createElement("td");
                        td.setAttribute("colspan","3");
                        td.setAttribute("style","background-color: #fff; text-align: center;");
                        td.innerHTML = "No Users";

                        tr.appendChild(td);
                        usersWithRuleTable.appendChild(tr);

                    }else{
                        for(var i = 0;i<data.usersWithRule.length;i++) {
                            var userWithRule = data.usersWithRule[i];

                            var tr = document.createElement("tr");
                            tr.setAttribute("onclick","getRotationRulesOfUser('"+userWithRule.id+"')");
                            tr.className = "white-bg userRow";
                            tr.id = "userRow"+userWithRule.id;
                            tr.name = "userRow";
                            tr.setAttribute("style","cursor:pointer");

                            var td = document.createElement("td");
                            td.innerHTML = userWithRule.fullName;
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            td.id = "userStatus-"+userWithRule.id;
                            if (userWithRule.isRuleActive == 1 || userWithRule.isRuleActive == "1"){
                                td.classList.add("text-success");
                                td.innerHTML = "Active";
                            }else{
                                td.classList.add("text-warning");
                                td.innerHTML = "Inactive";
                            }
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            var button = document.createElement("button");
                            button.classList.add("btn");
                            button.classList.add("btn-white");

                            if (userWithRule.isRuleActive == 1 || userWithRule.isRuleActive == "1"){
                                var buttonText = document.createTextNode("Pause");
                            }else{
                                var buttonText = document.createTextNode("Activate");
                            }

                            button.setAttribute("onclick","toggleUserRole(event,"+userWithRule.id+")");
                            button.id = "toggleUser-"+userWithRule.id;
                            button.appendChild(buttonText);
                            td.appendChild(button);
                            tr.appendChild(td);

                            usersWithRuleTable.appendChild(tr);

                        }
                    }

                    // ===== INITIALIZE USERS WITH RULE TABLE =====


                }

            }catch(e){
                swal("Oops", "Please try again later", "error");
            }
        });
    }

    function addUserRule(){
        var userIdToAdd = document.getElementById('userSalesId').value;

        var strUrl = BASE_URL+'/console/actions/leads/leadSalesRotation/createBlankRuleForUser.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                userId:userIdToAdd
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{

                data = JSON.parse(data);
                if(data == true){
                    getUsers();
                    addRule_back();
                    getRotationRulesOfUser(userIdToAdd);
                }else{
                    swal("Oops", "Please try again later", "error");
                }

            }catch(e){
                swal("Oops", "Please try again later", "error");
            }
        });
    }

    function getRotationRulesOfUser(userId){

        selectedUserId = userId;

        clearSelectedRows();

        if(document.getElementById('userRow'+userId)) {
            document.getElementById('userRow' + userId).classList.add("selectedUserRow");
        }

        var acceptProvidersDiv = document.getElementById('acceptProvidersDiv');
        acceptProvidersDiv.innerHTML = "";

        var acceptScheduleDiv = document.getElementById('acceptScheduleDiv');
        acceptScheduleDiv.innerHTML = "";

        if(userId == ""){
            document.getElementById('salesRule').style.display = "none";
            return;
        }

        var strUrl = BASE_URL+'/console/actions/leads/leadSalesRotation/getData.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "GET",
            data:{
                userId:userId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            document.getElementById('salesRule').style.display = "";
            try{
                data = JSON.parse(data);

                if(data != false){
                    displayProviders(providers,data.acceptProviders);
                    for(var i = 0;i<data.acceptSchedule.length;i++){
                        addRowToSchedule(data.acceptSchedule[i]);
                    }
                }else {
                    displayProviders(providers, []);
                }
            }catch(e){
                swal("Oops", "Please try again later", "error");
            }
        });
    }

    var providers;
    function getProviders(){

        var strUrl = BASE_URL+'/console/actions/leads/getProviders.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "GET",
            data:{
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{
                providers = JSON.parse(data);
            }catch(e){
                swal("Oops", "Please try again later", "error");
            }
        });
    }

    var countScheduleRows = 0;
    function addRowToSchedule(scheduleData){

        var preset_day = "";
        var preset_S = "";
        var preset_E = "";
        var preset_isAllDay = false;

        if(typeof scheduleData !== "undefined"){
            preset_day = scheduleData.day;
            preset_S = scheduleData.S;
            preset_E = scheduleData.E;
            preset_isAllDay = scheduleData.isAllDay;
        }

        countScheduleRows++;

        var acceptScheduleDiv = document.getElementById('acceptScheduleDiv');

        var rowDiv = document.createElement("div");

        var rowDelete = document.createElement("button");
        rowDelete.className = "btn btn-danger btn-circle btn-outline";
        rowDelete.type = "button";
        rowDelete.innerHTML = '<i class="fa fa-minus"></i>';
        rowDelete.setAttribute("onClick","deleteThisScheduleRow(this)");

        var daysSelect = document.createElement("select");
        daysSelect.className = "form-control";
        daysSelect.name = "daysSelect[]";
        daysSelect.setAttribute("style","width: 130px;display: inline-table;margin-left: 9px;");

        var option = document.createElement("option");
        option.innerHTML = "Sunday";
        daysSelect.appendChild(option);

        var option = document.createElement("option");
        option.innerHTML = "Monday";
        daysSelect.appendChild(option);

        var option = document.createElement("option");
        option.innerHTML = "Tuesday";
        daysSelect.appendChild(option);

        var option = document.createElement("option");
        option.innerHTML = "Wednesday";
        daysSelect.appendChild(option);

        var option = document.createElement("option");
        option.innerHTML = "Thursday";
        daysSelect.appendChild(option);

        var option = document.createElement("option");
        option.innerHTML = "Friday";
        daysSelect.appendChild(option);

        var option = document.createElement("option");
        option.innerHTML = "Saturday";
        daysSelect.appendChild(option);

        if(preset_day != ""){daysSelect.value = preset_day;}

        var S = document.createElement("input");
        S.type = "text";
        S.className = "form-control";
        S.name = "Sschedule[]";
        S.setAttribute("placeholder","00:00");
        S.setAttribute("autocomplete","off");
        S.id = "Sschedule"+countScheduleRows;
        S.setAttribute("onChange","STimeSet(this,'"+countScheduleRows+"')");
        S.setAttribute("style","width: 130px;display: inline-table;margin-left: 9px;");
        if(preset_S != ""){S.value = preset_S;}
        if(preset_isAllDay == "true"){S.disabled = true;}

        var to = document.createElement("span");
        to.innerHTML = "-";
        to.setAttribute("style","display: inline-table;margin-left: 9px;");

        var E = document.createElement("input");
        E.type = "text";
        E.className = "form-control";
        E.name = "Eschedule[]";
        E.setAttribute("placeholder","00:00");
        E.setAttribute("autocomplete","off");
        E.id = "Eschedule"+countScheduleRows;
        E.setAttribute("style","width: 130px;display: inline-table;margin-left: 9px;");
        if(preset_E != ""){E.value = preset_E;}
        if(preset_isAllDay == "true"){E.disabled = true;}

        var checkDiv = document.createElement("div");
        checkDiv.className = "checkbox checkbox-success";
        checkDiv.setAttribute("style","width: 93px;display: inline-table;margin-left: 9px;");

        var checkInput = document.createElement("input");
        checkInput.type = "checkbox";
        checkInput.id = "allDay"+countScheduleRows;
        checkInput.name = "allDay[]";
        checkInput.setAttribute("onChange","scheduleAllDayClicked(this,'"+countScheduleRows+"')")
        if(preset_isAllDay != ""){
            if(preset_isAllDay == "true"){
                checkInput.checked = true;
            }
        }

        var checkLabel = document.createElement("label");
        checkLabel.setAttribute("for","allDay"+countScheduleRows);
        checkLabel.innerHTML = "All Day";

        checkDiv.appendChild(checkInput);
        checkDiv.appendChild(checkLabel);


        rowDiv.appendChild(rowDelete);
        rowDiv.appendChild(daysSelect);
        rowDiv.appendChild(S);
        rowDiv.appendChild(to);
        rowDiv.appendChild(E);
        rowDiv.appendChild(checkDiv);

        acceptScheduleDiv.appendChild(rowDiv);

        $('#Sschedule'+countScheduleRows).datetimepicker({
            datepicker:false,
            format:'H:i',
            step:30
        });
        $('#Eschedule'+countScheduleRows).datetimepicker({
            datepicker:false,
            format:'H:i',
            step:30
        });
    }

    function deleteThisScheduleRow(obj){
        var row = obj.parentNode;
        if (row) {
            row.parentNode.removeChild(row);
        }
    }

    function displayProviders(providers,selectedProviders){

        if(selectedProviders == false){selectedProviders = [];}
        // ******** CREATE CHECK ALL ********

        var isAllChecked = true;

        for(var i = 0;i<providers.length;i++) {
            var providerData = providers[i];
            if(!selectedProviders.includes(providerData.id)){
                isAllChecked = false;
            }
        }


        var acceptProvidersDiv = document.getElementById('acceptProvidersDiv');

        var colDiv = document.createElement("div");
        colDiv.className = "col-md-3";

        var checkDiv = document.createElement("div");
        checkDiv.className = "checkbox checkbox-success";
        checkDiv.setAttribute("style", "display: inline-table;margin-left: 9px;");

        var checkInput = document.createElement("input");
        checkInput.type = "checkbox";
        checkInput.id = "checkall";
        checkInput.setAttribute("onChange","checkAllCheckboxes(this)");

        if(isAllChecked == true){
            checkInput.checked = true;
        }

        var checkLabel = document.createElement("label");
        checkLabel.setAttribute("for", "checkall");
        checkLabel.innerHTML = "Check All";

        checkDiv.appendChild(checkInput);
        checkDiv.appendChild(checkLabel);

        colDiv.appendChild(checkDiv);
        acceptProvidersDiv.appendChild(colDiv);
        // ******** CREATE CHECK ALL ********


        var countProviderRows = 0;

        for(var i = 0;i<providers.length;i++) {
            var providerData = providers[i];

            countProviderRows++;

            var acceptProvidersDiv = document.getElementById('acceptProvidersDiv');

            var colDiv = document.createElement("div");
            colDiv.className = "col-md-4";

            var checkDiv = document.createElement("div");
            checkDiv.className = "checkbox checkbox-success";
            checkDiv.setAttribute("style","display: inline-table;margin-left: 9px;");

            var checkInput = document.createElement("input");
            checkInput.type = "checkbox";
            checkInput.id = "provider" + countProviderRows;
            checkInput.name = "provider[]";
            checkInput.value = providerData.id;

            if(selectedProviders.includes(providerData.id)){
                checkInput.checked = true;
            }

            var checkLabel = document.createElement("label");
            checkLabel.setAttribute("for", "provider" + countProviderRows);
            checkLabel.innerHTML = providerData.providerName;

            checkDiv.appendChild(checkInput);
            checkDiv.appendChild(checkLabel);

            colDiv.appendChild(checkDiv);
            acceptProvidersDiv.appendChild(colDiv);
        }
    }

    function showRotationInfo(e){
        e.stopPropagation();
        swal({
            text: "By selecting this option, you allow each lead to be spontaneously allocated to your salesman automatically.",
            buttons: {
                skip:{
                    text:"Got It"
                }
            }
        });
    }

    function showLeadTagsInfo(e){
        e.stopPropagation();
        swal({
            text: "Here you can tag and colour code your leads to flag/mark or categorise them",
            buttons: {
                skip:{
                    text:"Got It"
                }
            }
        });
    }

    function saveRotationRule(){
        var userId = selectedUserId;
        var acceptScheduleDiv = document.getElementById('acceptScheduleDiv');

        var daysSelect = document.getElementsByName('daysSelect[]');
        var Sschedule = document.getElementsByName('Sschedule[]');
        var Eschedule = document.getElementsByName('Eschedule[]');
        var allDay = document.getElementsByName('allDay[]');

        var dataNotOk = false; // will be true if schedule time is left empty

        var schedule = [];
        for(var i = 0;i<daysSelect.length;i++){
            var theDay = daysSelect[i].value;
            var S = Sschedule[i].value;
            var E = Eschedule[i].value;
            var isAllDay = allDay[i].checked;

            var singleSchedule = {};
            singleSchedule.day = theDay;
            singleSchedule.S = S;
            singleSchedule.E = E;
            singleSchedule.isAllDay = isAllDay;

            if(isAllDay == false && (theDay == "" || S == "" || E == "")){dataNotOk = true;}
            schedule.push(singleSchedule);
        }

        var providers = [];
        var providersCheckboxes = document.getElementsByName('provider[]');
        for(var i = 0;i<providersCheckboxes.length;i++) {
            var providersCheckbox = providersCheckboxes[i];

            if(providersCheckbox.checked == true){
                providers.push(providersCheckbox.value);
            }
        }

        if(dataNotOk == true){
            document.getElementById('dataNotOk').style.display = '';
            return;
        }else{
            document.getElementById('dataNotOk').style.display = 'none';
        }

        var strUrl = BASE_URL+'/console/actions/leads/leadSalesRotation/updateRotationRulesOfUser.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                userId:userId,
                schedule:schedule,
                providers:providers
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            getRotationRulesOfUser(userId);
            try{
                data = JSON.parse(data);

                if(data == true){
                    document.getElementById('saveBtn').className = "btn btn-primary pull-right disabled";
                    document.getElementById('saveBtn').innerHTML = "Saved";

                    setTimeout(function(){
                        document.getElementById('saveBtn').className = "btn btn-primary pull-right";
                        document.getElementById('saveBtn').innerHTML = "Save";


                    }, 2000);

                }

            }catch(e){
                swal("Oops", "Please try again later", "error");
            }
        });


    }

    function scheduleAllDayClicked(obj,id){
        if(obj.checked == true){
            document.getElementById('Sschedule'+id).setAttribute("disabled","true");
            document.getElementById('Eschedule'+id).setAttribute("disabled","true");
        }else{
            document.getElementById('Sschedule'+id).removeAttribute("disabled","false");
            document.getElementById('Eschedule'+id).removeAttribute("disabled","false");
        }
    }

    function checkAllCheckboxes(obj){
        var provider = document.getElementsByName('provider[]');

        for(var i = 0;i<provider.length;i++){
            if(obj.checked == true){
                provider[i].checked = true;
            }else{
                provider[i].checked = false;
            }
        }
    }

    function STimeSet(obj,id){

        var Sschedule = document.getElementById('Sschedule'+id);

        $('#Eschedule'+id).datetimepicker({
            datepicker:false,
            format:'H:i',
            step:30,
            minTime:Sschedule.value
        });

    }

    function clearSelectedRows(){
        $(".userRow").removeClass("selectedUserRow");
    }

    function addRule(){
        document.getElementById('salesRule').style.display = 'none';
        document.getElementById('usersWithRules').style.display = 'none';
        document.getElementById('addARuleForUserDiv').style.display = '';
    }

    function addRule_back(){
        document.getElementById('salesRule').style.display = 'none';
        document.getElementById('usersWithRules').style.display = '';
        document.getElementById('addARuleForUserDiv').style.display = 'none';

        clearSelectedRows();
    }

    function toggleUserRole(e,userId){
        e.stopPropagation();
        var strUrl = BASE_URL+'/console/actions/leads/leadSalesRotation/toggleUserRotation.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                userId:userId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            
            try{
                data = JSON.parse(data);
                if (data == 1){
                    $("#userStatus-"+userId).removeClass("text-warning");
                    $("#userStatus-"+userId).addClass("text-success");
                    $("#userStatus-"+userId).text("Active");
                    $("#toggleUser-"+userId).text("Pause");
                }else{
                    $("#userStatus-"+userId).addClass("text-warning");
                    $("#userStatus-"+userId).removeClass("text-success");
                    $("#userStatus-"+userId).text("Inactive");
                    $("#toggleUser-"+userId).text("Activate");
                }

            }catch(e){
                swal("Oops", "Please try again later", "error");
            }
        });
    }

    function selectedUser(obj){
        if(obj.value != ""){
            document.getElementById('addUserRule').disabled = false;
        }else{
            document.getElementById('addUserRule').disabled = true;
        }
    }

    function getTags(){
        var strUrl = BASE_URL+'/console/actions/leads/lead/tags/getTags.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{

            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                var container = document.getElementById("leadTagsBody");
                container.innerHTML = "";
                if (data){
                    for (var i = 0; i<data.length;i++){
                        setTag(data[i],container);
                    }
                }else{
                    var tr = document.createElement("tr");
                    var td = document.createElement("td");
                    td.setAttribute("colspan",3);
                    var tdText = document.createTextNode("No Tags Available");
                    td.appendChild(tdText);
                    tr.appendChild(td);
                    container.appendChild(tr);
                }
            }catch (e) {
                toastr.error("Failed loading tags, please try again later");
            }
        });
    }

    function getEditTag(id){
        var strUrl = BASE_URL+'/console/actions/leads/lead/tags/getTagById.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                tagId:id
            },
            success: function (html) {
                strReturn = html;
            },
            async: false
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                $("#editId").val(data.id);
                $("#editTagName").val(data.name);
                $("#mcp-container").remove();
                var mcp = document.getElementById("tagEditColor");
                createPicker(mcp);
                $(document).ready(function () {
                    setCustomColor(data.color);
                });
            }catch (e) {
                toastr.error("Failed loading tag, please try again later");
            }
        });
    }

    function setTag(data,container) {
        var tr = document.createElement("tr");

        var td = document.createElement("td");
        var label = document.createElement("label");
        label.style.height = "22px";
        label.style.display = "block";
        label.style.margin = "0 auto";
        label.classList.add('label');
        label.style.backgroundColor = data.color;
        td.appendChild(label);
        tr.appendChild(td);

        var td = document.createElement("td");
        var tdText = document.createTextNode(data.name);
        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        td.classList.add("text-center");
        var deleteButton = document.createElement("button");
        deleteButton.setAttribute("onclick","deleteTag("+data.id+")");
        deleteButton.style.marginRight = "15px";
        deleteButton.classList.add("btn");
        deleteButton.classList.add("btn-xs");
        deleteButton.classList.add("btn-danger");

        var buttonTrash = document.createElement("i");
        buttonTrash.classList.add("fa");
        buttonTrash.classList.add("fa-trash");

        deleteButton.appendChild(buttonTrash);

        td.appendChild(deleteButton);

        var editButton = document.createElement("button");
        editButton.setAttribute('onclick',"toggleEdit(true,"+data.id+")");
        editButton.classList.add("btn");
        editButton.classList.add("btn-xs");
        editButton.classList.add("btn-warning");

        var buttonEdit = document.createElement("i");
        buttonEdit.classList.add("fa");
        buttonEdit.classList.add("fa-pencil");

        editButton.appendChild(buttonEdit);

        td.appendChild(editButton);
        tr.appendChild(td);

        container.appendChild(tr);
    }

    function toggleTab(type){
        $("#editTagContainer").css("display","none");
        $("#mcp-container").remove();
        if (type){
            var mcp = document.getElementById("tagColor");
            createPicker(mcp);
            $("#addNewTagContainer").css("display","block");
            $("#leadTagsTable").css("display","none");
        }else{
            $("#leadTagsTable").css("display","block");
            $("#addNewTagContainer").css("display","none");
        }
    }

    function toggleEdit(type,id){
        $("#addNewTagContainer").css("display","none");
        if (type){
            $("#editTagContainer").css("display","block");
            $("#leadTagsTable").css("display","none");
            getEditTag(id);
        }else{
            $("#leadTagsTable").css("display","block");
            $("#editTagContainer").css("display","none");
        }
    }

    function sendTag() {
        var name = $("#newTagName").val();
        if(name == ""){
            $("#newTagName").parent().addClass("has-error");
            setTimeout(function(){
                $("#newTagName").parent().removeClass("has-error");
            }, 3000);

            return;
        }

        var color = getColor();
        var strUrl = BASE_URL+'/console/actions/leads/lead/tags/addNewTag.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                name:name,
                color:color
            },
            success: function (html) {
                strReturn = html;
            },
            async: false
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data){
                    getTags();
                    toggleTab(false);
                    setCustomColor("#00000");
                    document.getElementById("newTagName").value = "";
                }else{
                    toastr.error("Adding a tag failed");
                }
            }catch (e) {
                toastr.error("Adding a tag failed");
            }
        });
    }

    function updateTag() {
        var name = $("#editTagName").val();
        var color = getColor();
        var id = $("#editId").val();

        var strUrl = BASE_URL+'/console/actions/leads/lead/tags/updateTag.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                tagId:id,
                name:name,
                color:color
            },
            success: function (html) {
                strReturn = html;
            },
            async: false
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data){
                    getTags();
                    toggleTab(false);
                    setCustomColor("#00000");
                    document.getElementById("newTagName").value = "";
                }else{
                    toastr.error("Adding a tag failed");
                }
            }catch (e) {
                toastr.error("Adding a tag failed");
            }
        });
    }

    function deleteTag(id) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this tag",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then((isConfirm)=>{
            if (isConfirm) {
                var strUrl = BASE_URL+'/console/actions/leads/lead/tags/deleteTag.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data:{
                        tagId:id
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: false
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data){
                            getTags();
                        }else{
                            toastr.error("deleting a tag failed");
                        }
                    }catch (e) {
                        toastr.error("deleting a tag failed");
                    }
                });
            }
        });
    }

    getTags();
    getProviders();
    getUsers();
</script>