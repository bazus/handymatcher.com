<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 05/10/2018
 * Time: 0:19
 */
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
if(isset($_GET['value']) && isset($_GET['type'])){
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");
    $val = $_GET['value'];
    $type = $_GET['type'];
    $leads = new leads($bouncer["credentials"]["orgId"]);
    $duplicates = $leads->getDuplicate($type,$val,$bouncer["credentials"]["userId"],$bouncer['isUserAnAdmin']);
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";</script>
<style>
    .modal-dialog{
        width: 80%;
    }
    .modal-body{
        padding: 15px;
    }
    @media (max-width: 480px) {
        .modal-dialog{
            margin: 0 auto;
            width: 90%;
        }
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>

<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <form id="groupForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 modal-body table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <?php if ($_GET['type'] == "email") {?>
                                    <th style="color: #ED5565;">Email</th>
                                    <th>Phone</th>
                                <?php }elseif($_GET['type'] == "phone") {?>
                                    <th>Email</th>
                                    <th style="color: #ED5565;">Phone</th>
                                <?php }?>
                                <th>Date Received</th>
                                <th>Provider</th>
                                </thead>
                                <tbody id="leadsTableHead">
                                <?php foreach ($duplicates as $lead) {
                                    if (!$lead['providerName']){$lead['providerName'] = "Manual";}
                                    ?>
                                    <tr <?php if ($lead['id'] == $_GET['value']){echo "style='background:#d4d7d8;'" ;} ?> onclick="moveLocation(<?= $lead['id'] ?>)">
                                        <td><?= $lead['id'] ?></td>
                                        <td><?= $lead['firstname'] ?></td>
                                        <td><?= $lead['lastname'] ?></td>
                                        <?php if ($_GET['type'] == "email") {?>
                                            <td style="color: #ED5565;"><?= $lead['email'] ?></td>
                                            <td><?= $lead['phone'] ?></td>
                                        <?php }elseif($_GET['type'] == "phone") {?>
                                            <td><?= $lead['email'] ?></td>
                                            <td style="color: #ED5565;"><?= $lead['phone'] ?></td>
                                        <?php }?>
                                        <td><?= $lead['dateReceived'] ?></td>
                                        <td><?= $lead['providerName'] ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-white btn-block" id="closeBtn" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    function moveLocation(id){
        window.open(BASE_URL+"/console/categories/leads/lead.php?leadId="+id);
    }
</script>