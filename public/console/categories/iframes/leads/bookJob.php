
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
if(isset($_GET['leadId'])){
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/crew.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_GET['leadId'];

    $trucks = new trucks($bouncer["credentials"]["orgId"]);
    $trucksData = $trucks->getTrucks(true);

    $carriers = new carriers($bouncer["credentials"]["orgId"]);
    $carriersData = $carriers->getCarriers(true);

    $crew = new crew($bouncer["credentials"]["orgId"]);
    $crewData = $crew->getCrew();
}
?>
<style>
    .greenCodeTag{
        color: #19b394 !important;
        font-weight: bold !important;
        font-size: 13px !important;
    }

    .content{
        height: 576px;
        overflow-y: scroll;
    }

    .footer{
        position: absolute;
        right: 0;
        width: 100%;
        border-top: 1px solid #d4d4d4;
        bottom: 0;
        padding: 7px;
        text-align: right;
        padding-right: 15px;
    }

    .table{
        margin-bottom: 0;
    }

    .table td{
        padding: 0.50rem !important;
        vertical-align: middle !important;
    }

    tbody>tr>td:first-child{
        text-align: center;
    }
    tbody>tr:first-child>td{
        border-top: none;
    }

    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }

    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }

    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }

    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }

    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }

    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }

    .btn-primary{
        background-color: #1ab394 !important;
        border-color: #1ab394 !important;
        color: #FFFFFF !important;
    }
    .crew-btns{
        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
        align-items: center;
    }
    .crew-btns .crew-send-sms, .crew-unassign{
        margin: 2px;
    }

    #assignedTableBody td{
        border: none !important;
    }
    .assignedTable thead th{
        border-bottom-width: 1px !important;
    }
    .swal-checkCrewAvailability{

    }
</style>
<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
    var leadId = "<?= $_GET['leadId']; ?>";
</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>


            <div id="wrapper" style="padding: 9px 9px 0px 9px; visibility: hidden">

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">


                            <h3 style="text-align: center;text-align: left;color: #2b2b2b;font-size: 21px;margin-bottom: 10px;">Operations</h3>
                            <table class="table table-bordered assignedTable" style="margin-bottom: 30px;background-color: #fff">
                                <thead>
                                <tr>
                                    <th style="width: 90px;">Type</th>
                                    <th>Name</th>
                                    <th style="width: 120px;"></th>
                                </tr>
                                </thead>
                                <tbody id="assignedTableBody">
                                </tbody>
                            </table>


                            <div class="tabs-container">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#tab-trucks"> Trucks</a></li>
                                    <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-carriers"> Carriers</a></li>
                                    <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tab-crew"> Crew</a></li>
                                </ul>
                                <div class="tab-content" style="border: 1px solid;border-color: #dee2e6; border-top: none;">
                                    <div id="tab-trucks" class="tab-pane active">
                                        <div class="panel-body">

                                            <table class="table">
                                                <tbody>
                                                <?php if ($trucksData && count($trucksData) > 0){ ?>
                                                    <?php foreach ($trucksData as $truck) { ?>
                                                        <tr>
                                                            <td style="width: 90px"><i class="fa fa-truck"></i></td>
                                                            <td><?= $truck['title'] ?></td>
                                                            <td style="width: 120px; text-align: center">
                                                                <button id="truck-btn-add-<?= $truck['id'] ?>" onclick="assign('truck','<?= $truck['id'] ?>')" class="btn btn-sm btn-primary">Assign</button>
                                                                <button id="truck-btn-minus-<?= $truck['id'] ?>" onclick="unAssign('truck','<?= $truck['id'] ?>')" class="btn btn-sm btn-danger" style="display: none;">Un-assign</button>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php }else{ ?>
                                                    <tr><td style="text-align: center" colspan="3">No Active Trucks</td></tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="tab-carriers" class="tab-pane">
                                        <div class="panel-body">

                                            <table class="table">
                                                <tbody>
                                                <?php if ($carriersData && count($carriersData) > 0){ ?>
                                                    <?php foreach ($carriersData as $carrier) { ?>
                                                        <tr>
                                                            <td style="width: 90px"><i class="fa fa-truck"></i></td>
                                                            <td><?= $carrier['name'] ?></td>
                                                            <td style="width: 120px; text-align: center">
                                                                <button id="carrier-btn-add-<?= $carrier['id'] ?>" onclick="assign('carrier','<?= $carrier['id'] ?>')" class="btn btn-sm btn-primary">Assign</button>
                                                                <button id="carrier-btn-minus-<?= $carrier['id'] ?>" onclick="unAssign('carrier','<?= $carrier['id'] ?>')" class="btn btn-sm btn-danger" style="display: none;">Un-assign</button>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php }else{ ?>
                                                    <tr><td style="text-align: center" colspan="3">No Active Carriers</td></tr>
                                                <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="tab-crew" class="tab-pane">
                                        <div class="panel-body" style="padding: 0">
                                            <table class="table">
                                                <tbody>
                                                <?php if ($crewData && count($crewData) > 0){ ?>
                                                    <?php foreach ($crewData as $crew) { ?>
                                                        <tr>
                                                            <td style="width: 90px"><i class="fa fa-user"></i></td>
                                                            <td><?= $crew['name'] ?></td>
                                                            <td><?php
                                                                if($crew['type'] == "1"){
                                                                    echo "Laborer";
                                                                }else if($crew['type'] == "2"){
                                                                    echo "Foreman";
                                                                }else if($crew['type'] == "3"){
                                                                    echo "Driver";
                                                                }
                                                                ?></td>
                                                            <td style="width: 100px; text-align: center">
                                                                <button id="crew-btn-add-<?= $crew['id'] ?>" onclick="checkifCrewAvailable('crew','<?= $crew['id'] ?>')" class="btn btn-sm btn-primary">Assign</button>
                                                                <button id="crew-btn-minus-<?= $crew['id'] ?>" onclick="unAssign('crew','<?= $crew['id'] ?>')" class="btn btn-sm btn-danger" style="display: none;">Un-assign</button>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php }else{ ?>
                                                    <tr><td style="text-align: center" colspan="3">No Active Crew</td></tr>
                                                <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer">
                    <button class="btn btn-sm btn-primary" onclick="parent.swal.close();" id="continueBtn" style="margin-left: 5px;">Continue</button>
                </div>
            </div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>


<script>
    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });

    <?php
    /*
     * I am not sure why this code was here - it was not in use and it caused problems so I commented it (niv)(9.3.20)

    //var trucksData = JSON.parse('<?php echo json_encode($trucksData); ?>');
    //var carriersData = JSON.parse('<?php echo json_encode($carriersData); ?>');
    //var crewData = JSON.parse('<?php echo json_encode($crewData); ?>');

     */

    ?>

    var assigned = [];
    assigned["trucks"] = [];
    assigned["carriers"] = [];
    assigned["crew"] = [];

    function assign(type,id){
        if(type == "" || id == ""){return;}

        var strUrl = BASE_URL + '/console/actions/leads/assignToOperation.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "get",
            data: {
                leadId: leadId,
                type:type,
                id:id
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if(data == true){
                    getOperation();
                }else{
                    return false;
                }
            } catch (e) {
                return false;
            }
        });

    }
    function unAssign(type,id){

        var strUrl = BASE_URL + '/console/actions/leads/unAssignFromOperation.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "get",
            data: {
                leadId: leadId,
                type:type,
                id:id
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if(data == true){
                    getOperation();
                }else{
                    return false;
                }
            } catch (e) {
                return false;
            }
        });

    }
    function setAssignedTable(){

        $("#assignedTableBody").html("");

        if(assigned["trucks"].length == 0 && assigned["carriers"].length == 0 && assigned["crew"].length == 0){
            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.className = "text-center";
            td.innerHTML = "Nothing assigned";
            td.setAttribute("colspan","3");

            tr.appendChild(td);
            $("#assignedTableBody").append(tr);

        }

        for(var i = 0;i<assigned["trucks"].length;i++){
            var assignId = assigned["trucks"][i]["assignId"];
            var truckName = assigned["trucks"][i]["name"];

            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.innerHTML = "<code class='greenCodeTag'>Truck</code>";

            tr.appendChild(td);

            var td = document.createElement("td");
            td.innerHTML = truckName;

            tr.appendChild(td);

            var td = document.createElement("td");
            td.className = "text-center";
            td.innerHTML = '<button class="btn btn-sm btn-danger" onclick="unAssign(\'truck\',\''+assignId+'\')">Un-assign</button>';

            tr.appendChild(td);
            $("#assignedTableBody").append(tr);
        }

        for(var i = 0;i<assigned["carriers"].length;i++){
            var assignId = assigned["carriers"][i]["assignId"];
            var carrierName = assigned["carriers"][i]["name"];

            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.innerHTML = "<code class='greenCodeTag'>Carrier</code>";

            tr.appendChild(td);

            var td = document.createElement("td");
            td.innerHTML = carrierName;

            tr.appendChild(td);

            var td = document.createElement("td");
            td.className = "text-center";
            td.innerHTML = '<button class="btn btn-sm btn-danger" onclick="unAssign(\'carrier\',\''+assignId+'\')">Un-assign</button>';

            tr.appendChild(td);
            $("#assignedTableBody").append(tr);
        }

        for(var i = 0;i<assigned["crew"].length;i++){
            var memberType = "";
            if(assigned["crew"][i]["type"] == "1"){
                memberType = "Laborer";
            }else if(assigned["crew"][i]["type"] == "2"){
                memberType = "Foreman";
            }else if(assigned["crew"][i]["type"] == "3"){
                memberType = "Driver";
            }

            var assignId = assigned["crew"][i]["assignId"];
            var crewMemberName = assigned["crew"][i]["name"] + " (" + memberType + ") ";
            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.innerHTML = "<code class='greenCodeTag'>Crew</code>";

            tr.appendChild(td);

            var td = document.createElement("td");
            td.innerHTML = crewMemberName;

            tr.appendChild(td);

            var td = document.createElement("td");
            td.className = "text-center crew-btns";
            td.innerHTML = '<button onclick="parent.sendSMScontroller.init(\''+assigned["crew"][i]["phone"]+'\',undefined,\'You have a new Job! For more details please reach out to your contact person at [CompanyName].\',parent.leadController.moving.operations.assign)" style="display: inline-block;border: 1px solid gainsboro;" class="btn btn-light btn-sm btn-mobile-block crew-send-sms">Send SMS</button><button class="btn btn-sm btn-danger crew-unassign" onclick="unAssign(\'crew\',\''+assignId+'\')">Un-assign</button>';

            tr.appendChild(td);
            $("#assignedTableBody").append(tr);
        }
    }

    setAssignedTable();

    function getOperation(){
        var strUrl = BASE_URL + '/console/actions/leads/getOperationList.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "get",
            data: {
                leadId: leadId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (jsondata) {
            try {
                var data = JSON.parse(jsondata);
                assigned = data;
                setAssignedTable();
            } catch (e) {
                Sentry.withScope(function(scope) {
                    Sentry.setExtra("data", jsondata);
                    Sentry.setExtra("comments", "getOperation()");
                    Sentry.captureException(e);
                });
                return false;
            }
        });
    }
    function checkifCrewAvailable(type,crewId) {

        var strUrl = BASE_URL + '/console/actions/moving/crew/checkIfCrewAvailable.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data: {
                leadId: leadId,
                crewId:crewId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);

                if(data != false){
                    if(data.status){
                        swal({
                            title: "This crew member has another job assigned to this date",
                            text: "Assign anyway?",
                            buttons: {
                                cancel:"Cancel",
                                yes:"Yes"
                            },
                            className: "swal-checkCrewAvailability"
                        }).then(function(isConfirm){
                            if (isConfirm) {
                                assign(type,crewId);
                            }else{
                                return false;
                            }
                        });
                    }else{
                        assign(type,crewId);
                    }

                }else{
                    // failed ajax

                }

            } catch (e) {
                return false;
            }
        });
    }
    getOperation();
</script>