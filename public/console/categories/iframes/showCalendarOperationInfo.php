<?php
$pagePermissions = array(false,true,true,array(["calendar",1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/operations.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

$operations = new operations($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$_GET['leadId']);
$operationData = $operations->getData();
$operationsAssigned = $operations->getAssigned();

$lead = new lead($_GET['leadId'],$bouncer["credentials"]["orgId"]);
$leadData = $lead->getData();

$movingLead = new movingLead($_GET['leadId'],$bouncer["credentials"]["orgId"]);
$movingLeadData = $movingLead->getData();
?>
<!DOCTYPE html>
<head>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">
</head>
<script>
    var BASE_URL = "<?php echo $_SERVER['LOCAL_NL_URL']; ?>";
</script>
<style>
    main{
        position: relative;
    }
    #leadButton{
        position: absolute;
        bottom: 0;
        right: 0;
    }
    .infoSection{
             font-weight: bold;
    }
    #jobStatus{
        margin-bottom: 5px;
    }
</style>
<body>
    <main>
        <div>
            <div id="jobStatus">
                <?php
                if($movingLeadData["status"] == 0){
                    echo '<div class="label">' .$movingLeadData["statusText"]. '</div>';
                }else if($movingLeadData["status"] == 1){
                    echo '<div class="label label-warning">' . $movingLeadData["statusText"]. '</div>';
                }
                else if($movingLeadData["status"] == 2){
                    echo '<div class="label label-info">' . $movingLeadData["statusText"]. '</div>';
                }
                else if($movingLeadData["status"] == 3){
                    echo '<div class="label label-primary">' . $movingLeadData["statusText"]. '</div>';
                }else if($movingLeadData["status"] == 4){
                    echo '<div class="label label-primary">' . $movingLeadData["statusText"]. '</div>';
                }else if($movingLeadData["status"] == 5){
                    echo '<div class="label label-primary">' . $movingLeadData["statusText"]. '</div>';
                }else if($movingLeadData["status"] == 6){
                    echo '<div class="label label-success">' . $movingLeadData["statusText"]. '</div>';
                }else if($movingLeadData["status"] == 7){
                    echo '<div class="label label-danger">' . $movingLeadData["statusText"]. '</div>';
                }

                ?>
            </div>
            <div id="fullname">
                <?php if($leadData["firstname"] || $leadData["lastname"]) { ?>
                <h5><strong><i class="fa  fa-user-o" style="font-size: 12px;"></i><?= ' ' .$leadData["firstname"] . ' ' . $leadData["lastname"]?></strong></h5>
            </div>
           <?php }
        if($movingLeadData["fromState"] && $movingLeadData["toState"] && $movingLeadData["fromCity"] && $movingLeadData["toCity"]) {
            echo '<p><i class="fa fa-map-marker"></i>' . ' ' . $movingLeadData["fromCity"] . ", " . $movingLeadData["fromState"] . " " . "<i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i>" . " " . $movingLeadData["toCity"] . ", " . $movingLeadData["toState"] . "</p>";
        }else if($movingLeadData["fromState"] && $movingLeadData["toState"]){
            echo '<p><i class="fa fa-map-marker"></i>' . ' ' . $movingLeadData["fromState"] . " " . "<i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i>" . " "  . $movingLeadData["toState"] . "</p>";
        }
        else if($movingLeadData["fromCity"] && $movingLeadData["toCity"]){
            echo '<p><i class="fa fa-map-marker"></i>' . ' ' . $movingLeadData["fromCity"] . " " . "<i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i>" . " " . $movingLeadData["toCity"] . "</p>";
        }
        ?>

        </div>
        <hr style="margin: 0 0 7px 0;">
        <div id="trucksAssigned">
            <div class="infoSection"><small><strong>Trucks Assigned:</strong></small></div>
            <?php if($operationsAssigned["trucks"]) { ?>
                <?php
                $trucksNames = [];
                for ($i = 0; $i < count($operationsAssigned["trucks"]); $i++) {
                    $trucksNames[] = $operationsAssigned["trucks"][$i]["name"];
                }
                echo '<span><small>' . implode(", ", $trucksNames) . '</small></span>';
            }else{
                echo '<span class="text-danger"><small>None</small></span>';
            }?>
        </div>
        <div id="carriersAssigned">
            <div class="infoSection"><small><strong>Carriers Assigned:</strong></small></div>
            <?php if($operationsAssigned["carriers"]){?>
                <?php
                $carriersNames = [];
                    for($i=0; $i< count($operationsAssigned["carriers"]); $i++){
                        $carriersNames [] = $operationsAssigned["carriers"][$i]["name"];
                }
                echo '<div><small>' . implode(", ", $carriersNames) . '</small></div>';
            }else{
                echo '<span class="text-danger"><small>None</small></span>';
            }?>
        </div>
        <div id="crewAssigned">
            <div class="infoSection"><small><strong>Crew Assigned:</strong></small></div>
            <?php if($operationsAssigned["crew"]){?>
                <?php
                $crewNames = [];
                for($i=0; $i< count($operationsAssigned["crew"]); $i++){
                    $crewNames[] = $operationsAssigned["crew"][$i]["name"];
                }
                echo '<div><small>' . implode(", ", $crewNames) . '</small></div>';
            }else{
                echo '<span class="text-danger"><small>None</small></span>';
            }?>
        </div>
    </main>



<!-- Mainly scripts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/moment.js"></script>

    <!-- Toastr -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

    <!-- SweetAlerts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

    <!-- Ladda -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>
</body>
</html>

