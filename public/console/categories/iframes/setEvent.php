<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,false,array(["calendar",2]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/wickedPicker/src/wickedpicker.min.css" rel="stylesheet">
<style>
    .wickedpicker {
        z-index: 9999;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Set Event Time</h4>
                <small>Event Will Be Set On Date <?= date("Y/m/d",strtotime($_GET['date'])) ?>, Please Choose The Hours.</small>
            </div>
            <form id="setEventForm" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/calendar/setEventDate.php">
                <input type="hidden" name="eventId" value="<?= $_GET['id'] ?>">
                <input type="hidden" name="theDate" value="<?= date("Y-m-d",strtotime($_GET['date'])) ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="dateS">Event Start Time</label>
                        <input class="form-control dateS timepicker" type="text" name="dateS" placeholder="Enter Event Start Hour">
                    </div>
                    <div class="form-group">
                        <label for="dateE">Event End Time</label>
                        <input class="form-control dateE timepicker" type="text" name="dateE" placeholder="Enter Event End Hour">
                    </div>
                    <label id="spanError" style="color:red;"></label>
                </div>
                <div class="modal-footer">
                    <p class="pull-left"><small>* <u>24 Hours Format</u></small></p>
                    <button type="button" class="btn btn-white" onclick="location.reload()" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" id="eventer" data-style="zoom-in" disabled>Setup Event</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/wickedPicker/src/wickedpicker.min.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script>
    $('.timepicker').wickedpicker({
        twentyFour: true
    });
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#setEventForm').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        l.ladda( 'stop' );
        $('#myCustomModal').modal('hide');

        if(statusText == "success"){
            var data = responseText;
            data = JSON.parse(data);
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };

            if(data == true){

                // Refresh the calendar

                /*
                var strUrl = BASE_URL+'/console/actions/user/getActiveEvents.php', strReturn = "";

                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true,
                }).done(function(data){

                    data = JSON.parse(data);
                    calendar.removeEventSources();
                    calendar.addEventSource( data );

                });
                */

            }else{
                // Failed
                toastr.error('Your request was not sent, please try again later.','Error');
            }
        }else{
            // Failed
            toastr.error('Your request was not sent, please try again later.','Error');
        }
    }
    $(".dateE").on("change",function(){
        if($(".dateE").val()<$(".dateS").val()){
            document.getElementById('spanError').innerHTML = "* End time can't be before the start time";
            document.getElementById('eventer').disabled = true;
        }else{
            document.getElementById('spanError').innerHTML = "";
            document.getElementById('eventer').disabled = false;
        }
    });
</script>