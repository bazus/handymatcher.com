<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,array(["calendar",3]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
$organization = new organization($bouncer["credentials"]["orgId"]);
$users = $organization->getUsersOfMyCompany($bouncer["credentials"]["userId"]);

?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
<style>
    .pac-container{
        z-index: 99999;
    }
    #dateS{
        overflow: hidden;
        text-overflow: clip;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <form id="issueForm" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/calendar/addEvent.php">
                <div class="modal-body" style="padding: 15px;">
                    <?php if (isset($_GET['type'])){echo "<input type='hidden' name='type' value='1'>";} ?>
                    <div class="form-group">
                        <label for="eventType">Choose Calendar</label>
                        <select id='eventType' class="form-control" required name='eventType'>
                            <option value="1"><?php echo $bouncer["userData"]["fullName"]; ?> (Private)</option>
                            <option value="2"><?php echo $bouncer["organizationData"]["organizationName"]; ?></option>
                        </select>
                    </div>
                    <input type="hidden" name="theStartDate" id="theStartDate">
                    <input type="hidden" name="theEndDate" id="theEndDate">
                    <div class="form-group">
                        <label for="eventTitle">Title</label>
                        <input required class="form-control" type="text" name="eventTitle" id="eventTitle" placeholder="Event Title">
                    </div>
                    <div class="form-group">
                        <label for="eventDescription">Description (optional)</label>
                        <textarea class="form-control" style="resize: vertical;" name="eventDescription" id="eventDescription" placeholder="Event Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="eventPlace">Location (optional)</label>
                        <input class="form-control" type="text" name="eventPlace" id="eventPlace" placeholder="Event Location">
                    </div>
                    <div class="form-group">
                        <label for="dateS">Time</label>
                        <div id="dateS" class="form-control" style="">
                            <i class="fa fa-calendar"></i>
                            <span></span> <b class="caret"></b>
                        </div>
                    </div>
                    <?php if (count($users) > 0) { ?>
                    <div class="form-group">
                        <select multiple class="form-control" id="usersToSend" name="usersToSend" data-placeholder="Invite Users To This Event">
                            <?php  foreach ($users as $user){?>
                                <option value="<?= $user['id'] ?>"><?= $user['fullName'] ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <?php }?>
                    <small id="spanNotice" class="text-warning"></small>
                    <small id="spanError" class="text-danger"></small>
                </div>
                <div class="modal-footer">
                    <p class="pull-left"><small>* <u>24 Hours Format</u></small></p>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" id="eventer" data-style="zoom-in">Create Event</button>

                </div>
            </form>

        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/chosen/chosen.jquery.js"></script>
<!-- Date range use moment.js same as full calendar plugin -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/daterangepicker/daterangepicker.js"></script>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/geocomplete/jquery.geocomplete.min.js"></script>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script>
    var l;
    var theStartDate;
    var theEndDate;

    $(document).ready(function() {

        $('#dateS span').html(moment("<?= $_GET['start'] ?>").format('MMMM D, YYYY HH:mm') + ' - ' + moment("<?= date('Y-m-d H:i:s',strtotime($_GET['end'] .' +1 hour')) ?>").format('MMMM D, YYYY HH:mm'));

        $('#dateS').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment("<?= $_GET['start'] ?>"),
            endDate: moment("<?= $_GET['end'] ?>"),
            minDate: '01/01/2018',
            maxDate: '12/31/2020',
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: true,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Set',
                cancelLabel: 'Cancel',
                fromLabel: 'From Date',
                toLabel: 'To Date',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {

            $('#dateS span').html(start.format('MMMM D, YYYY HH:mm') + ' - ' + end.format('MMMM D, YYYY HH:mm'));
            theStartDate = start.format('DD-MM-YYYY HH:mm');
            theEndDate = end.format('DD-MM-YYYY HH:mm');
            if("<?= $_GET['start'] ?>" >= "<?= $_GET['end'] ?>"){
                document.getElementById('spanError').innerHTML = "* The End time can't be before or the same as the start time";
                document.getElementById('eventer').disabled = true;
            }else{
                document.getElementById('spanError').innerHTML = "";
                document.getElementById('eventer').disabled = false;
            }
            $("#theStartDate").val(theStartDate);
            $("#theEndDate").val(theEndDate);

        });
        theStartDate = moment("<?= $_GET['start'] ?>").format('DD-MM-YYYY HH:mm');
        theEndDate = moment("<?= $_GET['end'] ?>").format('DD-MM-YYYY HH:mm');
        $("#theStartDate").val(theStartDate);
        $("#theEndDate").val(theEndDate);

    });

    $("#usersToSend").chosen({
        width: "100%",
        allow_single_deselect: true
    });

    $("#usersToSend").on("change",function(){
        if ($(this).val().length > 0){
            $("#spanNotice").text("* This Event Will Be Visible To All Users In The Organization");
        }else{
            $("#spanNotice").text("");
        }
    });

    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };
    $('#issueForm').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        if (document.getElementById("usersToSend") != undefined && $("#usersToSend").val().length > 0) {
            $("#calendarType").val(2);
            toastr.success("Organization","Event Created");
        }else{
            if (document.getElementById("eventType") && document.getElementById("eventType").value == "2"){
                $("#calendarType").val(2);
                toastr.success("Organization","Event Created");
            }else{
                $("#calendarType").val(1);
                toastr.success("Personal","Event Created");
            }
        }
        l.ladda('stop');
        $('#myCustomModal').modal('hide');
        calendarController.getEvents();
    }

</script>