<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 05/10/2018
 * Time: 0:19
 */
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
?>
<style>
    .modal-dialog{
        width: 80%;
    }
    /*.modal-body{*/
    /*    padding: 15px;*/
    /*}*/
    /*@media (max-width: 480px) {*/
    /*    .modal-dialog{*/
    /*        margin: 0 auto;*/
    /*        width: 90%;*/
    /*    }*/
    /*}*/

    #myCustomModal .table thead tr th{
        text-align: center;
    }
    #myCustomModal .table tbody tr td:nth-child(2),#myCustomModal .table tbody tr td:nth-child(3){
        font-weight: bold;
    }
    .pricing-text{
        width: 55%;
        position: absolute;
        top: 30%;
        left: 40%;
        font-size:1vw;
        color: #252525;
    }
    .we-provide-text{
        color: #252525;
        font-size:1.5vw;
    }
    .incoming-outgoing-text{
        width: 55%;
        position: absolute;
        bottom: 30%;
        left: 40%;
        color: #252525;
        font-size:2vw;
    }
    @media only screen and (max-width: 768px) {
        .modal-dialog{
            width: 100%;
            height: 100vh;
            margin: 0;
        }
        .table-responsive{
            border: none;
        }
        #pricing-img{
          display: none;
        }
        .pricing-text{
            padding: 1rem;
            color: black;
            font-size:5vw;
            width: 100%;
            left: 0;
            position: unset;
            background-color: #baf6ff;
            margin: 0;
        }
        .we-provide-text{
            font-size: 27px;
            color: black;
            font-size:6vw;
            position: unset;
        }
        .incoming-outgoing-text{
            color: black;
            font-size: 5.5vw;
            position: unset;
            left: 0;
            width: 100%;
            padding: 1rem;
            background-color: #baf6ff;
        }
    }

</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>

<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left; padding: 15px;">
                <button type="button" class="close" data-dismiss="modal" style="font-size: 28px;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Text Message Pricing</h4>
                <small>Prices for text messages platform</small>
            </div>
            <div class="modal-body" style="padding: 0;">
                <div class="table-responsive">
                    <img id="pricing-img" style="max-width: 100%;" src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/sms3.jpg">
                    <p class="pricing-text">
                        <span class="we-provide-text">We provide up to 3 numbers free</span> (included in your plan), any additional phone number will add $2/month/number to your monthly Network Leads Software subscription.
                    </p>
                    <div class="incoming-outgoing-text">
                        Incoming and Outgoing SMS: 1 cent
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>