<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
    .btn-custom:hover {
        background-color: #0b93d5 !important;
        color: white !important;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left;padding: 15px;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info pull-right" style="cursor:pointer;margin-right: 10px;margin-top: 2px;opacity: 0.3;" onclick="showPhoneSearchInfo();"></i>
                <h4 class="modal-title">Assign phone numbers</h4>
                <small>These will be used for Text Message / sms marketing</small>
            </div>
            <div class="row" style="padding: 5px;">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" style="padding: 0">
                            <select class="form-control" id="searchPhoneByState" style="border:none;height: 32px;width: fit-content;font-size: 12px;">
                                <option value="">All States</option>
                                <option value="AL">AL</option>
                                <option value="AK">AK</option>
                                <option value="AZ">AZ</option>
                                <option value="AR">AR</option>
                                <option value="CA">CA</option>
                                <option value="CO">CO</option>
                                <option value="CT">CT</option>
                                <option value="DC">DC</option>
                                <option value="DE">DE</option>
                                <option value="FL">FL</option>
                                <option value="GA">GA</option>
                                <option value="HI">HI</option>
                                <option value="IA">IA</option>
                                <option value="ID">ID</option>
                                <option value="IL">IL</option>
                                <option value="IN">IN</option>
                                <option value="KS">KS</option>
                                <option value="KY">KY</option>
                                <option value="LA">LA</option>
                                <option value="MA">MA</option>
                                <option value="MD">MD</option>
                                <option value="ME">ME</option>
                                <option value="MI">MI</option>
                                <option value="MN">MN</option>
                                <option value="MO">MO</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="NC">NC</option>
                                <option value="ND">ND</option>
                                <option value="NE">NE</option>
                                <option value="NH">NH</option>
                                <option value="NJ">NJ</option>
                                <option value="NM">NM</option>
                                <option value="NV">NV</option>
                                <option value="NY">NY</option>
                                <option value="OH">OH</option>
                                <option value="OK">OK</option>
                                <option value="OR">OR</option>
                                <option value="PA">PA</option>
                                <option value="RI">RI</option>
                                <option value="SC">SC</option>
                                <option value="SD">SD</option>
                                <option value="TN">TN</option>
                                <option value="TX">TX</option>
                                <option value="UT">UT</option>
                                <option value="VA">VA</option>
                                <option value="VT">VT</option>
                                <option value="WA">WA</option>
                                <option value="WI">WI</option>
                                <option value="WV">WV</option>
                                <option value="WY">WY</option>
                            </select>
                        </span>
                        <input autofocus type="text" class="form-control" id="searchPhoneNumber" onkeyup="checkPhoneNumberBeforeSend()" placeholder="Search availability of desired number or pattern">
                        <span class="input-group-btn">
                            <button class="btn btn-primary ladda-button search-ladda-button" id="phoneSearchBTN" disabled onclick="searchNumber()">Search</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin: 0">
                <div class="col-md-12 table-responsive" style="overflow-y: hidden;padding: 0;">
                    <table class="table table-bordered" style="margin-bottom: 0">
                        <thead>
                        <tr>
                            <th class="text-center">Phone Number</th>
                            <th class="text-center">Postal Code</th>
                            <th class="text-center">State</th>
                            <th class="text-center"></th>
                        </tr>
                        </thead>
                        <tbody id="phoneNumbers">
                        <tr>
                            <td class="text-center" colspan="4">--- Search for phone number ---</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<script>
    function checkPhoneNumberBeforeSend() {
        var phoneNumber = $("#searchPhoneNumber").val();
        if (phoneNumber.toString().length > 1){
            $("#phoneSearchBTN").removeAttr("disabled");
        }else{
            $("#phoneSearchBTN").attr("disabled",true);
        }
    }
    var l;
    $(document).ready(function () {
        l = $('.search-ladda-button').ladda();
    });
    function searchNumber() {
        var searchVal = document.getElementById("searchPhoneNumber").value;
        var searchState = document.getElementById("searchPhoneByState").value;
        if (searchVal.toString().length > 1) {
            l.ladda("start");
            $.ajax({
                url: BASE_URL + "/console/actions/system/twilio/searchAvailablePhoneNumbers.php",
                method: "GET",
                data: {
                    byNumber: searchVal,
                    searchState: searchState
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data.length > 0) {
                        document.getElementById("phoneNumbers").innerHTML = "";
                        for (var i = 0; i < data.length; i++) {
                            setPurchasePhoneNumbers(data[i]);
                        }
                        purchaseLadda = $('.purchase-ladda').ladda();
                        l.ladda("stop");
                    } else {
                        document.getElementById("phoneNumbers").innerHTML = "<tr><td class='text-center' colspan='4'>No matches found</td></tr>";
                        l.ladda("stop");
                    }
                } catch (e) {
                    l.ladda("stop");
                    toastr.error("Failed Loading Phone Data", "Error");
                }
            });
        }else{
            toastr.error("Search too short, 2 characters required","Error");
        }

    }
    function purchaseNewNumber(number,state,postalCode,objId) {
        $("#"+objId).ladda();
        $("#"+objId).ladda('start');
        if (number){
            $.ajax({
                url: BASE_URL + "/console/actions/system/twilio/buyPhoneNumber.php",
                method: "GET",
                data: {
                    phoneNumber: number,
                    state:state,
                    postalCode:postalCode
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data.status == true) {
                        toastr.success("Number purchased","Purchased");
                        $("#"+objId).ladda('stop');
                        getTwillioAccounts();
                        $("#myCustomModal").modal('hide');
                    } else {
                        if (data.resp == "maxphones"){
                            $("#"+objId).ladda('stop');
                            toastr.error("Max phone numbers per organization reached","Limit Reached");
                        } else{
                            $("#"+objId).ladda('stop');
                            toastr.error("There was an error purchasing this number, please try again later","Failed");
                        }
                    }
                } catch (e) {
                    $("#"+objId).ladda('stop');
                    toastr.error("There was an error purchasing this number, please try again later","Failed");
                }
            });
        }else{
            $("#"+objId).ladda('stop');
            toastr.error("There was an error purchasing this number, please try again later","Failed");
        }
    }

    var phoneNumberId = 1;
    function setPurchasePhoneNumbers(data){
        var container = document.getElementById("phoneNumbers");

        var tr = document.createElement("tr");

        var td = document.createElement("td");
        td.classList.add("text-center");
        // check data before setting it to dom
        if (!data.friendlyName){    if (data.phoneNumber){  data.friendlyName = data.phoneNumber;   }else{  data.friendlyName = "";    }   }
        var tdText = document.createTextNode(data.friendlyName);
        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        td.classList.add("text-center");
        // check data before setting it to dom
        if (!data.postalCode){  data.postalCode = "";     }
        var tdText = document.createTextNode(data.postalCode);
        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        td.classList.add("text-center");
        // check data before setting it to dom
        if (!data.region){  data.region = "";     }
        var tdText = document.createTextNode(data.region);
        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        td.classList.add("text-center");
        var tdButton = document.createElement("button");
        tdButton.setAttribute("onclick","purchaseNewNumber('"+data.phoneNumber+"','"+data.region+"','"+data.postalCode+"','phoneNumber-"+phoneNumberId+"')");
        tdButton.classList.add("btn");
        tdButton.classList.add("ladda-button");
        tdButton.classList.add("btn-primary");
        tdButton.classList.add("btn-block");
        tdButton.id = "phoneNumber-"+phoneNumberId;
        phoneNumberId++;

        var tdButtonText = document.createTextNode("Assign Number");
        tdButton.appendChild(tdButtonText);

        td.appendChild(tdButton);
        tr.appendChild(td);

        container.appendChild(tr);

    }
    function removeTextPlus(text){
        return text.replace('+',"");
    }
    function showPhoneSearchInfo() {
        swal({
            text: "Here you can search for specific phone number patterns or find numbers that are similar to those your company already use today, or relevant area codes etc...",
            buttons: {
                skip:{
                    text:"Got It"
                }
            },
            className: "left-text-swal"
        });
    }
</script>