<?php
$pagePermissions = array(false,true,true,array(['mailcenter',3]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

$email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$isLimitValid = $email->isLimitValid($bouncer["credentials"]["orgId"]);

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$organizationMailAccounts = $mailaccounts->getMyAccounts(true,true);

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$folders = $preEmails->getFoldersData();

$leadsIds = [];

if (isset($_GET['leadsIds'])){
    $leadsIds = $_GET['leadsIds'];
}
?>
<?php

if($isLimitValid == false){
    ?>
    <script>
        parent.limitReached("email");
    </script>
    <?php
}
?>

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/summernote/bs4/summernote-bs4.min.css" rel="stylesheet">

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/emailer.css" rel="stylesheet">

<!-- Ladda style -->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    var selectedFolder = 'all';
</script>

<style>
    html, body {
        height: 100%;
    }

    .navbar-nav li:hover > ul.dropdown-menu {
        display: block;
    }
    .dropdown-submenu {
        position:relative;
    }
    .dropdown-submenu>.dropdown-menu {
        top: 0 !important;
        left: 100% !important;
        margin-top:-6px !important;
        position: absolute !important;
    }

    /* rotate caret on hover */
    .dropdown-menu > li > a:hover:after {
        text-decoration: underline;
        transform: rotate(-90deg);
    }

    .leadTagsDropdown .dropdown-menu{
        transform: none !important;
        position: absolute !important;
    }
    .leadTagsDropdown>li>a{
        color: unset;
        padding: 0;
    }
    #btn_blank, #btn_template{
        -webkit-appearance: none;
    }
    @media print {
        body * {
            visibility: hidden;
        }
        page-wrapper * {
            visibility: hidden;
        }
        .note-editor  , .note-editor  * {
            visibility: visible;
        }
        .note-editor  {
            position: absolute;
            left: 0;
            top: 0;
            width:100%;
        }
        .note-toolbar{
            display:none;
        }
        .note-statusbar{
            display: none;
        }
        .note-placeholder{
            display: none !important;
        }
    }

</style>
<div id="wrapper" style="visibility: hidden;overflow-x: hidden; padding-right: 7px">
    <button style="font-size: 30px;outline: none;margin-top: 9px;" type="button" class="close" onclick="parent.swal.close();"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <div style="margin-bottom: 9px;">
        <div class="text-left" style="margin-top: 20px;">
            <a class="topLinks active" id="btn_template" type="button" onclick="emailerController.changeTemplate(1)">Template</a>
            <a class="topLinks" id="btn_blank" type="button" onclick="emailerController.changeTemplate(0)">Blank Email</a>
        </div>
    </div>
    <div id="templates">
        <div class="row">
            <div class="col-md-12">
                <div id="templateFolder">
                    <select onchange="emailerController.selectFolder(this)" class="form-control" id="selectedFolder">
                        <option value="all" selected>All Folders</option>
                        <?php
                        if (count($folders) > 0) {
                            ?>
                            <option value="NULL">Home</option>
                            <?php
                            foreach ($folders as $folder) { ?>
                                <option value="<?= $folder['id'] ?>"><?= $folder['name'] ?></option>
                            <?php }
                        }
                        ?>
                    </select>
                </div>
                <div id="myTemplates">

                </div>
            </div>
        </div>
    </div>
    <div id="blankEmail" style="display: none;margin-bottom: 59px;">
        <div class="contain-box">
            <div class="contain-body">
                    <div style="margin-top: 2px;" class="row">
                        <label class="col-md-2 control-label">From:</label>
                        <div class="col-md-10">
                            <select <?php if (count($organizationMailAccounts) == 0){?> disabled <?php } ?> class="form-control form-control-sm" id="fromAccount" onchange="emailerController.checkEmailInputs();">
                            <?php if (count($organizationMailAccounts) > 0){?>
                                    <?php
                                    foreach($organizationMailAccounts as $organizationMailAccount){
                                        ?>
                                        <option <?php if ($organizationMailAccount['id'] == $bouncer['organizationData']['mailAccount']){echo 'selected';} ?> value="<?php echo $organizationMailAccount['id']; ?>" ><?php echo $organizationMailAccount['email']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            <?php }else{ ?>
                                    <option value="">No emails account available</option>
                            <?php } ?>
                            </select>

                        </div>
                    </div>
                    <div style="margin-top: 2px;" class="row">
                        <label class="col-md-2 control-label">To:</label>
                        <div class="col-md-10">
                            <?php if(count($leadsIds) > 0){ ?>
                            <input type="text" id="toEmail" <?php if(count($leadsIds) > 0){echo " disabled ";} ?> value="<?php if(count($leadsIds) > 0){if(count($leadsIds) == 1){echo count($leadsIds)." lead";}else{echo count($leadsIds)." leads";}}?>" class="form-control form-control-sm cancelTo" placeholder="To Email Address" onkeyup="emailerController.checkEmailInputs();">
                            <?php }else{ ?>
                                <input id="toEmail" class="tagsinput form-control form-control-sm cancelTo" type="text" placeholder="Recipients" />
                            <?php } ?>
                        </div>
                    </div>
                    <div style="margin-top: 2px;" class="row">
                        <label class="col-md-2 control-label">Subject:</label>
                        <div class="col-md-10">
                            <input type="text" id="subjectEmail" class="form-control form-control-sm" placeholder="Email Subject" onkeyup="emailerController.checkEmailInputs();">
                        </div>
                    </div>
            </div>

            <div class="h-200" style="border-top: 1px solid #e7eaec;">

                <div class="summernote"></div>
            </div>
            <small id="errorSpan"></small>
            <div class="clearfix"></div>
            <div class="contain-footer">
                <button class="btn btn-outline-primary btn-sm" onclick="parent.swal.close();"><i class="fa fa-times"></i> Discard</button>
                <button class="btn btn-outline-primary btn-sm" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-sm btn-primary ladda-button" style="float: right" disabled data-style="expand-left" id="sendEmailBTN" onclick="sendEmail()"><i class="fa fa-reply"></i> Send</button>
            </div>

        </div>
    </div>

</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/popper.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/summernote/new/js/summernote-bs4.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<script>
    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });

    <?php if (count($leadsIds) == 0){ ?>
        function sendEmail() {
        if (emailerController.checkEmailInputs()) {

            var fromEmailAccountId = document.getElementById("fromAccount").value;
            var toEmail = document.getElementById("toEmail").value;
            var subject = document.getElementById("subjectEmail").value;
            var content = $('.summernote').summernote('code');

            var l = $("#sendEmailBTN").ladda();
            l.ladda("start");

            var strUrl = BASE_URL + '/console/actions/mail/sendEmail.php';

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    "from": fromEmailAccountId,
                    "to": toEmail,
                    "subject": subject,
                    "content": content
                },
                async: true
            }).done(function (data) {
                l.ladda("stop");

                handleResponse(data);
            });
        }
    }
    <?php } ?>

    <?php if (count($leadsIds) > 0){ ?>
    // ===== Email To Leads START =====
        function sendEmail(){
            if (emailerController.checkEmailInputs()) {

                var fromEmailAccountId = document.getElementById("fromAccount").value;
                var subject = document.getElementById("subjectEmail").value;
                var content = $('.summernote').summernote('code');

                var l = $("#sendEmailBTN").ladda();
                l.ladda("start");

                var strUrl = BASE_URL + '/console/actions/mail/sendEmailToLeads.php';

                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        "leadsIds": JSON.parse('<?php echo json_encode($leadsIds); ?>'),
                        "subject": subject,
                        "content": content,
                        "emailAccountId": fromEmailAccountId
                    },
                    async: true,
                }).done(function (data) {
                    l.ladda("stop");
                    handleResponse(data);
                });
            }
        }
    // ===== Email To Leads END =====
    <?php } ?>

    function handleResponse(data){

        // After email sent - intialize the callback
        var isCallbackAFunction =  emailerController.callbackFn && {}.toString.call(emailerController.callbackFn) === '[object Function]';
        if(emailerController.callbackFn != undefined && isCallbackAFunction){
            emailerController.callbackFn();
        }

        $("#myCustomModal").modal('hide');

        try{
            data = JSON.parse(data);

            if(data.responses.length == 1){

                if(data.responses[0].status == true){
                    parent.swal("Message sent","", "success");
                }else{
                    if(data.responses[0].responseCode == 2){
                        parent.swal("Email Not Sent", "This email address has unsubscribed and can no longer receive emails from you", "error");
                    }else{
                        parent.swal("Email Not Sent", "Your message was not sent. Please try again later.", "error");
                    }
                }
                return;
            }

            // =========== CONVERT RESPONSE ==============
            var responseTypes = [];
            var responses = [];

            var singleResponseType = [];
            singleResponseType["name"] = "Email Sent";
            singleResponseType["color"] = "#28a745";

            responseTypes[singleResponseType["name"]] = (singleResponseType);

            var singleResponseType = [];
            singleResponseType["name"] = "Not sent";
            singleResponseType["color"] = "#dc3545";

            responseTypes[singleResponseType["name"]] = (singleResponseType);


            for (var i = 0; i < data.responses.length; i++) {
                var response = {};
                response.data = data.responses[i].email;
                if (data.responses[i].status == true) {
                    response.responseType = "Email Sent";
                }else{
                    response.responseType = "Not sent";
                }

                if (data.responses[i].responseCode == 2) {
                    response.text = "Email unsubscribed";
                }
                responses.push(response);
            }

            var allData = [];
            allData.responses = responses;
            allData.responseTypes = responseTypes;
            // =========== CONVERT RESPONSE ==============

            parent.openSendingStats(allData);

        }catch (e) {
            console.log(e);
            parent.swal("Oops", "Your email was not sent. Please try again later.", "error");
        }

    }

    $(document).ready(function (){
        emailerController.initSummernote();
        emailerController.getEmailTemplates();

        $('.tagsinput').tagsinput({
            tagClass: 'badge badge-secondary'
        });
        $("#toEmail").on('itemAdded', function(event) {
            emailerController.checkEmailInputs();
        });
        $("#toEmail").on('itemRemoved', function(event) {
            emailerController.checkEmailInputs();
        });

    });

    var emailerController = {
        callbackFn:null,
        initSummernote:function(){


                var customLeadTags = function (context) {
                var ui = $.summernote.ui;
                var button = ui.button({
                    contents: ' <ul class="navbar-nav leadTagsDropdown">\n' +
                        '                <li class="nav-item dropdown leadTags">\n' +
                        '                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
                        '                        Tags\n' +
                        '                    </a>\n' +
                        '                </li>\n' +
                        '            </ul>',
                click: function () {
                    emailerController.createLeadTagsDropdown();
                    }
                });
                return button.render();
            };

            $('.summernote').summernote({
                placeholder:"Enter text..",
                dialogsInBody: true,
                minHeight:550,
                toolbar:[
                    ['style', ['fontname','fontsize','forecolor','backcolor']],
                    ['style', ['bold','italic','strikethrough', 'clear','customLeadTags']],
                    ['font', ['superscript', 'subscript']],
                    ['para', ['ul', 'ol', 'paragraph','height']],
                    ['link', ['linkDialogShow', 'unlink']],
                    ['misc',['undo','redo']],
                ],
                buttons: {
                    customLeadTags: customLeadTags
                },
                callbacks: {
                    onChange: function (contents, $editable) {
                        emailerController.checkEmailInputs();
                    }
                }
            });

        },
        selectFolder:function(obj){
            selectedFolder = obj.value;
            emailerController.getEmailTemplates();
        },
        changeTemplate:function(type){
            document.getElementById('btn_blank').classList.remove("active");
            document.getElementById('btn_template').classList.remove("active");

            if(type == 0){
                $("#blankEmail").show();
                $("#templates").hide();
                document.getElementById('btn_blank').classList.add("active");
            }else{
                $("#blankEmail").hide();
                $("#templates").show();
                document.getElementById('btn_template').classList.add("active");
            }
        },
        getEmailTemplates:function(){
            var strUrl = BASE_URL+'/console/actions/mail/preEmails/getPreEmails.php';

            jQuery.ajax({
                url: strUrl,
                async: true,
                method: "post",
                data: {folderId:selectedFolder}
            }).done(function (data) {

                try {

                    data = JSON.parse(data);
                    document.getElementById("myTemplates").innerHTML = "";
                    if (data.length > 0){
                        for (var i = 0;i<data.length;i++){
                            emailerController.setData(data[i]);
                        }
                    }else{
                        document.getElementById("myTemplates").innerHTML = "<div style='margin-top:25px;' class=\"alert alert-info\">\n" +
                            " No Templates available in this folder, <a class=\"alert-link\" href='"+BASE_URL+"/console/categories/mail/templates.php' target='_blank'>Create One</a>.\n" +
                            "</div>";
                    }

                }catch (e) {

                }

            });
        },
        setData:function(data){

            var container = document.getElementById("myTemplates");

            var item = document.createElement("div");
            item.classList.add("myTemplateItem");
            item.setAttribute("onclick","emailerController.setTemplate("+data.id+")");


            var image = document.createElement("div");
            image.classList.add("myTemplateImage");
            image.setAttribute("style","height: 182px;width: 150px;background:white;border-bottom: 2px solid rgba(69,69,69,0.5);");


            var img = document.createElement("iframe");
            img.className = "iframePreview";
            img.src = BASE_URL+"/console/actions/mail/preEmails/getPreEmailsImage.php?id="+data.id;

            var buttonsContainer = document.createElement("div");
            buttonsContainer.classList.add("buttonsContainer");

            image.appendChild(buttonsContainer);

            image.appendChild(img);

            item.appendChild(image);

            var title = document.createElement("div");
            title.classList.add("myTemplateTitle");

            var titleText = document.createTextNode(data.title);

            title.appendChild(titleText);
            item.appendChild(title);

            container.appendChild(item);
    },
        setTemplate:function(id){

            var strUrl = BASE_URL+'/console/actions/mail/preEmails/getPreEmailById.php';

            jQuery.ajax({
                url: strUrl,
                method:"POST",
                data:{
                    id: id
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);

                //try {
                if (data != false){
                    $('.summernote').summernote('code', data.content);
                    $("#subjectEmail").val(data.subject);

                }

                emailerController.checkEmailInputs();
            });
            emailerController.changeTemplate(0);
        },
        getCustomEmail:function(type,leadId,estimateId,jobAcceptanceId,claimId){

            // type
            // 1 - estimate, 2 - credit card authorization, 3 - estimate invoice, 4 - job acceptance form, 5 - estimate to carrier,6 = claim

            if(type == undefined){return;}
            var strUrl = BASE_URL+'/console/actions/mail/preEmails/getCustomEmail.php';
            jQuery.ajax({
                url: strUrl,
                async: true,
                method: "post",
                data: {
                    type:type,
                    leadId:leadId,
                    estimateId:estimateId,
                    jobAcceptanceId:jobAcceptanceId,
                    claimId:claimId
                }
            }).done(function (data) {

                try {
                    data = JSON.parse(data);
                    $('.summernote').summernote('code', data.content);
                    $("#subjectEmail").val(data.subject);
                    emailerController.changeTemplate(0);
                    emailerController.checkEmailInputs();

                }catch (e) {
                }

            });
        },
        checkEmailInputs:function(){

            var fromEmailAccountId = document.getElementById("fromAccount").value;
            var toMail = document.getElementById("toEmail").value;
            var subject = document.getElementById("subjectEmail").value;

            // ===== Email Compose START =====
            var isOk = true;
            if (fromEmailAccountId == "") {
                isOk = false;
            }
            if (toMail.trim() == "") {
                isOk = false;
            }
            if (subject.trim() == "") {
                isOk = false;
            }
            if ($('.summernote').summernote('isEmpty')) {
                isOk = false;
            }

            if (isOk === true){
                document.getElementById("sendEmailBTN").disabled = false;
                return true;
            }else{
                document.getElementById("sendEmailBTN").disabled = true;
                return false;
            }
            // ===== Email Compose END =====
        },
        setToEmail:function (toEmail) {
            document.getElementById("toEmail").value = toEmail;
        },
        createLeadTagsDropdown:function () {

            var list = document.createElement("ul");
            list.className = "navbar-nav";

            var list1 = document.createElement("ul");
            list1.className = "navbar-nav";

            var li1 = document.createElement("li");
            li1.className = "nav-item dropdown";

            var tagsList = document.createElement("ul");
            tagsList.className = "dropdown-menu";

            var tags = {
                "general": [
                        {name:"Current date (M/D/Y)",tag:"{currentdate}"},
                        {name:"Current time (H:M)",tag:"{currenttime}"}
                    ],
                "Lead": [
                    {name:"Lead Estimate URL",tag:"{leadupdateinventory}"},
                    {name:"Job Number",tag:"{leadjobnumber}"}
                ],
                "Lead Personal Details": [
                    {name:"Lead First Name",tag:"{leadfirstname}"},
                    {name:"Lead Last Name",tag:"{leadlastname}"},
                    {name:"Lead Phone",tag:"{leadphone}"},
                    {name:"Lead Email",tag:"{leademail}"}
                ],
                "Lead Moving Details":[
                    {name:"Lead From Zip Code",tag:"{leadfromzip}"},
                    {name:"Lead From City",tag:"{leadfromcity}"},
                    {name:"Lead From State",tag:"{leadfromstate}"},
                    {name:"Lead To Zip Code",tag:"{leadtozip}"},
                    {name:"Lead To City",tag:"{leadtocity}"},
                    {name:"Lead To State",tag:"{leadtostate}"},
                    {name:"Lead Move Date",tag:"{leadmovedate}"}
                ],
                "Company Details":[
                    {name:"Company Name",tag:"{companyname}"},
                    {name:"Company Phone",tag:"{companyphone}"},
                    {name:"Company Address",tag:"{companyaddress}"},
                    {name:"Company Website",tag:"{companywebsite}"},
                    {name:"Company USDOT",tag:"{companyusdot}"},
                    {name:"Company MCC",tag:"{companymcc}"},
                    {name:"Facebook Link",tag:"{facebooklink}"},
                    {name:"Instagram Link",tag:"{instagramlink}"},
                    {name:"Twitter Link",tag:"{twitterlink}"},
                    {name:"LinkedIn Link",tag:"{linkedinlink}"}
                ],
                "user":[
                    {name:"User Name",tag:"{username}"},
                    {name:"User Email",tag:"{useremail}"},
                    {name:"User Phone",tag:"{userphone}"}
                ]
            };

                for(var category in tags){
                    var tagsInCategory = tags[category];

                    var tagItem = document.createElement("li");
                    tagItem.className = "dropdown-submenu";

                    var tagA = document.createElement("a");
                    tagA.className = "dropdown-item dropdown-toggle";
                    tagA.innerHTML = category;

                    var tagUL = document.createElement("ul");
                    tagUL.className = "dropdown-menu";

                    for(var i = 0;i<tagsInCategory.length;i++){
                        var singleTag = tagsInCategory[i];

                        var tagLI = document.createElement("li");

                        var tagA2 = document.createElement("a");
                        tagA2.className = "dropdown-item";
                        tagA2.href = "#";
                        tagA2.innerHTML = singleTag.name;
                        tagA2.setAttribute("data-tag",singleTag.tag);
                        tagA2.onclick = function changeContent(tagA2){
                            var selectedTag = this.getAttribute("data-tag");

                            $(".summerNote").summernote('editor.restoreRange');
                            $(".summerNote").summernote('editor.focus');
                             $(".summerNote").summernote('editor.insertText', selectedTag);

                        };


                        tagLI.appendChild(tagA2);
                        tagUL.appendChild(tagLI);
                    }
                    tagItem.appendChild(tagA);
                    tagItem.appendChild(tagUL);
                    tagsList.appendChild(tagItem);

                }


            li1.appendChild(tagsList);
            list1.appendChild(li1);
            list.appendChild(list1);

            $('.leadTags').append(list);


        }
    }

</script>