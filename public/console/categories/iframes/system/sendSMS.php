<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

$organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);

$phoneNumbers = $organizationTwilio->getPhoneNumbers();
foreach($phoneNumbers as &$phoneNumber){
    $phoneNumber["prettyNumber"] = $organizationTwilio->prettifyPhone($phoneNumber["number"]);
}

if(isset($_GET['leadsIds'])){
    $leadsIds = array_filter($_GET['leadsIds']);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");

    $leads = new leads($bouncer["credentials"]["orgId"]);
    $leadsPhoneNumbers = $leads->getLeadsPhoneNumber($leadsIds);
}

if(isset($_GET['toPhoneNumber'])){
    $toPhoneNumber = $_GET['toPhoneNumber'];
}

$text = "";
if(isset($_GET['text'])){
    $text = $_GET['text'];
}
?>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia-new/HTML5_Full_Version/css/animate.css" rel="stylesheet">

<!-- Ladda style -->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>

<style>
    #sendSMSSwalDiv .badge-secondary-custom {
        background-color: #d1dade;
        color: #5e5e5e;
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-size: 11px;
        font-weight: 600;
        padding-bottom: 4px;
        padding-left: 6px;
        padding-right: 6px;
        text-shadow: none;
    }
    .swal-BTN-confirm{
        background-color: #7cd1f9;
        color: #fff;
        border: none;
        box-shadow: none;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        padding: 10px 24px;
        margin: 0;
        cursor: pointer;
    }
    .swal-BTN-confirm:active {
        background-color: #70bce0 !important;
    }
    .swal-BTN-confirm:not([disabled]):hover {
        background-color: #78cbf2 !important;
    }
    .swal-BTN-confirm:focus {
        outline: none;
        box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(43, 114, 165, .29);
    }
    .swal-BTN-confirm[disabled] {
        opacity: .5;
        cursor: default;
    }

    .swal-BTN-cancel{
        color: #555;
        background-color: #efefef;
        border: none;
        box-shadow: none;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        padding: 10px 24px;
        margin: 0;
        cursor: pointer;
    }
    .swal-BTN-cancel:active {
        background-color: #dedede !important;
    }
    .swal-BTN-cancel:not([disabled]):hover {
        background-color: #dedede !important;
    }
    .swal-BTN-cancel:focus {
        outline: none;
        box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(43, 114, 165, .29);
    }

    #chartdiv {
        width: 100%;
        height: 300px;
    }
    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }

    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }
</style>

<div id="sendSMSSwalDiv" style="visibility: hidden;color: #676a6c;">
    <div id="page1" style="padding:4px;position: absolute;width: 100%;height: 100%; overflow-x: hidden;overflow-y: hidden;">
        <h3 style="text-align: left;font-size: 13px;">From phone number</h3>
        <button style="position:fixed;top:-8px;right:4px;font-size: 25px;outline: none;" type="button" class="close" onclick="parent.swal.close();"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <select id="sendSMSModalFromPhoneNumberID" class="form-control" onchange="SMScontroller.checkFields()" style="margin-bottom: 13px;" <?php if(count($phoneNumbers) == 0){ ?> disabled <?php } ?>>
            <?php
                if(count($phoneNumbers) == 0){
                    ?>
                    <option value="">No phone numbers</option>
                    <?php
                }else{
                    foreach ($phoneNumbers as &$phoneNumber){
                        ?>
                        <option value="<?= $phoneNumber["id"] ?>"><?= $phoneNumber["prettyNumber"] ?></option>
                        <?php
                    }
                }
            ?>
        </select>

        <?php if(count($phoneNumbers) == 0){ ?>
            <div style="background-color: rgb(254, 250, 227);padding: 17px;border: 1px solid rgb(240, 225, 161);display: block;margin-top: 13px;margin-bottom: 13px;text-align: center;color: rgb(97, 83, 78);">
                <a target="_blank" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/mailSettings.php" style="color: unset;text-decoration: underline;">Click here to assign a new phone number</a>
            </div>
        <?php } ?>

        <?php if(isset($leadsIds)){ ?>
        <h3 style="text-align: left;font-size: 13px;">
            To leads
            <span style="float: right"><?php echo count($leadsPhoneNumbers); ?> leads selected</span>
        </h3>

        <select class="form-control m-b" multiple="true" id="sendSMSModalToLeads" style="margin-bottom: 13px;height: 139px;"  <?php if(count($leadsPhoneNumbers) == 0){ ?> disabled <?php }; ?>>
            <?php
            if(count($leadsPhoneNumbers) == 0){
                ?>
                <option value="">No leads selected</option>
                <?php
            }else{
                foreach ($leadsPhoneNumbers as $leadsPhoneNumber){
                    ?>
                    <option value="<?php echo $leadsPhoneNumber["leadId"]; ?>">#<?php echo $leadsPhoneNumber["jobNumber"]; ?> - <?php echo $leadsPhoneNumber["phone"]; ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <?php }elseif(isset($toPhoneNumber)){
            ?>
            <h3 style="text-align: left;font-size: 13px;">To phone number</h3>
            <input class="form-control" id="sendSMSModalToPhoneNumber" style="margin-bottom: 13px;" placeholder="To phone number" value="<?php echo $toPhoneNumber; ?>" onkeyup="SMScontroller.checkFields()">
        <?php } ?>


        <h3 style="text-align: left;font-size: 13px;">
            Message
            <a href="#" onclick="SMSTemplatesController.showTemplates()" style="font-size: 13px;float: right;color: cornflowerblue;text-decoration: none;">Apply template</a>
        </h3>

        <textarea id="sendSMSModalMessage" class="form-control" style="text-align: left;font-size: 81%;padding: 11px;height: 139px;" placeholder="Enter text.." onkeyup="SMScontroller.checkFields()"><?php echo $text; ?></textarea>

        <?php if(isset($leadsIds)){ ?>
        <div style="text-align: left;margin-top: 13px;margin-bottom: 13px;">
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[FirstName]');">First name</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[LastName]');">Last name</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[CompanyName]');">Company Name</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[inventorylink]');">Update inventory link</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[FromState]');">From state</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[ToState]');">To state</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[FromCity]');">From city</span>
            <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMScontroller.updateMessageArea('[ToCity]');">To city</span>
        </div>
        <?php } ?>


            <footer style="position: absolute;bottom: 0;right: 0;width: 100%;">
                <button style="display: none;" type="button" id="backBTN" class="swal-BTN-cancel" onclick="SMScontroller.callbackFN();">Back</button>
                <button style="float: right;" type="button" id="sendSMSBTN" disabled class="swal-BTN-confirm" onclick="SMScontroller.sendSMS();">Send SMS</button>
            </footer>

    </div>
    <div id="page2" class="animated bounceOutRight" style="padding:4px;position: absolute;width: 100%;height: 100%; overflow-x: hidden; display: none;">
        <?php
            require_once( $_SERVER['LOCAL_NL_PATH']."/console/categories/iframes/system/sendSMS_templates.php");
        ?>
    </div>
</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/core.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/charts.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/themes/animated.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<script>
    Pace.on("hide",  function(pace){
        document.getElementById("sendSMSSwalDiv").style.visibility = "";
    });

    // Override the default functions
    SMSTemplatesController.showTemplates = function () {
        $('#page2').css("display", "");
        $('#page2').removeAttr('class').attr('class', '');
        $('#page2').addClass('animated');
        $('#page2').addClass("slideInRight");

        $('#page1').removeAttr('class').attr('class', '');
        $('#page1').addClass('animated');
        $('#page1').addClass("slideOutLeft");
    };
    SMSTemplatesController.backFromTemplates = function () {
        $('#page2').removeAttr('class').attr('class', '');
        $('#page2').addClass('animated');
        $('#page2').addClass("bounceOutRight");

        setTimeout(function () {
            $('#page2').css("display", "none");
        }, 1000);

        $('#page1').removeAttr('class').attr('class', '');
        $('#page1').addClass('animated');
        $('#page1').addClass("bounceInLeft");
    };
    SMSTemplatesController.setTemplate = function (data) {
        var txtarea = document.getElementById("sendSMSModalMessage");
        txtarea.innerHTML = "";
        txtarea.value = "";

        SMScontroller.updateMessageArea(data);
        SMSTemplatesController.backFromTemplates();
    }

    var SMScontroller = {
        callbackFN:null,
        setupSendCallback:function(callbackFN){
            if(callbackFN != undefined){
                SMScontroller.callbackFN = callbackFN;
                document.getElementById("backBTN").style.display = "";
            }else{
                document.getElementById("backBTN").style.display = "none";
            }
        },
        checkFields:function () {
            var fromPhoneNumberID = false;
            var toInput = false;
            var msg = false;

            fromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
            <?php if(isset($leadsIds)){
                ?>

                    var sendSMSModalToLeads = document.getElementById("sendSMSModalToLeads").options;
                    var leadsIds = [];
                    for(var i = 0;i<sendSMSModalToLeads.length;i++){
                        leadsIds.push(sendSMSModalToLeads[i].value);
                    }

                    if(leadsIds == false || leadsIds.length == 0){
                        toInput = false;
                    }else{
                        toInput = true;
                    }

                <?php }elseif(isset($toPhoneNumber)){
                ?>
                toInput = document.getElementById("sendSMSModalToPhoneNumber").value;
                <?php } ?>
            msg = document.getElementById("sendSMSModalMessage").value;

            if(fromPhoneNumberID != "" && toInput != "" && msg != "" && fromPhoneNumberID != false && toInput != false && msg != false){
                $("#sendSMSBTN").attr('disabled', false);
                return true;
            }else{
                $("#sendSMSBTN").attr('disabled', 'disabled');
                return false;
            }
            return false;

        },
        updateMessageArea: function(text) {
            var txtarea = document.getElementById("sendSMSModalMessage");
            if (!txtarea) {
                return;
            }

            var scrollPos = txtarea.scrollTop;
            var strPos = 0;
            var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
                "ff" : (document.selection ? "ie" : false));
            if (br == "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                strPos = range.text.length;
            } else if (br == "ff") {
                strPos = txtarea.selectionStart;
            }

            var front = (txtarea.value).substring(0, strPos);
            var back = (txtarea.value).substring(strPos, txtarea.value.length);
            txtarea.value = front + text + back;
            strPos = strPos + text.length;
            if (br == "ie") {
                txtarea.focus();
                var ieRange = document.selection.createRange();
                ieRange.moveStart('character', -txtarea.value.length);
                ieRange.moveStart('character', strPos);
                ieRange.moveEnd('character', 0);
                ieRange.select();
            } else if (br == "ff") {
                txtarea.selectionStart = strPos;
                txtarea.selectionEnd = strPos;
                txtarea.focus();
            }

            txtarea.scrollTop = scrollPos;
            SMScontroller.checkFields();
        },
        setMessageText:function (text) {
            document.getElementById("sendSMSModalMessage").value = text;
        },
        handleResponse: function (data) {

            l.ladda("stop");
            try {
                data = JSON.parse(data);

                if(data.balance == false){
                    parent.limitReached("balance");
                    return;
                }

                if(data.responses.length == 1){
                    parent.swal.close();
                    if(data.responses[0].status == true){
                        $(parent)[0].swal("Message sent","", "success");
                    }else{
                        $(parent)[0].swal("Oops", "Your message was not sent. Please try again later.", "error");
                    }
                    return;
                }


                // =========== CONVERT RESPONSE ==============
                var responseTypes = [];
                var responses = [];

                var singleResponseType = [];
                singleResponseType["name"] = "Sent";
                singleResponseType["color"] = "#28a745";

                responseTypes[singleResponseType["name"]] = (singleResponseType);

                var singleResponseType = [];
                singleResponseType["name"] = "Failed";
                singleResponseType["color"] = "#dc3545";

                responseTypes[singleResponseType["name"]] = (singleResponseType);


                for (var i = 0; i < data.responses.length; i++) {
                    var response = {};
                    response.data = data.responses[i].toNumber;
                    if (data.responses[i].status == true) {
                        response.responseType = "Sent";
                    }else{
                        response.responseType = "Failed";
                    }
                    responses.push(response);
                }

                var allData = [];
                allData.responses = responses;
                allData.responseTypes = responseTypes;
                // =========== CONVERT RESPONSE ==============



                parent.openSendingStats(allData);
            }catch (e) {

            }
        },
    }



    <?php if(isset($leadsIds)){ ?>
    SMScontroller.sendSMS = function(){
        l.ladda("start");
        var fromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
        var msg = document.getElementById("sendSMSModalMessage").value;
        var sendSMSModalToLeads = document.getElementById("sendSMSModalToLeads").options;
        var leadsIds = [];
        for(var i = 0;i<sendSMSModalToLeads.length;i++){
            leadsIds.push(sendSMSModalToLeads[i].value);
        }


        var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/twilio/sendSMSToLeads.php', strReturn = "";

        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                fromPhoneNumberId:fromPhoneNumberID,
                leadsIds:leadsIds,
                msg:msg
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data)
        {
            SMScontroller.handleResponse(data);
        });

    };
    <?php } ?>

    <?php if(isset($toPhoneNumber)){ ?>
    SMScontroller.sendSMS = function(){
        l.ladda("start");
        var fromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
        var toPhoneNumber = document.getElementById("sendSMSModalToPhoneNumber").value;
        var msg = document.getElementById("sendSMSModalMessage").value;

        var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/twilio/sendSMS.php', strReturn = "";

        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                fromPhoneNumberId:fromPhoneNumberID,
                toPhoneNumber:toPhoneNumber,
                msg:msg
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            SMScontroller.handleResponse(data);
        });
    };
    <?php } ?>

    var l;
    $( document ).ready(function() {
        SMScontroller.checkFields();
        l = $('#sendSMSBTN').ladda();
    });
</script>
