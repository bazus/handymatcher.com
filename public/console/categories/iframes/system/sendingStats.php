<?php
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
?>

<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>"</script>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap4/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

<style>
    #chartdiv {
        width: 100%;
        height: 300px;
    }
    .swal-BTN-confirm{
        background-color: #7cd1f9;
        color: #fff;
        border: none;
        box-shadow: none;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        padding: 10px 24px;
        margin: 0;
        cursor: pointer;
    }
    .swal-BTN:active {
        background-color: #70bce0 !important;
    }
    .swal-BTN:not([disabled]):hover {
        background-color: #78cbf2 !important;
    }
    .swal-BTN:focus {
        outline: none;
        box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(43, 114, 165, .29);
    }
    .swal-BTN[disabled] {
        opacity: .5;
        cursor: default;
    }



    .pace {
        -webkit-pointer-events: none;
        pointer-events: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        position: fixed;
        height: 140px;
        width: 140px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .pace-inactive {
        display: none;
    }


    .pace .pace-activity {
        display: block;
        position: fixed;
        z-index: 2000;
        width: 140px;
        height: 140px;
        border: solid 2px transparent;
        border-top-color: #29d;
        border-left-color: #29d;
        border-radius: 70px;
        -webkit-animation: pace-spinner 400ms linear infinite;
        -moz-animation: pace-spinner 400ms linear infinite;
        -ms-animation: pace-spinner 400ms linear infinite;
        -o-animation: pace-spinner 400ms linear infinite;
        animation: pace-spinner 400ms linear infinite;
    }

    @-webkit-keyframes pace-spinner {
        0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes pace-spinner {
        0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes pace-spinner {
        0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes pace-spinner {
        0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
        100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @keyframes pace-spinner {
        0% { transform: rotate(0deg); transform: rotate(0deg); }
        100% { transform: rotate(360deg); transform: rotate(360deg); }
    }

</style>
<div class="animated bounceOutRight" id="wrapper" style="padding:4px;position: absolute;width: 100%;height: 100%; overflow-x: hidden;visibility: hidden;">
    <div id="chartdiv"></div>

    <table class="table table-bordered" style="margin-top: 13px;">
        <tbody id="responses"></tbody>
    </table>
</div>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap4/bootstrap.min.js"></script>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/core.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/charts.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/themes/animated.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<script>


    Pace.on("hide",  function(pace){
        document.getElementById("wrapper").style.visibility = "";
    });

    function init(data){

        for(var i = 0;i<data.responses.length;i++){
            var response = data.responses[i];
            var responseType = data.responseTypes[response.responseType];

            if(responseType.total == undefined){
                responseType.total = 1;
            }else{
                responseType.total++;
            }

            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.innerHTML = response.data;
            tr.appendChild(td);

            var td = document.createElement("td");
            td.innerHTML = "<span class=\"badge\" style='color: #fff; margin-right:3px;margin-bottom:3px;background-color: "+responseType.color+";'>"+responseType.name+"</span>";

            if(response.text != undefined && response.text != ""){
                td.innerHTML += "<span class=\"badge\" style='color: #fff;background-color: "+responseType.color+";'>"+response.text+"</span>";
            }

            tr.appendChild(td);

            document.getElementById("responses").appendChild(tr);
        }

        var chartData = [];
        for(var responsesType in data.responseTypes){

            if (data.responseTypes.hasOwnProperty(responsesType)) {

                var singleResponseType = {};
                singleResponseType.status = responsesType;
                singleResponseType.color = am4core.color(data.responseTypes[responsesType]["color"]);

                if(data.responseTypes[responsesType]["total"] == undefined){
                    singleResponseType.total = 0;
                }else{
                    singleResponseType.total = data.responseTypes[responsesType]["total"];
                }

                console.log(singleResponseType);
                chartData.push(singleResponseType);
            }
        }

/*
        var chartData = [{
            "status": "Sent",
            "sents": 10,
            "color": am4core.color("#28a745")
        }, {
            "status": "Failed",
            "sents": 1,
            "color": am4core.color("#dc3545")

        }];
*/

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.PieChart);

        // Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "total";
        pieSeries.dataFields.category = "status";

        // Let's cut a hole in our Pie chart the size of 30% the radius
        chart.innerRadius = am4core.percent(30);

        // Put a thick white border around each Slice
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        pieSeries.slices.template
            // change the cursor on hover to make it apparent the object can be interacted with
            .cursorOverStyle = [
            {
                "property": "cursor",
                "value": "pointer"
            }
        ];

        pieSeries.slices.template.propertyFields.fill = "color";

        pieSeries.ticks.template.disabled = true;
        pieSeries.alignLabels = false;
        pieSeries.labels.template.text = "";
        pieSeries.labels.template.radius = am4core.percent(-40);
        pieSeries.labels.template.fill = am4core.color("white");

        // Create a base filter effect (as if it's not there) for the hover to return to
        var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        shadow.opacity = 0;

        // Create hover state
        var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

        // Slightly shift the shadow and make it more prominent on hover
        var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        hoverShadow.opacity = 0.7;
        hoverShadow.blur = 5;

        // Add a legend
        chart.legend = new am4charts.Legend();

        chart.data = chartData;

    }
</script>