<style>
    #templatesArea{
        padding: 0px 10px 0px 10px;
        height: 543px;
        overflow: scroll;
        border-bottom: 1px solid gainsboro;
    }
    .templateTextarea-closed{
        height: 120px !important;
        margin-top: 13px;
        cursor: pointer;
        resize: none;
    }
    .templateTextarea-open{
        height: 120px !important;
        margin-top: 13px;
        resize: none;
    }

    .templateTextarea-BTN{
        float: right;
        font-size: 13px;
        margin-top: -20px;
        margin-right: 3px;
        position: relative;
    }

</style>
<div id="templatesArea"></div>

<footer>
    <button  type="button" style="position: absolute;bottom: 0;left: 0;" class="swal-BTN swal-BTN-confirm" onclick="SMSTemplatesController.backFromTemplates();">Back</button>
    <button  type="button" style="position: absolute;bottom: 0;right: 0;" class="swal-BTN swal-BTN-confirm" onclick="SMSTemplatesController.newTemplate(0,'',true);">Create template</button>
</footer>

<script>
    var SMSTemplatesController = {
        showTemplates:function(){
            // This function needs to be overwritten
            return false;
        },
        backFromTemplates:function(){
            // This function needs to be overwritten
            return false;
        },
        getTemplates:function() {


            var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/marketing/SMS/getTemplates.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "GET",
                data:{},
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                var templatesArea = document.getElementById("templatesArea");
                templatesArea.innerHTML = "";

                try{
                    data = JSON.parse(data);
                    for(var i = 0;i<data.length;i++){
                        SMSTemplatesController.newTemplate(data[i].id,data[i].text,false);
                    }
                }catch (e) {

                }
            });

        },
        newTemplate:function(templateId,text,isOpen){

            var templatesAreaElement = document.getElementById("templateTextArea"+templateId);
            if(templatesAreaElement != null){
                document.getElementById("templateTextArea"+templateId).focus();
                return;
            }

            var templatesArea = document.getElementById("templatesArea");
            var singleTemplate = document.createElement("div");
            singleTemplate.id = "singleTemplate"+templateId;

            var ta = document.createElement("textarea");
            ta.id = "templateTextArea"+templateId;
            ta.setAttribute("placeholder","Enter text..");
            ta.innerHTML = text;
            if(isOpen == true){
                ta.className = "form-control templateTextarea-open";
            }else{
                ta.setAttribute("readonly",true);
                ta.setAttribute("onclick","SMSTemplatesController.setTemplate(this.value)");
                ta.className = "form-control templateTextarea-closed";
            }

            var editBtn = document.createElement("a");
            editBtn.href = "javascript:void(0)";
            editBtn.id = "templateBTNEdit"+templateId;
            editBtn.className = "templateTextarea-BTN";
            editBtn.setAttribute("onclick","SMSTemplatesController.editTemplate("+templateId+")");
            editBtn.innerHTML = "Edit";
            if(isOpen == true) {
                editBtn.setAttribute("style","display: none;");
            }

            var saveBtn = document.createElement("a");
            saveBtn.href = "javascript:void(0)";
            saveBtn.id = "templateBTNSave"+templateId;
            saveBtn.className = "templateTextarea-BTN";
            saveBtn.setAttribute("onclick","SMSTemplatesController.saveTemplate("+templateId+")");
            saveBtn.innerHTML = "Save";
            if(isOpen != true) {
                saveBtn.setAttribute("style","display: none;");
            }

            var deleteBtn = document.createElement("a");
            deleteBtn.href = "javascript:void(0)";
            deleteBtn.id = "templateBTNDelete"+templateId;
            deleteBtn.className = "templateTextarea-BTN";
            deleteBtn.setAttribute("onclick","SMSTemplatesController.deleteTemplate("+templateId+")");
            deleteBtn.setAttribute("style","color: #3c3c3c;top: -101px;right: 3px");
            deleteBtn.innerHTML = "x";
            if(isOpen != true) {
                deleteBtn.style.display = "none";
            }

            var tagsDiv = document.createElement("div");
            tagsDiv.setAttribute("style","text-align: left;margin-top: 13px;margin-bottom: 13px;");
            tagsDiv.id = "tagsDivForTemplate"+templateId;
            tagsDiv.innerHTML = '<span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[FirstName]\');">First Name</span> <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[LastName]\');">Last Name</span><span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[CompanyName]\');">Company Name</span> <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[inventorylink]\');">Update inventory link</span> <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[FromState]\');">From state</span> <span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[ToState]\');">To state</span><span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[FromCity]\');">From city</span><span class="badge badge-secondary-custom" style="cursor:pointer;margin-right: 3px;" onclick="SMSTemplatesController.updateTemplateText('+templateId+',\'[ToCity]\');">To city</span>';
            if(isOpen != true) {
                tagsDiv.style.display = "none";
            }
            singleTemplate.appendChild(ta);
            singleTemplate.appendChild(editBtn);
            singleTemplate.appendChild(saveBtn);
            singleTemplate.appendChild(deleteBtn);
            singleTemplate.appendChild(tagsDiv);
            templatesArea.appendChild(singleTemplate);
        },
        deleteTemplate:function(templateId) {

            var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/marketing/SMS/deleteTemplate.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    templateId:templateId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data)
            {
                try{
                    data = JSON.parse(data);

                    if(data == true){
                        var elem = document.getElementById("singleTemplate"+templateId);
                        return elem.parentNode.removeChild(elem);
                    }
                }catch (e) {

                }

                // Reload templates
                SMSTemplatesController.getTemplates();
            });


        },
        saveTemplate:function(templateId) {
            var templateText = document.getElementById("templateTextArea"+templateId).value;

            if(templateText == ""){
                document.getElementById("templateTextArea"+templateId).focus();
                return;
            }

            var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/marketing/SMS/saveTemplate.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    templateId:templateId,
                    templateText:templateText
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data)
            {
                document.getElementById("templateBTNSave"+templateId).style.display = "none";
                document.getElementById("tagsDivForTemplate"+templateId).style.display = "none";
                document.getElementById("templateBTNDelete"+templateId).style.display = "none";
                document.getElementById("templateBTNEdit"+templateId).style.display = "";

                document.getElementById("templateTextArea"+templateId).classList.remove("templateTextarea-open");
                document.getElementById("templateTextArea"+templateId).classList.add("templateTextarea-closed");
                document.getElementById("templateTextArea"+templateId).setAttribute("readonly",true);
                document.getElementById("templateTextArea"+templateId).setAttribute("onclick","SMSTemplatesController.setTemplate(this.value)");

                // Reload templates
                SMSTemplatesController.getTemplates();
            });

        },
        editTemplate:function(templateId) {
            document.getElementById("templateBTNEdit"+templateId).style.display = "none";
            document.getElementById("tagsDivForTemplate"+templateId).style.display = "";
            document.getElementById("templateBTNSave"+templateId).style.display = "";
            document.getElementById("templateBTNDelete"+templateId).style.display = "";

            document.getElementById("templateTextArea"+templateId).classList.remove("templateTextarea-closed");
            document.getElementById("templateTextArea"+templateId).classList.add("templateTextarea-open");
            document.getElementById("templateTextArea"+templateId).removeAttribute("readonly");
            document.getElementById("templateTextArea"+templateId).removeAttribute("onclick");
        },
        setTemplate: function (data) {
            var txtarea = document.getElementById("sendSMSModalMessage");
            txtarea.innerHTML = "";
            txtarea.value = "";

            //SMScontroller.updateMessageArea(data);

            //SMScontroller.templates.backFromTemplates();
        },
        updateTemplateText: function(templateId,text) {
            var txtarea = document.getElementById("templateTextArea"+templateId);
            if (!txtarea) {
                return;
            }

            var scrollPos = txtarea.scrollTop;
            var strPos = 0;
            var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
                "ff" : (document.selection ? "ie" : false));
            if (br == "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                strPos = range.text.length;
            } else if (br == "ff") {
                strPos = txtarea.selectionStart;
            }

            var front = (txtarea.value).substring(0, strPos);
            var back = (txtarea.value).substring(strPos, txtarea.value.length);
            txtarea.value = front + text + back;
            strPos = strPos + text.length;
            if (br == "ie") {
                txtarea.focus();
                var ieRange = document.selection.createRange();
                ieRange.moveStart('character', -txtarea.value.length);
                ieRange.moveStart('character', strPos);
                ieRange.moveEnd('character', 0);
                ieRange.select();
            } else if (br == "ff") {
                txtarea.selectionStart = strPos;
                txtarea.selectionEnd = strPos;
                txtarea.focus();
            }

            txtarea.scrollTop = scrollPos;
            SMScontroller.checkFields();
        }
    }

    $( document ).ready(function() {
        SMSTemplatesController.getTemplates();
    });
</script>