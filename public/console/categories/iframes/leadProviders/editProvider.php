<?php
$isCalledFromModal = true;
if(isset($_GET['id'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");
    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $provider = $leadProviders->getProvider($_GET['id']);
}else{
    header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leadProviders.php");
    exit;
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-body">
                <div class="hr-line-dashed"></div>
                <div id="leadProviders" style="">
                    <form method="post" class="form-horizontal">
                        <div class="form-group"><label class="col-sm-4 control-label">Unique Key</label>
                            <div class="col-sm-8"><input type="text" name="providerName" id="providerKey" onkeyup="checkForm()" placeholder="Provider Key" class="form-control" value="<?= $provider['uniqueKey'] ?>" disabled="true"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8"><input type="text" name="providerName" id="providerName" onkeyup="checkForm()" placeholder="Provider Name" class="form-control" value="<?= $provider['providerName'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Address</label>
                            <div class="col-sm-8"><input type="email" name="emailAddress" id="emailAddress" onkeyup="checkForm()" placeholder="Email Address" class="form-control" value="<?= $provider['providerEmail'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Website</label>
                            <div class="col-sm-8"><input type="text" name="website" id="website" onkeyup="checkForm()" placeholder="Website Address" class="form-control" value="<?= $provider['providerWebsite'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Address</label>
                            <div class="col-sm-8"><input type="text" name="address" id="address" onkeyup="checkForm()" placeholder="Address" class="form-control" value="<?= $provider['providerAddress'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Phone</label>
                            <div class="col-sm-8"><input type="text" name="phone" id="phone" onkeyup="checkForm()" placeholder="Phone Number" class="form-control" value="<?= $provider['providerPhone'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Fax</label>
                            <div class="col-sm-8"><input type="text" name="fax" id="fax" onkeyup="checkForm()" placeholder="Fax Number" class="form-control" value="<?= $provider['providerFax'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Contact Name</label>
                            <div class="col-sm-8"><input type="text" name="contactName" id="contactName" onkeyup="checkForm()" placeholder="Contact Name" class="form-control" value="<?= $provider['providerContactName'] ?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Price Per Lead ($)</label>
                            <div class="col-sm-8"><input type="tel" name="pricePerLead" id="pricePerLead" onkeyup="checkForm()" placeholder="Price Per Lead ($)" class="form-control" value="<?= $provider['pricePerLead'] ?>"></div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="updateProvider()" id="updateProvider" disabled>Update</button>
            </div>
        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();


        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            var pricePerLead = document.getElementById("pricePerLead");
            pricePerLead.addEventListener(event, function() {
                if (returnCurrencyFromString(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });

        function returnCurrencyFromString(value) {
            return /^\d*[.,]?\d{0,2}$/.test(value);
        }
    });

    function updateProvider(){
        l.ladda( 'start' );

        var providerName = document.getElementById('providerName').value;
        var emailAddress = document.getElementById('emailAddress').value;
        var website = document.getElementById('website').value;
        var address = document.getElementById('address').value;
        var phone = document.getElementById('phone').value;
        var fax = document.getElementById('fax').value;
        var contactName = document.getElementById('contactName').value;
        var pricePerLead = document.getElementById('pricePerLead').value;


        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leadProviders/updateProvider.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                providerName:providerName,
                emailAddress:emailAddress,
                website:website,
                address:address,
                phone:phone,
                fax:fax,
                contactName:contactName,
                pricePerLead:pricePerLead,
                id:<?= $provider['id'] ?>
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda('stop');

            try{
                data = JSON.parse(data);

                $('#myCustomModal').modal('hide');

                if(data == true){
                    swal({title:"Success!", text:"Your Provider has been added",icon: "success",
                        buttons: {
                            ok:"Ok"
                        }
                    }).then((isConfirm)=>{
                        if (isConfirm) {
                            location.reload();
                        }
                    });
                }else{
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }
            }catch (e){
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }

        });
    };

    function checkForm(){
        document.getElementById('updateProvider').disabled = true;

        var providerName = document.getElementById('providerName').value;
        var emailAddress = document.getElementById('emailAddress').value;

        if(providerName != "" && emailAddress != ""){
            document.getElementById('updateProvider').disabled = false;
        }
    }
    checkForm();
</script>