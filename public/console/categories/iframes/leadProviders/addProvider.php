<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<style>
    .icon-NM{
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        background-image: url(https://d28bwzd6sq9xgl.cloudfront.net/images/favicon.ico);
        background-position: center center;
        height: 14px;
        width: 14px;
    }
    #NetworkMoving{
        text-align: center;
    }
    #NetworkMoving .networkLeadsContainer{
        display: inline-block;
        width: 100%;
    }
    #NetworkMoving .networkLeadsContainer .networkMovingImage{
        width: 100px;
        margin-left: 18%;
    }
    #NetworkMoving .networkLeadsContainer .networkMovingP{
        text-align: left;
        display: inline-block;
        font-size: 19px;
        font-weight: bold;
        color: #307480;
        width: 50%;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="btn-group">
                    <button class="btn my-btn btn-white active" id="changeType1" type="button" onclick="changeType(1)">Custom</button>
                    <button class="btn my-btn btn-white" id="changeType2" type="button" onclick="changeType(2)"><i class="icon-NM"></i> Network Moving</button>
                </div>
            </div>
            <div class="modal-body">
                <div id="leadProviders" style="">
                    <form method="post" class="form-horizontal">

                        <div class="form-group"><label class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8"><input type="text" name="providerName" id="providerName" onkeyup="checkForm()" placeholder="Provider Name" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Address</label>
                            <div class="col-sm-8"><input type="email" name="emailAddress" id="emailAddress" onkeyup="checkForm()" placeholder="Email Address" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Website</label>
                            <div class="col-sm-8"><input type="text" name="website" id="website" onkeyup="checkForm()" placeholder="Website Address" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Address</label>
                            <div class="col-sm-8"><input type="text" name="address" id="address" onkeyup="checkForm()" placeholder="Address" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Phone</label>
                            <div class="col-sm-8"><input type="text" name="phone" id="phone" onkeyup="checkForm()" placeholder="Phone Number" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Fax</label>
                            <div class="col-sm-8"><input type="text" name="fax" id="fax" onkeyup="checkForm()" placeholder="Fax Number" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Contact Name</label>
                            <div class="col-sm-8"><input type="text" name="contactName" id="contactName" onkeyup="checkForm()" placeholder="Contact Name" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Price Per Lead ($)</label>
                            <div class="col-sm-8"><input type="tel" name="pricePerLead" id="pricePerLead" onkeyup="checkForm()" placeholder="Price Per Lead ($)" class="form-control"></div>
                        </div>
                    </form>
                </div>
                <div id="NetworkMoving" style="display: none;">
                    <a target="_blank" class="networkLeadsContainer" href="https://www.networkmoving.com/partners.php?ref=NetworkLeads">
                        <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/home/images/Network%20Moving.png" class="pull-left networkMovingImage">
                        <p class="networkMovingP">
                            New to network moving?<br>
                            Click here and join our network moving partners<br>
                            And start receiving leads
                        </p>
                    </a><br>
                    <small>Already a Partner? <a onclick="createNM()" style="color: #676a6c;">Click Create</a></small>
                </div>
            </div>
            <div class="modal-footer">
                <div id="customButtons">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="createProvider()" id="createProvider" disabled>Create</button>
                </div>
                <div id="NMButtons" style="display: none;">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="createNM()" id="createProvider">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();

        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            var pricePerLead = document.getElementById("pricePerLead");
            pricePerLead.addEventListener(event, function() {
                if (returnCurrencyFromString(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });

        function returnCurrencyFromString(value) {
            return /^\d*[.,]?\d{0,2}$/.test(value);
        }
    });

    function createProvider(){
        l.ladda( 'start' );

        var providerName = document.getElementById('providerName').value;
        var emailAddress = document.getElementById('emailAddress').value;
        var website = document.getElementById('website').value;
        var address = document.getElementById('address').value;
        var phone = document.getElementById('phone').value;
        var fax = document.getElementById('fax').value;
        var contactName = document.getElementById('contactName').value;
        var pricePerLead = document.getElementById('pricePerLead').value;


        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leadProviders/createProvider.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                providerName:providerName,
                emailAddress:emailAddress,
                website:website,
                address:address,
                phone:phone,
                fax:fax,
                contactName:contactName,
                pricePerLead:pricePerLead
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda('stop');

            try{
                data = JSON.parse(data);

                $('#myCustomModal').modal('hide');

                if(data == true){
                    swal({title:"Success!", text:"Your Provider has been added",icon: "success",
                        buttons: {
                            ok:"Ok"
                        }
                    }).then((x)=>{
                        window.location = window.location.href.split("?")[0];
                    });

                }else{
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }
            }catch (e){
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }

        });
    };

    function checkForm(){
        document.getElementById('createProvider').disabled = true;

        var providerName = document.getElementById('providerName').value;
        var emailAddress = document.getElementById('emailAddress').value;

        if(providerName != "" && emailAddress != ""){
            document.getElementById('createProvider').disabled = false;
        }
    }
    checkForm();

    function changeType(type){
        try{
            document.getElementById("changeType1").classList.remove("active");
            document.getElementById("changeType2").classList.remove("active");
        } catch (e) {}
        if (type == 1){
            document.getElementById("leadProviders").style.display = "";
            document.getElementById("NetworkMoving").style.display = "none";
            document.getElementById("customButtons").style.display = "";
            document.getElementById("NMButtons").style.display = "none";
            document.getElementById("changeType1").classList.add("active");
        }else if (type == 2){
            document.getElementById("leadProviders").style.display = "none";
            document.getElementById("NetworkMoving").style.display = "";
            document.getElementById("customButtons").style.display = "none";
            document.getElementById("NMButtons").style.display = "";
            document.getElementById("changeType2").classList.add("active");
        }
    }
    function createNM(){
        l.ladda( 'start' );

        var providerName = "Network Moving";
        var emailAddress = "support@networkmoving.com";
        var website = "https://www.networkmoving.com";
        var address = "";
        var phone = "(866) 277 2073";
        var fax = "";
        var contactName = "Adir";
        var pricePerLead = "";
        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leadProviders/createProvider.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                providerName:providerName,
                emailAddress:emailAddress,
                website:website,
                address:address,
                phone:phone,
                fax:fax,
                contactName:contactName,
                pricePerLead:pricePerLead
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda('stop');

            try{
                data = JSON.parse(data);

                $('#myCustomModal').modal('hide');

                if(data == true){
                    swal({title:"Success!", text:"Your Provider has been added",icon: "success",
                        buttons: {
                            ok:"Ok"
                        }
                    }).then((x)=>{
                        window.location = window.location.href.split("?")[0];
                });

                }else{
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }
            }catch (e){
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }

        });
    }

</script>