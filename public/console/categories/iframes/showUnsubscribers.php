<?php
$pagePermissions = array(false,true,true,true);
// todos change permissions by package
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");


$organization = new organization($bouncer["credentials"]["orgId"]);
if ($bouncer['isUserAnAdmin'] == true) {
    $usersOfMyCompany = $organization->getUsersOfMyCompany();
}
$orgId = $bouncer["credentials"]["orgId"];


?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";</script>
<style>
    .modal-dialog{
        width: 50%;
    }

    .closeButton {
        width: 100px;
    }

    .center{
        text-align: center;
    }
    .modal-body{
        padding: 15px 15px 0 15px;
    }
    .unsubscribersTableHeader{
        text-transform: uppercase;
    }
    .table{
        margin-bottom: 0;
    }
    #closeBtn{
        /*margin-top: 15px;*/
    }

    #unsubscribersTable > tr > td{
        padding: 4px !important;
    }
    @media (max-width: 766px) {
        .modal-dialog{
            margin: 0 auto;
            width: 100%;
        }
        .mainRow{
            height: 750px;
        }
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>

<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">

            <div id="nav"></div>

            <form id="groupForm">
                <div class="modal-body">
                    <div id="paginationSection"></div>
                    <div class="row mainRow">
                        <div class="col-md-12 table-responsive" style="padding:15px 0 0 0;min-height: 551px;">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                <th class="text-center">Email</th>
                                <th class="text-center">Reason</th>
                                <th class="text-center">Date of unsubscribe</th>
                                </thead>
                                <tbody id="unsubscribersTable">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer center">
                    <button class="btn btn-primary closeButton" id="closeBtn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
var paginationController = {
    emailChunk: 0,
    currentPage : 0,
    totalPages: 0,
    rowsPerPage: 0,
    totalAmount: 0,
    dataType: "",
    data:[],
    btnId:"btnContainer",
    showAmountId: "showAmount",
    containerId:"",
    init:function (data, rowsPerPage, totalAmount, containerId, dataType) {
        var pages = 0;
        var pages = totalAmount / rowsPerPage;
        if (pages % 1 !== 0){
            pages = Math.floor(pages);
        }
        paginationController.dataType = dataType;
        paginationController.rowsPerPage = rowsPerPage;
        paginationController.totalPages = pages;
        paginationController.containerId = containerId;
        paginationController.data = data;
        paginationController.totalAmount = totalAmount;
        paginationController.createContainer();
        paginationController.createNavs();
        paginationController.showAmount();
        paginationController.buildRows(data);
    },
    createContainer:function (){

    document.getElementById(paginationController.containerId).innerHTML = "";
    var rowDiv = document.createElement("div");
    rowDiv.classList.add("row");
    rowDiv.style.margin ="0";

    var btnDiv = document.createElement("div");
    btnDiv.classList.add("btn-group");
    btnDiv.classList.add("pull-right");
    btnDiv.setAttribute("id", "btnContainer");
    rowDiv.appendChild(btnDiv);

     var amountDiv = document.createElement("div");
     amountDiv.classList.add("pull-left");
     amountDiv.setAttribute("id","showAmount");
     amountDiv.style.marginRight ="13px";
     rowDiv.appendChild(amountDiv);

     document.getElementById(paginationController.containerId).appendChild(rowDiv)


    },
    createNavs:function () {

        document.getElementById(paginationController.btnId).innerHTML = "";

        //create back button
        var backBTN =  document.createElement("button");
        backBTN.classList.add("btn");
        backBTN.classList.add("btn-white");
        backBTN.classList.add("btn-sm");
        backBTN.setAttribute("type","button");
        var leftArrow =  document.createElement("i");
        leftArrow.classList.add("fa");
        leftArrow.classList.add("fa-arrow-left");
        backBTN.appendChild(leftArrow);
        document.getElementById(paginationController.btnId).appendChild(backBTN);

        //create next button
        var nextBTN = document.createElement("button");
        nextBTN.classList.add("btn");
        nextBTN.classList.add("btn-white");
        nextBTN.classList.add("btn-sm");
        nextBTN.setAttribute("type","button");
        var rightArrow =  document.createElement("i");
        rightArrow.classList.add("fa");
        rightArrow.classList.add("fa-arrow-right");
        nextBTN.appendChild(rightArrow);
        document.getElementById(paginationController.btnId).appendChild(nextBTN);

        if(paginationController.currentPage == 0){
            backBTN.removeAttribute("onClick");
            backBTN.disabled = true;
        }else{
            backBTN.setAttribute("onClick","paginationController.back();");
            backBTN.disabled = false;
        }
        if(paginationController.currentPage == paginationController.totalPages){
            nextBTN.removeAttribute("onClick");
            nextBTN.disabled = true;
        }else{
            nextBTN.setAttribute("onClick","paginationController.next();");
            nextBTN.disabled = false;
        }
    },
    next: function(){
        paginationController.currentPage++;
        paginationController.emailChunk =  paginationController.rowsPerPage * paginationController.currentPage;
        var tBody = document.getElementById('unsubscribersTable');
        tBody.innerHTML = "";
        getUnsubscribersData(paginationController.emailChunk);
        paginationController.createNavs();
        paginationController.showAmount();
    },
    back: function(){
        paginationController.currentPage--;
        paginationController.emailChunk =  paginationController.rowsPerPage * paginationController.currentPage;
        var tBody = document.getElementById('unsubscribersTable');
        tBody.innerHTML = "";
        getUnsubscribersData(paginationController.emailChunk);
        paginationController.createNavs();
        paginationController.showAmount();
    },
    showAmount: function (){

        document.getElementById(paginationController.showAmountId).innerHTML = "";

        if (paginationController.totalAmount > 0){

            var first = paginationController.currentPage*paginationController.rowsPerPage+1;
            var last = (first-1)+paginationController.rowsPerPage;
            if(last > paginationController.totalAmount){
                last = paginationController.totalAmount;
            }

        }else{

            first = 0;
            last = 0;
        }
        var amountText = document.createElement("small");
        amountText.setAttribute("id","amountText");
        amountText.style.display ="inline-table";
        amountText.style.border = "1px dotted rgb(201,201,201)";
        amountText.style.padding = "6px 4px 4px 4px";
        amountText.style.height = "30px";

        amountText.innerHTML = "Showing <b>"+first+"-"+last+"</b> " + paginationController.dataType + " of <b>"+paginationController.totalAmount+"</b>";
        document.getElementById(paginationController.showAmountId).appendChild(amountText);
    },
    buildRows: function (data) {
        var tBody = document.getElementById('unsubscribersTable');
        // var data = JSON.parse(rows);
        for (var i=0; i < data.length;i++){

            var d = data[i];

            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.classList.add("text-center");
            var tdText = document.createTextNode(d.email);
            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            td.classList.add("text-center");
            var tdText = document.createTextNode(d.reason);
            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            td.classList.add("text-center");
            var tdText = document.createTextNode(d.unsubscribedTime);
            td.appendChild(tdText);
            tr.appendChild(td);

            tBody.appendChild(tr);
        }
    }
};

function getUnsubscribersData(unsubscribersPage) {

    var strUrl = BASE_URL + '/console/actions/mail/getUnsubscribers.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method: "post",
        data: {
            page: unsubscribersPage
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {

            var data = JSON.parse(data);
            paginationController.init(data["unsubscribersList"],20, data["numberOfUnsubscribers"] ,"paginationSection", "emails");

        } catch (e) {
            toastr.error("Error, Couldn't find data for you","sorry");
        }
    });


}

var unsubscribersPage = paginationController.currentPage;
getUnsubscribersData(unsubscribersPage);


</script>
