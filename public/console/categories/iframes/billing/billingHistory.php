<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<style>
    .dataTables_paginate{
        position: fixed;
        bottom: 75px;
        right: 18px;
    }
    .dataTables_wrapper .col-sm-12{
        padding:0;
    }
</style>
    <div>
        <div class="modal-content animated fadeIn" style="border:none; height: 100%">
            <div class="modal-header" style="text-align:left;padding: 15px;">
                <button type="button" class="close" onclick="parent.swal.close()" "><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div id="reportrange">
                    <i style="margin: 3px;" class="fa fa-calendar"></i>
                    <span>This Month</span> <b class="caret"></b>
                </div>
            </div>
            <div class="modal-body" style="margin-top: -7px;padding:0 15px;">
                <div class="" style="">
                    <table class="table table-hover text-center billingHistoryTable" data-page-size="15">
                        <thead>
                            <tr>
                                <th class="text-center th-date">Date</th>
                                <th class="text-center th-amount">Amount</th>
                                <th class="text-center th-type">Type</th>
                                <th class="text-center th-action">Action</th>
                            </tr>
                        </thead>
                        <tbody id="billingHistoryTableBody">
                            <tr><td colspan="4">Loading...</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer" style="position:fixed;bottom:0;right:0;width: 100%;">
                <button type="button" class="btn btn-white" onclick="parent.swal.close()">Close</button>
            </div>
        </div>
    </div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/moment.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.min.js"></script>
<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/dataTables/datatables.min.js"></script>

<script>
    var theStartDate = moment().startOf('month');
    var theEndDate = moment().endOf('month');

    $(document).ready(function() {
        
        $('#reportrange span').html("This Month");

        $('#reportrange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            minDate: '01/01/2018',
            maxDate: '12/31/2021',
            dateLimit: { days: 365 },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Go',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function(start, end, label) {
            if (label != "Custom") {
                $('#reportrange span').html(label);
            }else{
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            theStartDate = start;
            theEndDate = end;
            getBillingData()
        });

        getBillingData();

    });
    function getBillingData() {
        var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/system/organization/billing/getLastCharges.php", strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "GET",
            data:{
                startDate:theStartDate.format('YYYY-MM-DD'),
                endDate:theEndDate.format('YYYY-MM-DD')
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                var container = document.getElementById("billingHistoryTableBody");
                container.innerHTML = "";
                if (data.length > 0) {
                    if($('.dataTable').length){
                        var table = $('.billingHistoryTable').DataTable();
                        table.clear().destroy();
                    }
                    for (var i = 0; i < data.length; i++) {
                        setBillingHistoryTable(data[i], container);
                    }
                    startTableSort();
                }else{
                    var tr = document.createElement("tr");
                    var td = document.createElement("td");
                    td.setAttribute("colspan",4);
                    var tdText = document.createTextNode("No Transactions");
                    td.appendChild(tdText);
                    tr.appendChild(td);
                    container.appendChild(tr);
                }

            }catch (e) {
                swal("Oops", "Please try again later", "error");
            }
        });
    }
    function setBillingHistoryTable(data,container) {
        var tr = document.createElement("tr");

        var td = document.createElement("td");
        var tdText = document.createTextNode(data.changeDate);
        td.setAttribute("data-order", data.dateTimestamp);
        td.setAttribute("style", "min-width: 230px;");
        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        var tdText = document.createTextNode("$"+data.credit);
        td.appendChild(tdText);
        tr.appendChild(td);

        var td = document.createElement("td");
        td.setAttribute("style", "padding-top:13px;");
        var span = document.createElement("span");
        span.classList.add('label');
        span.classList.add('label-primary');
        if (data.type == 1) {
            var spanText = document.createTextNode("Added Funds");
        }else if (data.type == 2) {
            var spanText = document.createTextNode("Auto Recharge");
        }
        span.appendChild(spanText);
        td.appendChild(span);
        tr.appendChild(td);

        var td = document.createElement("td");
        if(data.refId){
            var a = document.createElement("a");
            a.href = data.receipt_url;
            if(data.receipt_url != "#"){a.target = "_blank"};
            a.classList.add("btn");
            a.classList.add("btn-xs");
            a.classList.add("btn-white");

            var aText = document.createTextNode("View Receipt");
            a.appendChild(aText);
            td.appendChild(a);
        }

        tr.appendChild(td);

        container.appendChild(tr);
    }
    function startTableSort() {

        var dTable = $('.billingHistoryTable').DataTable({
            "responsive": false,
            "autoWidth": false,
            "fixedColumns": true,
            "aaSorting": [],
            "oLanguage": {
                "sInfo": ""
            },
            "bFilter": false,
            "sScrollX": "100%",
            "pageLength": 10,
            "lengthChange": false,
            "columnDefs": [ {
                "targets": ["th-type","th-action"],
                "orderable": false,
                "visible": true
            } ],
            "language": {
                "paginate": {
                    "previous": "Previous ",
                    "next": " Next"
                }
            }
        });

        var info = dTable.page.info();
        if(info.pages == 1){
            $('.dataTables_paginate').hide();
        }

        $('.billingHistoryTable').DataTable();
        $('.dataTables_length').addClass('bs-select');

        $('thead').css("background-color","#F5F5F6");
    }
</script>