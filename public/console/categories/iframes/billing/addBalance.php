<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,array(["calendar",3]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
$organization = new organization($bouncer["credentials"]["orgId"]);
$users = $organization->getUsersOfMyCompany($bouncer["credentials"]["userId"]);
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<style>
    .pac-container{
        z-index: 99999;
    }
    #dateS{
        overflow: hidden;
        text-overflow: clip;
    }
    #myCustomModal .form-inline{
        /*margin-bottom: 38px;*/
        height: 34px;
    }
    #myCustomModal .form-inline .form-control, #myCustomModal .input-inline .form-control {
        width: 100%;
        /*float: right;*/
    }
    #myCustomModal .myModalContainer{
        width: 450px;
        display: block;
        margin: 0 auto;
    }
    #myCustomModal .my-form-group{
        display: inline;
    }
    #myCustomModal label[for='autoReachargeSwitchOn']{
        margin-left: 15px;
    }
    #myCustomModal label{
        float: left;
        height: 34px;
    }
    #myCustomModal label span{
        line-height: 34px;
    }
    @media only screen and (max-width: 768px){
        #myCustomModal .my-form-group{
            margin-left: 13px;
        }
        #myCustomModal label[for='autoReachargeSwitchOn']{
            margin-left: 3px;
        }
        #myCustomModal .myModalContainer{
            width: 300px;
        }
    }
    @media only screen and (max-width: 330px){
        #myCustomModal .myModalContainer{
            width: 250px;
        }
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="padding: 15px;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="btn-group">
                    <button class="btn btn-white active" id="changeTab1" type="button" onclick="changeTabContent(1)">Add Funds</button>
                    <button class="btn btn-white" id="changeTab2" type="button" onclick="changeTabContent(2)">Auto Recharge</button>
                </div>
            </div>
            <div class="modal-body" style="padding: 15px;">
                <div id="balanceTab1">
                    <div class="myModalContainer">
                        <div class="form-group form-inline">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <label for="rechargeAmount"><span>Amount</span></label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <select class="form-control" onchange="checkAmount($(this).val())" disabled id="rechargeAmount">
                                        <option value="10" selected>$10.00</option>
                                        <option value="20">$20.00</option>
                                        <option value="30">$30.00</option>
                                        <option value="40">$40.00</option>
                                        <option value="50">$50.00</option>
                                        <option value="60">$60.00</option>
                                        <option value="70">$70.00</option>
                                        <option value="80">$80.00</option>
                                        <option value="90">$90.00</option>
                                        <option value="100">$100.00</option>
                                        <option value="custom">Custom</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="rechargeCustomContainer" style="display:none;margin-top: 5px;">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <span style="display: inline-block;">
                                        <label for="rechargeCustom"><span>Custom Amount</span></label>
                                    </span>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <span style="display: inline-block;">
                                        <div class="input-group input-inline">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            <input type="number" class="form-control" value="10.00" min="10" onkeyup="checkRecharge()" placeholder="Type Amount" style="width: 100%;" id="rechargeCustom">
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="balanceTab2" style="display: none;">
                    <div class="myModalContainer">
                        <p>We'll only charge your payment method when your balance falls below the amount you set.</p>
<!--                            <input onclick="setSaveAutoRecharge()" style="font-size: 20px;" type="checkbox" id="enableAutoRecharge">-->
<!--                            <label class="control-label" for="enableAutoRecharge">Enable Auto Recharge</label>-->
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label><span>Auto Recharge</span></label>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div style="display: inline-block">
                                    <label style="cursor: pointer;float: none;height: unset;" for="autoReachargeSwitchOff">Disabled</label>
                                    <input type="radio" value="off" checked name="autoReachargeSwitchOff[]" class="autoRechargeSwitch" id="autoReachargeSwitchOff">
                                </div>
                                <div style="display: inline-block">
                                    <label style="cursor: pointer;float: none;height: unset;" for="autoReachargeSwitchOn">Enabled</label>
                                    <input type="radio" value="on" name="autoReachargeSwitchOff[]" class="autoRechargeSwitch" id="autoReachargeSwitchOn">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label for="autoRechargeBelow"><span>If the balance falls below</span></label>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-group input-inline">
                                    <span class="input-group-addon amountInfo" style="cursor: pointer;" onclick="showAmountBelowInfo()">
                                        <i class="fa fa-info"></i>
                                    </span>
                                    <select onchange="setSaveAutoRecharge()" disabled class="form-control" id="autoRechargeBelow">
                                        <option value="10" selected>$10.00</option>
                                        <option value="20">$20.00</option>
                                        <option value="30">$30.00</option>
                                        <option value="40">$40.00</option>
                                        <option value="50">$50.00</option>
                                        <option value="60">$60.00</option>
                                        <option value="70">$70.00</option>
                                        <option value="80">$80.00</option>
                                        <option value="90">$90.00</option>
                                        <option value="100">$100.00</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label for="autoRechargeAmount"><span>Recharge the balance to</span></label>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <select onchange="setSaveAutoRecharge();checkAutoAmount($(this).val())" disabled class="form-control" id="autoRechargeAmount">
                                    <option value="10">$10.00</option>
                                    <option value="20" selected>$20.00</option>
                                    <option value="30">$30.00</option>
                                    <option value="40">$40.00</option>
                                    <option value="50">$50.00</option>
                                    <option value="60">$60.00</option>
                                    <option value="70">$70.00</option>
                                    <option value="80">$80.00</option>
                                    <option value="90">$90.00</option>
                                    <option value="100">$100.00</option>
                                    <option value="custom">Custom</option>
                                </select>
                            </div>
                        </div>
                        <div id="autoRechargeCustomContainer" style="display:none;margin-top: 5px;">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <span>
                                        <label for="autoRechargeCustom"><span>Custom Amount</span></label>
                                    </span>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <span>
                                        <div class="input-group input-inline">
                                            <span id="customDollarAddon" class="input-group-addon">
                                                $
                                            </span>
                                            <input type="number" class="form-control" value="10.00" min="10" onchange="setSaveAutoRecharge();" placeholder="Type Amount" style="width: 100%;" id="autoRechargeCustom">
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="footerRecharge1">
                    <span id="noCreditCardLabel1" style="display:none;float: left;color: #ed5565;line-height: 33px;">No credit card available. <a href="javascript:$('#myCustomModal').modal('hide');parent.document.getElementById('stripeAddCard').click();">Add credit card</a></span>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="rechargeBTN" disabled>Add Funds</button>
                </div>
                <div id="footerRecharge2" style="display:none;">
                    <span id="noCreditCardLabel2" style="display:none;float: left;color: #ed5565;line-height: 33px;">No credit card available. <a href="javascript:$('#myCustomModal').modal('hide');parent.document.getElementById('stripeAddCard').click();">Add credit card</a></span>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="autoRechargeBTN" disabled>Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/chosen/chosen.jquery.js"></script>
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/iCheck/icheck.min.js"></script>

<script>
    $(document).ready(function () {

        l1 = $('#rechargeBTN').ladda();
        l2 = $('#autoRechargeBTN').ladda();

        getAutoRecharge();

        $('.autoRechargeSwitch').iCheck({
            radioClass: 'iradio_square-green',
        });
        $('.autoRechargeSwitch').on('ifChecked',function (event) {

            if(event.target.value == "on"){
                autoRechargeSwitch = true;
            }else if (event.target.value == "off"){
                autoRechargeSwitch = false;
            }

            setSaveAutoRecharge();

        });

        // get and set credit cards
        //var strUrl = '<?//= $_SERVER['LOCAL_NL_URL']; ?>///console/actions/system/organization/billing/getCreditCards.php', strReturn = "";
        //jQuery.ajax({
        //    url: strUrl,
        //    success: function (html) {
        //        strReturn = html;
        //    },
        //    async: true
        //}).done(function (data) {
        //    try{
        //        data = JSON.parse(data);
        //
        //        // var container1 =  document.getElementById("rechargeCC");
        //        // var container2 =  document.getElementById("autoRechargeCC");
        //
        //        if(data.status == true) {
        //            for (var i = 0; i < data.response.data.length; i++) {
        //
        //                var card = data.response.data[i];
        //
        //                var option = document.createElement("option");
        //                option.setAttribute("value",card.id);
        //
        //                var optionText = document.createTextNode(card.last4);
        //                option.appendChild(optionText);
        //
        //                container1.appendChild(option);
        //                container2.appendChild(option);
        //
        //            }
        //        }else{
        //            toastr.error(data.response,'Error');
        //        }
        //
        //    }catch (e){
        //        toastr.error('An error occurred. Please try again later.','Error');
        //    }
        //
        //});
    });

    var l1;
    var l2;
    var autoRechargeSwitch = false;

    function getAutoRecharge(){
        var strUrl = BASE_URL + '/console/actions/system/organization/billing/getAutoRecharge.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            async: true,
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                var autoRecharge = data.autoRecharge;
                var defaultcc = data.defaultcc;

                if (autoRecharge != false) {
                    if (autoRecharge.isOn == 1) {
                        autoRechargeSwitch = true;
                        document.getElementById("autoReachargeSwitchOn").checked = true;
                        document.getElementById("autoReachargeSwitchOff").checked = false;
                        $('.autoRechargeSwitch').iCheck("update");
                    } else {
                        autoRechargeSwitch = false;
                        document.getElementById("autoReachargeSwitchOff").checked = true;
                        document.getElementById("autoReachargeSwitchOn").checked = false;
                        $('.autoRechargeSwitch').iCheck("update");
                    }
                    setSaveAutoRecharge();

                    var balanceBelow = parseInt(autoRecharge.balanceBelow);

                    var selectIsMatchBelow = false;
                    $("#autoRechargeBelow option").each(function () {
                        if (balanceBelow == $(this).val()) {
                            selectIsMatchBelow = true;
                        }
                    });
                    if (selectIsMatchBelow) {
                        document.getElementById("autoRechargeBelow").value = balanceBelow;
                    } else {
                        var container = document.getElementById("autoRechargeBelow");
                        var option = document.createElement("option");
                        option.setAttribute("selected", true);
                        option.value = autoRecharge.balanceBelow;
                        var optionText = document.createTextNode("$" + autoRecharge.balanceBelow);

                        option.appendChild(optionText);
                        container.appendChild(option);

                        document.getElementById("autoRechargeBelow").value = autoRecharge.balanceBelow;
                    }

                    var balanceTo = parseInt(autoRecharge.balanceTo);
                    var selectIsMatch = false;
                    $("#autoRechargeAmount option").each(function () {
                        if (balanceTo == $(this).val()) {
                            selectIsMatch = true;
                        }
                    });
                    if (selectIsMatch) {
                        document.getElementById("autoRechargeAmount").value = balanceTo;
                    } else {
                        $("#autoRechargeCustomContainer").show();
                        document.getElementById("autoRechargeAmount").value = "custom";
                        document.getElementById("autoRechargeCustom").value = autoRecharge.balanceTo;
                    }
                }
                if(defaultcc == false || defaultcc.status == false || defaultcc.response.length == 0){
                    // No credit card available / Stripe Error
                    $("#rechargeBTN").attr("disabled",true);
                    $("#rechargeAmount").attr("disabled",true);
                    $("#noCreditCardLabel1").css("display",'block');
                    $("#noCreditCardLabel2").css("display",'block');
                    if (autoRecharge != false) {
                        if (autoRecharge.isOn == 0) {
                            $('.autoRechargeSwitch').iCheck('disable');
                        }
                    }else{
                        $('.autoRechargeSwitch').iCheck('disable');
                    }
                    return false;

                }else{
                    $("#rechargeBTN").removeAttr("disabled");
                    $("#rechargeAmount").removeAttr("disabled");
                    $("#noCreditCardLabel1").css("display",'none');
                    $("#noCreditCardLabel2").css("display",'none');

                }

            }catch (e) {

            }
        });
    }
    
    function changeTabContent(id) {
        $('#balanceTab1').hide();
        $('#balanceTab2').hide();
        $('#footerRecharge1').hide();
        $('#footerRecharge2').hide();
        $("#changeTab1").removeClass("active");
        $("#changeTab2").removeClass("active");

        $('#balanceTab'+id).show();
        $("#changeTab"+id).addClass("active");
        $('#footerRecharge'+id).show();
    }

    // Tab 1
    function checkAmount(amount) {
        if (amount == "custom"){
            $("#rechargeCustomContainer").show();
        }else{
            $("#rechargeCustomContainer").hide();
        }
        checkRecharge();
    }
    function checkRecharge() {
        var amount = $("#rechargeAmount").val();
        var custom = $("#rechargeCustom").val();

        if (amount != "") {
            if (amount != "custom") {
                $("#rechargeBTN").removeAttr("disabled");
            } else {
                if (custom >= 10) {
                    $("#rechargeBTN").removeAttr("disabled");
                } else {
                    $("#rechargeBTN").attr("disabled", true);
                }
            }
        }else{
            $("#rechargeBTN").attr("disabled",true);
        }
    }

    // Tab 1 Button
    $('#rechargeBTN').on("click",function () {
        var amount = document.getElementById("rechargeAmount").value;
        if (amount == "custom"){
            amount = document.getElementById("rechargeCustom").value;
        }

        if (amount >= 10) {
            l1.ladda('start');
            var strUrl = BASE_URL + '/console/actions/system/organization/billing/addFunds.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    amount: amount
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true,
            }).done(function (data) {

                l1.ladda('stop');
                try {
                    data = JSON.parse(data);
                    if (data.status == true) {
                        toastr.success("Funds added", "Success");
                        $("#myCustomModal").modal("hide");
                        initBillingInfo();
                    } else {
                        if (data.error && data.error.stripeCode == "missing"){
                            //toastr.error("You need to add a credit card in order to add funds", 'Missing credit card');
                            parent.document.getElementById("stripeAddCard").click();
                            $("#myCustomModal").modal('hide');
                        }else{
                            toastr.error("Unable to add funds, please try again later", 'Fail');
                        }
                    }
                } catch (e) {
                    toastr.error("Unable to add funds, please try again later", 'Fail');
                }


            });
        }else{
            toastr.error("Amount should be equals to/higher then 10","Incorrect amount");
        }
    });

    // Tab 2
    function checkAutoAmount() {
        var amount = $("#autoRechargeAmount").val();
        if (amount == "custom"){
            $("#autoRechargeCustomContainer").show();
        }else{
            $("#autoRechargeCustomContainer").hide();
        }
    }
    function setSaveAutoRecharge() {

        // dont allow to recharge to a smaller amount than the below amount
        $("#autoRechargeAmount option").each(function () {
            var value = $(this).val();
            if (value != "custom") {
                if (parseInt(value) <= parseInt($("#autoRechargeBelow").val())){
                    $(this).attr('disabled',true);
                }else{
                    $(this).removeAttr("disabled");
                }
            }else{
                $(this).removeAttr("disabled");
            }
        });

        checkAutoAmount();

        if (autoRechargeSwitch){
            $("#autoRechargeBelow").removeAttr("disabled");
            $("#autoRechargeAmount").removeAttr("disabled");
            $("#autoRechargeCustom").removeAttr("disabled");
            $("#customDollarAddon").css("background","white");
            $("#customDollarAddon").css("cursor","unset");
            $(".amountInfo").each(function () {
                $(this).css("background","#fff");
            });
        }else{
            $("#autoRechargeBelow").attr("disabled",true);
            $("#autoRechargeAmount").attr("disabled",true);
            $("#autoRechargeCustom").attr("disabled",true);
            $("#customDollarAddon").css("background","#eee");
            $("#customDollarAddon").css("cursor","not-allowed");
            $(".amountInfo").each(function () {
                $(this).css("background","#eee");
            });
        }
        var autoRechargeBelow = document.getElementById("autoRechargeBelow").value;
        var autoRechargeAmount = document.getElementById("autoRechargeAmount").value;
        var autoRechargeCustom = document.getElementById("autoRechargeCustom").value;

        if (autoRechargeBelow != ""){
            if (autoRechargeAmount != ""){
                if (autoRechargeAmount != "custom"){
                    $("#autoRechargeBTN").removeAttr("disabled");
                }else{
                    if (autoRechargeCustom != "") {

                        if (parseInt(autoRechargeBelow) < parseInt(autoRechargeCustom)){
                            $("#autoRechargeBTN").removeAttr("disabled");
                        } else{
                            $("#autoRechargeBTN").attr("disabled",true);
                        }
                    }else{
                        $("#autoRechargeBTN").attr("disabled",true);
                    }
                }
            }else{
                $("#autoRechargeBTN").attr("disabled",true);
            }
        }else{
            $("#autoRechargeBTN").attr("disabled",true);
        }
    }

    // Tab 2 Button
    $('#autoRechargeBTN').on("click",function () {

        var balanceBelow = document.getElementById("autoRechargeBelow").value;
        var balanceTo = document.getElementById("autoRechargeAmount").value;
        if (balanceTo == "custom"){
            balanceTo = document.getElementById("autoRechargeCustom").value;
        }
        if (parseInt(balanceBelow) > parseInt(balanceTo)){
            toastr.error('The recharge amount should be higher than the "falls below" amount.', 'Not Saved');
            return false;
        }
        if (balanceTo >= 10){
            l2.ladda('start');
            var strUrl = BASE_URL + '/console/actions/system/organization/billing/setAutoRecharge.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    isOn: autoRechargeSwitch,
                    balanceBelow: balanceBelow,
                    balanceTo:balanceTo
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true,
            }).done(function (data) {

                l2.ladda('stop');
                try {
                    data = JSON.parse(data);
                    if (data == true) {
                        toastr.success("Auto recharge data updated", "Success");
                        parent.initBillingInfo();
                        $("#myCustomModal").modal("hide");
                    } else if (data == "noCC") {
                        toastr.error("Unable to set auto recharge on, please add credit card first", 'No credit card');
                    }else{
                        toastr.error("Unable to set auto recharge, please try again later", 'Fail');
                    }
                } catch (e) {
                    toastr.error("Unable to set auto recharge, please try again later", 'Fail');
                }


            });
        }else{
            toastr.error("Recharge amount should be equals to/higher then 10","Incorrect recharge amount");
        }
    });
    function showAmountBelowInfo(){
        swal({
            text: 'You will only be charged once your balance falls below the amount you set.',
            buttons: {
                skip:{
                    text:"Got It",
                }
            }
        });
    }
    <?php if (isset($_GET['tab']) && $_GET['tab'] == "autoRecharge") {?>
    $(document).ready(function () {
        $('#balanceTab1').hide();
        $('#balanceTab2').show();
        $('#footerRecharge1').hide();
        $('#footerRecharge2').show();
        $("#changeTab1").removeClass("active");
        $("#changeTab2").addClass("active");
    });
    <?php } ?>
</script>
