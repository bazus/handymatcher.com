<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/todo/checkList.php");
$checkList = new checkList($bouncer["credentials"]["orgId"]);
$lists = $checkList->getData();
?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <?php if (isset($_GET['type']) && $_GET['type'] == 'list'){ ?>
            <form id="myFrom" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/todo/list/addList.php">
            <?php }elseif (isset($_GET['type']) && $_GET['type'] == 'item'){ ?>
            <form id="myFrom" method="post" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/todo/task/addTask.php">
            <?php } ?>
                <div class="modal-body" style="padding: 15px;">
                    <div class="form-group">
                        <?php if (isset($_GET['type']) && $_GET['type'] == 'item'){?>
                            <select onchange="document.getElementById('sendBTN').disabled = '';" name="list" class="form-control" style="margin-bottom: 15px;">
                                <option>Select A List</option>
                                <?php foreach ($lists as $item){?>
                                    <option value="<?= $item['id']?>"><?= $item['title'] ?></option>
                                <?php }?>
                            </select>
                        <?php }?>
                        <input class="form-control" type="text" name="title" id="title" placeholder="Enter List Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal" onclick="showCustomModal('categories/iframes/todo/todo.php')">Cancel</button>
                    <?php if (isset($_GET['type']) && $_GET['type'] == 'item'){ ?>
                        <button disabled="disabled" id="sendBTN" type="submit" class="ladda-button ladda-button-demo btn btn-primary" ]data-style="zoom-in">Add Todo Item</button>
                    <?php }elseif (isset($_GET['type']) && $_GET['type'] == 'list') { ?>
                        <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in">Add List</button>
                    <?php } ?>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#myFrom').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        l.ladda( 'stop' );
        $('#myCustomModal').modal('hide');

        if(statusText == "success"){
            var data = responseText;
            data = JSON.parse(data);
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            <?php if (isset($_GET['type']) && $_GET['type'] == 'item'){ ?>
            if(data['status'] == true){
                showCustomModal('categories/iframes/todo/todo.php');
            <?php }elseif (isset($_GET['type']) && $_GET['type'] == 'list') { ?>
            if(data == true){
                showCustomModal('categories/iframes/todo/todo.php');
            <?php } ?>
            }else{
                // Failed
                toastr.error('Your request was not sent, please try again later.','Error');
            }
        }else{
            // Failed
            toastr.error('Your request was not sent, please try again later.','Error');
        }
    }
</script>