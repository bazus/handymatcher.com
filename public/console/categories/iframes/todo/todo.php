<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/todo/checkList.php");
$checkList = new checkList($bouncer["credentials"]["orgId"]);
$lists = $checkList->getData();
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<style>
    .modal-content{
        padding: 5px;
    }
    .showDelete{
        display: block !important;
    }
    #todoTasks .liTask {
        margin-left: 2px;
        padding-left: 27px;
    }
    .liTask input{
        margin-left: 5px;
    }
    .ibox-title{
         border-color: unset;
         border-image: none;
         border-style: none;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>My todo list</h5>
                    <div class="dropdown">
                        <a data-toggle="dropdown" id="dropDownMenu" class="dropdown-toggle">
                            <i class="fa fa-gear" style="float: right;"></i>
                        </a>
                        <ul class="dropdown-menu" role="dropDownMenu" aria-labelledby="dropDownMenu" style="left: auto;right: 19px;">
                            <li role="presentation">
                                <a role="menuitem" onclick="addList()">Add New List</a>
                            </li>
                            <li role="presentation">
                                <a role="menuitem" onclick="addItem()">Add New Task</a>
                            </li>
                            <li role="presentation">
                                <a role="menuitem" onclick='deleteList()' id='deleteList' class='label-danger' style="color:white;">Delete List</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="todoContainer">
                    <select class="form-control" id="todoLists" style="display: none;">

                    </select>
                    <ul class="todo-list m-t small-list ui-sortable" id="todoTasks">

                    </ul>
                </div>
            </div>
            <div style="height: 39px;">
                <button class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system/todo.min.js"></script>