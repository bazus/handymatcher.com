<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
?>
<style>
    #toast-container>.toast:before {
        margin-left: -36px;
    }
    .callUsSwal .form-control{
        margin-bottom: 15px;
    }
    .callUsSwal .btn-default:hover{
        background: white;
    }
    .item{
        text-align: center;
        margin: 18px;
        padding: 13px;
        background: #fff;
        width: 28%;
        border: 1px dotted grey;
    }
    .item i{
        font-size: 30px;
        margin-top: 15px;
        margin-bottom: 15px;
        color:rgb(26, 179, 148);
    }
    .item button{
        margin-top: 15px;
    }
    .modal-dialog{
        width: 750px;
    }
    .modal-content{
        padding: 0 25px;
    }
    @media only screen and (max-width: 990px) {
        .item{
            width: unset;
        }
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div>
                <div class="row">
                    <div class="col-md-4 item">
                        <i class="fa fa-commenting"></i>
                        <h4>Chat with us</h4>
                        <p>The current wait time is about <span style="font-weight: bold">5-10</span> minutes</p>
                        <button class="btn btn-default" id="triggerIntercom" data-dismiss="modal">start a chat</button>
                    </div>
                    <div class="col-md-4 item">
                        <i class="fa fa-phone"></i>
                        <h4>Have us call you</h4>
                        <p>The current wait time is about <span style="font-weight: bold">5-10</span> minutes</p>
                        <button class="btn btn-default" onclick="talkToUs('','',0)" data-dismiss="modal">talk to us</button>
                    </div>
                    <div class="col-md-4 item">
                        <i class="fa fa-envelope"></i>
                        <h4>Send us an email</h4>
                        <p>The current response time is <span style="font-weight: bold">one</span> business day</p>
                        <button class="btn btn-default" onclick="sendEmail()" data-dismiss="modal">Send email</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <a href="http://help.network-leads.com/en/" target="_blank"><button type="button" class="btn btn-info pull-left">Open Help Center</button></a>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


    var l;

    function sendEmail() {
        $(document).ready(function () {
            l = $('#buttonSend').ladda();
        });
        var e = document.createElement("div");
        e.classList.add("text-left");
        var t = document.createElement("div"),
            n = document.createElement("label"),
            a = document.createTextNode("Full Name");
        n.appendChild(a), t.appendChild(n), (i = document.createElement("input")).type = "text",i.value="<?= $bouncer['userData']['fullName'] ?>", i.id = "fullname", i.classList.add("swal-content__input"), i.setAttribute("placeholder", "Type In Your Name"), i.setAttribute("required", "required"), t.appendChild(i), e.appendChild(t);
        var i, d = document.createElement("div");
        d.style.marginTop = "13px", n = document.createElement("label"), a = document.createTextNode("Email"), n.appendChild(a), d.appendChild(n), (i = document.createElement("input")).type = "text",i.value="<?= $bouncer['userData']['email'] ?>", i.id = "email", i.classList.add("swal-content__input"), i.setAttribute("placeholder", "Enter Your Email Address"), i.setAttribute("required", "required"), d.appendChild(i), e.appendChild(d);
        var s = document.createElement("div");
        s.style.marginTop = "13px", n = document.createElement("label"), a = document.createTextNode("Message"), n.appendChild(a), s.appendChild(n), (m = document.createElement("textarea")).classList.add("swal-content__input"), m.style.resize = "vertical", m.style.height = "200px", m.id = "content", m.setAttribute("placeholder", "Enter your message.."), m.setAttribute("required", "required"), s.appendChild(m), e.appendChild(s);
        var o = document.createElement("br"),
            r = document.createElement("br"),
            c = document.createElement("small"),
            u = document.createTextNode("* Here you can send us an Email if you need any kind of Assistant or Information");
        c.style.fontSize = "60%", c.appendChild(u), c.appendChild(o), e.appendChild(c);
        c = document.createElement("div");
        c.appendChild(m), c.appendChild(r), e.appendChild(c);
        var y = document.createElement("div");
        y.style.width = "100%", y.style.marginBottom = "5%", y.style.marginTop = "2%";
        var v = document.createElement("small");
        v.id = "errorSmall", v.classList.add("pull-left"), v.style.fontSize = "60%", v.style.color = "red", y.appendChild(v);
        var f = document.createElement("button");
        f.classList.add("btn"), f.classList.add("btn-default"), f.classList.add("pull-right"), f.setAttribute("onclick", "swal.close();"), f.style.marginBottom = "5%", f.style.marginTop = "2%";
        var g = document.createTextNode("Cancel");
        f.appendChild(g);
        var E = document.createElement("button");
        E.setAttribute("onclick", "acceptedEmail();"),E.setAttribute("data-style", "zoom-out"), E.style.marginLeft = "3px", E.style.marginRight = "3px", E.classList.add("btn"), E.classList.add("btn-success"), E.classList.add("pull-right"), E.id = "buttonSend", E.style.marginBottom = "5%", E.style.marginTop = "2%", g = document.createTextNode("Send"), E.appendChild(g), y.appendChild(E), y.appendChild(f), e.appendChild(y), swal({
            title: !1,
            content: e,
            buttons: !1,
            icon: !1
        });
    }
    function acceptedEmail() {
        l = $('#buttonSend').ladda();
        l.ladda('start');
        var e = $("#fullname").val(),
            t = $("#email").val(),
            n = $("#content").val();
        "" == e.trim() || "" == t.trim() || "" == n.trim() ? (l.ladda("stop"), $("#errorSmall").text("* Please Fill All Fields")) : (sendEmailToSystem(e, t, n))
    }

    function sendEmailToSystem(e, t, n) {

        var a = BASE_URL + "/actions/mail/sendEmailToSupport.php";
        return jQuery.ajax({
            url: a,
            method: "post",
            data: {
                name: e,
                email: t,
                content: n
            },
            success: function(e) {},
            async: 1
        }).done(function(e) {
            l.ladda("stop");
            try {
                if (e == "true"){
                    swal("Email Sent", "Thank you for contacting us. One of our representatives will be in contact with you ‌shortly. If you ever have any questions that require immediate assistance, please call us at (866) 277-2073.", "success");
                }else{
                    toastr.error("Email not sent, please try again later","Failed");
                }
            } catch (e) {
                toastr.error("Email not sent, please try again later","Failed");
            }
        }), !1
    }

    
    function talkToUs(phoneNumber,reason,error) {
        var content = document.createElement("div");

        var input = document.createElement("input");
        input.setAttribute("type","tel");
        input.value = phoneNumber;
        input.id = "phoneNumber";
        input.placeholder = "Contact Phone Number";
        input.classList.add("form-control");
        if (error == 1){
            input.style.border = "1px solid red";
        }

        content.appendChild(input);

        var input = document.createElement("input");
        input.setAttribute("type","text");
        input.value = reason;
        input.id = "reason";
        input.placeholder = "Call Reason";
        input.classList.add("form-control");
        if (error == 2){
            input.style.border = "1px solid red";
        }

        content.appendChild(input);

        var buttonsContainer = document.createElement("div");
        buttonsContainer.classList.add("swal-footer");
        buttonsContainer.style.padding = "0";

        var button = document.createElement("button");
        button.setAttribute("onclick","swal.close()");
        button.style.marginRight = "5px";
        button.classList.add("btn");
        button.classList.add("btn-default");

        var buttonText = document.createTextNode("Cancel");

        button.appendChild(buttonText);
        buttonsContainer.appendChild(button);

        var button = document.createElement("button");
        button.setAttribute("onclick","prepareTicket()");
        button.classList.add("btn");
        button.classList.add("laddaButton");
        button.classList.add("btn-success");

        var buttonText = document.createTextNode("Send");

        button.appendChild(buttonText);
        buttonsContainer.appendChild(button);

        content.appendChild(buttonsContainer);

        swal({
            title: "Have us call you",
            content: content,
            buttons: false,
            className: "callUsSwal"
        });
    }
    function prepareTicket(){
        var l = $(".laddaButton").ladda();
        l.ladda("start");

        var phoneNumber = document.getElementById("phoneNumber").value;
        var reason = document.getElementById("reason").value;
        if (phoneNumber.replace(/ /g,"") != "" && reason.replace(/ /g,"") != ""){
            sendCallTicket(phoneNumber,reason);
        }else{
            if (phoneNumber.replace(/ /g,"") == ""){
                toastr.error("All fields are required","Fail");
                l.ladda("stop");
                talkToUs(phoneNumber,reason,1);
                return false;
            }
            if (reason.replace(/ /g,"") == ""){
                toastr.error("All fields are required","Fail");
                l.ladda("stop");
                talkToUs(phoneNumber,reason,2);
                return false;
            }
        }
    }
    function sendCallTicket(p,r) {
        var l = $(".laddaButton").ladda();
        var a = BASE_URL + "/console/actions/system/sendCallTicket.php";
        return jQuery.ajax({
            url: a,
            method: "post",
            data: {
                phone: p,
                reason: r
            },
            async: 1
        }).done(function(e) {
            try {
                e = JSON.parse(e);
                if (e == true){
                    toastr.success("We got your ticket and will call you soon","Ticket received");
                    swal.close();
                    return 1;
                }else{
                    toastr.error("There was an error sending your ticket, please try again later","failed");
                    l.ladda("stop");
                    return !1
                }
            } catch (e) {
                l.ladda("stop");
                toastr.error("There was an error sending your ticket, please try again later","failed");
                return !1
            }
        }), !1
    }
</script>