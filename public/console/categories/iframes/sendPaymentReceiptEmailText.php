<?php

require_once($_SERVER['LOCAL_NL_PATH']."/console/connect/connect.php");
$connect = new connect();

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");

$paymentId = $_GET['paymentId'];
$orgId = $_GET['orgId'];

$payments = new payments($orgId);
$paymentData = $payments->getPayment($paymentId);

$organization = new organization($orgId);
$organizationData = $organization->getData();
$organizationPicture = $organizationData['logoPath'];

$paymentDate = date("l, M d Y \a\\t h:ia",strtotime($paymentData["paymentDate"]));

// calculate VAT
$vatCalculation = $payments->calculateVAT($paymentData["amount"],$paymentData["vatPer"],$paymentData["vatIsInclusive"]);

$amountWithoutVat = $paymentData["amount"];
if($paymentData["vatIsInclusive"] == "1"){
    $amountWithoutVat -= $vatCalculation["vatAmount"];
}

?>
<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Receipt</title>
    <style media="all" type="text/css">
        @page { margin: 0px; }
        body { margin: 0px; }

        @media only screen and (max-width: 640px) {
            .span-2,
            .span-3 {
                float: none !important;
                max-width: none !important;
                width: 100% !important;
            }
            .span-2 > table,
            .span-3 > table {
                max-width: 100% !important;
                width: 100% !important;
            }
        }

        @media all {
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }

        @media all {
            .btn-secondary a:hover {
                border-color: #34495e !important;
                color: #34495e !important;
            }
        }

        @media only screen and (max-width: 640px) {
            h1 {
                font-size: 36px !important;
                margin-bottom: 16px !important;
            }
            h2 {
                font-size: 28px !important;
                margin-bottom: 8px !important;
            }
            h3 {
                font-size: 22px !important;
                margin-bottom: 8px !important;
            }
            .main p,
            .main ul,
            .main ol,
            .main td,
            .main span {
                font-size: 16px !important;
            }
            .wrapper {
                padding: 8px !important;
            }
            .article {
                padding-left: 8px !important;
                padding-right: 8px !important;
            }
            .content {
                padding: 0 !important;
            }
            .container {
                padding: 0 !important;
                padding-top: 8px !important;
                width: 100% !important;
            }
            .header {
                margin-bottom: 8px !important;
                margin-top: 0 !important;
            }
            .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }
            .btn table {
                max-width: 100% !important;
                width: 100% !important;
            }
            .btn a {
                font-size: 16px !important;
                max-width: 100% !important;
                width: 100% !important;
            }
            .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
            .alert td {
                border-radius: 0 !important;
                font-size: 16px !important;
                padding-bottom: 16px !important;
                padding-left: 8px !important;
                padding-right: 8px !important;
                padding-top: 16px !important;
            }
            .receipt,
            .receipt-container {
                width: 100% !important;
            }
            .hr tr:first-of-type td,
            .hr tr:last-of-type td {
                height: 16px !important;
                line-height: 16px !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
        }

        table.print-friendly tr td, table.print-friendly tr th {
            page-break-inside: avoid;
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body style="font-family: Helvetica, sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f6f6f6; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;" width="100%" bgcolor="#f6f6f6">
    <tr>
        <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; margin: 0 auto !important; max-width: 600px; padding: 0; padding-top: 24px; width: 600px;" width="600" valign="top">
            <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 600px; padding: 0;">

                <!-- START CENTERED WHITE CONTAINER -->
                <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span>


                <?php
                if($organizationPicture != "") {
                    ?>
                    <!-- START HEADER -->
                    <div class="header" style="margin-bottom: 24px; margin-top: 0; width: 100%;">
                        <table border="0" cellpadding="0" cellspacing="0"
                               style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; min-width: 100%;"
                               width="100%">
                            <tr>
                                <td class="align-center"
                                    style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; text-align: center;"
                                    valign="top" align="center">
                                    <a href="http://htmlemail.io" target="_blank"
                                       style="color: #3498db; text-decoration: underline;">
                                        <img src="<?php echo $organizationPicture; ?>" height="41" alt="Logo"
                                             align="center"
                                             style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%;">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                }
                ?>
                <!-- END HEADER -->
                <table border="0" cellpadding="0" cellspacing="0" class="main print-friendly" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 4px;" width="100%">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 24px;" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                        <h1 style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 300; line-height: 1.4; margin: 0; font-size: 36px; margin-bottom: 24px; text-align: center; text-transform: capitalize;">Payment receipt</h1>
                                        <h2 class="align-center" style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1.4; margin: 0; font-size: 28px; margin-bottom: 16px; text-align: center;">Your order</h2>
                                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tr>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
                                                <td class="receipt-container" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; width: 80%;" width="80%" valign="top">
                                                    <table class="receipt" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin-bottom: 24px;" width="100%">
                                                        <tr class="receipt-subtle" style="color: #aaa;">
                                                            <td colspan="2" class="align-center" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; text-align: center; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top" align="center"><?php echo $paymentDate; ?> (EST)</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">Payment ID</td>
                                                            <td class="receipt-figure" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px; text-align: right;" valign="top" align="right">#10<?php echo $paymentData["id"]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">Amount (USD)</td>
                                                            <td class="receipt-figure" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px; text-align: right;" valign="top" align="right">$<?php echo $amountWithoutVat; ?></td>
                                                        </tr>
                                                        <tr class="receipt-subtle" style="color: #aaa;">
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">Tax (%<?php echo $paymentData["vatPer"]; ?>)</td>
                                                            <td class="receipt-figure" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px; text-align: right;" valign="top" align="right">$<?php echo $paymentData["vatAmount"]; ?></td>
                                                        </tr>
                                                        <tr class="receipt-bold">
                                                            <td style="font-family: Helvetica, sans-serif; vertical-align: top; margin: 0; padding: 8px; font-size: 18px; border-bottom: 2px solid #333; border-top: 2px solid #333; font-weight: 600;" valign="top">Total (USD)</td>
                                                            <td class="receipt-figure" style="font-family: Helvetica, sans-serif; vertical-align: top; margin: 0; padding: 8px; font-size: 18px; border-bottom: 2px solid #333; text-align: right; border-top: 2px solid #333; font-weight: 600;" valign="top" align="right">$<?php echo $paymentData["totalAmount"]; ?></td>
                                                        </tr>
                                                    </table>


                                                    <table class="receipt" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin-bottom: 24px;" width="100%">
                                                        <?php
                                                        if($paymentData["address"] != "" && $paymentData["city"] != "" && $paymentData["country"] != "" && $paymentData["zip"] != ""){
                                                        ?>
                                                        <tr>
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">
                                                                <h2 class="align-center" style="color: #222222; font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1.4; margin: 0; font-size: 28px; margin-bottom: 16px; text-align: center;">Your details</h2>
                                                                <br>
                                                                <?php echo $paymentData["address"]; ?><br>
                                                                <?php echo $paymentData["city"]; ?><br>
                                                                <?php echo $paymentData["country"]." ".$paymentData["zip"]; ?><br>
                                                        </tr>
                                                        <?php } ?>
                                                        <?php
                                                        if($paymentData["method"] == "1"){
                                                        ?>
                                                        <tr>
                                                            <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; border-bottom: 1px solid #eee; margin: 0; padding: 8px;" valign="top">
                                                                <br>Ending in *<?php echo $paymentData["creditCardNumber"]; ?>
                                                                <br>Expiring <?php echo $paymentData["expDate"]; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </table>
                                                    <p style="font-family: Helvetica, sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 16px;">Notice something wrong? Contact our support team and we'll be happy to help.</p>
                                                </td>
                                                <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- START CALL OUT -->
                    <tr>
                        <td class="wrapper section-callout" style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 24px; background-color: #1abc9c; color: #ffffff;" valign="top" bgcolor="#1abc9c">
                            <table class="print-friendly" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                <tr>
                                    <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top; color: #ffffff;" valign="top">
                                        <h2 class="align-center" style="font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1.4; margin: 0; font-size: 28px; margin-bottom: 16px; text-align: center; color: #ffffff;">Thank you for choosing us!</h2>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- END CALL OUT -->

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- START FOOTER -->
                <div class="footer" style="clear: both; padding-top: 24px; text-align: center; width: 100%;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                        <?php
                        if($organizationData["address"] != "" && $organizationData["city"] != "" && $organizationData["state"] != "" && $organizationData["zip"] != ""){
                        ?>
                        <tr>
                            <td class="content-block" style="font-family: Helvetica, sans-serif; vertical-align: top; padding-top: 0; padding-bottom: 24px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
                                <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"><?php echo $organizationData["organizationName"].", ".$organizationData["address"].", ".$organizationData["city"]." ".$organizationData["state"]." ".$organizationData["zip"]; ?></span>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td class="content-block powered-by" style="font-family: Helvetica, sans-serif; vertical-align: top; padding-top: 0; padding-bottom: 24px; font-size: 12px; color: #999999; text-align: center;" valign="top" align="center">
                                Powered by <a target="_blank" href="http://www.network-leads.com" title="Network Leads" style="color: #999999; font-size: 12px; text-align: center; text-decoration: none;">Network Leads</a>.
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- END FOOTER -->

                <!-- END CENTERED WHITE CONTAINER --></div>
        </td>
        <td style="font-family: Helvetica, sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
    </tr>
</table>
</body>
</html>

