<?php

// This file is not suppose to be in use anymore !
// I've moved this to 'getCustomEmail.php'
// But I'm too afraid to delete this


if(isset($_GET['leadId'])) {
    $pagePermissions = array(false, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $organizationMailAccounts = $mailaccounts->getMyAccounts(true, true);

    $leadId = $_GET['leadId'];

    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);

    $leadData = $lead->getData();
    $hasDefaultEmail = false; // used to determine if to add "disabled" attribute the Send button
}
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<style>
    .estimateContainer *{
        font-family: 'Montserrat', sans-serif;;
    }
    .estimateEmail{
        width: 850px;
        padding-top: 0;
        margin-top: 10px;
    }
    .estimateContainer{
        width: 80%;
        display: block;
        margin: 15px auto;
    }
    h2.bold{
        font-weight: bold;
    }
    .jobNo{
        font-size: 14px;
        color: #2f79ff;
        font-weight: 100;
    }
    .estimateHr{
        border-top: 1px solid #afadad;
        margin-bottom: 15px;
        margin-top: 15px;
    }
    h5 span.pull-right{
        font-weight: 100;
    }
    #myCustomModal .modal-footer{
        margin-top: 0;
    }
    .bottom-line{
        border-top: 1px solid black;
        display: block;
        width: 100%;
    }
    .topSpacer{
        margin-top: 50px;
    }
    .estimateContainer h3{
        margin-bottom: 15px;
    }
    .mailContent .form-group{
        margin-bottom: 5px;
        height: 34px;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog estimateEmail">

        <div class="modal-content animated fadeIn" style="box-shadow: none !important;">
            <div class="modal-head">
                <div class="row">
                    <div class="col-md-12 mailContent" style="padding: 15px 15px 0 15px;">
                        <?php if (count($organizationMailAccounts) > 0){ ?>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">From:</label>
                                <div class="col-sm-11">
                                    <select class="form-control" id="mailAccount">
                                        <option value="">Select Mail Account</option>
                                        <?php
                                        foreach($organizationMailAccounts as $organizationMailAccount){
                                            ?>
                                            <option <?php if (count($organizationMailAccounts) == 1){echo "selected";} ?> <?php if ($organizationMailAccount['id'] == $bouncer['organizationData']['mailAccount']){echo 'selected';$hasDefaultEmail = true;} ?> value="<?php echo $organizationMailAccount['id']; ?>" ><?php echo $organizationMailAccount['email']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">To:</label>
                                <div class="col-sm-11">
                                    <input type="text" id="toEmail" class="form-control cancelTo" placeholder="To Email Address" disabled value="<?= $leadData['email'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">Subject:</label>
                                <div class="col-sm-11">
                                    <input type="text" id="subjectEmail" class="form-control" placeholder="Email Subject" value="<?= "Moving Estimate From ".$bouncer['organizationData']['organizationName'] ?>">
                                </div>
                            </div>
                        <?php }else{ ?>
                            <div class="alert alert-info" style="width: 80%;display: block;margin: 0 auto;margin-bottom: 11px;">
                                No Active Mail Account, <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/mailSettings.php">Create One</a>
                            </div>
                        <?php } ?>
                        <hr style="margin-bottom: 0px;">
                    </div>
                </div>
            </div>

            <div class="modal-body" style="height: 75vh;background: #ffffff !important;padding: 0px;">
                <iframe src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/iframes/sendEstimateEmailText.php?leadId=<?= $_GET['leadId'] ?>&userId=<?= $bouncer["credentials"]["userId"] ?>&orgId=<?= $bouncer["credentials"]['credentials'] ?>" style="width: 100%;border: none;height: 100%;"></iframe>
            <!--
                <div class="estimateContainer">
                    <div class="row">
                        <?php if ($organizationPicture && getimagesize($organizationPicture) > 0){ ?>
                            <div class="col-md-12" style="margin-bottom: 25px;">
                                <img style="height: 60px;" src="<?= $organizationPicture ?>" alt="<?= $organizationData['organizationName'] ?>">
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div style="text-align: left">
                                <span style="font-size: 21px;">Moving Estimate</span>
                                <span style="float: right;margin-top: -30px;">
                                    <?php if ($organizationData['organizationName']) {
                                        echo $organizationData['organizationName']."<br>";
                                    }else{
                                        echo "<br>";
                                    }?>
                                    <?php if ($organizationData['address']) {
                                        echo $organizationData['address']."<br>";
                                    }else{
                                        echo "<br>";
                                    }?>
                                    <?php if ($organizationData['phone']) {
                                        echo $organizationData['phone']."<br>";
                                    }else{
                                        echo "<br>";
                                    }?>
                                    <span style="margin-top: 10px;color: rgb(82,126,187)">
                                        Job No: <?= $_GET['leadId'] ?>
                                    </span>
                                </span>
                            </div>
                            <div class="estimateHr"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (isset($leadData['firstname']) && isset($leadData['lastname'])){ ?>
                                <h4>Full Name: <?= $leadData['firstname']." ".$leadData['lastname'] ?></h4>
                            <?php } ?>
                            <?php if (isset($leadData['phone'])){ ?>
                                <h4>Phone: <?= $leadData['phone']?></h4>
                            <?php } ?>

                            <?php if (isset($leadData['email'])){ ?>
                                <h4>Email: <?= $leadData['email']?></h4>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left">
                            <div class="estimateHr"></div>
                        </div>
                    </div>
                    <?php if ($movingLeadData['fromZip'] || $movingLeadData['toZip']){ ?>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="text-left">Moving From</h3>
                            <?php if (isset($movingLeadData['fromState']) && $movingLeadData['fromState'] != ""){ ?>
                                <h5>State: <?= $movingLeadData['fromState'] ?></h5>
                            <?php } ?>
                            <?php if (isset($movingLeadData['fromAddress']) && $movingLeadData['fromAddress'] != ""){ ?>
                                <h5>Address: <?= $movingLeadData['fromAddress'] ?></h5>
                            <?php } ?>
                            <?php if (isset($movingLeadData['fromCity']) && $movingLeadData['fromCity'] != ""){ ?>
                                <h5>City: <?= $movingLeadData['fromCity'] ?></h5>
                            <?php } ?>
                            <?php if (isset($movingLeadData['fromZip']) && $movingLeadData['fromZip'] != ""){ ?>
                                <h5>ZIP Code: <?= $movingLeadData['fromZip'] ?></h5>
                            <?php } ?>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-left">Moving To</h3>
                            <?php if (isset($movingLeadData['toState']) && $movingLeadData['toState'] != ""){ ?>
                                <h5>State: <?= $movingLeadData['toState'] ?></h5>
                            <?php } ?>
                            <?php if (isset($movingLeadData['toAddress']) && $movingLeadData['toAddress'] != ""){ ?>
                                <h5>Address: <?= $movingLeadData['toAddress'] ?></h5>
                            <?php } ?>
                            <?php if (isset($movingLeadData['toCity']) && $movingLeadData['toCity'] != ""){ ?>
                                <h5>City: <?= $movingLeadData['toCity'] ?></h5>
                            <?php } ?>
                            <?php if (isset($movingLeadData['toZip']) && $movingLeadData['toZip'] != ""){ ?>
                                <h5>ZIP Code: <?= $movingLeadData['toZip'] ?></h5>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="estimateHr"></div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="text-left">Relocation Details</h3>
                            <h5>Job No: <span class="pull-right"><?= $_GET['leadId'] ?></span></h5>
                            <h5>Estimate Date: <span class="pull-right"><?= date("m/d/Y",strtotime($movingLeadData['requestedDeliveryDateStart'])) ?></span></h5>
                            <h5>Move Type: <span class="pull-right"><?= $typeOfMove ?></span></h5>
                            <h5>Estimated Volume: <span class="pull-right"><?= $estimateCalculation->calculationData['totalCF'] ?> <?php if($movingSettingsData['calculateBy'] == 0){echo "LBS";}else{echo "CF";} ?></span></h5>
                            <h5>Move Date: <span class="pull-right"><?= date("m/d/Y",strtotime($movingLeadData['moveDate'])) ?></span></h5>
                            <h5>Box Delivery: <span class="pull-right"><?= date("m/d/Y",strtotime($movingLeadData['boxDeliveryDate'])) ?></span></h5>
                            <h5>Pack Day: <span class="pull-right"><?= date("m/d/Y",strtotime($movingLeadData['pickupDate'])) ?></span></h5>
                            <h5>Delivery: <span class="pull-right"><?= date("m/d/Y",strtotime($movingLeadData['requestedDeliveryDateStart'])) ?></span></h5>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-left">Relocation Estimate</h3>
                            <?php

                            echo "<h5>Initial Price: <span class=\"pull-right\">$".number_format($estimateCalculation->calculationData['initPrice'],2, '.', ',')."</span></h5>";

                            if (isset($estimateCalculation->calculationData['fuel']) && $estimateCalculation->calculationData['fuel'] > 0){
                                echo "<h5>Fuel (".$data['fuel']."%): <span class=\"pull-right\">$".number_format($estimateCalculation->calculationData['fuel'],2, '.', ',')."</span></h5>";
                            }
                            if (isset($estimateCalculation->calculationData['coupon']) && $estimateCalculation->calculationData['coupon'] > 0){
                                echo "<h5>Coupon (".$data['coupon']."%): <span class=\"pull-right\">-$".number_format($estimateCalculation->calculationData['coupon'],2, '.', ',')."</span></h5>";
                            }
                            if (isset($estimateCalculation->calculationData['senior']) && $estimateCalculation->calculationData['senior'] > 0){
                                echo "<h5>Senior Discount (".$data['senior']."%): <span class=\"pull-right\">-$".number_format($estimateCalculation->calculationData['senior'],2, '.', ',')."</span></h5>";
                            }
                            if (isset($estimateCalculation->calculationData['veteran']) && $estimateCalculation->calculationData['veteran'] > 0){
                                echo "<h5>Veteran Discount (".$data['veteranDiscount']."%): <span class=\"pull-right\">-$".number_format($estimateCalculation->calculationData['veteran'],2, '.', ',')."</span></h5>";
                            }

                            if ($estimateCalculation->calculationData['totalPacking'] > 0){
                                echo "<h5>Packing + Unpacking: <span class=\"pull-right\">$".number_format($estimateCalculation->calculationData['totalPacking'],2, '.', ',')."</span></h5>";
                            }

                            if (isset($estimateCalculation->calculationData['discount']) && $estimateCalculation->calculationData['discount'] > 0){
                                echo "<h5>Discount (".$data['discount']."%): <span class=\"pull-right\">-$".number_format($estimateCalculation->calculationData['discount'],2, '.', ',')."</span></h5>";
                            }
                            if (isset($estimateCalculation->calculationData['extraCharges'])){
                                foreach ($estimateCalculation->calculationData['extraCharges'] as $charge){
                                    echo "<h5>".$charge['title'].": <span class=\"pull-right\">$".number_format($charge['price'],2, '.', ',')."</span></h5>";
                                }
                            }
                            if (isset($estimateCalculation->calculationData['agentFee']) && $estimateCalculation->calculationData['agentFee'] > 0){
                                echo "<h5>Agent Fee (".$data['agentFee']."%): <span class=\"pull-right\">$".number_format($estimateCalculation->calculationData['agentFee'],2, '.', ',')."</span></h5>";
                            }
                            echo "<div class=\"estimateHr\"></div>";
                            echo "<h5>Total Estimate: <span class=\"pull-right\">$".number_format($data['totalEstimate'],2, '.', ',')."</span></h5>";

                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="estimateHr"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-left">Understanding Your Estimate</h3>
                            <p>
                                <?php if (isset($movingSettingsData['estimateTerms']) && $movingSettingsData['estimateTerms']){ echo $movingSettingsData['estimateTerms']; } ?>
                            </p>
                        </div>
                    </div>
                    <?php if ($movingLeadData['moveInventory']){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="estimateHr"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <h3>Inventory</h3>
                            <table class="table table-hover">
                                <thead>
                                    <th>Qty</th>
                                    <th>Item Name</th>
                                    <th>Cubic feet</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($movingLeadData['moveInventory'] as $invetory){?>
                                        <tr>
                                            <td><?= $invetory['amount'] ?></td>
                                            <td><?= $invetory['name'] ?></td>
                                            <td><?= $invetory['cf'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><b>Total</b></td>
                                        <td></td>
                                        <td id="totalCuf"><b><?php  echo $totalCF ?></b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row topSpacer">
                        <div class="col-md-4">
                            <span class="bottom-line"></span>
                            Customer Name
                        </div>
                        <div class="col-md-4">
                            <span class="bottom-line"></span>
                            Customer Signature
                        </div>
                        <div class="col-md-4">
                            <span class="bottom-line"></span>
                            Date
                        </div>
                    </div>
                </div>
                -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="button" <?php if (!$hasDefaultEmail){echo 'disabled';} ?> class="ladda-button ladda-button-demo btn btn-primary" id="sendEstimate" data-style="zoom-in">Send</button>
            </div>
        </div>
    </div>
</div>


<!-- Ladda -->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });
    $("#sendEstimate").on('click',function () {
        l.ladda("start");

        var subjectEmail = $("#subjectEmail").val();

        var strUrl = BASE_URL+'/console/actions/mail/sendEstimateToLead.php';

        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                "leadId": '<?= $leadId ?>',
                "emailAccountId": $("#mailAccount").val(),
                subjectEmail: subjectEmail
            },
            async: true
        }).done(function (data) {
            try {
                l.ladda( 'stop' );
                data = JSON.parse(data);
                if (data.status == true){
                    toastr.success("Email Sent Successfully","Sent");
                    $('#myCustomModal').modal('hide');
                } else{
                    for (var i = 0;i<data.errors.length;i++){
                        toastr.error(data.errors[i],"ERROR");
                    }
                }
            }catch (e) {
                l.ladda( 'stop' );
                toastr.error("Email Not Sent","ERROR");
            }

        });
    });
    $("#mailAccount").on("change",function () {
       if ($(this).val() != ""){
           $("#sendEstimate").attr("disabled",false);
       }else{
           $("#sendEstimate").attr("disabled",true);
       }
    });
    <?php if (count($organizationMailAccounts) == 1){?>
        $("#sendEstimate").attr("disabled",false);
    <?php } ?>
</script>