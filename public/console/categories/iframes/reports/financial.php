
<?php
$pagePermissions = array(true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

?>

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<style>
    #totalPaymentChart{
        width: 100%;
        height: 500px;
    }
    .financialTable{
        font-size: 13px;
        color:rgb(103, 106, 108);
    }
    .financialTable th{
        text-align: center;
        border-bottom-width: 1px !important;
    }
    .financialTable td{
        text-align: center;
        cursor: pointer;
        vertical-align: middle !important;
    }
    .financialTable>tbody td{
        vertical-align: middle;
        padding: 5px;
    }
    body {
        background-color: #f3f3f4;
    }
    .tableFooterTh{
        border: none !important;
    }
    .dataTables_scrollFootInner table {
        border: none !important;
    }
    .input-group-addon, .input-group-btn{
        width: unset;
    }
    @media (max-width: 767px) {
        #exportChartList, #exportTableList {
            position: absolute !important;
            right: 0;
            left: auto;
            background-color: #fff !important;
        }
    }
    .dataTables_filter label {
        display: flex;
        margin-top: 13px;
        font-weight: 400;
    }
    .dataTables_filter input {
        margin-top: -3px;
        margin-left: 5px;
    }
    .ibox-title h3{
        margin-top: 0 !important;
    }

    .exportMenu{
        box-shadow: 0 0 3px rgba(86, 96, 117, 0.7) !important;
    }
</style>
<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
</script>
            <div id="wrapper" style="padding: 9px; background-color: #f3f3f4;">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox">
                                <div style="display: flex;justify-content: space-between;background-color: #ffffff;">

                                <div class="ibox-title" style="border-color: #ffffff;"><h3>Payments Chart</h3></div>

                                    <span class="input-group-addon" style="border: none">
                                        <ul class="nav navbar-nav navbar-right" style="margin:10px 0 0 0">
                                            <li class="dropdown">
                                                <span href="#" id="exportChartBtn" class="dropdown-toggle exportBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 7px 15px 8px 13px;">Export<span class="caret"></span></span>
                                                <ul class="dropdown-menu exportMenu" style="margin-top: 13px;color: #000;" id="exportChartList">
                                                    <li class="exportChartBtn"  id="csv"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to csv</a></li>
                                                    <li class="exportChartBtn" id="xlsx"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to xlsx</a></li>
                                                    <li class="exportChartBtn"  id="pdf"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to pdf</a></li>
                                                    <li class="exportChartBtn"  id="png"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to png</a></li>
                                                    <li class="exportChartBtn"  id="jpg"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to jpg</a></li>
                                                    <li class="exportChartBtn"  id="svg"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to svg</a></li>
                                                    <li class="exportChartBtn"  id="print"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Print</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </span>
                                </div>
                                <div class="ibox-content" id="paymentChartWrapper">
                                    <div id="totalPaymentChart"></div>
                                </div>
                            </div>
                            <div class="ibox" style="margin-bottom: 0px;">
                                <div style="display: flex;justify-content: space-between;background-color: #ffffff;">
                                    <div class="ibox-title"  style="border-color: #ffffff;"><h3>Leads Payments</h3></div>
                                    <div style="display: flex; justify-content: space-between">
                                        <div id="paymentSearch"></div>

                                    <span class="input-group-addon" style="border: none">
                                         <ul class="nav navbar-nav navbar-right" style="margin:14px 0 0 0">
                                            <li class="dropdown">
                                              <span href="#" id="exportTableBtn" class="dropdown-toggle exportBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 7px 15px 8px 13px;">Export<span class="caret"></span></span>
                                              <ul class="dropdown-menu exportMenu" style="margin-top: 13px;color: #000;" id="exportTableList">
                                                  <li class="exportBtn" id="csv"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to csv</a></li>
                                                  <li class="exportBtn" id="excel"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to excel</a></li>
                                                  <li class="exportBtn" id="pdf"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to pdf</a></li>
                                                  <li class="exportBtn" id="copy"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Copy</a></li>
                                                  <li class="exportBtn" id="print"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Print</a></li>
                                              </ul>
                                            </li>
                                          </ul>
                                    </span>
                                    </div>
                                </div>
                                </div>
                                <div class="ibox-content" style="padding-bottom: 5px;">
                                    <table class="table table-hover table-bordered financialTable">
                                        <thead>
                                        <tr>
                                            <th class="th-jobNumber" style="text-align:center;width: 73px;">Job #</th>
                                            <th class="">Total Estimate</th>
                                            <th class="">Total Payments</th>
                                            <th class="">Remaining Balance</th>
                                        </tr>
                                        </thead>
                                        <tbody  id="financialRows">
                                        <tr><td style="text-align:center;" colspan="4">No Payments</td></tr>
                                        </tbody>
                                        <tfoot style="">
                                        <tr>
                                            <th class="tableFooterTh">Total</th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/dataTables/datatables.min.js"></script>
<!-- amcharts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/core.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/charts.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/themes/material.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/amcharts/themes/animated.js"></script>

<script>
    var chart = "";
    $( document ).ready(function() {

    });

    var financialController = {

        getFinancialPaymentsTableStats: function () {
            var strUrl = BASE_URL+'/console/actions/reports/getFinancialReports.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    start: parent.theStartDate,
                    end: parent.theEndDate
                },
                success: function (html) {
                    strReturn = html;
                },
                async: false
            }).done(function (jsondata) {
                try {
                    var data = JSON.parse(jsondata);
                    console.log(data);
                    financialController.setFinancialTable(data);

                }catch (e) {
                    return false;
                }
            });

        },
        getFinancialPaymentsChartStats: function () {

            var strUrl = BASE_URL+'/console/actions/reports/getFinancialPaymentsChart.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    start: parent.theStartDate,
                    end: parent.theEndDate
                },
                success: function (html) {
                    strReturn = html;
                },
                async: false
            }).done(function (jsondata) {
                try {
                    var paymentData = JSON.parse(jsondata);
                    financialController.totalPaymentChart(paymentData);
                }catch (e) {
                    return false;
                }
            });

        },
        setFinancialTable: function(data){
            if($('.dataTable').length){
                var table = $('.financialTable').DataTable();
                table.destroy();
                $('tfoot').hide();
            }

            $("#financialRows").html("");

            if(data.length == 0){

                var tr = document.createElement("tr");

                var td = document.createElement("td");
                td.setAttribute("style","text-align:center;");
                td.innerHTML = "No Payments";
                td.setAttribute("colspan","4");

                tr.appendChild(td);
                document.getElementById("financialRows").appendChild(tr);
                return;
            }
            for (var i = 0; i < data.length; i++) {

                var row = document.createElement("tr");

                row.setAttribute("onClick","financialController.openJob('"+data[i].leadId+"')");

                var jobNumber = document.createElement("td");
                jobNumber.innerHTML = data[i].jobNumber;

                var totalEstimate = document.createElement("td");
                if(data[i].totalEstimate == "N/A"){
                    totalEstimate.innerText = data[i].totalEstimate;
                    totalEstimate.setAttribute("data-order", "-1");
                }else{
                    totalEstimate.innerText ="$" +  data[i].totalEstimate;
                    totalEstimate.setAttribute("data-order", data[i].totalEstimate);
                }

                var paymentAmount = document.createElement("td");
                if(data[i].totalPayments == "N/A"){
                    paymentAmount.innerText =data[i].totalPayments;
                    paymentAmount.setAttribute("data-order", "-1");
                }else{
                    paymentAmount.innerText ="$" +  data[i].totalPayments;
                    paymentAmount.setAttribute("data-order", data[i].totalPayments);
                }

                var paymentDifference = document.createElement("td");
                if(data[i].balance == "N/A"){
                    paymentDifference.innerText = data[i].balance;
                    paymentDifference.setAttribute("data-order", "-1");
                }else{
                    paymentDifference.innerText ="$" +  data[i].balance;
                    paymentDifference.setAttribute("data-order", data[i].balance);
                }


                row.appendChild(jobNumber);
                row.appendChild(totalEstimate);
                row.appendChild(paymentAmount);
                row.appendChild(paymentDifference);

                document.getElementById("financialRows").appendChild(row);
            }
            startTableSort();
        },
        openJob:function(leadId){
            window.open("<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/leads/lead.php?leadId="+leadId,"_blank");
        },
        totalPaymentChart: function (data) {
            am4core.ready(function() {


                am4core.useTheme(am4themes_material);
                am4core.useTheme(am4themes_animated);
// Themes end

                chart = am4core.create("totalPaymentChart", am4charts.XYChart);

                chart.data = data;
                chart.numberFormatter.numberFormat = "'$'#.#";

                // var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                var categoryAxis = chart.xAxes.push(new am4charts.DateAxis());
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.ticks.template.disabled = true;
                categoryAxis.renderer.line.opacity = 0;
                categoryAxis.renderer.grid.template.disabled = true;
                categoryAxis.renderer.minGridDistance = 40;
                categoryAxis.dataFields.category = "paymentsDate";
                categoryAxis.startLocation = 0.4;
                categoryAxis.endLocation = 0.6;
                // categoryAxis.tooltipText = "paymentsDate: {valueX.value}";



                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.tooltip.disabled = true;
                valueAxis.renderer.line.opacity = 0;
                valueAxis.renderer.ticks.template.disabled = true;
                valueAxis.min = 0;

                var lineSeries = chart.series.push(new am4charts.LineSeries());
                // lineSeries.dataFields.categoryX = "paymentsDate";
                lineSeries.dataFields.dateX = "paymentsDate";
                lineSeries.dataFields.valueY = "payment";
                lineSeries.tooltipText = "payment: {valueY.value}";
                lineSeries.fillOpacity = 0.5;
                lineSeries.strokeWidth = 3;
                // lineSeries.propertyFields.stroke = am4core.color("#1ab394");
                // lineSeries.propertyFields.fill = am4core.color("#1ab394");
                lineSeries.stroke = am4core.color("#1ab394");
                lineSeries.fill = am4core.color("#1ab394");


                var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
                bullet.circle.radius = 6;
                bullet.circle.fill = am4core.color("#fff");
                bullet.circle.stroke = am4core.color("#1ab394");
                bullet.circle.strokeWidth = 3;

                chart.cursor = new am4charts.XYCursor();
                chart.cursor.behavior = "panX";
                chart.cursor.lineX.opacity = 0;
                chart.cursor.lineY.opacity = 0;

                chart.scrollbarX = new am4core.Scrollbar();
                chart.scrollbarX.parent = chart.bottomAxesContainer;
                // chart.zoomOutButton.disabled = true;

                // Enable export
                // chart.exporting.menu = new am4core.ExportMenu();
            }); // end am4core.ready()



        }
    }
    $(".exportChartBtn").on("click", function(evt) {
        chart.exporting.export(evt.currentTarget.id,{timeoutDelay:10000});
    });

    function init(){
        financialController.getFinancialPaymentsChartStats();
        financialController.getFinancialPaymentsTableStats();
    }

    function startTableSort() {
        $('div.dataTables_filter').remove();
        $('tfoot').show();
        var dTable = $('.financialTable').DataTable({
            "searching": true,
            "responsive": false,
            "autoWidth": false,
            "fixedColumns": true,
            "aaSorting": [],
            "oLanguage": {
                "sInfo": ""
            },
            "bFilter": false,
            "sScrollX": "100%",
            "pageLength": 50,
            "lengthChange": false,
            "columnDefs": [ {
                "targets": ["th-jobNumber"],
                "orderable": false,
                "visible": true
            } ],
            "language": {
                "paginate": {
                    "previous": "Previous ",
                    "next": " Next"
                }
            },
            "oLanguage": {
                "sSearch": "<span></span> _INPUT_",
                "searchPlaceholder": "Search records"
            },
            // dom: 'Bfrtip',
            buttons: [
                { extend: 'copyHtml5', footer: true },
                { extend: 'excelHtml5', footer: true },
                { extend: 'csvHtml5', footer: true },
                { extend: 'pdfHtml5', footer: true },
                { extend: 'print', footer: true }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                //total payments

                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                if (api.column(2, { search:'applied' }).data().length) {

                    total = api
                        .column(2, { search:'applied' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                }else{
                    total = 0;
                }

                // Total over this page
                pageTotal = api
                    .column( 2, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 2 ).footer() ).html(
                    '$'+ total.toLocaleString(undefined, {minimumFractionDigits: 2})
                );

                //total estimates

                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,N/A]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                if (api.column(1, { search:'applied' }).data().length) {

                    // Total over all pages
                    total = api
                        .column(1, { search:'applied' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                }else{
                    total = 0;
                }
                // Total over this page
                pageTotal = api
                    .column( 1, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 1 ).footer() ).html(
                    '$'+ total.toLocaleString(undefined, {minimumFractionDigits: 2})
                );

                //total remaining balance

                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,N/A]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                if (api.column(3, { search:'applied' }).data().length) {

                    // Total over all pages
                    total = api
                        .column(3, { search:'applied' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                }else{
                    total = 0;
                }
                // Total over this page
                pageTotal = api
                    .column( 3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 3 ).footer() ).html(
                    '$'+ total.toLocaleString(undefined, {minimumFractionDigits: 2})
                );


            }
        });

        $(".exportBtn").on("click", function(evt) {

            var chart = $('#totalPaymentChart').detach();
            dTable.button( '.buttons-' + evt.currentTarget.id).trigger();
            chart.appendTo($("#paymentChartWrapper"));
        });

        var info = dTable.page.info();
        if(info.pages == 1){
            $('.dataTables_paginate').hide();
        }

        $('.financialTable').DataTable();
        $('.dataTables_length').addClass('bs-select');

        $('thead').css("background-color","#F5F5F6");

        $('div.dataTables_filter').appendTo( $('#paymentSearch') );
        $('div.dataTables_filter input').attr({placeholder: "Search"});

    }
</script>