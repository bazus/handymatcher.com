
<?php
$pagePermissions = array(true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

?>

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

<style>
    table{
        font-size: 13px;
        color:rgb(103, 106, 108);
    }
    table th{
        text-align: center;
        border-bottom-width: 1px !important;
    }
    table td{
        text-align: center;
        cursor: pointer;
        vertical-align: middle !important;
    }
    table>tbody td{
        vertical-align: middle;
        padding: 5px;
    }
    body {
        background-color: #f3f3f4;
    }
    @media (max-width: 767px) {
        #exportTableList {
            position: absolute !important;
            right: 0;
            left: auto;
            background-color: #fff !important;
        }
    }
    .tableFooterTh{
        border: none !important;
    }
    #advertFooter td{
        border: none !important;
        color: rgb(103, 106, 108);
        font-weight:700;
    }
    .dataTables_scrollFootInner table {
        border: none !important;
    }
    .dataTables_filter{
        /*margin-top: 10px;*/
    }
    .dataTables_filter label {
        display: flex;
        margin-top: 13px;
        font-weight: 400;
    }
    .dataTables_filter input {
        margin-top: -3px;
        margin-left: 5px;
    }
    .ibox-title h3{
        margin-top: 0 !important;
    }

    .exportMenu{
        box-shadow: 0 0 3px rgba(86, 96, 117, 0.7) !important;
    }
</style>
<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
</script>

            <div id="wrapper" style="padding: 9px;">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox">
                                <div style="display: flex;justify-content: space-between;background-color: #ffffff;">
                                    <div class="ibox-title" style="border-color: #ffffff;"><h3>Salesman Performance</h3></div>
                                    <div style="display: flex; justify-content: space-between">
                                        <div id="salesSearch"></div>
                                        <span class="input-group-addon" style="border: none">
                                             <ul class="nav navbar-nav navbar-right" style="margin:10px 0 0 0">
                                                <li class="dropdown">
                                                  <span href="#" id="exportTableBtn" class="dropdown-toggle exportBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 7px 15px 8px 13px;">Export<span class="caret"></span></span>
                                                  <ul class="dropdown-menu exportMenu" style="margin-top: 13px;color: #000;" id="exportTableList">
                                                      <li class="exportSalesmanBtn" id="csv"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to csv</a></li>
                                                      <li class="exportSalesmanBtn" id="excel"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to excel</a></li>
                                                      <li class="exportSalesmanBtn" id="pdf"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to pdf</a></li>
                                                      <li class="exportSalesmanBtn" id="copy"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Copy</a></li>
                                                      <li class="exportSalesmanBtn" id="print"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Print</a></li>
                                                  </ul>
                                                </li>
                                              </ul>
                                        </span>
                                    </div>
                                </div>
                                <div class="ibox-content" id="salesPerformanceWrapper">
                                    <table class="table table-bordered salesmanPerformance-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Booked Jobs</th>
                                            <th>Total Leads Handled</th>
                                            <th>Sent Emails</th>
                                            <th>Customer Payments<?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?><span onclick="parent.showCustomerPaymentsInfo()"><span style="cursor:pointer;margin-left: 3px;"><i style="font-size: 16px;" class="fa fa-info-circle"></i></span></span><?php } ?></th>
                                            <th>Sales Bonus</th>
                                        </tr>
                                        </thead>
                                        <tbody id="performance">
                                        </tbody>
                                        <tfoot style="">
                                        <tr>
                                            <th class="tableFooterTh">Total</th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="ibox">
                                <div id="advertisingIboxTableHeader" style="display: flex;justify-content: space-between;background-color: #ffffff;">
                                    <div class="ibox-title" style="border-color: #ffffff;"><h3>Advertising Performance</h3></div>
                                    <div style="display: flex; justify-content: space-between">
                                        <div id="advertisingSearch"></div>
                                        <span class="input-group-addon" style="border: none">
                                             <ul class="nav navbar-nav navbar-right" style="margin:10px 0 0 0">
                                                <li class="dropdown">
                                                  <span href="#" id="exportTableBtn" class="dropdown-toggle exportBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding: 7px 15px 8px 13px;">Export<span class="caret"></span></span>
                                                  <ul class="dropdown-menu exportMenu" style="margin-top: 13px;color: #000;" id="exportTableList">
                                                      <li class="exportAdvertisingBtn" id="csv"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to csv</a></li>
                                                      <li class="exportAdvertisingBtn" id="excel"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to excel</a></li>
                                                      <li class="exportAdvertisingBtn" id="pdf"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Export to pdf</a></li>
                                                      <li class="exportAdvertisingBtn" id="copy"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Copy</a></li>
                                                      <li class="exportAdvertisingBtn" id="print"><a href="#" onclick="" style="margin: 0;padding-left: 13px;">Print</a></li>
                                                  </ul>
                                                </li>
                                              </ul>
                                        </span>
                                    </div>
                                </div>
                                <div class="ibox-content" id="salesPerformanceWrapper">
                                    <table class="table table-bordered advertisingPerformance-table">
                                        <thead>
                                            <tr>
                                                <th>Provider Name</th>
                                                <th>Total Leads</th>
                                                <th>Total Leads Spent</th>
                                                <th>Booked Jobs</th>
                                                <th>Booked</th>
                                                <th>Booked Estimates</th>
                                                <th>Customer Payments</th>
                                            </tr>
                                        </thead>
                                        <tbody id="advertPerformance">

                                        </tbody>
                                        <tfoot style="">
                                        <tr>
                                            <th class="tableFooterTh">Total</th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                            <th class="tableFooterTh"></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/dataTables/datatables.min.js"></script>
<!-- SweetAlerts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var salesController = {

        getSalesData: function () {

            var strUrl = BASE_URL + '/console/actions/reports/getSalesData.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    start: parent.theStartDate,
                    end: parent.theEndDate
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);
                var container = document.getElementById("performance");
                container.innerHTML = "";
                if (data.length > 0) {

                    if($('.salesmanPerformance-table .dataTable').length){
                        var table = $('.salesmanPerformance-table').DataTable();
                        table.clear().destroy();
                        $('tfoot').hide();
                    }
                    for (var i = 0; i < data.length; i++) {
                        salesController.setSalesTable(data[i], container);
                    }
                    salesController.startSalesmanPerformanceTableSort();
                } else {
                    var tr = document.createElement("tr");

                    var td = document.createElement("td");
                    td.setAttribute("colspan", 9);
                    td.style.textAlign = "center";
                    var tdText = document.createTextNode("No Data Yet");

                    td.appendChild(tdText);
                    tr.appendChild(td);
                    container.appendChild(tr);
                }
            });
        },
        setSalesTable: function(data, container) {


            var tr = document.createElement("tr");

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.name);
            td.setAttribute("data-order", data.name);

            td.appendChild(tdText);
            tr.appendChild(td);


            var td = document.createElement("td");
            var tdText = document.createTextNode(data.books + " (" + data.bookPercentage + "%)");
            td.setAttribute("data-order", data.books);

            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.totalLeads);
            td.setAttribute("data-order", data.totalLeads);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.totalEmailsSent);
            td.setAttribute("data-order", data.totalEmailsSent);


            td.appendChild(tdText);
            tr.appendChild(td);


            var td = document.createElement("td");
            var tdText = document.createTextNode("$" + data.payments);
            td.setAttribute("data-order", data.payments);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.salesBonusPerc);
            td.setAttribute("data-order", data.salesBonus);


            td.appendChild(tdText);
            tr.appendChild(td);

            container.appendChild(tr);
        },
        startSalesmanPerformanceTableSort: function () {
            $('div#DataTables_Table_0_filter').remove();
            $('tfoot').show();
            var dTableSaleman = $('.salesmanPerformance-table').DataTable({
                "order": [[ 1, "asc" ]],
                "searching": true,
                "responsive": false,
                "autoWidth": false,
                "fixedColumns": true,
                "aaSorting": [],
                "oLanguage": {
                    "sInfo": ""
                },
                "bFilter": false,
                "sScrollX": "100%",
                "pageLength": 20,
                "lengthChange": false,
                "columnDefs": [ {
                    "targets": [],
                    "orderable": false,
                    "visible": true
                } ],
                "language": {
                    "paginate": {
                        "previous": "Previous ",
                        "next": " Next"
                    }
                },
                "oLanguage": {
                    "sSearch": "<span></span> _INPUT_",
                    "searchPlaceholder": "Search records"
                },
                // dom: 'Bfrtip',
                buttons: [
                    { extend: 'copyHtml5', footer: true },
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true },
                    { extend: 'pdfHtml5', footer: true },
                    { extend: 'print', footer: true }
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    //total Booked Jobs
                    var api = this.api();
                    api.columns(1, {
                        search: 'applied'
                    }).every(function() {
                        var sum = this
                            .nodes()
                            .reduce(function(a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat($(b).attr('data-order')) || 0;
                                return x + y;
                            }, 0);
                        $(this.footer()).html(sum);
                    });

                    //Total Leads Handled

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,N/A]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    if (api.column(2, { search:'applied' }).data().length) {

                        // Total over all pages
                        total = api
                            .column(2,{ search:'applied' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else{
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 2 ).footer() ).html(
                        total.toLocaleString()
                    );

                    //Total Sent Emails

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,N/A]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    if (api.column(3, { search:'applied' }).data().length) {

                        // Total over all pages
                        total = api
                            .column(3, { search:'applied' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else{
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 3 ).footer() ).html(
                        total.toLocaleString()
                    );

                    //Total Customer Payments

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,N/A]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    if (api.column(4, { search:'applied' }).data().length) {

                        // Total over all pages
                        total = api
                            .column(4,{ search:'applied' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else{
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        '$'+ total.toLocaleString(undefined, {minimumFractionDigits: 2})
                    );

                    //Total Sales Bonus
                    var api = this.api();
                    api.columns(5, {
                        search: 'applied'
                    }).every(function() {
                        var sum = this
                            .nodes()
                            .reduce(function(a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat($(b).attr('data-order')) || 0;
                                return x + y;
                            }, 0);
                        $(this.footer()).html("$" + sum.toLocaleString(undefined, {minimumFractionDigits: 2}));
                    });

                }
            });

            $(".exportSalesmanBtn").on("click", function(evt) {

                dTableSaleman.button( '.buttons-' + evt.currentTarget.id).trigger();
            });

            var info = dTableSaleman.page.info();
            if(info.pages == 1){
                $('.dataTables_paginate').hide();
            }

            $('.salesmanPerformance-table').DataTable();
            $('.dataTables_length').addClass('bs-select');

            $('thead').css("background-color","#F5F5F6");

            $('div#DataTables_Table_0_filter').appendTo( $('#salesSearch') );
            $('div.dataTables_filter input').attr({placeholder: "Search"});


        },
        getAdvertisingData: function (){
            try {
                var strUrl = BASE_URL + '/console/actions/reports/getAdvertisingData.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        start: parent.theStartDate,
                        end: parent.theEndDate
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {

                    data = JSON.parse(data);

                    if($('.salesmanPerformance-table .dataTable').length){
                        var table = $('.salesmanPerformance-table').DataTable();
                        table.clear().destroy();
                        $('tfoot').hide();
                    }
                    var container = document.getElementById("advertPerformance");
                    container.innerHTML = "";
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            salesController.setAdvertisingTable(data[i], container);
                        }

                        salesController.startAdvertisingPerformanceTableSort();
                        setTimeout(function(){
                            parent.reportsController.iframeHeight();
                        }, 100);

                    } else {
                        var tr = document.createElement("tr");

                        var td = document.createElement("td");
                        td.setAttribute("colspan", 6);
                        td.style.textAlign = "center";
                        var tdText = document.createTextNode("No Data Yet");

                        td.appendChild(tdText);
                        tr.appendChild(td);
                        container.appendChild(tr);
                    }
                });
                }catch (e) {

                }
        },
        setAdvertisingTable:function (data,container){

            var tr = document.createElement("tr");

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.name);
            td.setAttribute("data-order", data.name);

            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.totalLeads);
            td.setAttribute("data-order", data.totalLeads);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText =document.createTextNode("$" + data.totalLeadsSpent);
            td.setAttribute("data-order", data.totalLeadsSpent);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.totalBooked);
            td.setAttribute("data-order", data.totalBooked);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode(data.bookChange+"%");
            td.setAttribute("data-order", data.bookChange);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode("$"+data.bookedEstimate);
            td.setAttribute("data-order", data.bookedEstimate);


            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            var tdText = document.createTextNode("$"+data.customerPayments);
            td.setAttribute("data-order", data.customerPayments);


            td.appendChild(tdText);
            tr.appendChild(td);

            container.appendChild(tr);

        },
        startAdvertisingPerformanceTableSort: function () {
            $('div#DataTables_Table_1_filter').remove();
            $('tfoot').show();
            var dTableAdvertising = $('.advertisingPerformance-table').DataTable({
                "order": [[ 1, "asc" ]],
                "searching": true,
                "responsive": false,
                "autoWidth": false,
                "fixedColumns": true,
                "aaSorting": [],
                "oLanguage": {
                    "sInfo": ""
                },
                "bFilter": false,
                "sScrollX": "100%",
                "pageLength": 50,
                "lengthChange": false,
                "columnDefs": [ {
                    "targets": [],
                    "orderable": false,
                    "visible": true
                } ],
                "language": {
                    "paginate": {
                        "previous": "Previous ",
                        "next": " Next"
                    }
                },
                "oLanguage": {
                    "sSearch": "<span></span> _INPUT_",
                    "searchPlaceholder": "Search records"
                },
                // dom: 'Bfrtip',
                buttons: [
                    { extend: 'copyHtml5', footer: true },
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true },
                    { extend: 'pdfHtml5', footer: true },
                    { extend: 'print', footer: true }
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    //total leads

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    if (api.column(1, { search:'applied' }).data().length) {

                        total = api
                            .column(1 , { search:'applied' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else{
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 1 ).footer() ).html(
                        total.toLocaleString()
                    );

                    //total leads spent

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,N/A]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    if (api.column(2, { search:'applied' }).data().length) {

                        // Total over all pages
                        total = api
                            .column(2, { search:'applied' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else{
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 2 ).footer() ).html(
                        "$" + total.toLocaleString(undefined, {minimumFractionDigits: 2})
                    );

                    //Total Booked Jobs

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,N/A]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    if (api.column(3, { search:'applied' }).data().length) {

                        // Total over all pages
                        total = api
                            .column(3, { search:'applied' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else{
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 3 ).footer() ).html(
                        total.toLocaleString()
                    );

                    //Total Booked

                    if (api.column(4, { search:'applied' }).data().length) {

                        var totalSearchR = api.column(4, {search: 'applied'}).data().length;
                        var api = this.api();
                        api.columns(4, {
                            search: 'applied'
                        }).every(function () {
                            var sum = this
                                .nodes()
                                .reduce(function (a, b) {
                                    var x = parseFloat(a) || 0;
                                    var y = parseFloat($(b).attr('data-order')) || 0;
                                    return x + y;
                                }, 0);
                            if (totalSearchR > 1) {
                                $(this.footer()).html((sum / totalSearchR).toLocaleString(undefined, {minimumFractionDigits: 2}) + "% AVG");
                            } else {
                                $(this.footer()).html((sum / totalSearchR).toLocaleString(undefined, {minimumFractionDigits: 2}) + "%");
                            }
                        });
                    }else {
                        $( api.column( 4 ).footer() ).html(
                            0+"%"
                        );
                    }


                    //Total Booked Estimates

                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,N/A]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    if (api.column(5, { search:'applied' }).data().length) {

                        // Total over all pages
                        total = api
                            .column(5, {search: 'applied'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    }else {
                        total = 0;
                    }

                    // Total over this page
                    pageTotal = api
                        .column( 5, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 5 ).footer() ).html(
                        "$" + total.toLocaleString(undefined, {minimumFractionDigits: 2})
                    );

                    //Total Customer Payments

                    var api = this.api();
                    api.columns(6, {
                        search: 'applied'
                    }).every(function() {
                        var sum = this
                            .nodes()
                            .reduce(function(a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat($(b).attr('data-order')) || 0;
                                return x + y;
                            }, 0);
                        $(this.footer()).html("$" + sum.toLocaleString(undefined, {minimumFractionDigits: 2}));
                    });

                }
            });

            $(".exportAdvertisingBtn").on("click", function(evt) {

                dTableAdvertising.button( '.buttons-' + evt.currentTarget.id).trigger();
            });

            var info = dTableAdvertising.page.info();
            if(info.pages == 1){
                $('.dataTables_paginate').hide();
            }

            $('.advertisingPerformance-table').DataTable();
            $('.dataTables_length').addClass('bs-select');

            $('thead').css("background-color","#F5F5F6");

            $('div#DataTables_Table_1_filter').appendTo( $('#advertisingSearch') );
            $('div.dataTables_filter input').attr({placeholder: "Search"});

        }
    }
    function init(){
        $('.salesmanPerformance-table').DataTable().clear().destroy();
        $('.advertisingPerformance-table').DataTable().clear().destroy();

        salesController.getSalesData();
        salesController.getAdvertisingData();
    }

</script>