<?php
$isCalledFromModal = true;
$pagePermissions = array(false,array(1));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");
$leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
$providers = $leadProviders->getProviders(true);
?>
<script>var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";</script>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Insert New Lead</h4>
                <small>You can manually insert leads to the system.</small>
            </div>
                <div class="modal-body" style="padding-top: 0px;padding-bottom: 9px;">
                    <div class="row">
                        <div class="col-md-12 modal-body">
                           <h1>AAA</h1>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-white" id="closeBtn" data-dismiss="modal">Cancel</button>
                    <button class="ladda-button btn btn-primary" id="testing" data-style="zoom-out" onclick="test()">
                        <span class="ladda-label">Insert Lead</span>
                        <span class="ladda-spinner"></span>
                    </button>
                </div>

        </div>
    </div>
</div>

<script>
    l = $('#testing').ladda();

    function test(){
        l.ladda("start");
    }
</script>