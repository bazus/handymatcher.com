<?php


// This file is not suppose to be in use anymore !
// I've moved this to 'getCustomEmail.php'
// But I'm too afraid to delete this

require_once($_SERVER['LOCAL_NL_PATH']."/console/connect/connect.php");
$connect = new connect();

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

$leadId = $_GET['leadId'];
$orgId = $_GET['orgId'];

$lead = new lead($leadId,$orgId);
$leadData = $lead->getData();

$organization = new organization($orgId);
$organizationData = $organization->getData();
$organizationPicture = $organizationData['logoPath'];

?>

<!DOCTYPE html><html>

<head>
    <style>
body{
    margin: 0;
}
        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;font-family: \'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;display:block;width:600px;max-width:600px;margin:0 auto!important" valign="top" width="600">

            <div style="box-sizing:border-box;display:block;max-width:600px;margin:0 auto;padding:10px">

                <div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                    <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                        <tbody>

                        <tr>

                            <td align="center" style="box-sizing:border-box;padding:0;font-family:'Open Sans','Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>

                                           <?php if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){ ?>
                                               <img class="logoimg" src="<?php echo $organizationPicture; ?>" style="height: 55px;" alt="<?php echo $organizationData["organizationName"]; ?>'s Logo">

                                           <?php }else{ ?>
                                               <img class="logoimg" src="https://d2o9xrcicycxrg.cloudfront.net/home/images/NLwebsiteFL2.png" style="height: 55px;" alt="<?php echo $organizationData["organizationName"]; ?>'s Logo">
                                           <?php } ?>
                                         <!--   <img alt="Network Leads" height="22" src="https://d2o9xrcicycxrg.cloudfront.net/images/logo_small.png" style="max-width:100%;border-style:none;width:145px;height:auto" width="123">
                                       -->
                                    </span>

                            </td>

                        </tr>

                        </tbody>

                    </table>

                </div>


            <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                    <tbody>

                    <tr>

                        <td style="box-sizing:border-box;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;padding:30px" valign="top">

                            <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                <tbody>

                                <tr>

                                    <td style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top" valign="top">


                                        Dear <?= $leadData['firstname'] ?>,<br>

                                        <p style="margin:0;margin-bottom:30px;color:#294661;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;font-weight:300">

                                            Your free moving estimate is ready for you to review.<br>
                                            Please <a href="<?php echo $_SERVER["YMQ_URL"]."/".$leadData['secretKey'] ?>" target="_blank">click here</a> to go over the inclusions and parameters and if you wish, you can e-sign and lock your great price in!<br>
                                            <br>
                                            Sincerely,<br>
                                            <?= $organizationData['organizationName'] ?>
                                        </p>

                                    </td>

                                </tr>

                                </tbody>

                            </table>
                        </td>

                    </tr>

                    </tbody>

                </table>

            </div>
</div>

</td>


</tr>

</tbody>

</table>
</div>

</body>

</html>






