<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/notifications.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/actions/init/timeElapsed.php");

$notifications = new notifications($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$bouncer["userData"]["isAdmin"]);

?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Send Notification</h4>
            </div>
            <form id="notificationBox" action="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/user/addNotification.php" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="group">Send To Group:</label>
                        <select name="group" class="form-control">
                            <option value="1">User</option>
                            <option value="2">Group</option>
                            <option value="3">Admin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id">ID:</label>
                        <input type="number" class="form-control" name="id">
                    </div>
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <input type="text" class="form-control" name="description">
                    </div>
                </div>
                <div class="modal-footer">
                    <p class="pull-left">
                        <small class="text-muted"></small>
                    </p>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in">Add Notification</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#notificationBox').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {
        l.ladda( 'stop' );
        $('#myCustomModal').modal('hide');

        if(statusText == "success"){
            var data = responseText;
            data = JSON.parse(data);
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };

            if(data == true){
                location.reload();
            }else{
                // Failed
                toastr.error('Your request was not sent, please try again later.','Error');
            }
        }else{
            // Failed
            toastr.error('2Your request was not sent, please try again later.','Error');
        }
    }
</script>