<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/notifications.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/actions/init/timeElapsed.php");

$notifications = new notifications($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$bouncer["userData"]["isAdmin"]);
$notification = $notifications->getNotification($_GET['id'],2);
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-header" style="text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Organization Message From "<?= $notification['fullName'] ?>"</h4>
            </div>
            <form id="notificationBox" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label><?= $notification['title'] ?></label>
                        <p>
                            <?= $notification['description'] ?>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <p class="pull-left">
                        <small class="text-muted">Received <?=time_elapsed_string($notification['created_at'])?></small>
                    </p>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in">Mark As Read</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    var options = {
        beforeSubmit: beforeSubmit,  // pre-submit callback
        success: afterSubmit  // post-submit callback
    };

    $('#notificationBox').ajaxForm(options);

    function beforeSubmit() {
        l.ladda( 'start' );
    }
    function afterSubmit(responseText, statusText, xhr, $form) {

        var strUrl = "<?= $_SERVER['LOCAL_NL_URL'] ?>/console/actions/user/setNotificationAsRead.php", strReturn = "";
        var postId = <?= $notification['id'] ?>;
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {
                postId: postId,
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda( 'stop' );
            $('#myCustomModal').modal('hide');
            location.reload();
        });

    }
</script>