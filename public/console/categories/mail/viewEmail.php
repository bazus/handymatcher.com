<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
$mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);
if ($bouncer["credentials"]["userId"] !== $_GET['userSent']){
    if ($bouncer['isUserAnAdmin']){
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

        $userClass = new user($_GET['userSent'],false);
        if ($userClass->isValid == true){
            $email = $mailCenter->getEmail($_GET['id'],$_GET['userSent']);
            if (!$email){
                header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
                exit;
            }
        }else{
            header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
            exit;
        }

    }else{
        header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
        exit;
    }
}else{
    if (isset($_GET['id'])){
        $email = $mailCenter->getEmail($_GET['id'],$bouncer["credentials"]["userId"]);
    }else{
        header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
        exit;
    }
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Outgoing Emails</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Froala Editor -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/froala_editor.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/froala_style.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/plugins/table.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/plugins/colors.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/plugins/quick_insert.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/MailView.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailOutgoing.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>
    <script>

        function iframeLoaded() {
            var iFrameID = document.getElementById('idIframe');
            if(iFrameID) {
                // here you can make the height, I delete it first, then I make it again
                iFrameID.height = "";
                iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
            }
        }
    </script>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        Mail Center
                    </li>
                    <li>
                        <a href="<?=$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php"?>">Outgoing Mails</a>
                    </li>
                    <li class="active">
                        <strong>View Email</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 animated fadeInRight" style="margin-bottom: 29px;">
                <div class="mail-box-header">
                    <div class="pull-left tooltip-demo">
                        <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/outgoing.php" class="btn btn-white btn-sm"><i class="fa fa-times"></i> Close</a>
                    </div>
                    <span class="pull-right font-normal" style="text-align: right;">
                        Sent At: <b><?= date("F j, Y H:i",strtotime($email['sentDate'])) ?></b>
                        <br>
                        Opened At: <b>
                            <?php
                                if($email['didOpen'] == 0){
                                    echo '<span style="color: #F44336">Not Opened</span>';
                                }else {
                                    echo date("F j, Y H:i",strtotime($email['openDate']));
                                }
                            ?>
                        </b>
                    </span>

                    <br><br>
                    <div class="mail-tools tooltip-demo m-t-md">


                        <h3>
                            <span class="font-normal">Subject: </span><?= $email['subject'] ?>
                        </h3>
                        <span class="font-normal">From: </span><?= $email['fromEmail'] ?><br>
                        <span class="font-normal">To: </span><?= $email['toEmail'] ?>
                    </div>
                </div>
                <div class="mail-box">


                    <div class="mail-body">
                        <iframe id="idIframe" onload="iframeLoaded()" style="width: 100%;border: none" src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/previewSentEmail.php?id=<?php echo $email['id'].'&userSent='.$_GET['userSent']; ?>"></iframe>
                    </div>
                    <div class="clearfix"></div>


                </div>
            </div>
        </div>


        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<!-- Froala Editor -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/froala_editor.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/table.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/font_family.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/font_size.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/colors.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/inline_style.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/paragraph_style.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/paragraph_format.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/align.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/quote.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/link.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/js/plugins/quick_insert.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/mail/viewEmail.min.js"></script>


</body>
</html>

