<?php
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

?>
<!--<link href="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">-->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/bootstrapSocial/bootstrap-social.css" rel="stylesheet">

<style>
    #myCustomModal .passRulesList{
        margin-bottom: 0px;
        list-style: none;
        margin-left: -36px;
    }
    #myCustomModal .my-btn{
        border-color: rgba(210, 210, 210, 0.64) !important;
    }
    #myCustomModal .active{
        border-color: rgba(0, 0, 0, 0) !important;
        opacity: 0.6;
    }
    @media only screen and (max-width: 750px) {
        .btn-social>:first-child{
            display: none !important;
        }
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-body">
                <label>Account Type</label>
                <div class="btn-group">
                    <!--<button class="btn my-btn btn-white btn-google btn-social active" id="changeType2" type="button" onclick="changeType(2)"><span class="fa fa-google"></span> Google</button>-->
                    <button class="btn my-btn btn-white active" id="changeType5" type="button" onclick="changeType(5)"><span class="fa fa-google"></span>mail</button>
                    <button class="btn my-btn btn-white" id="changeType3" type="button" onclick="changeType(3)"><span class="fa fa-yahoo"></span>ahoo</button>
                    <button class="btn my-btn btn-white" id="changeType4" type="button" onclick="changeType(4)"><span class="fa fa-windows"></span> Office 365</button>
                    <button class="btn my-btn btn-white" id="changeType1" type="button" onclick="changeType(1)">Custom</button>
                </div>
                <div class="hr-line-dashed"></div>
                <div id="account_type_1" style="display: none;">
                    <form method="post" class="form-horizontal">

                        <div class="form-group"><label class="col-sm-4 control-label">Outgoing Mail Server</label>
                            <div class="col-sm-8"><input type="text" name="outgoingmailserver" id="outgoingmailserver" onkeyup="checkForm()" placeholder="Example: smtp.gmail.com" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Full Name<?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?><a onclick="showFullNameInfo()" style="color: unset;"><i id="popoverme" class="fa fa-info-circle" style="cursor: pointer;margin-left: 3px;" data-original-title="" title=""></i></a><?php } ?></label>
                            <div class="col-sm-8"><input type="text" name="fullName_1" id="fullName_1" onkeyup="checkForm()" placeholder="Full Name" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Address</label>
                            <div class="col-sm-8"><input type="email" name="emailAddress_1" id="emailAddress_1" onkeyup="checkForm()" placeholder="Email Address" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Password</label>
                            <div class="col-sm-8"><input type="password" name="emailPassword_1" id="emailPassword_1" onkeyup="checkForm()" placeholder="Email Password" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Port</label>
                            <div class="col-sm-8"><input type="text" name="emailPort" id="emailPort" onkeyup="checkForm()" placeholder="Example : 587" class="form-control"></div>
                        </div>
                    </form>
                </div>
               <!-- <div id="account_type_2" style="">

                    <form method="post" class="form-horizontal">

                        <div class="form-group"><label class="col-sm-4 control-label">Full Name<?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?><a onclick="showFullNameInfo()" style="color: unset;"><i id="popoverme" class="fa fa-info-circle" style="cursor: pointer;margin-left: 3px;" data-original-title="" title=""></i></a><?php } ?></label>
                            <div class="col-sm-8"><input type="email" name="fullName_2" id="fullName_2" placeholder="Full Name" class="form-control" onkeyup="checkForm()"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Address</label>
                            <div class="col-sm-8"><input type="email" name="emailAddress_2" id="emailAddress_2" placeholder="Email Address" class="form-control" onkeyup="checkForm()"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Password</label>
                            <div class="col-sm-8"><input type="password" name="emailPassword_2" id="emailPassword_2" placeholder="Email Password" class="form-control" onkeyup="checkForm()"></div>
                        </div>
                        <p>
                            Make sure the email settings is turned ON for <a href="https://myaccount.google.com/lesssecureapps" target="_blank" title="allow less secure apps" style="text-decoration: underline">Less Secure Apps</a>.<br>
                            For more info <a href="https://support.google.com/a/answer/6260879?hl=en" target="_blank" title="allow less secure apps" style="text-decoration: underline">click here</a>.
                        </p>

                    </form>

                </div>-->
                <div id="account_type_3" style="display: none">

                    <form method="post" class="form-horizontal">

                        <div class="form-group"><label class="col-sm-4 control-label">Full Name<?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?><a onclick="showFullNameInfo()" style="color: unset;"><i id="popoverme" class="fa fa-info-circle" style="cursor: pointer;margin-left: 3px;" data-original-title="" title=""></i></a><?php } ?></label>
                            <div class="col-sm-8"><input type="email" name="fullName_3" id="fullName_3" placeholder="Full Name" class="form-control" onkeyup="checkForm()"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Address</label>
                            <div class="col-sm-8"><input type="email" name="emailAddress_3" id="emailAddress_3" placeholder="Email Address" class="form-control" onkeyup="checkForm()"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-4 control-label">Email Password</label>
                            <div class="col-sm-8"><input type="password" name="emailPassword_3" id="emailPassword_3" placeholder="Email Password" class="form-control" onkeyup="checkForm()"></div>
                        </div>
                        <p>
                            Make sure the email settings is turned ON for <a href="https://help.yahoo.com/kb/SLN27791.html" target="_blank" title="allow less secure apps" style="text-decoration: underline">Less Secure Apps</a>.<br>
                            If you have a business account you need to <a href="https://help.smallbusiness.yahoo.net/s/article/SLN29264" target="_blank" title="Create an app password" style="text-decoration: underline">create an app password</a>.
                        </p>

                    </form>

                </div>
                <div id="account_type_4" style="display: none">

                        <form method="post" class="form-horizontal">

                            <div class="form-group"><label class="col-sm-4 control-label">Full Name<?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?><a onclick="showFullNameInfo()" style="color: unset;"><i id="popoverme" class="fa fa-info-circle" style="cursor: pointer;margin-left: 3px;" data-original-title="" title=""></i></a><?php } ?></label>
                                <div class="col-sm-8"><input type="email" name="fullName_4" id="fullName_4" placeholder="Full Name" class="form-control" onkeyup="checkForm()"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Email Address</label>
                                <div class="col-sm-8"><input type="email" name="emailAddress_4" id="emailAddress_4" placeholder="Email Address" class="form-control" onkeyup="checkForm()"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Email Password</label>
                                <div class="col-sm-8"><input type="password" name="emailPassword_4" id="emailPassword_4" placeholder="Email Password" class="form-control" onkeyup="checkForm()"></div>
                            </div>

                        </form>

                    </div>
                <div id="account_type_5" style="text-align: center">
                  <a href="<?php  echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/API/authorizeGoogleAPI.php?app=gmail">
                      <img src="https://d2o9xrcicycxrg.cloudfront.net/home/images/icons/google/btn_google_signin_dark_normal_web@2x.png" style="width: 173px;" />
                  </a>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="createAccount()" id="createAccount" disabled>Create</button>
            </div>
        </div>
    </div>
</div>

<!--<script src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>-->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    function showFullNameInfo(){
        swal({
            title: "Full Name",
            text: "This is the Name that will feature in your outgoing emails from the software and what recipients will see in their inbox.",
            buttons: {
                skip:{
                    text:"Got It"
                }
            }
        });
    }

    function createAccount(){
        l.ladda( 'start' );

        var outgoingmailserver = "";
        var fullName = "";
        var emailAddress = "";
        var emailPassword = "";
        var emailPort = "";

        if(mailType == 1){
            outgoingmailserver = document.getElementById('outgoingmailserver').value;
            fullName = document.getElementById('fullName_1').value;
            emailAddress = document.getElementById('emailAddress_1').value;
            emailPassword = document.getElementById('emailPassword_1').value;
            emailPort = document.getElementById('emailPort').value;
        }

        if(mailType == 2){
            /*
            fullName = document.getElementById('fullName_2').value;
            emailAddress = document.getElementById('emailAddress_2').value;
            emailPassword = document.getElementById('emailPassword_2').value;
        */
        }
        if(mailType == 3){
            fullName = document.getElementById('fullName_3').value;
            emailAddress = document.getElementById('emailAddress_3').value;
            emailPassword = document.getElementById('emailPassword_3').value;
        }
        if(mailType == 4){
            fullName = document.getElementById('fullName_4').value;
            emailAddress = document.getElementById('emailAddress_4').value;
            emailPassword = document.getElementById('emailPassword_4').value;
        }


        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/createAccount.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                outgoingmailserver:outgoingmailserver,
                fullName:fullName,
                emailAddress:emailAddress,
                emailPassword:emailPassword,
                emailPort:emailPort,
                accountType:mailType
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            l.ladda('stop');

            try{
                data = JSON.parse(data);

                $('#myCustomModal').modal('hide');

                if(data == true){
                    swal({title:"Success!", text:"Your account has been added",icon: "success",
                        buttons: {
                         ok:"Ok"
                        }
                    }).then((isConfirm)=>{
                        parent.getEmailsAccountData();
                    });

                }else{
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }
            }catch (e){
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }

        });
    };

    var mailType = 5;
    function changeType(typeNumber) {
        mailType = typeNumber;
        document.getElementById('changeType1').className = 'btn my-btn btn-white';
        document.getElementById('changeType3').className = 'btn my-btn btn-white';
        document.getElementById('changeType4').className = 'btn my-btn btn-white';
        document.getElementById('changeType5').className = 'btn my-btn btn-white';

        document.getElementById('account_type_1').style.display = 'none';
        document.getElementById('account_type_3').style.display = 'none';
        document.getElementById('account_type_4').style.display = 'none';
        document.getElementById('account_type_5').style.display = 'none';

        document.getElementById('changeType'+typeNumber).classList.add("active");
        document.getElementById('account_type_'+typeNumber).style.display = '';
        checkForm();
    }

    function checkForm(){
        document.getElementById('createAccount').disabled = true;

        if(mailType == 1){
            var outgoingmailserver = document.getElementById('outgoingmailserver').value;
            var fullName_1 = document.getElementById('fullName_1').value;
            var emailAddress_1 = document.getElementById('emailAddress_1').value;
            var emailPassword_1 = document.getElementById('emailPassword_1').value;
            var emailPort = document.getElementById('emailPort').value;

            if(fullName_1 != "" && outgoingmailserver != "" && emailAddress_1 != "" && emailPassword_1 != "" && emailPort != ""){
                document.getElementById('createAccount').disabled = false;
            }
        }

        if(mailType == 2){
            /*
            var fullName_2 = document.getElementById('fullName_2').value;
            var emailAddress_2 = document.getElementById('emailAddress_2').value;
            var emailPassword_2 = document.getElementById('emailPassword_2').value;

            if(fullName_2 != "" && emailAddress_2 != "" && emailPassword_2 != ""){
                document.getElementById('createAccount').disabled = false;
            }
             */
        }
        if(mailType == 3){
            var fullName_3 = document.getElementById('fullName_3').value;
            var emailAddress_3 = document.getElementById('emailAddress_3').value;
            var emailPassword_3 = document.getElementById('emailPassword_3').value;

            if(fullName_3 != "" & emailAddress_3 != "" && emailPassword_3 != ""){
                document.getElementById('createAccount').disabled = false;
            }
        }
        if(mailType == 4){
            var fullName_4 = document.getElementById('fullName_4').value;
            var emailAddress_4 = document.getElementById('emailAddress_4').value;
            var emailPassword_4 = document.getElementById('emailPassword_4').value;

            if(fullName_4 != "" && emailAddress_4 != "" && emailPassword_4 != ""){
                document.getElementById('createAccount').disabled = false;
            }
        }
    }
    checkForm();

</script>