<?php
if(!isset($_GET['accountId'])){
    header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/');
    exit;
}else{
    $accountId = $_GET['accountId'];
}
$isCalledFromModal = true;
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

?>
<!--<link href="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">-->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<style>
    .passRulesList{
        margin-bottom: 0px;
        list-style: none;
        margin-left: -36px;
    }
</style>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myCustomModal" id="execModal" style="display: none"></button>
<div class="modal inmodal" id="myCustomModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">

        <div class="modal-content animated fadeIn">
            <div class="modal-body">
                <div class="form-group" id="passwordGroup">
                    <label for="password">password for email account:</label>
                    <input id="password" onkeyup="checkPass(this)" type="password" class="form-control" placeholder="Password" />
                </div>
                * Your password is encrypted and stored securely
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <button type="button" class="ladda-button ladda-button-demo btn btn-primary" data-style="zoom-in" onclick="saveNewPassword()" id="saveNewPassword" disabled>Update Password</button>
            </div>
        </div>
    </div>
</div>
<!--<script src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>-->
<script async src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>
    var l;
    $(document).ready(function () {
        l = $('.ladda-button-demo').ladda();
    });

    function saveNewPassword(){
        l.ladda( 'start' );

        var password = document.getElementById('password').value;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/updateAccountPassword.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                accountId:'<?php echo $accountId; ?>',
                password:password
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            l.ladda('stop');

            try{
                data = JSON.parse(data);

                $('#myCustomModal').modal('hide');

                if(data == true){
                    swal({
                        title: "Success!",
                        text: "You changed your password",
                        icon: "success",
                        buttons: {
                            ok:"Ok"
                        }
                    });
                }else{
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }
            }catch (e){
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }

        });
    }

    function checkPass(obj) {
        if(obj.value == ""){
            document.getElementById('saveNewPassword').disabled = true;
        }else{
            document.getElementById('saveNewPassword').disabled = false;
        }
    }

</script>