<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$id = $_GET['id'];

$data = array();
$data["organizationName"] = $bouncer["organizationData"]["organizationName"];
$data["organizationLogo"] = $bouncer["organizationData"]["logoPath"];
$data["departmentTitle"] = $bouncer["userData"]["departmentTitle"];
$data["userName"] = $bouncer['userData']["fullName"];
$data["organizationPhone"] = $bouncer["organizationData"]["phone"];
$data["organizationAddress"] = $bouncer["organizationData"]["address"];
$data["organizationCity"] = $bouncer["organizationData"]["city"];
$data["organizationState"] = $bouncer["organizationData"]["state"];
$data["organizationZip"] = $bouncer["organizationData"]["zip"];
$data["organizationWebsite"] = $bouncer["organizationData"]["website"];


$preEmailData = array();
switch($id){
    case "pre-1":
        $preEmailData = $preEmails->getPreEmail(1,$data);
        break;
    default:
        $preEmailData = $preEmails->getDataById($id);
}
if (!$preEmailData){
    header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/templates.php");
    exit;
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Mailbox Accounts</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>
</head>

<body>
    <div class="row">
        <div class="m-t-lg col-lg-offset-1 col-lg-10 col-lg-offset-1  animated fadeInRight">
            <div class="mail-box-header">
                <h2>
                    View Message
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="pull-right tooltip-demo">
                        <a class="btn btn-default" href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/templates.php">Go Back</a>
                    </div>
                    <h3><span class="font-normal">Subject: </span><?= $preEmailData['subject'] ?></h3>
                    <h5><span class="font-normal">Title: </span><?= $preEmailData['title'] ?></h5>
                </div>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                   <?php
                   echo html_entity_decode($preEmailData['content']); ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</body>
</html>

