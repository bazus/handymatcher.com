<?php
$pagePermissions = array(false,true,true,true);
// todos change permissions by package
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");

$organization = new organization($bouncer["credentials"]["orgId"]);
if ($bouncer['isUserAnAdmin'] == true) {
    $usersOfMyCompany = $organization->getUsersOfMyCompany();
}
$orgId = $bouncer["credentials"]["orgId"];
$loadFooterHelp = false;
$email = new email();

$emailsData = $email->isLimitValid($bouncer["credentials"]["orgId"],true);

if($emailsData['emailsLimit'] >= 10000){$emailsData['emailsLimit'] = "unlimited";}
$totalEmailsOfLimit = "Total emails sent this month: ".$emailsData['totalEmailsSent']." / ".$emailsData['emailsLimit'];
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Mail</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailOutgoing.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>

</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">

<style>

</style>
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        Marketing
                    </li>
                    <li  class="active">
                        <strong>Outgoing E-Mails</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 animated" style="margin-bottom: 29px;">
                <div class="mail-box-header">
                    <div class="row" style="margin: 0px;padding-bottom:10px;display: flex; flex-wrap: wrap;justify-content: space-between">
                        <div style="display: flex; margin-right:auto;flex-wrap: wrap;">
                            <div id="dateRangeOutgoingEmails" class="form-control" style="margin-bottom: 5px;width: max-content;width:-moz-max-content;">
                                <i class="fa fa-calendar"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                            <div id="composeNewEmailWrapper">
                                <?php $checkAuths_mailcenter = $bouncer["userAuthorization"]->checkAuthorizations(array(["mailcenter",3]));
                                if ($checkAuths_mailcenter){
                                    ?>
                                    <a onclick="sendEmailsModal()"><button id="composeNewEmail" class="btn btn-primary">Compose New Email</button></a>
                                <?php }else{ ?>
                                    <a><button id="composeNewEmail" class="btn btn-primary" disabled>Compose New Email</button></a>
                                <?php } ?>
                            </div>
                            <div id="allTheUnsubscribesWrapper" style="margin-bottom: 5px;">
                                <button type="button" id="allTheUnsubscribes" class="btn btn-danger buttonUnsubscribers" name="<?php echo $orgId ?>" onclick="showCustomModal('categories/iframes/showUnsubscribers.php')">list of unsubscribers</button>
                            </div>
                        </div>
                        <div id="searchAndSelectUserContainer" style="display: flex;flex-wrap: wrap;">
                            <div class="input-group" style="margin-left: 0px;margin-right: 15px;" id="searchBarContainer"></div>
                            <?php if ($bouncer['isUserAnAdmin'] == true){ ?>
                                <select style="height: 36px;width: 150px;" id="userSentSelect">
                                    <option value="">Show All</option>
                                    <?php foreach ($usersOfMyCompany as $user){ ?>
                                        <option value="<?= $user['id'] ?>"><?= $user['fullName'] ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </div>
                    </div>

                </div>

                <div class="mail-box table-responsive" style="padding:0 20px;">
                    <table class="table table-bordered table-mail dataTables-outgoingEmails" style="margin-bottom: 0px;">
                        <thead>
                            <th class="th-sentBy" style="min-width: 200px;">Sent By</th>
                            <th class="th-from" style="width: 300px;">From</th>
                            <th class="th-to" style="width: 300px;">To</th>
                            <th class="th-subject" style="">Subject</th>
                            <th class="th-dateSent" style="min-width: 150px;">Date Sent</th>
                            <th class="th-opened" style="width: 150px;">Opened</th>
                        </thead>
                        <tbody id="emailsContainer">
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6" style="text-align: right"><?= $totalEmailsOfLimit; ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>
</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<!--<script src="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>-->

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/moment.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/dataTables/datatables.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/mail/outgoing.min.js?v=1.3.7.4"></script>



<?php
//if($bouncer["userSettingsData"]["firstTutorial"] == "1" && $bouncer['isUserAnAdmin'] == true) {
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/admin/adminTutorial_stage_email_step_3.php");
//}else if ($bouncer["userSettingsData"]['firstTutorial'] == "1" && $bouncer['isUserAnAdmin'] == false){
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/user/userTutorial_stage_email_step_3.php");
//}
?>
</body>
</html>

