<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

$pagePermissions = array(false,true,true,true);
// todos change permissions by package
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$myAccounts = $mailaccounts->getMyAccounts(true);
$organization = new organization($bouncer["credentials"]["orgId"]);
$defaultAccount = $organization->getDefaultMailAccount();

$departments = $organization->getDepartments();

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Mailbox Settings</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailSettings.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailOutgoing.css" rel="stylesheet">
    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>
    <style>
        .phoneNumber{
            background-color: #f2f2f2 !important;
        }
        .phoneNumber:hover{
            background-color: #e4e4e4 !important;
        }
        .swal-footer {
            text-align: center;
        }

        .disabledTable{
            position: relative;
        }
        .disabledTable:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .table > tbody > tr > td{
            border-bottom: 1px solid #e7eaec;
        }
        .table > thead{
            background-color: #f7f7f7;
        }
        .dropdown-menu{
            position: absolute;
            left: -60px;
        }
        .settings-dropdown .btn-danger:hover{
            background-color: #f02e42;
        }
        @media (max-width: 992px) {
            #phoneNumbersContainer{
                padding-bottom: 20px;
            }
        }
    </style>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        Marketing
                    </li>
                    <li  class="active">
                        <strong>Settings</strong>
                    </li>
                </ol>
            </div>
        </div>


        <div class="row">

            <div class="col-md-6" id="emailAccountsContainer">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Email accounts</h5>
                        <?php $checkAuths_mailcenterCreate = $bouncer["userAuthorization"]->checkAuthorizations(array(["mailcenter",3]));
                        if ($checkAuths_mailcenterCreate){
                        ?>
                        <div class="ibox-tools">
                            <a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="showMailCenterInfo()"><i class="fa fa-info"></i></a>
                            <a href="javascript:void(0)" class="btn btn-primary btn-xs" onclick="addAccount()">Connect account</a>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="ibox-content" style="padding: 0; height: 65vh; overflow-y: scroll">
                        <div>
                            <table class="table table-hover issue-tracker" style="margin-bottom: 0px;">
                                <thead>
                                <tr>
                                    <td style="text-align: left;">Email address</td>
                                    <td style="text-align: center;">Private Account <span onclick="showPrivateAccountInfo()" style="cursor: pointer;"><i style="font-size: 16px;margin-left: 5px;" class="fa fa-info-circle"></i></span></td>
                                    <td style="text-align: center;">Actions</td>
                                    <td style="text-align: center;">Settings</td>
                                </tr>
                                </thead>
                                <tbody id="mailSettingsAccounts">
                                    <tr style="background-color: #fff;"><td style="text-align: center" colspan="5">No Accounts connected</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ibox-footer" style="height: 37px;">

                    </div>
                </div>
            </div>
            <div class="col-md-6" id="phoneNumbersContainer">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Phone Numbers</h5>
                        <?php $checkAuths_mailcenterCreate = $bouncer["userAuthorization"]->checkAuthorizations(array(["mailcenter",3]));
                        if ($checkAuths_mailcenterCreate){
                            ?>
                            <div class="ibox-tools">
                                <?php
                                if($bouncer["organizationData"]["organizationPackage"] == "1"){
                                    ?>
                                    <a class="btn btn-primary btn-xs" onclick="limitReached('package')" id="createTwilioSubAccount"> Create Account </a>
                                    <a class="btn btn-primary btn-xs" onclick="limitReached('package')" id="purchaseTwilioPhoneNumber"> Assign Phone Number </a>
                                    <?php
                                }else{
                                    ?>
                                    <a class="btn btn-primary btn-xs" onclick="createSubAccount()" id="createTwilioSubAccount"> Create Account </a>
                                    <a class="btn btn-primary btn-xs" onclick="purchaseNumber()" id="purchaseTwilioPhoneNumber"> Assign Phone Number </a>
                                    <?php
                                }
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="ibox-content" style="padding: 0;  height: 65vh; overflow-y: scroll">
                        <div>
                            <table class="table table-hover issue-tracker" id="mailSettingsPhoneNumbersTable" style="margin: 0px;">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">Phone number</td>
                                    <td style="text-align: center;">State</td>
                                    <td style="text-align: center;">Postal Code</td>
                                    <td style="text-align: center;">Actions</td>
                                </tr>
                                </thead>
                                <tbody id="mailSettingsPhoneNumbers">
                                    <tr style="background-color: #fff;"><td style="text-align: center" colspan="5">No phones connected</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="ibox-footer" style="height: 37px;">
                        <a id="textMsg" style="font-weight: 600;" onclick="showCustomModal('categories/iframes/phone/smsPricing.php');">SMS Pricing</a>
                        <a style="float: right" onclick="activateAccount(false)" id="activateTwilioPhoneNumber"> Activate Account </a>
                    </div>

                </div>
            </div>
        </div>

        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/mail/settings.min.js?v=1.3.7.9"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/phone/settings.min.js?v=1.3.7.9"></script>
<?php
//if($bouncer["userSettingsData"]["firstTutorial"] == "1" && $bouncer['isUserAnAdmin'] == true) {
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/admin/adminTutorial_stage_email_step_1.php");
//}else if ($bouncer["userSettingsData"]['firstTutorial'] == "1" && $bouncer['isUserAnAdmin'] == false){
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/user/userTutorial_stage_email_step_1.php");
//}
?>
<?php if (isset($_GET['addAccount'])){ ?>
    <script>
        $(document).ready(function () {
            addAccount();
        });
    </script>
<?php } ?>
<?php if (isset($_GET['newAccount']) && $_GET['newAccount'] == "added"){ ?>
    <script>
        $(document).ready(function () {
            swal({title:"Success!", text:"Your account has been added",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{

            });
        });
    </script>
<?php } ?>
<?php if (isset($_GET['newAccount']) && $_GET['newAccount'] == "authorized"){ ?>
    <script>
        $(document).ready(function () {
            swal({title:"Success!", text:"Your account has been authorized",icon: "success",
                buttons: {
                    ok:"Ok"
                }
            }).then((isConfirm)=>{

            });
        });
    </script>
<?php } ?>
<script>
 
</script>
</body>
</html>

