<?php

// I am not sure if this file is in use and I am too much scared to delete it
// So I'm not touching it for now
// If you came across this file because it is used somewhere in the system and you can't find out where then may god have mercy on you


$pagePermissions = array(false,true,true,array(['mailcenter',3]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

$email = new email($bouncer["credentials"]["userId"]);
$isLimitValid = $email->isLimitValid($bouncer["credentials"]["orgId"]);

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$organizationMailAccounts = $mailaccounts->getMyAccounts(true,true);

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$folders = $preEmails->getFoldersData();
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads - Compose Email</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <!-- myComment: required with jquery ui js to disappear the title text -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Angular_Seed_Project/js/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/summernote/summernote.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailCompose.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/compose.css" rel="stylesheet">
    <script>
        var selectedFolder = null;
    </script>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li  class="active">
                        <strong>Mail</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12 animated fadeInRight" style="margin-bottom: 30px;">
                <?php if ($organizationMailAccounts && count($organizationMailAccounts)){ ?>
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <div class="btn-group">
                            <button class="btn btn-white active" id="btn_blank" type="button" onclick="changeTemplate(0)">Blank Email</button>
                            <button class="btn btn-white" id="btn_template" type="button" onclick="changeTemplate(1)">Template</button>
                        </div>
                    </div>
                    <h2>
                        Compose Email
                    </h2>
                </div>
                <div id="blankEmail">
                    <div class="mail-box">
                        <div class="mail-body">
                            <form class="form-horizontal" method="get">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">From:</label>
                                    <div class="col-sm-11">
                                        <select class="form-control" id="fromAccount">
                                            <?php
                                            foreach($organizationMailAccounts as $organizationMailAccount){
                                                ?>
                                                <option value="<?php echo $organizationMailAccount['id']; ?>" ><?php echo $organizationMailAccount['email']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">To:</label>
                                    <div class="col-sm-11">
                                        <input type="text" id="toEmail" class="form-control" placeholder="To Email Address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Subject:</label>
                                    <div class="col-sm-11">
                                        <input type="text" id="subjectEmail" class="form-control" placeholder="Email Subject" >
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="mail-text h-200">

                            <div class="summernote"></div>
                        </div>
                        <small id="errorSpan"></small>
                        <div class="mail-body text-right tooltip-demo">
                            <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/outgoing.php" class="btn btn-white btn-sm"><i class="fa fa-times"></i> Discard</a>
                            <button onclick="prepareMail(0,this);" class="btn btn-sm btn-primary ladda-button ladda-button0" data-style="expand-left"><i class="fa fa-reply"></i> Send</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div id="templates" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="templateFolder">
                                <select onchange="selectFolder(this)" class="form-control" id="selectedFolder">
                                    <option value="NULL">Home</option>
                                    <?php foreach ($folders as $folder){?>
                                        <option value="<?= $folder['id'] ?>"><?= $folder['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="myTemplates">

                            </div>
                            <div class="mail-box" id="templateEmail" style="display: none;">
                                <div class="mail-body">
                                    <form class="form-horizontal" method="get">
                                        <div class="form-group">
                                            <label class="col-sm-1 control-label">From:</label>
                                            <div class="col-sm-11">
                                                <select class="form-control" id="fromTemplateAccount">
                                                    <?php
                                                    foreach($organizationMailAccounts as $organizationMailAccount){
                                                        ?>
                                                        <option value="<?php echo $organizationMailAccount['id']; ?>" ><?php echo $organizationMailAccount['email']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-1 control-label">To:</label>
                                            <div class="col-sm-11">
                                                <input type="text" id="toTemplateEmail" class="form-control" placeholder="To Email Address">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-1 control-label">Subject:</label>
                                            <div class="col-sm-11">
                                                <input type="text" id="subjectTemplateEmail" class="form-control" placeholder="Email Subject" >
                                            </div>
                                        </div>
                                    </form>

                                </div>

                                <div class="mail-text text-center">
                                    <iframe id="templateContainer" style="border: 1px solid #d4d4d4; width: 750px;margin-top: 23px;"></iframe>
                                </div>
                                <small id="errorTemplateSpan"></small>
                                <div class="mail-body text-right tooltip-demo">
                                    <button onclick="goBack();" class="btn btn-white btn-sm"><i class="fa fa-arrow-circle-o-left"></i> back</button>
                                    <button onclick="prepareMail(1,this);" class="btn btn-sm btn-primary ladda-button ladda-button1" data-style="expand-left"><i class="fa fa-reply"></i> Send</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }else{ ?>
                    <div class="mail-box" style="padding: 25px;">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="text-align: center">No Active Email Accounts Found</h2>
                                <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/mail/mailSettings.php"><button class="btn btn-success btn-block">Go To Mail Center</button></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>


        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<?php if ($organizationMailAccounts && count($organizationMailAccounts)){ ?>
<!-- summernote -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/summernote/summernote.min.js"></script>

    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/mail/compose.min.js"></script>

    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<script>
    function prepareMail(isTemplate,el){

        var l = $(el).ladda();
        l.ladda("start");
        if (isTemplate) {
            var fromMail = document.getElementById("fromTemplateAccount").value;
            var toMail = document.getElementById("toTemplateEmail").value;
            var subject = document.getElementById("subjectTemplateEmail").value;
            var content = templateId;
        }else {
            var fromMail = document.getElementById("fromAccount").value;
            var toMail = document.getElementById("toEmail").value;
            var subject = document.getElementById("subjectEmail").value;
            var content = $('.summernote').summernote('code');
        }
        var errorString = "";
        var isOk = true;
        if (isTemplate) {
            if (fromMail == "" || fromMail == 0) {
                errorString += "<br>* From Mail Account Can't Be Empty";
                isOk = false;
            }
            if (toMail.trim() == "") {
                errorString += "<br>* To Mail Account Can't Be Empty";
                isOk = false;
            }
            if (subject.trim() == "") {
                errorString += "<br>* Mail Subject Can't Be Empty";
                isOk = false;
            }
        }else{
            if (fromMail == "" || fromMail == 0) {
                errorString += "<br>* From Mail Account Can't Be Empty";
                isOk = false;
            }
            if (toMail.trim() == "") {
                errorString += "<br>* To Mail Account Can't Be Empty";
                isOk = false;
            }
            if (subject.trim() == "") {
                errorString += "<br>* Mail Subject Can't Be Empty";
                isOk = false;
            }
            if (content.trim() == "" || content == "<p><br></p>") {
                errorString += "<br>* Mail Content Can't Be Empty";
                isOk = false;
            }
        }

        if (isOk === true){
            <?php if ($isLimitValid == true){ ?>
                sendEmail(toMail,fromMail,subject,content,isTemplate,l);
            <?php }else{ ?>
            l.ladda("stop");
            limitReached("email");
            return false;
            <?php } ?>
        }else{
            errorSwal(errorString,l);
        }
    }
    <?php if ($isLimitValid == true){ ?>
    function sendEmail(toEmail,fromEmail,subject,content,isTemplate,l){
        var strUrl = BASE_URL+'/console/actions/mail/sendEmail.php';

        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                "to": toEmail,
                "from": fromEmail,
                "subject": subject,
                "content": content,
                "isTemplate":isTemplate
            },
            async: true
        }).done(function (data) {

            try {
                data = JSON.parse(data);
                if (data.status == true){
                    toastr.success("Email Sent Successfully","Snet");
                    setTimeout(function(){
                        location.href = BASE_URL+"/console/categories/mail/outgoing.php";
                    },1500);
                } else{
                    toastr.error("Email Not Sent","ERROR");
                }
                l.ladda("stop");

            }catch (e) {
                toastr.error("Email Not Sent","ERROR");
                l.ladda("stop");
            }

        });
    }
    <?php } ?>
    function selectFolder(obj){
        selectedFolder = obj.value;
        getData();
    }
</script>
<?php } ?>
</body>
</html>

