<?php
$pagePermissions = array(false,true,true,array(['mailcenter',3]));

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"]);
$myAccounts = $mailaccounts->getMyAccounts(false,true);

$organization = new organization($bouncer["credentials"]["orgId"]);
$defaultAccount = $organization->getDefaultMailAccount();

$departments = $organization->getDepartments();

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | My Templates</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailTemplates.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailOutgoing.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
        <?php if (isset($_GET['folder'])){ ?>
        var selectedFolder = <?= $_GET['folder'] ?>;
        <?php }else{ ?>
        var selectedFolder = null;
        <?php } ?>
    </script>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;margin-bottom: 0px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        Marketing
                    </li>
                    <li  class="active">
                        <strong>E-Mail Templates</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="addFolder">
                    <button class="btn btn-primary btn-sm" onclick="addNewFolder()"><i class="fa fa-plus"></i></button>
                    <span id="activeFolder">

                    </span>
                    <a class="text-info pull-right" style="text-decoration: underline" href="http://help.network-leads.com/en/articles/3105138-email-marketing" target="_blank">Help</a>
                </div>
                <div id="myFolders">

                </div>
                <hr style="margin: 8px;border-top: 1px solid #a7a7a7;">
                <div id="myTemplates">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="padding: 0;">
                <div id="preDefinedTemplates">
                    <div id="preDefinedTemplatesHead">
                        <h3>Choose from one of our pre-made templates</h3>
                    </div>
                    <div id="preDefinedTemplatesBody">
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(0)">
                            <div class="preDefinedTemplateImage">
                                <img alt="Blank" style="background: white;width:150px;height: 182px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAP7//wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Blank
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Blank Page
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(4)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/requestingaquote.jpg" alt="Requesting a quote">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Requesting a quote
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Leads
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(17)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/severalattempts.jpg" alt="Several attempts to reach you">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Several attempts to reach you
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Leads
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(1)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/No-Response.jpg" alt="No Response/Bad Number">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                No Response/Bad Number
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Contacts & Communication
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(7)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/automaticleadresponse.jpg" alt="Automatic Lead Response">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Automatic Lead Response
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Contacts & Communication
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(6)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/Transportation.jpg" alt="Your Pick-up is scheduled on">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Your Pick-up is scheduled on
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Transportation & Storage
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(2)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/itemisshipped.jpg" alt="Your items has now been shipped!">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Your items has now been shipped!
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Transportation & Storage
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(3)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/movingservices.jpg" alt="Moving services">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Moving services
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Marketing
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(5)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/Registration%20Completed.jpg" alt="Registartion Completed">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                My moving estimate
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Estimate
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(9)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/holidays.jpg" alt="Happy Holidays">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Happy Holidays
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Holidays
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(10)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/firstleadresponse.png" alt="First lead response">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                First lead response
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Leads
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(11)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/discount.jpg" alt="Discount">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Discount
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Marketing
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(12)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/newyearsale.jpg" alt="New year sale">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                New year sale
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Marketing
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(14)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/easter.jpg" alt="Easter Treat – Up to 10% off">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Easter Treat Up to 10% off
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Marketing
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(15)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/coronastayhealthy.jpg" alt="Coronavirus and Staying Healthy">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Coronavirus and Staying Healthy
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Marketing
                            </div>
                        </div>
                        <div class="preDefinedTemplateItem" onclick="previewPreDefinedTemplate(16)">
                            <div class="preDefinedTemplateImage">
                                <img src="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/images/templates/givebackcorona.jpg" alt="Coronavirus Relief Fund">
                            </div>
                            <div class="preDefinedTemplateTitle">
                                Coronavirus Relief Fund
                            </div>
                            <div class="preDefinedTemplateDescription">
                                Marketing
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>

<div id="imageContainer"></div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/popper.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- Jasny -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/html2canvas.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/mail/templates.min.js?v=1.3.7.5"></script>

<script>

    function createSwalContent(title,subject,department){
        var container = document.createElement("div");
        container.classList.add("input-group");

        var input = document.createElement("input");
        input.setAttribute("placeholder","Template Name");
        input.id = "editTitle";
        input.value = title;
        input.classList.add("form-control");
        container.appendChild(input);

        var input = document.createElement("input");
        input.setAttribute("placeholder","Email Subject");
        input.id = "editSubject";
        input.value = subject;
        input.classList.add("form-control");
        container.appendChild(input);

        var select = document.createElement("select");
        select.classList.add("form-control");
        select.id = "editDep";
        <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
        select.setAttribute("style","width:93%");
        <?php } ?>

        var option = document.createElement("option");
        option.setAttribute("value",null);
        var optionText = document.createTextNode("Select Department (Optional)");

        option.appendChild(optionText);
        select.appendChild(option);

        <?php foreach ($departments as $department){ ?>
        var option = document.createElement("option");
        option.setAttribute("value",<?= $department['id'] ?>);
        if (department == '<?=$department['id'] ?>'){
            option.setAttribute("selected","true");
        }
        var optionText = document.createTextNode("<?= $department['title'] ?>");
        option.appendChild(optionText);
        select.appendChild(option);
        <?php } ?>
        container.appendChild(select);
        <?php if($bouncer["userSettingsData"]['helpMode'] == 1){ ?>
        var a = document.createElement("a");
        a.setAttribute("style","display: inline-table;float: left;");
        a.setAttribute("data-toggle","tooltip");
        a.setAttribute("white-space","nowrap");
        a.setAttribute("data-placement","right");
        a.setAttribute("title","This Template will only be visible to users under this Department");

        var span = document.createElement("span");
        var i = document.createElement("i");
        i.setAttribute("style","font-size: 27px;margin-left: 5px;vertical-align: middle;margin-top: 10%;");
        i.classList.add("fa");
        i.classList.add("fa-question");
        span.appendChild(i);
        a.appendChild(span);
        container.appendChild(a);
        <?php } ?>
        return container;
    }
</script>
<?php
//if($bouncer["userSettingsData"]["firstTutorial"] == "1" && $bouncer['isUserAnAdmin'] == true) {
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/admin/adminTutorial_stage_email_step_2.php");
//}else if ($bouncer["userSettingsData"]['firstTutorial'] == "1" && $bouncer['isUserAnAdmin'] == false){
//    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/categories/user/firstTutorial/user/userTutorial_stage_email_step_2.php");
//}
?>
</body>
</html>

