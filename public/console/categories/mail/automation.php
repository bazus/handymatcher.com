<?php
$pagePermissions = array(false,true,true,true);
// todos change permissions by package
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");


?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Campaigns</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Froala Editor -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/froala_editor.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/froala_style.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/plugins/table.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/plugins/colors.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/froala/css/plugins/quick_insert.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/mailOutgoing.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>

    <style>
        .table-minimal tr{
            font-size: 13px;
            line-height: 20px;
            border: 1px solid #e9ecef;
            border-bottom: 0;
            transition: background-color 0.3s;
        }

        .table-minimal tr:last-of-type{
            border-bottom: 1px solid #e9ecef;
        }
        .table-minimal thead tr {
            border-color: transparent;
        }
        .table-minimal td{
            font-weight: 600;
            background-color: #fff;
            padding: 20px 12px;
            max-width: 27ch;
        }
        .table-minimal th {
            font-size: 12px;
            font-weight: 600;
            line-height: 15px;
            letter-spacing: 1px;
            color: #7f90a0;
            text-align: left;
            text-transform: uppercase;
            vertical-align: middle;
        }

        .campaignsTable th:first-child,.campaignsTable>tbody td:first-child{
            text-align: left;
        }
        .campaignsTable th:not(:first-child),.campaignsTable>tbody td:not(:first-child){
            text-align: center;
        }

        .swal-emails-campaigns{
            width: 900px !important;
            overflow: scroll;
        }
        .swal-emails-campaigns .swal-content{
            margin: 0px !important;
            padding: 0px !important;
        }

        #campaignsTableBody td{
            vertical-align: middle;
        }
    </style>

</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        Marketing
                    </li>
                    <li  class="active">
                        <strong>Campaigns</strong>
                    </li>
                </ol>
            </div>
        </div>
        
        <div class="tabs-container">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a class="nav-link active" data-toggle="tab" href="#tab-1"> Campaigns</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" id="tab-1" class="tab-pane active">
                    <div class="row">
                        <div class="col-lg-12" style="margin-bottom: 29px;">
                            <div class="mail-box-header" style="border-bottom: 1px solid #e7eaec;border-top: none; text-align: right;padding: 10px">
                                <button onclick="emailCampaignsController.openCreateCampaign()" role="button" class="btn btn-success">Create</button>
                            </div>
                            <div class="mail-box" style="padding: 13px">

                                <table class="table table-minimal campaignsTable">
                                    <thead>
                                        <tr>
                                            <th>Campaign Name</th>
                                            <th>Status (active/disabled)</th>
                                            <th>Delivered</th>
                                            <th>Opens</th>
                                            <th>Settings</th>
                                        </tr>
                                    </thead>
                                    <tbody id="campaignsTableBody"></tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>
    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>


</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>


<script>

    var emailCampaignsController = {
        getCampaigns: function() {
        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/campaigns/getCampaigns.php',
            strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            console.log(data);
            data = JSON.parse(data);

            var campaignsTableBody = document.getElementById('campaignsTableBody');
            campaignsTableBody.innerHTML = "";
            for(var i = 0;i<data.length;i++){
                emailCampaignsController.createCampaignRow(data[i]);
            }
        });
    },
        createCampaignRow:function(data){
        console.log(data);

        var campaignsTableBody = document.getElementById('campaignsTableBody');

        var tr = document.createElement("tr");

        var td;


        td = document.createElement("td");
        td.innerHTML = data.name;
        tr.appendChild(td);

        td = document.createElement("td");
        if(data.isActive == "1"){
            td.innerHTML = '<span class="badge badge-primary">Active</span>';
        }else{
            td.innerHTML = '<span class="badge badge-warning">Paused</span>';
        }
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = "---";
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = "---";
        tr.appendChild(td);


        // Settings
        var td = document.createElement("td");
        td.setAttribute("style","text-align:center");

        var div = document.createElement("div");
        div.className = "dropdown";

        var button = document.createElement("button");
        button.className = "btn btn-default dropdown-toggle";
        button.setAttribute("type","button");
        button.setAttribute("data-toggle","dropdown");
        button.setAttribute("aria-haspopup","true");
        button.setAttribute("aria-expanded","true");
        button.id = "dropdownMenu"+data.id;
        button.innerHTML = "Settings ";

        var buttonSpan = document.createElement("span");
        buttonSpan.className = "caret";

        var ul = document.createElement("ul");
        ul.className = "dropdown-menu";
        ul.setAttribute("aria-labelledby","dropdownMenu"+data.id);

        if(data.isActive == "1") {
            ul.innerHTML += '<li><a href="#" onclick="emailCampaignsController.pauseCampaign('+data.id+')">Pause Campaign</a></li>';
        }else{
            ul.innerHTML += '<li><a href="#" onclick="emailCampaignsController.activateCampaign('+data.id+')">Re-activate Campaign</a></li>';
        }
        ul.innerHTML += '<li role="separator" class="divider"></li>';
        ul.innerHTML += '<li><a href="#" class="btn-danger" style="color:#fff;" onclick="emailCampaignsController.deleteCampaign('+data.id+')">Delete Campaign</a></li>';

        button.appendChild(buttonSpan);
        div.appendChild(button);
        div.appendChild(ul);

        td.appendChild(div);
        tr.appendChild(td);

        campaignsTableBody.appendChild(tr);

    },
        deleteCampaign:function(campaignId){
            var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/campaigns/deleteCampaign.php',
                strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{campaignId:campaignId},
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                console.log(data);
                emailCampaignsController.getCampaigns();
            });
        },
        pauseCampaign:function(campaignId){
            var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/campaigns/pauseCampaign.php',
                strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{campaignId:campaignId},
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                emailCampaignsController.getCampaigns();
            });
        },
        activateCampaign:function(campaignId){
            var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/mail/campaigns/activateCampaign.php',
                strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{campaignId:campaignId},
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                emailCampaignsController.getCampaigns();
            });
        },
        openCreateCampaign:function() {

            var content = document.createElement("div");

            var iframe = document.createElement("iframe");
            iframe.src = BASE_URL+"/console/categories/iframes/mail/createCampaign.php";
            iframe.setAttribute("style","border:none;width: 100%;height: 753px;");
            iframe.setAttribute("name","email-campaigns-iframe");

            content.appendChild(iframe);

            swal({
                content: content,
                buttons: false,
                className: "swal-emails-campaigns"
            });
        }

    };

    emailCampaignsController.getCampaigns();

</script>
</body>
</html>

