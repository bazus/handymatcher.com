<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
$mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);
if ($bouncer["credentials"]["userId"] !== $_GET['userSent']){
    if ($bouncer['isUserAnAdmin']){
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

        $userClass = new user($_GET['userSent'],false);
        if ($userClass->isValid == true){
            $email = $mailCenter->getEmail($_GET['id'],$_GET['userSent']);
            if (!$email){
                header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
                exit;
            }
        }else{
            header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
            exit;
        }

    }else{
        header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
        exit;
    }
}else{
    if (isset($_GET['id'])){
        $email = $mailCenter->getEmail($_GET['id'],$bouncer["credentials"]["userId"]);
    }else{
        header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/mail/outgoing.php");
        exit;
    }
}


?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Mailbox Accounts</title>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <script>
        var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";
    </script>

    <style>
        .mail-box{
            margin-bottom: 0px !important;
        }
    </style>
</head>

<body style="background-color: #fff;">
    <div class="row">
        <div class="m-t-lg col-lg-offset-1 col-lg-10 col-lg-offset-1  animated fadeInRight" style="margin: 0;width: 100%;">
            <div class="mail-box">
                <div class="mail-body">
                   <?php
                    echo html_entity_decode($email['content']);
                   ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</body>
</html>

