<?php

$pagePermissions = array(true,true,true,true);

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/classes/plans.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/billing.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");

$organization = new organization($bouncer["credentials"]["orgId"]);
$organizationData = $organization->getData();
$userFullName = "";
if (isset($organizationData['userIdCreated'])){
    $user = new user($organizationData['userIdCreated']);
    $userData = $user->getData();
    if (isset($userData['fullName'])){
        $userFullName = $userData['fullName'];
    }
}

$email = new email();

$emailsData = $email->isLimitValid($bouncer["credentials"]["orgId"],true);

if($emailsData['emailsLimit'] >= 10000){$emailsData['emailsLimit'] = "unlimited";}

$usersData = $organization->canAddUsers(true);
if($usersData['usersLimit'] >= 100){$usersData['usersLimit'] = "unlimited";}

$plans = new plans();
$allPlans = $plans->getAllPlans();

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads | Billing</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/css/flags.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/plugins/plans/style.min.css" rel="stylesheet">
    <style>
        .oldPrice {
            position: absolute;
            margin-top: 3px;
            margin-left: 3px;
            font-size: 13px !important;
        }
        .oldPrice:before {
            position: absolute;
            content: "";
            left: 0;
            top: 50%;
            right: 0;
            border-top: 1px solid;
            border-color: inherit;
            -webkit-transform:rotate(-5deg);
            -moz-transform:rotate(-5deg);
            -ms-transform:rotate(-5deg);
            -o-transform:rotate(-5deg);
            transform:rotate(-5deg);
        }
        .price{
            float: left;
            margin-top: -40px;
            padding: 5px;
            width: 100%;
            background: rgb(0,140,244);
            color: white;
            font-size: 1.2em;
            letter-spacing: 2px;
        }
        .product-imitation {
            padding: 13px 0px;
        }
        .pricingPlansTable>tbody>tr>td {
            height: 36px;
        }
        .btn-start {
            background: url(<?= $_SERVER['CDN'] ?>/welcome/Button.png);
            background-repeat: no-repeat;
            background-size: cover;
            color: white;
            margin-top: 10px;
            font-weight: 100;
            height: 34px;
        }
        .product-name {
            font-size: 29px;
            display: initial !important;
        }
        div#fullScreen {
            position: fixed;
            left: 0;
            top: 0;
            background: rgba(0,0,0,.4);
            height: 100vh;
            width: 100%;
            z-index: 999999999;
        }

        #productContainer{
            padding-top: 0px !important;
        }
        .payment-card{
            height: 200px;
        }
        .ibox-title {
            padding: 15px 13px;
        }
        #clipboard-btn{
            font-size: 10px;
        }
        #referral{
            display: none;
        }
        @media only screen and (max-width: 768px){
            .payment-card{
                min-height: 200px;
                height: auto;
            }
        }
        #myCreditCard{
            margin-right: 3px;
        }
        @media only screen and (max-width: 992px){
            #planType{
                margin:0 !important;
                display: flex;
                justify-content: center;
            }
            .saveannual{
                position: unset !important;
                font-size: 10px;
            }
            .productPrice{
                font-size: 40px !important;
                 padding-bottom: 0 !important;
            }
            .productPrice h5{
                position: unset !important;
            }
            .littleHr{
                padding-bottom: 25px !important;
            }
            .itemInclude{
                font-size: 16px !important;
            }
        }
        @media (max-width: 576px) {
            .swal-billing-history{
                width: 100% !important;
                height: 100% !important;
            }
            #swal-billing-history-iframe{
                height: 100% !important;
            }
        }
        .swal-billing-history{
            margin: 0;
            height: 600px;
            width: 90%;
        }
        .swal-billing-history .swal-content{
            margin: 0;
            padding: 0;
        }
    </style>

    <!-- Mainly scripts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
</head>

<body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
<div id="wrapper">
    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/side.php"); ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/top.php"); ?>

        <div class="row wrapper border-bottom white-bg page-heading" style="padding: 9px 0px 9px 4px;">
            <div class="col-lg-10">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/"; ?>" title="Home Page">Home</a>
                    </li>
                    <li>
                        <a href="../user/management.php">Organization Management</a>
                    </li>
                    <li  class="active">
                        <strong>Billing</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="payment-card" style="padding: 6px;">
                    <?php
                    $currentPlan = $plans->getSpecificPlan($organizationData["organizationPackage"]);
                    ?>
                    <div class="col-sm-12">
                        <h2>Network Leads</h2>
                        <small>
                            <strong>Current Plan:</strong> <?php echo $currentPlan["title"]; ?> <?php if($organizationData["organizationPackage"] < 4){ ?> (<a href="#upgrade">Upgrade</a>) <?php } ?>
                        </small>
                        <br>
                        <small>
                            <strong>Plan Status:</strong>
                            <?php
                            if($organizationData["organizationPackageStatus"] == "1"){ ?>
                                <span style="color: #29D240">Active</span> (<a onclick="preCancelPlan()">Cancel subscription</a>)
                                <?php
                            }else{
                                $now = date("Y-m-d h:i A",strtotime("now"));
                                $cancellationDate = date("Y-m-d h:i A",strtotime($organizationData["subscriptionEndDate"]));
                                if($cancellationDate >= $now){
                                    ?>
                                    <span style="color: #F44336">Pending Cancellation on <?php echo date("F jS Y",strtotime($organizationData["subscriptionEndDate"])); ?></span> (<a onclick="reactivatePlan()">re-activate</a>)
                                    <?php
                                }else{
                                    ?>
                                    <span style="color: #F44336">Cancelled</span>
                                <?php } ?>
                            <?php } ?>
                        </small>
                        <br>
                        <small>
                            <strong>Name:</strong> <?= $userFullName ?>
                        </small>
                        <br>
                        <small>
                            <button id="clipboard-btn" class="btn btn-white" onclick="copyToClipboard('#referral')" style="margin-top: 9px;"><i class="fa fa-copy"></i> Copy referral link to clipboard</button>
                            <br>
                            <span id="referral" onclick="copyToClipboard('#referral')"><?php echo $_SERVER['LOCAL_NL_URL'] . "?referral=" . $bouncer["organizationData"]["secretKey"]; ?></span>
                        </small>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="payment-card" style="padding: 6px;">
                    <div class="col-md-12">
                        <h2>Text Message Credit</h2>
                        <small>
                            <strong>Auto Recharge:</strong> <span onclick="showCustomModal('categories/iframes/billing/addBalance.php?tab=autoRecharge')" class="label pull-right" style="cursor: pointer;margin-top: 3px;" id="isAutoRechargeOn"></span>
                        </small>
                        <small style="margin-top: 10px;display: block">
                            <strong>Balance:</strong>
                            <button  style="float: right;font-size: 9px;margin-left: 4px;" onclick="showCustomModal('categories/iframes/billing/addBalance.php')" class="btn btn-primary pull-right btn-xs" style="font-size: 9px;"><i class="fa fa-plus"></i></button>
                            <span style="float: right">$<span id="myBalance">0.00</span></span>

                        </small>
                        <small style="margin-top: 10px;display: block">
                            <strong>Credit Card: </strong> <form style="display:inline;" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/billing/addCard.php" method="post" class="addCardForm"><button class="btn btn-warning pull-right btn-xs" id="stripeAddCard" style="font-size: 9px;" data-style="zoom-in" data-email="<?php echo $bouncer["userData"]["email"]; ?>" data-image="https://s3.amazonaws.com/thenetworkleads/general/logo/iconwithglobe_256x256.png" type="submit" data-key="<?php echo $_SERVER['STRIPE_PK']; ?>"  data-name="Network Leads" data-description="Add New Card" data-panel-label="Add Card" data-label="Add Card"><span class="ladda-label"><i class="fa fa-pencil"></i></span><span class="ladda-spinner text-primary"></span></button></form> <span id="myCreditCard" class="pull-right"></span>
                        </small>
                        <small style="margin-top: 10px;display: block">
                            <a class="pull-right" onclick="showBillingHistory()">Billing History</a>
                        </small>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="payment-card" style="padding: 6px;">
                    <div class="col-md-12">
                        <h2>Usage</h2>
                        <small>
                            <strong>User Limit:</strong> <?php echo $usersData['totalUsersInOrganization']." Of ".$usersData['usersLimit'] ?>
                        </small>
                        <div class="progress progress-mini" style="margin-bottom: 15px;">
                            <div style="width: <?= number_format(($usersData['totalUsersInOrganization']/$usersData['usersLimit']*100),2) ?>%;" class="progress-bar"></div>
                        </div>
                        <small>
                            <strong>Email Limit:</strong> <?php echo $emailsData['totalEmailsSent']." Of ".$emailsData['emailsLimit'] ?> (per month)
                        </small>
                        <div class="progress progress-mini" style="margin-bottom: 15px;">
                            <div style="width: <?= number_format(($emailsData['totalEmailsSent']/$emailsData['emailsLimit']*100),2) ?>%;" class="progress-bar"></div>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="col-md-5">-->
<!--                <div class="payment-card" style="padding: 0px !important;overflow-y: auto">-->
<!--                    <div class="ibox" style="margin-bottom: 0px;">-->
<!--                        <div class="ibox-title" style="border: none;">-->
<!--                            <h5 style="margin: 4px 0 0;">Credit Cards</h5>-->
<!--                            <div class="ibox-tools">-->
<!--                                <form action="--><?php //echo $_SERVER['LOCAL_NL_URL']; ?><!--/console/actions/system/organization/billing/addCard.php" method="post" class="addCardForm">-->
<!--                                    <button class="btn btn-primary btn-xs" id="stripeAddCard" data-style="zoom-in" data-email="--><?php //echo $bouncer["userData"]["email"]; ?><!--" data-image="https://s3.amazonaws.com/thenetworkleads/general/logo/iconwithglobe_256x256.png" type="submit" data-key="--><?php //echo $_SERVER['STRIPE_PK']; ?><!--"  data-name="Network Leads" data-description="Add New Card" data-panel-label="Add Card" data-label="Add Card">-->
<!--                                        <span class="ladda-label"><i class="fa fa-plus"></i> Add Card</span>-->
<!--                                        <span class="ladda-spinner text-primary"></span>-->
<!--                                    </button>-->
<!--                                </form>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="ibox-content" style="padding: 0px;">-->
<!--                            <div class="project-list">-->
<!--                                <table class="table table-hover" style="margin-bottom: 0px;overflow-y: auto">-->
<!--                                    <tbody id="cardsTableBody"></tbody>-->
<!--                                </table>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
<!--            </div>-->
        </div>

        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 50px" id="upgrade">
                <?php $hrefPlan = false; require_once($_SERVER['LOCAL_NL_PATH']."/plugins/plans/plan.php"); ?>
            </div>
        </div>

        <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/footer.php"); ?>

        <!-- =========== CANELLATION 'ARE YOU SURE' MODAL ===========-->
        <div id="areyousurecancel" style="display: none; text-align: left;">
            <h1>We're sorry to see you go!</h1>
            <div class="alert alert-info" style="margin-top: 20px;font-size: 14px;">
                If you would like to proceed with canceling your subscription, please select "Cancel Subscription" below.
            </div>
        </div>
        <!-- =========== CANELLATION 'ARE YOU SURE' MODAL ===========-->

    </div>

    <?php require_once($_SERVER['LOCAL_NL_PATH']."/console/rightside.php"); ?>

</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/flagstrap/dist/js/jquery.flagstrap.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>


<script>

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();


        swal({title:"Your link has been copied", text:"would you like to share the link with a friend by sms?",icon: "success",
            buttons: ["No thanks", "Send SMS"],

        }).then((isConfirm)=> {
            if (isConfirm) {
                sendSMScontroller.init("",undefined,"<?php echo $bouncer["userData"]["fullName"]; ?> is inviting you to sign up to Network Leads Moving Software: <?php echo $_SERVER['LOCAL_NL_URL'] . "?referral=" . $bouncer["organizationData"]["secretKey"]; ?>");
            }
        });
    };
    function openBillingHistory(){

        var content = document.createElement("div");

        var iframe = document.createElement("iframe");

        iframe.src = BASE_URL+"/console/categories/iframes/billing/billingHistory.php";


        iframe.setAttribute("style","border:none;width: 100%;height:600px;");
        iframe.setAttribute("name","swal-billing-history-iframe");
        iframe.setAttribute("id","swal-billing-history-iframe");

        content.appendChild(iframe);

        swal({
            content: content,
            buttons: false,
            className: "swal-billing-history"
        });
    }


                    function preCancelPlan(){

                        var content = document.getElementById("areyousurecancel");
                        content.style.display = "";

                        swal({
                            content:content,
                            dangerMode: true,
                            buttons: {
                                cancel:"close",
                                continuetocancel:"Cancel Subscription",
                            },
                        }).then((isConfirm)=>{
                            if (isConfirm) {
                                cancelPlan();
                            }
                        });

                    }

                    function cancelPlan(){
                        var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/billing/cancelPlan.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "POST",
                            data:{
                                cancel:"true"
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {

                            try{
                                data = JSON.parse(data);

                                if(data == true){
                                    swal({title:"Success!", text:"Your plan has been cancelled",icon: "success",
                                        buttons: {
                                            ok:"Ok"
                                        }
                                    }).then((isConfirm)=>{
                                        window.location.replace(BASE_URL+"/console/categories/organization/billing.php");
                                    });
                                }else{
                                    swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",
                                        buttons: {
                                            ok:"Ok"
                                        }
                                    }).then((isConfirm)=>{
                                        window.location.replace(BASE_URL+"/console/categories/organization/billing.php");
                                    });
                                }
                            }catch (e){
                                swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",
                                    buttons: {
                                        ok:"Ok"
                                    }
                                }).then((isConfirm)=>{
                                    window.location.replace(BASE_URL+"/console/categories/organization/billing.php");
                                });
                            }

                        });
                    }

                    function reactivatePlan(){
                        var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/billing/reactivatePlan.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "POST",
                            data:{
                                reactivate:"true"
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {

                            try{
                                data = JSON.parse(data);

                                if(data == true){
                                    swal({title:"Success", text:"Your plan is now active again",icon: "success",
                                        buttons: {
                                            ok:"Ok"
                                        }
                                    }).then((isConfirm)=>{
                                        window.location.replace(BASE_URL+"/console/categories/organization/billing.php");
                                    });
                                }else{
                                    swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",
                                        buttons: {
                                            ok:"Ok"
                                        }
                                    }).then((isConfirm)=>{
                                        window.location.replace(BASE_URL+"/console/categories/organization/billing.php");
                                    });
                                }
                            }catch (e){
                                swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",
                                    buttons: {
                                        ok:"Ok"
                                    }
                                }).then((isConfirm)=>{
                                    window.location.replace(BASE_URL+"/console/categories/organization/billing.php");
                                });
                            }

                        });
                    }

                    function showBillingHistory(){
                        openBillingHistory();
                        // showCustomModal('categories/iframes/billing/billingHistory.php');
                    }

                    // STRIPE CREDIT CARDS (START)

                    function initBillingInfo(){
                        var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/billing/getDefaultCreditCardAndBalance.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "GET",
                            data:{},
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {

                            try{
                                data = JSON.parse(data);

                                if(data.defaultcreditcard.status == true) {
                                    if (data.defaultcreditcard.response != false) {
                                        // document.getElementById('cardsTableBody').innerHTML = "";
                                        var card = data.defaultcreditcard.response;

                                        $("#myBalance").text(data.currentBalance);
                                        $("#myCreditCard").text("ends with " + card.last4);
                                    }else{
                                        $("#myBalance").text(data.currentBalance);
                                        $("#myCreditCard").text("No Card");
                                    }

                                }else{
                                    if (data.currentBalance){
                                        $("#myBalance").text(data.currentBalance);
                                    }
                                    $("#myCreditCard").text("No card");
                                }
                                if (data.autoRecharge){
                                    $("#isAutoRechargeOn").removeClass("label-primary");
                                    $("#isAutoRechargeOn").removeClass("label-danger");

                                    if (data.autoRecharge.isOn == 1){
                                        $("#isAutoRechargeOn").addClass("label-primary");
                                        $("#isAutoRechargeOn").text("Enabled");
                                    }else{
                                        $("#isAutoRechargeOn").addClass("label-danger");
                                        $("#isAutoRechargeOn").text("Disabled");
                                    }
                                }else{
                                    $("#isAutoRechargeOn").addClass("label-danger");
                                    $("#isAutoRechargeOn").text("Disabled");
                                }


                            }catch (e){
                                $("#myCreditCard").text("No Card");
                                toastr.error('An error occurred. Please try again later.','Error');
                            }

                        });
                    }

                    $('#stripeAddCard').on('click', function(event) {

                        event.preventDefault();

                        var $button = $(this),
                            $form = $button.parents('form');
                        var opts = $.extend({}, $button.data(), {
                            token: function(result) {
                                $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                            }
                        });

                        opts.opened = function() {
                        };

                        StripeCheckout.open(opts);

                    });

                    $(".addCardForm").submit(function(e) {

                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize() // serializes the form's elements.
                        }).done(function (data) {

                            try{
                                data = JSON.parse(data);

                                if(data.status == true){
                                    toastr.success("Card added","Success");
                                }else{
                                    toastr.error(data.response,'Error');
                                }
                            }catch (e){
                                toastr.error('An error occurred. Please try again later.','Error');
                            }
                            initBillingInfo();

                        });

                        e.preventDefault(); // avoid to execute the actual submit of the form.
                    });

                    function deleteCard(cardId){
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover this card",
                            icon: "warning",
                            dangerMode: true,
                            buttons: true,
                        }).then(function(isConfirm) {
                            if (isConfirm) {
                                var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/billing/deleteCard.php',
                                    strReturn = "";
                                jQuery.ajax({
                                    url: strUrl,
                                    method: "POST",
                                    data: {
                                        source: cardId
                                    },
                                    success: function (html) {
                                        strReturn = html;
                                    },
                                    async: true
                                }).done(function (data) {

                                    try {
                                        data = JSON.parse(data);

                                        if (data.status == true) {
                                            toastr.success("Card Deleted", "Deleted");
                                        } else {
                                            toastr.error(data.response, 'Error');
                                        }
                                    } catch (e) {
                                        toastr.error('An error occurred. Please try again later.', 'Error');
                                    }
                                    initBillingInfo();

                                });
                            }
                        });
                    }

                    function makeCardDefault(cardId){
                        var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/billing/makeCardDefault.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "POST",
                            data:{
                                source:cardId
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {

                            try{
                                data = JSON.parse(data);

                                if(data.status != true){
                                    toastr.error(data.response,'Error');
                                }
                            }catch (e){
                                toastr.error('An error occurred. Please try again later.','Error');
                            }
                            initBillingInfo();

                        });
                    }

                    initBillingInfo();

                    // STRIPE CREDIT CARDS (END)

                    // Balance (START)
                    function addBalance() {
                        alert("Add Balance Function");
                    }
                    // Balance (END)
                    <?php if (isset($_GET['recharge'])){ ?>
        showCustomModal('categories/iframes/billing/addBalance.php');
    <?php } ?>
    <?php if (isset($_GET['addCreditCard'])){ ?>
        $(document).ready(function () {
            $("#stripeAddCard").click();
        });
    <?php } ?>
</script>

</body>
</html>

