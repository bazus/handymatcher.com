<?php
$ignoreWelcomeMode = true;
$ignoreRenewMode = true;

$pagePermissions = array(true,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userAuthorization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/classes/plans.php");


$plans = new plans();
$allPlans = $plans->getAllPlans();

$organization = new organization($bouncer["credentials"]["orgId"]);
$organizationData = $organization->getData();


// ========== Check if user is not in welcome mode ==========
//if($bouncer["userData"]["welcomeMode"] == "0" && (!isset($_GET['page']) && $_GET['page'] != "2")){
//    // User is not in welcome mode
//    header('Location: '.$_SERVER['LOCAL_NL_URL']."/console/index.php");
//    exit;
//}
// ========== Check if user is not in welcome mode ==========

$isWelcomeMode = true;

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Network Leads</title>

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
    <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/plugins/plans/style.min.css" rel="stylesheet">
    <script> const BASE_URL = "<?= $_SERVER['LOCAL_NL_URL'] ?>";</script>
    <style>
        body{
            background-image: url(<?= $_SERVER['CDN'] ?>/welcome/newsBG.jpg);
            background-repeat: no-repeat;
            background-size: cover;
        }
        body,html{
            height: fit-content !important;
        }
        .rowDetail p.MobileRow{
            display: none;
        }
        .plan{
            text-align: center;
            width: 100%;
            margin-top: 25px;
            margin-bottom: 30px;
        }
        .planTypes{
            text-align: left;
            padding: 2px;
            width: 94%;
            margin-top: 25px;
            margin-bottom: 30px;
        }
        .planTypes .planBody .rowDetail{
            padding-left: 10px;
        }
        .planHead .planName{
            background: rgb(45,204,157);
            color:white;
            padding: 3px;
        }
        .planHead .planName h5{
            margin: 0;
            font-size: 1.1em;
        }
        .planHead .planPrice{
            background: url(<?= $_SERVER['CDN'] ?>/welcome/Button.png);
            background-repeat: no-repeat;
            background-size: cover;
            color:white;
            padding: 5px;
            font-size: 2em;
            letter-spacing: 1px;
            font-weight: bold;
        }
        .planHead .planPrice small{
            display: block;
            letter-spacing: 0px;
            font-size: 0.35em;
            font-weight: normal;
            color:rgba(245,245,245,0.8);
        }
        .planHead .planPrice span{
            letter-spacing: 2px;
            font-size: 0.7em;
            font-weight: normal;
        }
        .planBody .rowDetail:nth-child(odd){
            background: rgb(245,245,245);
        }
        .planBody .rowDetail:nth-child(even){
            background: rgb(235,235,235);
        }
        .planBody .rowDetail{
            height: 35px;
        }
        .planBody .rowDetail p{
            line-height: 13px;
            padding-top: 11px;
        }
        .planBody .rowDetail p i{
            vertical-align: middle;
        }
        .btn-start{
            background: url(<?= $_SERVER['CDN'] ?>/welcome/Button.png);
            background-repeat: no-repeat;
            background-size: cover;
            color: white;
            margin-top: 10px;
            font-weight: 100;
            height: 34px;
        }
        .planTypeHead{
            height: 80px;
        }
        .included{
            content: url(<?= $_SERVER['CDN'] ?>/welcome/yes.png);
        }
        .notIncluded{
            /*content: url(<?= $_SERVER['CDN'] ?>/welcome/no.png);*/
        }
        #bottomSection{
            position: fixed;
            bottom: 15px;
            left: 15px;
        }
        @media only screen and (max-width: 993px){
            .rowDetail p:not(.MobileRow){
                display: none;
            }
            .rowDetail p.MobileRow{
                display: block;
                font-size: 1.3em;
            }
            .rowDetail p{
                font-size: 1.3em;
            }
            .packageInclude{
                color:#3dd2a1;
            }
            .packageNotInclude{
                -webkit-text-decoration-line: line-through; /* Safari */
                text-decoration-line: line-through;
                color:#e30000bf;
            }
        }

        @media only screen and (max-width: 480px){

            body{
                background-image: none;
                background-color: rgb(156,231,251) !important;
            }
            .logoElement{
                background: white;
                width: 100%;
                padding: 0;
                margin: 0;
            }
            .logoElement img{
                float: none !important;
                margin: 0 auto;
                display: block;
            }

            .wrapper-content {
                 padding: 0;
            }
            .planBody{
                display: none;
            }
            .welcome h1{
                font-size: 29px;
            }
            .welcome h3{
                font-size: 1.50em !important;
            }
            .col-md-offset-2{
                margin-bottom: 25px;
            }
        }

        @media only screen and (max-width: 750px) {
            .saveannual {
                position: unset !important;
            }

            #bottomSection {
                position: unset;
                text-align: center;
                margin-bottom: 13px;
            }
        }

        @media only screen and (max-width: 1024px) {
            .marger{
                margin-top: 0!important;
            }
        }

        @media only screen and (min-width: 750px) {
            .welcomeH1{
                margin-top: 0px !important;
            }
            #welcomeTab1{
                margin: 0px !important;
            }
        }


        .monthlyPlan{

        }
        .yearlyPlan{

        }
        .hidePlan{
            display:none;
        }

        #addNewUserDiv{
            background-color: #fff;
            border-radius: 13px;
        }
        .oldPrice {
            position: absolute;
            margin-top: 3px;
            margin-left: 3px;
            font-size: 13px !important;
        }
        .oldPrice:before {
            position: absolute;
            content: "";
            left: 0;
            top: 50%;
            right: 0;
            border-top: 1px solid;
            border-color: inherit;
            -webkit-transform:rotate(-5deg);
            -moz-transform:rotate(-5deg);
            -ms-transform:rotate(-5deg);
            -o-transform:rotate(-5deg);
            transform:rotate(-5deg);
        }
        .ballon-container {
            background: url(<?= $_SERVER['CDN'] ?>/welcome/cloud.png);
            position: absolute;
            height: 182px;
            width: 350px;
            background-size: cover;
            padding: 5px;
            text-align: center;
            margin-top: -60px;
            margin-left: 80%;
            z-index: 9;
            color: black;
        }
        .ballon {
            display: block;
            color: #414141;
            vertical-align: middle;
            margin-top: 35px;
            font-size: 12px;
        }
        #addNewUserDiv{
            margin-top: 15px;
        }
    </style>
</head>

<body>
<div id="wrapper">

    <div id="page-wrapper" style="margin: 0px !important;">

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-md-12 logoElement">
                    <img src="<?php echo $_SERVER['CDN']; ?>/home/images/NLwebsiteFL2.png" alt="Logo" height="85" style="float: left;">
                </div>
            </div>

            <div class="row">
                <div class="container">
                    <div class="col-md-12 marger" style="margin-top: -43px;">
                        <div id="welcomeTab1" style="margin: 30px auto;">
                            <div class="ballon-container hidden-xs hidden-sm hidden-md">
                            <span class="ballon">
                                <i class="fa fa-handshake-o" style="font-size: 22px;margin-bottom: 5px;"></i><br>
                                <b>REMEMBER , THERE'S<br>
                                    NO COMMITMENT<br></b>
                                Cancel at any time without<br> fees or penalties
                            </span>
                            </div>
                            <div class="row">
                                <div class="col-md-12" >
                                    <div class="center-block text-center welcome">
                                        <?php if (isset($_GET['type']) && $_GET['type'] == 'renew'){ ?>
                                            <h1 style="text-transform: uppercase;font-weight: bold;color:#414141;" class="welcomeH1">Welcome Back to Network Leads</h1>
                                            <h3 style="font-weight: 100;font-size: 1.4em;color: #414141">Your package has expired, please renew your subscription.</h3>
                                        <?php }else{ ?>
                                            <h1 style="text-transform: uppercase;font-weight: bold;color:#414141;" class="welcomeH1">Welcome to Network Leads!</h1>
                                            <h3 style="font-weight: 100;font-size: 1.4em;color: #414141">To get started, Please select your appropriate package</h3>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="monthly">
                                <!--                            <div class="pull-left hidden-xs hidden-sm" style="margin-left: -175px;position: absolute;">-->
                                <!--                                <div class="planTypes">-->
                                <!--                                    <div class="planHead">-->
                                <!--                                        <div class="planName">-->
                                <!--                                            <h5>Promo Code</h5>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="planPrice">-->
                                <!--                                            <span style="font-size: 25px;"><input id="orgCoupon" style="color:black;height: 25px;" class="form-control" type="text"></span>-->
                                <!--                                            <small id="btnContainer"><button onclick="checkCoupon()" id="checkCouponBtn" style="margin-top: 5px;" class="btn btn-xs btn-primary center-block">Redeem</button></small>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="planBody" style="    min-width: 139px;    max-width: 172px;">-->
                                <!--                                        <div class="rowDetail">-->
                                <!---->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>Full lead management</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>File Manager</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>Calendar</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>In-organization Chat</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p style="padding-top: 4px;">Manage trucks, inventory & materials</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>Manage employees</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>Manage departments</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p style="padding-top: 4px;">Receive sms & email when new lead arrives</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>Email marketing</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p># of users in organization</p>-->
                                <!--                                        </div>-->
                                <!--                                        <div class="rowDetail">-->
                                <!--                                            <p>support</p>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <!--                            </div>-->
                                <?php $hrefPlan = false; require_once($_SERVER['LOCAL_NL_PATH']."/plugins/plans/plan.php");?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="bottomSection">
        <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/login/actions/logout.php" class="btn btn-default">Log out</a>
        <a onclick="redeemCoupon()" class="btn btn-default">Redeem coupon</a>
    </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>

<script>

    $(".planContent").hover(function(){
        $(this).children("div").children("div.product-desc").children("div.text-right").children("form").children("button").removeClass("btn-outline");
        $(this).children("div").children("div.product-desc").children("div.text-right").children("form").children("button").css("width","110%");
        $(this).children("div").children("div.product-desc").children("div.text-right").children("form").children("button").css("margin-left","-4%");
    },function(){
        $(this).children("div").children("div.product-desc").children("div.text-right").children("form").children("button").addClass("btn-outline");
        $(this).children("div").children("div.product-desc").children("div.text-right").children("form").children("button").css("width","92.5px");
        $(this).children("div").children("div.product-desc").children("div.text-right").children("form").children("button").css("margin-left","auto");
    });

    function redeemCoupon() {
        swal({
            text: 'Enter coupon code',
            content: "input",
            button: {
                text: "Submit",
                closeModal: true
            },
        })
            .then(coupon => {
                if (!coupon) throw null;
                return fetch(BASE_URL+`/console/actions/system/organization/redeemCoupon.php?coupon=${coupon}`);
            })
            .then(results => {
                return results.json();
            })
            .then(json => {

                if(json == true){
                    swal({
                        title: "Success",
                        text:"Your account is now active",
                        icon: "success",
                        buttons: {
                            gotIt:"Ok"
                        }
                    }).then((isConfirm)=>{
                        goto(BASE_URL+"/console/");
                    });
                }else{
                    toastr.error("Coupon does not exist","Please try again with a valid coupon");
                }
            });
    }

    function goto(url){
        window.location = url;
    }
</script>

</body>
</html>