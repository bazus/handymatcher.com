<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

$organization = new organization($bouncer["credentials"]["orgId"]);
$getUsersOfMyCompany = $organization->getTotalUsersInOrganization();


if(!$bouncer["isUserAnAdmin"]){
    // if user is not an admin, redirect him to leads page
    header('Location: categories/leads/leads.php');
    exit;
}

$showUserInviteInfo = true;
$showAddProviders = true;
if ( (strtotime("now") - strtotime($bouncer['userData']['signUpDate'])) > 60*60*24 ){
    $showUserInviteInfo = false;
}
if ( (strtotime("now") - strtotime($bouncer["organizationData"]['createdAt'])) > 60*60*24*7 ){
    $showAddProviders = false;
}
if (isset($_COOKIE['inviteNewUsers'])){
    $showUserInviteInfo = false;
}
if (!$bouncer["isNotFreeUser"]){
    $showUserInviteInfo = false;
}
if ($getUsersOfMyCompany > 1){
    $showUserInviteInfo = false;
}
if (!$bouncer['isUserAnAdmin']){
    $showUserInviteInfo = false;
    $showAddProviders = false;
}
if ($bouncer['organizationData']['organizationPackage'] != 3){
    $showUserInviteInfo = false;
}

if($showAddProviders == true){
    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $totalLeadProviders = $leadProviders->getTotalProvidersActive();

    if($totalLeadProviders != 0){
        $showAddProviders = false;
    }
}
?>

<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Network Leads</title>

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/animate.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/style.css" rel="stylesheet">

        <!-- Ladda style -->
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.css" rel="stylesheet">


        <link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/index.css?v=1.3.1" rel="stylesheet">

        <script>var BASE_URL = "<?php $_SERVER['LOCAL_NL_URL'] ?>";var expireDate = "<?= date('') ?>"</script>

        <style>
            .noProviders{
                cursor: pointer;
                margin: 0;
                display: block;
                width:100%;
                text-align: center;
                background: #ed5564;
            }

            .noProviders p{
                padding: 4px;
                margin: 0;
                line-height: 1;
                vertical-align: middle;
                overflow: hidden;
                color:white;
            }
            .label-purple{
                background-color: #652bbd;
                color:#fff;
            }
            .label-orange{
                background-color: #FF9800;
                color:#fff;
            }
            .lead-status-container{
                display: flex;
                justify-content: space-between;
                margin-top:7px;
            }
            .lead-status-amount{
                padding:3px 10px;
                min-width: 40px;
            }
            .lead-status{
                padding:3px 6px;
                width: 90px;
            }
        </style>
    </head>

    <body class="<?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>mini-navbar<?php } ?>">
    <?php if ($showAddProviders == true){ ?>
        <span id="noProviders" class="noProviders" onclick="location.href = '<?= $_SERVER["LOCAL_NL_URL"] ?>/console/categories/leads/leadProviders.php?addProvider=1'">
            <p>You do not have any lead providers set up yet</p>
            <p>Click here to add a lead provider</p>
        </span>
    <?php } ?>
    <div id="wrapper">
       <?php require_once("side.php"); ?>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <?php require_once("top.php"); ?>
            <div class="wrapper wrapper-content">
<?php
if($bouncer['organizationData']["organizationTypeId"] == "1"){

    $checkAuths_financials = $bouncer["userAuthorization"]->checkAuthorizations(array(["financials",1]));

    if($checkAuths_financials) {
        ?>
        <?php if ($showUserInviteInfo){ ?>
            <div class="panel panel-info" id="addNewUserPanel">
                <div class="panel-heading">
                    <span class="close-link pull-right" style="margin-top: -9px;" onclick="closeAddNewUsers()">
                        <i class="fa fa-times"></i>
                    </span>
                </div>
                <div class="panel-body">
                    <p>
                        You do not have any users in your organization.<br>
                        Would you like to invite a new user?<br>
                        <br>
                        <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/user/management.php?tab=users&showUsersModal=1" role="button" class="btn btn-info btn-sm">Invite new user</a>
                        <a onclick="closeAddNewUsers()" class="btn btn-default btn-sm">Dismiss</a>
                    </p>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <div id="dateRangeHomePage" class="form-control"
                     style="float: left;margin-bottom: 10px; max-width: fit-content;">
                    <i class="fa fa-calendar"></i>
                    <span></span> <b class="caret"></b>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right spanner">Monthly</span>
                        <h5>Leads</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="totalLeadsByDate">0</h1>
                        <div class="stat-percent font-bold"><span id="totalLeadsChange"></span></div>
                        <small>Total leads</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right spanner">Monthly</span>
                        <h5>Jobs Booked</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="totalBookedEstimatesByDate">0</h1>
                        <div class="stat-percent font-bold"><span
                                    id="totalBookedEstimatesChange"></span></div>
                        <small>Total Jobs Booked</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right spanner">Monthly</span>
                        <h5>Jobs Completed</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="totalFinishedJobsByDate">0</h1>
                        <div class="stat-percent font-bold"><span id="totalFinishedJobsChange"></span>
                        </div>
                        <small>Total Jobs Completed</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right spanner">Monthly</span>
                        <h5>Job Payments</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="totalLeadPaymentsByDate">$0.00</h1>
                        <div class="stat-percent font-bold"><span id="totalLeadPaymentsChange"></span>
                        </div>
                        <small>Total Job Payments</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Activity</h5>
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" id="activity-task1" onclick="setGraphDate(1,this)" class="btn btn-xs btn-white active">Daily</button>
                                <button type="button" id="activity-task2" onclick="setGraphDate(2,this)" class="btn btn-xs btn-white">Weekly</button>
                                <button type="button" id="activity-task3" onclick="setGraphDate(3,this)" class="btn btn-xs btn-white">Monthly</button>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-dashboard-chart"
                                         style="padding: 0px; position: relative;"></div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <ul class="stat-list">
                                    <li>
                                        <h2 class="no-margins" id="chartTotalLeads">0</h2>
                                        <small>Total Leads</small>
                                        <hr>
                                    </li>
                                    <li>
                                        <h2 class="no-margins" id="chartTotalBooked">0</h2>
                                        <small>Booked Jobs</small>
                                        <hr>

                                    </li>
                                    <li>
                                        <h2 class="no-margins" id="chartTotalPayments">$0.00</h2>
                                        <small>Payments from Jobs</small>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <!--                    <div class="col-lg-4">-->
            <!---->
            <!--                    </div>-->

            <div class="col-lg-12">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Top Lead Providers</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th class="providerWidth">Provider Name</th>
                                        <th class="providerWidth">Leads Received</th>
                                        <th class="providerWidth">Last Lead</th>
                                        <th class="providerWidth">E-mails Open Rate</th>
                                    </tr>
                                    </thead>
                                    <tbody id="topProviders">
                                    <tr style="border: 1px solid #e4e4e4;"><td colspan="5" style="background-color: #e7eaec36;text-align: center;">No Providers Yet <a href="<?= $_SERVER['LOCAL_NL_URL'] ?>/console/categories/leads/leadProviders.php">(Add Provider)</a></td></tr>
                                    </tbody>
                                </table>
                                <div id="providerButton" style="margin-top: 3px; "></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-default pull-right spanner">Monthly</span>
                                <h5>Leads By Status</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div id="lead-status-wrapper">
                                    <div class="lead-status-container" style="margin-top: 0;">
                                        <div class="label label lead-status">New</div>
                                        <div class="label label lead-status-amount" id="leadStatusNewAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-warning lead-status">Pending</div>
                                        <div class="label label-warning lead-status-amount" id="leadStatusPendingAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-info lead-status">Quoted</div>
                                        <div class="label label-info lead-status-amount" id="leadStatusQuotedAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-primary lead-status">Booked</div>
                                        <div class="label label-primary lead-status-amount" id="leadStatusBookedAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-primary lead-status">In progress</div>
                                        <div class="label label-primary lead-status-amount" id="leadStatusInProgressAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-primary lead-status">Storage/Transit</div>
                                        <div class="label label-primary lead-status-amount" id="leadStatusStorageAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-success lead-status">Completed</div>
                                        <div class="label label-success lead-status-amount" id="leadStatusCompletedAmount">0</div>
                                    </div>
                                    <div class="lead-status-container">
                                        <div class="label label-danger lead-status">Cancelled</div>
                                        <div class="label label-danger lead-status-amount" id="leadStatusCancelledAmount">0</div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-purple pull-right spanner">Monthly</span>
                                <h5>E-mails Sent</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins" id="totalEmailsSentByDate">0</h1>
                                <div class="stat-percent font-bold"><span id="totalEmailsSentChange"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-orange pull-right spanner">Monthly</span>
                                <h5>SMS Sent</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins" id="totalLeadSmsSentByDate">0</h1>
                                <div class="stat-percent font-bold"><span id="totalLeadSmsSentChange"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>


<!--        </div>-->


        <?php

        }
    }

?>
            </div>
            <?php require_once("footer.php"); ?>
        </div>

        <?php require_once("rightside.php"); ?>


    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/bootstrap.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/inspinia.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/toastr/toastr.min.js"></script>

    <!-- Ladda -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>

    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
    <!-- Date range picker -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/moment.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/daterangepicker-latest/daterangepicker.min.js"></script>

    <!-- Flot -->
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.time.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/index.min.js"></script>
    <?php
    if($bouncer['organizationData']["organizationTypeId"] == "1") {
        ?>
        <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system/stats.min.js"></script>
        <?php
    }
    ?>
    </body>
</html>
