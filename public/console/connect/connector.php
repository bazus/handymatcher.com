<?php
/* Description : this class handles all queries sent to the MYSQL DB
 * Developer : Niv Apo
 * Created : 28.3.18
 */

if(!isset($_SERVER["ENVIRONMENT"])){
    $_SERVER["ENVIRONMENT"] = "prod";
}

if($_SERVER["ENVIRONMENT"] == "stage" || $_SERVER["ENVIRONMENT"] == "local") {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

class connector{

    private $mysql;
    public $errors = array();
    private $localTimeZoneOffset = "America/New_York";

    public function __construct()
    {
        // connect $this->mysql to global mysql var
        $this->mysql = $GLOBALS['mysql'];
        // =============== (SATRT) get user timezone ===============
        if(isset($_COOKIE['localTimeZone'])) {
            $this->localTimeZoneOffset = $_COOKIE['localTimeZone'];
        }
        // =============== (END) get user timezone ===============

    }

    public function execute($query = '',$binds = array(),$errorVar = NULL,$ignoreErrors = false){
        // Standard sql execution

        try{
            $executeIt = $this->mysql->prepare($query);

            if(!is_null($binds) && count($binds) > 0) {
                // Bind params
                foreach ($binds as $bind) {

                    // If bind 3 is true - pass the timezone
                    if(isset($bind[3]) && $bind[3] == true){
                        $executeIt->bindValue($bind[0], $this->localTimeZoneOffset, $bind[2]);
                    }else{
                        $executeIt->bindValue($bind[0], $bind[1], $bind[2]);
                    }
                }
            }

            if(!$executeIt->execute()){
                return false;
            }

            return $executeIt;

        }catch(PDOException $e) {
            if($ignoreErrors === true){return false;}

            $errorMSG = "Error: " . $e->getMessage();
            if($_SERVER["ENVIRONMENT"] == "stage" || $_SERVER["ENVIRONMENT"] == "local") {
                //var_dump($errorMSG);
                //var_dump($query);
            }
            $this->errors[] = $errorMSG;

            if($this->is_assoc($errorVar)) {
                $title = $errorVar['title'];
                $type = $errorVar['type'];
                $problem = $errorMSG;
                $level = "";
                $description = "";
                $theQbinds = "";
                $isUrgent = "12345675645634";
            }else{
                $title = $errorVar[0];
                $type = $errorVar[1];
                $problem = $errorMSG;
                $level = $errorVar[2];
                $description = $errorVar[3]." | params : ".json_encode($errorVar[4]);
                $theQbinds = json_encode($binds);
                if(isset($errorVar[5])){
                    $isUrgent = $errorVar[5];
                }else{
                    $isUrgent = false;
                }
            }

            if($isUrgent === NULL){$isUrgent = false;}

            if($isUrgent == true){
                shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>"[niv] Urgent Error In System"))) . ""); // > /dev/null 2>/dev/null &
            }

            list($scriptPath) = get_included_files();

            $binds = [];
            $binds[] = array(':title', $title, PDO::PARAM_STR);
            $binds[] = array(':type', $type, PDO::PARAM_STR);
            $binds[] = array(':problem', $problem, PDO::PARAM_STR);
            $binds[] = array(':page', $scriptPath, PDO::PARAM_STR);
            $binds[] = array(':description', $description, PDO::PARAM_STR);
            $binds[] = array(':level', $level, PDO::PARAM_STR);
            $binds[] = array(':binds', $theQbinds, PDO::PARAM_STR);
            $binds[] = array(':isUrgent', $isUrgent, PDO::PARAM_BOOL);
            $binds[] = array(':environment', $_SERVER["ENVIRONMENT"], PDO::PARAM_STR);

            $this->execute("INSERT INTO networkleads_db.systemLog(title,type,problem,page,description,level,binds,isUrgent,environment) VALUES(:title,:type,:problem,:page,:description,:level,:binds,:isUrgent,:environment)",$binds,NULL,true);

            return false;
        }

        return false;
    }

    public function fetch($results = '',$secureFetch = false){
        if(!$secureFetch) {
            return $results->fetch(PDO::FETCH_ASSOC);
        }else{
            $dataArr = $results->fetch(PDO::FETCH_ASSOC);

            if($dataArr == false){return false;}
            array_walk($dataArr, array($this, 'secureValue'));
            return $dataArr;
        }
    }
    
    public function secureValue(&$item1, $key){
        $item1 = htmlentities($item1);
    }

    public function fetch_num_rows($results = ''){
        return $results->fetchColumn();
    }
    public function last_insert_id(){
        return $this->mysql->lastInsertId();
    }

    public static function is_assoc(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }

    public function getLastError(){
        return $this->errors[count($this->errors)-1];
    }
}


/*
$database = new connect("mydb");
$database->close();
*/