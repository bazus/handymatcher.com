class DialerAPI {
    constructor() {

        this.conn = null;

        fetch('https://flavescent-heron-5207.twil.io/capability-token')
            .then(res => res.json())
            .then(data => {
                console.log('Token: ', data);

                // Setup Twilio.Device
                this.device = new Twilio.Device(data.token, {
                    // Set Opus as our preferred codec. Opus generally performs better, requiring less bandwidth and
                    // providing better audio quality in restrained network conditions. Opus will be default in 2.0.
                    codecPreferences: ['opus', 'pcmu'],
                    // Use fake DTMF tones client-side. Real tones are still sent to the other end of the call,
                    // but the client-side DTMF tones are fake. This prevents the local mic capturing the DTMF tone
                    // a second time and sending the tone twice. This will be default in 2.0.
                    fakeLocalDTMF: true,
                    // Use `enableRingingState` to enable the device to emit the `ringing`
                    // state. The TwiML backend also needs to have the attribute
                    // `answerOnBridge` also set to true in the `Dial` verb. This option
                    // changes the behavior of the SDK to consider a call `ringing` starting
                    // from the connection to the TwiML backend to when the recipient of
                    // the `Dial` verb answers.
                    enableRingingState: true,
                });

                this.device.on('ready', device => {
                    console.log('Twilio.Device Ready!');
                });

                this.device.on('error', error => {
                    console.log('Twilio.Device Error: ' + error.message);
                    toastr.error('Error: ' + error.message);
                });

                this.device.on('connect', conn => {
                    console.log('Successfully established call!');
                    this.conn = conn;
                    dialer.callStarted();
                });

                this.device.on('disconnect', function (conn) {
                    console.log('Call ended.');
                    dialer.stopCall();
                });
            })
            .catch(function (err) {
                console.log('tesssssst');
                console.log(err);
            });
    }

    startCall() {
        // get the phone number to connect the call to
        var params = {
            To: window.dialer.inputedNumber
        };

        console.log('Calling ' + params.To + '...');

        if (this.device) {
            var outgoingConnection = this.device.connect(params);
            outgoingConnection.on('ringing', function () {
                console.log('Ringing...');
            });
        }
    }

    stopCall() {
        console.log('Hanging up...');
        if (this.device) {
            this.device.disconnectAll();
        }
    }

    toggleMute() {
        console.log(this.conn.isMuted());

        if (!this.conn.isMuted()) {
            console.log('muted...');
            this.conn.mute(true)
            console.log(this.conn.isMuted());

            return true
        } else {
            console.log('unmuted...');
            this.conn.mute(false)
            console.log(this.conn.isMuted());
            return false
        }
    }
}

window.dialerApi = new DialerAPI();