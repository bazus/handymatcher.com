
class Dialer {
    constructor() {
        this.isFirstTimeOpened = true;
        this.inputedNumber = '';
        this.isInCall = false;

        this.numPadScreen = null;
        this.dialerCallScreen = null;
        this.numberInput = null;
        this.numberCallingDiv = null;
        this.backToCallBtn = null;
        this.muteCallBtn = null;
        this.exitBtn = null;
        this.dailerHTML = null;

        //this gets the file path of this script in order for us to load files relative to this file location
        var scripts = document.getElementsByTagName("script");
        this.addrOfFile = scripts[scripts.length - 1].src;

        this.initDialerWrapper();

        $('#dialerBtn').click(() => this.open());


        open();
    }

    initDialerWrapper() {
        this.dailerHTML = document.createElement('div');
        this.dailerHTML.className = 'dialerWrapper';

        //todo: fix adress to work with any env
        $.get(this.addrOfFile + "/../dialer.html", (data) => {
            this.dailerHTML.innerHTML = data.trim();
        });
    }

    initAfterSwal() {
        $('#callBtn').click(() => this.goToCall());
        $('#stopCallBtn').click(() => this.stopCall());
        $('.dialerNumPad_number').click((e) => this.onNumClick(e.target.innerHTML));
        $('#backToKeypadBtn').click(() => this.goToKeyBoard())

        this.numPadScreen = $('.dialerNumPad');
        this.dialerCallScreen = $('.dialerCallScreen');

        this.numberInput = $('#numberInput');
        this.numberCallingDiv = $('#numberCallingDiv');
        this.backToCallBtn = $('#backToCallBtn');
        this.muteCallBtn = $('#muteCallBtn')
        this.exitBtn = $('#exitBtn')

        this.backToCallBtn.bind('click', () => this.goToCall())
        this.muteCallBtn.click(() => this.toggleMuteCall())
        this.exitBtn.click(() => swal.close())

        this.numberInput.change((e) => this.inputedNumber = e.target.value)
        this.numberInput.on("animationend", () => {
            this.numberInput.removeClass("shake");
        });

        // const phoneMask = new Inputmask("+9725x-xxx-xxxx");
        // phoneMask.definitions.x = phoneMask.definitions['9'];
        // delete phoneMask.definitions[9];

        // phoneMask.mask(this.numberInput);


        this.dialerCallScreen.hide()
        this.isFirstTimeOpened = false;  
    }

    onNumClick(num) {
        this.inputedNumber += num;
        this.numberInput.val(this.inputedNumber);
        this.numberInput.focus();
    }

    sanity() {
        console.log('hey there !')
    }

    open() {
        swal({
            content: this.dailerHTML,
            buttons: false,
            closeOnClickOutside: false,
            className: 'swal-dialer'
        });

        if (this.isFirstTimeOpened) {
            this.initAfterSwal()
        }
    }

    toggleMuteCall() {
        if (window.dialerApi.toggleMute()){
            this.muteCallBtn.text('unmute');
        }else{
            this.muteCallBtn.text('mute');
        }
    }

    goToCall() {
        if (this.validateNumber()) {
            if (!this.isInCall) {
                window.dialerApi.startCall();
                this.isInCall = true;
            }
            this.numPadScreen.hide()
            this.dialerCallScreen.show()
            this.numberCallingDiv.text('calling ' + this.inputedNumber);

            this.backToCallBtn.text("back");
            this.backToCallBtn.bind('click', () => this.goToCall())
            this.exitBtn.hide();
        }
    }

    goToKeyBoard() {
        this.numPadScreen.show()
        this.dialerCallScreen.hide()
    }

    stopCall() {
        window.dialerApi.stopCall();
        this.backToCallBtn.text("");
        this.backToCallBtn.unbind();
        this.exitBtn.show();
        this.goToKeyBoard();
        this.isInCall = false;
    }

    callStarted() {
        this.numberCallingDiv.text('talking with ' + this.inputedNumber);
    }

    validateNumber() {
        console.log(this.inputedNumber);

        // if (/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/.test(this.inputedNumber)){
        // if (/^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/.test(this.inputedNumber)) {
            if(/\+[1-9]{10,15}/)
            return true
        // }
        this.numberInput.addClass("shake");
        return false;
    }
}

var dialer = new Dialer();