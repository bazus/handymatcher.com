function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function resendInvitationEmail(userId,obj,error,oldValue){

    var content = document.createElement("div");

    var header = document.createElement("h2");
    header.style.margin = "20px auto";
    header.style.fontWeight = "bold";
    header.style.fontSize = "20px";
    var headerText = document.createTextNode("Resend invitation by email");

    header.appendChild(headerText);

    content.appendChild(header);

    var input = document.createElement("input");
    input.style.margin = "25px auto";
    input.classList.add("form-control");
    input.id = "invitationEmail";
    input.placeholder = "Email Address";
    input.setAttribute("onkeyup","$('#invitationError').remove();checkInviteEmail(this);");

    if (oldValue !== undefined){
        input.value = oldValue;
    }

    content.appendChild(input);

    if (error !== undefined){

        var errorSmall = document.createElement("small");
        errorSmall.style.marginTop = "20px";
        errorSmall.style.display = "block";
        errorSmall.style.textAlign = "left";
        errorSmall.id = "invitationError";
        errorSmall.style.color = "rgb(221, 80, 80)";

        var errorText = document.createTextNode(error);

        errorSmall.appendChild(errorText);

        content.appendChild(errorSmall);

    }
    var buttonContainer = document.createElement("div");
    buttonContainer.style.height = "50px";
    buttonContainer.style.margin = "15px 0";

    var button = document.createElement("button");
    button.id = "closeSwal";
    button.style.cssFloat = "right";
    button.style.marginLeft = "15px";
    button.classList.add("btn");
    button.classList.add("btn-default");

    var buttonText = document.createTextNode("Close");

    button.appendChild(buttonText);
    buttonContainer.appendChild(button);

    var button = document.createElement("button");
    button.id = "sendInvitation";
    button.setAttribute("disabled","disabled");
    button.style.cssFloat = "right";
    button.style.marginLeft = "15px";
    button.classList.add("btn");
    button.classList.add("btn-success");

    var buttonText = document.createTextNode("Re-send user invitation");

    button.appendChild(buttonText);
    buttonContainer.appendChild(button);

    content.appendChild(buttonContainer);

    swal({
        title: false,
        content: content,
        buttons: false
    });

    $("#closeSwal").on("click",function () {
        swal.close();
    });
    
    $("#sendInvitation").on("click",function () {
        swal.close();
        var email = document.getElementById("invitationEmail").value;
        if (validateEmail(email)) {
            var strUrl = BASE_URL + '/console/actions/user/resendInvitationEmail.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    email: email,
                    userId: userId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);

                if (data == true) {
                    obj.parentNode.innerHTML = '<span class="label label-primary">Invitation Email Sent</span>';
                } else {
                    obj.parentNode.innerHTML = '<span class="label label-danger">Please Wait A While</span>';
                }

            });
        }else{
            resendInvitationEmail(userId,obj,"* Email address is not valid",email);
        }
    });



}

function checkInviteEmail(obj){
    if (validateEmail(obj.value)) {
        $('#sendInvitation').prop('disabled', false);
    }else{
        $('#sendInvitation').attr('disabled', 'disabled');
    }
}

function saveOrganizationForm() {

    var l = $('#saveChanges').ladda();

        l.ladda('start');

        var form = $("#organizationDataForm");
        var url = BASE_URL + '/console/actions/system/organization/saveChanges.php';

        jQuery.ajax({
            url: url,
            method: "POST",
            data: form.serialize(),
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try {
                data = JSON.parse(data);
                if (data == true) {
                    toastr.success('Your changes were successfully saved.', 'Changes Saved');
                } else {
                    toastr.error('Your changes were not saved. Please try again later.', 'Oops');
                }
            } catch (e) {
                toastr.error('Your changes were not saved. Please try again later.', 'Oops');
            }

            // Stop loading
            l.ladda('stop');
        });
}

$(document).ready(function (){

    $('#companyName').bind("keyup change", function(e) {
        generateCompanyNameKey(this);
    });

    $('#companyKey').bind("keyup change", function(e) {
        cleanCompanyNameKey(this);
    });
});

function generateCompanyNameKey(obj){

    var companyInput   = $(obj).val();

    // To lower case
    companyInput = companyInput.toLowerCase().trim();

    // Remove all non-alpha-numeric and replace spaces with the '-' signs
    companyInput = companyInput.split(/[^A-Za-z0-9 ]/g).join("");
    companyInput = companyInput.split(" ").join("-");

    $('#companyKey').val(companyInput);

    if(!companyInput){
        $('#companyKeyTaken').show();
        $('#companyKeyNotTaken').hide();
        return;
    }

    checkIfOrganizationNameKeyExists(companyInput);
}

function cleanCompanyNameKey(obj){

    var companyKeyInput = $(obj).val();
    var originalInput =  $(obj).val();
    if(!companyKeyInput){
        $('#companyKeyTaken').show();
        $('#companyKeyNotTaken').hide();
        return;
    }
    // To lower case & trim
    companyKeyInput = companyKeyInput.toLowerCase().trim();

    // Remove all non-alpha-numeric (but keep the '-' signs)
    companyKeyInput = companyKeyInput.split("-").join(" ");
    companyKeyInput = companyKeyInput.split(/[^A-Za-z0-9 ]/g).join("");
    companyKeyInput = companyKeyInput.split(" ").join("-");

    // Remove spacial chars from BEGINNING of string
    if(companyKeyInput[0].match(/[^a-z0-9+]+/gi)){
        companyKeyInput = companyKeyInput.substring(1);
    }

    if(originalInput != companyKeyInput){
        $('#companyKey').val(companyKeyInput);
    }
    checkIfOrganizationNameKeyExists(companyKeyInput);
}

function checkIfOrganizationNameKeyExists(companyKey){

    var strUrl = BASE_URL+'/console/actions/user/checkCompanyKeyExist.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            companyKey: companyKey
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        data = JSON.parse(data);

        if(data == true){
            $('#companyKeyTaken').hide();
            $('#companyKeyNotTaken').show();
        }else {
            $('#companyKeyTaken').show();
            $('#companyKeyNotTaken').hide();
        }

    });

}



$(".navbar-minimalize").on("click",function(){
    $(".mob").toggle();
});
if(!$("body").hasClass("mini-navbar")){
    $(".mob").hide();
}

function revokeUser(userId,type,obj){

    var strUrl = BASE_URL+'/console/actions/user/revokeUser.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            userId: userId,
            type: type
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        data = JSON.parse(data);
        if(data == true){
            if(type == 1){
                obj.parentNode.innerHTML = '<a role="menuitem" class="label-default" style="color:white;" onclick="revokeUser('+userId+',0,this)">UnRevoke</button>';
                toastr.success('Your changes were successfully saved. User Is Revoked','Changes Saved');
            }else{
                obj.parentNode.innerHTML = '<a role="menuitem" class="label-danger" style="color:white;" onclick="revokeUser('+userId+',1,this)">Revoke</a>';
                toastr.success('Your changes were successfully saved. User Is Unrevoked','Changes Saved');
            }
        }

        setTimeout(function(){ window.location = '?tab=users'; }, 1500);



    });

}
var selectedUserIdTab = "";
var selectedDepIdTab = "";
function changedTab(tabName){
    tab = tabName;

    var organizationTopRight = document.getElementById('organizationTopRight');
    var usersTopRight = document.getElementById('usersTopRight');
    var depTopRight = document.getElementById('depTopRight');

    if(organizationTopRight){organizationTopRight.style.display = 'none';}
    if(usersTopRight){usersTopRight.style.display = 'none';}
    if(depTopRight){depTopRight.style.display = 'none';}

    if(tabName == "main" && organizationTopRight){
        organizationTopRight.style.display = '';
    }

    if(tabName == "users" && usersTopRight){
        usersTopRight.style.display = '';
    }

    if(tabName == "main"){
        var sideTabs = document.getElementById("sideTabs").getElementsByClassName("tab-pane active");
        for(var i = 0; i < sideTabs.length; i++){
            sideTabs[i].className = "tab-pane";
        }
        document.getElementById("organizationSideTab").className = 'tab-pane active';
    }


    if(tabName == "departments" && depTopRight){
        depTopRight.style.display = '';
    }

}

function showUserTab(userId) {
    selectedUserIdTab = userId;
    document.getElementById("organizationTopRight").style.display = "none";
    document.getElementById('contact-'+userId+'-trigger').click();
}

function showDepartmentTab(depId) {
    selectedDepIdTab = depId;
    document.getElementById("organizationTopRight").style.display = "none";
    document.getElementById('department-'+depId+'-trigger').click();
}

function toggleSales(userId,create){
    if (create && document.getElementById("user_salesBonusPerc_"+userId) != null){
        var container = document.getElementById("salesmanBonus-"+userId);

        var li = document.createElement("li");
        li.classList.add("list-group-item");

        var label = document.createElement("label");
        var labelText = document.createTextNode("Sales Bonus Percentage");

        label.appendChild(labelText);

        var input = document.createElement("input");
        input.setAttribute("type",'number');
        input.setAttribute("min",0);
        input.setAttribute("max",100);
        input.classList.add("form-control");
        input.id = "user_salesBonusPerc_"+userId;
        input.value = 0;

        li.appendChild(label);
        li.appendChild(input);

        container.appendChild(li);

    }else{
        $("#salesmanBonus-"+userId).html("");
    }
}

function checkUserType(userId){
    var user_type = document.getElementById("user_userType_"+userId).value;
    if (user_type == "3"){
        toggleSales(userId,true);
    }else{
        toggleSales(userId,false);
    }
}

function updateUserData(userId){

    var user_comment = document.getElementById("user_comment_"+userId).value;
    var user_fullName = document.getElementById("user_fullName_"+userId).value;
    var user_email = document.getElementById("user_email_"+userId).value;
    var user_phoneCC = countryCodeStates['countryCodeState_'+userId];
    var user_phone = document.getElementById("user_phone_"+userId).value;
    var user_jobTitle = document.getElementById("user_jobTitle_"+userId).value;
    var user_department = document.getElementById("user_department_"+userId).value;
    var user_type = document.getElementById("user_userType_"+userId).value;
    if (document.getElementById("user_salesBonusPerc_"+userId) != null){
        var user_salesBonusPerc = document.getElementById("user_salesBonusPerc_"+userId).value;
    }else{
        var user_salesBonusPerc = 0;
    }
    var strUrl = BASE_URL+'/console/actions/user/updateUserData.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            userId: userId,
            comments: user_comment,
            fullName: user_fullName,
            email: user_email,
            phoneCC:user_phoneCC,
            phone: user_phone,
            jobTitle: user_jobTitle,
            department: user_department,
            userType: user_type,
            userSalesBonusPerc:user_salesBonusPerc
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        try{
            data = JSON.parse(data);

            if(data == true){
                toastr.success('Your changes were successfully saved.','Changes Saved');
            }else{
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }
        }catch (e){
            toastr.error('Your changes were not saved. Please try again later.','Oops');
        }

    });
}


function updateDepartmentData(depId){

    var title = document.getElementById("department_title_"+depId).value;
    var address = document.getElementById("department_address_"+depId).value;
    var city = document.getElementById("department_city_"+depId).value;
    var state = document.getElementById("department_state_"+depId).value;
    var zip = document.getElementById("department_zip_"+depId).value;
    var phone = document.getElementById("department_phone_"+depId).value;
    var owner = document.getElementById("department_owner_"+depId).value;
    var description = document.getElementById("department_description_"+depId).value;

    var strUrl = BASE_URL+'/console/actions/user/updateDepartmentData.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            depId: depId,
            title: title,
            address: address,
            city: city,
            state:state,
            zip: zip,
            phone: phone,
            owner:owner,
            description:description
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        try{
            data = JSON.parse(data);

            if(data == true){
                toastr.success('Your changes were successfully saved.','Changes Saved');
            }else{
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }
        }catch (e){
            toastr.error('Your changes were not saved. Please try again later.','Oops');
        }

        setTimeout(function(){
            window.location = "?tab="+tab+"&departmentSelected="+selectedDepIdTab;
        }, 1500);
    });
}

function deleteDepartment(depId){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this department later.",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/user/deleteDepartment.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    depId: depId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try{
                    data = JSON.parse(data);

                    if(data == true){
                        swal({title:"Deleted!", text:"Department has been deleted.",icon: "success",buttons: {ok:"Ok"}}).then(function(x){
                            goTo("?tab="+tab);
                        });
                    }else{
                        swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",buttons: {ok:"Ok"}}).then(function(x){
                            goTo("?tab="+tab);
                        });
                    }
                }catch (e){
                    swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",buttons: {ok:"Ok"}}).then(function(x){
                        goTo("?tab="+tab);
                    });
                }

            });

        }
    });
}

var countryCodeStates = [];
function ccstate(userId,userState) {
    var countryCodeState = userState;
    countryCodeStates['countryCodeState_'+userId] = countryCodeState;
    $('#countryCodeState_'+userId).flagStrap({
        countries: {
            "AF": "Afghanistan +93",
            "AL": "Albania +355",
            "DZ": "Algeria +213",
            "AS": "American Samoa +1",
            "AD": "Andorra +376",
            "AO": "Angola +244",
            "AI": "Anguilla +1",
            "AG": "Antigua and Barbuda +1",
            "AR": "Argentina +54",
            "AM": "Armenia +374",
            "AW": "Aruba +297",
            "AU": "Australia +61",
            "AT": "Austria +43",
            "AZ": "Azerbaijan +994",
            "BS": "Bahamas +1",
            "BH": "Bahrain +973",
            "BD": "Bangladesh +880",
            "BB": "Barbados +1",
            "BY": "Belarus +375",
            "BE": "Belgium +32",
            "BZ": "Belize +501",
            "BJ": "Benin +229",
            "BM": "Bermuda +1",
            "BT": "Bhutan +975",
            "BO": "Bolivia, Plurinational State of +591",
            "BA": "Bosnia and Herzegovina +387",
            "BW": "Botswana +267",
            "BR": "Brazil +55",
            "IO": "British Indian Ocean Territory +246",
            "BN": "Brunei Darussalam +673",
            "BG": "Bulgaria +359",
            "BF": "Burkina Faso +226",
            "BI": "Burundi +257",
            "KH": "Cambodia +855",
            "CM": "Cameroon +237",
            "CA": "Canada +1",
            "CV": "Cape Verde +238",
            "KY": "Cayman Islands +1",
            "CF": "Central African Republic +236",
            "TD": "Chad +235",
            "CL": "Chile +56",
            "CN": "China +86",
            "CO": "Colombia +57",
            "KM": "Comoros +269",
            "CK": "Cook Islands +682",
            "CR": "Costa Rica +506",
            "CI": "Côte d'Ivoire +225",
            "HR": "Croatia +385",
            "CU": "Cuba +53",
            "CW": "Curaçao +599",
            "CY": "Cyprus +357",
            "CZ": "Czech Republic +420",
            "DK": "Denmark +45",
            "DJ": "Djibouti +253",
            "DM": "Dominica +1",
            "DO": "Dominican Republic +1",
            "EC": "Ecuador +593",
            "EG": "Egypt +20",
            "SV": "El Salvador +503",
            "GQ": "Equatorial Guinea +240",
            "ER": "Eritrea +291",
            "EE": "Estonia +372",
            "ET": "Ethiopia +251",
            "FK": "Falkland Islands (Malvinas) +500",
            "FO": "Faroe Islands +298",
            "FJ": "Fiji +679",
            "FI": "Finland +358",
            "FR": "France +33",
            "GF": "French Guiana +594",
            "PF": "French Polynesia +689",
            "GA": "Gabon +241",
            "GM": "Gambia +220",
            "GE": "Georgia +995",
            "DE": "Germany +49",
            "GH": "Ghana +233",
            "GI": "Gibraltar +350",
            "GR": "Greece +30",
            "GL": "Greenland +299",
            "GD": "Grenada +1",
            "GP": "Guadeloupe +590",
            "GU": "Guam +1",
            "GT": "Guatemala +502",
            "HT": "Haiti +509",
            "HN": "Honduras +504",
            "HK": "Hong Kong +852",
            "HU": "Hungary +36",
            "IS": "Iceland +354",
            "IN": "India +91",
            "ID": "Indonesia +62",
            "IR": "Iran +98",
            "IQ": "Iraq +964",
            "IE": "Ireland +353",
            "IL": "Israel +972",
            "IT": "Italy +39",
            "JM": "Jamaica +1",
            "JP": "Japan +81",
            "JO": "Jordan +962",
            "KZ": "Kazakhstan +7",
            "KE": "Kenya +254",
            "KI": "Kiribati +686",
            "KW": "Kuwait +965",
            "KG": "Kyrgyzstan +996",
            "LV": "Latvia +371",
            "LB": "Lebanon +961",
            "LS": "Lesotho +266",
            "LR": "Liberia +231",
            "LY": "Libya +218",
            "LI": "Liechtenstein +423",
            "LT": "Lithuania +370",
            "LU": "Luxembourg +352",
            "MO": "Macao +853",
            "MK": "Macedonia (FYROM) +389",
            "MG": "Madagascar +261",
            "MW": "Malawi +265",
            "MY": "Malaysia +60",
            "MV": "Maldives +960",
            "ML": "Mali +223",
            "MT": "Malta +356",
            "MH": "Marshall Islands +692",
            "MQ": "Martinique +596",
            "MR": "Mauritania +222",
            "MU": "Mauritius +230",
            "MX": "Mexico +52",
            "FM": "Micronesia +691",
            "MD": "Moldova +373",
            "MC": "Monaco +377",
            "MN": "Mongolia +976",
            "ME": "Montenegro +382",
            "MS": "Montserrat +1",
            "MA": "Morocco +212",
            "MZ": "Mozambique +258",
            "MM": "Myanmar (Burma) +95",
            "NA": "Namibia +264",
            "NR": "Nauru +674",
            "NP": "Nepal +977",
            "NL": "Netherlands +31",
            "NC": "New Caledonia +687",
            "NZ": "New Zealand +64",
            "NI": "Nicaragua +505",
            "NE": "Niger +227",
            "NG": "Nigeria +234",
            "NU": "Niue +683",
            "NF": "Norfolk Island +672",
            "NO": "Norway +47",
            "OM": "Oman +968",
            "PK": "Pakistan +92",
            "PW": "Palau +680",
            "PA": "Panama +507",
            "PG": "Papua New Guinea +675",
            "PY": "Paraguay +595",
            "PE": "Peru +51",
            "PH": "Philippines +63",
            "PL": "Poland +48",
            "PT": "Portugal +351",
            "PR": "Puerto Rico +1",
            "QA": "Qatar +974",
            "RE": "Réunion +262",
            "RO": "Romania +40",
            "RU": "Russian Federation +7",
            "RW": "Rwanda +250",
            "WS": "Samoa +685",
            "SM": "San Marino +378",
            "ST": "Sao Tome and Principe +239",
            "SA": "Saudi Arabia +966",
            "SN": "Senegal +221",
            "RS": "Serbia +381",
            "SC": "Seychelles +248",
            "SL": "Sierra Leone +232",
            "SG": "Singapore +65",
            "SX": "Sint Maarten +1",
            "SK": "Slovakia +421",
            "SI": "Slovenia +386",
            "SB": "Solomon Islands +677",
            "SO": "Somalia +252",
            "ZA": "South Africa +27",
            "SS": "South Sudan +211",
            "ES": "Spain +34",
            "LK": "Sri Lanka +94",
            "SD": "Sudan +249",
            "SR": "Suriname +597",
            "SZ": "Swaziland +268",
            "SE": "Sweden +46",
            "CH": "Switzerland +41",
            "SY": "Syria +963",
            "TW": "Taiwan +886",
            "TJ": "Tajikistan +992",
            "TZ": "Tanzania +255",
            "TH": "Thailand +66",
            "TL": "Timor-Leste +670",
            "TG": "Togo +228",
            "TK": "Tokelau +690",
            "TO": "Tonga +676",
            "TT": "Trinidad and Tobago +1",
            "TN": "Tunisia +216",
            "TR": "Turkey +90",
            "TM": "Turkmenistan +993",
            "TC": "Turks and Caicos Islands +1",
            "TV": "Tuvalu +688",
            "UG": "Uganda +256",
            "UA": "Ukraine +380",
            "AE": "United Arab Emirates +971",
            "GB": "United Kingdom +44",
            "US": "United States +1",
            "UY": "Uruguay +598",
            "UZ": "Uzbekistan +998",
            "VU": "Vanuatu +678",
            "VE": "Venezuela +58",
            "VN": "Vietnam +84",
            "WF": "Wallis and Futuna +681",
            "YE": "Yemen +967",
            "ZM": "Zambia +260",
            "ZW": "Zimbabwe +263"
        },
        placeholder: {
            value: "",
            text: "Country Code"
        },
        buttonSize: "btn-sm",
        labelMargin: "10px",
        scrollableHeight: "350px",
        onSelect: function (value, element) {
            countryCodeState = value;
            countryCodeStates['countryCodeState_'+userId] = countryCodeState;

            setTimeout(function(){
                fixFlag(userId);
            }, 1);
        }
    });
    fixFlag(userId);
}

function fixFlag(userId){
    var selectedFlag = document.getElementById('countryCodeState_'+userId).childNodes[1].childNodes[0].childNodes[0];
    var myNode = document.getElementById('countryCodeState_'+userId).childNodes[1].childNodes[0];
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    document.getElementById('countryCodeState_'+userId).childNodes[1].childNodes[0].appendChild(selectedFlag);

}
function showUsersFromDep(depId){
    var usersTableBody = document.getElementById("usersTableBody");
    var matches = 0;
    for (var i = 0, row; row = usersTableBody.rows[i]; i++) {
        if(depId == ""){
            row.style.display = '';
        }else{
            if(row.classList.contains("department_"+depId)){
                row.style.display = '';
                matches++;
            }else{
                row.style.display = 'none';
            }
        }
    }
    if (matches > 0){
        document.getElementById("noUsers").style.display = "none";
    } else{
        if (depId !== ""){
            document.getElementById("noUsers").style.display = "";
        }else{
            document.getElementById("noUsers").style.display = "none";
        }
    }
}

function saveUserAuthorizations(userId) {

    var form = $("#userAuthorizationsForm-"+userId);
    var url = BASE_URL+"/console/actions/user/updateAuthorizations.php";

    var authorizations = form.serialize();

    $.ajax({
        type: "POST",
        url: url,
        data: authorizations+"&userId="+userId,
        success: function(html) {
            strReturn = html;
        },
        async:true
    }).done(function( data ) {
        try{
            data = JSON.parse(data);

            if(data == true){
                toastr.success('Your changes were successfully saved.','Changes Saved');
            }else{
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }
        }catch (e){
            toastr.error('Your changes were not saved. Please try again later.','Oops');
        }

    });;

}
function toggleAdminPrivilages(userId,makeAdmin){

    if(makeAdmin == true){
        var strUrl = BASE_URL+'/console/actions/user/makeUserAdmin.php', strReturn = "";
    }else{
        var strUrl = BASE_URL+'/console/actions/user/unsetUserAdmin.php', strReturn = "";
    }
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            userId: userId
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try{
            data = JSON.parse(data);

            if(data == true){
                if(makeAdmin == true) {
                    swal({title: "Success", text: "User is now an admin",icon: "success",
                        buttons: {
                            ok:"Ok"
                        },
                    }).then(function(isConfirm){
                        goTo("?tab=" + tab + "&userSelected=" + userId);
                    });
                }else{
                    swal({title: "Success", text: "User is now not an admin",icon: "success",
                        buttons: {
                            ok:"Ok"
                        },
                    }).then(function(isConfirm){
                        goTo("?tab=" + tab + "&userSelected=" + userId);
                    });
                }
            }else{
                swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",
                    buttons: {
                        ok:"Ok"
                    },
                }).then(function(isConfirm){
                    goTo("?tab=" + tab + "&userSelected=" + userId);
                });
            }
        }catch (e){
            swal({title:"Oops", text:"Your changes were not saved. Please try again later.",icon: "error",
                buttons: {
                    ok:"Ok"
                },
            }).then(function(isConfirm){
                goTo("?tab=" + tab + "&userSelected=" + userId);
            });
        }

    });

}
function removeOrgLogo(){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover the Logo",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/system/organization/removeLogo.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                success: function (html) {
                    strReturn = html;
                },
                async: true,
            }).done(function (data) {
                location.reload();
            });
        }
    });
}
function goTo(url){
    window.location = url;
}
function toggleGoal(path){
    $("#"+path).prop('disabled', function(i, v) { return !v; });
    $("#"+path).val("");
}
function saveGoals(userId){
    var goals = {
        "bookedJobs":document.getElementById("bookedJobsGoal-"+userId).value,
        "sendEstimates":document.getElementById("estimatesGoals-"+userId).value,
        "targetSalesSum":document.getElementById("targetSumGoals-"+userId).value,
        "isNewGoal":document.getElementById("isNewGoal-"+userId).value,
        "userId":userId,
        "startDate":theStartDate[userId],
        "endDate":theEndDate[userId]
    };
    var strUrl = BASE_URL+'/console/actions/user/setUserGoals.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            data: goals
        },
        async:true
    }).done(function(data){
        try {
            data = JSON.parse(data);
            if (data == true){
                toastr.success('Your changes were successfully saved.','Changes Saved');
                if (goals.isNewGoal == "true"){
                    document.getElementById("isNewGoal-"+userId).value = "false";
                }
            } else{
                toastr.error('Your changes were not saved. Please try again later.','Oops');
            }
        }catch (e) {
            toastr.error('Your changes were not saved. Please try again later.','Oops');
        }
    });
}
var theStartDate = [];
var theEndDate = [];


// ================================ START TOGGLE USERS ROWS ==============================
var selectedUsers = [];
var selectedNames = [];
function toggleEmailUser(id,name){
    if(selectedUsers.includes(id)){
        selectedUsers.remove(id);
        selectedNames.remove(name);
        $("#user-"+id).removeClass("selectedRow");
    }else{
        selectedUsers.push(id);
        selectedNames.push(name);
        $("#user-"+id).addClass("selectedRow");
    }
}

// prototype for removing from array by value
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

// ================================ END TOGGLE USERS ROWS ==============================