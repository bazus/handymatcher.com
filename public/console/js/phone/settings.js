function purchaseNumber() {
    showCustomModal("categories/iframes/phone/addNumber.php");
}

function createSubAccount() {

    var content = document.createElement("div");
    var contentText = document.createTextNode("Activating your text message account, will allow you to purchase phone numbers, for text message interaction with your potential clients from within each lead! ");
    content.appendChild(contentText);
    swal({
        title: false,
        content:content,
        icon: false,
        dangerMode: false,
        buttons: {
            ok: "Activate Account",
            cancel: "Cancel"
        },
    }).then(

        function (isConfirm) {
            if (isConfirm == "ok"){
                $.ajax({
                    url: BASE_URL + "/console/actions/system/twilio/createSubAccount.php",
                    method: "GET",
                    data: {
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true) {
                            toastr.success("Account created","Created");
                            getTwillioAccounts();
                        } else {
                            toastr.error("Failed creating an account", "Error");
                        }
                    } catch (e) {
                        toastr.error("Failed creating an account", "Error");
                    }
                });
            }
        }

    );

}

function getTwillioAccounts(){
    $.ajax({
        url: BASE_URL+"/console/actions/system/twilio/getSubAccountsAndNumbers.php",
        async: true
    }).done(function (data) {

        document.getElementById("createTwilioSubAccount").style.display = "none";
        document.getElementById("purchaseTwilioPhoneNumber").style.display = "none";
        document.getElementById("activateTwilioPhoneNumber").style.display = "none";

        try {
            data = JSON.parse(data);
            if (data != false){
                document.getElementById("purchaseTwilioPhoneNumber").style.display = "";
                document.getElementById("activateTwilioPhoneNumber").style.display = "";

                if (data.isActive == "1"){
                    updateAccountInfo(true);
                    document.getElementById("purchaseTwilioPhoneNumber").classList.remove("disabled");
                }else{
                    document.getElementById("purchaseTwilioPhoneNumber").classList.add("disabled");
                    updateAccountInfo(false);
                }

                document.getElementById("mailSettingsPhoneNumbers").innerHTML = "";
                if(data.phoneNumbers.length == 0){
                    var tr = document.createElement("tr");

                    var td = document.createElement("td");
                    td.setAttribute("colspan","5");
                    td.setAttribute("style","text-align:center;");
                    td.innerHTML = "No phones connected";

                    tr.appendChild(td);

                    document.getElementById("mailSettingsPhoneNumbers").appendChild(tr);
                }else{
                    for (var i = 0; i < data.phoneNumbers.length; i++) {
                        setTwillioPhones(data.phoneNumbers[i]);
                    }
                }
            }else{
                document.getElementById("createTwilioSubAccount").style.display = "";
            }
        } catch (e) {
            toastr.error("Failed loading phone table","Error");
        }
    });
}

function activateAccount(status) {
    var txt = "";
    if(status == true){
        txt = "You will be able to send or receive text messages";
    }else{
        txt = "You will not be able to send or receive text messages";
    }
    swal({
        title: "Are you sure?",
        text: txt,
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(value){
        if( value == true){
            $.ajax({
                url: BASE_URL+"/console/actions/system/twilio/toggleAccountStatus.php",
                method:"POST",
                data:{
                    status:status
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data == "1"){
                        updateAccountInfo(true);
                        document.getElementById("purchaseTwilioPhoneNumber").classList.remove("disabled");
                    }else{
                        updateAccountInfo(false);
                        document.getElementById("purchaseTwilioPhoneNumber").classList.add("disabled");
                    }
                }catch (e) {
                    getTwillioAccounts();
                }
            });
        }});
}
function updateAccountInfo(type) {
    if (type == true){
        document.getElementById("mailSettingsPhoneNumbersTable").classList.remove("disabledTable");
        $("#activateTwilioPhoneNumber").html("<i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> Suspend Account");
        $("#activateTwilioPhoneNumber").attr("onclick","activateAccount(false)");
    }else{
        document.getElementById("mailSettingsPhoneNumbersTable").classList.add("disabledTable");
        $("#activateTwilioPhoneNumber").html("Activate Account");
        $("#activateTwilioPhoneNumber").attr("onclick","activateAccount(true)");
    }
}

function setTwillioPhones(data) {

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    td.innerHTML = "";

    tr.appendChild(td);

    var td = document.createElement("td");
    td.classList.add("text-center");
    td.innerHTML = data.number;

    tr.appendChild(td);

    if (!data.state){data.state = "";}
    var td = document.createElement("td");
    td.classList.add("text-center");
    td.innerHTML = data.state;

    tr.appendChild(td);

    if (!data.postalCode){data.postalCode = "----";}
    var td = document.createElement("td");
    td.classList.add("text-center");
    td.innerHTML = data.postalCode;

    tr.appendChild(td);

    // var td = document.createElement("td");
    // td.innerHTML = data.sms.outgoing;
    //
    // tr.appendChild(td);
    //
    // var td = document.createElement("td");
    // td.innerHTML = data.sms.incoming;
    //
    // tr.appendChild(td);

    var td = document.createElement("td");
    td.classList.add("text-center");

    var tdButton = document.createElement("button");
    tdButton.style.marginBottom = 0;
    tdButton.setAttribute("onclick","deleteNumber("+data.id+")");
    tdButton.classList.add("btn");
    tdButton.classList.add("btn-xs");
    tdButton.classList.add("btn-danger")
    tdButton.setAttribute("style","margin-bottom:0;vertical-align: -webkit-baseline-middle;");
    tdButton.innerHTML = "Delete Phone Number";

    td.appendChild(tdButton);
    tr.appendChild(td);

    document.getElementById("mailSettingsPhoneNumbers").appendChild(tr);
}

function deleteNumber(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this number",
        icon: "warning",
        dangerMode: true,
        buttons: {
            ok: {
                text: "Delete",
                className: "btn-danger"
            },
            cancel: "Cancel"
        },
    }).then(
        function (isConfirm) {
            if (isConfirm == "ok"){
                $.ajax({
                    url: BASE_URL+"/console/actions/system/twilio/deletePhoneNumber.php",
                    method: "GET",
                    data: {
                        phoneNumberId: id
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true){
                            getTwillioAccounts();
                            toastr.success("Number deleted","Deleted");
                        }else{
                            toastr.error("Failed deleting the number","Error");
                        }
                    } catch (e) {
                        toastr.error("Failed deleting the number","Error");
                    }
                });
            }
        }
    );
}


// Initiate
getTwillioAccounts();