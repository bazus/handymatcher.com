var calendar;

var calendarController = {
    calendarsList: [],
    calendar: null,
    operationsColor:null,
    tempCalendarColor:null,
    init: function(){
        var cal = $('#calendar');
        calendarController.calendar = new FullCalendar.Calendar(cal,{
            customButtons: {
                customBack: {
                    text: '<',
                    click: function() {
                        calendarController.calendar.prev();
                        calendarController.getEvents();
                    }
                },
                customNext: {
                    text: '>',
                    click: function() {
                        calendarController.calendar.next();
                        calendarController.getEvents();
                    }
                },
                today: {
                    text: 'Today',
                    click: function() {

                        var todayDate = moment().format('YYYY-MM-DD');
                        calendarController.calendar.gotoDate(todayDate);

                        calendarController.getEvents();
                    }
                },
            },
            header: {
                left: 'customBack,customNext today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            droppable: true, // this allows things to be dropped onto the calendar
            timeFormat: 'HH:mm',
            contentHeight:'900',
            navLinks: true, // can click day/week names to navigate views
            allDaySlot: false,
            themeSystem: 'bootstrap4',
            selectable: true,
            selectHelper: true,
            eventBackgroundColor: "#1AB394",
            eventBorderColor: "#1AB394",
            fixedWeekCount: false,
            aspectRatio: 1.2,
            select: function(start, end) {
                setEventTitle(start,end);
            },
            longPressDelay: 100,
            eventClick: function(calEvent,element, jsEvent, view) {
                calendarController.handleClick(calEvent,element);
            },
            eventResize: function(event){
                calendarController.handleResize(event);
            },
            eventDrop: function(event, delta, revertFunc) {
                calendarController.handleDrop(event);
            },
            eventRender: function(event, element) {
                calendarController.handleRender(event,element);
            },
            drop: function(date ) {
                date = date.format("YYYY-MM-DD HH:mm:ss");
                $(this).remove();
                showCustomModal('categories/iframes/setEvent.php?id='+$(this)[0].id+"&date="+date);
            }
        });
        calendarController.calendar.render();
        calendarController.getEvents();
    },
    handleClick: function(event){
        if (event.type == "event") {
            showCustomModal('categories/iframes/showEvent.php?eventId=' + event.id);
        } else if (event.type == "operation") {
            var eventTitle = event.title;
            var eventTitleV2 = eventTitle.replace(/\s/g,'');
            var operationEventId =  eventTitleV2.replace('#', '');

            //create popover title
            var div = document.createElement("div");
            var span = document.createElement("span");
            span.innerHTML = eventTitle;

            var a = document.createElement("a");
            a.setAttribute("style","float:right; color:white");
            a.setAttribute("href",BASE_URL+'/console/categories/leads/lead.php?leadId='+parseInt(event.id), '_blank');
            a.setAttribute("target","_blank");
            a.innerHTML = 'Open';

            div.appendChild(span);
            div.appendChild(a);

            //hide popover first to avoid duplicate popover windows
            var attr = $("#" + operationEventId).attr('aria-describedby');
            if(typeof attr !== undefined && attr !== false) {
                $('.popover').popover('hide');
            }

                $("#" + operationEventId).popover({
                    title: div,
                    html: true,
                    placement: 'auto',
                        // function (context, source) {
                        //     var position = $(source).position();
                        //
                        //     // if (position.left > 0) {
                        //     //     return "left";
                        //     // }
                        //     // if (position.left < 515) {
                        //     //     return "right";
                        //     // }
                        //     if (position.top < 110) {
                        //         return "bottom";
                        //     }
                        //     return "top";
                        // },
                    container: 'body',
                    content: function () {
                        return '<iframe src="' + BASE_URL + '/console/categories/iframes/showCalendarOperationInfo.php?leadId=' + event.id + '"style="border:none;">' +
                            '</iframe>';
                    },
                    sanitize: false
                }).popover('toggle');

                //background color for popover title
                $(".popover-title").css("background-color", calendarController.operationsColor[0]);
                $(".popover-title").css("color", "white");

        } else if (event.type == "reminder") {
            showCustomModal("categories/iframes/leads/reminder.php?leadId="+event.leadId+"&calendar=1");
        }
    },
    handleResize: function(event){

        if (event.type == "event") {
            var dateS = event.start.format("YYYY-MM-DD HH:mm:ss");
            var dateE = event.end.format("YYYY-MM-DD HH:mm:ss");
            var id = event.id;
            calendarController.setEventDate(id,dateS,dateE);
        }

    },
    handleDrop:function(event){
        if (event.className[0] !== "holidayEvent") {
            try {
                if (event.className[0] != "operationEvent") {
                    var dateS = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var dateE;
                    if (event.end == null) {
                        dateE = event.start.format("YYYY-MM-DD HH:mm:ss");
                    } else {
                        dateE = event.end.format("YYYY-MM-DD HH:mm:ss");
                    }
                    var id = event.id;
                    calendarController.setEventDate(id, dateS, dateE);
                } else if (event.className[0] !== "holidayEvent") {
                    var dateS = event.start.format("YYYY-MM-DD HH:mm:ss");
                    if (event.end == null) {
                        var dateE = event.start.format("YYYY-MM-DD HH:mm:ss");
                    } else {
                        var dateE = event.end.format("YYYY-MM-DD HH:mm:ss");
                    }
                    var id = event.id;
                    calendarController.setOperationsDate(id, dateS,dateE,event.className[1]);
                }

            } catch (e) {
                calendarController.getEvents();
            }
        }

    },
    handleRender:function(event,element){
        if(event.type == "operation") {
            element.find(".fc-title").remove();
            element.find(".fc-time").remove();

            var eventTitle = event.title;
            var eventTitleV2 = eventTitle.replace(/\s/g,'');
            var operationEventId =  eventTitleV2.replace('#', '');
            $(element).attr("id",operationEventId);
            $(element).attr("data-toggle","popover");
            $(element).attr("data-content","");

            if(event.fromState && event.toState){
                var new_description =
                    event.title+' '+'<div class="calendar-move-info">'+event.fullName+'</div>'+' '+'<div class="calendar-move-info">'+' ['+event.fromState+' - '+event.toState+']'+'</div>';

            }else{
                var new_description =
                     event.title+' '+'<div class="calendar-move-info">'+event.fullName+'</div>';

            }

            element.append(new_description);
        }
    },
    getEvents: function (calendarId,type){
        if(type == undefined){
            calendarController.getCalendarEvents(calendarId);
            calendarController.getGoogleCalendarEvents(calendarId);
        }else{
            if(type == "regular"){
                calendarController.getCalendarEvents(calendarId);
            }
            if(type == "google"){
                calendarController.getGoogleCalendarEvents(calendarId);
            }
        }
    },
    clearSources:function(calendarId){
        // ============= (START) REMOVE SOURCES BEFORE RE-FETCH THEM AGAIN =============
        if(calendarId != undefined){
            if(document.getElementById(calendarId).checked == false){
                // Find the selected calendar
                calendarController.removeSource(calendarId);
                return false;
            }
        }else{
            for(var k in calendarController.calendarsList){
                if(calendarController.calendarsList[k].type == "regular"){
                    calendarController.removeSource(k);
                }
            }
        }
        // ============= (START) REMOVE SOURCES BEFORE RE-FETCH THEM AGAIN =============
        return true;
    },
    clearGoogleSources:function(calendarId){
        // ============= (START) REMOVE SOURCES BEFORE RE-FETCH THEM AGAIN =============
        if(calendarId != undefined){

            if(document.getElementById(calendarId).checked == false){
                // Find the selected calendar
                calendarController.removeSource(calendarId);
            }
        }else{
            var googlecalendarsInputs = document.getElementsByName("googlecalendar[]");
            for (i = 0; i < googlecalendarsInputs.length; i++) {
                calendarController.removeSource(googlecalendarsInputs[i].id);
            }
        }
        // ============= (END) REMOVE SOURCES BEFORE RE-FETCH THEM AGAIN =============
    },
    getCalendarEvents: function (calendarId) {

        // First - clear the sources before fetching them again
        var clearSources = calendarController.clearSources(calendarId);
        if(clearSources == false){return;}

        var showPersonal = false;
        var showOrganization = false;
        var showOperations = false;
        var showHolidays = false;

        if(calendarId == undefined) {
            showPersonal = document.getElementById("personalCheckbox").checked;
            showOrganization = document.getElementById("organizationCheckbox").checked;
            showOperations = document.getElementById("operationsCheckbox").checked;
            showHolidays = document.getElementById("holidays").checked;
        }else{
            if(calendarId == "personalCheckbox"){showPersonal = true;}
            if(calendarId == "organizationCheckbox"){showOrganization = true;}
            if(calendarId == "operationsCheckbox"){showOperations = true;}
            if(calendarId == "holidays"){showHolidays = true;}
        }

        if(showPersonal == true){document.getElementById("personalCheckbox").disabled = true;}
        if(showOrganization == true){document.getElementById("organizationCheckbox").disabled = true;}
        if(showOperations == true){document.getElementById("operationsCheckbox").disabled = true;}
        if(showHolidays == true){document.getElementById("holidays").disabled = true;}

        var strUrl = BASE_URL+'/console/actions/system/calendar/getEvents.php', strReturn = "";

        var dateRange = calendarController.getCalendarDateRange();
        var fromDate = new Date(dateRange.start);
        var toDate = new Date(dateRange.end);

        var fromDateText = fromDate.yyyymmdd();
        var toDateText = toDate.yyyymmdd();

        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data:{
                showPersonal:showPersonal,
                showOrganization:showOrganization,
                showOperations:showOperations,
                showHolidays:showHolidays,
                fromDate:fromDateText,
                toDate:toDateText,
                calendarId:calendarId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true,
        }).done(function(data){

            data = JSON.parse(data);

            for(var i = 0;i<data.length;i++){
                var singleCalendar = data[i];
                document.getElementById(singleCalendar.id).disabled = false;
                calendarController.addSource(singleCalendar);
            }
        });
    },
    getGoogleCalendarEvents: function (calendarId){
        // First - clear the sources before fetching them again
        calendarController.clearGoogleSources(calendarId);

        var googleCalendarsIds = [];
        if(calendarId == undefined){
            var googlecalendarsInputs = document.getElementsByName("googlecalendar[]");
            for (i = 0; i < googlecalendarsInputs.length; i++) {
                if(googlecalendarsInputs[i].checked) {
                    googleCalendarsIds.push(googlecalendarsInputs[i].value);
                }
            }

        }else {
            if (document.getElementById(calendarId).checked == true) {
                googleCalendarsIds.push(calendarId);
            }
        }


        for(var i = 0;i<googleCalendarsIds.length;i++){
            document.getElementById(googleCalendarsIds[i]).disabled = true;
        }

        var strUrl = BASE_URL+'/console/actions/system/calendar/getGoogleCalendarEvents.php', strReturn = "";

        var dateRange = calendarController.getCalendarDateRange();
        var fromDate = new Date(dateRange.start);
        var toDate = new Date(dateRange.end);

        var fromDateText = fromDate.yyyymmdd();
        var toDateText = toDate.yyyymmdd();


        jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    fromDate: fromDateText,
                    toDate: toDateText,
                    googlecalendars: googleCalendarsIds
                },
                success: function (html) {
                    strReturn = html;

                },
                async: true,
            }).done(function (data) {
                var data = JSON.parse(data);

                for (var i = 0; i < data.length; i++) {
                    var singleCalendar = data[i];

                    document.getElementById(singleCalendar.id).disabled = false;
                    calendarController.addSource(singleCalendar);
                }
            });
    },
    addSource: function (data) {

        calendarController.calendar.removeEventSource(data.id);
        calendarController.calendar.addEventSource(data);

        calendarController.calendarsList[data.id] = data;
        calendarController.setCalendarColor(data.id,data.color);
    },
    removeSource: function (calendarId) {
        if(calendarController.calendarsList[calendarId] == undefined){return;}
        calendarController.calendar.removeEventSource(calendarController.calendarsList[calendarId].id);
        delete calendarController.calendarsList[calendarId];
    },
    getCalendarDateRange:function () {
        var view = calendarController.calendar.view;
        var start = view.start._d;
        var end = view.end._d;
        var dates = { start: start, end: end };
        return dates;
    },
    createNewEvent: function () {
        showCustomModal('categories/iframes/addEvent.php?eventType='+document.getElementById("calendarType").value);
    },
    setEventDate: function (eventId,startDate,endDate) {
        var strUrl = BASE_URL + '/console/actions/system/calendar/updateEventDate.php', strReturn = "";

        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: {
                eventId: eventId,
                dateS: startDate,
                dateE: endDate
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        });
    },
    removeGoogleCalendarSync:function () {
        swal({
            title: "Are you sure?",
            // text: "Once deleted, you will not be able to recover this file",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then(function(isConfirm){
            if (isConfirm) {
                var strUrl = BASE_URL+'/console/actions/system/calendar/removeGoogleSync.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        isDeleted: 1
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function(data){
                    location.reload();
                });
            }
        });
    },
    pickCalendarColor:function (calendarId) {
        if(calendarController.calendarsList[calendarId]) {
            var calendarColor = calendarController.calendarsList[calendarId].color;
        }
        showCustomModal("categories/iframes/setCalendarColor.php?calendarId="+calendarId);
    },
    getCalendarColor:function(calendarId){
        var strUrl = BASE_URL + '/console/actions/system/calendar/getCalendarColor.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data: {
                calendarId:calendarId,
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data != false){
                   calendarController.setCalendarColor(calendarId,data);
                }
            } catch (e) {
                toastr.error("Something went wrong");
            }
        });
    },
    setCalendarColor:function (calendarId,color) {

        if(calendarId == "operationsCheckbox"){
            calendarController.operationsColor = color;
        }

        if(calendarId == undefined || color == undefined){return;}
        //if the checkbox is uncheck color them with tempColor
        else if(!calendarController.calendarsList[calendarId]){
            calendarController.tempCalendarColor = color;
            document.getElementById(calendarId + "_label").style.setProperty('--bac', calendarController.tempCalendarColor);
            document.getElementById(calendarId + "_label").style.setProperty('--boc', calendarController.tempCalendarColor);
            document.getElementById(calendarId + "_label").style.setProperty('--c', "#fff");

        }else{

            calendarController.calendarsList[calendarId].color = color;

            document.getElementById(calendarId + "_label").style.setProperty('--bac', calendarController.calendarsList[calendarId].color);
            document.getElementById(calendarId + "_label").style.setProperty('--boc', calendarController.calendarsList[calendarId].color);
            document.getElementById(calendarId + "_label").style.setProperty('--c', "#fff");

            //color calendar events
            for (var b = 0; b < calendarController.calendarsList[calendarId].events.length; b++) {
                if(calendarController.calendarsList[calendarId].events[b].color == null) {
                    calendarController.calendarsList[calendarId].events[b].backgroundColor = color;
                    calendarController.calendarsList[calendarId].events[b].borderColor = color;
                }
            }

            var newEventSrc = calendarController.calendarsList[calendarId];
            calendarController.calendar.removeEventSource(calendarId);
            calendarController.calendar.addEventSource(newEventSrc);
        }


    }
};

Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear()+"-"+
        (mm>9 ? '' : '0') + mm+"-"+
        (dd>9 ? '' : '0') + dd
    ].join('');
};

//exit popover on body click
$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('destroy');
        }
    });
});

