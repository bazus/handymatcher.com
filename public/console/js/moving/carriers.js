
function getCarriers(){
    loadingState();
    var strUrl = BASE_URL+'/console/actions/moving/carriers/getCarriers.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        document.getElementById("carriersTable").innerHTML = "";

        data = JSON.parse(data);
        if(data.length > 0){
            setCarriers(data);
        }else{
            document.getElementById("carriersTable").innerHTML = "<tr id='emptyCarrier'> <td style=\"background-color: #e7eaec36;text-align: center;\">No carriers yet</td><td style=\"background-color: #e7eaec36;text-align: center;\"></td></td> </tr>";
        }
        stopLoadingState();

    });
}

function setCarriers(data){

    var carriersTable = document.getElementById("carriersTable");

    for(var i=0;i<data.length;i++){

        var carrier = data[i];

        var tr = document.createElement("tr");
        //tr.setAttribute("onclick", "getTruck("+x.id+")");

        var tdName = document.createElement("td");
        tdName.className = "text-center";
        tdName.innerHTML = carrier.name;

        tr.appendChild(tdName);

        var tdActions = document.createElement("td");
        tdActions.className = "text-center";


        if(carrier.isActive == "1"){
            var a = document.createElement("a");
            a.className = "btn btn-warning btn-sm";
            a.setAttribute("style","margin-right: 5px;");
            a.innerHTML = "Disable";
            a.setAttribute("onClick","toggleCarrierActivation('"+carrier.id+"',false)");

            tdActions.appendChild(a);
        }else{
            var a = document.createElement("a");
            a.className = "btn btn-warning btn-sm";
            a.setAttribute("style","margin-right: 5px;");
            a.innerHTML = "Re-activate";
            a.setAttribute("onClick","toggleCarrierActivation('"+carrier.id+"',true)");

            tdActions.appendChild(a);
        }

        var a = document.createElement("a");
        a.className = "btn btn-info btn-sm";
        a.setAttribute("style","margin-right: 5px;");
        a.setAttribute("onclick","updateCarrier('"+carrier.id+"')");

        a.innerHTML = "Edit";

        tdActions.appendChild(a);

        tr.appendChild(tdActions);


        carriersTable.appendChild(tr);
    }
}

function toggleCarrierActivation(id,isActive){

    var strUrl = BASE_URL+'/console/actions/moving/carriers/toggleCarrierActivation.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            isActive: isActive,
            id: id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        try{
            data = JSON.parse(data);

            if(data == true){
                if(isActive == true){
                    toastr.success("Carrier is now active", "activated");
                }else{
                    toastr.success("Carrier is now un-active", "un-activated");
                }

            }else{
                toastr.error("Data not saved", "error");
            }
        }catch (e) {
            toastr.error("Data not saved", "error");
        }

        getCarriers();
    });


}
function showJobAcceptanceInfo(e){
    e.preventDefault();
    swal({
        text: 'This is the text and terms for your Job acceptance form.',
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}