
var updateIds = [];

function getMaterials(){
    loadingState();
    var strUrl = BASE_URL+'/console/actions/moving/materials/getMaterials.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        document.getElementById("materialsNamesTableBody").innerHTML = "";

        try{
            data = JSON.parse(data);
            if(data.length > 0){
                for(var i=0;i<data.length;i++) {
                    setMaterialRow(data[i]);
                }
                setChange();
            }else{
                document.getElementById("materialsNamesTableBody").innerHTML = "<tr id='emptyMat'> <td colspan=\"5\" style=\"background-color: #e7eaec36;text-align: center;\">No Materials Yet</td> </tr>";
            }
        }catch(e){
            swal("Oops", "Please try again later", "error");
        }
        stopLoadingState();
    });
}

function setMaterialRow(material){

    var container = document.getElementById("materialsNamesTableBody");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var input = document.createElement("input");
    input.setAttribute("placeholder","Title");
    input.setAttribute("type","text");
    input.setAttribute("onkeyup","updateId("+material.id+")");
    input.classList.add("form-control");
    input.id = "title-"+material.id;
    input.value = decodeText(material.title);

    td.appendChild(input);
    tr.appendChild(td);

    var td = document.createElement("td");
    var input = document.createElement("input");
    input.setAttribute("type","text");
    input.setAttribute("onkeyup","updateId("+material.id+")");
    input.setAttribute("min",0.00);
    input.classList.add("form-control");
    input.classList.add("touchspin"+material.id);
    input.id = "cost-"+material.id;
    input.value = material.cost;
    var x = document.createElement("span");
    x.classList.add("input-group-addon");
    x.classList.add("pull-left");
    x.setAttribute("style","line-height:21px;height:34px;width:34px;font-size: 1.2em;");
    var xspan = document.createTextNode("$");

    x.appendChild(xspan);
    td.appendChild(x);
    td.appendChild(input);
    tr.appendChild(td);

    var td = document.createElement("td");
    var input = document.createElement("input");
    input.setAttribute("type","text");
    input.setAttribute("onkeyup","updateId("+material.id+")");
    input.setAttribute("min",0.00);
    input.classList.add("form-control");
    input.classList.add("touchspin"+material.id);
    input.id = "local-"+material.id;
    input.value = material.localPrice;
    var x = document.createElement("span");
    x.classList.add("input-group-addon");
    x.classList.add("pull-left");
    x.setAttribute("style","line-height:21px;height:34px;width:34px;font-size: 1.2em;");
    var xspan = document.createTextNode("$");

    x.appendChild(xspan);
    td.appendChild(x);
    td.appendChild(input);
    tr.appendChild(td);

    var td = document.createElement("td");
    var input = document.createElement("input");
    input.setAttribute("type","text");
    input.setAttribute("onkeyup","updateId("+material.id+")");
    input.setAttribute("min",0.00);
    input.classList.add("form-control");
    input.classList.add("touchspin"+material.id);
    input.id = "long-"+material.id;
    input.value = material.longPrice;
    var x = document.createElement("span");
    x.classList.add("input-group-addon");
    x.classList.add("pull-left");
    x.setAttribute("style","line-height:21px;height:34px;width:34px;font-size: 1.2em;");
    var xspan = document.createTextNode("$");

    x.appendChild(xspan);
    td.appendChild(x);
    td.appendChild(input);
    tr.appendChild(td);

    var td = document.createElement("td");
    var button = document.createElement("button");
    button.setAttribute("onclick","deleteMaterial("+material.id+")");
    button.classList.add("btn");
    button.classList.add("btn-danger");
    button.classList.add("btn-box");
    var span = document.createElement("span");
    span.classList.add("fa");
    span.classList.add("fa-remove");
    button.appendChild(span);
    td.appendChild(button);
    tr.appendChild(td);

    container.appendChild(tr);
    setTocuhSpin(material.id);

}

function setTocuhSpin(id){
    var touchspin = ".touchspin"+id;
    $(touchspin).TouchSpin({
        buttondown_class: 'btn btn-white btn-change',
        buttonup_class: 'btn btn-white btn-change',
        min: 0,
        max: 10000,
        decimals: 2,
        step: 0.01
    });

}

function deleteMaterial(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Material",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/moving/materials/deleteMaterial.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    id: id
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try {
                    data = JSON.parse(data);
                    getMaterials();
                }catch(e){
                    swal("Oops", "Please try again later", "error");
                }

            });
        }
    });
}
function decodeText(text){
    try {
        var elem = document.createElement('textarea');
        elem.innerHTML = text;
        var decoded = elem.value;
        return decoded;
    }catch (e) {return text;}
}
function updateMaterials(){

    var isValid = true;

    if(updateIds.length > 0){
        for(var i=0;i<updateIds.length;i++){

            var id = updateIds[i];
            var title = document.getElementById("title-"+id).value;
            var cost = document.getElementById("cost-"+id).value;
            var local = document.getElementById("local-"+id).value;
            var long = document.getElementById("long-"+id).value;


            var strUrl = BASE_URL+'/console/actions/moving/materials/updateMaterial.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method:"POST",
                data:{
                    id:id,
                    title:title,
                    cost:cost,
                    local:local,
                    long:long,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: false
            }).done(function(data){

                try {
                    data = JSON.parse(data);
                    if(data != true){
                        isValid = false;
                    }
                }catch (e) {
                    swal("Oops", "Please try again later", "error");
                    isValid = false;
                }

            });
        }
        if (isValid == true){
            toastr.success("Materials has been updated","Updated");
        }
        updateIds = [];
        getMaterials();
        document.getElementById("saveBtn").disabled = true;
        document.getElementById("saveBtnBottom").disabled = true;
    }

}

function updateId(id){
    if (! updateIds.includes(id) ){
        updateIds.push(id);
    }
    document.getElementById("saveBtn").disabled = false;
    document.getElementById("saveBtnBottom").disabled = false;
}

function addMaterial(){
    if(document.getElementById("emptyMat") !== null){
        document.getElementById("emptyMat").remove();
    }
    var strUrl = BASE_URL+'/console/actions/moving/materials/addMaterial.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function(requestData){
        try {
            var data = JSON.parse(requestData);
            setMaterialRow(data);
            document.getElementById("title-"+data.id).focus();
            setChange();
        }
        catch (e) {
            swal("Oops", "Please try again later", "error");
            Sentry.withScope(function(scope){
                Sentry.setExtra("data", requestData);
                Sentry.setExtra("comments", "addMaterial()");
                Sentry.captureException(e);
            });
        }
    });
}

function setChange() {
    $(".btn-change").on("click", function () {
        var id = getSecondPart($(this).parent().parent().children()[2].id);
        updateId(id);
    });
}

function getSecondPart(str) {
    return str.split('-')[1];
}