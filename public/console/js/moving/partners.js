function getData(){
    if (NLid){
        var strUrl = BASE_URL+'/api/partners/getCompanyData.php';
        jQuery.ajax({
            url: strUrl,
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data.status == true) {
                    setOrgData(data);
                    getCapmaignData();
                }else{
                    toastr.error('organization data invalid');
                }

            }catch (e) {
                toastr.error('organization data invalid');
            }
        });
    }else{
        toastr.error('organization data invalid');
    }
}
function setOrgData(data){
    // Org Name
    $("#orgName").text(data.data.name);

    // Active
    if (data.data.isHold == 0){
        $("#isActive").addClass("label-primary");
        $("#isActive").text('Active');
    }else{
        if (data.data.isDeleted == 0){
            $("#isActive").addClass("label-warning");
            $("#isActive").text('Inactive');
        }else{
            $("#isActive").addClass("label-danger");
            $("#isActive").text('Deleted');
        }
    }

    // Contact Name
    $("#contactName").text(data.data.owner);

    // Personal Rep
    $("#personalRep").text("Adir Pellach");

    // Join Date
    $("#joinDate").text(data.data.dateAdded.formatDate());

    // Type
    if (data.data.clientLeadsTypeId == 1) {
        $("#orgType").text("Moving");
    }else if(data.data.clientLeadsTypeId == 2){
        $("#orgType").text("Car Shipping");
    }

    // Files
    if (data.data.files.length > 0){
        $("#mainTab").removeClass("col-lg-12");
        $("#mainTab").addClass("col-lg-9");
        $("#filesTab").css('display','');
    } else{

    }
}
function getCapmaignData(){
    $("#noCampaigns").text("Loading...");
    var strUrl = BASE_URL+'/api/partners/getCampaigns.php';
    jQuery.ajax({
        url: strUrl,
        async: true
    }).done(function (data) {
        try {
            var container = document.getElementById("campaignContent");
            container.innerHTML = "";
            data = JSON.parse(data);
            if (data.status == true){
                if (data.data.length > 0){
                    $("#noCampaigns").remove();
                    for (var i = 0; i < data.data.length; i++) {
                        setCapmaign(data.data[i]);
                    }
                    getCapmaignRequests();
                }else{
                    $("#noCampaigns").text("No Campaigns");
                }
            }else{
                $("#noCampaigns").text("No Campaigns");
                toastr.error('organization campaign data invalid');
            }

        }catch (e) {
            $("#noCampaigns").text("No Campaigns");
            toastr.error('organization campaign data invalid');
        }
    });
}
function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
function setCapmaign(data){
    var container = document.getElementById("campaignContent");
    var tr = document.createElement("tr");
    tr.id = "campaign-"+data.id;

    // Name
    var td = document.createElement("td");
    var tdText = document.createTextNode(data.name);
    td.appendChild(tdText);
    tr.appendChild(td);

    // Budget
    var td = document.createElement("td");
    if (data.currentName){
        if (data.budget){
            var tdText = document.createTextNode(data.currentName+" ($"+formatMoney(data.budget)+")");
        }
    }else{
        td.classList.add("text-danger");
        var tdText = document.createTextNode("No Budget");
    }
    td.appendChild(tdText);
    tr.appendChild(td);


    // Filters
    var td = document.createElement("td");
    if (data.filters && data.filters.length > 0){
        var tdText = document.createTextNode(data.filters.join());
        td.appendChild(tdText);
        tr.appendChild(td);
    } else{
        var tdText = document.createTextNode("None");
        td.style.color = "#1ab394";
        td.appendChild(tdText);
        tr.appendChild(td);
    }

    // Post Methods
    if (data.postMethods && data.postMethods.length > 0){
        var td = document.createElement("td");
        var tdText = document.createTextNode(data.postMethods.join());
        td.appendChild(tdText);
        tr.appendChild(td);
    } else {
        var td = document.createElement("td");
        td.classList.add("text-danger");
        var tdText = document.createTextNode("None");
        td.appendChild(tdText);
        tr.appendChild(td);
    }

    // Last Lead
    var td = document.createElement("td");
    if (data.lastLead){
        var tdText = document.createTextNode(data.lastLead+" (EST)");
    }else{
        var tdText = document.createTextNode("---");
    }
    td.appendChild(tdText);
    tr.appendChild(td);

    // Leads Today
    var td = document.createElement("td");
    if (data.leadsToday){
        var tdText = document.createTextNode(data.leadsToday);
    }else{
        var tdText = document.createTextNode(0);
    }
    td.appendChild(tdText);
    tr.appendChild(td);

    // Status
    var td = document.createElement("td");
    td.classList.add("text-center");
    var span = document.createElement("span");
    span.id = "status-"+data.id;
    span.style.cursor = "pointer";
    span.setAttribute('onclick',"activeCampaign("+data.id+")");
    span.classList.add("label");
    if (data.isActive == 1){
        span.classList.add("label-primary");
        var spanText = document.createTextNode("Active");
    }else{
        tr.style.background = "#f3f3f3";
        span.classList.add("label-warning");
        var spanText = document.createTextNode("Paused");
    }
    span.appendChild(spanText);
    td.appendChild(span);
    tr.appendChild(td);



    container.appendChild(tr);

}
function getCapmaignRequests(){
    $("#noRequests").text("Loading...");
    var strUrl = BASE_URL+'/api/partners/getCompanyRequests.php';
    jQuery.ajax({
        url: strUrl,
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true){
                if (data.data.length > 0){
                    var container = document.getElementById("requests");
                    container.innerHTML = "";
                    $("#noRequests").remove();
                    for (var i = 0; i < data.data.length; i++) {
                        setRequest(data.data[i]);
                    }
                }else{
                    $("#noRequests").text("No Requests");
                }
            }else{
                $("#noRequests").text("No Requests");
            }

        }catch (e) {
            $("#noRequests").text("No Requests");
        }
    });
}
function setRequest(data){
    var container = document.getElementById("requests");

    var tr = document.createElement("tr");
    var status = document.createElement("td");
    var statusLabel = document.createElement("span");
    statusLabel.classList.add('label');
    if (data.isApproved == 0){
        statusLabel.classList.add('label-warning');
        var statusText = document.createTextNode("Not Approved");
    } else if (data.isApproved == 1) {
        statusLabel.classList.add('label-primary');
        var statusText = document.createTextNode("Approved");
    }else if (data.isApproved == null){
        statusLabel.classList.add('label-success');
        var statusText = document.createTextNode("Pending");
    }
    statusLabel.appendChild(statusText);
    status.appendChild(statusLabel);
    tr.appendChild(status);

    var leadId = document.createElement("td");
    var a = document.createElement("a");
    a.setAttribute("href",BASE_URL+"/console/categories/leads/lead.php?leadId="+data.leadId);
    a.setAttribute("target","_blank");
    var leadIdText = document.createTextNode(data.leadId);
    a.appendChild(leadIdText);
    leadId.appendChild(a);
    tr.appendChild(leadId);

    var openDate = document.createElement("td");
    var openDateText = document.createTextNode(data.requestDate+" (EST)");

    openDate.appendChild(openDateText);
    tr.appendChild(openDate);

    var reasons = document.createElement("td");
    reasons.classList.add("reasons");
    reasons.setAttribute("title",data.reason);
    var reasonsText = document.createTextNode(data.reason);

    reasons.appendChild(reasonsText);
    tr.appendChild(reasons);

    var response = document.createElement("td");
    if (data.isApproved != 1) {
        if (data.response && data.response != "" && data.response != "null") {
            var responseButton = document.createElement("button");
            responseButton.classList.add("btn");
            responseButton.classList.add("btn-xs");
            responseButton.classList.add("btn-default");
            responseButton.setAttribute("onclick", "mySwal('" + data.response + "')");
            var buttonText = document.createTextNode("Show More");
            responseButton.appendChild(buttonText);
            response.appendChild(responseButton);
            tr.appendChild(response);
        } else {
            var responseText = document.createTextNode("No Response Yet");
            response.appendChild(responseText);
            tr.appendChild(response);
        }
    }else{
        var span = document.createElement("span");
        span.classList.add("text-navy");
        var responseText = document.createTextNode("Approved");
        span.appendChild(responseText);
        response.appendChild(span);
        tr.appendChild(response);
    }

    container.appendChild(tr);
}
function mySwal(resp){
    swal({title:false, text:resp,icon: false,
        buttons: {mybtn:{text:"Got It"}}
    });
}
function activeCampaign(id){
    swal({
        title: "Are you sure?",
        text: "Toggle this campaign status",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {

            var strUrl = BASE_URL+'/api/partners/toggleCampaign.php';
            jQuery.ajax({
                url: strUrl,
                method:"get",
                data:{
                    CID: id
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data.status == true){
                        if (data.data.isActive == true){
                            $("#status-"+id).text("Active");
                            $("#status-"+id).removeClass('label-warning');
                            $("#status-"+id).addClass('label-primary');
                            $("#campaign-"+id).css("background","white");
                        }else{
                            $("#status-"+id).text("Paused");
                            $("#status-"+id).addClass('label-warning');
                            $("#status-"+id).removeClass('label-primary');
                            $("#campaign-"+id).css("background","#f3f3f3");
                        }

                    }else{
                        return false;
                    }
                }catch (e) {
                    return false;
                }
            });
        } else {

        }
    });
}
String.prototype.formatDate = function(theDate){
    return moment(theDate).format("MMMM Do YYYY");
};

// Initialize
getData();