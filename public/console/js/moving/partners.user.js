function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
function getCapmaignRequests(){
    $("#noRequests").text("Loading...");
    var strUrl = BASE_URL+'/api/partners/getCompanyRequests.php';
    jQuery.ajax({
        url: strUrl,
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.status == true){
                if (data.data.length > 0){
                    var container = document.getElementById("requests");
                    container.innerHTML = "";
                    $("#noRequests").remove();
                    for (var i = 0; i < data.data.length; i++) {
                        setRequest(data.data[i]);
                    }
                }else{
                    $("#noRequests").text("No Requests");
                }
            }else{
                $("#noRequests").text("No Requests");
            }

        }catch (e) {
            $("#noRequests").text("No Requests");
        }
    });
}
function setRequest(data){
    var container = document.getElementById("requests");

    var tr = document.createElement("tr");
    var status = document.createElement("td");
    var statusLabel = document.createElement("span");
    statusLabel.classList.add('label');
    if (data.isApproved == 0){
        statusLabel.classList.add('label-warning');
        var statusText = document.createTextNode("Not Approved");
    } else if (data.isApproved == 1) {
        statusLabel.classList.add('label-primary');
        var statusText = document.createTextNode("Approved");
    }else if (data.isApproved == null){
        statusLabel.classList.add('label-success');
        var statusText = document.createTextNode("Pending");
    }
    statusLabel.appendChild(statusText);
    status.appendChild(statusLabel);
    tr.appendChild(status);

    var leadId = document.createElement("td");
    var a = document.createElement("a");
    a.setAttribute("href",BASE_URL+"/console/categories/leads/lead.php?leadId="+data.leadId);
    a.setAttribute("target","_blank");
    var leadIdText = document.createTextNode(data.leadId);
    a.appendChild(leadIdText);
    leadId.appendChild(a);
    tr.appendChild(leadId);

    var openDate = document.createElement("td");
    var openDateText = document.createTextNode(data.requestDate);

    openDate.appendChild(openDateText);
    tr.appendChild(openDate);

    var reasons = document.createElement("td");
    reasons.classList.add("reasons");
    reasons.setAttribute("title",data.reason);
    var reasonsText = document.createTextNode(data.reason);

    reasons.appendChild(reasonsText);
    tr.appendChild(reasons);

    var response = document.createElement("td");
    if (data.isApproved != 1) {
        if (data.response && data.response != "" && data.response != "null") {
            var responseButton = document.createElement("button");
            responseButton.classList.add("btn");
            responseButton.classList.add("btn-xs");
            responseButton.classList.add("btn-default");
            responseButton.setAttribute("onclick", "mySwal('" + data.response + "')");
            var buttonText = document.createTextNode("Show More");
            responseButton.appendChild(buttonText);
            response.appendChild(responseButton);
            tr.appendChild(response);
        } else {
            var responseText = document.createTextNode("No Response Yet");
            response.appendChild(responseText);
            tr.appendChild(response);
        }
    }else{
        var span = document.createElement("span");
        span.classList.add("text-navy");
        var responseText = document.createTextNode("Approved");
        span.appendChild(responseText);
        response.appendChild(span);
        tr.appendChild(response);
    }

    container.appendChild(tr);
}
function mySwal(resp){
    swal({title:false, text:resp,icon: false,
        buttons: {mybtn:{text:"Got It"}}
    });
}
function activeCampaign(id){
    swal({
        title: "Are you sure?",
        text: "Toggle this campaign status",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {

            var strUrl = BASE_URL+'/api/partners/toggleCampaign.php';
            jQuery.ajax({
                url: strUrl,
                method:"get",
                data:{
                    CID: id
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data.status == true){
                        if (data.data.isActive == true){
                            $("#status-"+id).text("Active");
                            $("#status-"+id).removeClass('label-warning');
                            $("#status-"+id).addClass('label-primary');
                            $("#campaign-"+id).css("background","white");
                        }else{
                            $("#status-"+id).text("Paused");
                            $("#status-"+id).addClass('label-warning');
                            $("#status-"+id).removeClass('label-primary');
                            $("#campaign-"+id).css("background","#f3f3f3");
                        }

                    }else{
                        return false;
                    }
                }catch (e) {
                    return false;
                }
            });
        } else {

        }
    });
}
String.prototype.formatDate = function(theDate){
    return moment(theDate).format("MMMM Do YYYY");
};

// Initialize
getCapmaignRequests();