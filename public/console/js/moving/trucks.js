
function getTrucks(){
    loadingState();
    var strUrl = BASE_URL+'/console/actions/moving/trucks/getTrucks.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        document.getElementById("trucksNames").innerHTML = "";

        data = JSON.parse(data);
        if(data.length > 0){
            document.getElementById("truckDetail").style.display = "";
            setTrucks(data);
        }else{
            document.getElementById("truckDetail").style.display = "none";
            document.getElementById("trucksNames").innerHTML = "<tr id='emptyTruck'> <td colspan=\"5\" style=\"background-color: #e7eaec36;text-align: center;\">No Trucks Yet</td> </tr>";
        }
        stopLoadingState();
    });
}

function setTrucks(data){
    if(document.getElementById("emptyTruck") !== null){
        document.getElementById("emptyTruck").remove();
    }
    var container = document.getElementById("trucksNames");
    for(var i=0;i<data.length;i++){

        var x = data[i];
        var tr = document.createElement("tr");
        tr.classList.add("text-center");
        tr.setAttribute("onclick", "getTruck("+x.id+")");

        var tdName = document.createElement("td");
        tdName.id = "truck-name"+x.id;
        var nodeName = document.createTextNode(x.title);

        tdName.appendChild(nodeName);
        tr.appendChild(tdName);


        // create a button
        var activeTd = document.createElement("td");
        var activeDiv =  document.createElement("div");
        activeDiv.classList.add("onoffswitch");
        activeDiv.style.margin = "6px auto";

        var activeCheckbox = document.createElement("input");
        activeCheckbox.setAttribute("type","checkbox");
        activeCheckbox.classList.add("onoffswitch-checkbox");
        activeCheckbox.id = "activeTruckBtn"+x.id;
        if(x.isActive == 1){
            activeCheckbox.setAttribute("checked","true");
        }

        var activeLabel = document.createElement("label");
        activeLabel.classList.add("onoffswitch-label");
        activeLabel.setAttribute("onclick","toggleTruckActivation("+x.isActive+","+x.id+",'"+x.title+"',event)");
        activeLabel.id = "activeTruckLabel"+x.id;

        var spanIn = document.createElement("span");
        spanIn.classList.add("onoffswitch-inner");
        spanIn.classList.add("text-left");
        activeLabel.appendChild(spanIn);

        var spanSwitch = document.createElement("span");
        spanSwitch.classList.add("onoffswitch-switch");
        activeLabel.appendChild(spanSwitch);

        activeDiv.appendChild(activeCheckbox);
        activeDiv.appendChild(activeLabel);
        activeTd.appendChild(activeDiv);
        tr.appendChild(activeTd);

        container.appendChild(tr);

    }
    if(data.length == 1){
        getTruck(data[0].id);
    }else{
        var check = document.getElementById("trucksLeftDetails").innerHTML;
        if(check.trim() == ""){
            document.getElementById("truckDetail").style.display = "none";
        }
    }
}

function deleteTruck(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Truck!",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/moving/trucks/deleteTruck.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    truckId: id
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);

                getTrucks();
                // clear right menu
                document.getElementById("trucksLeftDetails").innerHTML = "";
                document.getElementById("trucksRightDetails").innerHTML = "";
                document.getElementById("titleSpan").innerHTML = "";
                document.getElementById("truckRightButtons").innerHTML = "";
            });
        }
    });
}

function getTruck(truckId){

    var buttonContainer = document.getElementById("truckRightButtons");
    buttonContainer.innerHTML = "";
    var leftContainer = document.getElementById("trucksLeftDetails");
    leftContainer.innerHTML = "";

    var rightContainer = document.getElementById("trucksRightDetails");
    rightContainer.innerHTML = "";

    document.getElementById("truckDetail").style.display = "";

    var strUrl = BASE_URL+'/console/actions/moving/trucks/getTruck.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "post",
        success: function (html) {
            strReturn = html;
        },
        data:{
          truckId:truckId
        },
        async: true
    }).done(function (data) {
        data = JSON.parse(data);
        setTruck(data,leftContainer,rightContainer,buttonContainer);
    });

}

function setTruck(x,leftContainer,rightContainer,buttonContainer){ // x = data

    document.getElementById("titleSpan").innerHTML = " - "+x.title;
    // ==================== Left Container ====================

    // ==================== Title ====================
    var titleLi = document.createElement("li");
    titleLi.classList.add("list-group-item");
    titleLi.classList.add("fist-item");

    var titleLabel = document.createElement("label");

    var titleLabelNode = document.createTextNode("Title");
    titleLabel.appendChild(titleLabelNode);
    titleLi.appendChild(titleLabel);

    var titleInput = document.createElement("input");
    titleInput.setAttribute("type","text");
    titleInput.classList.add("form-control");
    titleInput.id = "truck-title-"+x.id;
    titleInput.value = x.title;

    titleLi.appendChild(titleInput);
    // ==================== End Title ====================
    leftContainer.appendChild(titleLi);

    // ==================== Description ====================
    var descriptionLi = document.createElement("li");
    descriptionLi.classList.add("list-group-item");
    descriptionLi.classList.add("fist-item");

    var descriptionLabel = document.createElement("label");

    var descriptionLabelNode = document.createTextNode("Description");
    descriptionLabel.appendChild(descriptionLabelNode);
    descriptionLi.appendChild(descriptionLabel);

    var descriptionInput = document.createElement("input");
    descriptionInput.setAttribute("type","text");
    descriptionInput.classList.add("form-control");
    descriptionInput.id = "truck-description-"+x.id;
    descriptionInput.value = x.description;

    descriptionLi.appendChild(descriptionInput);
    // ==================== End Description ====================
    leftContainer.appendChild(descriptionLi);

    // ==================== Model ====================
    var modelLi = document.createElement("li");
    modelLi.classList.add("list-group-item");
    modelLi.classList.add("fist-item");

    var modelLabel = document.createElement("label");

    var modelLabelNode = document.createTextNode("Model");
    modelLabel.appendChild(modelLabelNode);
    modelLi.appendChild(modelLabel);

    var modelInput = document.createElement("input");
    modelInput.setAttribute("type","text");
    modelInput.classList.add("form-control");
    modelInput.id = "truck-model-"+x.id;
    modelInput.value = x.model;

    modelLi.appendChild(modelInput);
    // ==================== End Model ====================
    leftContainer.appendChild(modelLi);

    // ==================== mfYear ====================
    var mfYearLi = document.createElement("li");
    mfYearLi.classList.add("list-group-item");
    mfYearLi.classList.add("fist-item");

    var mfYearLabel = document.createElement("label");

    var mfYearLabelNode = document.createTextNode("Mfc. Year");
    mfYearLabel.appendChild(mfYearLabelNode);
    mfYearLi.appendChild(mfYearLabel);

    var mfYearInput = document.createElement("input");
    mfYearInput.setAttribute("type","text");
    mfYearInput.classList.add("form-control");
    mfYearInput.id = "truck-mfYear-"+x.id;
    mfYearInput.value = x.mfYear;

    mfYearLi.appendChild(mfYearInput);
    // ==================== End mfYear ====================
    leftContainer.appendChild(mfYearLi);

    // ==================== vin ====================
    var vinLi = document.createElement("li");
    vinLi.classList.add("list-group-item");
    vinLi.classList.add("fist-item");

    var vinLabel = document.createElement("label");

    var vinLabelNode = document.createTextNode("VIN");
    vinLabel.appendChild(vinLabelNode);
    vinLi.appendChild(vinLabel);

    var vinInput = document.createElement("input");
    vinInput.setAttribute("type","text");
    vinInput.classList.add("form-control");
    vinInput.id = "truck-vin-"+x.id;
    vinInput.value = x.vin;

    vinLi.appendChild(vinInput);
    // ==================== End vin ====================
    leftContainer.appendChild(vinLi);

    // ==================== PlateNumber ====================
    var plateNumberLi = document.createElement("li");
    plateNumberLi.classList.add("list-group-item");
    plateNumberLi.classList.add("fist-item");

    var plateNumberLabel = document.createElement("label");

    var plateNumberNode = document.createTextNode("Plate Number");
    plateNumberLabel.appendChild(plateNumberNode);
    plateNumberLi.appendChild(plateNumberLabel);

    var plateNumberInput = document.createElement("input");
    plateNumberInput.setAttribute("type","text");
    plateNumberInput.classList.add("form-control");
    plateNumberInput.id = "truck-plateNumber-"+x.id;
    plateNumberInput.value = x.plateNumber;

    plateNumberLi.appendChild(plateNumberInput);
    // ==================== End PlateNumber ====================
    leftContainer.appendChild(plateNumberLi);

    // ==================== Tire Size ====================
    var tireSizeLi = document.createElement("li");
    tireSizeLi.classList.add("list-group-item");
    tireSizeLi.classList.add("fist-item");

    var tireSizeLabel = document.createElement("label");

    var tireSizeNode = document.createTextNode("Tire Size");
    tireSizeLabel.appendChild(tireSizeNode);
    tireSizeLi.appendChild(tireSizeLabel);

    var tireSizeInput = document.createElement("input");
    tireSizeInput.setAttribute("type","text");
    tireSizeInput.classList.add("form-control");
    tireSizeInput.id = "truck-tireSize-"+x.id;
    tireSizeInput.value = x.tireSize;

    tireSizeLi.appendChild(tireSizeInput);
    // ==================== End Tire Size ====================
    leftContainer.appendChild(tireSizeLi);
    // ==================== End Left Container ====================

    // ==================== Right Container ====================

    // ==================== State ====================
    var stateLi = document.createElement("li");
    stateLi.classList.add("list-group-item");
    stateLi.classList.add("fist-item");

    var stateLabel = document.createElement("label");

    var stateNode = document.createTextNode("State");
    stateLabel.appendChild(stateNode);
    stateLi.appendChild(stateLabel);

    var stateInput = document.createElement("input");
    stateInput.setAttribute("type","text");
    stateInput.classList.add("form-control");
    stateInput.id = "truck-state-"+x.id;
    stateInput.value = x.state;

    stateLi.appendChild(stateInput);
    // ==================== End State ====================
    rightContainer.appendChild(stateLi);

    // ==================== Capactiy ====================
    var capacityLi = document.createElement("li");
    capacityLi.classList.add("list-group-item");
    capacityLi.classList.add("fist-item");

    var capacityLabel = document.createElement("label");

    var capacityNode = document.createTextNode("Capacity (Cubic Feet)");
    capacityLabel.appendChild(capacityNode);
    capacityLi.appendChild(capacityLabel);

    var capacityInput = document.createElement("input");
    capacityInput.setAttribute("type","number");
    capacityInput.setAttribute("min",0);
    capacityInput.classList.add("form-control");
    capacityInput.id = "truck-capacity-"+x.id;
    capacityInput.value = x.capacity;

    capacityLi.appendChild(capacityInput);
    // ==================== End Capacity ====================
    rightContainer.appendChild(capacityLi);

    // ==================== PurchaseDate ====================

    var purchaseDateLi = document.createElement("li");
    purchaseDateLi.classList.add("list-group-item");
    purchaseDateLi.classList.add("fist-item");

    var purchaseDateLabel = document.createElement("label");

    var purchaseDateNode = document.createTextNode("Purchase Date");
    purchaseDateLabel.appendChild(purchaseDateNode);
    purchaseDateLi.appendChild(purchaseDateLabel);
    //old date
    // var purchaseDateInput = document.createElement("input");
    // purchaseDateInput.setAttribute("type","date");
    // purchaseDateInput.classList.add("form-control");
    // purchaseDateInput.id = "truck-purchaseDate-"+x.id;
    // purchaseDateInput.value = x.purchaseDate;
    var purchaseDateDiv = document.createElement("div");
    purchaseDateDiv.classList.add("input-group");
    purchaseDateDiv.classList.add("date");
    purchaseDateDiv.id = "date_picker";

    //calender icon
    //
    // var purchaseDateSpan = document.createElement("span");
    // purchaseDateSpan.classList.add("input-group-addon");
    //
    // var purchaseDateCalander = document.createElement("li");
    // purchaseDateCalander.classList.add("fa");
    // purchaseDateCalander.classList.add("fa-calendar");


    var purchaseDateInput = document.createElement("input");
    purchaseDateInput.setAttribute("type","text");
    purchaseDateInput.classList.add("form-control");
    purchaseDateInput.id = "truck-purchaseDate-"+x.id;
    purchaseDateInput.value =reverseDate(x.purchaseDate);


    // purchaseDateDiv.appendChild(purchaseDateSpan);
    // purchaseDateSpan.appendChild(purchaseDateCalander);
    purchaseDateDiv.appendChild(purchaseDateInput);

    purchaseDateLi.appendChild(purchaseDateDiv);
    // ==================== End purchaseDate ====================
    rightContainer.appendChild(purchaseDateLi);


    // ==================== purchasePrice ====================
    var purchasePriceLi = document.createElement("li");
    purchasePriceLi.classList.add("list-group-item");
    purchasePriceLi.classList.add("fist-item");

    var purchasePriceLabel = document.createElement("label");

    var purchasePriceNode = document.createTextNode("Purchase Price");
    purchasePriceLabel.appendChild(purchasePriceNode);
    purchasePriceLi.appendChild(purchasePriceLabel);

    var purchasePriceInput = document.createElement("input");
    purchasePriceInput.setAttribute("type","text");
    purchasePriceInput.setAttribute("min",0);
    purchasePriceInput.classList.add("form-control");
    purchasePriceInput.id = "truck-purchasePrice-"+x.id;
    purchasePriceInput.value = x.purchasePrice;

    purchasePriceLi.appendChild(purchasePriceInput);
    // ==================== End purchasePrice ====================
    rightContainer.appendChild(purchasePriceLi);

    // ==================== purchaseMiles ====================
    var purchaseMilesLi = document.createElement("li");
    purchaseMilesLi.classList.add("list-group-item");
    purchaseMilesLi.classList.add("fist-item");

    var purchaseMilesLabel = document.createElement("label");

    var purchaseMilesNode = document.createTextNode("Purchase Miles");
    purchaseMilesLabel.appendChild(purchaseMilesNode);
    purchaseMilesLi.appendChild(purchaseMilesLabel);

    var purchaseMilesInput = document.createElement("input");
    purchaseMilesInput.setAttribute("type","text");
    purchaseMilesInput.setAttribute("min",0);
    purchaseMilesInput.classList.add("form-control");
    purchaseMilesInput.id = "truck-purchaseMiles-"+x.id;
    purchaseMilesInput.value = x.purchaseMiles;

    purchaseMilesLi.appendChild(purchaseMilesInput);
    // ==================== End purchaseMiles ====================
    rightContainer.appendChild(purchaseMilesLi);

    // check if truck is sold
    if(x.isSold == 1){


        // ==================== SoldDate ====================

        var soldDateLi = document.createElement("li");
        soldDateLi.classList.add("list-group-item");
        soldDateLi.classList.add("fist-item");
        soldDateLi.classList.add("has-warning");

        var soldDateLabel = document.createElement("label");
        soldDateLabel.classList.add("text-warning");

        var soldDateNode = document.createTextNode("sold Date");
        soldDateLabel.appendChild(soldDateNode);
        soldDateLi.appendChild(soldDateLabel);

        var soldDateDiv = document.createElement("div");
        soldDateDiv.classList.add("input-group");
        soldDateDiv.classList.add("date");
        soldDateDiv.id = "date_picker";

        var soldDateInput = document.createElement("input");
        soldDateInput.setAttribute("type","text");
        soldDateInput.classList.add("form-control");
        soldDateInput.id = "truck-soldDate-"+x.id;
        soldDateInput.value = reverseDate(x.soldDate);

        soldDateDiv.appendChild(soldDateInput);
        soldDateLi.appendChild(soldDateDiv);
        // ==================== End soldDate ====================
        rightContainer.appendChild(soldDateLi);

        // ==================== soldPrice ====================
        var soldPriceLi = document.createElement("li");
        soldPriceLi.classList.add("list-group-item");
        soldPriceLi.classList.add("fist-item");
        soldPriceLi.classList.add("has-warning");

        var soldPriceLabel = document.createElement("label");
        soldPriceLabel.classList.add("text-warning");

        var soldPriceNode = document.createTextNode("sold Price");
        soldPriceLabel.appendChild(soldPriceNode);
        soldPriceLi.appendChild(soldPriceLabel);

        var soldPriceInput = document.createElement("input");
        soldPriceInput.setAttribute("type","text");
        soldPriceInput.classList.add("form-control");
        soldPriceInput.id = "truck-soldPrice-"+x.id;
        soldPriceInput.value = x.soldPrice;

        soldPriceLi.appendChild(soldPriceInput);
        // ==================== End soldPrice ====================
        rightContainer.appendChild(soldPriceLi);

        // ==================== soldMiles ====================
        var soldMilesLi = document.createElement("li");
        soldMilesLi.classList.add("list-group-item");
        soldMilesLi.classList.add("fist-item");
        soldMilesLi.classList.add("has-warning");

        var soldMilesLabel = document.createElement("label");
        soldMilesLabel.classList.add("text-warning");

        var soldMilesNode = document.createTextNode("sold Miles");
        soldMilesLabel.appendChild(soldMilesNode);
        soldMilesLi.appendChild(soldMilesLabel);

        var soldMilesInput = document.createElement("input");
        soldMilesInput.setAttribute("type","text");
        soldMilesInput.classList.add("form-control");
        soldMilesInput.id = "truck-soldMiles-"+x.id;
        soldMilesInput.value = x.soldMiles;

        soldMilesLi.appendChild(soldMilesInput);
        // ==================== End soldMiles ====================
        rightContainer.appendChild(soldMilesLi);

    }
    // ==================== Comment ====================
    var commentLi = document.createElement("li");
    commentLi.classList.add("list-group-item");
    commentLi.classList.add("fist-item");
    commentLi.style.paddingBottom = "15px";

    var commentLabel = document.createElement("label");

    var commentLabelNode = document.createTextNode("Comments");
    commentLabel.appendChild(commentLabelNode);
    commentLi.appendChild(commentLabel);

    var commentTextArea = document.createElement("textarea");
    commentTextArea.style.height = "103px";
    commentTextArea.setAttribute("placeholder","Comments for this truck");
    commentTextArea.classList.add("form-control");
    commentTextArea.id = "truck-comment-"+x.id;

    var commentTextNode = document.createTextNode(x.comment);

    commentTextArea.appendChild(commentTextNode);
    commentLi.appendChild(commentTextArea);
    // ==================== End Comment ====================
    rightContainer.appendChild(commentLi);

    var divRow = document.createElement("div");
    divRow.classList.add("row");

    var divColDel = document.createElement("div");
    divColDel.classList.add("col-md-4");
    divColDel.style.padding = "2px";
    // ==================== Delete Button ====================
    var deleteBtn = document.createElement("button");
    var deleteNode = document.createTextNode("Remove Truck");
    deleteBtn.setAttribute("onclick","deleteTruck("+x.id+")");
    deleteBtn.classList.add("btn");
    deleteBtn.classList.add("btn-block");
    deleteBtn.classList.add("btn-danger");
    deleteBtn.classList.add("btn-sm");
    deleteBtn.appendChild(deleteNode);
    divColDel.appendChild(deleteBtn);
    divRow.appendChild(divColDel);
    // ==================== End Delete Button ====================

    // ==================== Sold Button ====================
    var divColSold = document.createElement("div");
    divColSold.classList.add("col-md-4");
    divColSold.style.padding = "2px";
    var SoldBTN = document.createElement("button");
    SoldBTN.setAttribute("onclick","sold("+x.isSold+","+x.id+",'"+x.title+"')");
    SoldBTN.classList.add("btn");
    SoldBTN.classList.add("btn-block");
    SoldBTN.classList.add("btn-sm");
    if(x.isSold == 1){
        var SoldNode = document.createTextNode("Mark As Unsold");
    }else{
        var SoldNode = document.createTextNode("Mark As Sold");
    }
    SoldBTN.classList.add("btn-warning");
    SoldBTN.appendChild(SoldNode);
    divColSold.appendChild(SoldBTN);
    divRow.appendChild(divColSold);
    // ==================== Sold Button ====================

    // ==================== Save Button ====================
    var divColSave = document.createElement("div");
    divColSave.classList.add("col-md-4");
    divColSave.style.padding = "2px";
    var updateBTN = document.createElement("button");
    var updateNode = document.createTextNode("Save Changes");
    updateBTN.setAttribute("onclick","updateTruck("+x.id+")");
    updateBTN.classList.add("btn");
    updateBTN.classList.add("btn-block");
    updateBTN.classList.add("btn-sm");
    updateBTN.classList.add("btn-primary");
    updateBTN.appendChild(updateNode);
    divColSave.appendChild(updateBTN);
    divRow.appendChild(divColSave);
    // ==================== End Save Button ====================
    buttonContainer.appendChild(divRow);

    $(function() {
        $(purchaseDateInput).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10)
            }
        );
    });

    $(function() {
        $(soldDateInput).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'),10)
            }
        );
    });


    function reverseDate(date) {
        if (date) {
            var dateArray = date.split("-");
            var newDateArray = dateArray[1] + "/" + dateArray[2] + "/" + dateArray[0];

            return newDateArray;
        }else{
            return "";
        }
    }

    // ==================== End Right Container ====================
    setIChekcs();
}

function toggleTruckActivation(isActive,id,title,e){

    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    isActive = (isActive==0?1:0);

    var strUrl = BASE_URL+'/console/actions/moving/trucks/activeTruck.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            isActive: isActive,
            id: id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        data = JSON.parse(data);

        if(data == true){
            var btn = document.getElementById("activeTruckBtn"+id);
            var label = document.getElementById("activeTruckLabel"+id);
            if(isActive == 1){
                //swal("Success!", "Truck '"+title+"' Has Been Activated", "success");
                btn.checked = true;
                label.setAttribute("onclick","toggleTruckActivation(1,"+id+",'"+title+"',event)");
            }else{
                //swal("Success!", "Truck '"+title+"' Has Been Deactivated", "success");
                btn.checked = false;
                label.setAttribute("onclick","toggleTruckActivation(0,"+id+",'"+title+"',event)");
            }
        }

    });


}

function sold(isSold,id,title){

    isSold = (isSold==0?1:0);

    var strUrl = BASE_URL+'/console/actions/moving/trucks/soldTruck.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            isSold: isSold,
            id: id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        data = JSON.parse(data);
        var btn = document.getElementById("activeTruckBtn"+id);
        var label = document.getElementById("activeTruckLabel"+id);
        if(data == true){
            if(isSold == 1){
                swal("Success!", "Truck '"+title+"' has been mark as Sold", "success");
                btn.checked = false;
                label.setAttribute("onclick","toggleTruckActivation(0,"+id+",'"+title+"',event)");
            }else{
                swal("Success!", "Truck '"+title+"' has been mark as Unsold", "success");
                label.setAttribute("onclick","toggleTruckActivation(1,"+id+",'"+title+"',event)");
            }
        }

        getTruck(id);

    });

}

function updateTruck(id){

    var title = document.getElementById("truck-title-"+id).value;
    var description = document.getElementById("truck-description-"+id).value;
    var model = document.getElementById("truck-model-"+id).value;
    var year = document.getElementById("truck-mfYear-"+id).value;
    var vin = document.getElementById("truck-vin-"+id).value;
    var plate = document.getElementById("truck-plateNumber-"+id).value;
    var tire = document.getElementById("truck-tireSize-"+id).value;
    var state = document.getElementById("truck-state-"+id).value;
    var comment = document.getElementById("truck-comment-"+id).value;
    var capacity = document.getElementById("truck-capacity-"+id).value;
    var purchaseDate = document.getElementById("truck-purchaseDate-"+id).value;
    var purchasePrice = document.getElementById("truck-purchasePrice-"+id).value;
    var purchaseMiles = document.getElementById("truck-purchaseMiles-"+id).value;
    var soldDate = (document.getElementById("truck-soldDate-"+id)) ? document.getElementById("truck-soldDate-"+id).value : null;
    var soldPrice = (document.getElementById("truck-soldPrice-"+id)) ? document.getElementById("truck-soldPrice-"+id).value : null;
    var soldMiles = (document.getElementById("truck-soldMiles-"+id)) ? document.getElementById("truck-soldMiles-"+id).value : null;

    var strUrl = BASE_URL+'/console/actions/moving/trucks/updateTruck.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method:"post",
        data:{
            id:id,
            title:title,
            description:description,
            model:model,
            year:year,
            vin:vin,
            plate:plate,
            tire:tire,
            state:state,
            comment:comment,
            capacity:capacity,
            purchaseDate:purchaseDate,
            purchasePrice:purchasePrice,
            purchaseMiles:purchaseMiles,
            soldDate:soldDate,
            soldPrice:soldPrice,
            soldMiles:soldMiles
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        data = JSON.parse(data);
        if(data == true){
            toastr.success("Success!", "Truck ''"+title+"'' has been Updated", "success");
            document.getElementById("trucksLeftDetails").innerHTML = "";
            document.getElementById("trucksRightDetails").innerHTML = "";
            getTruck(id);
            $("#titleSpan").text(" - "+title);
            $("#truck-name"+id).text(title);
        }
    });


}

function setIChekcs(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    $("[class*='icheckbox'],[class*='iradio']").each(function(){
        $(this).find("ins").attr("onclick", $(this).find("input").attr("onclick"));
    });
}