function formatCheckBox(el){ if(el.checked === true){ return 1; }else{ return 0; } }

$(".settingsForm").bind('keyup change', function (){
    setSave();
});

function setSave(){
    $("#price-send").attr("disabled",false);
    document.getElementById("price-send").classList.remove("btn-default");
    document.getElementById("price-send").classList.add("btn-success");
}

function saveJobAcceptanceTerms(){

    var strUrl = BASE_URL+'/console/actions/moving/movingSettings/saveJobAcceptanceTerms.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            terms:$("#summernote2").summernote("code")
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try{
            data = JSON.parse(data);
            if(data === true){
                toastr.success("Job Acceptance Terms & conditions saved");
            }else{
                swal("Oops", "Please try again later", "error");
            }
        }catch(e){
            swal("Oops", "Please try again later", "error");
        }
    });
}

function saveData(){

    var jobBoardFeature = document.getElementById("jobBoardFeature").checked;
    if(jobBoardFeature){jobBoardFeature = 1;}else{jobBoardFeature = 0;};

    var dataArray = {
        "estimateEmailId": document.getElementById("estimateEmailId").value,
        "estimateEmailAddressId": document.getElementById("estimateEmailAddressId").value,
        "sendEstimateEmail": formatCheckBox(document.getElementById("sendEstimateEmail")),
        "calculateBy": document.getElementById("calculateBy").value,
        "cflbsratio": document.getElementById("cflbsratio").value,
        "blockDuplicatesLeadsBasedPhone": formatCheckBox(document.getElementById("blockDuplicatesLeadsBasedPhone")),
        "blockDuplicatesLeadsBasedEmail": formatCheckBox(document.getElementById("blockDuplicatesLeadsBasedEmail")),
        "seniorDiscount": document.getElementById("seniorDiscount").value,
        "couponDiscount": document.getElementById("couponDiscount").value,
        "veteranDiscount": document.getElementById("veteranDiscount").value,
        "calculateFuel": document.getElementById("calculateFuel").value,
        "estimateTerms": $("#summernote").summernote("code"),
        "FVPTerms": $("#summernoteFVP").summernote("code"),
        "estimateTermsCreditCard": $("#summernote3").summernote("code"),
        "jobBoardFeature":jobBoardFeature,
    };

    var strUrl = BASE_URL+'/console/actions/moving/movingSettings/update.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            data:dataArray
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try{
            data = JSON.parse(data);
            if(data === true){
                toastr.success("Move Management Settings Updated","Updated");
                $("#price-send").attr("disabled",true);
                document.getElementById("price-send").classList.add("btn-default");
                document.getElementById("price-send").classList.remove("btn-success");
            }else{
                swal("Oops", "Please try again later", "error");
            }
        }catch(e){
            swal("Oops", "Please try again later", "error");
        }
    });
}

function setICheck(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    $('input').on('ifClicked', function(){
        setSave();
    });
}
document.onload = setICheck();

function toggleSendEstimate(obj){
    if(obj.checked == true){
        document.getElementById('estimateEmailId').disabled = false;
        document.getElementById('estimateEmailAddressId').disabled = false;
    }else{
        document.getElementById('estimateEmailId').disabled = true;
        document.getElementById('estimateEmailAddressId').disabled = true;
    }
}

function showEstimateEmailInfo(e){
    e.preventDefault();
    swal({
        text: 'By selecting this option, an automatic email will be sent with custom tags according to the email template selected with every new lead that arrives.',
        buttons: {
            templates:{
                text:"Go To Email templates",
                className: "btn-link"
            },
            skip:{
                text:"Got It"
            }

        }
    }).then(function(value){
        if (value == "templates"){
            location.href = BASE_URL+"/console/categories/mail/templates.php";
        }
    });

}

function showEstimateInitialPriceInfo(e){
    e.preventDefault();
    swal({
        text: 'TEXT',
        buttons: {
            skip:{
                text:"Got It"
            }

        }
    }).then(function(value){


    });

}

function showEstimateTermsInfo(e){
    e.preventDefault();
    swal({
        text: 'This information should summarize what your estimate details - what is included, what is not and what is subject to changes due to conditions.',
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}

function showFVPInfo(e){
    e.preventDefault();
    swal({
        text: "This is the text that will feature explaining the terms of the full value protection, on your company's moving estimate.",
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}


function showEstimateTermsCreditCardInfo(e){
    e.preventDefault();
    swal({
        text: "This section can be used to edit the terms of your credit card authorization form.",
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}


$(function() {

    $('#summernote').summernote({
        height: "316px",
        toolbar:[
            ['style', ['fontname','fontsize','color','forecolor','backcolor','bold','italic','strikethrough', 'clear']],
            ['font', ['superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph','height']],
            ['groupName',['customLeadTags']],
            ['link', ['linkDialogShow', 'unlink']],
            ['misc',['undo','redo','codeview']]]
        ,
        callbacks: {
            onChange: function(contents, $editable) {
                setSave();
            }
        }
    });
    $('#summernoteFVP').summernote({
        height: "316px",
        toolbar:[
            ['style', ['fontname','fontsize','color','forecolor','backcolor','bold','italic','strikethrough', 'clear']],
            ['font', ['superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph','height']],
            ['groupName',['customLeadTags']],
            ['link', ['linkDialogShow', 'unlink']],
            ['misc',['undo','redo','codeview']]]
        ,
        callbacks: {
            onChange: function(contents, $editable) {
                setSave();
            }
        }
    });
    $('#summernote2').summernote({
        height: "316px",
        toolbar:[
            ['style', ['fontname','fontsize','color','forecolor','backcolor','bold','italic','strikethrough', 'clear']],
            ['font', ['superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph','height']],
            ['groupName',['customLeadTags']],
            ['link', ['linkDialogShow', 'unlink']],
            ['misc',['undo','redo','codeview']]]
        ,
        callbacks: {
            onChange: function(contents, $editable) {

            }
        }
    });

    var companyNameTag = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: 'companyName',
            click: function () {
                $("#summernote3").summernote('editor.saveRange');

                $("#summernote3").summernote('editor.focus');
                $("#summernote3").summernote('editor.insertText', "{companyName}");

            }
        });
        return button.render();
    };

    $('#summernote3').summernote({
        height: "316px",
        toolbar:[
            ['style', ['fontname','fontsize','color','forecolor','backcolor','bold','italic','strikethrough', 'clear']],
            ['font', ['superscript', 'subscript']],
            ['para', ['ul', 'ol', 'paragraph','height']],
            ['groupName',['companyNameTag']],
            ['link', ['linkDialogShow', 'unlink']],
            ['misc',['undo','redo','codeview']]
        ],
        buttons: {
            companyNameTag: companyNameTag
        }
        ,
        callbacks: {
            onChange: function(contents, $editable) {
                setSave();
            }
        }
    });





});