
function getCrew(){
    loadingState();
    var strUrl = BASE_URL+'/console/actions/moving/crew/getCrew.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        document.getElementById("crewTable").innerHTML = "";

        data = JSON.parse(data);
        if(data.length > 0){
            setCrew(data);
        }else{
            document.getElementById("crewTable").innerHTML = "<tr id='emptyCrew'> <td colspan='3' style=\"background-color: #e7eaec36;text-align: center;\">No crew members yet</td> </tr>";
        }
        stopLoadingState();

    });
}

function setCrew(data){

    var crewTable = document.getElementById("crewTable");
    crewTable.innerHTML = "";
    var type = "";
    for(var i=0;i<data.length;i++) {

        var crew = data[i];

        var tr = document.createElement("tr");

        var tdName = document.createElement("td");
        tdName.className = "text-center";
        tdName.innerHTML = crew.name;

        switch (crew.type) {
            case "1":
                type = "Laborer";
                break;
            case "2":
                type = "Foreman";
                break;
            case "3":
                type = "Driver";
                break;
            default:
                type = "Not defined";
        }
        var tdType = document.createElement("td");
        tdType.className = "text-center";
        tdType.innerHTML = type;

        var tdActions = document.createElement("td");
        tdActions.className = "text-center";

        var actionsBtnDiv  = document.createElement("div");
        actionsBtnDiv.setAttribute("style","display:flex;justify-content:space-evenly;");

        var editCrew = document.createElement("a");
        editCrew.className = "btn btn-info btn-sm";
        // editCrew.setAttribute("onClick","addNewCrewMember('categories/iframes/moving/addCrew.php?crewId="+crew.id+"')");
        editCrew.setAttribute("onClick","addNewCrewMember('"+crew.id+"')");
        editCrew.innerHTML = "Edit";

        var deleteCrew = document.createElement("a");
        deleteCrew.className = "btn btn-danger btn-sm";
        deleteCrew.setAttribute("onClick","deleteCrew('"+crew.id+"')");
        deleteCrew.innerHTML = "Delete";

        actionsBtnDiv.appendChild(editCrew);
        actionsBtnDiv.appendChild(deleteCrew);

        tdActions.appendChild(actionsBtnDiv);


        tr.appendChild(tdName);
        tr.appendChild(tdType);
        tr.appendChild(tdActions);

        crewTable.appendChild(tr);
    }
}
function deleteCrew(crewId) {

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this crew member",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {

            var strUrl = BASE_URL + '/console/actions/moving/crew/deleteCrew.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "post",
                data: {
                    crewId: crewId,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data == true) {
                        $("#myCustomModal").modal('hide');
                        getCrew();
                        toastr.success("Crew member Deleted", "Deleted");
                    } else {
                        toastr.error("Can not delete crew member", "Failed");
                    }
                } catch (e) {
                    toastr.error("Can not crew member", "Failed");
                }
            });
        }else{

        }
    });

}