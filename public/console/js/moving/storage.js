function getStorages(){
    loadingState();
    var strUrl = BASE_URL+'/console/actions/moving/storage/getStorages.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        document.getElementById("storagesNames").innerHTML = "";

        data = JSON.parse(data);
        if (data.length > 0) {
            document.getElementById('storageDetails').style.display = "";
            setStorages(data);
        }else{
            document.getElementById('storageDetails').style.display = "none";
            document.getElementById("storagesNames").innerHTML = "<tr id='emptystorage'> <td colspan=\"5\" style=\"background-color: #e7eaec36;text-align: center;\">No Storage Yet</td> </tr>";
        }
        stopLoadingState();
    });
}

function setStorages(data){

    if(document.getElementById("emptystorage") !== null){
        document.getElementById("emptystorage").remove();
    }
    var container = document.getElementById("storagesNames");
    for(var i=0;i<data.length;i++){

        var storage = data[i];
        var tr = document.createElement("tr");
        tr.classList.add("text-center");
        tr.setAttribute("onclick", "getStorage("+storage.id+");");

        var tdName = document.createElement("td");

        var nodeName = document.createTextNode(storage.title);

        tdName.appendChild(nodeName);
        tr.appendChild(tdName);
        // create a button
        var activeTd = document.createElement("td");
        var activeDiv =  document.createElement("div");
        activeDiv.classList.add("onoffswitch");
        activeDiv.style.margin = "6px auto";

        var activeCheckbox = document.createElement("input");
        activeCheckbox.setAttribute("type","checkbox");
        activeCheckbox.classList.add("onoffswitch-checkbox");
        activeCheckbox.id = "activeBtn"+storage.id;
        if(storage.isActive == 1){
            activeCheckbox.setAttribute("checked","true");
        }

        var activeLabel = document.createElement("label");
        activeLabel.classList.add("onoffswitch-label");
        activeLabel.id = "activeLabel"+storage.id;
        activeLabel.setAttribute("onclick","toggleActivation("+storage.isActive+","+storage.id+",event);");

        var spanIn = document.createElement("span");
        spanIn.classList.add("onoffswitch-inner");
        spanIn.classList.add("text-left");
        activeLabel.appendChild(spanIn);

        var spanSwitch = document.createElement("span");
        spanSwitch.classList.add("onoffswitch-switch");
        activeLabel.appendChild(spanSwitch);

        activeDiv.appendChild(activeCheckbox);
        activeDiv.appendChild(activeLabel);
        activeTd.appendChild(activeDiv);
        tr.appendChild(activeTd);

        container.appendChild(tr);

    }
    if(data.length == 1){
        getStorage(data[0].id);
    }else{
        var check = document.getElementById("StorageLeftDetails").innerHTML;
        if(check.trim() == ""){
            document.getElementById("storageDetails").style.display = "none";
        }
    }
}

function deleteStorage(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Storage!",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/moving/storage/deleteStorage.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    storageId: id
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                data = JSON.parse(data);

                getStorages();

                document.getElementById("titleSpanStorage").innerHTML = "";
                document.getElementById("StorageLeftDetails").innerHTML = "";
                document.getElementById("StorageRightDetails").innerHTML = "";
                document.getElementById("storageBottomLeftDetails").innerHTML = "";
                document.getElementById("storageBottomRightDetails").innerHTML = "";
                document.getElementById("storageRightButtons").innerHTML = "";

            });
        }
    });
}

function getStorage(storageId){

    var buttonContainer = document.getElementById("storageRightButtons");
    buttonContainer.innerHTML = "";

    var leftContainer = document.getElementById("StorageLeftDetails");
    leftContainer.innerHTML = "";

    var rightContainer = document.getElementById("StorageRightDetails");
    rightContainer.innerHTML = "";
    document.getElementById('storageDetails').style.display = "";
    var strUrl = BASE_URL+'/console/actions/moving/storage/getStorage.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "post",
        success: function (html) {
            strReturn = html;
        },
        data:{
            storageId:storageId
        },
        async: false
    }).done(function (data) {
        data = JSON.parse(data);
        setStorage(data,leftContainer,rightContainer,buttonContainer);
    });

}

function setStorage(storage,leftContainer,rightContainer,buttonContainer){ // x = data

    var leftBottomContainer = document.getElementById("storageBottomLeftDetails");
    var rightBottomContainer = document.getElementById("storageBottomRightDetails");
    leftBottomContainer.innerHTML = "";
    rightBottomContainer.innerHTML = "";

    document.getElementById("titleSpanStorage").innerHTML = " - "+storage.title;
    // ==================== Left Container ====================
    var h3 = document.createElement("h3");
    h3.style.color = "#28B294";
    var textNode = document.createTextNode("Storage Information");
    h3.appendChild(textNode);
    leftContainer.appendChild(h3);
    // ==================== Title ====================
    var titleLi = document.createElement("li");
    titleLi.classList.add("list-group-item");
    titleLi.classList.add("fist-item");

    var titleLabel = document.createElement("label");

    var titleLabelNode = document.createTextNode("Facility Name");
    titleLabel.appendChild(titleLabelNode);
    titleLi.appendChild(titleLabel);

    var titleInput = document.createElement("input");
    titleInput.setAttribute("type","text");
    titleInput.classList.add("form-control");
    titleInput.id = "storage-title-"+storage.id;
    titleInput.value = storage.title;

    titleLi.appendChild(titleInput);
    leftContainer.appendChild(titleLi);
    // ==================== End Title ====================

    // ==================== Address ====================
    var addressLi = document.createElement("li");
    addressLi.classList.add("list-group-item");
    addressLi.classList.add("fist-item");

    var addressLabel = document.createElement("label");

    var addressLabelNode = document.createTextNode("Billing Address");
    addressLabel.appendChild(addressLabelNode);
    addressLi.appendChild(addressLabel);

    var addressInput = document.createElement("input");
    addressInput.setAttribute("type","text");
    addressInput.classList.add("form-control");
    addressInput.id = "storage-address-"+storage.id;
    addressInput.value = storage.address;

    addressLi.appendChild(addressInput);
    leftContainer.appendChild(addressLi);
    // ==================== End Address ====================

    // ==================== City ====================
    var cityLi = document.createElement("li");
    cityLi.classList.add("list-group-item");
    cityLi.classList.add("fist-item");

    var cityLabel = document.createElement("label");

    var cityLabelNode = document.createTextNode("City");
    cityLabel.appendChild(cityLabelNode);
    cityLi.appendChild(cityLabel);

    var cityInput = document.createElement("input");
    cityInput.setAttribute("type","text");
    cityInput.classList.add("form-control");
    cityInput.id = "storage-city-"+storage.id;
    cityInput.value = storage.city;

    cityLi.appendChild(cityInput);
    leftContainer.appendChild(cityLi);
    // ==================== End City ====================

    // ==================== State ====================
    var stateLi = document.createElement("li");
    stateLi.classList.add("list-group-item");
    stateLi.classList.add("fist-item");

    var stateLabel = document.createElement("label");

    var stateLabelNode = document.createTextNode("State");
    stateLabel.appendChild(stateLabelNode);
    stateLi.appendChild(stateLabel);

    var stateInput = document.createElement("input");
    stateInput.setAttribute("type","text");
    stateInput.classList.add("form-control");
    stateInput.id = "storage-state-"+storage.id;
    stateInput.value = storage.state;

    stateLi.appendChild(stateInput);
    leftContainer.appendChild(stateLi);
    // ==================== End State ====================

    // ==================== zip ====================
    var zipLi = document.createElement("li");
    zipLi.classList.add("list-group-item");
    zipLi.classList.add("fist-item");

    var zipLabel = document.createElement("label");

    var zipLabelNode = document.createTextNode("ZIP");
    zipLabel.appendChild(zipLabelNode);
    zipLi.appendChild(zipLabel);

    var zipInput = document.createElement("input");
    zipInput.setAttribute("type","text");
    zipInput.classList.add("form-control");
    zipInput.id = "storage-zip-"+storage.id;
    zipInput.value = storage.zip;

    zipLi.appendChild(zipInput);
    leftContainer.appendChild(zipLi);
    // ==================== End zip ====================
    // ==================== End Left Container ====================

    // ==================== Right Container ====================

    // ==================== Phone ====================
    var phoneLi = document.createElement("li");
    phoneLi.classList.add("list-group-item");
    phoneLi.classList.add("fist-item");

    var phoneLabel = document.createElement("label");
    phoneLabel.style.marginTop = "28px";

    var phoneNode = document.createTextNode("Contact Phone");
    phoneLabel.appendChild(phoneNode);
    phoneLi.appendChild(phoneLabel);

    var phoneInput = document.createElement("input");
    phoneInput.setAttribute("type","text");
    phoneInput.classList.add("form-control");
    phoneInput.id = "storage-phone-"+storage.id;
    phoneInput.value = storage.phone;

    phoneLi.appendChild(phoneInput);
    rightContainer.appendChild(phoneLi);
    // ==================== End Phone ====================

    // ==================== Contact ====================
    var contactLi = document.createElement("li");
    contactLi.classList.add("list-group-item");
    contactLi.classList.add("fist-item");

    var contactLabel = document.createElement("label");

    var contactNode = document.createTextNode("Contact Name");
    contactLabel.appendChild(contactNode);
    contactLi.appendChild(contactLabel);

    var contactInput = document.createElement("input");
    contactInput.setAttribute("type","text");
    contactInput.classList.add("form-control");
    contactInput.id = "storage-contact-"+storage.id;
    contactInput.value = storage.contact;

    contactLi.appendChild(contactInput);
    rightContainer.appendChild(contactLi);
    // ==================== End Contact ====================

    // ==================== Email ====================
    var emailLi = document.createElement("li");
    emailLi.classList.add("list-group-item");
    emailLi.classList.add("fist-item");

    var emailLabel = document.createElement("label");

    var emailNode = document.createTextNode("Contact Email");
    emailLabel.appendChild(emailNode);
    emailLi.appendChild(emailLabel);

    var emailInput = document.createElement("input");
    emailInput.setAttribute("type","text");
    emailInput.classList.add("form-control");
    emailInput.id = "storage-email-"+storage.id;
    emailInput.value = storage.email;

    emailLi.appendChild(emailInput);
    rightContainer.appendChild(emailLi);
    // ==================== End Email ====================

    // ==================== Comment ====================
    var commentLi = document.createElement("li");
    commentLi.classList.add("list-group-item");
    commentLi.classList.add("fist-item");

    var commentLabel = document.createElement("label");

    var commentLabelNode = document.createTextNode("Comments");
    commentLabel.appendChild(commentLabelNode);
    commentLi.appendChild(commentLabel);

    var commentTextArea = document.createElement("textarea");
    commentTextArea.style.height = "84px";
    commentTextArea.classList.add("form-control");
    commentTextArea.setAttribute("placeholder","Comments for this storage");
    commentTextArea.id = "storage-comment-"+storage.id;

    var commentTextNode = document.createTextNode(storage.comment);

    commentTextArea.appendChild(commentTextNode);
    commentLi.appendChild(commentTextArea);
    rightContainer.appendChild(commentLi);
    // ==================== End Comment ====================
    // ==================== End Right Container ====================

    // ==================== Bottom Details ====================

    // ==================== Price Per 100 CuF ====================
    var pricePer100Li = document.createElement("li");
    pricePer100Li.classList.add("list-group-item");
    pricePer100Li.classList.add("fist-item");

    var pricePer100Label = document.createElement("label");

    var pricePer100Node = document.createTextNode("Price Per 100 Cubic Feet");
    pricePer100Label.appendChild(pricePer100Node);
    pricePer100Li.appendChild(pricePer100Label);

    var pricePer100Input = document.createElement("input");
    pricePer100Input.setAttribute("type","number");
    pricePer100Input.setAttribute("min",0);
    pricePer100Input.classList.add("form-control");
    pricePer100Input.id = "storage-pricePer100-"+storage.id;
    pricePer100Input.value = storage.price_daily_per_100;

    pricePer100Li.appendChild(pricePer100Input);
    leftBottomContainer.appendChild(pricePer100Li);
    // ==================== End Price Per 100 CuF ====================

    // ==================== Price Per 1000 Lbs ====================
    var pricePer1000Li = document.createElement("li");
    pricePer1000Li.classList.add("list-group-item");
    pricePer1000Li.classList.add("fist-item");

    var pricePer1000Label = document.createElement("label");

    var pricePer1000Node = document.createTextNode("Price Per 1000 Lbs");
    pricePer1000Label.appendChild(pricePer1000Node);
    pricePer1000Li.appendChild(pricePer1000Label);

    var pricePer1000Input = document.createElement("input");
    pricePer1000Input.setAttribute("type","number");
    pricePer1000Input.setAttribute("min",0);
    pricePer1000Input.classList.add("form-control");
    pricePer1000Input.id = "storage-pricePer1000-"+storage.id;
    pricePer1000Input.value = storage.price_daily_per_1000;

    pricePer1000Li.appendChild(pricePer1000Input);
    leftBottomContainer.appendChild(pricePer1000Li);
    // ==================== End Price Per 1000 Lbs ====================

    // ==================== Price Per Crate ====================
    var pricePerCrateLi = document.createElement("li");
    pricePerCrateLi.classList.add("list-group-item");
    pricePerCrateLi.classList.add("fist-item");

    var pricePerCrateLabel = document.createElement("label");

    var pricePerCrateNode = document.createTextNode("Price Per Crate");
    pricePerCrateLabel.appendChild(pricePerCrateNode);
    pricePerCrateLi.appendChild(pricePerCrateLabel);

    var pricePerCrateInput = document.createElement("input");
    pricePerCrateInput.setAttribute("type","number");
    pricePerCrateInput.setAttribute("min",0);
    pricePerCrateInput.classList.add("form-control");
    pricePerCrateInput.id = "storage-pricePerCrate-"+storage.id;
    pricePerCrateInput.value = storage.price_daily_per_crate;

    pricePerCrateLi.appendChild(pricePerCrateInput);
    rightBottomContainer.appendChild(pricePerCrateLi);
    // ==================== End Price Per Crate ====================

    // ==================== Tax ====================
    var taxLi = document.createElement("li");
    taxLi.classList.add("list-group-item");
    taxLi.classList.add("fist-item");

    var taxLabel = document.createElement("label");

    var taxNode = document.createTextNode("Tax %");
    taxLabel.appendChild(taxNode);
    taxLi.appendChild(taxLabel);

    var taxInput = document.createElement("input");
    taxInput.setAttribute("type","number");
    taxInput.setAttribute("min",0);
    taxInput.setAttribute("max",100);
    taxInput.classList.add("form-control");
    taxInput.id = "storage-tax-"+storage.id;
    taxInput.value = storage.tax;

    taxLi.appendChild(taxInput);
    rightBottomContainer.appendChild(taxLi);
    // ==================== End Tax ====================

    // ==================== Flat Rate ====================
    var flatRateLi = document.createElement("li");
    flatRateLi.classList.add("list-group-item");
    flatRateLi.classList.add("fist-item");

    var flatRateLabel = document.createElement("label");

    var flatRateNode = document.createTextNode("Price Flat Rate");
    flatRateLabel.appendChild(flatRateNode);
    flatRateLi.appendChild(flatRateLabel);

    var flatRateInput = document.createElement("input");
    flatRateInput.setAttribute("type","number");
    flatRateInput.setAttribute("min",0);
    flatRateInput.classList.add("form-control");
    flatRateInput.id = "storage-flatRate-"+storage.id;
    flatRateInput.value = storage.price_flat_rate;

    flatRateLi.appendChild(flatRateInput);
    rightBottomContainer.appendChild(flatRateLi);
    // ==================== End Flat Rate ====================

    // ==================== Late Fee ====================
    var lateFeeLi = document.createElement("li");
    lateFeeLi.classList.add("list-group-item");
    lateFeeLi.classList.add("fist-item");

    var lateFeeLabel = document.createElement("label");

    var lateFeeNode = document.createTextNode("Late Fee");
    lateFeeLabel.appendChild(lateFeeNode);
    lateFeeLi.appendChild(lateFeeLabel);

    var lateFeeInput = document.createElement("input");
    lateFeeInput.setAttribute("type","number");
    lateFeeInput.setAttribute("min",0);
    lateFeeInput.classList.add("form-control");
    lateFeeInput.id = "storage-lateFee-"+storage.id;
    lateFeeInput.value = storage.lateFeePerDay;

    lateFeeLi.appendChild(lateFeeInput);
    leftBottomContainer.appendChild(lateFeeLi);
    // ==================== End Late Fee ====================
    var br = document.createElement("br");
    rightBottomContainer.appendChild(br);

    var divRow = document.createElement("div");
    divRow.classList.add("row");

    var divColDel = document.createElement("div");
    divColDel.classList.add("col-md-6");
    // ==================== Delete Button ====================
    var deleteBtn = document.createElement("button");
    var deleteNode = document.createTextNode("Remove Storage");
    deleteBtn.setAttribute("onclick","deleteStorage("+storage.id+")")
    deleteBtn.classList.add("btn");
    deleteBtn.classList.add("btn-block");
    deleteBtn.classList.add("btn-sm");
    deleteBtn.classList.add("btn-danger");
    deleteBtn.style.marginBottom = "5px";
    deleteBtn.appendChild(deleteNode);
    divColDel.appendChild(deleteBtn);
    divRow.appendChild(divColDel);
    // ==================== End Delete Button ====================
    // ==================== Save Button ====================
    var divColSave = document.createElement("div");
    divColSave.classList.add("col-md-6");
    var updateBTN = document.createElement("button");
    var updateNode = document.createTextNode("Save Changes");
    updateBTN.setAttribute("onclick","updateStorage("+storage.id+")")
    updateBTN.classList.add("btn");
    updateBTN.classList.add("btn-block");
    updateBTN.classList.add("btn-sm");
    updateBTN.classList.add("btn-primary");
    updateBTN.style.marginBottom = "5px";
    updateBTN.appendChild(updateNode);
    divColSave.appendChild(updateBTN);
    divRow.appendChild(divColSave);
    buttonContainer.appendChild(divRow);
    // ==================== End Save Button ====================
}
function toggleActivation(isActive,id,e){

    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    isActive = (isActive==0?1:0);

    var strUrl = BASE_URL+'/console/actions/moving/storage/activeStorage.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            isActive: isActive,
            id: id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        try {
            data = JSON.parse(data);

            if (data == true) {
                var btn = document.getElementById("activeBtn" + id);
                var label = document.getElementById("activeLabel" + id);
                if (isActive == 1) {
                    //swal("Success!", "Storage '" + title + "' Has Been Activated", "success");
                    btn.checked = true;
                    label.setAttribute("onclick", "toggleActivation(1," + id + ")");
                } else {
                    //swal("Success!", "Storage '" + title + "' Has Been Deactivated", "success");
                    btn.checked = false;
                    label.setAttribute("onclick", "toggleActivation(0," + id + ")");
                }
            }
        }catch(e){
            swal("Oops", "Please try again later", "error");
        }

    });



}

function updateStorage(id){

    var title = document.getElementById("storage-title-"+id).value;
    var address = document.getElementById("storage-address-"+id).value;
    var city = document.getElementById("storage-city-"+id).value;
    var state = document.getElementById("storage-state-"+id).value;
    var zip = document.getElementById("storage-zip-"+id).value;
    var phone = document.getElementById("storage-phone-"+id).value;
    var contact = document.getElementById("storage-contact-"+id).value;
    var email = document.getElementById("storage-email-"+id).value;
    var pricePer100 = document.getElementById("storage-pricePer100-"+id).value;
    var pricePer1000 = document.getElementById("storage-pricePer1000-"+id).value;
    var pricePerCrate = document.getElementById("storage-pricePerCrate-"+id).value;
    var tax = document.getElementById("storage-tax-"+id).value;
    var lateFee = document.getElementById("storage-lateFee-"+id).value;
    var comment = document.getElementById("storage-comment-"+id).value;
    var flatRate = document.getElementById("storage-flatRate-"+id).value;


    var strUrl = BASE_URL+'/console/actions/moving/storage/updateStorage.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method:"post",
        data:{
            id:id,
            title:title,
            address:address,
            city:city,
            state:state,
            zip:zip,
            phone:phone,
            contact:contact,
            email:email,
            pricePer100:pricePer100,
            pricePer1000:pricePer1000,
            pricePerCrate:pricePerCrate,
            tax:tax,
            lateFee:lateFee,
            comment:comment,
            flatRate:flatRate
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        data = JSON.parse(data);
        if(data == true){
            toastr.success("Storage ''"+title+"'' has been Updated", "Success");
            getStorage(id);
            getStorages();
        }
    });
}

