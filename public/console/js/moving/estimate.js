// This file is not in use since 09/12/2019 (moved to a function inside lead.js)

function initMovingEstimate(){

    // data should get:

    // - initPrice
    // - pricePerCf
    // - fuel
    // - discountToggle
    // - coupon
    // - senior
    // - veteranDiscount
    // - packersToggle
    // - packingHours
    // - packingPerHour
    // - unpackingHours
    // - unpackingPerHour
    // - agentFee
    // - movingMaterials

    // =================== START GETTING THE DATA ===================
    var data = {
        "initPrice":parseFloat(document.getElementById("totalCF").value),
        "pricePerCf":parseFloat(document.getElementById("perCF").value),
        "fuel":parseFloat(document.getElementById("fuelSurcharge").value),
        "coupon":parseFloat(document.getElementById("couponDiscount").value),
        "senior":parseFloat(document.getElementById("seniorDiscount").value),
        "veteranDiscount":parseFloat(document.getElementById("veteranDiscount").value),
        "packingPackers":parseFloat(document.getElementById("packers").value),
        "packingHours":parseFloat(document.getElementById("packersHrs").value),
        "packingPerHour":parseFloat(document.getElementById("packersPerHrs").value),
        "unpackingPackers":parseFloat(document.getElementById("unpackers").value),
        "unpackingHours":parseFloat(document.getElementById("unpackersHrs").value),
        "unpackingPerHour":parseFloat(document.getElementById("unpackersPerHrs").value),
        "discount":parseFloat(document.getElementById("generalDiscount").value),
        "storageFee":parseFloat(document.getElementById("monthlyStorageFee").value),
        "extraCharges":extraCharges,
        "agentFee":parseFloat(document.getElementById("agentFee").value),
        "movingMaterials":parseFloat(document.getElementById("movingMaterials").value),
        "comments":document.getElementById("estimateComments").value
    };
    if (document.getElementById("discountsToggle").checked) {data.discountToggle = true;}else{data.discountToggle = false;}
    if (document.getElementById("packersToggle").checked)   {data.packersToggle = true;}else{data.packersToggle = false;}
    // =================== END GETTING THE DATA ===================


    // =================== START CALCULATING THE DATA ===================
    var dataArray = [];

    // =================== START InitPrice ===================
    var currentPrice = parseFloat(data.initPrice*data.pricePerCf);
    if (isNaN(currentPrice)){
        currentPrice = 0;
    }

    dataArray['startInitial'] = currentPrice;
    // =================== END InitPrice ===================
    // =================== START fuel ===================
    data.fuel = (currentPrice/100)*data.fuel;
    if (isNaN(data.fuel)){
        data.fuel = 0;
    }
    currentPrice = parseFloat(currentPrice+data.fuel);
    dataArray['fuel'] = data.fuel;

    dataArray['subTotal1'] = currentPrice;
    // =================== END fuel ===================
    // =================== START discounts & coupons ===================
    if (data.discountToggle == true){
        var CALCcouponDiscount = ((currentPrice)/100)*data.coupon;
        var CALCSeniorDiscount = ((currentPrice-CALCcouponDiscount)/100)*data.senior;
        var CALCVeteranDiscount = ((currentPrice-CALCcouponDiscount-CALCSeniorDiscount)/100)*data.veteranDiscount;

        if (isNaN(CALCcouponDiscount)){
            CALCcouponDiscount = 0;
        }
        if (isNaN(CALCSeniorDiscount)){
            CALCSeniorDiscount = 0;
        }
        if (isNaN(CALCVeteranDiscount)){
            CALCVeteranDiscount = 0;
        }
        dataArray['coupon'] = CALCcouponDiscount;
        dataArray['senior'] = CALCSeniorDiscount;
        dataArray['veteranDiscount'] = CALCVeteranDiscount;


        var totalDiscount = parseFloat(CALCcouponDiscount);
        totalDiscount = totalDiscount + parseFloat(CALCSeniorDiscount);
        totalDiscount = totalDiscount + parseFloat(CALCVeteranDiscount);

        currentPrice = parseFloat(currentPrice-totalDiscount);
    }else{
        dataArray['coupon'] = 0;
        dataArray['senior'] = 0;
        dataArray['veteranDiscount'] = 0;
    }
    dataArray['subTotal2'] = currentPrice;
    // =================== END discounts & coupons ===================
    // =================== START Packers ===================
    if (data.packersToggle == true) {
        var CALApacking = parseFloat(data.packingHours * data.packingPerHour);
        if (isNaN(CALApacking)) {
            CALApacking = 0;
        }
        dataArray['packing'] = CALApacking;

        var CALAunpacking = parseFloat(data.unpackingHours * data.unpackingPerHour);
        if (isNaN(CALAunpacking)) {
            CALAunpacking = 0;
        }
        dataArray['unpacking'] = CALAunpacking;

        var packingAndUnpackingCosts = CALApacking + CALAunpacking;
        if (isNaN(packingAndUnpackingCosts)) {
            packingAndUnpackingCosts = 0;
        }
        currentPrice = parseFloat(currentPrice + packingAndUnpackingCosts);
    }else{
        dataArray['packing'] = 0;
        dataArray['unpacking'] = 0;
    }
    dataArray['subTotal3'] = currentPrice;
    // =================== END Packers ===================
    // =================== START Discount ===================
    var CALCgeneralDiscount = (currentPrice/100)*data.discount;
    if (isNaN(CALCgeneralDiscount)){
        CALCgeneralDiscount = 0;
    }

    dataArray['generalDiscount'] = CALCgeneralDiscount;

    currentPrice = parseFloat(currentPrice-CALCgeneralDiscount);
    dataArray['subTotal4'] = currentPrice;
    // =================== END Discount ===================
    // =================== START Extra charges ===================
    if (extraArr.length > 0){
        for (var i=0;i<extraArr.length;i++){
            var extraCharge = parseFloat(document.getElementById("Extra-Price-"+extraArr[i]).value);
            if (isNaN(extraCharge)){
                extraCharge = 0;
            }
            dataArray['extraCharge-'+i] = extraCharge;
            $("#Extra-Total-"+extraArr[i]).val(extraCharge);
            currentPrice = parseFloat(currentPrice) + parseFloat(extraCharge);
        }
    }
    // =================== END Extra charges ===================
    // =================== START Agent Fee ===================
    if (isNaN(data.agentFee)){
        data.agentFee = 0;
    }
    if (data.agentFee !== 0){
        var CALCagentFee = (currentPrice * data.agentFee) / 100;
        currentPrice = parseFloat(currentPrice+CALCagentFee);
        dataArray['agentFee'] = CALCagentFee;
    }else{
        dataArray['agentFee'] = 0;
    }
    if (data.movingMaterials !== 0){
        dataArray['movingMaterials'] = data.movingMaterials;
        currentPrice = parseFloat(currentPrice+data.movingMaterials);
    }
    // =================== END Agent Fee ===================

    dataArray['totalEstimate'] = currentPrice;

    // =================== END CALCULATING THE DATA ===================



    // =================== START SETTING THE DATA ===================
    $("#startInitial").val(dataArray["startInitial"].toFixed(2));
    $("#totalFuelSurcharge").val(dataArray["fuel"].toFixed(2));
    $("#subTotal1").text("$"+dataArray["subTotal1"].toFixed(2));
    $("#totalCouponDiscount").val(dataArray["coupon"].toFixed(2));
    $("#totalSeniorDiscount").val(dataArray["senior"].toFixed(2));
    $("#totalVeteranDiscount").val(dataArray["veteranDiscount"].toFixed(2));
    $("#subTotal2").text("$"+dataArray["subTotal2"].toFixed(2));
    $("#totalPackingCosts").val(dataArray["packing"].toFixed(2));
    $("#totalUnPackingCosts").val(dataArray["unpacking"].toFixed(2));
    $("#subTotal3").text("$"+dataArray["subTotal3"].toFixed(2));
    $("#totalGeneralDiscount").val(dataArray["generalDiscount"].toFixed(2));
    $("#subTotal4").text("$"+dataArray["subTotal4"].toFixed(2));
    $("#totalAgentFee").val(dataArray["agentFee"].toFixed(2));

    if (dataArray["movingMaterials"]) {
        $("#totalMovingMaterials").val(dataArray["movingMaterials"].toFixed(2));
    }else{
        $("#totalMovingMaterials").val(0.00);
    }
    $("#TotalEstimate").text("$"+dataArray["totalEstimate"].toFixed(2));
    // =================== END SETTING THE DATA ===================
}