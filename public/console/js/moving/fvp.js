var l;
var d;
$(document).ready(function () {
    l = $('.updateFvpBtn').ladda();
    d = $('.resetFvpBtn').ladda();
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        var input = $(".fvp-input");
        console.log(input);
        for (var i = 0; i < input.length; i++){
            input[i].addEventListener(event, function() {
                if (returnCurrencyFromString(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        }
    });

});
var FVPcontroller = {
    getData: function(){
        var strUrl = BASE_URL+'/console/actions/moving/fvp/getData.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {

            try{
                data = JSON.parse(data);
                FVPcontroller.setFVPHeader(data.levels);
                FVPcontroller.setFVPTable(data.data);
            }catch(e){
                console.log(e);
                swal("Oops", "Please try again later", "error");
            }
        });
    },
    setFVPHeader: function(data){

        document.getElementById("fvpTableHeader").innerHTML = "";
        var tableHeader = document.getElementById("fvpTableHeader");

        if(data.length > 0){
            var tr = document.createElement("tr");

            var th = document.createElement("th");
            th.innerHTML = "Amount of Liability <br> (up to)";

            tr.appendChild(th);

            for(var i = 0;i<data.length;i++){
                var th = document.createElement("th");

                var input = document.createElement("input");
                input.className = "form-control fvp-input";
                input.value = data[i];
                input.name = "levels[]";
                input.type = "tel";
                FVPcontroller.stringAndSpecialCharCleanerFromInput(input);

                th.appendChild(input);
                tr.appendChild(th);
            }
            tableHeader.appendChild(tr);
        }else{

        }

    },
    setFVPTable: function(data){
        document.getElementById("fvpTableBody").innerHTML = "";
        var tableBody = document.getElementById("fvpTableBody");

        for(var level in data){
            var rates = data[level];

            var row = FVPcontroller.setRow(rates,level);

            tableBody.appendChild(row);
        }

    },
    setRow:function(rates,level){
        var tr = document.createElement("tr");
        var td = document.createElement("td");

        var input = document.createElement("input");
        input.className = "form-control fvp-input";
        input.value = level;
        input.name = "aol[]";
        input.type = "number";
        FVPcontroller.stringAndSpecialCharCleanerFromInput(input);

        td.appendChild(input);
        tr.appendChild(td);

        for(var i = 0;i<rates.length;i++){
            var rate = rates[i];

            var td = document.createElement("td");

            var totalRows = ($('#fvpTableBody tr').length);

            var input = document.createElement("input");
            input.className = "form-control fvp-input";
            input.value = rate;
            input.name = "rate["+totalRows+"][]";
            input.type = "number";
            FVPcontroller.stringAndSpecialCharCleanerFromInput(input);

            td.appendChild(input);
            tr.appendChild(td);
        }
        return tr;
    },
    addRow:function(){

        var table = document.getElementById("fvpTableBody");
        var rowCount = table.rows.length;

        if(rowCount >= 25){
            return;
        }

        try{
            var tableRef = document.getElementById('fvpTableBody');
            var totalCol = tableRef.rows[0].cells.length-1;

        }catch (e) {
            var tableRef = document.getElementById('fvpTableHeader');
            var totalCol = tableRef.rows[0].cells.length-1;
        }

        var rates = [];
        for(var i = 0;i<totalCol;i++){
            rates.push(0);
        }
        var row = FVPcontroller.setRow(rates,0);

        tableRef.appendChild(row);

    },
    removeRow:function(){
        var table = document.getElementById("fvpTableBody");
        var rowCount = table.rows.length;

        if(rowCount >= 2){
            table.deleteRow(rowCount -1);
        }
    },
    addCol:function(){
        var colCount = document.getElementById('fvpTableHeader').rows[0].cells.length;
        if(colCount >= 10){
            return;
        }


        $("#fvpTableHeader").find('tr').each(function(){
            var trow = $(this);
            var th = document.createElement("th");

            var input = document.createElement("input");
            input.className = "form-control fvp-input";
            input.value = "0";
            input.name = "levels[]";
            input.type = "number";
            FVPcontroller.stringAndSpecialCharCleanerFromInput(input);

            th.append(input);
            trow.append(th);

        });

        $("#fvpTableBody").find('tr').each(function(){
            var trow = $(this);
            var rowCount = $(this).index();

            // trow.append('<td><input type="tel" class="form-control fvp-input" value="0" name="rate['+rowCount+'][]" /></td>');
            var td = document.createElement("td");

            var input = document.createElement("input");
            input.className = "form-control fvp-input";
            input.value = "0";
            input.name = rate+'['+rowCount+'][]';
            input.type = "number";
            FVPcontroller.stringAndSpecialCharCleanerFromInput(input);

            td.append(input);
            trow.append(td);
        });
    },
    removeCol:function(){
        var colCount = document.getElementById('fvpTableHeader').rows[0].cells.length;

        if(colCount >= 3){
            $('#fvpTableHeader tr').find('th:last-child, td:last-child').remove();
            $('#fvpTableBody tr').find('th:last-child, td:last-child').remove();
        }
    },
    saveFVP: function(responseText, statusText, xhr, $form) {
        l.ladda('stop');
        if(statusText == "success"){
            toastr.success("Full Value Protection Updated","Updated");
        }else{
            toastr.error("Updating Failed");
        }
    },
    resetToDefault: function () {
        swal({
            title: "Are you sure?",
            text: "This will reset the full value protection entries back to default.",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then( function (isConfirm) {
            if (isConfirm) {
                d.ladda( 'start' );
                var strUrl = BASE_URL + '/console/actions/moving/fvp/resetData.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        check: "1",
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data == true) {
                            d.ladda('stop');
                            toastr.success("Full Value Protection Reset","Reset");
                            FVPcontroller.getData();
                        }
                    } catch (e) {
                        d.ladda('stop');
                        toastr.error("Reset Failed");
                    }
                });
            }
        });
    },
    valuationChargeFormula: function () {
        var valuationCharge = $('#valuationCharge').val();
        var amountOfLiability = $('#amountOfLiability').val();
        var anchorNumber = $('#anchorNumber').val();

        var rate = (valuationCharge * anchorNumber) / amountOfLiability;
        rate = rate.toFixed(2)
        $('#rate').val(rate);
    },
    stringAndSpecialCharCleanerFromInput: function (input) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                input.addEventListener(event, function() {
                    if (returnCurrencyFromString(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
        });
    },
    openRateCalculator: function(){
        $( "#ValuationChargeRateCalculator").show();
        $( "#openCalculatorBtn").hide();
        $( "#ValuationChargeRateCalculator" ).draggable();
    },
    closeRateCalculator: function () {
        $( "#openCalculatorBtn").show();
        $( "#ValuationChargeRateCalculator").hide();
    }
}



$('#fvpForm').ajaxForm({
   beforeSubmit: beforeSubmit,  // pre-submit callback
    success: FVPcontroller.saveFVP  // post-submit callback
});
function beforeSubmit() {
    l.ladda( 'start' );
}
function returnCurrencyFromString(value) {
    return /^\d*[.,]?\d{0,2}$/.test(value);
}