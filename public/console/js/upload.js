Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};
var userIdSelected = null;
var currentFolderId = 0;
var parentFolderId = null;

// ======= Start Drop section =======
var drop_id = 0;
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(id) {
    drop_id = id;
}
function drop(destination) {
    if(document.getElementById("file"+drop_id) != null){
        document.getElementById("file"+drop_id).remove();
        setFileFolder(drop_id,destination);
    }
    loadFolder(currentFolderId);
}
$("body *").droppable({
    drop: function(){
        resetPosition();
    }
});
// ======= End Drop section =======
function reloader(){
    // clear everything, applying back to root folder
    currentFolderId = 0;
    //clear all container and set folders
    $(".myMenu").hide();
    filesCheck = [];
    clearItems();
    getFolders(0);
    // var treeView = document.getElementById("treeView");
    // treeView.innerHTML = "";
    // var spanRoot = document.createElement("span");
    // spanRoot.style.cursor = "pointer";
    // spanRoot.classList.add("label");
    // spanRoot.classList.add("label-info");
    // spanRoot.id = "root";
    // spanRoot.setAttribute("onclick","reloader()");
    // var spanText = document.createTextNode("Home");
    // spanRoot.appendChild(spanText);
    // treeView.appendChild(spanRoot);
}
function setActive(current){
    var a = document.querySelectorAll("#folderContainer li a");
    for (var i = 0; i < a.length; i++) {
        a[i].style.fontWeight = "unset";
    }
    current.style.fontWeight = "bold";
}
function loadFolder(folderID){

    if(folderID == null){folderID = 0;}

    currentFolderId = folderID;
    document.getElementById("folderId").value = folderID;

    // empty the container first
    clearItems();
    // get files and set them to the container
    // getFolderFolders(folderID);
    getFolderFiles(folderID);
    $(".myMenu").hide();
    filesChecked = 0;
    filesCheck = [];

}

function getFolders(folderID){
    currentFolderId = folderID;
    loadingState();
    var strUrl = BASE_URL+'/console/actions/user/getUserFolders.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            userIdSelected:userIdSelected,
            folderID: folderID
        },
        success: function (html) {
            strReturn = html;
        },
        async: false
    }).done(function (data) {

        $("#items-container").html("");
        data = JSON.parse(data);
        parentFolderId = data.parentFolderId;
        // if(folderID > 0){
        //     setToRoot(parentFolderId,data.folders,folderID);
        //
        // }else{
        //     setFolders(data.folders,folderID);
        // }
        getFolderFiles(folderID);
        var folderContainer = document.getElementById("folderContainer");
        folderContainer.innerHTML = "";
        // set Home left menu
        var li = document.createElement("li");
        li.id = "rootFolder";
        li.classList.add("ui-droppable");
        li.setAttribute("ondrop","drop(0)");
        li.setAttribute("ondragover","allowDrop(event)");
        var a = document.createElement("a");
        a.setAttribute("onclick","loadFolder(0);setActive(this);");
        a.style.fontWeight = "bold";
        var i = document.createElement("i");
        i.classList.add("fa");
        i.classList.add("fa-home");
        var aText = document.createTextNode("Home");
        a.appendChild(i);
        a.appendChild(aText);
        a.classList.add("sideFolders");
        li.appendChild(a);

        folderContainer.appendChild(li);
        for (var i=0;i<data['folders'].length;i++){
            setSideFolders(data['folders'][i],folderContainer);
        }
        $("#rootFolder").droppable({
            hoverClass: "ui-state-hover",
            ondrop: function(){
                drop(parentFolderId);
            }
        });

    });

}

function resetPosition(){
    try {
        document.getElementById("file"+drop_id).setAttribute("style","position:relative;");
    }catch (e) {

    }
}

function clearItems(){

    var itemsContainer = $("#items-container");
    // empty the container
    itemsContainer.html("");

}
function getFolderFiles(folderID){

    destDir = "?folder="+folderID;
    var strUrl = BASE_URL+'/console/actions/user/getUserFiles.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            userIdSelected:userIdSelected,
            folderID: folderID
        },
        success: function (html) {
            strReturn = html;
        },
        async: false
    }).done(function (data) {

        data = JSON.parse(data);
        setFolderFiles(data);

    });

}

function setSideFolders(data,container){
    // sets left menu folders (treeView)
    var li = document.createElement("li");
    li.title = data.folder_name;
    li.id = "sideFolder-"+data.id;
    li.classList.add("ui-droppable");
    li.setAttribute("ondrop","drop("+data['folder_id']+")");
    li.setAttribute("ondragover","allowDrop(event)");

    var a = document.createElement("a");
    a.setAttribute("onclick","loadFolder("+data.folder_id+");setActive(this);");
    a.classList.add("sideFolders");

    var i = document.createElement("i");
    i.style.verticalAlign = "middle";
    i.style.marginLeft = "13px";
    i.classList.add("fa");
    i.classList.add("fa-level-down");

    var i2 = document.createElement("i");
    i2.style.verticalAlign = "middle";
    i2.classList.add("fa");
    i2.classList.add("fa-folder");

    var span = document.createElement("span");
    span.classList.add("spanText");
    span.style.textOverflow = "ellipsis";
    span.style.overflow = "hidden";
    span.style.whiteSpace = "nowrap";
    span.style.maxWidth = "49%";
    span.style.display = "inline-block";
    span.style.verticalAlign = "middle";

    var spanText = document.createTextNode(data.folder_name);

    a.appendChild(i);
    a.appendChild(i2);
    span.appendChild(spanText);
    a.appendChild(span);

    var buttonContainer = document.createElement("span");
    var button = document.createElement("button");
    button.classList.add("btn");
    button.classList.add("btn-danger");
    button.classList.add("btn-xs");
    button.classList.add("pull-right");
    button.setAttribute("onclick","removeDir("+data.id+")");
    button.style.display = "none";
    button.style.verticalAlign = "middle";
    button.style.padding = "0px 5px";
    button.style.fontSize = "11px";

    var buttonText = document.createTextNode("x");

    button.appendChild(buttonText);
    buttonContainer.appendChild(button);

    var button = document.createElement("button");
    button.classList.add("btn");
    button.classList.add("btn-success");
    button.classList.add("btn-xs");
    button.classList.add("pull-right");
    button.setAttribute("onclick","changeName("+data.id+",'"+escapeHTML(data.folder_name)+"',event)");
    button.style.display = "none";
    button.style.verticalAlign = "middle";
    button.style.padding = "0px 4px";
    button.style.fontSize = "11px";
    button.style.marginRight = "3px";

    var buttonI = document.createElement("i");
    buttonI.style.margin = 0;
    buttonI.style.color = "white";
    buttonI.classList.add("fa");
    buttonI.classList.add("fa-pencil");

    button.appendChild(buttonI);
    buttonContainer.appendChild(button);
    a.appendChild(buttonContainer);
    li.appendChild(a);

    container.appendChild(li);

    $("#sideFolder-"+data.id).droppable({
        hoverClass: "ui-state-hover",
        ondrop: function(){
            drop(folderID);
        },
        greedy: false
    });
}
function escapeHTML(str) {return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");}

function unescapeHTML(str) {return str.replace(/&amp;/g, "&").replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&quot;/g,'"').replace(/&#039;/g,"'");}

function changeName(id,name,e) {

    e.stopImmediatePropagation();
    var container = document.getElementById("sideFolder-"+id);
    container.innerHTML = "";

    var div = document.createElement("div");
    div.classList.add("input-group");
    div.style.marginTop = "2px";
    div.style.marginBottom = "2px";
    div.innerHTML = "<i class=\"fa fa-level-down\" style=\"margin-left: 13px;\"></i><i class=\"fa fa-folder\"></i>";

    var input = document.createElement("input");
    input.style.width = "57.8%";
    input.style.boxShadow = "none";
    input.style.border = "none";
    input.style.outline = "none";
    input.style.margin = "3px";
    input.setAttribute("type","text");
    input.value = unescapeHTML(name);
    input.id = "editFolderName";
    var span = document.createElement("span");
    span.style.paddingTop = "3px";
    span.classList.add("input-group-btn");

    var button = document.createElement("button");
    button.setAttribute("onclick","changeFolderName("+id+")");
    button.style.marginTop = "-3px";
    button.style.padding = "0px 4px";
    button.style.fontSize = "11px";
    button.style.borderRadius = "3px";
    button.classList.add('btn');
    button.classList.add('btn-xs');
    button.classList.add('btn-primary');

    var i = document.createElement("i");
    i.style.marginRight = "0";
    i.style.color = "white";
    i.classList.add("fa");
    i.classList.add("fa-save");

    button.appendChild(i);

    var button2 = document.createElement("button");
    button2.setAttribute("onclick","reloader()");
    button2.style.marginTop = "-3px";
    button2.style.marginLeft = "3px";
    button2.style.padding = "0px 4px";
    button2.style.fontSize = "11px";
    button2.style.borderRadius = "3px";
    button2.classList.add('btn');
    button2.classList.add('btn-xs');
    button2.classList.add('btn-warning');

    var i2 = document.createElement("i");
    i2.style.marginRight = "0";
    i2.style.color = "white";
    i2.classList.add("fa");
    i2.classList.add("fa-undo");

    button2.appendChild(i2);
    div.appendChild(input);
    span.appendChild(button);
    span.appendChild(button2);
    div.appendChild(span);
    container.appendChild(div);
    document.getElementById("editFolderName").focus();
}

function changeFolderName(id) {

    var name = document.getElementById("editFolderName").value;
    var strUrl = BASE_URL+'/console/actions/user/setFolderName.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            userIdSelected:userIdSelected,
            id:id,
            name:name
        },
        success: function (html) {
            strReturn = html;
        },
        async: false
    }).done(function () {
        reloader();
    });

}

function setFileFolder(file_id,folder_id){
    var strUrl = BASE_URL+'/console/actions/user/setFileFolder.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            userIdSelected:userIdSelected,
            folderID: folder_id,
            fileID: file_id
        },
        success: function (html) {
            strReturn = html;
        },
        async: false
    });
}

function setFolderFiles(data){

    var itemsContainer = $("#items-container");

    for(var i=0;i<data.length;i++) {

        // setting Files Data
        var divFileBox = document.createElement("div");
        // divFileBox.setAttribute("onclick","window.open('"+BASE_URL+"/console/actions/system/filemanager/getLink.php?imageURL="+data[i]['file_url']+"')");
        divFileBox.style.cursor = "pointer";
        divFileBox.classList.add("file-box");
        divFileBox.classList.add("contextMenu");
        divFileBox.setAttribute("dataID",data[i]['id']);
        divFileBox.setAttribute("id", "file" + data[i]['id']);
        // make file dragable with the file ID
        divFileBox.setAttribute("draggable", "true");
        divFileBox.setAttribute("ondragend","myFunc()");
        divFileBox.setAttribute("ondragstart", "drag("+data[i]['id']+")");

        var divFile = document.createElement("div");
        divFile.classList.add("file");

        var aLink = document.createElement("a");
        aLink.setAttribute("href", BASE_URL+"/console/actions/system/filemanager/getLink.php?imageURL="+data[i]['file_url']);
        aLink.setAttribute("target","_blank");

        var is_img = false;
        if(data[i]['file_type'] == "jpeg" || data[i]['file_type'] == "JPEG" || data[i]['file_type'] == "jpg" || data[i]['file_type'] == "JPG" || data[i]['file_type'] == "gif" || data[i]['file_type'] == "GIF" || data[i]['file_type'] == "png" || data[i]['file_type'] == "PNG"){

            var divImage = document.createElement("div");
            divImage.setAttribute("onclick","window.open('"+BASE_URL+"/console/actions/system/filemanager/getLink.php?imageURL="+data[i]['file_url']+"')");
            divImage.classList.add("icon");
            divImage.style.backgroundImage = "url("+BASE_URL+"/console/actions/system/filemanager/getLink.php?imageURL="+data[i]['file_url']+")";
            divImage.style.backgroundSize = "cover";
            divImage.style.backgroundPosition = "center";
            divImage.style.backgroundRepeat = "no-repeat";

            var iconI = document.createElement("i");
            // iconI.classList.add("fa");
            // iconI.classList.add("fa-image");

            is_img = true;
        }else{
            var divIcon = document.createElement("div");
            divIcon.setAttribute("onclick","window.open('"+BASE_URL+"/console/actions/system/filemanager/getLink.php?imageURL="+data[i]['file_url']+"')");
            divIcon.classList.add("icon");

            var iconI = document.createElement("i");
            iconI.classList.add("fa");
            iconI.classList.add("fa-file");
        }

        if(data[i]['file_error']){
            var myText = document.createTextNode(data[i]['file_name']+" File Not Found!");
        }else{
            var myText = document.createTextNode(data[i]['file_name']);
        }
        var br = document.createElement("br");

        var small = document.createElement("small");
        small.classList.add("smallDate");
        var smallText = document.createTextNode("Added: "+data[i]["date_created"]);
        small.appendChild(smallText);

        var deleteBtn = document.createElement("button");
        deleteBtn.classList.add("pull-right");
        deleteBtn.classList.add("btn");
        deleteBtn.classList.add("btn-danger");
        deleteBtn.classList.add("btn-xs");
        deleteBtn.style.display = "none";
        deleteBtn.style.fontSize = "9px";
        deleteBtn.classList.add("deleteBtn");
        deleteBtn.setAttribute("onclick","removeFile("+data[i]['id']+",event)");
        var deleteText = document.createTextNode("X");

        deleteBtn.appendChild(deleteText);
        small.appendChild(deleteBtn);

        var divName = document.createElement("div");
        divName.classList.add("file-name");
        divName.style.whiteSpace = "nowrap";
        divName.style.overflow = "hidden";
        divName.style.textOverflow = "ellipsis";
        divName.title = data[i]['file_name'];
        divName.appendChild(myText);
        divName.appendChild(br);
        divName.appendChild(small);

        if(is_img == true){

            divImage.appendChild(iconI);

            divFile.appendChild(divImage);

        }else{

            divIcon.appendChild(iconI);
            divFile.appendChild(divIcon);
        }
        aLink.appendChild(divName);

        if(data[i]['file_error']){
            divFile.appendChild(divName);
        }else {
            divFile.appendChild(aLink);
        }

        divFileBox.appendChild(divFile);


        itemsContainer.append(divFileBox);
        var id = data[i]['id'];
        var name = data[i]['file_name'];
        $("#file"+id).draggable({
            start: function(event, ui) {
                $(this).draggable("option", "cursorAt", {
                    left: Math.floor(this.clientWidth / 2),
                    top: Math.floor(this.clientHeight / 2)
                });
            },
            drag: function(){
                drag(this.id.replace("file",""));
            },
            ondrop: function(){
                resetPosition();
            }
        });
    }
    stopLoadingState();
}

var filesChecked = 0;
var filesCheck = [];

function removeDir(dirID){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Folder",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/user/removeFolder.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    userIdSelected:userIdSelected,
                    folderID: dirID
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                $("#sideFolder-"+dirID).remove();
                reloader();

            });
        }
    });
}

function removeFile(fileID,e){
    if (e){ e.preventDefault(); }
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this files!",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/user/removeFile.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    userIdSelected:userIdSelected,
                    files: fileID
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            });
            $("#file"+fileID).remove();
        }
    });
}
$.contextMenu({
    selector: '.contextMenu',
    callback: function(key, options) {

        var myID = this[0].id;
        var r = /\d+/g;
        myID = myID.match(r);

        if(key == "Delete"){

            removeFile(myID[0]);

        }
    },
    items: {
        "Delete": {name: "Delete", icon: "fa-remove"},
    }
});

function checkFileBeforeUpload(el){
    document.getElementById("userIdSelected").value = userIdSelected;
    if (el.files[0].size/1024/1024 < 10){
        var dot = ".";
        var x = setInterval(function(){
            if (dot == "."){
                dot = "..";
            }else if (dot == "..") {
                dot = "...";
            }else if (dot == "..."){
                dot = ".";
            }
            document.getElementById("uploadButton").innerText = "Please Wait"+dot;
        },400);
        document.getElementById("uploadButton").disabled = true;
        var strUrl = BASE_URL+'/console/actions/user/uploadFile.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method: "POST",
            data: new FormData($('#uploadForm')[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function (html) {
                strReturn = html;
            },
            async: true,
        }).done(function (data) {
            try{
                data = JSON.parse(data);
                if (data.success == false){
                    for (var i =0;i<data.error.length;i++){
                        toastr.error(data.error[i],"Fail");
                    }
                }

            }catch (e) {

            }
            loadFolder(currentFolderId);
            clearInterval(x);
            document.getElementById("uploadButton").innerText = "Upload Files";
            document.getElementById("uploadButton").disabled = false;
        });
    }else{
        toastr.error("File Size is Too large, Max 10MB","Error");
    }
}
function createFolder(){
    var container = document.createElement("div");
    var h2 = document.createElement("h2");
    var h2Text = document.createTextNode("Select New Folder Name");
    h2.appendChild(h2Text);
    var input = document.createElement("input");
    input.id = "newFolderName";
    input.setAttribute("type","text");
    input.classList.add("form-control");
    container.appendChild(h2);
    container.appendChild(input);
    swal({
        title: false,
        content: container,
        buttons: {
            cancel: true,
            send: "Create"
        },
        icon: false,
    }).then(function(value){
        if (value == "send"){
            var strUrl = BASE_URL+'/console/actions/user/addFolder.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    userIdSelected:userIdSelected,
                    folderName: document.getElementById("newFolderName").value,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function(data){
                try {
                    data = JSON.parse(data);
                    reloader();
                    setTimeout(function(){$("#sideFolder-"+data+"> a").click();},150);
                }catch (e) { reloader(); }
            });
        }
    });
}



reloader();

// function setTreeView(id,name){
//     if(folderLoaded.includes(id)){
//         document.getElementById("tree-"+folderLoaded[folderLoaded.length-1]).remove();
//         folderLoaded = folderLoaded.filter(e => e !== folderLoaded[folderLoaded.length-1]);
//     }else{
//         folderLoaded.push(id);
//         var treeView = document.getElementById("treeView");
//         var treeViewcontainer = document.createElement("span");
//         treeViewcontainer.id = "tree-"+id;
//         var span = document.createElement("span");
//         span.classList.add("label");
//         span.classList.add("label-info");
//         var spanText = document.createTextNode(name);
//         var i = document.createElement("i");
//         i.classList.add("fa");
//         i.classList.add("fa-arrow-circle-right");
//
//         span.appendChild(spanText);
//         treeViewcontainer.appendChild(i);
//         treeViewcontainer.appendChild(span);
//         treeView.appendChild(treeViewcontainer);
//     }
// }

// function setToRoot(parentId,data,folderId){
//
//     var itemsContainer = $("#items-container");
//
//     // setting inside a folder - to Root button
//     var divFileBox = document.createElement("div");
//     divFileBox.classList.add("file-box");
//     divFileBox.id = "root"+parentId;
//     // make root dropable (so users can take things out of this folder)
//     divFileBox.setAttribute("ondrop","drop('"+parentId+"')");
//     divFileBox.setAttribute("ondragover","allowDrop(event)");
//
//     var divFile = document.createElement("div");
//     divFile.classList.add("file");
//     if(parentId == null || parentId == 0) {
//         var aLink = document.createElement("a");
//         aLink.setAttribute("onclick", "reloader()");
//     }else{
//         var aLink = document.createElement("a");
//         aLink.setAttribute("onclick", "loadFolder("+parentId+")");
//     }
//     var span = document.createElement("span");
//     span.classList.add("corner");
//
//     var divIcon = document.createElement("div");
//     divIcon.classList.add("icon");
//
//     var iconI = document.createElement("i");
//     iconI.classList.add("fa");
//     if(parentId == null || parentId == 0) {
//         iconI.classList.add("fa-home");
//         var myText = document.createTextNode("Home Folder");
//     }else{
//         iconI.classList.add("fa-level-up");
//         var myText = document.createTextNode("Go Back");
//     }
//     var br = document.createElement("br");
//
//     var small = document.createElement("small");
//     if(parentId == null || parentId == 0) {
//         var smallText = document.createTextNode("Back To Root Folder");
//     }else{
//         var smallText = document.createTextNode("Go Back One Folder");
//     }
//     small.appendChild(smallText);
//
//     var divName = document.createElement("div");
//     divName.classList.add("file-name");
//     divName.appendChild(myText);
//     divName.appendChild(br);
//     divName.appendChild(small);
//
//     aLink.appendChild(span);
//
//     divIcon.appendChild(iconI);
//
//     aLink.appendChild(divIcon);
//     aLink.appendChild(divName);
//
//     divFile.appendChild(aLink);
//
//     divFileBox.appendChild(divFile);
//
//
//     itemsContainer.append(divFileBox);
//
//     $("#root"+parentId).droppable({
//         hoverClass: "ui-state-hover",
//         drop: function(){
//             drop(parentId);
//         },
//         greedy: false
//     });
//
//     setFolders(data,folderId);
// }

// $.contextMenu({
//     selector: '.folderMenu',
//     callback: function(key, options) {
//
//         var myID = this[0].id;
//         var r = /\d+/g;
//         myID = myID.match(r);
//
//         if(key == "Delete"){
//
//             removeDir(myID[0]);
//
//         }
//     },
//     items: {
//         "Delete": {name: "Delete", icon: "fa-remove"},
//     }
// });

// function removeFiles(){
//     swal({
//         title: "Are you sure?",
//         text: "Once deleted, you will not be able to recover this files!",
//         icon: "warning",
//         dangerMode: true,
//         buttons: true,
//     }).then((isConfirm)=>{
//         if (isConfirm) {
//             var files = JSON.stringify(filesCheck);
//             var strUrl = BASE_URL+'/console/actions/user/removeFile.php', strReturn = "";
//             jQuery.ajax({
//                 url: strUrl,
//                 method: "POST",
//                 data: {
//                     files: files
//                 },
//                 success: function (html) {
//                     strReturn = html;
//                 },
//                 async: true,
//             }).done(function (data) {
//
//                 filesCheck = [];
//                 filesChecked = 0;
//                 reloader();
//
//             });
//         }
//     });
//
// }

// function getFolderFolders(folderID){
//
//     var strUrl = BASE_URL+'/console/actions/user/getUserFolders.php', strReturn = "";
//     jQuery.ajax({
//         url: strUrl,
//         method: "POST",
//         data:{
//             folderID: folderID
//         },
//         success: function (html) {
//             strReturn = html;
//         },
//         async: true
//     }).done(function (data) {
//         data = JSON.parse(data);
//         parentFolderId = data.parentFolderId;
//         // setTreeView(parentFolderId,data.parentFolder.folder_name);
//         if(folderID > 0){
//             // setToRoot(parentFolderId,data.folders,folderID);
//             // $("#root"+parentFolderId).droppable({
//             //     drop: function(){
//             //         drop(parentFolderId);
//             //     }
//             // });
//
//         }else{
//             setFolders(data.folders,folderID);
//         }
//     });
// }

// function setFolders(data,folderID){
//
//     var itemsContainer = $("#items-container");
//
//     for(var i=0;i<data.length;i++){
//
//         // setting Folders Data
//         var divFileBox = document.createElement("div");
//         divFileBox.classList.add("file-box");
//         divFileBox.classList.add("folderMenu");
//         divFileBox.setAttribute("id","folder"+data[i]['folder_id']);
//         // make folder dropable with the file ID
//         divFileBox.setAttribute("ondrop","drop("+data[i]['folder_id']+")");
//         divFileBox.setAttribute("ondragover","allowDrop(event)");
//
//         var divFile = document.createElement("div");
//         divFile.classList.add("file");
//
//         var aLink = document.createElement("a");
//         aLink.setAttribute("onclick","loadFolder("+data[i]['folder_id']+")");
//
//         var divIcon = document.createElement("div");
//         divIcon.classList.add("icon");
//
//         var iconI = document.createElement("i");
//         iconI.classList.add("fa");
//         iconI.classList.add("fa-folder");
//
//         var myText = document.createTextNode(data[i]['folder_name']);
//         var br = document.createElement("br");
//
//         var small = document.createElement("small");
//         var smallText = document.createTextNode("Added: "+data[i]["create_date"]);
//         small.appendChild(smallText);
//
//         var divName = document.createElement("div");
//         divName.classList.add("file-name");
//         divName.appendChild(myText);
//         divName.appendChild(br);
//         divName.appendChild(small);
//
//
//         divIcon.appendChild(iconI);
//
//         aLink.appendChild(divIcon);
//         aLink.appendChild(divName);
//
//         divFile.appendChild(aLink);
//
//         divFileBox.appendChild(divFile);
//
//
//         itemsContainer.append(divFileBox);
//         $("#folder"+data[i]['folder_id']).droppable({
//             hoverClass: "ui-state-hover",
//             ondrop: function(){
//                 drop(folderID);
//             },
//             greedy: false
//         });
//     }
//     getFolderFiles(folderID);
//
// }

// function checker(check,value) {
//
//     if(check == true){
//
//         filesChecked++;
//         filesCheck.push(value);
//
//     }else{
//
//         filesCheck = filesCheck.filter(function(item){
//
//             return item !== value;
//
//         });
//
//         filesChecked--;
//
//     }
//
//     if(filesChecked > 0 && filesCheck.length > 0){
//
//         $(".myMenu").show();
//
//     }else{
//
//         $(".myMenu").hide();
//
//     }
//
// }