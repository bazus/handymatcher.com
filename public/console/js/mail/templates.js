function addslashes( str ) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}
function previewPreDefinedTemplate(id){
    switch (id) {
        case 0:
            window.location = BASE_URL+"/console/categories/mail/templater.php?type=0";
            break;
        default:
            window.location = BASE_URL+"/console/categories/mail/templater.php?type=0&template="+id;
            break;
    }
}
function setDelete(id,title){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover the Template '"+title+"' later.",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if(isConfirm) {
            var strUrl = BASE_URL + '/console/actions/mail/preEmails/deletePreEmail.php';

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    id: id
                },
                async: true
            }).done(function (data) {

                try {

                    data = JSON.parse(data);
                    if (data == true) {

                        getData();

                    }

                } catch (e) {


                }

            });
        }
    });
}

function getData(){
    if (!document.getElementById("folder-"+selectedFolder)){
        selectedFolder = null;
    }
    var strUrl = BASE_URL+'/console/actions/mail/preEmails/getPreEmails.php';

    jQuery.ajax({
        url: strUrl,
        async: true,
        method: "post",
        data: {folderId:selectedFolder}
    }).done(function (data) {

        try {

            data = JSON.parse(data);

            document.getElementById("myTemplates").innerHTML = "";
            if (data.length > 0){
                for (var i = 0;i<data.length;i++){
                    setData(data[i]);
                }
            }
            var folder = $("#folder-"+selectedFolder+" .templateFolderTop i");
            folder.removeClass("fa-folder");
            folder.addClass("fa-folder-open");
            $("#activeFolder").html("Current folder: <b>"+$("#folder-"+selectedFolder+" .templateFolderName").text()+"</b>");

        }catch (e) {

        }

    });
}

function getFoldersData(){
    var strUrl = BASE_URL+'/console/actions/mail/preEmails/getFolders.php';

    jQuery.ajax({
        url: strUrl,
        async: false
    }).done(function (data) {
        try {
            data = JSON.parse(data);

            document.getElementById("myFolders").innerHTML = "";

            var rootFolder = {"name":"Home","id":null};

            setFolders(rootFolder,"rootFolder");

            if (data.length > 0){
                for (var i = 0;i<data.length;i++){
                    setFolders(data[i]);
                }
            }
        }catch (e) {}
    });
}

function setFolders(data,customClass){
    var container = document.getElementById("myFolders");
    var div = document.createElement("div");
    div.setAttribute("onclick","showFolder("+data.id+")");
    div.title = data.name;
    div.id = "folder-"+data.id;
    div.classList.add("templateFolder");

    var upper = document.createElement("div");
    upper.classList.add("templateFolderTop");

    if (customClass){
        upper.classList.add(customClass);
    }

    var i = document.createElement("i");
    i.classList.add("fa");
    i.classList.add("fa-folder");

    upper.appendChild(i);
    div.appendChild(upper);

    var folderName = document.createElement("div");
    folderName.classList.add("templateFolderName");

    var folderNameText = document.createTextNode(data.name);

    folderName.appendChild(folderNameText);
    div.appendChild(folderName);

    container.appendChild(div);

}

function showFolder(id){
    var folder = $("#folder-"+selectedFolder+" .templateFolderTop i");

    folder.addClass("fa-folder");
    folder.removeClass("fa-folder-open");

    selectedFolder = id;
    getData();
}

function htmlDecode(input) {

    var doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;

}

function setData(data){

    var container = document.getElementById("myTemplates");

    var item = document.createElement("div");
    item.classList.add("myTemplateItem");

    var image = document.createElement("div");
    image.classList.add("myTemplateImage");
    image.setAttribute("style","height: 182px;width: 150px;background:white;");


    var img = document.createElement("iframe");
    img.className = "iframePreview";
    img.src = BASE_URL+"/console/actions/mail/preEmails/getPreEmailsImage.php?id="+data.id;


    var buttonsContainer = document.createElement("div");
    buttonsContainer.classList.add("buttonsContainer");
    //buttonsContainer.style.opacity = '0';

    var buttonEdit = document.createElement("button");
    buttonEdit.classList.add("btn");
    buttonEdit.classList.add("btn-warning");
    buttonEdit.classList.add("middleButton");
    buttonEdit.setAttribute("onclick","editTemplate("+data.id+")");
    var buttonEditText = document.createTextNode("Edit");
    buttonEdit.appendChild(buttonEditText);
    buttonsContainer.appendChild(buttonEdit);

    var buttonPreview = document.createElement("button");
    buttonPreview.classList.add("btn");
    buttonPreview.classList.add("btn-info");
    buttonPreview.classList.add("middleButton");
    buttonPreview.setAttribute("onclick","previewTemplate("+data.id+")");
    var buttonPreviewText = document.createTextNode("Preview");
    buttonPreview.appendChild(buttonPreviewText);
    buttonsContainer.appendChild(buttonPreview);

    image.appendChild(buttonsContainer);
    image.appendChild(img);

    item.appendChild(image);

    var title = document.createElement("div");
    title.classList.add("myTemplateTitle");
    title.title = data.title;
    title.setAttribute("onclick","previewTemplate("+data.id+")");

    var titleText = document.createTextNode(data.title);

    title.appendChild(titleText);
    item.appendChild(title);

    var department = document.createElement("div");
    department.classList.add("myTemplateDepartment");
    if (data.department == null || data.department == ""){
        var departmentText = document.createTextNode("Unset Department (Global)");
        department.classList.add("text-muted");
    }else{
        department.classList.add("font-weight-bold");
        var departmentText = document.createTextNode(data.dTitle);
    }
    department.appendChild(departmentText);

    var div1 = document.createElement('div');
    div1.className = 'dropdown dropdown-toggle myTemplateOptions dropdowner';

    div1.innerHTML= " <i type=\"button\" class=\"fa fa-gear \" data-toggle=\"dropdown\"></i>";

    var div2 = document.createElement(('div'));
    div2.className = 'dropdown-menu';
    div2.setAttribute("style","z-index: 99999;");

    var li1 = document.createElement("li");
    var li1a = document.createElement("a");
    li1a.setAttribute("onclick","previewTemplate("+data.id+")");
    var li1aText = document.createTextNode("Preview Template");
    li1a.appendChild(li1aText);
    li1.appendChild(li1a);
    div2.appendChild(li1);

    var li5 = document.createElement("li");
    var li5a = document.createElement("a");
    li5a.setAttribute("onclick","editDetails("+data.id+",'"+data.title+"','"+data.subject+"','"+data.department+"')");
    var li5aText = document.createTextNode("Edit Details");
    li5a.appendChild(li5aText);
    li5.appendChild(li5a);
    div2.appendChild(li5);

    var li3 = document.createElement("li");
    var li3a = document.createElement("a");
    li3a.setAttribute("onclick","setDelete("+data.id+",'"+data.title+"')");
    var li3aText = document.createTextNode("Delete Template");
    li3a.appendChild(li3aText);
    li3.appendChild(li3a);
    div2.appendChild(li3);

    var li4 = document.createElement("li");
    var li4a = document.createElement("a");
    li4a.setAttribute("onclick","changeTemplateFolder("+data.id+",'"+addslashes(data.title)+"')");
    var li4aText = document.createTextNode("Change Template Folder");
    li4a.appendChild(li4aText);
    li4.appendChild(li4a);
    div2.appendChild(li4);

    var li6 = document.createElement("li");
    var li6a = document.createElement("a");
    li6a.setAttribute("onclick","duplicateTemplate("+data.id+")");
    var li6aText = document.createTextNode("Duplicate Template");
    li6a.appendChild(li6aText);
    li6.appendChild(li6a);
    div2.appendChild(li6);

    div1.append(div2);
    department.appendChild(div1);
    item.appendChild(department);
    container.appendChild(item);

}

function changeTemplateFolder(id,name){
    // showCustomModal('categories/iframes/preEmails/changeFolder.php?id='+id+"&name="+name);
    var content = document.createElement("div");

    var select = document.createElement("select");
    select.id = "changeFolder";
    select.classList.add("form-control");
    select.setAttribute("name","changeFolder");

    var option = document.createElement("option");
    option.value = "";
    var optionText = document.createTextNode("Select Template Folder");
    option.appendChild(optionText);
    select.appendChild(option);

    var option = document.createElement("option");
    option.value = null;
    var optionText = document.createTextNode("Home Folder");
    option.appendChild(optionText);
    select.appendChild(option);


    var strUrl = BASE_URL+'/console/actions/mail/preEmails/getFolders.php';

    jQuery.ajax({
        url: strUrl,
        async: false
    }).done(function (data) {
        try {
            data = JSON.parse(data);

            if (data.length > 0){
                for (var i = 0;i<data.length;i++){
                    var option = document.createElement("option");
                    option.value = data[i].id;
                    var optionText = document.createTextNode(data[i].name);
                    option.appendChild(optionText);
                    select.appendChild(option);
                }
            }
        }catch (e) {}
    });

    content.appendChild(select);

    swal({
        title: "Change \""+name+"\" Template Folder\n",
        content: content,
        buttons: {
            save: {
                text: "Move to folder",
                value: "save"
            },
            cancel: true
        },
        icon: false,
    }).then(function(value){
        if (value == "save"){
            var folder = $("#changeFolder").val();

            if (folder && folder != "") {

                var strUrl = BASE_URL + '/console/actions/mail/preEmails/changeFolder.php', strReturn = "";

                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        folder: folder,
                        id: id
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: false
                }).done(function (data) {
                    $(document).ready(function () {
                        selectedFolder = folder;
                        getData();
                    });
                });
            }else{
                changeTemplateFolder(id,name);
            }
        }
    });
}
function editTemplate(id){
    window.location = BASE_URL+"/console/categories/mail/templater.php?id="+id+"&type=1";
}
function previewTemplate(id){
    window.open(BASE_URL+"/console/categories/mail/previewTemplate.php?id="+id,"_blank");
}

function duplicateTemplate(id){
    window.open(BASE_URL+"/console/categories/mail/templater.php?dup="+id);
}



function editDetails(id,title,subject,department){
    var swalContent = createSwalContent(title,subject,department);
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    swal({
        title: "Select Template Name & Subject",
        content: swalContent,
        buttons: {
            save: {
                text: "Save",
                value: "save"
            },
            cancel: true
        },
        icon: false,
    }).then(function(value){
        if(value === "save"){
            var strUrl = BASE_URL + '/console/actions/mail/preEmails/updatePreEmailsInfo.php';

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    id:id,
                    title:document.getElementById("editTitle").value,
                    subject:document.getElementById("editSubject").value,
                    department:document.getElementById("editDep").value
                },
                async: true
            }).done(function (data) {

                try {

                    data = JSON.parse(data);

                    if (data == true) {
                        toastr.success("Template '"+document.getElementById("editTitle").value+"' Updated");
                        getData();
                    }else{
                        toastr.error("Error Saving Template");
                    }


                } catch (e) {

                    toastr.error("Error Saving Template");

                }

            });
        }
    });
}

function addNewFolder() {
    var content = document.createElement("div");

    var input = document.createElement("input");
    input.id = "folderName";
    input.classList.add("form-control");
    input.setAttribute("type","text");
    input.setAttribute("name","folderName");
    input.setAttribute("placeholder","New folder name");
    content.appendChild(input);

    swal({
        title: "Add new folder",
        content: content,
        buttons: {
            save: {
                text: "Create folder",
                value: "save"
            },
            cancel: true
        },
        icon: false,
    }).then(function(value){
        if (value == "save"){
            var folderName = $("#folderName").val();
            if (folderName != "") {
                var strUrl = BASE_URL + '/console/actions/mail/preEmails/createFolder.php', strReturn = "";

                jQuery.ajax({
                    url: strUrl,
                    method: "POST",
                    data: {
                        name: folderName
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: false
                }).done(function (data) {

                    $(document).ready(function () {
                        getFoldersData();
                    });

                });
            }else{
                swal("Folder name can\'t be empty").then(function(x){
                    addNewFolder();
                });
            }
        }
    });
}

// Init
$(document).ready(function () {
    getFoldersData();
    getData();
});