
$(document).ready(function () {
    l = $('.ladda-button-demo').ladda();

    $('#dateRangeOutgoingEmails span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#dateRangeOutgoingEmails').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment().startOf('month'),
        endDate: moment(),
        minDate: '01/01/2018',
        maxDate: '12/31/2020',
        dateLimit: { days: 365 },
        showDropdowns: true,
        showWeekNumbers: false,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last Week': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Go',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function(start, end, label) {
        theLabel = label;
        $(".spanner").each(function(){
            if (label == "Custom"){
                $(this).text("Custom Date");
            } else{
                $(this).text(label);
            }
        });
        if (label != "Custom") {
            $('#dateRangeReports span').html(label);
        }else{
            $('#dateRangeReports span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        theStartDate = start.format('DD-MM-YYYY');
        theEndDate = end.format('DD-MM-YYYY');

        getEmails();
    });
    theStartDate = moment().startOf('month').format('DD-MM-YYYY');
    theEndDate = moment().format('DD-MM-YYYY');
    getEmails();

});



function getEmails(){

    var container = document.getElementById("emailsContainer");
    container.innerHTML = "<tr><td class=\"text-center font-bold\" colspan=\"6\">Loading...</td></tr>";

    if (document.getElementById("userSentSelect")){
        var userId = document.getElementById("userSentSelect").value;
    }else{
        var userId = "";
    }
    var strUrl = BASE_URL+'/console/actions/mail/getMails.php';

    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            userId:userId,
            startDate:theStartDate,
            endDate:theEndDate
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            container.innerHTML = "";

            if($('.dataTable').length){
                var table = $('.dataTables-outgoingEmails').DataTable();
                table.clear().destroy();
            }

            if (data['emails'].length > 0){
                for (var i=0;i<data['emails'].length;i++){
                    setEmails(data['emails'][i],container);
                }
                startTableSort();
            }else{
                setNoRows(container);
            }
        }catch (e) {
        }
    });

}
function setNoRows(container){

    var tr = document.createElement("tr");
    var td = document.createElement("td");
    td.setAttribute("colspan",6);
    td.style.textAlign = "center";
    var tdText = document.createTextNode("No Emails To Show");
    td.appendChild(tdText);
    tr.appendChild(td);
    container.appendChild(tr);
}

function setEmails(data,container){

    var tr = document.createElement("tr");
    if(data.leadId != false && data.leadId != "" && data.leadId != null) {
        // Open the lead
        tr.setAttribute("onclick", "window.open(BASE_URL+'/console/categories/leads/lead.php?leadId=" + data.leadId+"')");
    }else{
        // Open the email
        tr.setAttribute("onclick", "window.open(BASE_URL+'/console/categories/mail/viewEmail.php?id=" + data.id + "&userSent=" + data.userSent + "')");
    }
    tr.style.cursor = "pointer";
    tr.classList.add("byUser-"+data.userSent);

    var td = document.createElement("td");
    if(data.userSentData == null){
        td.innerHTML = "<span class='text-success'>System</span>";
    }else{
        td.innerHTML = "<span class='text-navy'>"+data.userSentData.fullName+"</span>";
    }
    tr.appendChild(td);

    var td = document.createElement("td");
    td.innerHTML = data.fromEmail;
    tr.appendChild(td);

    var td = document.createElement("td");
    td.innerHTML = data.toEmail;
    tr.appendChild(td);

    var td = document.createElement("td");
    // var tdText = document.createTextNode();
    td.innerHTML = data.subject;
    tr.appendChild(td);

    var td = document.createElement("td");
    td.title = data.sentDate;

    var tdText = document.createTextNode(data.sentDateConverted);

    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    td.classList.add("mail-status");
    if (data.status == 1) {

        if(data.didOpen == "1"){
            td.classList.add('text-navy');
            td.title = data.openDate;

            if(data.openDateText == undefined){
                var tdText = document.createTextNode("Opened");
            }else{
                var tdText = document.createTextNode(data.openDateText);
            }

        }else{
            var tdText = document.createTextNode("Not opened");
        }
    }else{
        td.classList.add('text-danger');
        var tdText = document.createTextNode("Not Sent");
    }

    td.appendChild(tdText);
    tr.appendChild(td);

    container.appendChild(tr);
}
$("#userSentSelect").on("change",function(){
    getEmails();
});
function startTableSort() {
    $('.dataTables_filter').remove();
    var dTable = $('.dataTables-outgoingEmails').DataTable({
        "searching": true,
        "responsive": false,
        "autoWidth": false,
        "fixedColumns": true,
        "aaSorting": [],
        "oLanguage": {
            "sInfo": "",
            "sSearch": "<span></span> _INPUT_",
            "searchPlaceholder": "Search"
        },
        "bFilter": false,
        "sScrollX": "100%",
        "pageLength": 20,
        "lengthChange": false,
        "columnDefs": [ {
            "targets": ["th-sentBy","th-from","th-to","th-subject","th-dateSent","th-opened"],
            "orderable": false,
            "visible": true
        } ]
    });
    var info = dTable.page.info();
    if(info.pages == 1){
        $('.dataTables_paginate').hide();
    }
    $('.dataTables_length').addClass('bs-select');
    $('thead').css("background-color","rgb(245, 245, 246);");

    $('div.dataTables_filter').appendTo( $('#searchBarContainer') );
    $('div.dataTables_filter input').attr({placeholder: "Search"});

}