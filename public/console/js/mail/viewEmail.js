$(document).ready(function () {
    l = $('.ladda-button-demo').ladda();
});


var currentPage = 0;

function getEmails(action){
    switch (action) {
        case "+":
            currentPage++;
            break;
        case "-":
            if (currentPage > 0){
                currentPage--;
            }
            break;
    }
    var strUrl = BASE_URL+'/console/actions/mail/getMails.php';

    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            page:currentPage
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            var container = document.getElementById("emailsContainer");
            container.innerHTML = "";
            if (data['totalEmails'] > 0){
                for (var i=0;i<data['emails'].length;i++){
                    setEmails(data['emails'][i],container);
                }
            }else{
                setNoRows(container);
            }
            var first = currentPage*25+1;
            var last = (first-1)+data['emails'].length;
            document.getElementById("totalEmails").innerHTML = "Showing <b>"+first+"-"+last+"</b> Emails Of <b>"+data['totalEmails']+"</b>";
            document.getElementById("totalEmails").style.display = "inline-table";

            if (currentPage > 0){
                document.getElementById("goBack").disabled = false;
            } else{
                document.getElementById("goBack").disabled = true;
            }
            if (last < data['totalEmails']) {
                document.getElementById("goForward").disabled = false;
            }else{
                document.getElementById("goForward").disabled = true;
            }

        }catch (e) {

        }
    });

}
function setNoRows(container){
    var tr = document.createElement("tr");
    var td = document.createElement("td");
    td.setAttribute("colspan",4);
    td.style.textAlign = "center";
    var tdText = document.createTextNode("No Emails To Show");
    td.appendChild(tdText);
    tr.appendChild(td);
    container.appendChild(tr);
}
function setEmails(data,container){

    var tr = document.createElement("tr");
    var td = document.createElement("td");
    td.style.overflowX = "scroll";
    td.classList.add("mail-ontact");
    var tdText1 = document.createTextNode(data.fromEmail+" ");
    var b = document.createElement("b");
    var i = document.createElement("i");
    i.classList.add("fa");
    i.classList.add("fa-long-arrow-right");
    b.appendChild(i);
    var tdText2 = document.createTextNode(" "+data.toEmail);
    td.appendChild(tdText1);
    td.appendChild(b);
    td.appendChild(tdText2);
    tr.appendChild(td);

    var td = document.createElement("td");
    td.classList.add("mail-subject");
    var tdText = document.createTextNode(data.subject);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    td.classList.add("mail-date");
    var tdText = document.createTextNode(data.sentDate);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var button = document.createElement("button");
    button.classList.add("btn");
    button.classList.add("btn-default");
    button.classList.add("btn-block");
    button.setAttribute("onclick","showCustomModal('categories/iframes/showEmail.php?id="+data.id+"')");
    var buttonText = document.createTextNode("View Email");
    button.appendChild(buttonText);
    td.appendChild(button);
    tr.appendChild(td);

    container.appendChild(tr);

}