$(function() {
    /*

    // I have no Idea why this code is here and what it does but am too afraid to delete it (16.2.20)(niv)

    // define dropdown
    $.FroalaEditor.DefineIcon('insertDropDown', {NAME: 'address-card-o'});

    // define dropdown buttons and actions
    $.FroalaEditor.RegisterCommand('insertDropDown', {
        title: 'Insert Data',
        type: 'dropdown',
        focus: false,
        undo: false,
        align: "right",
        options: {
            '{leadfirstname}': 'Insert Lead First Name',
            '{leadlastname}': 'Insert Lead Last Name',
            '{leademail}': 'Insert Lead Email',
            '{leadphone}': 'Insert Lead Phone',
            '{leadfromzip}': 'Insert Lead From Zip',
            '{leadtozip}': 'Insert Lead To Zip',
            '{leadupdateinventory}': "Insert 'Update Inventory' Link",
            '{leadjobnumber}': "Insert Job Number",
            '{leadmovedate}': "Insert Lead Move Date",
            '{companyname}': "Insert Company Name",
            '{companyphone}': "Insert Company Phone",
            '{companyaddress}': "Insert Company Address",
            '{companywebsite}': "Insert Company Website",
            '{companyusdot}': "Insert Company USDOT",
            '{companymcc}': "Insert Company MCC"
        },
        callback: function (cmd, val) {
            this.html.insert(val);
        }
    });
    // init froala
    $('textarea').froalaEditor({
        key: 'nD5E2E1A1wG1G1B2C1B1D7C4E1E4H3jcpB-13tE-11hjqmkmgbg1vH4dgd==',
        height: 450,
        heightMax: 1500,
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', '|', 'fontFamily', 'fontSize', 'colors', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote','insertDropDown', '-', 'insertLink', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo'],
    })

    */
});
function getEmailsAccountData() {
    var strUrl = BASE_URL+'/console/actions/mail/getEmailsAccountData.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        try{
            data = JSON.parse(data);
            document.getElementById("mailSettingsAccounts").innerHTML = "";
            if (data.length > 0){
                for (var i = 0;i<data.length;i++){
                    setEmailAccount(data[i],data.length);
                }
            }else{
                $("#mailSettingsAccounts").html("<tr style=\"background-color: #fff;\"><td style=\"text-align: center\" colspan=\"5\">No Accounts connected</td></tr>");
            }

        }catch (e){

        }

    });
}

function setEmailAccount(data,length) {

    var container = document.getElementById("mailSettingsAccounts");

    var tr = document.createElement("tr");

    // Type
    var td = document.createElement("td");
    td.style.verticalAlign = "middle";

    var emailAddressSpan = document.createElement("span");
    emailAddressSpan.setAttribute("style","font-weight: 600;color: #676a6c;");
    emailAddressSpan.innerHTML = '<a href="#">'+data.email+'</a>';

    var emailTypeSmall = document.createElement("small");
    emailTypeSmall.setAttribute("style","display:block");
    if (data.accountType == 0){

        if(data.requiredSSL == 1){
            emailTypeSmall.innerHTML = "Custom - SSL";
        }else{
            emailTypeSmall.innerHTML = "Custom";
        }

    }else if (data.accountType == 1) {
        emailTypeSmall.innerHTML = "Google";
    }else if (data.accountType == 2) {
        emailTypeSmall.innerHTML = "Yahoo";
    }else if (data.accountType == 3) {
        emailTypeSmall.innerHTML = "Office";
    }else if (data.accountType == 4) {
        emailTypeSmall.innerHTML = "Gmail";
    }else if (data.accountType == 5) {
        emailTypeSmall.innerHTML = "Office 365";
    }


    td.appendChild(emailAddressSpan);

    var div = document.createElement("div");
    div.setAttribute("style","float:right");

    if (length > 1){
        var span = document.createElement("span");

        if (data.isDefault){
            span.className = "label label-info";
            span.innerHTML = 'Default';
            div.appendChild(span);
        }
    }


    if (data.isActive != 1){
        var span = document.createElement("span");
        span.className = "label label-warning";
        span.innerHTML = "Disabled";
        span.style.marginLeft = "3px";
        div.appendChild(span);
    }
    td.appendChild(div);
    td.appendChild(emailTypeSmall);

    tr.appendChild(td);

    // Private Account
    var td = document.createElement("td");
    td.style.verticalAlign = "middle";

    var small = document.createElement("small");
    small.classList.add("text-navy");

    var div = document.createElement("div");
    div.classList.add("switch");
    div.setAttribute("align","center");


    var innerDiv = document.createElement("div");
    innerDiv.classList.add("onoffswitch");

    var input = document.createElement("input");
    input.id = "isPrivateAccount"+data.id;
    input.classList.add("onoffswitch-checkbox");
    input.setAttribute("type","checkbox");
    input.setAttribute("name","isPrivateAccount");
    input.setAttribute("onchange","togglePrivateAccount(this,"+data.id+")");
    if (data.isPrivate == 1){
        input.setAttribute("checked",true);
    }

    var label = document.createElement("label");
    label.classList.add("onoffswitch-label");
    label.setAttribute("for","isPrivateAccount"+data.id);

    var span = document.createElement("span");
    span.classList.add("onoffswitch-inner");
    span.style.textAlign = "left";

    label.appendChild(span);

    var span = document.createElement("span");
    span.classList.add("onoffswitch-switch");

    label.appendChild(span);

    innerDiv.appendChild(input);
    innerDiv.appendChild(label);

    div.appendChild(innerDiv);
    small.appendChild(div);
    td.appendChild(small);
    tr.appendChild(td);

    // Send test email
    var td = document.createElement("td");
    td.setAttribute("style","text-align:center");

    if (data.accountType == 4 && data.isAuthorized == false) {
        var tdButton = document.createElement("button");
        tdButton.setAttribute("onclick","window.location = '"+BASE_URL+"/console/actions/API/authorizeGoogleAPI.php?app=gmail'");
        tdButton.classList.add("btn");
        tdButton.classList.add("btn-primary");
        tdButton.classList.add("btn-sm");
        var tdButtonText = document.createTextNode("Authorize Account");

        tdButton.appendChild(tdButtonText);
        td.appendChild(tdButton);

    }else if(data.accountType == 5 && data.isAuthorized == false){
        var tdButton = document.createElement("button");
        tdButton.setAttribute("onclick","window.location = '"+BASE_URL+"/console/actions/API/authorizeMicrosoftAPI.php'");
        tdButton.classList.add("btn");
        tdButton.classList.add("btn-primary");
        tdButton.classList.add("btn-sm");
        var tdButtonText = document.createTextNode("Authorize Account");

        tdButton.appendChild(tdButtonText);
        td.appendChild(tdButton);
    }else{
        var button = document.createElement("button");
        button.id = "ladda-"+data.id;
        button.classList.add("ladda-button");
        button.classList.add("btn");
        button.classList.add("btn-white");
        button.classList.add("btn-sm");
        button.setAttribute("onclick","testMailConnection("+data.id+")");
        button.setAttribute("data-spinner-color","black");
        button.setAttribute("data-style","zoom-out");

        var buttonText = document.createTextNode("Send Test Email");
        button.appendChild(buttonText);

        td.appendChild(button);
    }
    tr.appendChild(td);

    // Settings
    var td = document.createElement("td");
    td.setAttribute("style","text-align:center");

    var div = document.createElement("div");
    div.className = "dropdown settings-dropdown";

    var button = document.createElement("button");
    button.className = "btn btn-default dropdown-toggle";
    button.setAttribute("type","button");
    button.setAttribute("data-toggle","dropdown");
    button.setAttribute("aria-haspopup","true");
    button.setAttribute("aria-expanded","true");
    button.id = "dropdownMenu"+data.id;
    button.innerHTML = "Settings ";

    var buttonSpan = document.createElement("span");
    buttonSpan.className = "caret";


    var ul = document.createElement("ul");
    ul.className = "dropdown-menu";
    ul.setAttribute("aria-labelledby","dropdownMenu"+data.id);

    if (length > 1 && !data.isDefault) {
        ul.innerHTML += '<li><a href="#" onclick="setDefault('+data.id+')">Set Default</a></li>';
    }

    if (data.accountType != 4 && data.accountType != 5) {
        ul.innerHTML += '<li><a href="#" onclick="updateMailAccountPassword('+data.id+')">Update Password</a></li>';
    }

    if (data.isActive == 1){
        ul.innerHTML += '<li><a href="#" onclick="disableAccount('+data.id+')">Disable Account</a></li>';
    }else{
        ul.innerHTML += '<li><a href="#" onclick="reActivateAccount('+data.id+')">Re-activate</a></li>';
    }
    ul.innerHTML += '<li role="separator" class="divider"></li>';
    ul.innerHTML += '<li><a href="#" class="btn-danger" style="color:#fff;" onclick="deleteAccount('+data.id+')">Delete Account</a></li>';

    button.appendChild(buttonSpan);
    div.appendChild(button);
    div.appendChild(ul);

    td.appendChild(div);
    tr.appendChild(td);


    container.appendChild(tr);
}

function disableAccount(accountId){
    swal({
        title: "Are you sure?",
        text: "This will set your account as in-active",
        icon: "warning",
        dangerMode: true,
        buttons: {
            skip:{
                text:"Cancel",
                className:"btn-default",
                value:"no",
            },
            confirm:{
                text:"Disable",
                className:"btn-warning",
                value:"yes",
            },

        }
    }).then(function(value){
        if( value == "yes"){
            var strUrl = BASE_URL+'/console/actions/mail/disableAccount.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    accountId: accountId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                getEmailsAccountData();
            });
        }});
}


function reActivateAccount(accountId){
    swal({
        title: "Are you sure?",
        text: "This will set your account as active",
        icon: "warning",
        dangerMode: true,
        buttons: {
            skip:{
                text:"Cancel",
                className:"btn-default",
                value:"no",
            },
            confirm:{
                text:"Reactivate",
                className:"btn-warning",
                value:"yes",
            },

        }
    }).then(function(value){
        if( value == "yes")
        {
            var strUrl = BASE_URL + '/console/actions/mail/reActivateAccount.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    accountId: accountId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try {
                    data = JSON.parse(data);

                    if (data == true) {
                        toastr.success('Account is now active', 'Success');
                    } else {
                        toastr.error('Your changes were not saved. Please try again later.', 'Oops');
                    }
                } catch (e) {
                    toastr.error('Your changes were not saved. Please try again later.', 'Oops');
                }

                getEmailsAccountData();

            });
        }
    });
}



function deleteAccount(accountId){
    swal({
        title: "Are you sure?",
        text: "This will delete your account",
        icon: "warning",
        dangerMode: true,
        buttons: {
            skip:{
                text:"Cancel",
                className:"btn-default",
                value:"no",
            },
            confirm:{
                text:"Delete",
                className:"btn-danger",
                value:"yes",
            },

        }
    }).then(function(value){
        if( value == "yes")
        {
            var strUrl = BASE_URL + '/console/actions/mail/deleteAccount.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    accountId: accountId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try {
                    data = JSON.parse(data);

                    if (data == true) {
                        toastr.success('Account is now deleted', 'Success');
                    } else {
                        toastr.error('Your changes were not saved. Please try again later.', 'Oops');
                    }
                } catch (e) {
                    toastr.error('Your changes were not saved. Please try again later.', 'Oops');
                }

                getEmailsAccountData();

            });
        }
    });
}

function testMailConnection(accountId) {
    $('#cusmod').hide();

    var l = $( '#ladda-'+accountId ).ladda();
    l.ladda( 'start' );

    var content = document.createElement("div");
    content.innerHTML = "<img src='https://demos.laraget.com/images/loading2.gif' style='width: 150px;' />";
    window.swal({
        title: "Sending test email..",
        text: "Please wait",
        content: content,
        button: false,
        closeOnClickOutside: false,
        closeOnEsc: false
    });

    var strUrl = BASE_URL+'/console/actions/mail/testAccountConnection.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            accountId: accountId
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        l.ladda('stop');

        window.swal.close()

        try{
            data = JSON.parse(data);

            if(data.status == true){

                swal({title:"Success!", text:"Your account is now connected",icon: "success",
                    buttons: {
                        ok:"Ok"
                    }
                });
            }else{
                if (data.didLimitReached == true){
                    limitReached('email');
                } else{
                    var content = document.createElement("div");
                    content.innerHTML = "Could not make a connection.<br>Please Check your settings/password and try again.";

                    swal({
                        content:content,
                        icon: "warning",
                        buttons: {
                            ok:"Ok",
                            changePassword:"Change Password"
                        }
                    }).then(function(value){
                        if(value == "changePassword"){
                            updateMailAccountPassword(accountId);
                        }
                    });
                }
            }
        }catch (e){
            var content = document.createElement("div");
            content.innerHTML = "Could not make a connection.<br>Please Check your settings/password and try again.";

            swal({
                content:content,
                icon: "warning",
                buttons: {
                    ok:"Ok"
                }
            });
        }


    });
}

function updateMailAccountPassword(accountId){
    showCustomModal('categories/mail/updateMailAccountPassword.php?accountId='+accountId);
}

function adjustMailSettingIframeHeight(height){
    document.getElementById("mailSettingIframe").style.height = ((height -  document.getElementById("mailSettingIframe").offsetTop) + "px");
}

function addAccount(){
    var content = document.createElement("iframe");
    content.setAttribute("style","border: none;width: 100%;height: 200px;");
    content.src = BASE_URL+'/console/categories/iframes/mail/setAccount.php';
    content.id = "mailSettingIframe";

    var settings = {
        animation:"fadeInUp",
        //template: "cusmod-blue",
        hideCloseBTN:false,
        content: content
    };
    initCusmod("#cusmod", settings);
}

function setDefault(id){

    var strUrl = BASE_URL+'/console/actions/mail/setDefault.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data: {
            id: id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        getEmailsAccountData();

    });
}

$(document).ready(function () {
    l = $('.ladda-button-demo').ladda();
});

function changeView(view,id){
    switch (view){
        case 1:

            document.getElementById("view1").style.display = "";
            document.getElementById("view2").style.display = "none";
            document.getElementById("view3").style.display = "none";

            var btn = document.getElementById("myBTN");
            btn.innerText = "Add New";
            btn.setAttribute("onclick","changeView(2)");
            btn.classList.add("btn-primary");
            btn.classList.remove("btn-success");

            getData();

            break;
        case 2:

            document.getElementById("view1").style.display = "none";
            document.getElementById("view2").style.display = "";
            document.getElementById("view3").style.display = "none";


            var btn = document.getElementById("myBTN");
            btn.innerText = "Go Back";
            btn.setAttribute("onclick","changeView(1)");
            btn.classList.add("btn-success");
            btn.classList.remove("btn-primary");

            break;

        case 3:

            document.getElementById("view1").style.display = "none";
            document.getElementById("view2").style.display = "none";
            document.getElementById("view3").style.display = "";

            var btn = document.getElementById("myBTN");
            btn.innerText = "Go Back";
            btn.setAttribute("onclick","changeView(1)");
            btn.classList.add("btn-success");
            btn.classList.remove("btn-primary");
            getDataById(id);

            break;
    }
}


function togglePrivateAccount(obj,accountId){
    obj.checked = !obj.checked;

    var strUrl = BASE_URL+'/console/actions/mail/togglePrivateAccount.php';

    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            accountId:accountId
        },
        async: true
    }).done(function (data) {

        try {
            data = JSON.parse(data);
            if(data.status == true){
                if(data.resp == "0"){
                    obj.checked = false;
                }
                if(data.resp == "1"){
                    obj.checked = true;
                }
            }
        }catch (e) {

        }
    });
}

function showMailCenterInfo(){
    swal({
        text: 'This feature allows you to securely link your personal email account to our  system. This will enable you to send emails directly from your email account using your Network Leads software.',
        buttons: {
            skip:{
                text:"Got It",
            }
        }
    });
}
function showPrivateAccountInfo(){
    swal({
        text: "This feature allows you to set mail accounts as private so that they will be inaccessible to other users in your organization",
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}

// Initialize
getEmailsAccountData();