
$('.summernote').summernote({
    toolbar:[
        ['style', ['fontname','fontsize','color','forecolor','backcolor','bold','italic','strikethrough', 'clear']],
        ['font', ['superscript', 'subscript']],
        ['para', ['ul', 'ol', 'paragraph','height']],
        ['groupName',['customLeadTags']],
        ['link', ['linkDialogShow', 'unlink']],
        ['misc',['undo','redo','codeview']]
    ],
    placeholder: "Write Your Message..",
    minHeight:300,
});
var lad = [];
$(document).ready(function () {
    lad[0] = $('.ladda-button0').ladda();
    lad[1] = $('.ladda-button1').ladda();
});

function errorSwal(errorString,l){
    var swalContent = document.createElement("div");
    swalContent.style.color = "red";
    swalContent.style.textAlign = "left";
    swalContent.innerHTML = errorString;

    swal({
        content: swalContent,
        buttons: {
            gotIt:{text:"Got It"}
        },
        dangerMode: true,
    }).then(function(value){
        l.ladda("stop");
        return false;
    });
}

function getData(){
    var strUrl = BASE_URL+'/console/actions/mail/preEmails/getPreEmails.php';

    jQuery.ajax({
        url: strUrl,
        async: true,
        method: "post",
        data: {folderId:selectedFolder}
    }).done(function (data) {

        try {

            data = JSON.parse(data);

            document.getElementById("myTemplates").innerHTML = "";
            if (data.length > 0){
                for (var i = 0;i<data.length;i++){
                    setData(data[i]);
                }
            }else{
                document.getElementById("myTemplates").innerHTML = "<div style='margin-top:25px;' class=\"alert alert-info\">\n" +
                    " No Templates available in this folder, <a class=\"alert-link\" href='"+BASE_URL+"/console/categories/mail/templates.php'>Go Create One</a>.\n" +
                    "</div>";
            }

        }catch (e) {

        }

    });
}
getData();
function htmlDecode(input)
{
    var doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
}

function setData(data){

    var container = document.getElementById("myTemplates");

    var item = document.createElement("div");
    item.classList.add("myTemplateItem");
    item.setAttribute("onclick","setTemplate("+data.id+")");


    var image = document.createElement("div");
    image.classList.add("myTemplateImage");
    image.setAttribute("style","height: 182px;width: 150px;background:white;");


    var img = document.createElement("iframe");
    img.className = "iframePreview";
    img.src = BASE_URL+"/console/actions/mail/preEmails/getPreEmailsImage.php?id="+data.id;

    var buttonsContainer = document.createElement("div");
    buttonsContainer.classList.add("buttonsContainer");

    image.appendChild(buttonsContainer);

    image.appendChild(img);

    item.appendChild(image);

    var title = document.createElement("div");
    title.classList.add("myTemplateTitle");

    var titleText = document.createTextNode(data.title);

    title.appendChild(titleText);
    item.appendChild(title);

    var department = document.createElement("div");
    department.classList.add("myTemplateDepartment");
    if (data.department == null || data.department == ""){
        var departmentText = document.createTextNode("Unset Department (Global)");
        department.classList.add("text-muted");
    }else{
        department.classList.add("font-weight-bold");
        var departmentText = document.createTextNode(data.dTitle);
    }
    department.appendChild(departmentText);

    item.appendChild(department);

    container.appendChild(item);
}
function changeTemplate(type){
    document.getElementById('btn_blank').classList.remove("active");
    document.getElementById('btn_template').classList.remove("active");

    if(type == 0){
        $("#blankEmail").show();
        $("#templates").hide();
        document.getElementById('btn_blank').classList.add("active");
    }else{
        $("#blankEmail").hide();
        $("#templates").show();
        document.getElementById('btn_template').classList.add("active");
    }
}

var templateId = null;

function setTemplate(id){
    document.getElementById("templateFolder").style.display = "none";
    templateId = id;
    document.getElementById("templateEmail").style.display = "";
    document.getElementById("myTemplates").style.display = "none";
    document.getElementById("templateContainer").src = BASE_URL+"/console/actions/mail/preEmails/getPreEmailsImage.php?id="+id;
    document.getElementById("templateContainer").onload = function(){
        var iframe = document.getElementById("templateContainer");
        var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
        var iframeHeight = iframeDocument.children[0].clientHeight;
        $("#templateContainer").height(iframeHeight);
    };

}
function goBack(){
    document.getElementById("myTemplates").style.display = "";
    document.getElementById("templateEmail").style.display = "none";
}