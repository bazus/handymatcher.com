var theStartDate;
var theEndDate;
var theLabel = "This Month";
$(document).ready(function() {

    $('#dateRangeHomePage span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#dateRangeHomePage').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment().startOf('month'),
        endDate: moment(),
        minDate: '01/01/2018',
        maxDate: '12/31/2020',
        dateLimit: { days: 365 },
        showDropdowns: true,
        showWeekNumbers: false,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last Week': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Go',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function(start, end, label) {
        theLabel = label;
        $(".spanner").each(function(){
            if (label == "Custom"){
                $(this).text("Custom Date");
            } else{
                $(this).text(label);
            }
        });
        if (label != "Custom") {
            $('#dateRangeHomePage span').html(label);
        }else{
            $('#dateRangeHomePage span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        theStartDate = start.format('DD-MM-YYYY');
        theEndDate = end.format('DD-MM-YYYY');

        getStatsData();

    });
    theStartDate = moment().startOf('month').format('DD-MM-YYYY');
    theEndDate = moment().format('DD-MM-YYYY');
    getStatsData();

});
function setLoading(){
    $("#totalLeadsByDate").parent(".ibox-content").addClass("sk-loading");
    $("#totalFinishedJobsByDate").parent(".ibox-content").addClass("sk-loading");
    $("#totalBookedEstimatesByDate").parent(".ibox-content").addClass("sk-loading");
    $("#totalLeadPaymentsByDate").parent(".ibox-content").addClass("sk-loading");
    $("#totalEmailsSentByDate").parent(".ibox-content").addClass("sk-loading");
    $("#totalLeadSmsSentByDate").parent(".ibox-content").addClass("sk-loading");
    $("#topProviders").parent("table").parent(".ibox-content").addClass("sk-loading");
    $("#lead-status-wrapper").parent(".ibox-content").addClass("sk-loading");

}
function setUnLoading(){
    $("#totalLeadsByDate").parent(".ibox-content").removeClass("sk-loading");
    $("#totalFinishedJobsByDate").parent(".ibox-content").removeClass("sk-loading");
    $("#totalBookedEstimatesByDate").parent(".ibox-content").removeClass("sk-loading");
    $("#totalLeadPaymentsByDate").parent(".ibox-content").removeClass("sk-loading");
    $("#totalEmailsSentByDate").parent(".ibox-content").removeClass("sk-loading");
    $("#totalLeadSmsSentByDate").parent(".ibox-content").removeClass("sk-loading");
    $("#topProviders").parent("table").parent(".ibox-content").removeClass("sk-loading");
    $("#lead-status-wrapper").parent(".ibox-content").removeClass("sk-loading");
}
setLoading();
function getStatsData(){

    setLoading();

    var strUrl = BASE_URL+'/console/actions/system/getIndexStats.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            start: theStartDate,
            end: theEndDate,
            label: theLabel
        },
        success: function (html) {
            strReturn = html;
        },
        async: false
    }).done(function (data) {
        data = JSON.parse(data);
        setData(data);

    });
}
function setData(data){

    var noChangeLabel = "No Change";
    var customDatesPerecntageLabel = "---";

    // ================== Leads ==================
    $("#totalLeadsByDate").text(data.totalLeads.totalLeads);

    data.totalLeads.totalLeadsChange = Math.round(data.totalLeads.totalLeadsChange);
    if (data.totalLeads.totalLeadsIsCustom == true){
        $("#totalLeadsChange").html(customDatesPerecntageLabel);
    }else{
        if (data.totalLeads.totalLeadsChange > 0){
            $("#totalLeadsChange").parent().css("color","#1AB394");
            if (data.totalLeads.totalLeadsChange > 500){
                $("#totalLeadsChange").html(">500"+"% <i class=\"fa fa-level-up\"></i>");
            } else{
                $("#totalLeadsChange").html(data.totalLeads.totalLeadsChange+"% <i class=\"fa fa-level-up\"></i>");
            }
        }else{
            if (data.totalLeads.totalLeadsChange == 0){
                $("#totalLeadsChange").parent().css("color", "#676A6C");
                $("#totalLeadsChange").html(noChangeLabel);
            } else{
                $("#totalLeadsChange").parent().addClass("text-danger");
                if (data.totalLeads.totalLeadsChange < -500){
                    $("#totalLeadsChange").html("<500"+"% <i class=\"fa fa-level-down\"></i>");
                } else{
                    $("#totalLeadsChange").html(data.totalLeads.totalLeadsChange+"% <i class=\"fa fa-level-down\"></i>");
                    $("#totalLeadsChange").parent().css("color", "#ed5565");
                }
            }
        }
    }
    // ================== Leads ==================
    // ================== Finished Jobs ==================
    $("#totalFinishedJobsByDate").text(data.totalFinished.totalFinishedJobs);

    data.totalFinished.totalFinishedJobsChange = Math.round(data.totalFinished.totalFinishedJobsChange);
    if (data.totalFinished.totalFinishedJobsIsCustom == true){
        $("#totalFinishedJobsChange").html(customDatesPerecntageLabel);
    }else {
        if (data.totalFinished.totalFinishedJobsChange > 0) {
            $("#totalFinishedJobsChange").parent().css("color", "#1AB394");
            if (data.totalFinished.totalFinishedJobsChange > 500) {
                $("#totalFinishedJobsChange").html(">500" + "% <i class=\"fa fa-level-up\"></i>");
            } else {
                $("#totalFinishedJobsChange").html(data.totalFinished.totalFinishedJobsChange + "% <i class=\"fa fa-level-up\"></i>");
            }
        } else {
            if (data.totalFinished.totalFinishedJobsChange == 0) {
                $("#totalFinishedJobsChange").parent().css("color", "#676A6C");
                $("#totalFinishedJobsChange").html(noChangeLabel);
            } else {
                $("#totalFinishedJobsChange").parent().addClass("text-danger");
                if (data.totalFinished.totalFinishedJobsChange < -500) {
                    $("#totalFinishedJobsChange").html("<500" + "% <i class=\"fa fa-level-down\"></i>");
                } else {
                    $("#totalFinishedJobsChange").html(data.totalFinished.totalFinishedJobsChange + "% <i class=\"fa fa-level-down\"></i>");
                    $("#totalFinishedJobsChange").parent().css("color", "#ed5565");
                }
            }
        }
    }
    // ================== Finished Jobs ==================
    // ================== E-mails Sent ==================
    $("#totalEmailsSentByDate").text(data.totalEmails.totalEmailsSent);

    data.totalEmails.totalEmailsSentChange = Math.round(data.totalEmails.totalEmailsSentChange)
    if (data.totalEmails.totalEmailsSentCustom == true){
        $("#totalEmailsSentChange").html(customDatesPerecntageLabel);
    }else {
        if (data.totalEmails.totalEmailsSentChange > 0) {
            $("#totalEmailsSentChange").parent().css("color", "#1AB394");
            if (data.totalEmails.totalEmailsSentChange > 500) {
                $("#totalEmailsSentChange").html(">500" + "% <i class=\"fa fa-level-up\"></i>");
            } else {

                $("#totalEmailsSentChange").html(data.totalEmails.totalEmailsSentChange + "% <i class=\"fa fa-level-up\"></i>");
            }
        } else {
            if (data.totalEmails.totalEmailsSentChange == 0) {
                $("#totalEmailsSentChange").parent().css("color", "#676A6C");
                $("#totalEmailsSentChange").html(noChangeLabel);
            } else {
                $("#totalEmailsSentChange").parent().addClass("text-danger");
                if (data.totalEmails.totalEmailsSentChange < -500) {
                    $("#totalEmailsSentChange").html("<500" + "% <i class=\"fa fa-level-down\"></i>");
                } else {
                    $("#totalEmailsSentChange").html(data.totalEmails.totalEmailsSentChange + "% <i class=\"fa fa-level-down\"></i>");
                    $("#totalEmailsSentChange").parent().css("color", "#ed5565");
                }
            }
        }
    }
    // ================== E-mails Sent ==================
    // ================== lead sms Sent ==================
    $("#totalLeadSmsSentByDate").text(data.totalLeadSms.totalLeadSmsSent);

    data.totalLeadSms.totalLeadSmsSentChange = Math.round(data.totalLeadSms.totalLeadSmsSentChange)
    if (data.totalLeadSms.totalLeadSmsSentCustom == true){
        $("#totalLeadSmsSentChange").html(customDatesPerecntageLabel);
    }else {
        if (data.totalLeadSms.totalLeadSmsSentChange > 0) {
            $("#totalLeadSmsSentChange").parent().css("color", "#1AB394");
            if (data.totalLeadSms.totalLeadSmsSentChange > 500) {
                $("#totalLeadSmsSentChange").html(">500" + "% <i class=\"fa fa-level-up\"></i>");
            } else {

                $("#totalLeadSmsSentChange").html(data.totalLeadSms.totalLeadSmsSentChange + "% <i class=\"fa fa-level-up\"></i>");
            }
        } else {
            if (data.totalLeadSms.totalLeadSmsSentChange == 0) {
                $("#totalLeadSmsSentChange").parent().css("color", "#676A6C");
                $("#totalLeadSmsSentChange").html(noChangeLabel);
            } else {
                $("#totalLeadSmsSentChange").parent().addClass("text-danger");
                if (data.totalLeadSms.totalLeadSmsSentChange < -500) {
                    $("#totalLeadSmsSentChange").html("<500" + "% <i class=\"fa fa-level-down\"></i>");
                } else {
                    $("#totalLeadSmsSentChange").html(data.totalLeadSms.totalLeadSmsSentChange + "% <i class=\"fa fa-level-down\"></i>");
                    $("#totalLeadSmsSentChange").parent().css("color", "#ed5565");
                }
            }
        }
    }
    // ================== lead sms Sent ==================
    // ================== Booked Estimate ==================
    $("#totalBookedEstimatesByDate").text(data.totalBooked.totalBookedEstimates);
    data.totalBooked.totalBookedEstimatesChange = Math.round(data.totalBooked.totalBookedEstimatesChange);
    if (data.totalBooked.totalBookedEstimatesIsCustom == true){
        $("#totalBookedEstimatesChange").html(customDatesPerecntageLabel);
    }else {
        if (data.totalBooked.totalBookedEstimatesChange > 0) {
            $("#totalBookedEstimatesChange").parent().css("color", "#1AB394");
            if (data.totalBooked.totalBookedEstimatesChange > 500) {
                $("#totalBookedEstimatesChange").html(">500" + "% <i class=\"fa fa-level-up\"></i>");
            } else {
                $("#totalBookedEstimatesChange").html(data.totalBooked.totalBookedEstimatesChange + "% <i class=\"fa fa-level-up\"></i>");
            }
        } else {
            if (data.totalBooked.totalBookedEstimatesChange == 0) {
                $("#totalBookedEstimatesChange").parent().css("color", "#676A6C");
                $("#totalBookedEstimatesChange").html(noChangeLabel);
            } else {

                $("#totalBookedEstimatesChange").parent().addClass("text-danger");
                if (data.totalBooked.totalBookedEstimatesChange < -500) {
                    $("#totalBookedEstimatesChange").html("<500" + "% <i class=\"fa fa-level-down\"></i>");
                } else {
                    $("#totalBookedEstimatesChange").html(data.totalBooked.totalBookedEstimatesChange + "% <i class=\"fa fa-level-down\"></i>");
                    $("#totalBookedEstimatesChange").parent().css("color", "#ed5565");

                }
            }
        }
    }
    // ================== Booked Estimate ==================
    // ================== Lead Payments ==================
    $("#totalLeadPaymentsByDate").text("$"+data.totalPayments.totalLeadPayments);

    data.totalPayments.totalLeadPaymentsChange = Math.round(data.totalPayments.totalLeadPaymentsChange);
    if (data.totalPayments.totalLeadPaymentsIsCustom == true){
        $("#totalLeadPaymentsChange").html(customDatesPerecntageLabel);
    }else {
        if (data.totalPayments.totalLeadPaymentsChange > 0) {
            $("#totalLeadPaymentsChange").parent().css("color", "#1AB394");
            if (data.totalPayments.totalLeadPaymentsChange > 500) {
                $("#totalLeadPaymentsChange").html(">500" + "% <i class=\"fa fa-level-up\"></i>");
            } else {
                $("#totalLeadPaymentsChange").html(data.totalPayments.totalLeadPaymentsChange + "% <i class=\"fa fa-level-up\"></i>");
            }
        } else {
            if (data.totalPayments.totalLeadPaymentsChange == 0) {
                $("#totalLeadPaymentsChange").parent().css("color", "#676A6C");
                $("#totalLeadPaymentsChange").html(noChangeLabel);
            } else {

                $("#totalLeadPaymentsChange").parent().addClass("text-danger");
                if (data.totalPayments.totalLeadPaymentsChange < -500) {
                    $("#totalLeadPaymentsChange").html("<500" + "% <i class=\"fa fa-level-down\"></i>");
                } else {
                    $("#totalLeadPaymentsChange").html(data.totalPayments.totalLeadPaymentsChange + "% <i class=\"fa fa-level-down\"></i>");
                    $("#totalLeadPaymentsChange").parent().css("color", "#ed5565");

                }
            }
        }
    }
    // ================== Lead Payments ==================
    // ================== Top Providers ==================

    var container = $("#topProviders");
    container.html("");

    if (data.topProviders != null && data.topProviders.length > 0){

        // ========= GET THE AVARAGE  OF EMAILS OPEN RATES
        var emailOpenRateAVG = 0;
        for (var x=0;x<data.topProviders.length;x++) {
            emailOpenRateAVG += data.topProviders[x].emailOpenRatePrec
        }
        emailOpenRateAVG = Math.round(emailOpenRateAVG/data.topProviders.length);

        for (var x=0;x<data.topProviders.length;x++){
            var provider = data.topProviders[x];
            provider.LastMonthChange = Math.round(provider.LastMonthChange * 100) / 100;

            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var tdText = document.createTextNode(provider.name);

            td.appendChild(tdText);
            tr.appendChild(td);

            var td = document.createElement("td");
            if (provider.LastMonthChange == ""){
                var tdText = document.createTextNode(provider.total);
                td.appendChild(tdText);
            } else {
                var tdText = document.createTextNode(provider.total + " / ");

                var i = document.createElement("i");
                var span = document.createElement("span");
                var lastMonthChange = "";
                i.classList.add("fa");
                if (provider.LastMonthChange > 0) {
                    i.classList.add("fa-level-up");
                    span.style.color = "#1AB394";
                    if (provider.LastMonthChange > 500) {
                        lastMonthChange = ">500%";
                    } else {
                        lastMonthChange = provider.LastMonthChange + "%";
                    }

                } else {
                    if (provider.LastMonthChange == 0) {
                        span.style.color = "#676A6C";
                        span.style.fontSize = "10px";
                        lastMonthChange = "No Change From Last Month";
                    } else {
                        span.style.color = "#ED5565";
                        i.classList.add("fa-level-down");
                        if (provider.LastMonthChange < -500) {
                            lastMonthChange = "<500%";
                        }
                    }
                }
                span.title = provider.LastMonthChange + "% From Last Month";
                var lastMonthChangeText = document.createTextNode(lastMonthChange);
                span.appendChild(lastMonthChangeText);
                span.appendChild(i);
                td.appendChild(tdText);
                td.appendChild(span);
            }
            tr.appendChild(td);

            var td = document.createElement("td");
            var span = document.createElement("span");
            span.title = provider.lastLead;
            span.innerHTML = provider.convertedLastLead;


            td.appendChild(span);

            tr.appendChild(td);

            container.append(tr);

            //provider percentage
            var td = document.createElement("td");
            var span = document.createElement("span");
            span.innerHTML = Math.round(provider.emailOpenRatePrec) + "%";
            if(provider.emailOpenRatePrec >= emailOpenRateAVG){
                span.setAttribute("style","color: rgb(26, 179, 148);");
            }else{
                span.setAttribute("style","color: #f8ac59;");
            }

            td.appendChild(span);

            tr.appendChild(td);

            container.append(tr);
        }
    }else{
        if (data.haveProviders === true){
            // Have providers but no data to display
            var tr = document.createElement("tr");
            tr.setAttribute("style","border: 1px solid #e4e4e4;");

            var td = document.createElement("td");
            td.setAttribute("colspan","5");
            td.setAttribute("style","background-color: #e7eaec36;text-align: center;");
            td.innerHTML = 'No Top Providers';

            tr.appendChild(td);
            container.append(tr);
        }else{
            // Doesn't have any providers
            var tr = document.createElement("tr");
            tr.setAttribute("style","border: 1px solid #e4e4e4;");

            var td = document.createElement("td");
            td.setAttribute("colspan","5");
            td.setAttribute("style","background-color: #e7eaec36;text-align: center;");
            td.innerHTML = 'No Providers Yet <a href="'+BASE_URL+'/console/categories/leads/leadProviders.php">(Add Provider)</a>';

            tr.appendChild(td);
            container.append(tr);
        }

    }
    // ================== Top Providers ==================

    // ================== Lead vs Booked ==================
    var data2 = [];
    var data3 = [];
    var strUrl = BASE_URL + '/console/actions/leads/getLeadsByDay.php';
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            activityType: activityType
        },
        async: false
    }).done(function (data) {
        data = JSON.parse(data);

        var totalLeads = 0;
        var totalBooked = 0;

        for (var i = 0; i < data['leads'].length; i++){
            totalLeads += parseInt(data["leads"][i]['total']);
            totalBooked += parseInt(data["bookedJobs"][i]['total']);
            data2.push([new Date(data["bookedJobs"][i]['date']).getTime(),parseInt(data["bookedJobs"][i]['total'])]);
            data3.push([new Date(data["leads"][i]['date']).getTime(),parseInt(data["leads"][i]['total'])]);
        }

        if (totalLeads) {
            document.getElementById("chartTotalLeads").innerText = totalLeads;
        }else{
            document.getElementById("chartTotalLeads").innerText = 0;
        }
        if (totalBooked){
            document.getElementById("chartTotalBooked").innerText = totalBooked;
        }else{
            document.getElementById("chartTotalBooked").innerText = 0;
        }
        if (data['payments']) {
            document.getElementById("chartTotalPayments").innerText = "$"+data['payments'];
        }else{
            document.getElementById("chartTotalPayments").innerText = "$0.00";
        }

        var dataset = [
            {
                label: "Leads",
                data: data3,
                color: "#1ab394",
                bars: {
                    show: true,
                    align: "center",
                    barWidth: 24 * 60 * 60 * 600,
                    lineWidth:0
                }

            }, {
                label: "Booked Jobs",
                data: data2,
                yaxis: 1,
                color: "#1C84C6",
                lines: {
                    lineWidth:1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                splines: {
                    show: true,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                },
            }
        ];
        if (activityType == 1){
            var tickSize = [3,"day"];
        }else if (activityType == 2){
            var tickSize = [7,"day"];
        }else if (activityType == 3){
            var tickSize = [1,"month"];
        }
        $.plot($("#flot-dashboard-chart"),dataset, {
            legend: {
                position:"nw"
            },
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderColor: "#FFF"
            },
            xaxis: {
                mode: "time",
                tickSize: tickSize,
                tickLength: 0,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 11,
                axisLabelFontFamily: 'Arial',
                axisLabelPadding: 2,
                color: "#d5d5d5"
            },

        });
        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #1C84C5",
            padding: "4px",
            color: "white",
            fontSize: "14px",
            "background-color": "#1C84C5",
            opacity: 0.80
        }).appendTo("body");
        $("#flot-dashboard-chart").bind("plothover", function (event, pos, item) {
            if (pos){
                if (item){
                    var text;
                    if (item.seriesIndex == 0){
                        text = " Leads";
                    }else{
                        text = " Booked Jobs";
                    }
                    $("#tooltip").html(item.datapoint[1]+text)
                        .css({top: item.pageY-35, left: item.pageX+8})
                        .fadeIn(400);
                }
            }
        });
    });
    // ================== Lead vs Booked ==================
    // ================== Lead Status ==================
    $('.lead-status-amount').html("0");
    for (var key in data.totalLeadsStatus) {
        switch(key) {
            case "0":
                $('#leadStatusNewAmount').html(data.totalLeadsStatus[key]);
                break;
            case "1":
                $('#leadStatusPendingAmount').html(data.totalLeadsStatus[key]);
                break;
            case "2":
                $('#leadStatusQuotedAmount').html(data.totalLeadsStatus[key]);
                break;
            case "3":
                $('#leadStatusBookedAmount').html(data.totalLeadsStatus[key]);
                break;
            case "4":
                $('#leadStatusInProgressAmount').html(data.totalLeadsStatus[key]);
                break;
            case "5":
                $('#leadStatusStorageAmount').html(data.totalLeadsStatus[key]);
                break;
            case "6":
                $('#leadStatusCompletedAmount').html(data.totalLeadsStatus[key]);
                break;
            case "7":
                $('#leadStatusCancelledAmount').html(data.totalLeadsStatus[key]);
                break;

            default:
        }
    }


    setUnLoading();
}

var activityType = 1;

function setGraphDate(val,el){
    activityType = val;
    getStatsData();
    try {
        document.getElementById("activity-task1").classList.remove("active");
        document.getElementById("activity-task2").classList.remove("active");
        document.getElementById("activity-task3").classList.remove("active");
    }catch (e) {}
    el.classList.add("active");
}