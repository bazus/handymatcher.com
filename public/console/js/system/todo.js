function getLists(){
    var strUrl = BASE_URL+'/console/actions/system/todo/list/getList.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        data = JSON.parse(data);
        document.getElementById("todoContainer").innerHTML = "<select id='todoLists' class='form-control'> \n </select> <ul class='todo-list m-t small-list ui-sortable' id='todoTasks'>\n </ul>";
        var container = document.getElementById("todoLists");
        container.innerHTML = "";
        if(data.length > 0) {
            container.style.display = "inline";

            for (var i = 0; i < data.length; i++) {
                setLists(data[i],container);
            }

            getTasks(data[0].id);

            var str = data[0].title;

            if(str.length > 10){
                str = str.substring(0, 8);
                document.getElementById("deleteList").innerText = "Delete "+str.trim()+"..";
            }else{
                document.getElementById("deleteList").innerText = "Delete "+str;
            }


            $("#todoLists").on("change",function(){
                getTasks($(this).val());
                var str = $("#todoLists option:selected").text();
                if(str.length > 10){
                    str = str.substring(0, 8);
                    document.getElementById("deleteList").innerText = "Delete "+str.trim()+"..";
                }else{
                    document.getElementById("deleteList").innerText = "Delete "+$("#todoLists option:selected").text();
                }
            });
        }else{
            container.style.display = "none";
            container.parentElement.innerHTML = "<div class=\"alert alert-info\">Here you can create a list of to-do tasks and keep track of your tasks.</div><button onclick='addList()' class='btn btn-primary btn-block'>Create First List</button>";
        }
    });
}

getLists();

function setLists(data,container){

    var option = document.createElement("option");
    option.value = data.id;

    var optionText = document.createTextNode(data.title);

    option.appendChild(optionText);
    container.appendChild(option);
}

function getTasks(id){
    var strUrl = BASE_URL+'/console/actions/system/todo/task/getTask.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method: "post",
        data:{
            id:id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        data = JSON.parse(data);

        var container = document.getElementById("todoTasks");
        container.innerHTML = "";

        if (data.length > 0){
            for (var i = 0; i < data.length; i++) {
                setTasks(data[i],container);
            }
        } else{
            container.innerHTML = "<button onclick=\"addItem()\" id='firstTask' class=\"btn btn-success btn-block\">Create First Task</button>";
        }

        $(".tasker").on("change",function(){
            var cuttedId = this.id.replace("task-","");
            var checked = this.checked;

            activeTask(cuttedId,checked);

            $(this).parent().toggleClass("delTask");
        });

        $(".liTask").hover(function () {
            $(this).children("span").addClass("showDelete");
        }, function () {
            $(this).children("span").removeClass("showDelete");
        });
    });
}

function setTasks(data,container){

    var li = document.createElement("li");
    li.style.cursor = "pointer";
    li.classList.add("checkbox");
    li.classList.add("checkbox-primary");
    li.classList.add("liTask");
    li.style.marginTop = "3px";
    li.style.paddingLeft = "27px";

    // var a = document.createElement("a");
    // // a.href = "#";
    // a.classList.add("check-link");

    var label = document.createElement("label");
    // label.classList.add("m-l-xs");
    label.setAttribute("for","task-"+data.id);
    var labelText = document.createTextNode(data.title);

    var input = document.createElement("input");
    input.type = "checkbox";
    input.id = "task-"+data.id;
    input.name = "task-"+data.id;
    input.classList.add("tasker");
    if (data.isActive == 1){
        input.setAttribute("checked","true");
    }else{
        li.classList.add("delTask");
    }

    var span = document.createElement("span");
    span.classList.add("btn");
    span.classList.add("btn-sm");
    span.classList.add("btn-danger");
    span.style.cssFloat = "right";
    span.style.display = "none";
    span.style.fontSize = "7px";
    span.style.borderRadius = "20%";
    span.setAttribute("onclick","deleteTask("+data.id+")");

    var spanText = document.createTextNode("X");
    span.appendChild(spanText);

    label.appendChild(labelText);
    li.appendChild(input);
    li.appendChild(label);
    li.appendChild(span);

    container.appendChild(li);
}

function activeTask(id,active){

    var strUrl = BASE_URL+'/console/actions/system/todo/task/activeTask.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method: "post",
        data:{
            id:id,
            active:active
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data !== true){
                toastr.error("Error Saving Task Condition "+data);
            }
        }catch (e) {
            toastr.error("Error Saving Task Condition");
        }
    });


}

function addList(){
    $("#myCustomModal").modal("hide");
    showCustomModal("/categories/iframes/todo/add.php?type=list");
}

function addItem(){

    if (!document.getElementById("task-new")){
        if(document.getElementById("firstTask")){
            document.getElementById("firstTask").remove();
        }
        var container = document.getElementById("todoTasks");

        var li = document.createElement("li");
        li.style.cursor = "pointer";
        li.classList.add("checkbox");
        li.style.marginTop = "3px";
        li.classList.add("checkbox-primary");
        //
        // var a = document.createElement("a");
        // // a.href = "#";
        // a.classList.add("check-link");
        var div = document.createElement("div");
        div.classList.add("input-group");

        var col = document.createElement("div");
        col.classList.add("col-md-12");

        var input = document.createElement("input");
        input.setAttribute("type","text");
        input.setAttribute("placeholder","Enter Task Name");
        input.classList.add("form-control");
        input.id = "task-new";

        var span = document.createElement("span");
        span.classList.add("input-group-btn");

        var button = document.createElement("button");
        button.classList.add("btn");
        button.classList.add("btn-sm");
        button.classList.add("btn-success");
        button.setAttribute("style","padding: 7px;margin-bottom: 0px;");
        button.setAttribute("onclick","createTask()");

        var buttonText = document.createTextNode("Save");
        button.appendChild(buttonText);

        span.appendChild(button);
        div.appendChild(input);
        div.appendChild(span);


        li.appendChild(div);

        container.appendChild(li);
    }

}
function createTask(){
    var strUrl = BASE_URL+'/console/actions/system/todo/task/addTask.php', strReturn = "";
    var title = document.getElementById("task-new").value;
    document.getElementById("task-new").remove();
    var list = document.getElementById("todoLists").value;
    jQuery.ajax({
        url: strUrl,
        method: "post",
        data:{
            title:title,
            list:list
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data['status'] !== true){
                swal("Error", "Error Creating Task", "error");
            }else{
                getTasks(data['id']);
            }
        }catch (e) {
            swal("Error", "Error Creating Task", "error");
        }
    });
}

function deleteTask(id){
    swal({
        title: "Are you sure?",
        text: "Your will not be able to recover this Task",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/system/todo/task/deleteTask.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "post",
                data:{
                    id:id,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data !== true){
                        swal("Error", "Error Deleting Task", "error");
                    }else{
                        getLists();
                    }
                }catch (e) {
                    swal("Error", "Error Deleting Task", "error");
                }
            });
        }
    });

}

function deleteList(){
    var id = document.getElementById("todoLists").value;
    var val = $("#todoLists option:selected").text();
    swal({
        title: "Are you sure?",
        text: "Your will not be able to recover this List Named '"+val+"'",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/system/todo/list/deleteList.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "post",
                data:{
                    id:id,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data !== true){
                        swal("Error", "Error Deleting List", "error");
                    }else{
                        swal("Deleted", "Your List '"+val+"' is Deleted", "success");
                    }
                    getLists();
                }catch (e) {
                    swal("Error", "Error Deleting List '"+val+"'", "error");
                }
            });
        }
    });
}