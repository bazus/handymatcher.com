var rooms = [];

function getUsersInOrganization(){

    var strUrl = BASE_URL + '/console/actions/chat/getUsers.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data.length>0){
                setUsers(data);
            }else{
                var container = document.getElementById("chatUsers");
                var div = document.createElement("div");
                div.style.textAlign = "center";
                var h3 = document.createElement("h3");
                h3.style.marginBottom = "20px";
                h3.style.marginTop = "20px";
                var h3Text = document.createTextNode("No Users in organization");
                h3.appendChild(h3Text);
                div.appendChild(h3);
                if (document.getElementById("chatAlert") !== null){

                    var a = document.createElement("a");
                    a.href = BASE_URL+"/console/categories/user/management.php?tab=users&showUsersModal=1";

                    var button = document.createElement("button");
                    button.classList.add("btn");
                    button.classList.add("btn-block");
                    button.classList.add("btn-default");
                    button.setAttribute("style","width: 90%;margin: 0 auto;");

                    var buttonText = document.createTextNode("Invite Users");
                    button.appendChild(buttonText);

                    a.appendChild(button);
                    div.appendChild(a);

                    container.appendChild(div);
                }else{
                    container.appendChild(div);
                }
            }
        }catch (e) {

        }
    });
}

function setUsers(users) {
        var container = document.getElementById("chatUsers");
        for(var i=0; i<users.length; i++) {
            // create the users chat
            var user = users[i];

            var profileDiv = document.createElement("div");
            profileDiv.classList.add("client-avatar");
            profileDiv.setAttribute("style","width: 29px;height: 29px;float: left;");

            var profilePic = document.createElement("img");
            profilePic.alt = user.name;
            profilePic.src = user.profilePicture;

            var div = document.createElement("div");
            div.classList.add("sidebar-message");
            div.setAttribute("onclick","checkForRoomWithUser('"+user.id+"');$('.right-sidebar-toggle').click();");

            var body = document.createElement("div");
            body.classList.add("media-body");
            body.style.lineHeight = "29px";

            var bold = document.createElement("b");
            bold.style.marginLeft = "5px";

            var name = document.createTextNode(user.fullName);

            profileDiv.appendChild(profilePic);
            bold.appendChild(name);
            body.appendChild(profileDiv);
            body.appendChild(bold);
            div.appendChild(body);
            container.appendChild(div);
        }
}

function checkForRoomWithUser(chatWithUserId,disableOpenEffect){
    var doesExist = false;
    for(var i = 0;i<rooms.length;i++){
        if(rooms[i] == chatWithUserId){
            toggleChat(chatWithUserId,true);
            doesExist = true;
        }
    }
    if(!doesExist){
        // Get User Data
        var strUrl = BASE_URL + '/console/actions/chat/getUserData.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            data:{
                userId:chatWithUserId
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                createRoom(data,disableOpenEffect);
            }catch (e) {

            }
        });
    }
}

function toggleChat(chatId,showChat) {
    var chatBox = document.getElementById('chat'+chatId);
    if(showChat){
        if($(chatBox).hasClass("closedChat")){
            $(chatBox).toggleClass("closedChat");
        }
    }else {
        $(chatBox).toggleClass("closedChat");
    }
}

function createRoom(userData,disableOpenEffect) {

    // SET ROOM AS OPEN IN USER TABLE
    var strUrl = BASE_URL + '/console/actions/chat/setRoomAsOpen.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method:"POST",
        data:{
            userId:userData.id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

    });

    //
    document.getElementById('open-small-chat').style.display = 'block';
    document.getElementById('open-small-chat').innerHTML = '<i class="fa fa-remove"></i>';

    var chatRooms = document.getElementById('chatRooms');

    var chatBox = document.createElement("div");
    chatBox.id = "chat"+userData.id;
    if(disableOpenEffect == true){
        chatBox.className = "small-chat-box active";
    }else {
        chatBox.className = "small-chat-box fadeInRight animated active";
    }
    var chatHeading = document.createElement("div");
    chatHeading.className = "heading";
    chatHeading.setAttribute("draggable","true");
    chatHeading.style.cursor = "pointer";
    chatHeading.setAttribute("onClick","toggleChat('"+userData.id+"')");
    chatHeading.innerHTML = '<small class="chat-date pull-right">\n' +
        '                <small class="chat-date pull-right">\n' +
        '                    <i class="fa fa-times" onclick="closeChat('+userData.id+')" style="font-size: 13px;line-height: 19px;"></i>\n' +
        '                </small>\n' +
        '            </small>\n' +
        '            '+userData.fullName;

    var chatSlimScroll = document.createElement("div");
    chatSlimScroll.className = "slimScrollDiv";
    chatSlimScroll.setAttribute("style","position: relative; overflow: hidden; width: auto; height: 234px;");

    var chatContent = document.createElement("div");
    chatContent.className = "content";
    chatContent.id = "chat"+userData.id+"content";
    chatContent.setAttribute("style","float: left;overflow-y: auto;height: 234px;width: 100%;");

    var slimScrollBar = document.createElement("div");
    slimScrollBar.className = "slimScrollBar";
    slimScrollBar.setAttribute("style","background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 164.928px;");

    var slimScrollRail = document.createElement("div");
    slimScrollRail.className = "slimScrollRail";
    slimScrollRail.setAttribute("style","width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.4; z-index: 90; right: 1px;");

    var formChat = document.createElement("div");
    formChat.className = "form-chat";

    var formBottom = document.createElement("div");
    formBottom.className = "input-group input-group-sm";

    var msgInput = document.createElement("input");
    msgInput.type = "text";
    msgInput.id = "chatTextInput"+userData.id;
    msgInput.className = "form-control chatQ";

    var inputGroup = document.createElement("span");
    inputGroup.className = "input-group-btn";
    //inputGroup.innerHTML = '<button class="btn btn-primary" type="button" onclick="sendMyMSG(\''+userData.id+'\')">Send</button>';

    var sendButton = document.createElement("button");
    sendButton.className = "btn btn-primary";
    sendButton.type = "button";
    sendButton.setAttribute("onclick","sendMyMSG('"+userData.id+"')");
    sendButton.innerHTML = "Send";

    inputGroup.appendChild(sendButton);


    formBottom.appendChild(msgInput);
    formBottom.appendChild(inputGroup);

    chatSlimScroll.appendChild(chatContent);
    chatSlimScroll.appendChild(slimScrollBar);
    chatSlimScroll.appendChild(slimScrollRail);

    formChat.appendChild(formBottom);

    chatBox.appendChild(chatHeading);
    chatBox.appendChild(chatSlimScroll);
    chatBox.appendChild(formChat);

    chatRooms.appendChild(chatBox);

    rooms.push(userData.id);


    $(msgInput).keypress(function (e) {
        if (e.which == 13) {
            sendButton.click();
            return false;
        }
    });


    var allChats = document.getElementsByClassName("small-chat-box");
    for(var i = 0;i<allChats.length;i++){

        var singleChat = allChats[i];
        if(!$(singleChat).hasClass("active")){
            singleChat.classList.add("active");
        }
    }


    var strUrl = BASE_URL + '/console/actions/chat/getChatConversation.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method:"POST",
        data:{
            userId:userData.id
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            for(var i = 0;i<data.length;i++){
                var singleMSG = data[i];
                var d = new Date(singleMSG.sentTime.replace(/\s/, 'T'));
                var nowTime = d.getHours() + ":" + (d.getMinutes()<10?'0':'') + d.getMinutes();
                addMSG(userData.id,singleMSG.message, nowTime, singleMSG.type);
            }
        }catch (e) {

        }
    });

}

function addMSG(chatId,msg,time,type){

    var chatContent = document.getElementById("chat"+chatId+"content");

    var msgDiv = document.createElement("div");
    if(type === "right") {
        msgDiv.className = "right";
    }else{
        msgDiv.className = "left";
    }

    var author = document.createElement("div");
    author.className = "author-name";
    author.innerHTML = '<small class="chat-date"> '+time+'</small>';

    var msgText = document.createElement("div");
    if(type === "right") {
        msgText.className = "chat-message";
    }else{
        msgText.className = "chat-message active";
    }
    msgText.innerHTML = msg;

    msgDiv.appendChild(author);
    msgDiv.appendChild(msgText);

    chatContent.appendChild(msgDiv);

    chatContent.scrollTop = chatContent.scrollHeight;

}

function sendMyMSG(chatId) {

    var msg = document.getElementById('chatTextInput'+chatId).value;
    document.getElementById('chatTextInput'+chatId).value = "";

    if(msg == ""){return;}

    var strUrl = BASE_URL + '/console/actions/chat/addMsg.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method:"POST",
        data:{
            toUserId:chatId,
            msg:msg
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);

            socket.emit('sendmsg', {userid:chatId,mymsg:msg,msgId:data.msgId});

            if(data.status == true) {
                var d = new Date();
                var nowTime = d.getHours() + ":" + (d.getMinutes()<10?'0':'') + d.getMinutes();

                addMSG(chatId,msg, nowTime, "right");

            }
        }catch (e) {

        }
    });

}

function closeChat(chatId){

    // SET ROOM AS OPEN IN USER TABLE
    var strUrl = BASE_URL + '/console/actions/chat/setRoomAsClosed.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method:"POST",
        data:{
            userId:chatId
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

    });

    var singleChat = document.getElementById("chat"+chatId);
    singleChat.parentNode.removeChild(singleChat);

    var roomIndex = rooms.indexOf(chatId);
    rooms.splice(roomIndex, 1);

    if(rooms.length == 0){
        document.getElementById('open-small-chat').innerHTML = '<i class="fa fa-comments"></i>';
    }
}

getUsersInOrganization();