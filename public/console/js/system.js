var CDN = "https://d2o9xrcicycxrg.cloudfront.net";

function limitReached(type) {
    if (type == "email") {
        var imgTypeLink = CDN + "/console/images/icons/emailsLimit.png";
        var h2TypeText = "You have reached your sending limit";
    } else if (type == "user") {
        var imgTypeLink = CDN + "/console/images/icons/usersLimit.png";
        var h2TypeText = "You have reached your user limit";
    } else if (type == "package") {
        var imgTypeLink = CDN + "/console/images/icons/featureLimit.png";
        var h2TypeText = "Your plan does not include this feature";
    } else if (type == "balance") {
        var imgTypeLink = CDN + "/console/images/icons/balanceLimit.png";
        var h2TypeText = "Your balance is too low";
    }


    var content = document.createElement("div");
    var closeBtn = document.createElement("button");
    closeBtn.classList.add("btn");
    closeBtn.classList.add("btn-xs");
    closeBtn.classList.add("btn-default");
    closeBtn.classList.add("pull-right");
    closeBtn.setAttribute("onclick", "swal.close()");

    var closeBtnI = document.createElement("i");
    closeBtnI.classList.add("fa");
    closeBtnI.classList.add("fa-times");

    closeBtn.appendChild(closeBtnI);
    content.appendChild(closeBtn);

    var img = document.createElement("img");
    img.style.height = "250px";
    img.setAttribute("src", imgTypeLink);

    content.appendChild(img);

    var h2 = document.createElement("h2");
    h2.style.fontSize = "22px";
    h2.style.fontWeight = 800;
    h2.style.margin = "25px auto";
    var h2Text = document.createTextNode(h2TypeText);

    h2.appendChild(h2Text);

    content.appendChild(h2);
    if (type != "balance"){
        var button = document.createElement("button");
        button.classList.add("btn");
        button.classList.add("btn-primary");
        button.setAttribute("onclick", "offerToUpgradePlan()");

        var buttonText = document.createTextNode("Upgrade your plan");
        button.appendChild(buttonText);
    }else{
        var button = document.createElement("button");
        button.classList.add("btn");
        button.classList.add("btn-primary");
        button.setAttribute("onclick", "rechargeUserBalance()");

        var buttonText = document.createTextNode("Recharge your balance");
        button.appendChild(buttonText);
    }
    content.appendChild(button);

    swal({
        content: content,
        buttons:false
    });

}

function unAuthorizedAction(){
    var content = document.createElement("div");
    content.setAttribute("style","text-align:center;border-top: 1px solid #f3f3f3;padding-top: 19px;");

    var contentP = document.createElement("p");
    contentP.setAttribute("style","width: 444px;display: inline-table;font-size:18px;");
    contentP.innerHTML = "You are not authorized to access this page";

    content.appendChild(contentP);

    var contentP = document.createElement("p");
    contentP.setAttribute("style","width: 444px;display: inline-table;font-size:12px;");
    contentP.innerHTML = "Contact your administrator for more information";

    content.appendChild(contentP);

    var footer = document.createElement("div");

    var closeBTN = document.createElement("a");
    closeBTN.className = "cusmod-button";
    closeBTN.setAttribute("style","background-color: #2196F3;");
    closeBTN.setAttribute("data-dismiss","cusmod");
    closeBTN.innerHTML = "Got it";

    footer.appendChild(closeBTN);

    var settings = {
        animation:"fadeInUp",
        template: "cusmod-blue",
        hideCloseBTN:false,
        header:"Access denied",
        content: content,
        footer: footer
    };
    initCusmod("#cusmod", settings);
}

function offerToUpgradePlan(){
    swal.close();
    location.href = BASE_URL+"/console/categories/organization/billing.php#upgrade";
}

function rechargeUserBalance(){
    swal.close();
    location.href = BASE_URL+"/console/categories/organization/billing.php?recharge=1";
}

function userCantSendAutoEmail() {
    document.getElementById("sendEstimateEmail").checked = false;
    var content = document.createElement("div");

    var contentText = document.createTextNode("To send an automatic email, you need to have an active linked email account and a template that will be sent to incoming leads.");

    content.append(contentText);

    var buttonContainer = document.createElement("div");
    buttonContainer.style.marginTop = "15px";

    var buttonEmail = document.createElement("button");
    buttonEmail.setAttribute("onclick","location.href = '"+BASE_URL+"/console/categories/mail/mailSettings.php'");
    buttonEmail.style.marginRight = "5px";
    buttonEmail.classList.add("btn");
    buttonEmail.classList.add("btn-sm");
    buttonEmail.classList.add("btn-default");
    var buttonText = document.createTextNode("Add mail account");
    buttonEmail.append(buttonText);
    buttonContainer.appendChild(buttonEmail);

    var buttonTemplate = document.createElement("button");
    buttonTemplate.setAttribute("onclick","location.href = '"+BASE_URL+"/console/categories/mail/templates.php'");
    buttonTemplate.classList.add("btn");
    buttonTemplate.classList.add("btn-sm");
    buttonTemplate.classList.add("btn-default");
    var buttonText = document.createTextNode("Add mail template");
    buttonTemplate.append(buttonText);
    buttonContainer.appendChild(buttonTemplate);

    content.appendChild(buttonContainer);
    swal({
        text: false,
        content:content,
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}