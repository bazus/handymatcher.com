var leadController = {
    page:{
        change:function (id,emailId,userSent) {
            document.getElementById("previewSubject").value = "";
            $('#previewContent').html('');

            if (id == 1){
                $("#contactClientContent").css('display','none');
                leadController.contact.getLeadEmailsData();
                document.getElementById("newEmailTab").style.display = "none";
                document.getElementById("tableTab").style.display = "";
                document.getElementById("buttonsPage1").style.display = "flex";
                document.getElementById("buttonsPage2").style.display = "none";
                document.getElementById("previewEmail").style.display = "none";

            }else if (id == 2) {
                $("#contactClientContent").css('display','block');
                leadController.contact.getLeadEmailDataById(emailId,userSent,function () {
                    var iFrameID = document.getElementById('previewContent');
                    if(iFrameID) {
                        // here you can make the height, I delete it first, then I make it again
                        iFrameID.height = "";
                        iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
                    }
                });
                document.getElementById("newEmailTab").style.display = "none";
                document.getElementById("tableTab").style.display = "none";
                document.getElementById("buttonsPage1").style.display = "none";
                document.getElementById("buttonsPage2").style.display = "";
                document.getElementById("previewEmail").style.display = "";
            }
        },
        showReminderModal:function () {
            showCustomModal("categories/iframes/leads/reminder.php?leadId="+leadId);
        },
        setLeftSideTop:function () {
            document.getElementById("leadTopName").innerText = leadData.leadData.firstname+" "+leadData.leadData.lastname;

            // =========== (START) SET USER HANDELING ===========
            var containerSelect = document.getElementById("userHandelingSelect");
            containerSelect.innerHTML = "";

            if (leadData.usersToAssign && leadData.usersToAssign.length > 0) {

                for (var i = 0; i < leadData.usersToAssign.length; i++) {

                    var option = document.createElement('option');
                    option.value = leadData.usersToAssign[i].id;
                    option.innerHTML = "Assign to "+leadData.usersToAssign[i].fullName;

                    if (leadData.leadData.userIdAssigned == leadData.usersToAssign[i].id){
                        option.selected = true;
                    }

                    containerSelect.appendChild(option);
                }
            }else{
                containerSelect.disabled = true;
            }
            // =========== (END) SET USER HANDELING ===========

            // Set the job number
            if(leadData.leadData.jobNumber == null){
                document.getElementById("jobNumber").innerText = leadData.leadData.id;
            }else{
                document.getElementById("jobNumber").innerText = leadData.leadData.jobNumber;
            }

            document.getElementById("leadTopName").classList.add("text-navy");
            if (leadData.leadData.email != null && leadData.leadData.email != "") {
                document.getElementById("leadTopEmail").innerText = leadData.leadData.email;
                document.getElementById("leadTopEmail").classList.add("text-navy");
                document.getElementById("emailIcon").classList.add("text-navy");
            }else{
                document.getElementById("leadTopEmail").innerText = "None";
                document.getElementById("leadTopEmail").classList.remove("text-navy");
                document.getElementById("emailIcon").classList.remove("text-navy");
            }
            if (leadData.leadData.phone != null && leadData.leadData.phone != "") {

                if (isaphonenumber(leadData.leadData.phone)) {

                    var phoneNumberHref = document.createElement('a');
                    phoneNumberHref.classList.add("text-navy");
                    phoneNumberHref.href = "tel:+1-" + leadData.leadData.phone;
                    phoneNumberHref.innerHTML = "<i class=\"fa fa-phone\"></i> "+leadData.leadData.phone;
                    document.getElementById("leadTopPhone").innerHTML = "";
                    document.getElementById("leadTopPhone").appendChild(phoneNumberHref);


                }else{
                    document.getElementById("leadTopPhone").innerHTML = "<i class=\"fa fa-phone\"></i> "+leadData.leadData.phone;
                    document.getElementById("leadTopPhone").classList.add("text-navy");
                }
            }else{
                document.getElementById("leadTopPhone").classList.remove("text-navy");
                document.getElementById("leadTopPhone").innerText = "None";
            }

            if (leadData.leadData.isVIP == 1){
                document.getElementById("vipLead").checked = true;
                document.getElementById("estimateVIP").innerHTML = '<span style="float: right;font-size: 13px;padding: 7px;" class="label label-primary">Hot Lead</span>';
            }else{
                document.getElementById("vipLead").checked = false;
                document.getElementById("estimateVIP").innerHTML = '';
            }

            if (leadData.leadData.isBadLead == 1){
                document.getElementById("badLeadReasons").innerText = "Bad Lead";
            }

            if (leadData.leadData.userCreated){
                document.getElementById("userHandle").innerText =  leadData.leadData.userCreated;
            }else{
                document.getElementById("userHandle").innerText = "---";
            }
            document.getElementById("dateReceived").innerHTML = "<i class=\"fa fa-clock-o\"></i> "+ leadData.leadData.dateReceived;

            document.getElementById("leadNotes").innerText = leadData.leadData.notes;

        },
        showSectionById:function(tabId){
            if ( $("#"+tabId).is(':visible') ){
                $("#"+tabId).slideUp('fast');
                $("#moveDetailsSpan").show();
                $("#moveDetailsIcon").attr('class', 'fa fa-arrow-down');
            }else{
                $("#"+tabId).slideDown('fast');
                $("#moveDetailsSpan").hide();
                $("#moveDetailsIcon").attr('class', 'fa fa-arrow-up');
            }
        },
        showTabById:function(tabId){
            $("#tab-1").hide();
            $("#tab-2").hide();

            $("#tabMenu1").removeClass("active");
            $("#tabMenu2").removeClass("active");

            $("#tabMenu"+tabId).addClass("active");

            $("#tab-"+tabId).show();
        },
        setLeftSide:function(){
            leadController.page.setLeadStatusSelect();

            // =========== (START) SET DEPARTMENTS ===========
            var containerSelect = document.getElementById("department");
            containerSelect.innerHTML = "";


            if (movingLeadData.departments && movingLeadData.departments.length > 0) {
                var option = document.createElement('option');
                option.value = "";
                option.innerHTML = "Unassigned";

                containerSelect.appendChild(option);

                for (var i = 0; i < movingLeadData.departments.length; i++) {

                    var option = document.createElement('option');
                    option.value = movingLeadData.departments[i].id;
                    option.innerHTML = movingLeadData.departments[i].title;

                    if (movingLeadData.movingLeadData.departmentId == movingLeadData.departments[i].id){
                        option.selected = true;
                    }

                    containerSelect.appendChild(option);
                }
            }else{
                containerSelect.disabled = true;
            }
            // =========== (END) SET DEPARTMENTS ===========

            if (movingLeadData.movingLeadData.priority != null && movingLeadData.movingLeadData.priority != ""){
                document.getElementById("priority").value = movingLeadData.movingLeadData.priority;
            }
            if (movingLeadData.movingLeadData.reference != null && movingLeadData.movingLeadData.reference != ""){
                document.getElementById("reference").value = movingLeadData.movingLeadData.reference;
            }

            leadController.lead.setBoxDate(movingLeadData.movingLeadData.boxDeliveryDateStart,movingLeadData.movingLeadData.boxDeliveryDateEnd);
            leadController.lead.setPickupDate(movingLeadData.movingLeadData.pickupDateStart,movingLeadData.movingLeadData.pickupDateEnd);
            leadController.lead.setDeliveryDate(movingLeadData.movingLeadData.requestedDeliveryDateStart,movingLeadData.movingLeadData.requestedDeliveryDateEnd);
            // PART 3
            if (movingLeadData.movingLeadData.status >= 1){
                if(document.getElementById("bookTip")){document.getElementById("bookTip").style.display = "none";}
            }

            if (movingLeadData.canRefund){

                var container = document.getElementById("leadSettingsButtons");
                container.innerHTML = "";
                var div = document.createElement("div");
                div.classList.add("col-lg-4");
                div.classList.add("col-md-12");
                div.style.textAlign = "center";
                div.style.padding = "3px";

                var button = document.createElement("button");
                button.style.marginBottom = "5px";
                button.setAttribute('onclick','leadController.lead.deleteLead()');
                button.classList.add('btn');
                button.classList.add('btn-xs');
                button.classList.add('btn-block');
                button.classList.add('btn-danger');

                var buttonText = document.createTextNode("Delete Lead");

                button.appendChild(buttonText);
                div.appendChild(button);
                container.appendChild(div);

                var div = document.createElement("div");
                div.classList.add("col-lg-4");
                div.classList.add("col-md-12");
                div.style.padding = "3px";
                div.style.textAlign = "center";

                var button = document.createElement("button");
                button.style.marginBottom = "5px";
                button.setAttribute('onclick','leadController.lead.badLead()');
                button.classList.add('btn');
                button.classList.add('btn-xs');
                button.classList.add('btn-block');
                button.classList.add('btn-warning');

                var buttonText = document.createTextNode("Mark Bad Lead");

                button.appendChild(buttonText);
                div.appendChild(button);
                container.appendChild(div);

                var div = document.createElement("div");
                div.classList.add("col-lg-4");
                div.classList.add("col-md-12");
                div.style.padding = "3px";
                div.style.textAlign = "center";
                if (movingLeadData.hasRefund){
                    var button = document.createElement("button");
                    button.id = "refundLeadBtn";

                    button.setAttribute('style','background:#7cd1f9;border-color:#7cd1f9;');
                    button.classList.add('btn');
                    button.classList.add('btn-xs');
                    button.classList.add('btn-block');
                    button.classList.add('btn-warning');
                    button.setAttribute("disabled",true);

                    var buttonText = document.createTextNode("Request Sent");
                }else{
                    var button = document.createElement("button");
                    button.id = "refundLeadBtn";
                    button.setAttribute('onclick','leadController.lead.refund()');
                    button.setAttribute('style','background:#7cd1f9;border-color:#7cd1f9;');
                    button.classList.add('btn');
                    button.classList.add('btn-xs');
                    button.classList.add('btn-block');
                    button.classList.add('btn-warning');

                    var buttonText = document.createTextNode("Request Credit");
                }

                button.appendChild(buttonText);
                div.appendChild(button);
                container.appendChild(div);

            }else{
                var container = document.getElementById("leadSettingsButtons");
                container.innerHTML = "";
                var div = document.createElement("div");
                div.classList.add("col-md-6");
                div.style.textAlign = "center";

                var button = document.createElement("button");
                button.setAttribute('onclick','leadController.lead.deleteLead()');
                button.classList.add('btn');
                button.classList.add('btn-sm');
                button.classList.add('btn-block');
                button.classList.add('btn-danger');

                var buttonText = document.createTextNode("Delete Lead");

                button.appendChild(buttonText);
                div.appendChild(button);
                container.appendChild(div);

                var div = document.createElement("div");
                div.classList.add("col-md-6");
                div.style.textAlign = "center";

                var button = document.createElement("button");
                button.setAttribute('onclick','leadController.lead.badLead()');
                button.classList.add('btn');
                button.classList.add('btn-sm');
                button.classList.add('btn-block');
                button.classList.add('btn-warning');

                var buttonText = document.createTextNode("Mark Bad Lead");

                button.appendChild(buttonText);
                div.appendChild(button);
                container.appendChild(div);
            }

            // Check if lead need storage
            if (movingLeadData.movingLeadData.needStorage == '1') {
                $('#needStorageEstimate').css('display', '');
            }
            if (movingLeadData.movingLeadData.includeBoxDeliveryDate == "0"){
                $("#boxDatePicker").addClass("disabled-picker");
            }else{
                document.getElementById("includeBoxDeliveryDate").checked = true;
            }
            if (movingLeadData.movingLeadData.includePickupDate == "0"){
                $("#pickupDatePicker").addClass("disabled-picker");
            }else{
                document.getElementById("includePickupDate").checked = true;
            }
            if (movingLeadData.movingLeadData.includeRequestDeliveryDate == "0"){
                $("#requestedDeliveryDateStartPicker").addClass("disabled-picker");
            }else{
                document.getElementById("includeRequestDeliveryDate").checked = true;
            }
        },
        setLeadStatusSelect:function(){

            // clear the list
            $("#estimateChangeStatusDropdownList").html("");

            // Change to "New"
            var li = document.createElement("li");
            if (movingLeadData.movingLeadData.status == "0") {li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(0)");
            a.setAttribute("style", "margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label" style="font-size: 10px;padding: 2px 6px;">New</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "Pending"
            var li = document.createElement("li");
            if (movingLeadData.movingLeadData.status == "1") {li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(1)");
            a.setAttribute("style", "margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-warning" style="font-size: 10px;padding: 2px 6px;">Pending</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "Quoted"
            var li = document.createElement("li");
            if(movingLeadData.movingLeadData.status == "2"){li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(2)");
            a.setAttribute("style","margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-info" style="font-size: 10px;padding: 2px 6px;">Quoted</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "Booked"
            var li = document.createElement("li");
            if(movingLeadData.movingLeadData.status == "3"){li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(3)");
            a.setAttribute("style","margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-primary" style="font-size: 10px;padding: 2px 6px;">Booked</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "In progress"
            var li = document.createElement("li");
            if(movingLeadData.movingLeadData.status == "4"){li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(4)");
            a.setAttribute("style","margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-primary" style="font-size: 10px;padding: 2px 6px;">In progress</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "Storage/Transit"
            var li = document.createElement("li");
            if(movingLeadData.movingLeadData.status == "5"){li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(5)");
            a.setAttribute("style","margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-primary" style="font-size: 10px;padding: 2px 6px;">Storage/Transit</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "Completed"
            var li = document.createElement("li");
            if(movingLeadData.movingLeadData.status == "6"){li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(6)");
            a.setAttribute("style","margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-success" style="font-size: 10px;padding: 2px 6px;">Completed</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            // Change to "Cancelled"
            var li = document.createElement("li");
            if(movingLeadData.movingLeadData.status == "7"){li.className = "disabled";}
            var a = document.createElement("a");
            a.href = "#";
            a.setAttribute("onClick","leadController.lead.changeLeadStatus(7)");
            a.setAttribute("style","margin: 0;padding-left: 13px;");
            a.innerHTML = 'Change to <code class="label label-danger" style="font-size: 10px;padding: 2px 6px;">Cancelled</code>';
            li.appendChild(a);
            $("#estimateChangeStatusDropdownList").append(li);

            document.getElementById("estimateStatus").innerText = movingLeadData.movingLeadData.statusText;

            switch (movingLeadData.movingLeadData.status) {
                case "0":
                    document.getElementById("estimateStatusWrapper").className = "input-group";
                    break;
                case "1":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-warning";
                    break;
                case "2":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-info";
                    break;
                case "3":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-primary";
                    break;
                case "4":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-primary";
                    break;
                case "5":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-primary";
                    break;
                case "6":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-success";
                    break;
                case "7":
                    document.getElementById("estimateStatusWrapper").className = "input-group lead-status-danger";
                    break;
            }


            // Show the 'operation' tab if lead is booked
            if(movingLeadData.movingLeadData.status >= 3){
                $("#left-7").css("display","");
                $("#operationsLeftSide").css("display","");
            }else{
                $("#left-7").css("display","none");
                $("#operationsLeftSide").css("display","none");
            }

        },
        setLeadDetails:function(){

            document.getElementById("leadFullName").innerText = leadData.leadData.firstname + " " + leadData.leadData.lastname;
            document.getElementById("leadEditFirstname").value = leadData.leadData.firstname;
            document.getElementById("leadEditLastname").value = leadData.leadData.lastname;
            if (leadData.leadData.phone){
                document.getElementById("leadPhone").innerText = leadData.leadData.phone;
                document.getElementById("leadEditPhone").value = leadData.leadData.phone;
                document.getElementById("leadPhone").classList.add("text-navy");
            }else{
                document.getElementById("leadPhone").innerText = "None";
                document.getElementById("leadPhone").classList.remove("text-navy");
            }
            if (leadData.leadData.phone2){
                document.getElementById("leadSecondPhone").innerText = leadData.leadData.phone2;
                document.getElementById("leadEditPhone2").value = leadData.leadData.phone2;
                document.getElementById("leadSecondPhone").classList.add("text-navy");
            }else{
                document.getElementById("leadSecondPhone").innerText = "None";
                document.getElementById("leadSecondPhone").classList.remove("text-navy");
            }
            if (leadData.leadData.email){
                document.getElementById("leadEmail").innerText = leadData.leadData.email;
                document.getElementById("leadEditEmail").value = leadData.leadData.email;
                document.getElementById("leadEmail").classList.add("text-navy");
            }else{
                document.getElementById("leadEmail").innerText = "None";
                document.getElementById("leadEmail").classList.remove("text-navy");
            }
            if (leadData.leadData.providerName){
                document.getElementById("leadProvider").innerText = leadData.leadData.providerName;
                document.getElementById("leadEditProviderName").value = leadData.leadData.providerName;
                document.getElementById("leadProvider").classList.add("text-navy");
            }else{
                document.getElementById("leadProvider").innerText = "None";
                document.getElementById("leadProvider").classList.remove("text-navy");
            }
            if (leadData.leadData.ipAddress){
                document.getElementById("leadIp").innerText = leadData.leadData.ipAddress;
                document.getElementById("leadEditIpAddress").value = leadData.leadData.ipAddress;
                document.getElementById("leadIp").classList.add("text-navy");
            }else{
                document.getElementById("leadIp").innerText = "None";
                document.getElementById("leadIp").classList.remove("text-navy");
            }
            if (leadData.leadData.comments){
                document.getElementById("leadEditComments").value = leadData.leadData.comments;
                document.getElementById("leadComment").innerHTML = leadData.leadData.comments;
            }else{
                document.getElementById("leadComment").innerHTML = "";
            }
            if (leadData.leadData.notes){
                document.getElementById("leadNotes").value = leadData.leadData.notes;
            }
        },
        setSettings:function(){
            if(leadData.leadData.isNM == "1" || leadData.leadData.request == "" || leadData.leadData.request == null || leadData.leadData.providerId == null){
                document.getElementById("leadRawPostDataDiv").style.display = "none";
            }else{
                document.getElementById("leadRawPostDataDiv").style.display = "";
            }
        },
        setMovingLeadDetails:function(){
            // From Details
            var fromText = "";

            if (movingLeadData.movingLeadData.fromCity){
                fromText += movingLeadData.movingLeadData.fromCity+", ";
                document.getElementById("moveEditFromCity").value = movingLeadData.movingLeadData.fromCity;
            }

            if (movingLeadData.movingLeadData.fromState){
                fromText += movingLeadData.movingLeadData.fromState+", ";
                document.getElementById("moveEditFromState").value = movingLeadData.movingLeadData.fromState;
            }

            if (movingLeadData.movingLeadData.fromZip){
                fromText += movingLeadData.movingLeadData.fromZip+", ";
                document.getElementById("moveEditFromZip").value = movingLeadData.movingLeadData.fromZip;
            }
            fromText = fromText.substring(0,fromText.length -2);

            if (fromText == "") {fromText = "None";document.getElementById("movingFrom").classList.remove("text-navy");}else{
                document.getElementById("movingFrom").classList.add("text-navy");
            }
            document.getElementById("movingFrom").innerText = fromText;
            document.getElementById("movingFrom").setAttribute("title",fromText);

            if (movingLeadData.movingLeadData.fromAddress) {
                document.getElementById("movingFromAddress").innerText = movingLeadData.movingLeadData.fromAddress;
                document.getElementById("moveEditFromAddress").value = movingLeadData.movingLeadData.fromAddress;
                document.getElementById("movingFromAddress").classList.add("text-navy");
            }else{document.getElementById("movingFromAddress").innerText = "None";document.getElementById("movingFromAddress").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.fromLevel) {
                document.getElementById("movingFromLevel").innerText = movingLeadData.movingLeadData.fromLevel;
                document.getElementById("moveEditFromLevel").value = movingLeadData.movingLeadData.fromLevel;
                document.getElementById("movingFromLevel").classList.add("text-navy");
            }else{document.getElementById("movingFromLevel").innerText = "None";document.getElementById("movingFromLevel").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.fromFloor) {
                document.getElementById("movingFromFloor").innerText = movingLeadData.movingLeadData.fromFloor;
                document.getElementById("moveEditFromFloor").value = movingLeadData.movingLeadData.fromFloor;
                document.getElementById("movingFromFloor").classList.add("text-navy");
            }else{document.getElementById("movingFromFloor").innerText = "None";document.getElementById("movingFromFloor").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.fromAptNumber) {
                document.getElementById("movingFromApt").innerText = movingLeadData.movingLeadData.fromAptNumber;
                document.getElementById("moveEditFromApartment").value = movingLeadData.movingLeadData.fromAptNumber;
                document.getElementById("movingFromApt").classList.add("text-navy");
            }else{document.getElementById("movingFromApt").innerText = "None";document.getElementById("movingFromApt").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.fromAptType) {
                document.getElementById("movingFromAptType").innerText = movingLeadData.movingLeadData.fromAptType;
                document.getElementById("moveEditFromApartmentType").value = movingLeadData.movingLeadData.fromAptType;
                document.getElementById("movingFromAptType").classList.add("text-navy");
            }else{document.getElementById("movingFromAptType").innerText = "None";document.getElementById("movingFromAptType").classList.remove("text-navy");}
            
            // To Details
            var toText = "";

            if (movingLeadData.movingLeadData.toCity){
                toText += movingLeadData.movingLeadData.toCity+", ";
                document.getElementById("moveEditToCity").value = movingLeadData.movingLeadData.toCity;
            }

            if (movingLeadData.movingLeadData.toState){
                toText += movingLeadData.movingLeadData.toState+", ";
                document.getElementById("moveEditToState").value = movingLeadData.movingLeadData.toState;
            }

            if (movingLeadData.movingLeadData.toZip){
                toText += movingLeadData.movingLeadData.toZip+", ";
                document.getElementById("moveEditToZip").value = movingLeadData.movingLeadData.toZip;
            }
            toText = toText.substring(0,toText.length -2);
            if (toText == "") {toText = "None";document.getElementById("movingTo").classList.remove("text-navy");}else{
                document.getElementById("movingTo").classList.add("text-navy");
            }
            document.getElementById("movingTo").innerText = toText;
            document.getElementById("movingTo").setAttribute("title",toText);

            if (movingLeadData.movingLeadData.toAddress) {
                document.getElementById("movingToAddress").innerText = movingLeadData.movingLeadData.toAddress;
                document.getElementById("moveEditToAddress").value = movingLeadData.movingLeadData.toAddress;
                document.getElementById("movingToAddress").classList.add("text-navy");
            }else{document.getElementById("movingToAddress").innerText = "None";document.getElementById("movingToAddress").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.toLevel) {
                document.getElementById("movingToLevel").innerText = movingLeadData.movingLeadData.toLevel;
                document.getElementById("moveEditToLevel").value = movingLeadData.movingLeadData.toLevel;
                document.getElementById("movingToLevel").classList.add("text-navy");
            }else{document.getElementById("movingToLevel").innerText = "None";document.getElementById("movingToLevel").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.toFloor) {
                document.getElementById("movingToFloor").innerText = movingLeadData.movingLeadData.toFloor;
                document.getElementById("moveEditToFloor").value = movingLeadData.movingLeadData.toFloor;
                document.getElementById("movingToFloor").classList.add("text-navy");
            }else{document.getElementById("movingToFloor").innerText = "None";document.getElementById("movingToFloor").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.toAptNumber) {
                document.getElementById("movingToApt").innerText = movingLeadData.movingLeadData.toAptNumber;
                document.getElementById("moveEditToApartment").value = movingLeadData.movingLeadData.toAptNumber;
                document.getElementById("movingToApt").classList.add("text-navy");
            }else{document.getElementById("movingToApt").innerText = "None";document.getElementById("movingToApt").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.toAptType) {
                document.getElementById("movingToAptType").innerText = movingLeadData.movingLeadData.toAptType;
                document.getElementById("moveEditToApartmentType").value = movingLeadData.movingLeadData.toAptType;
                document.getElementById("movingToAptType").classList.add("text-navy");
            }else{document.getElementById("movingToAptType").innerText = "None";document.getElementById("movingToAptType").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.moveDate) {
                document.getElementById("movingDate").innerText = movingLeadData.movingLeadData.moveDate;
                // document.getElementById("moveEditDate").value = movingLeadData.movingLeadData.moveDateFormated;
                leadController.lead.setMoveDate(movingLeadData.movingLeadData.moveDateFormated);
                document.getElementById("movingDate").classList.add("text-navy");
            }else{document.getElementById("movingDate").innerText = "None";document.getElementById("movingDate").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.typeOfMove == 0) {
                document.getElementById("movingType").innerText = "Local Moving";
                document.getElementById("moveEditType").value = movingLeadData.movingLeadData.typeOfMove;
            }else{
                document.getElementById("movingType").innerText = "Long Distance";
                document.getElementById("moveEditType").value = movingLeadData.movingLeadData.typeOfMove;
            }

            if (movingLeadData.movingLeadData.moveSizeLBS && movingLeadData.movingLeadData.moveSizeLBS > 0) {

                var moveSizeNumber = 0;
                if(movingLeadData.movingSettingsData.calculateBy == "1"){
                    moveSizeNumber = movingLeadData.movingLeadData.moveSizeCF;
                    document.getElementById("moveSizeNumberLabel").innerHTML = "Move Size (CF)";
                    document.getElementById("moveEditSizeNumberLabel").innerHTML = "Move Size (CF)";
                }else{
                    moveSizeNumber = movingLeadData.movingLeadData.moveSizeLBS;
                    document.getElementById("moveSizeNumberLabel").innerHTML = "Move Size (LBS)";
                    document.getElementById("moveEditSizeNumberLabel").innerHTML = "Move Size (LBS)";
                }

                document.getElementById("movingSizeNumber").innerText = moveSizeNumber;
                document.getElementById("moveEditSizeNumber").value = moveSizeNumber;
            }else{document.getElementById("movingSizeNumber").innerText = "None";document.getElementById("movingSizeNumber").classList.remove("text-navy");}

            if (movingLeadData.movingLeadData.moveSize) {
                document.getElementById("movingSize").innerText = movingLeadData.movingLeadData.moveSize;
                document.getElementById("moveEditSize").value = movingLeadData.movingLeadData.moveSize;
            }else{document.getElementById("movingSize").innerText = "None";document.getElementById("movingSize").classList.remove("text-navy");}

            // auto Transport
            if (movingLeadData.movingLeadData.autoTransport){
                document.getElementById("movingAutoTransport").innerHTML = "";
                document.getElementById('autoTransport').innerHTML = "";

                for (var i = 0; i < movingLeadData.movingLeadData.autoTransport.length; i++) {
                    if (movingLeadData.movingLeadData.autoTransport[i].make && movingLeadData.movingLeadData.autoTransport[i].model && movingLeadData.movingLeadData.autoTransport[i].year && movingLeadData.movingLeadData.autoTransport[i].runs) {
                        leadController.autoTransport.addVehicle();
                        document.getElementById("make" + countVehicles).value = movingLeadData.movingLeadData.autoTransport[i].make;
                        leadController.autoTransport.updateModel(countVehicles, movingLeadData.movingLeadData.autoTransport[i].make);
                        document.getElementById("model" + countVehicles).value = movingLeadData.movingLeadData.autoTransport[i].model;
                        document.getElementById("year" + countVehicles).value = movingLeadData.movingLeadData.autoTransport[i].year;
                        document.getElementById("runs" + countVehicles).value = movingLeadData.movingLeadData.autoTransport[i].runs;
                        var runs;
                        if (movingLeadData.movingLeadData.autoTransport[i].runs == 1) {
                            runs = "Running";
                        } else {
                            runs = "Not Running";
                        }
                        document.getElementById("movingAutoTransport").innerHTML = document.getElementById("movingAutoTransport").innerHTML + movingLeadData.movingLeadData.autoTransport[i].make + ", " + movingLeadData.movingLeadData.autoTransport[i].model + ", " + movingLeadData.movingLeadData.autoTransport[i].year + ", " + runs + "<br>";
                    }
                }
            }else{
                document.getElementById("movingAutoTransport").innerHTML = "None";
                document.getElementById("movingAutoTransport").classList.remove("text-navy");
            }

            // On-site estimator
            if (movingLeadData.movingLeadData.estimator) {
                document.getElementById("onSiteEstimator").innerText = movingLeadData.movingLeadData.estimator;
                document.getElementById("onSiteEstimatorEdit").value = movingLeadData.movingLeadData.estimator;
            }else{document.getElementById("onSiteEstimator").innerText = "None";document.getElementById("onSiteEstimator").classList.remove("text-navy");}


            if (movingLeadData.movingLeadData.bindingType != null && movingLeadData.movingLeadData.bindingType != ""){
                document.getElementById("binding").value = movingLeadData.movingLeadData.bindingType;

                if(movingLeadData.movingLeadData.bindingType == "1"){
                    document.getElementById("bindingText").innerText = "Binding";
                }else if(movingLeadData.movingLeadData.bindingType == "0"){
                    document.getElementById("bindingText").innerText = "Non-binding";
                }else{
                    document.getElementById("bindingText").innerText = "None";
                    document.getElementById("bindingText").classList.remove("text-navy");
                }

            }else{
                document.getElementById("binding").value = "";
                document.getElementById("bindingText").innerText = "None";
                document.getElementById("bindingText").classList.remove("text-navy");
            }

            if (movingLeadData.movingLeadData.needStorage == 1){
                document.getElementById("needStorageText").innerText = "Yes";
                document.getElementById("needStorage").checked = true;
            }else{
                document.getElementById("needStorageText").innerText = "No";
                document.getElementById("needStorage").checked = false;
            }

        },
        setLeftLinks:function (id) {
            $("#leftPager .myLinks").each(function(){
                if($(this).hasClass("active")){
                    $(this).removeClass("active");
                }
            });
            $("#left-"+id).addClass("active");
            // id:
            // 1 = lead & moving
            // 2 = contact client
            // 3 = client inventory
            // 4 = move estimate
            // 5 = client updated info
            // 6 = files & documents
            // 7 = operations
            // 8 = claims
            // continue Here

            $("#pageContainer > div").each(function(){
                $(this).hide();
            });
            $("#page-"+id).show();
            if (id == '5'|| id == 5){
                leadController.page.setClientUpdatedInfoToSeen();
            }
            if (id == 7){
                leadController.moving.operations.jobAcceptance.getForms();
            }
            if (id == 8){
                leadController.lead.claims.getClaims();
            }
            if (id == 9){
                leadController.lead.getLeadPaymentDetails();
            }
        },
        setClientUpdatedInfoToSeen:function () {
            var strUrl = BASE_URL+'/console/actions/leads/markAsSeen.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId,
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data == true){
                        $("#newUpdatedData").css("display","none");
                        return true;
                    }else{
                        return false;
                    }
                }catch (e) {
                    return false;
                }
            });
        },
        prepareGoogleMap:function(){

            // Set the first and second addresses
            var leadFrom = movingLeadData.movingLeadData.fromState + " " + movingLeadData.movingLeadData.fromCity + " " + movingLeadData.movingLeadData.fromZip + " " + movingLeadData.movingLeadData.fromAddress;
            var leadTo = movingLeadData.movingLeadData.toState + " " + movingLeadData.movingLeadData.toCity + " " + movingLeadData.movingLeadData.toZip + " " + movingLeadData.movingLeadData.toAddress;

            var firstAddress = false;
            var secondAddress = false;

            if (leadFrom != "" && leadFrom != false) {
                firstAddress = leadFrom;
                if(leadTo != "" && leadTo != false){
                    secondAddress = leadTo;
                }
            }else{
                if(leadTo != "" && leadTo != false){
                    firstAddress = leadTo;
                }
            }

            directionsDisplay = new google.maps.DirectionsRenderer();

            if(firstAddress != false) {
                var geocoder = new google.maps.Geocoder();

                var origin;
                var destination;

                // Get the cordinates for the first address (it's not neccesarry the lead 'from' data - it could also be the lead 'to' data)
                geocoder.geocode({'address': firstAddress}, function (results, status) {
                    try {
                        if (status === 'OK') {
                            origin = results[0].geometry.location;

                            // Initialize the google map with the 'middle of USA' cordinates
                            map = new google.maps.Map(document.getElementById('map'), {
                                center: {lat: 37.0902, lng: 95.7129},
                                zoom: 8
                            });

                            // Set the map with the 'from' data
                            map.setCenter(origin);
                            google.maps.event.trigger(map, "resize");
                            directionsDisplay.setMap(map);

                            // Show the map
                            document.getElementById("map").style.display = "";

                            // Get the cordinates for the 'to' data
                            if (secondAddress != "" && secondAddress != false) {
                                geocoder.geocode({'address': secondAddress}, function (results, status) {
                                    try {
                                        if (status === 'OK') {

                                            destination = results[0].geometry.location;

                                            // Set the route
                                            directionsService.route({
                                                origin: origin,
                                                destination: destination,
                                                travelMode: 'DRIVING',
                                                avoidHighways: false,
                                                avoidTolls: false
                                            }, function (response, status) {
                                                if (status === 'OK') {
                                                    directionsDisplay.setDirections(response);
                                                    routeBounds = response.routes[0].bounds;
                                                }
                                            });

                                            // Set the distance traveling
                                            var service = new google.maps.DistanceMatrixService;
                                            service.getDistanceMatrix({
                                                origins: [origin],
                                                destinations: [destination],
                                                travelMode: 'DRIVING',
                                                unitSystem: google.maps.UnitSystem.IMPERIAL,
                                                avoidHighways: false,
                                                avoidTolls: false
                                            }, function(response, status) {
                                                if (status !== 'OK') {

                                                } else {
                                                    var results = response.rows[0].elements;
                                                    if (results[0].status != "ZERO_RESULTS") {
                                                        document.getElementById("distanceMiles").innerHTML = results[0].distance.text;
                                                    }else{

                                                    }
                                                }
                                            });



                                        }
                                    } catch (e) {

                                    }
                                });
                            }

                        }
                    } catch (e) {

                    }
                });
            }else{
                // If no 'first address' - hide the map
                document.getElementById("map").style.display = "none";
            }
        },
        showTermsInEstimatePageInfo: function(){

            var content = document.getElementById("estimateSettingsShowTerms");

            // Make a copy of the dom element so we son't display the original
            var cln = content.cloneNode(true);
            cln.style.display = "";

            swal({
                className: "swal-responsive-on-mobile",
                content: cln,
                buttons: {
                    skip:{
                        text:"Got It"
                    }
                }
            });
        },
        showESignatueInEstimatePageInfo: function(){
            var content = document.getElementById("estimateSettingsAllowEsignature");

            // Make a copy of the dom element so we son't display the original
            var cln = content.cloneNode(true);
            cln.style.display = "";

            swal({
                content: cln,
                buttons: {
                    skip:{
                        text:"Got It"
                    }
                }
            });
        },
        showInventoryInEstimatePageInfo: function(){
            var content = document.getElementById("estimateSettingsShowInventory");

            // Make a copy of the dom element so we son't display the original
            var cln = content.cloneNode(true);
            cln.style.display = "";

            swal({
                content: cln,
                buttons: {
                    skip:{
                        text:"Got It"
                    }
                }
            });
        },
        showPaymentsInEstimatePageInfo: function(){
            var content = document.getElementById("estimateSettingsShowPayments");

            // Make a copy of the dom element so we son't display the original
            var cln = content.cloneNode(true);
            cln.style.display = "";

            swal({
                content: cln,
                buttons: {
                    skip:{
                        text:"Got It"
                    }
                }
            });
        },
            showEmailUnsubscribedWarning:function () {
            // SET LEAD CONTACT (UNSUBSCRIBED WARNING)
            if(leadData.isEmailUnsibscribed == true){
                document.getElementById("emailHasBeenUnsubscribed").style.display = "";
                document.getElementById("leadEmailThatUnsubscribed").innerHTML = " ("+leadData.leadData.email+")";
            }else{
                document.getElementById("emailHasBeenUnsubscribed").style.display = "none";
                document.getElementById("leadEmailThatUnsubscribed").innerHTML = "";
            }
        }
    },
    lead: {
        payments:{
            setPaymentTable:function (data) {
                var container = document.getElementById("paymentTable");

                var tr = document.createElement("tr");

                var td = document.createElement("td");
                td.id = "amount-"+data.id;
                var tdText = document.createTextNode("$"+data.paymentTotal);

                td.appendChild(tdText);
                tr.appendChild(td);

                var td = document.createElement("td");

                var div = document.createElement("div");
                div.setAttribute("style","width: 123px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;");
                div.innerHTML = data.description;

                td.appendChild(div);

                tr.appendChild(td);

                var td = document.createElement("td");
                if(data.paymentDateText == undefined){
                    tdText = document.createTextNode("Now");
                }else{
                    tdText = document.createTextNode(data.paymentDateText);
                }
                td.appendChild(tdText);
                tr.appendChild(td);


                var td = document.createElement("td");
                var a = document.createElement("a");
                a.setAttribute("onclick","showCustomModal('categories/iframes/moving/showPayment.php?id="+data.id+"')");
                a.classList.add("btn");
                a.classList.add("btn-info");
                a.classList.add("btn-xs");
                a.classList.add("btn-block");
                var aText = document.createTextNode("Show More");

                a.appendChild(aText);
                td.appendChild(a);
                tr.appendChild(td);

                container.appendChild(tr);
            },
            setCCAFTable:function (data) {
                var container = document.getElementById("ccafTable");

                var tr = document.createElement("tr");

                var td = document.createElement("td");
                var tdText = document.createTextNode("$"+data.signedAmount);

                td.appendChild(tdText);
                tr.appendChild(td);

                var td = document.createElement("td");

                var div = document.createElement("div");
                div.setAttribute("style","width: 123px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;");
                div.innerHTML = data.signedName;

                td.appendChild(div);

                tr.appendChild(td);

                var td = document.createElement("td");
                if(data.signDate == undefined){
                    tdText = document.createTextNode("Now");
                }else{
                    tdText = document.createTextNode(data.signDateText);
                }
                td.appendChild(tdText);
                tr.appendChild(td);

                var td = document.createElement("td");
                var a = document.createElement("a");
                a.setAttribute("onclick","showCustomModal('categories/iframes/moving/showCCAF.php?id="+data.id+"')");
                a.classList.add("btn");
                a.classList.add("btn-info");
                a.classList.add("btn-xs");
                a.classList.add("btn-block");
                var aText = document.createTextNode("Show More");

                a.appendChild(aText);
                td.appendChild(a);
                tr.appendChild(td);

                container.appendChild(tr);
            },
            addPayment: function(CCAF){

                var content = document.createElement("div");

                var iframe = document.createElement("iframe");
                iframe.src = BASE_URL+"/console/categories/iframes/addPayment.php";

                iframe.setAttribute("style","border:none;width: 100%;height: 790px;margin-bottom:-5px");
                iframe.setAttribute("name","addPayment-iframe");
                iframe.onload= function() {
                    window.frames['addPayment-iframe'].paymentController.getCCAF(CCAF);
                };

                content.appendChild(iframe);

                swal({
                    className: "swal-add-payment",
                    content: content,
                    closeOnConfirm: false, //It does close the popup when I click on close button
                    buttons:false,

                });
            }
        },
        claims:{
            getClaims:function () {
                var strUrl = BASE_URL+'/console/actions/leads/lead/claims/getClaims.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "GET",
                    data: {
                        leadId: leadId
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {

                    $("#claimsTable").html("");
                    $("#claimsTotal").html("");

                    try{
                        data = JSON.parse(data);

                        if(data.length == 0){

                            $("#deleteSelectedClaims").hide();
                            $("#checkAllClaims").prop("checked", false);

                            var tr = document.createElement("tr");

                            var td = document.createElement("td");
                            td.setAttribute("colspan","6");
                            td.setAttribute("style","text-align: center;");
                            td.innerHTML = "No claims yet";

                            tr.appendChild(td);
                            document.getElementById("claimsTable").appendChild(tr);
                        }else{
                            $("#claimsTotal").html(data.length+" claims");
                        }

                        for(var i = 0;i<data.length;i++){

                            var singleClaim = data[i];


                            var tr = document.createElement("tr");

                            var td = document.createElement("td");

                            var checkbox  = document.createElement("input");
                            checkbox.setAttribute('type', 'checkbox');
                            checkbox.setAttribute('value', singleClaim.id);
                            checkbox.setAttribute('name', 'claimCheckbox');
                            checkbox.classList.add("claimCheckbox");

                            td.appendChild(checkbox);
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            td.setAttribute("style","text-align:left;");
                            td.innerHTML = singleClaim.fullName;

                            tr.appendChild(td);

                            var td = document.createElement("td");
                            td.setAttribute("style","text-align:left;");


                            if(singleClaim.filesAttached.length == 0){
                                td.innerHTML = '<span class="label label-warning">No Files</span>';
                            }else{
                                var ul = document.createElement("ul");
                                ul.setAttribute("style","list-style: none;padding-left: 0;margin-bottom: 0px;");

                                for(var j = 0;j<singleClaim.filesAttached.length;j++){
                                    var singleFile = singleClaim.filesAttached[j];

                                    var li = document.createElement("li");
                                    li.innerHTML = '<a target="_blank" href="'+BASE_URL+'/api/files/getFileByKey.php?key='+singleFile.key+'&fileURL='+singleFile.url+'">'+singleFile.name+'</a>';

                                    ul.appendChild(li);
                                }
                                td.appendChild(ul);
                            }

                            tr.appendChild(td);

                            var td = document.createElement("td");
                            td.setAttribute("style","text-align:center;");

                            if(!singleClaim.signature){
                                var span = document.createElement("span");
                                span.className = "label label-warning";
                                span.innerHTML = "Pending";
                            }else{
                                var span = document.createElement("span");
                                span.className = "label label-primary";
                                span.innerHTML = "Signed";

                            }
                            td.appendChild(span);

                            tr.appendChild(td);

                            var td = document.createElement("td");
                            td.setAttribute("style","text-align:center;");
                            var sendBtn = document.createElement("button");
                            sendBtn.className = "btn btn-default btn-xs";
                            sendBtn.setAttribute("onClick","leadController.lead.claims.sendClaimLink('"+ singleClaim.id+"','"+ singleClaim.firstName+"','"+singleClaim.lastName +"','" +singleClaim.email +"','" +singleClaim.phone1 + "','" + singleClaim.secretKey + "','" + singleClaim.secretToken +"')");
                            sendBtn.innerHTML = "Send claim link";
                            td.appendChild(sendBtn);

                            tr.appendChild(td);

                            var td = document.createElement("td");
                            td.setAttribute("style","text-align:center;");
                            var openBtn = document.createElement("button");
                            openBtn.className = "btn btn-default btn-xs";
                            openBtn.id = "";
                            openBtn.setAttribute("onClick","leadController.lead.claims.openClaim('"+ singleClaim.id+"')");
                            openBtn.innerHTML = "Open claim";
                            td.appendChild(openBtn);

                            tr.appendChild(td);


                            document.getElementById("claimsTable").appendChild(tr);
                        }

                    }catch (e) {
                        toastr.error("Could not get claims","Error");
                    }
                });
            },
            sendClaimLink: function (claimId,firstName,lastName,email,phone,secretKey,secretToken) {
                swal({
                    className: "CarrierSmsOrEmailSwal",
                    text: "Send claim link to " + firstName + " by",
                    buttons:{
                        cancel:false,
                        SMS:true,
                        Email:true
                    },

                }).then(function(value) {

                    switch (value) {

                        case "SMS":
                            if(leadData.leadData.jobNumber == null || leadData.leadData.jobNumber == ""){
                                var jobNumberOrLeadId = leadData.leadData.id;
                            }else{
                                var jobNumberOrLeadId = leadData.leadData.jobNumber;
                            }
                            var link =  BASE_YMQ_URL+"/leads.php?key=" + leadData.leadData.secretKey+"&page=claims&id="+claimId;

                            var txt = "Hi "+firstName+", click on the following link to view your claim from [CompanyName] : \r\n \r\n "+link;
                            sendSMScontroller.init(phone,undefined,txt);
                            break;

                        case "Email":
                            sendEmailsModal(null,email,[6,leadId,null,null,claimId]);
                            break;

                        default:
                    }
                });

            },
            deleteSelected:function(){
                var claimsIds = [];
                $.each($(".claimCheckbox:checked"), function(){
                    claimsIds.push($(this).val());
                });
                swal({
                    title: "Are you sure?",
                    text: "This will permanently delete these claims."  + "\n" + "If you are sure you want to to this please continue.\n",
                    icon: "warning",
                    dangerMode: true,
                    buttons: true,
                }).then( function (isConfirm) {
                    if (isConfirm) {
                        var strUrl = BASE_URL + '/console/actions/leads/lead/claims/deleteClaims.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "post",
                            data: {
                                claimsIds: claimsIds,
                                leadId: leadId
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {
                            try {
                                data = JSON.parse(data);
                                if (data == true) {
                                    if(claimsIds.length == 1){
                                        toastr.success("Claim Deleted", "Deleted");
                                    }else if(claimsIds.length > 1){
                                        toastr.success("Claims Deleted", "Deleted");
                                    }

                                    leadController.lead.claims.getClaims();
                                    $("#deleteSelectedClaims").hide();
                                }
                            } catch (e) {
                                toastr.error("Deleting Claims Failed");
                                $("#deleteSelectedClaims").hide();
                            }
                        });
                    }
                });
            },
            createClaim:function () {
                var iframe = document.createElement("iframe");
                iframe.src = BASE_URL+"/console/categories/iframes/leads/createClaim.php?leadId="+leadId;

                iframe.setAttribute("style","border:none;width: 100%;height: 100%;margin-bottom:-5px");
                iframe.setAttribute("name","lead-create-claim-iframe");
                iframe.onload= function() {
                    //window.frames['lead-create-claim-iframe'].init("AAA");
                };

                swal({
                    className: "swal-lead-create-claim",
                    content: iframe,
                    buttons:false,
                });
            },
            openClaim:function (claimId) {
                var link =  BASE_YMQ_URL+"/leads.php?key=" + leadData.leadData.secretKey+"&page=claims&id="+claimId;
                window.open(link,"_blank");
            }
        },
        getLeadDetails:function () {
            var strUrl = BASE_URL+'/console/actions/leads/lead/getLeadDetails.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try {
                    leadData = JSON.parse(data);
                }catch (e) {
                    // Failed loading this lead
                }
                // Set left side top info and notes
                leadController.page.setLeftSideTop();

                // Set lead settings tab
                leadController.page.setSettings();

                // Show a warning in the emails tab if the lead email have been unsubscribed
                leadController.page.showEmailUnsubscribedWarning();

                    // Set Lead Details Right Side
                    leadController.page.setLeadDetails();

                    // Set Client Update Data
                    if (leadData.updatedInfo) {

                        document.getElementById("left-5").style.display = "";

                        // lead Details
                        //firstname
                        document.getElementById("updateFirstname").innerText = leadData.updatedInfo.firstname;
                        document.getElementById("btnUpdateFirstname").setAttribute("onclick","updateClientInfo(1,'"+htmlEscape(leadData.updatedInfo.firstname)+"','leadEditFirstname')");
                        //lastname
                        document.getElementById("updateLastname").innerText = leadData.updatedInfo.lastname;
                        document.getElementById("btnUpdateLastname").setAttribute("onclick","updateClientInfo(1,'"+htmlEscape(leadData.updatedInfo.lastname)+"','leadEditLastname')");
                        //phone
                        document.getElementById("updatePhone").innerText = leadData.updatedInfo.phone;
                        document.getElementById("btnUpdatePhone").setAttribute("onclick","updateClientInfo(1,'"+htmlEscape(leadData.updatedInfo.phone)+"','leadEditPhone')");

                        document.getElementById("updateEmail").innerText = leadData.updatedInfo.email;
                        document.getElementById("btnUpdateEmail").setAttribute("onclick","updateClientInfo(1,'"+htmlEscape(leadData.updatedInfo.email)+"','leadEditEmail')");


                        document.getElementById("updateComments").innerText = leadData.updatedInfo.comments;
                        document.getElementById("btnUpdateComments").setAttribute("onclick","updateClientInfo(1,'"+htmlEscape(leadData.updatedInfo.comments)+"','leadEditComments')");

                        // moving Details
                        document.getElementById("updateFromState").innerText = leadData.updatedInfo.fromState;
                        document.getElementById("btnUpdateFState").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromState)+"','moveEditFromState')");

                        document.getElementById("updateToState").innerText = leadData.updatedInfo.toState;
                        document.getElementById("btnUpdateTState").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toState)+"','moveEditToState')");

                        document.getElementById("updateFromCity").innerText = leadData.updatedInfo.fromCity;
                        document.getElementById("btnUpdateFCity").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromCity)+"','moveEditFromCity')");

                        document.getElementById("updateToCity").innerText = leadData.updatedInfo.toCity;
                        document.getElementById("btnUpdateTCity").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toCity)+"','moveEditToCity')");

                        document.getElementById("updateFromZip").innerText = leadData.updatedInfo.fromZip;
                        document.getElementById("btnUpdateFZip").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromZip)+"','moveEditFromZip')");

                        document.getElementById("updateToZip").innerText = leadData.updatedInfo.toZip;
                        document.getElementById("btnUpdateTZip").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toZip)+"','moveEditToZip')");

                        document.getElementById("updateFromAddress").innerText = leadData.updatedInfo.fromAddress;
                        document.getElementById("btnUpdateFAddress").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromAddress)+"','moveEditFromAddress')");

                        document.getElementById("updateToAddress").innerText = leadData.updatedInfo.toAddress;
                        document.getElementById("btnUpdateTAddress").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toAddress)+"','moveEditToAddress')");

                        document.getElementById("updateFromLevel").innerText = leadData.updatedInfo.fromLevel;
                        document.getElementById("btnUpdateFLevel").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromLevel)+"','moveEditFromLevel')");

                        document.getElementById("updateToLevel").innerText = leadData.updatedInfo.toLevel;
                        document.getElementById("btnUpdateTLevel").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toLevel)+"','moveEditToLevel')");

                        document.getElementById("updateFromFloor").innerText = leadData.updatedInfo.fromFloor;
                        document.getElementById("btnUpdateFFloor").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromFloor)+"','moveEditFromFloor')");

                        document.getElementById("updateToFloor").innerText = leadData.updatedInfo.toFloor;
                        document.getElementById("btnUpdateTFloor").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toFloor)+"','moveEditToFloor')");

                        document.getElementById("updateFromApt").innerText = leadData.updatedInfo.fromAptNumber;
                        document.getElementById("btnUpdateFApt").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.fromAptNumber)+"','moveEditFromApartment')");

                        document.getElementById("updateToApt").innerText = leadData.updatedInfo.toAptNumber;
                        document.getElementById("btnUpdateTApt").setAttribute("onclick","updateClientInfo(2,'"+htmlEscape(leadData.updatedInfo.toAptNumber)+"','moveEditToApartment')");

                        var needStorageText = "No";
                        if(leadData.updatedInfo.needStorage == "1"){
                            needStorageText = "Yes";
                        }
                        document.getElementById("updateNeedStorage").innerText = needStorageText;
                        document.getElementById("btnUpdateNeedStorage").setAttribute("onclick","updateClientInfo(4,'"+htmlEscape(leadData.updatedInfo.needStorage)+"','needStorage')");

                        document.getElementById("updateMoveDate").innerText = leadData.updatedInfo.movingDate;
                        document.getElementById("btnUpdateMoveDate").setAttribute("onclick","updateClientInfo(3,'"+htmlEscape(leadData.updatedInfo.formattedMovingDate)+"','moveEditDate')");

                        // inventory
                        if (leadData.updatedInfo.inventory && leadData.updatedInfo.inventory.length > 0){
                            var container = document.getElementById("updateInventoryTable");
                            container.innerHTML = "";
                            for(var i=0;i<leadData.updatedInfo.inventory.length;i++){
                                var current = leadData.updatedInfo.inventory[i];

                                var tr = document.createElement("tr");
                                var td = document.createElement("td");
                                var tdText = document.createTextNode(current.name);
                                td.appendChild(tdText);
                                tr.appendChild(td);

                                var td = document.createElement("td");
                                var tdText = document.createTextNode(current.cf);
                                td.appendChild(tdText);
                                tr.appendChild(td);

                                var td = document.createElement("td");
                                var tdText = document.createTextNode(current.amount);
                                td.appendChild(tdText);
                                tr.appendChild(td);

                                container.appendChild(tr);
                            }
                        }else{
                            if (document.getElementById("updateInventory") != null){
                                document.getElementById("updateInventory").remove();
                            }
                        }



                        if (leadData.updatedInfo.isSeen == "0") {
                            $("#newUpdatedData").css("display", "block");
                        }
                    }

            });
        },
        getLeadMovingDetails:function () {
            var strUrl = BASE_URL+'/console/actions/leads/lead/getLeadMovingDetails.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                movingLeadData = JSON.parse(data);

                // Set Estimate Details Left Side
                leadController.page.setLeftSide();

                // Set Moving Details Right Side
                leadController.page.setMovingLeadDetails();

                // Set Client Inventory
                if (movingLeadData.estimateItems) {
                    document.getElementById("itemsTable").innerHTML = "";

                    for (var i = 0; i < movingLeadData.estimateItems.length; i++) {
                        leadController.moving.inventory.setInventory(movingLeadData.estimateItems[i]);
                    }
                    document.getElementById("tableTotalCf").innerHTML = "<b>Total Cubic Feet: </b>" + data.totalCF;
                    if (movingLeadData.totalCF > 0) {
                        $("#inventoryCFLabel").html("<span style='margin-right: 3px;' class='label label-success pull-right'>" + movingLeadData.totalCF + " CF</span>");
                    }else{
                        $("#inventoryCFLabel").html("");
                    }

                    totalCF = movingLeadData.totalCF;
                    if (movingLeadData.estimateItems.length > 0) {
                        $("#inventoryTotal").html('<span class="label label-primary pull-right">' + movingLeadData.estimateItems.length + ' Items</span>');
                    } else {
                        $("#inventoryTotal").html("");
                    }
                }

                // Set Inventory
                leadController.moving.inventory.setClientInventory();

                // Google Data
                if(leadGoogleMaps){
                    leadController.page.prepareGoogleMap();
                }

                // Set Estimates
                leadController.moving.estimate.initEstimates();

                leadController.moving.operations.setOperations(movingLeadData.movingLeadData.operations);
            });
        },
        getLeadPaymentDetails:function () {
            var strUrl = BASE_URL+'/console/actions/leads/lead/getLeadPaymentDetails.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try {
                    data = JSON.parse(data);
                }catch (e) {
                    // Failed loading this lead
                }
                    // Set Payments
                    document.getElementById("paymentTable").innerHTML = "";
                    if (data.paymentData && data.paymentData.length > 0) {
                        for (var i = 0; i < data.paymentData.length; i++) {
                            leadController.lead.payments.setPaymentTable(data.paymentData[i]);
                        }

                        var container = document.getElementById("paymentTable");
                        var tr = document.createElement("tr");
                        tr.id = "noPaymentRow";
                        var td = document.createElement("td");
                        td.setAttribute("colspan", "4");
                        td.style.textAlign = "left";

                        var div = document.createElement("div");
                        div.setAttribute("style","float: left;font-weight: bold;");
                        div.innerHTML = "Total: $"+data.totalPayments;

                        td.appendChild(div);

                        if(data.paymentsLeft != null) {
                            var div = document.createElement("div");
                            div.setAttribute("style", "float: right;");
                            div.innerHTML = "Balance remaining: $" + data.paymentsLeft;

                            td.appendChild(div);
                        }
                        tr.appendChild(td);
                        container.appendChild(tr);

                    }else{
                        var container = document.getElementById("paymentTable");
                        var tr = document.createElement("tr");
                        tr.id = "noPaymentRow";
                        var td = document.createElement("td");
                        td.setAttribute("colspan", "4");
                        td.style.textAlign = "center";
                        var tdText = document.createTextNode("No \Payments Yet");
                        td.appendChild(tdText);
                        tr.appendChild(td);
                        container.appendChild(tr);
                    }



                    // Set CCAF
                    document.getElementById("ccafTable").innerHTML = "";
                    if (data.ccaf && data.ccaf.length > 0) {
                        for (var i = 0; i < data.ccaf.length; i++) {
                            leadController.lead.payments.setCCAFTable(data.ccaf[i]);
                        }
                    }else{
                        var container = document.getElementById("ccafTable");
                        var tr = document.createElement("tr");
                        tr.id = "noPaymentRow";
                        var td = document.createElement("td");
                        td.setAttribute("colspan", "4");
                        td.style.textAlign = "center";
                        var tdText = document.createTextNode("No Credit card authorizations yet");
                        td.appendChild(tdText);
                        tr.appendChild(td);
                        container.appendChild(tr);
                    }

            });
        },
        toggleVip:function () {
            var vipLead = document.getElementById("vipLead").checked;

            var strUrl = BASE_URL+'/console/actions/leads/updateLeadVIP.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId,
                    vip:vipLead
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data == true){


                        if(vipLead){
                            leadData.leadData.isVIP = "1";
                        }else{
                            leadData.leadData.isVIP = "0";
                        }
                       leadController.page.setLeftSideTop();
                    }else{
                        return false;
                    }
                }catch (e) {
                    return false;
                }
            });
        },
        updateLeadNotes:function () {
            var notes = document.getElementById("leadNotes").value;
            var strUrl = BASE_URL+'/console/actions/leads/updateLeadNotes.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId,
                    notes: notes
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    if (data == true) {
                        estimateLadda.ladda("stop");
                    }else{
                        toastr.error("Failed Saving Lead Notes","Failed");
                    }
                }
                catch (e) {
                    toastr.error("Failed Saving Lead Notes","Failed");
                }
            });
        },
        updateLeadData:function (leadData) {
            leadData.firstname = leadData.firstname.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function(letter) {
                return letter.toUpperCase();
            });
            leadData.lastname = leadData.lastname.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function(letter) {
                return letter.toUpperCase();
            });
            var strUrl = BASE_URL+'/console/actions/leads/updateDetails.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId,
                    leadData: leadData
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (jsondata) {
                try {
                    var data = JSON.parse(jsondata);
                    if (data == true){
                        leadLadda.ladda("stop");
                        if (leadData.firstname || leadData.lastname){
                            document.getElementById("leadFullName").classList.add("text-navy");
                            document.getElementById("leadTopName").classList.add("text-navy");
                        }
                        document.getElementById("leadFullName").innerText = leadData.firstname + " " + leadData.lastname;
                        document.getElementById("leadTopName").innerText = leadData.firstname + " " + leadData.lastname;

                        if (leadData.phone != "") {
                            if (isaphonenumber(leadData.phone)) {

                                var phoneNumberHref = document.createElement('a');
                                phoneNumberHref.classList.add("text-navy");
                                phoneNumberHref.href = "tel:+01-" + leadData.phone;
                                phoneNumberHref.innerHTML = "<i class=\"fa fa-phone\"></i> "+leadData.phone;
                                document.getElementById("leadTopPhone").innerHTML = "";
                                document.getElementById("leadTopPhone").appendChild(phoneNumberHref);
                                document.getElementById("leadTopPhone").classList.add("text-navy");

                            }else{
                                document.getElementById("leadTopPhone").innerHTML = "<i class=\"fa fa-phone\"></i> "+leadData.phone;
                                document.getElementById("leadTopPhone").classList.add("text-navy");
                            }
                            document.getElementById("leadPhone").innerText = leadData.phone;
                            document.getElementById("leadPhone").classList.add("text-navy");
                        }else{
                            document.getElementById("leadPhone").innerText = "None";
                            document.getElementById("leadTopPhone").innerText = "None";
                            document.getElementById("leadTopPhone").classList.remove("text-navy");
                            try{document.getElementById("leadPhone").classList.remove("text-navy");}catch(e){}
                        }

                        if (leadData.phone2 != "") {
                            document.getElementById("leadSecondPhone").innerText = leadData.phone2;
                            document.getElementById("leadSecondPhone").classList.add("text-navy");
                        }else{
                            document.getElementById("leadSecondPhone").innerText = "None";
                            try{document.getElementById("leadSecondPhone").classList.remove("text-navy");}catch(e){}
                        }
                        if (leadData.email != "") {
                            document.getElementById("leadEmail").innerText = leadData.email;
                            document.getElementById("leadTopEmail").innerText = leadData.email;
                            document.getElementById("leadEmail").classList.add("text-navy");
                            document.getElementById("leadTopEmail").classList.add("text-navy");
                            document.getElementById("emailIcon").classList.add("text-navy");
                        }else{
                            document.getElementById("leadTopEmail").classList.remove("text-navy");
                            document.getElementById("emailIcon").classList.remove("text-navy");
                            document.getElementById("leadEmail").innerText = "None";
                            document.getElementById("leadTopEmail").innerText = "None";
                            try{document.getElementById("leadEmail").classList.remove("text-navy");}catch(e){}
                        }
                        if (leadData.comments != "") {
                            document.getElementById("leadComment").innerHTML = leadData.comments;
                        }else{
                            document.getElementById("leadComment").innerHTML = "";
                        }

                        // reaload sms side iframe
                        document.getElementById('sms-side-iframe').contentWindow.location.reload();

                        $("#leadDetails").show();
                        $("#leadDetailsEdit").hide();

                        leadController.lead.getLeadDetails();

                        toastr.success("Lead details updated","Saved");

                        return true;
                    }else{
                        leadLadda.ladda("stop");
                        toastr.error("Failed Saving Lead Details","Failed");
                        Sentry.withScope(function(scope) {
                            Sentry.setExtra("data", jsondata);
                            Sentry.setExtra("comments", "leadController - updateLeadData");
                            Sentry.captureException(new Error("Failed Saving Lead Details"));
                        });
                        return false;
                    }
                }catch (e) {
                    leadLadda.ladda("stop");
                    toastr.error("Failed Saving Lead Details","Failed");
                    Sentry.withScope(function(scope) {
                        Sentry.setExtra("data", jsondata);
                        Sentry.setExtra("comments", "leadController - updateLeadData");
                        Sentry.captureException(e);
                    });
                    return false;
                }
            });
        },
        changeLeadStatus:function (status) {

            var strUrl = BASE_URL + '/console/actions/moving/leads/updateLeadStatus.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "post",
                data: {
                    id: leadId,
                    status: status
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (jsondata) {
                try {
                    var data = JSON.parse(jsondata);
                    if (data == true) {
                        if(status == 3){
                            leadController.moving.operations.assign();
                        }
                        leadController.lead.getLeadMovingDetails();
                        leadController.lead.getLeadLogs();
                    } else {
                        return false;
                    }
                } catch (e) {
                    Sentry.withScope(function(scope) {
                        Sentry.setExtra("data", jsondata);
                        Sentry.setExtra("comments", "leadController - changeLeadStatus");
                        Sentry.captureException(e);
                    });
                    return false;
                }
            });


        },
        updateLeadInfo:function () {
            swal({
                title: false,
                text: "Are you sure you would like to replace all the leads details?",
                icon: false,
                dangerMode: false,
                buttons: true,
            }).then(

                function (isConfirm){
                    if (isConfirm) {
                        leadController.lead.setLeadUpdateInfo("lead");
                    }
                });
        },
        updateMovingInfo:function () {
            swal({
                title: false,
                text: "Are you sure you would like to replace all the lead moving details?",
                icon: false,
                dangerMode: false,
                buttons: true,
            }).then(
                function (isConfirm) {
                    if (isConfirm) {
                        leadController.lead.setLeadUpdateInfo("moving");
                    }
                });
        },
        setLeadUpdateInfo:function (type) {
            var strUrl = BASE_URL+'/console/actions/moving/leads/setLeadUpdatedInfo.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"post",
                data:{
                    leadId:leadId,
                    type:type
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (jsondata) {
                try {
                    var data = JSON.parse(jsondata);
                    if (data == true){
                        if (type == "moving"){
                            leadController.lead.getLeadMovingDetails();
                            toastr.success("All of the lead moving details were replaced.","Saved");
                        }
                        if (type == "lead"){
                            leadController.lead.getLeadDetails();
                            toastr.success("All of the lead details were replaced.","Saved");
                        }
                    }else{
                        Sentry.withScope(function(scope) {
                            Sentry.setExtra("data", jsondata);
                            Sentry.setExtra("comments", "leadController - setLeadUpdateInfo");
                            Sentry.captureException(new Error("Cannot update Lead"));
                        });
                        toastr.error("Cannot update Lead","Fail");
                    }
                }catch (e) {
                    Sentry.withScope(function(scope) {
                        Sentry.setExtra("data", jsondata);
                        Sentry.setExtra("comments", "leadController - setLeadUpdateInfo");
                        Sentry.captureException(e);
                    });
                    toastr.error("Cannot update Lead", "Fail");
                }
            });
        },
        deleteLead:function () {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this lead",
                icon: "warning",
                dangerMode: true,
                buttons: true,
            }).then(function(isConfirm){
                if (isConfirm) {

                    var strUrl = BASE_URL+'/console/actions/leads/deleteLead.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method:"post",
                        data:{
                            leadId:leadId
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function (jsondata) {
                        try {
                            var data = JSON.parse(jsondata);
                            if (data == true){
                                window.location = BASE_URL+'/console/categories/leads/leads.php';
                            }else{
                                return false;
                            }
                        }catch (e) {
                            Sentry.withScope(function(scope) {
                                Sentry.setExtra("data", jsondata);
                                Sentry.setExtra("comments", "leadController - deleteLead");
                                Sentry.captureException(e);
                            });
                            return false;
                        }
                    });
                } else {

                }
            });
        },
        setMoveDate:function (theDate) {
            $('#moveEditDatePicker #moveEditDate').html(moment(theDate).format('MMMM D, YYYY'));

            $('#moveEditDatePicker').daterangepicker({
                singleDatePicker: true,
                format: 'MM/DD/YYYY',
                startDate: moment(theDate),
                endDate: moment(),
                dateLimit: { days: 365 },
                showDropdowns: true,
                showWeekNumbers: false,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Go',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                $('#moveEditDatePicker #moveEditDate').html(end.format('MMMM D, YYYY'));
            });

            $("#moveEditDate").on("DOMSubtreeModified",function(){

            });

        },
        setBoxDate:function (theDate,endDate) {
            $('#boxDatePicker #boxDate').html(moment(theDate).format('MM/D/YYYY HH:mm')+"-"+moment(endDate).format('MM/D/YYYY HH:mm'));

            $('#boxDatePicker').daterangepicker({
                singleDatePicker: false,
                format: 'MM/DD/YYYY HH:mm',
                startDate: moment(theDate),
                endDate: moment(endDate),
                dateLimit: {days: 365},
                showDropdowns: false,
                showWeekNumbers: false,
                timePicker: true,
                timePickerIncrement: 1,
                timePicker12Hour: false,
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Go',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                $('#boxDatePicker #boxDate').html(start.format('MM/D/YYYY HH:mm')+"-"+end.format('MM/D/YYYY HH:mm'));
            });

            $("#boxDate").on("DOMSubtreeModified", function () {

            });
        },
        setPickupDate:function (theDate,endDate) {
            $('#pickupDatePicker #pickupDate').html(moment(theDate).format('MM/D/YYYY HH:mm')+"-"+moment(endDate).format('MM/D/YYYY HH:mm'));

            $('#pickupDatePicker').daterangepicker({
                singleDatePicker: false,
                format: 'MM/DD/YYYY HH:mm',
                startDate: moment(theDate),
                endDate: moment(endDate),
                dateLimit: {days: 365},
                showDropdowns: true,
                showWeekNumbers: false,
                timePicker: true,
                timePickerIncrement: 1,
                timePicker12Hour: false,
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Go',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                $('#pickupDatePicker #pickupDate').html(start.format('MM/D/YYYY HH:mm')+"-"+end.format('MM/D/YYYY HH:mm'));
            });

            $("#pickupDate").on("DOMSubtreeModified", function () {
            });
        },
        setDeliveryDate:function (theDate,endDate) {
            $('#requestedDeliveryDateStartPicker #requestedDeliveryDateStart').html(moment(theDate).format('MM/D/YYYY HH:mm')+"-"+moment(endDate).format('MM/D/YYYY HH:mm'));

            $('#requestedDeliveryDateStartPicker').daterangepicker({
                singleDatePicker: false,
                format: 'MM/DD/YYYY HH:mm',
                startDate: moment(theDate),
                endDate: moment(endDate),
                dateLimit: { days: 365 },
                showDropdowns: true,
                showWeekNumbers: false,
                timePicker: true,
                timePickerIncrement: 1,
                timePicker12Hour: false,
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Go',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                $('#requestedDeliveryDateStartPicker #requestedDeliveryDateStart').html(start.format('MM/D/YYYY HH:mm')+"-"+end.format('MM/D/YYYY HH:mm'));
            });

            $("#requestedDeliveryDateStart").on("DOMSubtreeModified",function(){
            });

        },
        refund:function () {
            var values = [
                "Fake Name",
                "Fake Email",
                "Phone Number is a Fax",
                "Wrong Phone Number",
                "Disconnected Phone Number",
                "Already Booked",
                "Already Moved",
                "Pod Rental",
                "Not Moving",
                "Truck Rental",
                "Move Date Is 6 Or More Months Away",
                "Moving Only 1 Item",
                "Car Shipping Only",
                "Duplicate",
                "Shipping None Household Goods",
                "Unable to Make Contact",
                "Outside of the Pick-Up Radius",
                "Outside of the Delivery Radius",
                "Local Move",
                "Language Barrier"
            ];

            var content = document.createElement('div');

            var select = document.createElement("select");
            select.id = "creditReason";
            select.style.marginBottom = "5px";
            select.classList.add('form-control');
            var option = document.createElement("option");
            option.value = "";
            var optionText = document.createTextNode("Select Credit Reason");

            option.appendChild(optionText);
            select.appendChild(option);

            for(var i = 0;i<values.length;i++){
                var option = document.createElement("option");
                option.value = values[i];
                var optionText = document.createTextNode(values[i]);

                option.appendChild(optionText);
                select.appendChild(option);
            }

            var textarea = document.createElement('textarea');
            textarea.style.height = "150px";
            textarea.style.resize = "none";
            textarea.setAttribute("type",'text');
            textarea.setAttribute("placeholder",'Comments.. (optional)');
            textarea.id = "creditComment";
            textarea.classList.add('form-control');

            content.appendChild(select);
            content.appendChild(textarea);
            swal({
                title: false,
                content: content,
                buttons: {
                    save: {
                        text:"Submit Credit Request",
                        value:"send",

                    },
                    cancel: true
                },
                icon: false,
                dangerMode: true,
            }).then(function(value){
                if(value == "send"){
                    var reason = document.getElementById("creditReason").value;
                    var comments = document.getElementById("creditComment").value;
                    var strUrl = BASE_URL + '/console/actions/leads/markAsRefund.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method: "post",
                        data: {
                            leadId: leadId,
                            reason: reason,
                            comments: comments
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function (data) {
                        try {
                            data = JSON.parse(data);
                            if(data.status == 'true' || data.status == true){
                                $("#refundLeadBtn").attr("disabled",true);
                                $("#refundLeadBtn").text("Request Sent");
                                $("#refundLeadBtn").attr("onclick","toastr.success('lead credit request already sent','Sent')");
                                toastr.success(data.msg,"Request Sent");
                            }else{
                                leadController.lead.refund();
                                toastr.error(data.msg,"Error");
                            }
                        } catch (e) {
                            toastr.error("Request not sent","Error");
                        }
                    });
                }
            });
        },
        badLead:function () {
            var values = [
                "Fake Name",
                "Fake Email",
                "Phone Number is a Fax",
                "Wrong Phone Number",
                "Disconnected Phone Number",
                "Already Booked",
                "Already Moved",
                "Pod Rental",
                "Not Moving",
                "Truck Rental",
                "Move Date Is 6 Or More Months Away",
                "Moving Only 1 Item",
                "Car Shipping Only",
                "Duplicate",
                "Shipping None Household Goods",
                "Unable to Make Contact",
                "Outside of the Pick-Up Radius",
                "Outside of the Delivery Radius",
                "Local Move",
                "Language Barrier"
            ];
            var content = document.createElement("div");
            content.style.textAlign = "left";
            var names = [];


            // Get bad lead reasons
            var strUrl = BASE_URL + '/console/actions/leads/getBadLeadReasons.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "post",
                data: {
                    leadId: leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: false
            }).done(function (data) {
                try {
                    data = JSON.parse(data);
                    names = data;

                    for (var i = 0; i < values.length; i++) {
                        var val = values[i];
                        var container = document.createElement("div");
                        container.classList.add("checkbox");
                        container.classList.add("checkbox-primary");

                        var label = document.createElement("label");
                        label.setAttribute('for',val);

                        var labelText = document.createTextNode(val);

                        label.appendChild(labelText);

                        var input = document.createElement("input");
                        input.type = "checkbox";
                        input.id = val;
                        input.value = val;
                        if (names.includes(val)){
                            input.checked = true;
                        }

                        container.appendChild(input);
                        container.appendChild(label);

                        content.appendChild(container);
                    }

                    swal({
                        title: "Select Bad Lead Reasons",
                        content: content,
                        buttons: {
                            save: {
                                text:"Save",
                                value:"save"
                            },
                            cancel: true
                        },
                        icon: false,
                        dangerMode: true,
                    }).then(function(value){
                        if(value === "save"){
                            var choices = {
                                "Fake Name": document.getElementById("Fake Name").checked,
                                "Fake Email": document.getElementById("Fake Email").checked,
                                "Phone Number is a Fax": document.getElementById("Phone Number is a Fax").checked,
                                "Wrong Phone Number": document.getElementById("Wrong Phone Number").checked,
                                "Disconnected Phone Number": document.getElementById("Disconnected Phone Number").checked,
                                "Already Booked": document.getElementById("Already Booked").checked,
                                "Already Moved": document.getElementById("Already Moved").checked,
                                "Pod Rental": document.getElementById("Pod Rental").checked,
                                "Not Moving": document.getElementById("Not Moving").checked,
                                "Truck Rental": document.getElementById("Truck Rental").checked,
                                "Move Date Is 6 Or More Months Away": document.getElementById("Move Date Is 6 Or More Months Away").checked,
                                "Moving Only 1 Item": document.getElementById("Moving Only 1 Item").checked,
                                "Car Shipping Only": document.getElementById("Car Shipping Only").checked,
                                "Duplicate": document.getElementById("Duplicate").checked,
                                "Shipping None Household Goods": document.getElementById("Shipping None Household Goods").checked,
                                "Unable to Make Contact": document.getElementById("Unable to Make Contact").checked,
                                "Outside of the Pick-Up Radius": document.getElementById("Outside of the Pick-Up Radius").checked,
                                "Outside of the Delivery Radius": document.getElementById("Outside of the Delivery Radius").checked,
                                "Local Move": document.getElementById("Local Move").checked,
                                "Language Barrier": document.getElementById("Language Barrier").checked
                            };
                            var reasons = [];
                            var isBadLead = 0;
                            var spanText = "Bad Lead";

                            for (var i in choices){
                                if (choices[i] === true){
                                    reasons.push(i);
                                }
                            }

                            if (spanText.length > 0){
                                if (reasons.length > 0) {
                                    document.getElementById("badLeadReasons").innerText = spanText;
                                }else{
                                    document.getElementById("badLeadReasons").innerText = "";
                                }
                            }else{
                                document.getElementById("badLeadReasons").innerText = "";
                            }

                            if (reasons.length > 0){
                                isBadLead = 1;
                            }else{
                                reasons = "";
                            }

                            var strUrl = BASE_URL + '/console/actions/leads/markAsBadLead.php', strReturn = "";
                            jQuery.ajax({
                                url: strUrl,
                                method: "post",
                                data: {
                                    leadId: leadId,
                                    badLeadText: reasons,
                                    isBadLead: isBadLead
                                },
                                success: function (html) {
                                    strReturn = html;
                                },
                                async: true
                            }).done(function (data) {
                                try {
                                    data = JSON.parse(data);
                                    document.getElementById("saveBTN").disabled = true;
                                } catch (e) {
                                    return false;
                                }
                            });
                        }
                    });


                } catch (e) {
                    names = [];
                }
            });

        },
        showRawPostData:function () {
            var content = document.createElement("div");
            content.innerHTML = '<div style="padding: 13px;text-align:left" id="main"><pre>'+JSON.stringify(leadData.leadData.request, undefined, 4)+'</pre></div>';

            var settings = {
                animation:"fadeInUp",
                //template: "cusmod-blue",
                hideCloseBTN:false,
                content: content
            };
            initCusmod("#cusmod", settings);
        },
        getLeadLogs:function () {
            var strUrl = BASE_URL+'/console/actions/leads/lead/getLeadLogs.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method:"POST",
                data:{
                    leadId:leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try {
                    data = JSON.parse(data);
                }catch (e) {
                    // Failed loading this lead
                }

                // Set Logs
                $("#leadLogStream").html("");
                if (data && data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        leadController.lead.setLeadLog(data[i]);
                    }

                    // Adjust logs to the botton of the container (show newest logs)
                    var element = document.getElementById("leadLogStreamContainer");
                    element.scrollTop = element.scrollHeight ;

                }else{
                   // Do something
                }
            });
        },
        setLeadLog:function (singlelog) {


            var icon = "fa fa-circle";
            var logstring = singlelog.logText.toLowerCase();

            if(logstring.includes("lead received")){
                icon = "fa fa-dot-circle-o";
            }

            if(logstring.includes("new sms from")){
                icon = "fa fa-commenting-o";
            }

            if(logstring.includes("client updated lead information")){
                icon = "fa fa-pencil";
            }

            if(logstring.includes("signed a credit card authorization form") || logstring.includes("signed estimate contract") || logstring.includes("signed a claim")){
                icon = "fa fa-check-square-o bg-primary";
            }

            if(singlelog.userIdCreatedName != null){
                singlelog.logText = "<span style='color: #1c84c6;font-weight: 900;'>["+singlelog.userIdCreatedName+"]</span> "+singlelog.logText;
            }

            var stream = document.createElement("div");
            stream.className = "stream";

            var badge = document.createElement("div");
            badge.className = "stream-badge";
            badge.innerHTML = '<i class="'+icon+'"></i>';

            var panel = document.createElement("div");
            panel.className = "stream-panel";

            var info = document.createElement("div");
            info.className = "stream-info";
            info.innerHTML = singlelog.logText+" - ";

            var logtime = document.createElement("span");
            logtime.setAttribute("style","color:#337ab7");
            logtime.innerHTML = singlelog.dateAddedShort;
            logtime.title = singlelog.dateAdded;

            info.appendChild(logtime);
            panel.appendChild(info);

            stream.appendChild(badge);
            stream.appendChild(panel);

            document.getElementById("leadLogStream").appendChild(stream);


        },
        addCustomLog:function () {

            var wrapper = document.createElement("div");

            var closeBtn = document.createElement("button");
            closeBtn.setAttribute("style", "font-size: 25px;outline: none;position: fixed;top: 2px;right: 10px;")
            closeBtn.setAttribute("type", "button");
            closeBtn.setAttribute("onClick", "swal.close()");
            closeBtn.classList.add("close")

            var closeBtnSpan = document.createElement("span");
            closeBtnSpan.setAttribute("aria-hidden", "true");
            closeBtnSpan.innerHTML = "&times;";

            var closeBtnSpan2= document.createElement("span");
            closeBtnSpan2.classList.add("sr-only");
            closeBtnSpan2.innerHTML = "Close";

            closeBtn.appendChild(closeBtnSpan);
            closeBtn.appendChild(closeBtnSpan2);


            var form = document.createElement("div");
            form.className = "input-group m-b";
            form.setAttribute("style", "display:flex;padding-top: 10px;");

            var textDiv = document.createElement("div");
            textDiv.classList.add("form-group");

            var nameInput = document.createElement("input");
            nameInput.setAttribute("id", "customLogText");
            nameInput.setAttribute("name", "customLogText");
            nameInput.setAttribute("type", "text");
            nameInput.setAttribute("placeholder", "Enter log text..");
            nameInput.classList.add("form-control");
            nameInput.setAttribute("style", "margin-bottom: 10px;");
            nameInput.classList.add("form-control-sm");

            textDiv.appendChild(nameInput);

            // var select = document.createElement("select");
            // select.setAttribute("id", "");
            // select.setAttribute("name", "");
            // select.setAttribute("type", "text");
            //
            // select.classList.add("form-control");
            // select.classList.add("form-control-sm");

            // form.appendChild(select);

            var buttonDiv = document.createElement("div");
            buttonDiv.className = "input-group-append";

            var sendBTN = document.createElement("span");
            sendBTN.classList.add("btn-primary");
            sendBTN.classList.add("input-group-addon");
            sendBTN.innerHTML = "Add";
            sendBTN.type = "button";
            sendBTN.setAttribute("style", "margin-left:9px; height:34px;cursor:pointer;border: 1px solid #e5e6e7;border-left: none;background-color: #1ab394; border-color: #1ab394;color: #FFFFFF; width:initial;");
            sendBTN.setAttribute("onClick", "leadController.lead.sendCustomLog()");

            buttonDiv.appendChild(sendBTN);

            form.appendChild(nameInput);
            form.appendChild(buttonDiv);

            wrapper.appendChild(closeBtn);
            wrapper.appendChild(form);

            swal({
                title: "",
                content: wrapper,
                buttons: false
            });
        },
        sendCustomLog:function () {

            var logText = $('#customLogText').val().trim();
            if(!logText){
                $('#customLogText').css("border-color","red");
                setTimeout(function(){
                    $('#customLogText').css("border-color","rgb(229, 230, 231)");
                    }, 1000);
            }else{

                var strUrl = BASE_URL+'/console/actions/leads/lead/setCustomLog.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        logText: logText,
                        leadId: leadId
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (resp) {
                    try {
                        var data = JSON.parse(resp);
                        leadController.lead.getLeadLogs();
                        swal.close();

                    }catch (e) {
                        toastr.error("Error, please try again later","Error");
                        Sentry.withScope(function(scope) {
                            Sentry.setExtra("data", resp);
                            Sentry.setExtra("comments", "leadController - sendCustomLog");
                            Sentry.captureException(new Error("Failed Saving custom log"));
                        });
                    }
                });
            }
        }
    },
    contact:{
        getLeadEmailsData:function () {
            var strUrl = BASE_URL+'/console/actions/mail/getEmailsToLead.php';

            jQuery.ajax({
                url: strUrl,
                method:"POST",
                data:{
                    "leadId": leadId
                },
                async: true
            }).done(function (data) {
                try {

                    var emails = JSON.parse(data);

                    var container = document.getElementById("tableBody");
                    container.innerHTML = "";
                    if (emails.length > 0) {
                        for (var i = 0; i < emails.length; i++) {




                            var tr = document.createElement("tr");

                            var td = document.createElement("td");
                            var tdText = document.createTextNode(emails[i].timeSent);

                            td.appendChild(tdText);
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            var tdText = document.createTextNode(emails[i].subject);

                            td.appendChild(tdText);
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            var tdText = document.createTextNode(emails[i].toEmail);

                            td.appendChild(tdText);
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            var tdText = document.createTextNode(emails[i].fullName);

                            td.appendChild(tdText);
                            tr.appendChild(td);

                            var td = document.createElement("td");

                            if (emails[i].didOpen == 1){
                                var tdText = document.createTextNode(emails[i].timeFormatted);
                                td.setAttribute("title",emails[i].openDate);
                            } else{
                                var tdText = document.createTextNode("Didn't Open");
                            }

                            td.appendChild(tdText);
                            tr.appendChild(td);

                            var td = document.createElement("td");
                            var a = document.createElement("a");
                            a.setAttribute("onClick","leadController.page.change(2,"+emails[i].id+","+emails[i].userSent+")");
                            var aText = document.createTextNode("Preview");

                            a.appendChild(aText);
                            td.appendChild(a);
                            tr.appendChild(td);

                            container.appendChild(tr);
                        }
                        if(emails.length > 1){
                            $("#emailsTotal").html("<span class='label label-primary pull-right'>"+emails.length+" Emails</span>");
                        }else{
                            $("#emailsTotal").html("<span class='label label-primary pull-right'>"+emails.length+" Email</span>");
                        }
                    }else{
                        var tr = document.createElement("tr");
                        var td = document.createElement("td");
                        td.colSpan = "8";
                        td.style.textAlign = "center";
                        td.style.backgroundColor = "#ffffff";
                        var tdText = document.createTextNode("No Emails Sent To This Lead");

                        td.appendChild(tdText);
                        tr.appendChild(td);
                        container.appendChild(tr);
                    }

                }catch (e) {

                }

            });
        },
        getLeadEmailDataById:function (id,userSent) {
            var strUrl = BASE_URL+'/console/actions/mail/getEmailToLeadById.php';

            jQuery.ajax({
                url: strUrl,
                method:"POST",
                data:{
                    leadId: leadId,
                    id:id,
                    userSent: userSent
                },
                async: false
            }).done(function (data) {
                try {
                    data = JSON.parse(data);

                    document.getElementById("previewSubject").value = data.subject;
                    var iframe = $('#previewContent');
                    iframe.attr("src",BASE_URL+"/console/categories/mail/previewSentEmail.php?id="+id+"&userSent="+data.userSent);
                    iframe.load();

                    $('#emailPreviewData').html("By User: "+data.fullName+"<br>At "+data.timeSent);
                }catch (e) {

                }

            });
        },
    },
    files:{
        all: [],
        get:function () {

            var strUrl = BASE_URL+'/console/actions/leads/getLeadFiles.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    id: leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try{
                    data = JSON.parse(data);
                    if (data){
                        var itemsContainer = $("#leadFiles");
                        itemsContainer.html("");
                        if(data.data.length > 0){
                            for(var i=0; i<data.data.length;i++){
                                leadController.files.set(data.data[i],itemsContainer);
                            }
                            if(data.data.length == 1){
                                $("#filesTotal").text(data.data.length+" File");
                            }else{
                                $("#filesTotal").text(data.data.length+" Files");
                            }
                        }else{
                            $("#filesTotal").text("");
                            itemsContainer.html("<div class=\"alert alert-warning\">No files for this lead</div>");
                        }
                        if (!data.uploadAble){
                            $("#uploadBtn").attr("disabled",true);
                            $("#uploadBtn").text("Max 9 files Reached");
                        }else{
                            $("#uploadBtn").attr("disabled",false);
                            $("#uploadBtn").text("Upload File");
                        }
                    }
                }catch (e){

                }

            });

        },
        set:function (data,itemsContainer) {

            data['file_type'] = data['url'].split('.').pop();
            // setting Files Data
            var divFileBox = document.createElement("div");
            divFileBox.style.cursor = "pointer";
            divFileBox.classList.add("file-box");
            if (data['deleteAble'] == true){
                divFileBox.classList.add("contextMenu");
            }
            divFileBox.setAttribute("dataID",data['id']);
            divFileBox.setAttribute("id", "file" + data['id']);
            // make file dragable with the file ID
            divFileBox.setAttribute("draggable", "false");

            var divFile = document.createElement("div");
            divFile.classList.add("file");

            var aLink = document.createElement("a");
            aLink.setAttribute("href", BASE_URL+"/console/actions/leads/getLink.php?imageURL="+data['url']);
            aLink.setAttribute("target","_blank");

            var is_img = false;
            if(data['file_type'] == "jpeg" || data['file_type'] == "JPEG" || data['file_type'] == "jpg" || data['file_type'] == "JPG" || data['file_type'] == "gif" || data['file_type'] == "GIF" || data['file_type'] == "png" || data['file_type'] == "PNG"){

                var divImage = document.createElement("div");
                divImage.classList.add("icon");
                divImage.style.backgroundImage = "url("+BASE_URL+"/console/actions/leads/getLink.php?imageURL="+data['url']+")";
                divImage.style.backgroundSize = "cover";
                divImage.style.backgroundPosition = "center";
                divImage.style.backgroundRepeat = "no-repeat";

                var iconI = document.createElement("i");
                // iconI.classList.add("fa");
                // iconI.classList.add("fa-image");

                is_img = true;
            }else{
                var divIcon = document.createElement("div");
                divIcon.classList.add("icon");

                var iconI = document.createElement("i");
                iconI.classList.add("fa");
                iconI.classList.add("fa-file");
            }

            if(data['file_error']){
                var myText = document.createTextNode(data['originalFileName']+" File Not Found!");
            }else{
                var myText = document.createTextNode(data['originalFileName']);
            }
            var br = document.createElement("br");

            var small = document.createElement("small");
            small.classList.add("smallDate");
            var smallText = document.createTextNode("Added: "+data["CreatedAt"]);
            small.appendChild(smallText);

            var deleteBtn = document.createElement("button");
            deleteBtn.classList.add("pull-right");
            deleteBtn.classList.add("btn");
            deleteBtn.classList.add("btn-danger");
            deleteBtn.classList.add("btn-xs");
            deleteBtn.style.fontSize = "8px";
            deleteBtn.classList.add("deleteBtn");
            deleteBtn.setAttribute("onclick","leadController.files.removeFile("+data['id']+",event)");
            var deleteText = document.createTextNode("X");

            deleteBtn.appendChild(deleteText);
            small.appendChild(deleteBtn);

            var divName = document.createElement("div");
            divName.classList.add("file-name");
            divName.style.whiteSpace = "nowrap";
            divName.style.overflow = "hidden";
            divName.style.textOverflow = "ellipsis";
            divName.title = data['orignialFileName'];
            divName.appendChild(myText);
            divName.appendChild(br);
            divName.appendChild(small);

            if(is_img == true){

                divImage.appendChild(iconI);

                divFile.appendChild(divImage);

            }else{

                divIcon.appendChild(iconI);
                divFile.appendChild(divIcon);
            }
            aLink.appendChild(divName);

            if(data['file_error']){
                divFile.appendChild(divName);
            }else {
                divFile.appendChild(aLink);
            }

            divFileBox.appendChild(divFile);


            itemsContainer.append(divFileBox);
        },
        removeFile:function (fileID,e) {
            if (e){
                e.preventDefault();
            }
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file",
                icon: "warning",
                dangerMode: true,
                buttons: true,
            }).then(function(isConfirm){
                if (isConfirm) {
                    var strUrl = BASE_URL+'/console/actions/leads/removeFile.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method: "POST",
                        data: {
                            fileId: fileID,
                            leadId: leadId
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function(data){
                        if (data == true|| data == "true"){
                            leadController.files.get();
                        }else{
                            toastr.error("Unable to remove this file");
                        }
                    });
                }
            });
        },
        prepareUpload:function (event) {
            leadController.files.all = event.target.files;
            leadController.files.uploadFiles();
        },
        uploadFiles:function (event) {
            $("#uploadBtn").attr("disabled",true);
            $("#uploadBtn").text("Uploading...");

            var data = new FormData();
            $.each(leadController.files.all, function(key, value)
            {
                data.append(key, value);
            });

            data.append('leadId', leadId);

            var strUrl = BASE_URL+'/console/actions/leads/uploadFile.php';

            $.ajax({
                url: strUrl,
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(data, textStatus, jqXHR){

                },
                async: true
            }).done(function(data){
                try {
                    if (data.success == true){
                        leadController.files.get();
                    }else if (data == false || data == "false") {
                        toastr.error("Uploading File failed, 9 Files maximum per lead","Error");
                    }else{
                        toastr.error(data.error[0],"Error");
                    }
                }catch (e) {
                    toastr.error("Uploading File failed, please try again later");
                }
                leadController.files.get();
                $("#uploadBtn").attr("disabled",false);
                $("#uploadBtn").text("Upload File");
            });
        }
    },
    tags:{
        showTagsModal:function () {
            showCustomModal("categories/iframes/leads/tags.php?leadId="+leadId);
        },
        reload:function () {
            var strUrl = BASE_URL+'/console/actions/leads/lead/getLeadTags.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    leadId: leadId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function(data){
                try {
                    data = JSON.parse(data);

                    var container = document.getElementById("leadTags");
                    container.innerHTML = "";

                    if (data){

                        var tagsLabel = document.createElement("div");
                        tagsLabel.innerHTML = "Lead Tags";
                        tagsLabel.className = "pull-left";

                        var tagsContent = document.createElement("div");
                        tagsContent.className = "pull-right";
                        tagsContent.setAttribute("style","padding-top:7px;");

                        for (var i = 0; i<data.length;i++){
                            leadController.tags.set(data[i],tagsContent);
                        }

                        container.appendChild(tagsLabel);
                        container.appendChild(tagsContent);
                    }
                }catch (e) {
                    document.getElementById("leadTags").innerHTML = "";
                }
            });
        },
        set:function (data,container) {
            var span = document.createElement("span");
            span.setAttribute("onclick","leadController.tags.showTagsModal()");
            span.classList.add("label");
            span.classList.add("tagColor");
            span.style.color = data.color;

            var spanText = document.createTextNode(data.name);
            span.appendChild(spanText);
            container.appendChild(span);
        }

    },
    moving:{
        inventory:{
            setClientInventory:function(){
                if (movingLeadData.inventory) {
                    var container = document.getElementById('inventoryGroup');
                    container.innerHTML = "<option value=\"0\">Select Items Group</option>";
                    var containerList = document.getElementById('inventoryItemsList');
                    containerList.innerHTML = "";
                    for (var i = 0; i < movingLeadData.inventory.length; i++) {

                        var current = movingLeadData.inventory[i];

                        var option = document.createElement('option');
                        option.value = current.id;

                        var optionText = document.createTextNode(current.title);
                        option.appendChild(optionText);
                        container.appendChild(option);

                        var current = movingLeadData.inventory[i];

                        var ul = document.createElement('ul');
                        ul.id = 'item-' + current.id;
                        ul.classList.add('items');
                        ul.classList.add('list-group');
                        ul.classList.add('clear-list');
                        ul.classList.add('m-t');
                        ul.style.display = "none";

                        for (var x = 0; x < current.items.length; x++) {
                            var currentItem = current.items[x];

                            var search = [];

                            search['name'] = currentItem.title;
                            search['group'] = current.id;
                            search['item'] = currentItem;

                            allInventoryItems.push(search);

                            var li = document.createElement('li');
                            li.classList.add('list-group-item');

                            var span = document.createElement('span');
                            span.id = 'cf-' + currentItem.id;
                            span.classList.add('pull-right');

                            var spanText = document.createTextNode(currentItem.cf + " CuF");
                            span.appendChild(spanText);
                            li.appendChild(span);

                            var currentItemStringify = JSON.stringify(currentItem);

                            var span = document.createElement('span');
                            span.setAttribute('onclick', 'setTableItem(' + currentItemStringify + ')');
                            span.setAttribute('style', 'font-size: 19px; padding: 0px 6px; cursor: pointer;');
                            span.classList.add('label');
                            span.classList.add('label-primary');

                            var spanText = document.createTextNode('+');
                            span.appendChild(spanText);
                            li.appendChild(span);

                            var span = document.createElement('span');
                            span.setAttribute('style', 'margin-left: 5px;');
                            span.classList.add('itemQuery');

                            var spanText = document.createTextNode(currentItem.title);
                            span.appendChild(spanText);
                            li.appendChild(span);

                            ul.appendChild(li);

                            containerList.appendChild(ul);
                        }

                    }
                }
                leadController.moving.inventory.calcCF();
            },
            setTocuhSpin:function (id,cf,title) {
                var touchspin = ".touchspin"+id;

                $(touchspin).TouchSpin({
                    buttondown_class: 'btn btn-white btn-change',
                    buttonup_class: 'btn btn-white btn-change',
                    min: 0,
                    max: 100000,
                }).on("touchspin.on.startupspin", function() {
                    leadController.moving.inventory.calcCF();
                }).on("touchspin.on.startdownspin", function() {
                    leadController.moving.inventory.calcCF();
                    if(document.getElementById("amount-"+id).value == 0 || document.getElementById("amount-"+id).value == "0"){
                        document.getElementById(title+"-"+id).remove();
                        var index = search(id,items);
                        items.splice(index, 1);
                    }
                });

            },
            setTocuhSpinCF:function (id,cf,title) {
                var touchspin = ".touchspinCF-"+id;

                $(touchspin).TouchSpin({
                    buttondown_class: 'btn btn-white btn-change',
                    buttonup_class: 'btn btn-white btn-change',
                    min: 0,
                    max: 100000,
                }).on("touchspin.on.startupspin", function() {
                    var index = search(id,items);
                    items[index].cf++;
                    leadController.moving.inventory.calcCF();
                }).on("touchspin.on.startdownspin", function() {
                    var index = search(id,items);
                    items[index].cf--;
                    leadController.moving.inventory.calcCF();
                });

            },
            calcCF:function () {
                var totalCF = 0;
                items.splice(0,items.length);
                $("#itemsTable tr").each(function(){
                    var dirtyId = $(this)[0].id;
                    var id = dirtyId.split("-");
                    id = id[id.length-1];

                    var cf = $("#itemcf-"+id).val();
                    if (!cf){
                        cf = 0;
                    }
                    var amount = $("#amount-"+id).val();
                    var name = $("#text-"+id).text();
                    var showCF = document.getElementById("showCF-"+id).checked;
                    items.push({"id": id, "name": name, "cf": cf, 'amount': amount, 'showCF': !showCF});
                    if(!showCF){
                        totalCF = totalCF+(cf*amount);
                    }
                });
                $("#tableTotalCf").html("<b>Total Cubic Feet: </b>"+totalCF);
                return totalCF;
            },
            updateInventoryEstimate:function () {
                var itemsToSend = [];
                totalCF = leadController.moving.inventory.calcCF();
                for (var i=0;i<items.length;i++){
                    if ("name" in items[i]){
                        try {
                            var amount = parseInt(document.getElementById("amount-"+(items[i].id)).value);
                            itemsToSend.push({"id":items[i].id,"amount":amount,"name":items[i].name,"cf":items[i].cf,"showCF":items[i].showCF});
                        }catch (e) {
                            var amount = 1;
                            itemsToSend.push({"id":items[i].id,"amount":amount,"name":items[i].name,"cf":items[i].cf,"showCF":items[i].showCF});
                        }
                    }else{
                        try {
                            var amount = parseInt(document.getElementById("amount-"+items[i].id).value);
                            itemsToSend.push({"id":items[i].id,"amount":amount,"showCF":items[i].showCF});
                        }catch (e) {
                            var amount = 1;
                            itemsToSend.push({"id":items[i].id,"amount":amount,"showCF":items[i].showCF});
                        }
                    }
                }
                var strUrl = BASE_URL+'/console/actions/moving/leads/updateEstimate.php', strReturn = "";

                jQuery.ajax({
                    url: strUrl,
                    method:"post",
                    data:{
                        leadId:leadId,
                        items: itemsToSend,
                        totalCF:totalCF
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        if (data.updateInventory) {
                            toastr.success("Inventory Saved","Saved",{preventDuplicates:true});
                            // set estimate total CF
                            if($("#estimateCForLBS").val() != 2){
                                if($("#estimateCForLBS").val() == 0) {
                                    $("#totalCF").val(data.lbs);
                                }else{
                                    $("#totalCF").val(data.cf);
                                }
                            }
                            var totalTempCF = parseInt(data.cf);
                            if (totalTempCF){
                                $("#inventoryCFLabel").html("<span style='margin-right: 3px;' class='label label-success pull-right'>"+totalTempCF+" CF</span>");
                            }else{
                                $("#inventoryCFLabel").html("");
                            }
                            if (itemsToSend.length > 0){
                                $("#inventoryTotal").html('<span class="label label-primary pull-right">'+itemsToSend.length+' Items</span>');
                            }else {
                                $("#inventoryTotal").html('');
                            }

                            if (data.updateLBS){
                                leadController.moving.inventory.askToUpdateEstimateInventory(true);
                            }else{
                                toastr.error("Error Saving Inventory");
                            }
                        }else{
                            toastr.error("Error Saving Inventory");
                        }
                    }catch (e) {
                        toastr.error("Error Saving Inventory");
                    }
                    leadController.moving.inventory.calcCF();
                    leadController.moving.estimate.initMovingEstimate();
                });
            },
            clearInventoryEstimate:function () {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this inventory",
                    icon: "warning",
                    dangerMode: true,
                    buttons: true,
                }).then(

                    function (isConfirm){
                        if (isConfirm) {
                            var strUrl = BASE_URL+'/console/actions/moving/leads/updateEstimate.php', strReturn = "";

                            jQuery.ajax({
                                url: strUrl,
                                method:"post",
                                data:{
                                    leadId:leadId,
                                    items: [],
                                    totalCF:0
                                },
                                success: function (html) {
                                    strReturn = html;
                                },
                                async: true
                            }).done(function (data) {
                                try {
                                    data = JSON.parse(data);
                                    if (data.updateInventory) {
                                        toastr.success("Inventory Cleared","Cleared",{preventDuplicates:true});
                                        if (data.updateLBS){
                                            leadController.moving.inventory.askToUpdateEstimateInventory(true);
                                        }else{
                                            toastr.error("Error Clearing Inventory");
                                        }
                                    }else{
                                        toastr.error("Error Clearing Inventory");
                                    }
                                }catch (e) {
                                    toastr.error("Error Clearing Inventory");
                                }
                                leadController.lead.getLeadMovingDetails();
                                leadController.moving.inventory.calcCF();
                                leadController.moving.estimate.initMovingEstimate();
                            });
                        }
                    });
            },
            updateEstimateWeightByInventory:function () {
                leadController.moving.inventory.askToUpdateEstimateInventory(false);

                var strUrl = BASE_URL+'/console/actions/moving/leads/updateEstimateWeightByInventory.php', strReturn = "";

                jQuery.ajax({
                    url: strUrl,
                    method:"post",
                    data:{
                        leadId:leadId
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    // get the moving details again
                    leadController.lead.getLeadMovingDetails();
                    try {
                        data = JSON.parse(data);
                        if (data == true) {
                            toastr.success("Lead Weight Updated","",{preventDuplicates:true});

                        }else{
                            toastr.error("Error Updating Move Size");
                        }
                    }catch (e) {
                        toastr.error("Error Updating Move Size");
                    }

                });
            },
            askToUpdateEstimateInventory:function (level) {
                if(level == true){
                    document.getElementById("updateInventoryBTNDiv").style.display = "none";
                    document.getElementById("updateInventoryEstimateBTNDiv").style.display = "";
                }else{
                    document.getElementById("updateInventoryEstimateBTNDiv").style.display = "none";
                    document.getElementById("updateInventoryBTNDiv").style.display = "";
                }
            },
            changeView:function (type) {
                switch (type) {
                    case 1:
                        document.getElementById("selectorGroup").style.display = "";
                        document.getElementById("addNewItems").style.display = "none";
                        document.getElementById("addItem").setAttribute("onclick","leadController.moving.inventory.changeView(2)");
                        document.getElementById("addItem").innerText = "Cannot find your item? Add manually";
                        break;
                    case 2:
                        document.getElementById("selectorGroup").style.display = "none";
                        document.getElementById("addNewItems").style.display = "";
                        document.getElementById("addItem").setAttribute("onclick","leadController.moving.inventory.changeView(1)");
                        document.getElementById("addItem").innerText = "Go Back";
                        break;
                }
            },
            addNewItem:function () {
                var title  = document.getElementById("newItemTitle").value;
                var cf  = document.getElementById("newItemCF").value;

                if (title == "") {
                    toastr.error("Item Name Can't Be Empty");
                    return false;
                }
                if (cf == ""|| cf < 1) {
                    toastr.error("Item Cubic Feet Can't Be Empty");
                    return false;
                }

                try {
                    document.getElementById("amount-"+title).value;
                    document.getElementById("errorSmall").innerText = "* Cant add another item in the same name";
                }catch (e) {
                    leadController.moving.inventory.setInventory({"id":Math.floor(Math.random() * Math.floor(9999999)),"name":title,"cf":parseInt(cf),"amount":1});
                    document.getElementById("newItemTitle").value = "";
                    document.getElementById("newItemCF").value = "";
                    document.getElementById("errorSmall").innerText = "";
                    totalCF = totalCF+parseInt(cf);
                    $("#tableTotalCf").html("<b>Total Cubic Feet: </b>"+totalCF);
                    leadController.moving.inventory.changeView(1);
                }
            },
            setInventory:function (data) {
                var container = document.getElementById("itemsTable");
                var tr = document.createElement("tr");
                tr.id = data.name+"-"+data.id;

                var td = document.createElement("td");
                td.id = "text-"+data.id;
                var tdText = document.createTextNode(data.name);
                td.appendChild(tdText);
                tr.appendChild(td);

                var td = document.createElement("td");
                var tdInput = document.createElement("input");
                tdInput.setAttribute("onchange","leadController.moving.inventory.calcCF()");
                tdInput.id = "itemcf-"+data.id;
                tdInput.classList.add("form-control");
                tdInput.classList.add("CFInput");
                tdInput.classList.add("touchspinCF-"+data.id);
                tdInput.setAttribute("type","number");
                tdInput.setAttribute("value",data.cf);
                if (typeof data.showCF != "undefined" && data.showCF == "false"){
                    tdInput.setAttribute("disabled",true);
                }

                td.appendChild(tdInput);
                tr.appendChild(td);

                var td = document.createElement("td");
                var tdInput = document.createElement("input");
                tdInput.setAttribute("onchange","leadController.moving.inventory.calcCF()");
                tdInput.id = "amount-"+data.id;
                tdInput.classList.add("form-control");
                tdInput.classList.add("CFInput");
                tdInput.classList.add("touchspin"+data.id);
                tdInput.setAttribute("type","number");
                tdInput.setAttribute("value",data.amount);

                td.appendChild(tdInput);
                tr.appendChild(td);

                var td = document.createElement("td");
                var div = document.createElement("div");
                div.classList.add("checkbox");
                div.classList.add("checkbox-danger");
                var tdInput = document.createElement("input");
                tdInput.id = "showCF-"+data.id;
                tdInput.setAttribute("type","checkbox");
                tdInput.setAttribute("name","showCF-"+data.id);
                if (typeof data.showCF != "undefined" && data.showCF == "false"){
                    tdInput.setAttribute("checked",true);
                }
                var tdLabel = document.createElement("label");
                tdLabel.setAttribute("for","showCF-"+data.id);
                tdLabel.innerHTML = "Hide CF";

                div.appendChild(tdInput);
                div.appendChild(tdLabel);
                td.appendChild(div);
                tr.appendChild(td);

                container.appendChild(tr);
                leadController.moving.inventory.setTocuhSpin(data.id,data.cf,data.name);
                leadController.moving.inventory.setTocuhSpinCF(data.id,data.cf,data.name);
                if (typeof data.showCF != "undefined" && data.showCF == "false"){
                    items.push({"id": data.id, "name": data.name, "cf": data.cf, 'amount': data.amount, 'showCF': false});
                }else {
                    items.push({"id": data.id, "name": data.name, "cf": data.cf, 'amount': data.amount, 'showCF': true});
                }

                $("#showCF-"+data.id).on("change",function () {
                    var index = search(data.id,items);
                    if (document.getElementById("showCF-" + data.id).checked == true) {
                        $("#itemcf-" + data.id).val("");
                        $("#itemcf-" + data.id).attr("disabled", true);
                        if (index) {
                            items[index].showCF = false;
                            items[index].cf = null;
                        }
                        leadController.moving.inventory.calcCF();
                    } else {
                        $("#itemcf-" + data.id).val(1);
                        $("#itemcf-" + data.id).attr("disabled", false);
                        if (index) {
                            items[index].showCF = true;
                        }
                        leadController.moving.inventory.calcCF();
                    }
                });
            },
            addToInventory:function () {
                swal({
                    title: "Are you sure?",
                    text: "Estimate Inventory will be updated",
                    buttons: true,
                }).then(function(isConfirm){
                    if (isConfirm) {
                        var strUrl = BASE_URL + '/console/actions/leads/addConfirmationInventory.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "post",
                            data: {
                                id: leadId
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {
                            try {
                                data = JSON.parse(data);
                                if (data != true){
                                    toastr.error("Error","Error Saving Client Inventory");
                                }else{
                                    toastr.success("Inventory Updated","Updated");
                                    leadController.lead.getLeadMovingDetails();
                                }
                            }catch (e) {
                                toastr.error("Error","Error Saving Client Inventory");
                            }
                        });
                    }
                });
            },
            searchItem:function (obj) {
                var search = obj.value.toLowerCase();
                var group = $("#inventoryGroup").val();
                if (search == ""){
                    $("#inventoryItemsList").show();
                    var containerList = document.getElementById('inventoryItemsSearch');
                    containerList.innerHTML = "";
                } else{
                    $("#inventoryItemsList").hide();
                    var itemsToShow = [];
                    for (var i = 0; i<allInventoryItems.length;i++){
                        var current = allInventoryItems[i];
                        if (current.name.toLowerCase().includes(search)){
                            if (group == 0){
                                itemsToShow.push(current.item);
                            }else{
                                if (current.group == group){
                                    itemsToShow.push(current.item);
                                }
                            }
                        }
                    }
                    var containerList = document.getElementById('inventoryItemsSearch');
                    containerList.innerHTML = "";

                    if (itemsToShow.length > 0) {
                        for (var x = 0; x < itemsToShow.length; x++) {

                            var currentItem = itemsToShow[x];

                            var li = document.createElement('li');
                            li.classList.add('list-group-item');

                            var span = document.createElement('span');
                            span.id = 'cf-' + currentItem.id;
                            span.classList.add('pull-right');

                            var spanText = document.createTextNode(currentItem.cf + " CuF");
                            span.appendChild(spanText);
                            li.appendChild(span);

                            var currentItemStringify = JSON.stringify(currentItem);

                            var span = document.createElement('span');
                            span.setAttribute('onclick', 'setTableItem(' + currentItemStringify + ')');
                            span.setAttribute('style', 'font-size: 19px; padding: 0px 6px; cursor: pointer;');
                            span.classList.add('label');
                            span.classList.add('label-primary');

                            var spanText = document.createTextNode('+');
                            span.appendChild(spanText);
                            li.appendChild(span);

                            var span = document.createElement('span');
                            span.setAttribute('style', 'margin-left: 5px;');
                            span.classList.add('itemQuery');

                            var spanText = document.createTextNode(currentItem.title);
                            span.appendChild(spanText);
                            li.appendChild(span);

                            containerList.appendChild(li);
                        }
                    }else{
                        $("#inventoryItemsSearch").html("<li class='list-group-item'><small>No items found for: "+obj.value+"</small></li>");
                    }
                }
            }
        },
        estimate:{
            initEstimates:function(){

                document.getElementById("estimatesTabs").innerHTML = '';

                if(currentEstimate == null){
                    if(movingLeadData.estimates.length > 0) {

                        for(var i = 0;i<movingLeadData.estimates.length;i++) {
                            if(movingLeadData.estimates[i].isActive == "1"){
                                currentEstimate = i;
                            }
                        }
                        if(currentEstimate == null){currentEstimate = movingLeadData.estimates.length-1;}

                    }else{
                        currentEstimate = 0;
                    }
                }
                if(movingLeadData.estimates.length > 0) {
                    for(var i = 0;i<movingLeadData.estimates.length;i++){
                        leadController.moving.estimate.createNewEstimateTab(i);
                    }
                }else{
                    leadController.moving.estimate.createNewEstimateTab(null);
                }

            },
            createNewEstimateTab:function(tabId){
                var isNewTab = false;
                if(tabId == null || tabId == undefined){
                    isNewTab = true;
                    $("#estimatesTabs li").removeClass("active");
                    movingLeadData.estimates.push(false);

                    // Set the current estimate to the newest
                    tabId = movingLeadData.estimates.length-1;
                    currentEstimate = tabId;
                }

                var li = document.createElement("li");
                if(tabId == currentEstimate){
                    li.className = "active";
                }
                li.setAttribute("onclick",'currentEstimate = '+tabId+';leadController.moving.estimate.prepareEstimateCalculation()');

                var a = document.createElement("a");
                a.setAttribute("data-toggle","tab");
                a.className = "estimatesMenuToggles";
                a.setAttribute("style","padding-left: 16px;");


                if(isNewTab == false){

                    var icon = document.createElement("i");

                    icon.className = "dropdown-toggle fa fa-chevron-down";
                    icon.setAttribute("style","cursor:pointer");
                    icon.setAttribute("data-toggle","dropdown");
                    icon.setAttribute("aria-expanded","false");
                    icon.setAttribute("onclick","event.stopPropagation();$('.estimatesMenuToggles').removeClass('open');this.parentNode.classList.toggle('open')");

                    if(movingLeadData.estimates[tabId].isActive == "1") {
                        icon.style.color = "#1ab394";
                    }else{
                        icon.style.color = "#ca4440";
                    }

                    a.appendChild(icon);

                    var ul = document.createElement("ul");
                    ul.className = "dropdown-menu dropdown-estimate-tab";


                    if(movingLeadData.estimates[tabId].isActive == "0") {
                        var subli3 = document.createElement("li");
                        subli3.innerHTML = "<a href=\"#\" class=\"dropdown-item\" onclick=\"leadController.moving.estimate.activateEstimate(" + movingLeadData.estimates[tabId].id + ");\">Activate estimate</a>";
                        ul.appendChild(subli3);
                    }else{
                        var subli3 = document.createElement("li");
                        subli3.innerHTML = "<a href=\"#\" class=\"dropdown-item\" onclick=\"leadController.moving.estimate.disableEstimate(" + movingLeadData.estimates[tabId].id + ");\">Disable estimate</a>";
                        ul.appendChild(subli3);
                    }

                    if(movingLeadData.estimates[tabId].customerSignature){
                        var subli2 = document.createElement("li");
                        subli2.innerHTML = "<a href=\"#\" class=\"dropdown-item\" onclick=\"leadController.moving.estimate.clearUserContract(" + movingLeadData.estimates[tabId].id + ");\">Clear Contract</a>";
                        ul.appendChild(subli2);
                    }

                    var liseperator = document.createElement("li");
                    liseperator.setAttribute("role","separator");
                    liseperator.setAttribute("class","divider");
                    liseperator.setAttribute("style","margin: 0;background-color: #e5e5e5;");

                    ul.appendChild(liseperator);

                    var subli1 = document.createElement("li");
                    subli1.innerHTML = "<a href=\"#\" class=\"dropdown-item\" onclick=\"leadController.moving.estimate.deleteEstimate(" + movingLeadData.estimates[tabId].id + ");\">Delete estimate</a>";
                    ul.appendChild(subli1);

                    a.appendChild(ul);

                }

                var span = document.createElement("span");
                if(isNewTab == true){
                    span.innerHTML = "New Estimate";
                }else{
                    span.innerHTML = "Estimate #" + (tabId + 1);

                    if(movingLeadData.estimates[tabId].isActive == "1") {
                        span.style.color = "#1ab394";
                    }else{
                        span.style.color = "#ca4440";
                    }

                }
                a.appendChild(span);

                li.appendChild(a);
                document.getElementById("estimatesTabs").appendChild(li);


                // If last estimate is new - disable the add new estimate button
                if(movingLeadData.estimates[movingLeadData.estimates.length-1] == false){
                    document.getElementById("createEstimateUL").classList.add("disabled");
                }else{
                    document.getElementById("createEstimateUL").classList.remove("disabled");
                }

                leadController.moving.estimate.prepareEstimateCalculation();

            },
            initMovingEstimate:function(){
                // this data gets all the estimate inputs data and calculates the subtotals and everything in the right side

                // First - calculate the FVP
                leadController.moving.estimate.fvp.calculateFVP();

                // =================== START GETTING THE DATA ===================
                var data = {
                    "initPrice":parseFloat(document.getElementById("totalCF").value),
                    "estimateCForLBS":parseInt(document.getElementById("estimateCForLBS").value),
                    "pricePerCf":parseFloat(document.getElementById("perCF").value),
                    "fuel":parseFloat(document.getElementById("fuelSurcharge").value),
                    "fuelType":parseFloat(document.getElementById("estimateFuelType").value),
                    "discount":parseFloat(document.getElementById("generalDiscount").value),
                    "discountType":parseInt(document.getElementById("generalDiscountType").value),
                    "coupon":parseFloat(document.getElementById("couponDiscount").value),
                    "couponType":parseInt(document.getElementById("couponDiscountType").value),
                    "senior":parseFloat(document.getElementById("seniorDiscount").value),
                    "seniorType":parseInt(document.getElementById("seniorDiscountType").value),
                    "veteranDiscount":parseFloat(document.getElementById("veteranDiscount").value),
                    "veteranDiscountType":parseInt(document.getElementById("veteranDiscountType").value),
                    "packingPackers":parseFloat(document.getElementById("packers").value),
                    "packingHours":parseFloat(document.getElementById("packersHrs").value),
                    "packingPerHour":parseFloat(document.getElementById("packersPerHrs").value),
                    "unpackingPackers":parseFloat(document.getElementById("unpackers").value),
                    "unpackingHours":parseFloat(document.getElementById("unpackersHrs").value),
                    "unpackingPerHour":parseFloat(document.getElementById("unpackersPerHrs").value),
                    "storageFee":parseFloat(document.getElementById("monthlyStorageFee").value),
                    "extraCharges":extraCharges,
                    "valueProtectionCharge":parseFloat(document.getElementById("valueProtectionCharge").value),
                    "agentFee":parseFloat(document.getElementById("agentFee").value),
                    "agentFeeType":parseInt(document.getElementById("agentFeeType").value),
                    "estimateComments":document.getElementById("estimateComments").value
                };


                if (document.getElementById("discountsToggle").checked) {data.discountToggle = true;}else{data.discountToggle = false;}
                if (document.getElementById("packersToggle").checked)   {data.packersToggle = true;}else{data.packersToggle = false;}
                if (document.getElementById("fvpToggle").checked)   {data.fvpToggle = true;}else{data.fvpToggle = false;}
                // =================== END GETTING THE DATA ===================

                // =================== START CALCULATING THE DATA ===================
                var dataArray = [];

                // =================== START InitPrice ===================
                var currentPrice = 0;
                switch(data.estimateCForLBS){
                    case 0:
                        var currentPrice = parseFloat(data.initPrice*data.pricePerCf);
                        break;
                    case 1:
                        var currentPrice = parseFloat(data.initPrice*data.pricePerCf);
                        break;
                    case 2:
                        var currentPrice = parseFloat(data.initPrice*data.pricePerCf);
                        break;
                    case 3:
                        var currentPrice = parseFloat(data.initPrice);
                        break;
                    default:
                        var currentPrice = parseFloat(data.initPrice*data.pricePerCf);
                        break;
                }
                if (isNaN(currentPrice)){
                    currentPrice = 0;
                }

                dataArray['startInitial'] = currentPrice;
                // =================== END InitPrice ===================

                // =================== START fuel ===================
                switch(data.fuelType) {
                    case 0:
                        data.fuel = (currentPrice/100)*data.fuel;
                        break;
                    case 1:
                        data.fuel = data.fuel;
                        break;
                }
                if (isNaN(data.fuel)){
                    data.fuel = 0;
                }
                currentPrice = parseFloat(currentPrice+data.fuel);
                dataArray['fuel'] = data.fuel;

                // =================== END fuel ===================

                dataArray['subTotal1'] = currentPrice;

                // =================== START Packers ===================
                if (data.packersToggle == true) {
                    var CALApacking = parseFloat(data.packingHours * data.packingPerHour);
                    if (isNaN(CALApacking)) {
                        CALApacking = 0;
                    }
                    dataArray['packing'] = CALApacking;

                    var CALAunpacking = parseFloat(data.unpackingHours * data.unpackingPerHour);
                    if (isNaN(CALAunpacking)) {
                        CALAunpacking = 0;
                    }
                    dataArray['unpacking'] = CALAunpacking;

                    var packingAndUnpackingCosts = CALApacking + CALAunpacking;
                    if (isNaN(packingAndUnpackingCosts)) {
                        packingAndUnpackingCosts = 0;
                    }
                    currentPrice = parseFloat(currentPrice + packingAndUnpackingCosts);
                }else{
                    dataArray['packing'] = 0;
                    dataArray['unpacking'] = 0;
                }
                // =================== END Packers ===================

                dataArray['subTotal2'] = currentPrice;

                // =================== START Agent Fee ===================
                if (isNaN(data.agentFee)){
                    data.agentFee = 0;
                }
                if (data.agentFee !== 0){

                    if(data.agentFeeType == 0){
                        var CALCagentFee = (currentPrice * data.agentFee) / 100;
                    }else{
                        var CALCagentFee = data.agentFee;
                    }

                    currentPrice = parseFloat(currentPrice+CALCagentFee);
                    dataArray['agentFee'] = CALCagentFee;
                }else{
                    dataArray['agentFee'] = 0;
                }
                // =================== END Agent Fee ===================

                // =================== START Extra charges ===================
                var extraPrice = document.getElementsByName("Extra-Price[]");
                var extraText = document.getElementsByName("Extra-Text[]");
                var extraTotal = document.getElementsByName("Extra-Total[]");

                for (var i=0;i<extraPrice.length;i++) {
                    var extraCharge = parseFloat(extraPrice[i].value);
                    if (isNaN(extraCharge)){
                        extraCharge = 0;
                    }
                    extraTotal[i].value = extraCharge;
                    currentPrice = parseFloat(currentPrice) + parseFloat(extraCharge);
                }
                // =================== END Extra charges ===================

                // =================== START Materials ===================
                var materialsTotal = document.getElementsByName("materials-total[]");

                for (var i=0;i<materialsTotal.length;i++) {

                    var materialsTotalPrice = parseFloat(materialsTotal[i].value);
                    if (isNaN(materialsTotalPrice)){
                        materialsTotalPrice = parseFloat(0);
                    }
                    currentPrice = parseFloat(currentPrice) + parseFloat(materialsTotalPrice);
                }
                // =================== END Materials ===================

                dataArray['subTotal3'] = currentPrice;


                // =================== START FVP ===================
                dataArray['valueProtectionCharge'] = data.valueProtectionCharge;
                currentPrice = parseFloat(currentPrice + dataArray['valueProtectionCharge']);
                // =================== END FVP ===================

                dataArray['subTotal4'] = currentPrice;


                // =================== START discounts & coupons ===================
                if (data.discountToggle == true){

                    if(data.discountType == 0){
                        var CALCgeneralDiscount = ((currentPrice)/100)*data.discount;
                    }else{
                        var CALCgeneralDiscount = data.discount;
                    }

                    if(data.couponType == 0){
                        var CALCcouponDiscount = ((currentPrice-CALCgeneralDiscount)/100)*data.coupon;
                    }else{
                        var CALCcouponDiscount = data.coupon;
                    }

                    if(data.seniorType == 0){
                        var CALCSeniorDiscount = ((currentPrice-CALCgeneralDiscount-CALCcouponDiscount)/100)*data.senior;
                    }else{
                        var CALCSeniorDiscount = data.senior;
                    }

                    if(data.veteranDiscountType == 0){
                        var CALCVeteranDiscount = ((currentPrice-CALCgeneralDiscount-CALCcouponDiscount-CALCSeniorDiscount)/100)*data.veteranDiscount;
                    }else{
                        var CALCVeteranDiscount = data.veteranDiscount;
                    }

                    if (isNaN(CALCgeneralDiscount)){
                        CALCgeneralDiscount = 0;
                    }
                    if (isNaN(CALCcouponDiscount)){
                        CALCcouponDiscount = 0;
                    }
                    if (isNaN(CALCSeniorDiscount)){
                        CALCSeniorDiscount = 0;
                    }
                    if (isNaN(CALCVeteranDiscount)){
                        CALCVeteranDiscount = 0;
                    }
                    dataArray['generalDiscount'] = CALCgeneralDiscount;
                    dataArray['coupon'] = CALCcouponDiscount;
                    dataArray['senior'] = CALCSeniorDiscount;
                    dataArray['veteranDiscount'] = CALCVeteranDiscount;


                    var totalDiscount = parseFloat(CALCgeneralDiscount);
                    totalDiscount = totalDiscount + parseFloat(CALCcouponDiscount);
                    totalDiscount = totalDiscount + parseFloat(CALCSeniorDiscount);
                    totalDiscount = totalDiscount + parseFloat(CALCVeteranDiscount);

                    currentPrice = parseFloat(currentPrice-totalDiscount);
                }else{
                    dataArray['generalDiscount'] = 0;
                    dataArray['coupon'] = 0;
                    dataArray['senior'] = 0;
                    dataArray['veteranDiscount'] = 0;
                }
                // =================== END discounts & coupons ===================

                dataArray['subTotal5'] = currentPrice;

                dataArray['totalEstimate'] = currentPrice;

                // =================== END CALCULATING THE DATA ===================

                // =================== START SETTING THE DATA ===================
                $("#startInitial").val(dataArray["startInitial"].toFixed(2));
                $("#totalFuelSurcharge").val(dataArray["fuel"].toFixed(2));
                $("#totalGeneralDiscount").val(dataArray["generalDiscount"].toFixed(2));
                $("#totalCouponDiscount").val(dataArray["coupon"].toFixed(2));
                $("#totalSeniorDiscount").val(dataArray["senior"].toFixed(2));
                $("#totalVeteranDiscount").val(dataArray["veteranDiscount"].toFixed(2));
                $("#totalPackingCosts").val(dataArray["packing"].toFixed(2));
                $("#totalUnPackingCosts").val(dataArray["unpacking"].toFixed(2));
                $("#totalAgentFee").val(dataArray["agentFee"].toFixed(2));
                $("#valueProtectionCharge").val(dataArray["valueProtectionCharge"].toFixed(2));
                $("#subTotal1").text("$"+dataArray["subTotal1"].toFixed(2));
                $("#subTotal2").text("$"+dataArray["subTotal2"].toFixed(2));
                $("#subTotal3").text("$"+dataArray["subTotal3"].toFixed(2));
                $("#subTotal4").text("$"+dataArray["subTotal4"].toFixed(2));
                $("#subTotal5").text("$"+dataArray["subTotal5"].toFixed(2));

                $("#TotalEstimate").text("$"+dataArray["totalEstimate"].toFixed(2));
                // =================== END SETTING THE DATA ===================

            },
            prepareEstimateCalculation:function () {
                // This function sets the moving estimate data to the inputs (it prepares the form for calculation)
                // 'estimateI' is the position of the estimate in the estimates array
                var estimateData = movingLeadData.estimates[currentEstimate];

                document.getElementById("unActiveEstimateAlert").style.display = "none";

                if (movingLeadData.movingSettingsData) {
                    $("#couponDiscount").val(parseFloat(movingLeadData.movingSettingsData.couponDiscount));
                    $("#seniorDiscount").val(parseFloat(movingLeadData.movingSettingsData.seniorDiscount));
                    $("#veteranDiscount").val(parseFloat(movingLeadData.movingSettingsData.veteranDiscount));
                    $("#fuelSurcharge").val(parseFloat(movingLeadData.movingSettingsData.calculateFuel));

                    leadController.moving.estimate.setPricePerBasedOnTariff();
                }else{
                    $("#generalDiscount").val(0);
                    $("#couponDiscount").val(0);
                    $("#seniorDiscount").val(0);
                    $("#veteranDiscount").val(0);
                    $("#fuelSurcharge").val(0);
                }

                // Set the FVP TABLE
                leadController.moving.estimate.fvp.setFVPtable();

                // ======= (START) FIRST - MAKE SURE ALL INPUTS ARE OPEN (NOT DISABLED) =======

                // empty extra charges
                document.getElementById("extraCharges").innerHTML = '';

                // empty materials
                document.getElementById("movingMaterials").innerHTML = '';

                // empty attached files
                document.getElementById("estimateAttachedFileDiv").style.display = 'none';
                document.getElementById("estimateAttachedFile").innerHTML = '';

                // =============== ENABLE ATTACHED FILES DELETE BUTTON ===============
                $('#estimatesTab').find('.closeIcon').removeClass('disabled');
                // =============== ENABLE ATTACHED FILES DELETE BUTTON ===============

                // =============== ENABLE ALL INPUTS IN ESTIMATE ===============
                $(".estimateRow button:not(.keepDisabled)").prop("disabled", false);
                $(".estimateRow input:not(.keepDisabled)").prop("disabled", false);
                $(".estimateRow select:not(.keepDisabled):not(.selected)").prop("disabled", false);
                $(".estimateRow textarea:not(.keepDisabled)").prop("disabled", false);
                // =============== ENABLE ALL INPUTS IN ESTIMATE ===============

                // === ENABLE ESTIMATE DISPLAY SETTINGS (display terms, display signing,display inventory) ===
                $("#showEstimateTerms").attr("disabled",false);
                $("#showEstimateSign").attr("disabled",false);
                $("#showEstimateInventory").attr("disabled",false);
                $("#showEstimatePayments").attr("disabled",false);
                // === ENABLE ESTIMATE DISPLAY SETTINGS (display terms, display signing,display inventory) ===

                $("#agentFeeType").val(0);
                $("#signArea").html("");
                $("#ipArea").html("");
                // ======= (END) FIRST - MAKE SURE ALL IMPUTS ARE OPEN (NOT DISABLED) =======

                // ======= (START) RESET INPUTS  =======
                document.getElementById("showEstimateTerms").checked = true;
                document.getElementById("showEstimateSign").checked = true;
                document.getElementById("showEstimateInventory").checked = true;
                document.getElementById("showEstimatePayments").checked = true;

                document.getElementById("discountsToggle").checked = false;
                document.getElementById("generalDiscount").disabled = true;
                document.getElementById("generalDiscountType").value = 0;
                document.getElementById("couponDiscount").disabled = true;
                document.getElementById("couponDiscountType").value = 0;
                document.getElementById("seniorDiscount").disabled = true;
                document.getElementById("seniorDiscountType").value = 0;
                document.getElementById("veteranDiscount").disabled = true;
                document.getElementById("veteranDiscountType").value = 0;

                document.getElementById("fvpToggle").checked = false;

                document.getElementById("packersToggle").checked = false;
                document.getElementById("packers").disabled = true;
                document.getElementById("packersHrs").disabled = true;
                document.getElementById("packersPerHrs").disabled = true;
                document.getElementById("unpackers").disabled = true;
                document.getElementById("unpackersHrs").disabled = true;
                document.getElementById("unpackersPerHrs").disabled = true;
                document.getElementById("packers").value = "0";
                document.getElementById("packersHrs").value = "0";
                document.getElementById("packersPerHrs").value = "0";
                document.getElementById("unpackers").value = "0";
                document.getElementById("unpackersHrs").value = "0";
                document.getElementById("unpackersPerHrs").value = "0";

                document.getElementById("valueProtectionAOL").value = "0";

                leadController.moving.estimate.fvp.changeFVPType();
                // ======= (END) RESET INPUTS  =======

                $("#estimateTotal").html('');

                if (estimateData != false && estimateData != undefined) {

                    document.getElementById("sendInvoiceBTN").disabled = false;

                    if(estimateData.isActive == "0"){
                        document.getElementById("unActiveEstimateAlert").style.display = "";
                    }else{
                        document.getElementById("unActiveEstimateAlert").style.display = "none";
                    }
                    // ====== (START) Set the CUSTOM BUTTONS ======
                    if(estimateData.displayTerms == "1"){
                        document.getElementById("showEstimateTerms").checked = true;
                    }else{
                        document.getElementById("showEstimateTerms").checked = false;
                    }

                    if(estimateData.displaySigning == "1"){
                        document.getElementById("showEstimateSign").checked = true;
                    }else{
                        document.getElementById("showEstimateSign").checked = false;
                    }

                    if(estimateData.displayInventory == "1"){
                        document.getElementById("showEstimateInventory").checked = true;
                    }else{
                        document.getElementById("showEstimateInventory").checked = false;
                    }

                    if(estimateData.displayPayments == "1"){
                        document.getElementById("showEstimatePayments").checked = true;
                    }else{
                        document.getElementById("showEstimatePayments").checked = false;
                    }
                    // ====== (END) Set the CUSTOM BUTTONS ======



                    // ====== (START) Set the ESTIMATE INIT SECTIONS ======
                    $('#estimateCForLBS').val(estimateData.calcType);
                    setPricePer();
                    $("#totalCF").val(parseFloat(estimateData.initPrice));
                    $("#perCF").val(parseFloat(estimateData.pricePerCf));
                    // ====== (START) Set the ESTIMATE INIT SECTIONS ======


                    // ====== (START) Set the FUEL & DISCOUNTS ======
                    $("#fuelSurcharge").val(parseFloat(estimateData.fuel));
                    $('#estimateFuelType').val(estimateData.fuelType);

                    $("#generalDiscount").val(parseFloat(estimateData.discount));
                    $("#generalDiscountType").val(parseInt(estimateData.discountType));
                    $("#couponDiscount").val(parseFloat(estimateData.coupon));
                    $("#couponDiscountType").val(parseInt(estimateData.couponType));
                    $("#seniorDiscount").val(parseFloat(estimateData.senior));
                    $("#seniorDiscountType").val(parseInt(estimateData.seniorType));
                    $("#veteranDiscount").val(parseFloat(estimateData.veteranDiscount));
                    $("#veteranDiscountType").val(parseInt(estimateData.veteranDiscountType));
                    if (estimateData.discount > 0 || estimateData.coupon > 0 || estimateData.senior > 0 || estimateData.veteranDiscount > 0) {
                        document.getElementById("discountsToggle").checked = true;
                        document.getElementById("generalDiscount").disabled = false;
                        document.getElementById("couponDiscount").disabled = false;
                        document.getElementById("seniorDiscount").disabled = false;
                        document.getElementById("veteranDiscount").disabled = false;
                    }
                    // ====== (END) Set the FUEL & DISCOUNTS ======

                    // ====== (START) Set the PACKERS ======
                    $("#packers").val(parseInt(estimateData.packingPackers));
                    $("#packersHrs").val(parseFloat(estimateData.packingHours));
                    $("#packersPerHrs").val(parseFloat(estimateData.packingPerHour));
                    $("#unpackers").val(parseInt(estimateData.unpackingPackers));
                    $("#unpackersHrs").val(parseFloat(estimateData.unpackingHours));
                    $("#unpackersPerHrs").val(parseFloat(estimateData.unpackingPerHour));
                    if (estimateData.packingPackers > 0 || estimateData.packingHours > 0 || estimateData.packingPerHour > 0 || estimateData.unpackingPackers > 0 || estimateData.unpackingHours > 0 || estimateData.unpackingPerHour > 0) {
                        document.getElementById("packersToggle").checked = true;
                        document.getElementById("packers").disabled = false;
                        document.getElementById("packersHrs").disabled = false;
                        document.getElementById("packersPerHrs").disabled = false;
                        document.getElementById("unpackers").disabled = false;
                        document.getElementById("unpackersHrs").disabled = false;
                        document.getElementById("unpackersPerHrs").disabled = false;
                    }
                    // ====== (END) Set the PACKERS ======

                    // ====== (START) Set the AGENT FEE & MOVING MATERIALS & MONTHLY STORAGE ======
                    $("#agentFee").val(parseFloat(estimateData.agentFee));
                    $("#agentFeeType").val(parseInt(estimateData.agentFeeType));
                    $("#monthlyStorageFee").val(parseFloat(estimateData.storageFee));
                    // ====== (END) Set the AGENT FEE & MOVING MATERIALS & MONTHLY STORAGE ======

                    // ====== (START) Set the COMMENTS ======
                    $("#estimateComments").val(estimateData.estimateComments);
                    // ====== (START) Set the COMMENTS ======

                    // ====== (START) Set the EXTRA CHARGES ======
                    if (estimateData.extraCharges){
                        for (var i = 0; i < estimateData.extraCharges.length; i++) {
                            var current = estimateData.extraCharges[i];
                            leadController.moving.estimate.createExtra(current.title,current.price);
                        }
                    }
                    // ====== (END) Set the EXTRA CHARGES ======

                    // ====== (START) Set the FVP ======
                    if(estimateData.valueProtectionAdded == "1"){
                        document.getElementById("fvpToggle").checked = true;
                    }else{
                        document.getElementById("fvpToggle").checked = false;
                    }
                    if(estimateData.valueProtectionType == "1"){
                        document.getElementById("valueProtectionType1").checked = true;
                    }else{
                        document.getElementById("valueProtectionType2").checked = true;
                    }
                    $("#valueProtectionAOL").val(estimateData.valueProtectionAOL);

                    var cells = document.getElementsByName("FVPda");
                    for (var i = 0; i < cells.length; i++) {
                        console.log(estimateData.valueProtectionDA);
                        if(cells[i].value == estimateData.valueProtectionDA){
                            cells[i].checked = true;
                        }
                    }

                    leadController.moving.estimate.fvp.changeFVPType();

                    console.log('=== SET THE FVP ===');
                    console.log(estimateData);
                    console.log('=== SET THE FVP ===');
                    // ====== (END) Set the FVP ======

                    // ====== (START) Set the MATERIALS ======
                    if (estimateData.materials){
                        for (var i = 0; i < estimateData.materials.length; i++) {
                            var material = estimateData.materials[i];
                            leadController.moving.estimate.addMaterial(material.amount,material.title,material.totalPrice);
                        }
                    }
                    // ====== (END) Set the MATERIALS ======

                    // ====== (START) Set the ATTACHED FILES ======
                    if (estimateData.attachedFiles){
                        for (var i = 0; i < estimateData.attachedFiles.length; i++){
                            var attachedFile = estimateData.attachedFiles[i];
                            leadController.moving.estimate.addFile(attachedFile);
                        }
                    }
                    // ====== (START) Set the ATTACHED FILES ======

                    // ====== (START) Set the ESTIMATE TOTAL ======
                    if (estimateData.totalEstimate > 0 && estimateData.isActive == "1"){
                        $("#estimateTotal").html('<span class="label label-primary pull-right">$'+estimateData.totalEstimate.toFixed(2)+'</span>');
                    }else{
                        $("#estimateTotal").html('');
                    }
                    // ====== (END) Set the ESTIMATE TOTAL ======


                    // ====== IF CUSTOMER SIGNED THE CONTRACT THEN CLOSE THE ESTIMATE ======
                    if (estimateData.customerSignature){


                        var extraPrice = document.getElementsByName("Extra-Price[]");
                        var extraText = document.getElementsByName("Extra-Text[]");

                        for (var e = 0; e < extraPrice.length; e++){
                            extraText[e].disabled = true;
                            extraPrice[e].disabled = true;
                        }

                        // =============== DISABLE ATTACHED FILES DELETE BUTTON ===============
                        $('#estimatesTab').find('.closeIcon').addClass('disabled');
                        // =============== DISABLE ATTACHED FILES DELETE BUTTON ===============

                        // =============== DISABLE ALL INPUTS IN ESTIMATE ===============
                        $(".estimateRow button:not(.keepDisabled)").prop("disabled", true);
                        $(".estimateRow input:not(.keepDisabled)").prop("disabled", true);
                        $(".estimateRow select:not(.keepDisabled)").prop("disabled", true);
                        $(".estimateRow textarea:not(.keepDisabled)").prop("disabled", true);

                        $("#openEstimateBTN").prop("disabled", false); // keep the 'open estimate' button active
                        $("#sendInvoiceBTN").prop("disabled", false); // keep the 'send invoice' button active
                        // =============== DISABLE ALL INPUTS IN ESTIMATE ===============




/*
                        $("#totalCF").attr("disabled",true);
                        $("#extraChargeBtn").attr("disabled",true);

                        $("#addMaterialBtn").attr("disabled",true);
                        $("#attachFileBTN").attr("disabled",true);
                        $("#saveBtn").attr("disabled",true);
                        $("#estimateCForLBS").attr("disabled",true);
                        $("#estimateFuelType").attr("disabled",true);
                        $("#perCF").attr("disabled",true);
                        $("#fuelSurcharge").attr("disabled",true);
                        $("#discountsToggle").attr("disabled",true);
                        $("#packersToggle").attr("disabled",true);

                        // === DISABLE ESTIMATE DISCOUNTS ===
                        $("#generalDiscount").attr("disabled",true);
                        $("#couponDiscount").attr("disabled",true);
                        $("#seniorDiscount").attr("disabled",true);
                        $("#veteranDiscount").attr("disabled",true);
                        // === DISABLE ESTIMATE DISCOUNTS ===

                        // === DISABLE ESTIMATE PACKERS ===
                        $("#packers").attr("disabled",true);
                        $("#packersHrs").attr("disabled",true);
                        $("#packersPerHrs").attr("disabled",true);
                        $("#unpackers").attr("disabled",true);
                        $("#unpackersHrs").attr("disabled",true);
                        $("#unpackersPerHrs").attr("disabled",true);
                        // === DISABLE ESTIMATE PACKERS ===

                                                $("#estimateComments").attr("disabled",true);

*/
                        // === DISABLE ESTIMATE DISPLAY SETTINGS (display terms, display signing,display inventory) ===
                        $("#showEstimateTerms").attr("disabled",true);
                        $("#showEstimateSign").attr("disabled",true);
                        $("#showEstimateInventory").attr("disabled",true);
                        $("#showEstimatePayments").attr("disabled",true);
                        // === DISABLE ESTIMATE DISPLAY SETTINGS (display terms, display signing,display inventory) ===


                        $("#signArea").html(
                            "<div class='row' style='display: flex; flex-wrap: wrap; margin-top: 1rem;'>" +
                            "<div class='col-md-4 signItem' style='text-align: center;font-size: 30px; margin-top: auto;'>" +
                            estimateData.customerName +
                            "<span style=\"border-top: 1px solid black;display: block;width: 80%;margin-left: 35px\"></span>" +
                            "<span style='text-align: center;display: block;width: 100%;font-size: 12px;'>Customer Name</span>" +
                            "</div>" +
                            "<div class='col-md-4 signItem' style='margin-top: auto;'>" +
                            "<img style='width:100%' src='" + estimateData.customerSignature + "'>" +
                            "<span style=\"border-top: 1px solid black;display: block;width: 80%;margin-left: 35px\"></span>" +
                            "<span style='text-align: center;display: block;width: 100%;font-size: 12px;'>Customer Signature</span>" +
                            "</div>" +
                            "<div class='col-md-4 signItem' style='text-align: center;font-size: 30px; margin-top: auto;'>" +
                            estimateData.dateSigned + '<br>' +
                            estimateData.timeSigned +
                            "<span style=\"border-top: 1px solid black;display: block;width: 80%;margin-left: 35px\"></span>" +
                            "<span style='text-align: center;display: block;width: 100%;font-size: 12px;'>Date</span>" +
                            "</div>" +
                            "</div>"
                        );

                        $("#ipArea").html(
                            "<div>Customer IP: " + estimateData.customerIP +"</div>"
                        );
                    }
                    // ====== IF CUSTOMER SIGNED THE CONTRACT THEN CLOSE THE ESTIMATE ======

                } else {

                    document.getElementById("sendInvoiceBTN").disabled = true;

                    // ====== (START) RESET THE INPUTS ======
                    $("#generalDiscount").val("0");
                    $("#agentFee").val("0");
                    $("#estimateComments").val("");
                    // ====== (END) RESET THE INPUTS ======


                    // ====== (START) SET THE DEFAULT INIT PRICE CALCULATION (CF/LBS/HOURS/USD) ======
                    $("#estimateCForLBS").val(movingLeadData.movingSettingsData.calculateBy);
                    setPricePer();
                    // ====== (END) SET THE DEFAULT INIT PRICE CALCULATION (CF/LBS/HOURS/USD) ======

                    // ======== START SETTING THE INIT (totalCF) ========
                    if (movingLeadData.totalCF) {
                        var totalSize = totalCF;
                        if (movingLeadData.movingSettingsData.calculateBy == 0) {
                            totalSize = totalSize * movingLeadData.movingSettingsData.cflbsratio;
                        }else if (movingLeadData.movingSettingsData.calculateBy == 2){
                            totalSize = 0;
                        }

                        $("#totalCF").val(parseInt(totalSize));
                    } else {
                        $("#totalCF").val(0);
                    }
                    // ======== END SETTING THE INIT (totalCF) ========

                }

                leadController.moving.estimate.fvp.toggleFVP();
                leadController.moving.estimate.togglePackers();
                leadController.moving.estimate.toggleDiscounts();
                leadController.moving.estimate.initMovingEstimate();

            },
            setPricePerBasedOnTariff:function(){
                var tariff = movingLeadData.movingSettingsData.tariff;
                var fromState = movingLeadData.movingLeadData.fromState;
                var tariffOfCurrentState = tariff[fromState];

                // Reset the base tariff
                $("#perCF").val(0);

                if(tariffOfCurrentState != null && tariffOfCurrentState != undefined){

                    var a = parseFloat(tariffOfCurrentState.cf);
                    var tariffInCF = parseFloat(tariffOfCurrentState.cf).toFixed(2);

                    if(movingLeadData.movingSettingsData.calculateBy == "1"){
                        // The calculation is by CF
                        $("#perCF").val(tariffInCF);
                    }
                }
            },
            saveCalc:function () {
                leadController.moving.estimate.initMovingEstimate();

                var estimateSaveBtn = $("#saveBtn").ladda();
                estimateSaveBtn.ladda("start");

                // ======================== GET EXTRA CHARGES ========================
                var extraCharges = [];

                var extraPrice = document.getElementsByName("Extra-Price[]");
                var extraText = document.getElementsByName("Extra-Text[]");
                var extraTotal = document.getElementsByName("Extra-Total[]");

                for (var i=0;i<extraPrice.length;i++){
                    if (extraPrice[i].value) {
                        var arr = {
                            'title': extraText[i].value,
                            "price": extraPrice[i].value
                        };
                        extraCharges.push(arr);
                    }
                }
                // ======================== GET EXTRA CHARGES ========================

                // ======================== GET MATERIALS ========================
                var materials = [];

                var materialsName = document.getElementsByName("materials-name[]");
                var materialsAmount = document.getElementsByName("materials-amount[]");
                var materialsTotal = document.getElementsByName("materials-total[]");

                for (var i=0;i<materialsName.length;i++){
                    if (materialsName[i].value) {
                        var itemPrice = materialsName[i].options[materialsName[i].selectedIndex].getAttribute("data-price");

                        var arr = {
                            'amount': materialsAmount[i].value,
                            'title': materialsName[i].value,
                            'pricePerItem': itemPrice,
                            'totalPrice': materialsTotal[i].value
                        };
                        materials.push(arr);
                    }
                }
                // ======================== GET MATERIALS ========================

                // ======================== GET ATTACHED FILES ========================
                var attachedFiles = [];

                var estimateFileName = document.getElementsByName("estimateFileName[]");
                var estimateFileKey = document.getElementsByName("estimateFileKey[]");
                var estimateFileURL = document.getElementsByName("estimateFileURL[]");
                for (var i=0;i<estimateFileName.length;i++){
                        var singleFile = {
                            "fileName": estimateFileName[i].value,
                            "fileKey": estimateFileKey[i].value,
                            "fileURL": estimateFileURL[i].value
                        };
                    attachedFiles.push(singleFile);

                }
                // ======================== GET ATTACHED FILES ========================

                var FVPoption = document.querySelector('input[name="FVPoption"]:checked').value;
                var FVPda = document.querySelector('input[name="FVPda"]:checked').value;

                var data = {
                    "initPrice":parseFloat(document.getElementById("totalCF").value),
                    "calcType":parseFloat(document.getElementById("estimateCForLBS").value),
                    "pricePerCf":parseFloat(document.getElementById("perCF").value),
                    "fuel":parseFloat(document.getElementById("fuelSurcharge").value),
                    "fuelType":parseInt(document.getElementById("estimateFuelType").value),
                    "discount":parseFloat(document.getElementById("generalDiscount").value),
                    "discountType":parseInt(document.getElementById("generalDiscountType").value),
                    "coupon":parseFloat(document.getElementById("couponDiscount").value),
                    "couponType":parseInt(document.getElementById("couponDiscountType").value),
                    "senior":parseFloat(document.getElementById("seniorDiscount").value),
                    "seniorType":parseInt(document.getElementById("seniorDiscountType").value),
                    "veteranDiscount":parseFloat(document.getElementById("veteranDiscount").value),
                    "veteranDiscountType":parseInt(document.getElementById("veteranDiscountType").value),
                    "packingPackers":parseFloat(document.getElementById("packers").value),
                    "packingHours":parseFloat(document.getElementById("packersHrs").value),
                    "packingPerHour":parseFloat(document.getElementById("packersPerHrs").value),
                    "unpackingPackers":parseFloat(document.getElementById("unpackers").value),
                    "unpackingHours":parseFloat(document.getElementById("unpackersHrs").value),
                    "unpackingPerHour":parseFloat(document.getElementById("unpackersPerHrs").value),
                    "storageFee":parseFloat(document.getElementById("monthlyStorageFee").value),
                    "extraCharges":extraCharges,
                    "agentFee":parseFloat(document.getElementById("agentFee").value),
                    "agentFeeType":parseInt(document.getElementById("agentFeeType").value),
                    "valueProtectionAdded":document.getElementById("fvpToggle").checked,
                    "valueProtectionType":FVPoption,
                    "valueProtectionAOL":document.getElementById("valueProtectionAOL").value,
                    "valueProtectionCharge":document.getElementById("valueProtectionCharge").value,
                    "valueProtectionDA":FVPda,
                    "showEstimateTerms":document.getElementById("showEstimateTerms").checked,
                    "showEstimateSign":document.getElementById("showEstimateSign").checked,
                    "showEstimateInventory":document.getElementById("showEstimateInventory").checked,
                    "showEstimatePayments":document.getElementById("showEstimatePayments").checked,
                    "estimateComments":document.getElementById("estimateComments").value,
                    "attachedFiles":attachedFiles,
                    "materials":materials
                };

                console.log(data);

                if (document.getElementById("discountsToggle").checked == false){
                    data.discount = 0;
                    data.coupon = 0;
                    data.senior = 0;
                    data.veteranDiscount = 0;
                }
                if (document.getElementById("packersToggle").checked == false){
                    data.packingPackers = 0;
                    data.packingHours = 0;
                    data.packingPerHour = 0;
                    data.unpackingPackers = 0;
                    data.unpackingHours = 0;
                    data.unpackingPerHour = 0;
                }

                if (document.getElementById("fvpToggle").checked == false){
                    data.valueProtectionType = 0;
                    data.valueProtectionAOL = 0;
                    data.valueProtectionDA = 0;
                    data.valueProtectionCharge = 0;
                }

                var estimateId = false;
                if(movingLeadData.estimates[currentEstimate] != false){
                    estimateId = movingLeadData.estimates[currentEstimate].id;
                }

                var strUrl = BASE_URL + '/console/actions/moving/leads/setEstimateCalculation.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        leadId: leadId,
                        estimateId:estimateId,
                        data: data
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {

                    leadController.lead.getLeadMovingDetails();
                    try {
                        data = JSON.parse(data);
                        if (data != false) {
                            toastr.success("Estimate Saved", "Saved");
                        }else{
                            toastr.error("Error updating estimate","Error");
                        }
                        estimateSaveBtn.ladda("stop");
                    }catch (e) {

                    }
                });

            },
            createExtra:function (title,price) {

                var container = document.getElementById("extraCharges");
                var divRow = document.createElement("div");
                divRow.classList.add("row");

                var label = document.createElement("label");
                label.classList.add("col-sm-3");
                label.classList.add("control-label");

                var closeIcon = document.createElement("span");
                closeIcon.className = "closeIcon";
                closeIcon.setAttribute("style","float: left;margin-right: 7px;");
                closeIcon.innerHTML = "×";
                closeIcon.setAttribute("onClick","leadController.moving.estimate.removeExtra(this)");

                var labelText = document.createElement("span");
                labelText.innerHTML = "Extra Charge";

                label.appendChild(closeIcon);
                label.appendChild(labelText);

                divRow.appendChild(label);

                var divCol = document.createElement("div");
                divCol.classList.add("col-sm-9");

                var divInputGroup = document.createElement("div");
                divInputGroup.classList.add("input-group");
                divInputGroup.classList.add("m-b");
                divInputGroup.style.width = "215px";
                divInputGroup.style.display = "inline-table";

                var inputForm = document.createElement("input");
                inputForm.classList.add("form-control");
                inputForm.classList.add("estimateChangable");
                inputForm.name = "Extra-Text[]";
                inputForm.setAttribute("type", "text");
                inputForm.setAttribute("placeholder", "Title");
                if(title != undefined){
                    inputForm.value = title;
                }

                divInputGroup.appendChild(inputForm);
                divCol.appendChild(divInputGroup);

                var divInputGroup = document.createElement("div");
                divInputGroup.classList.add("input-group");
                divInputGroup.classList.add("m-b");
                divInputGroup.style.width = "215px";
                divInputGroup.style.display = "inline-table";
                divInputGroup.style.marginLeft = "3px";

                var inputForm = document.createElement("input");
                inputForm.classList.add("form-control");
                inputForm.classList.add("estimateChangable");
                inputForm.name = "Extra-Price[]";
                inputForm.onChange = "leadController.moving.estimate.initMovingEstimate();";
                inputForm.setAttribute("onChange","leadController.moving.estimate.initMovingEstimate()");
                inputForm.setAttribute("type", "number");
                inputForm.setAttribute("min", '0');
                inputForm.setAttribute("step", "0.5");
                inputForm.setAttribute("placeholder", "Price");
                if(price != undefined){
                    inputForm.value = price;
                }
                var spanAddon = document.createElement("span");
                spanAddon.classList.add("input-group-addon");
                var spanAddonText = document.createTextNode("$");

                spanAddon.appendChild(spanAddonText);

                divInputGroup.appendChild(inputForm);
                divInputGroup.appendChild(spanAddon);

                var div = document.createElement("div");
                div.style.width = "93px";
                div.style.cssFloat = "right";
                div.style.marginRight = "3px";

                var inputTotal = document.createElement("input");
                inputTotal.setAttribute("type", "text");
                inputTotal.classList.add("form-control");
                inputTotal.name = "Extra-Total[]";
                inputTotal.setAttribute("disabled", "disabled");
                inputTotal.style.backgroundColor = "#efefef";
                inputTotal.style.display = "inline-table";
                inputTotal.style.width = "70px";
                inputTotal.style.cssFloat = "right";
                inputTotal.value = '0';
                var label = document.createElement("label");
                label.style.cssFloat = "right";
                label.style.marginRight = "3px";
                label.style.marginTop = "1px";
                label.style.fontSize = "19px";
                label.style.fontWeight = "normal";
                label.style.display = "inline-table";

                var labelText = document.createTextNode("$");
                label.appendChild(labelText);

                div.appendChild(inputTotal);
                div.appendChild(label);

                divCol.appendChild(divInputGroup);
                divCol.appendChild(div);
                divRow.appendChild(divCol);

                container.appendChild(divRow);
            },
            removeExtra:function (obj) {

                var o = obj.parentNode.parentNode;
                o.parentNode.removeChild(o);

                // re-calculate the estimate
                leadController.moving.estimate.initMovingEstimate();
            },
            removeMaterial:function (obj) {

                var o = obj.parentNode.parentNode;
                o.parentNode.removeChild(o);

                // re-calculate the estimate
                leadController.moving.estimate.initMovingEstimate();
            },
            toggleDiscounts:function () {
                var obj = document.getElementById("discountsToggle");

                if (obj.checked == true){
                    $("#generalDiscount").attr("disabled",false);
                    $("#seniorDiscount").attr("disabled",false);
                    $("#couponDiscount").attr("disabled",false);
                    $("#veteranDiscount").attr("disabled",false);

                    $( "#generalDiscountRowDiv" ).removeClass( "divFeatureDisabled" );
                    $( "#couponDiscountRowDiv" ).removeClass( "divFeatureDisabled" );
                    $( "#seniorDiscountRowDiv" ).removeClass( "divFeatureDisabled" );
                    $( "#veteranDiscountRowDiv" ).removeClass( "divFeatureDisabled" );

                }else{
                    $("#generalDiscount").attr("disabled",true);
                    $("#seniorDiscount").attr("disabled",true);
                    $("#couponDiscount").attr("disabled",true);
                    $("#veteranDiscount").attr("disabled",true);

                    $( "#generalDiscountRowDiv" ).addClass( "divFeatureDisabled" );
                    $( "#couponDiscountRowDiv" ).addClass( "divFeatureDisabled" );
                    $( "#seniorDiscountRowDiv" ).addClass( "divFeatureDisabled" );
                    $( "#veteranDiscountRowDiv" ).addClass( "divFeatureDisabled" );
                }
                leadController.moving.estimate.initMovingEstimate();
            },
            togglePackers:function () {
                var obj = document.getElementById("packersToggle");
                if (obj.checked == true){
                    $("#packers").attr("disabled",false);
                    $("#packersHrs").attr("disabled",false);
                    $("#packersPerHrs").attr("disabled",false);

                    $("#unpackers").attr("disabled",false);
                    $("#unpackersHrs").attr("disabled",false);
                    $("#unpackersPerHrs").attr("disabled",false);

                    $( "#packingRowDiv" ).removeClass( "divFeatureDisabled" );
                    $( "#unpackingRowDiv" ).removeClass( "divFeatureDisabled" );
                }else{
                    $("#packers").attr("disabled",true);

                    $("#packersHrs").attr("disabled",true);

                    $("#packersPerHrs").attr("disabled",true);


                    $("#unpackers").attr("disabled",true);

                    $("#unpackersHrs").attr("disabled",true);

                    $("#unpackersPerHrs").attr("disabled",true);

                    $( "#packingRowDiv" ).addClass( "divFeatureDisabled" );
                    $( "#unpackingRowDiv" ).addClass( "divFeatureDisabled" );
                }
                leadController.moving.estimate.initMovingEstimate();
            },
            activateEstimate:function (estimateId) {

                var strUrl = BASE_URL + '/console/actions/moving/leads/activateEstimate.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        leadId: leadId,
                        estimateId:estimateId
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        leadController.lead.getLeadMovingDetails();
                        leadController.lead.getLeadPaymentDetails();

                    } catch (e) {
                        toastr.error("Cannot update estimate", "Error");
                    }
                });

},
            disableEstimate:function (estimateId) {

                var strUrl = BASE_URL + '/console/actions/moving/leads/disableEstimate.php', strReturn = "";
                jQuery.ajax({
                    url: strUrl,
                    method: "post",
                    data: {
                        leadId: leadId,
                        estimateId:estimateId
                    },
                    success: function (html) {
                        strReturn = html;
                    },
                    async: true
                }).done(function (data) {
                    try {
                        data = JSON.parse(data);
                        leadController.lead.getLeadMovingDetails();
                    } catch (e) {
                        toastr.error("Cannot update estimate", "Error");
                    }
                });

            },
            clearUserContract:function (estimateId) {
                var globalEstimateId = estimateId;

                var content = document.createElement("div");
                content.innerHTML = "This estimate is already signed for.<br> In the event that you would like to make changes the signature will be erased.";
                swal({
                    title: "Are you sure?",
                    content: content,
                    icon: "warning",
                    dangerMode: true,
                    buttons: {
                        cancel: true,
                        ok: {
                            text:"OK",
                            className: "swal-button--danger",
                            value:"ok"
                        }
                    },
                    className:"text-center"
                }).then(function(isConfirm) {
                    if (isConfirm) {


                        var strUrl = BASE_URL + '/console/actions/moving/leads/clearEstimateContract.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "post",
                            data: {
                                leadId: leadId,
                                estimateId:globalEstimateId
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {
                            try {
                                data = JSON.parse(data);
                                leadController.lead.getLeadMovingDetails();
                            } catch (e) {
                                toastr.error("Cannot update estimate", "Error");
                            }
                        });


                    }
                });
            },
            deleteEstimate:function (estimateId) {
                var globalEstimateId = estimateId;
                var content = document.createElement("div");
                content.innerHTML = "This estimate will be erased.";
                swal({
                    title: "Are you sure?",
                    content: content,
                    icon: "warning",
                    dangerMode: true,
                    buttons: {
                        cancel: true,
                        ok: {
                            text:"OK",
                            className: "swal-button--danger",
                            value:"ok"
                        }
                    },
                    className:"text-center"
                }).then(function(isConfirm) {
                    if (isConfirm) {

                        var strUrl = BASE_URL + '/console/actions/moving/leads/deleteEstimate.php', strReturn = "";
                        jQuery.ajax({
                            url: strUrl,
                            method: "post",
                            data: {
                                leadId: leadId,
                                estimateId:globalEstimateId
                            },
                            success: function (html) {
                                strReturn = html;
                            },
                            async: true
                        }).done(function (data) {
                            try {
                                data = JSON.parse(data);
                                currentEstimate = null;
                                leadController.lead.getLeadMovingDetails();
                            } catch (e) {
                                toastr.error("Cannot update estimate", "Error");
                            }
                        });

                    }
                });

            },
            removeFile:function(obj,e){
                e.preventDefault();

                swal({
                    title: "Are you sure?",
                    text: "This will remove the file from the estimate",
                    dangerMode: true,
                    buttons: {
                        cancel: true,
                        save: {
                            text: "Yes",
                            value: "Yes"
                        }
                    },
                }).then(function(isConfirm) {

                    if (isConfirm) {

                        var o = obj.parentNode.parentNode.parentNode.parentNode;
                        o.parentNode.removeChild(o);
                        var estimateFileName = document.getElementsByName("estimateFileName[]");
                        if(estimateFileName.length == 0){
                            // No files - hide the files section
                            document.getElementById("estimateAttachedFileDiv").style.display = 'none';
                        }

                    }
                });


            },
            addFile:function (file) {

                // Make sure the files are visible
                document.getElementById("estimateAttachedFileDiv").style.display = '';

                var filebox = document.createElement("div");
                filebox.className = "file-box";
                filebox.setAttribute("style","width: 140px;margin-top: 20px;margin-right: 20px;float:none;display: inline-table;");

                var fileDiv = document.createElement("div");
                fileDiv.className = "file";
                fileDiv.setAttribute("style","margin: 0px;");

                var a = document.createElement("a");
                a.setAttribute("target","_blank");
                a.setAttribute("href",BASE_URL+'/api/files/getFileByKey.php?key='+file.fileKey+'&fileURL='+file.fileURL);

                var corner = document.createElement("span");
                corner.setAttribute("style","position: absolute;right: 4px;bottom: 4px;");
                var removeBtn = document.createElement("button");
                removeBtn.className = "closeIcon pull-right btn btn-danger btn-xs";
                removeBtn.setAttribute("onClick","leadController.moving.estimate.removeFile(this,event);");
                removeBtn.setAttribute("style","font-size: 8px;line-height: unset !important;");
                removeBtn.innerHTML = 'X';

                var icon = document.createElement("div");
                icon.className = "icon";
                icon.innerHTML = '<i class="fa fa-file"></i>';

                var filenamediv = document.createElement("div");
                filenamediv.className = "file-name";
                filenamediv.setAttribute("style","padding: 4px;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;overflow: hidden;text-overflow: ellipsis;height: 62px;");
                filenamediv.innerHTML = file.fileName;


                corner.appendChild(removeBtn);
                a.appendChild(corner);
                a.appendChild(icon);
                a.appendChild(filenamediv);

                fileDiv.appendChild(a);

                filebox.appendChild(fileDiv);

                var hiddens = document.createElement("div");
                hiddens.setAttribute("style","float: left;");

                var hidden1 = document.createElement("input");
                hidden1.setAttribute("type","hidden");
                hidden1.name = "estimateFileName[]";
                hidden1.value = file.fileName;

                var hidden2 = document.createElement("input");
                hidden2.setAttribute("type","hidden");
                hidden2.name = "estimateFileKey[]";
                hidden2.value = file.fileKey;

                var hidden3 = document.createElement("input");
                hidden3.setAttribute("type","hidden");
                hidden3.name = "estimateFileURL[]";
                hidden3.value = file.fileURL;

                hiddens.appendChild(hidden1);
                hiddens.appendChild(hidden2);
                hiddens.appendChild(hidden3);

                filebox.appendChild(hiddens);

                document.getElementById("estimateAttachedFile").appendChild(filebox);
            },
            addMaterial:function (amount,title,totalPrice) {
                var container = document.getElementById("movingMaterials");

                var divRow = document.createElement("div");
                divRow.classList.add("row");


                var divCol = document.createElement("label");
                divCol.classList.add("col-sm-3");
                divCol.classList.add("control-label");
                divCol.innerHTML = 'Material';

                var closeIcon = document.createElement("span");
                closeIcon.className = "closeIcon";
                closeIcon.setAttribute("style","float: left;margin-right: 7px;");
                closeIcon.innerHTML = "×";
                closeIcon.setAttribute("onClick","leadController.moving.estimate.removeMaterial(this)");

                divCol.appendChild(closeIcon);
                divRow.appendChild(divCol);

                var divCol = document.createElement("div");
                divCol.classList.add("col-sm-9");
                divCol.classList.add("control-label");

                // ITEM AMOUNT
                var divInputGroup = document.createElement("div");
                divInputGroup.classList.add("input-group");
                divInputGroup.setAttribute("style","width: 71px;margin-left: 3px;display: inline-table;");

                var materialsCount = document.createElement("select");
                materialsCount.classList.add("form-control");
                materialsCount.classList.add("estimateChangable");
                materialsCount.name = "materials-amount[]";
                materialsCount.setAttribute("style","height: 29px");

                for(var i = 1;i<100;i++){
                    var o = document.createElement("option");
                    o.value = i;
                    o.innerHTML = i;

                    if(amount !== undefined && amount == i){
                        o.selected = true;
                    }
                    materialsCount.appendChild(o);
                }

                divInputGroup.appendChild(materialsCount);
                divCol.appendChild(divInputGroup);

                // ITEM NAME
                var divInputGroup = document.createElement("div");
                divInputGroup.classList.add("input-group");
                divInputGroup.setAttribute("style","width:215px;margin-left: 3px;display: inline-table;");

                var materialsSelect = document.createElement("select");
                materialsSelect.className = "form-control";
                materialsSelect.setAttribute("style","height: 29px");
                materialsSelect.name = "materials-name[]";

                var materialOption = document.createElement("option");
                materialOption.innerHTML = "Select material";
                materialOption.value = "";

                materialsSelect.appendChild(materialOption);

                // ============== CALCULAT THE TOTAL IF MATERIAL ITEM ==============
                $( materialsSelect ).change(function() {
                    var itemCount = materialsCount.value;

                    var materialTotalPrice = parseFloat(itemCount*this.options[this.selectedIndex].getAttribute("data-price"));

                    inputTotal.value = materialTotalPrice.toFixed(2);
                    leadController.moving.estimate.initMovingEstimate();
                });
                $( materialsCount ).change(function() {
                    var itemCount = this.value;
                    var materialTotalPrice = parseFloat(itemCount*materialsSelect.options[materialsSelect.selectedIndex].getAttribute("data-price"));

                    inputTotal.value = materialTotalPrice.toFixed(2);
                    leadController.moving.estimate.initMovingEstimate();
                });
                // ============== CALCULAT THE TOTAL IF MATERIAL ITEM ==============

                for(var i = 0;i<movingLeadData.materials.length;i++){
                    var materialOption = document.createElement("option");
                    materialOption.setAttribute("data-price",movingLeadData.materials[i].localPrice);
                    materialOption.value = movingLeadData.materials[i].title;
                    materialOption.innerHTML = movingLeadData.materials[i].title +' ($'+movingLeadData.materials[i].localPrice+')';

                    if(title !== undefined && title == movingLeadData.materials[i].title){
                        materialOption.selected = true;
                    }

                    materialsSelect.appendChild(materialOption);
                }

                divInputGroup.appendChild(materialsSelect);

                divCol.appendChild(divInputGroup);

                var div = document.createElement("div");
                div.style.width = "93px";
                div.style.cssFloat = "right";
                div.style.marginRight = "3px";

                var inputTotal = document.createElement("input");
                inputTotal.setAttribute("type", "text");
                inputTotal.classList.add("form-control");
                inputTotal.name = "materials-total[]";
                inputTotal.setAttribute("onChange","leadController.moving.estimate.initMovingEstimate()");
                inputTotal.setAttribute("type", "number");
                inputTotal.setAttribute("min", '0.00');
                inputTotal.setAttribute("step", '0.01');
                inputTotal.style.display = "inline-table";
                inputTotal.style.width = "70px";
                inputTotal.style.cssFloat = "right";
                if(totalPrice !== undefined){
                    inputTotal.value = parseFloat(totalPrice).toFixed(2);
                }else{
                    inputTotal.value = parseFloat('0.00').toFixed(2);
                }

                var label = document.createElement("label");
                label.style.cssFloat = "right";
                label.style.marginRight = "3px";
                label.style.marginTop = "1px";
                label.style.fontSize = "19px";
                label.style.fontWeight = "normal";
                label.style.display = "inline-table";

                var labelText = document.createTextNode("$");
                label.appendChild(labelText);

                div.appendChild(inputTotal);
                div.appendChild(label);

                divCol.appendChild(div);
                divRow.appendChild(divCol);

                container.appendChild(divRow);
            },
            fvp:{
                toggleFVP:function () {
                    var obj = document.getElementById("fvpToggle");
                    if (obj.checked == true){
                        // Allow FVP
                        $( "#FVProwDiv" ).removeClass( "divFeatureDisabled" );
                    }else{
                        // Disable FVP
                        $( "#FVProwDiv" ).addClass( "divFeatureDisabled" );
                    }
                    leadController.moving.estimate.initMovingEstimate();
                },
                changeFVPType:function(){
                    var FVPoption = document.querySelector('input[name="FVPoption"]:checked').value;

                    if(FVPoption == "true"){
                        document.getElementById("valueProtectionAOL").disabled = false;
                        var cells = document.getElementsByName("FVPda");
                        for (var i = 0; i < cells.length; i++) {
                            cells[i].disabled = false;
                        }
                    }else{
                        document.getElementById("valueProtectionAOL").disabled = true;
                        var cells = document.getElementsByName("FVPda");
                        for (var i = 0; i < cells.length; i++) {
                            cells[i].disabled = true;
                        }
                    }
                },
                setFVPtable:function () {

                    var fullvalueprotection = movingLeadData.fullvalueprotection;

                    var data = fullvalueprotection.levels;

                    // ===== SET =====
                    var FVPdaList = document.getElementById("FVPdaList");
                    FVPdaList.innerHTML = "";

                    if(data.length > 0){
                        for(var i = 0;i<data.length;i++){

                            var wrapper = document.createElement("div");
                            wrapper.setAttribute("style", "display:flex;flex-direction: column;margin-right: 1rem;");

                            var radioWrapper = document.createElement("div");
                            radioWrapper.setAttribute("style", "margin:auto;");

                            var div = document.createElement("div");
                            div.className = "radio";
                            div.setAttribute("style", "display: flex;flex-direction: column;");

                            var input = document.createElement("input");
                            input.type = "radio";
                            input.name = "FVPda";
                            input.id = "FVPda"+i;
                            input.className = "form-control";
                            if(i == 0){input.checked = true;}
                            input.setAttribute("onchange", "leadController.moving.estimate.fvp.calculateFVP();leadController.moving.estimate.initMovingEstimate();");
                            input.setAttribute("style", "margin:auto; width: 0");
                            input.value = data[i];

                            var label = document.createElement("label");
                            label.setAttribute("for", "FVPda"+i);
                            label.setAttribute("style", "padding: 0;");

                            var amount = document.createElement("div");
                            amount.innerHTML = "$"+data[i];
                            amount.setAttribute("style", "text-align:center");

                            div.appendChild(input);
                            div.appendChild(label);

                            radioWrapper.appendChild(div);


                            wrapper.appendChild(radioWrapper);
                            wrapper.appendChild(amount);


                            FVPdaList.appendChild(wrapper);

                        }
                    }
                    // ===== SET =====

                },
                setFVPAOL:function(){
                    var totalCF = document.getElementById("totalCF").value;
                    var estimateCForLBS = document.getElementById("estimateCForLBS").value;

                    if(estimateCForLBS == "0"){
                        // LBS
                        // Multiply by 6
                        var DAOL = totalCF*6;
                        document.getElementById("valueProtectionAOL").value = DAOL;
                    }
                    if(estimateCForLBS == "1"){
                        // CF
                        // First, convert to LBS and then multiply by 6

                        var convertionRatio = movingLeadData.movingSettingsData.cflbsratio;
                        var DAOL = (totalCF*convertionRatio)*6;
                        document.getElementById("valueProtectionAOL").value = DAOL;
                    }

                    leadController.moving.estimate.initMovingEstimate();
                },
                calculateFVP:function (returnCalculation) {

                    var fvpToggle = document.getElementById("fvpToggle").checked;
                    var FVPoption = document.querySelector('input[name="FVPoption"]:checked').value;

                    if(fvpToggle == false || FVPoption == "false"){
                        document.getElementById("valueProtectionCharge").value = parseFloat("0.00").toFixed(2);
                        return;
                    }

                    var FVPda = document.querySelector('input[name="FVPda"]:checked').value;

                    var fullvalueprotection = movingLeadData.fullvalueprotection;

                    var levels = fullvalueprotection.levels;
                    var levelPos = levels.indexOf(FVPda);

                    var DAOL = document.getElementById("valueProtectionAOL").value;

                    var chosenRate = null;
                    for(var level in fullvalueprotection.data) {

                        if(chosenRate == null){
                            chosenRate = fullvalueprotection.data[level][levelPos];
                        }

                        chosenRate = fullvalueprotection.data[level][levelPos];

                        if(parseFloat(level) >= parseFloat(DAOL)){
                            break;
                        }
                    }

                    var totalChargesFORFVP = (DAOL*chosenRate)/1000;

                    if(returnCalculation == undefined){
                        // Was not asked to return the calculation - update to total FVP charge
                        document.getElementById("valueProtectionCharge").value = parseFloat(totalChargesFORFVP).toFixed(2);
                    }else{
                        // Was asked to return the calculation - return it
                        return parseFloat(totalChargesFORFVP).toFixed(2);
                    }
                }
            }
        },
        operations:{
            setOperations:function (data) {
                $("#operationTabTotalCarriers").html("");
                $("#operationTabTotalTrucks").html("");
                $("#operationTabTotalCrew").html("");

                if(data.carriersAssigned.length == 0  && data.trucksAssigned.length == 0 && data.crewAssigned.length == 0) {
                    $("#operationTabTotalAssigned").css("display","");
                }else{
                    $("#operationTabTotalAssigned").css("display","none");
                }

                if(data.carriersAssigned.length > 0){
                    $("#operationTabTotalCarriers").html(data.carriersAssigned.length+" "+(data.carriersAssigned.length > 1 ? "carriers" : "carrier"));
                }
                if(data.trucksAssigned.length > 0){
                    $("#operationTabTotalTrucks").html(data.trucksAssigned.length+" "+(data.trucksAssigned.length > 1 ? "trucks" : "truck"));
                }
                if(data.crewAssigned.length > 0){
                    $("#operationTabTotalCrew").html(data.crewAssigned.length+" "+(data.crewAssigned.length > 1 ? "crew" : "crew"));
                }

            },
            assign:function () {

                var iframe = document.createElement("iframe");
                iframe.src = BASE_URL+"/console/categories/iframes/leads/bookJob.php?leadId="+leadId;

                iframe.setAttribute("style","border:none;width: 100%;height: 631px;");
                iframe.setAttribute("name","assign-to-operation-iframe");
                iframe.onload= function() {
                    //window.frames['assign-to-operation-iframe'].init(data);
                };

                swal({
                    content: iframe,
                    buttons:false,
                    className: "swal-operations-assign"
                }).then(function(data) {
                    leadController.lead.getLeadMovingDetails();
                });;
            },
            jobAcceptance:{
                createForm:function () {

                    var doesHaveActiveEstimate = false;
                    for(var i = 0;i<movingLeadData.estimates.length;i++){
                        if(movingLeadData.estimates[i].customerSignature != "" && movingLeadData.estimates[i].customerSignature != null && movingLeadData.estimates[i].isActive == "1"){
                            doesHaveActiveEstimate = true;
                        }
                    }

                    var strUrl = BASE_URL+'/console/actions/moving/carriers/getCarriers.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method: "GET",
                        data: {
                            onlyActives:true
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function (data) {
                        try{
                            var carriers = JSON.parse(data);

                            var content = document.createElement("div");

                            if(carriers.length == 0){
                                var selectLabel = document.createElement("span");
                                selectLabel.setAttribute("style"," text-align: left;float: left;margin: 3px;margin-top: 13px;");
                                selectLabel.innerHTML = "Select carrier:";

                                content.appendChild(selectLabel);

                                var select = document.createElement("select");
                                select.className = "form-control";

                                var option = document.createElement("option");
                                option.innerHTML = "No carriers available";

                                select.appendChild(option);
                                select.setAttribute("disabled",true);

                                var a = document.createElement("a");
                                a.setAttribute("target","_blank");
                                a.setAttribute("style","text-decoration: underline;");
                                a.href = BASE_URL+'/console/categories/moving/home.php';

                                var div = document.createElement("div");
                                div.className = "alert alert-info";
                                div.setAttribute("style","margin-top: 20px;");
                                div.innerHTML = 'Click here to add carriers.';

                                content.appendChild(select);
                                a.appendChild(div);
                                content.appendChild(a);

                                swal({
                                    content: content,
                                    buttons:false
                                });
                            }else{

                                if(doesHaveActiveEstimate != true){
                                    leadController.moving.operations.jobAcceptance.showNoSignedEstimateSwal();
                                }else{
                                    var selectLabel = document.createElement("span");
                                    selectLabel.setAttribute("style"," text-align: left;float: left;margin: 3px;margin-top: 13px;");
                                    selectLabel.innerHTML = "Select carrier:";

                                    content.appendChild(selectLabel);

                                    var select = document.createElement("select");
                                    select.className = "form-control";

                                    for(var i = 0;i<carriers.length;i++) {
                                        var option = document.createElement("option");
                                        option.value = carriers[i].id;
                                        option.innerHTML = carriers[i].name;
                                        option.setAttribute("data-email",carriers[i].email);
                                        option.setAttribute("data-phone",carriers[i].phone1);
                                        option.setAttribute("data-name",carriers[i].name);

                                        select.appendChild(option);
                                    }
                                    content.appendChild(select);


                                    var selectLabel = document.createElement("span");
                                    selectLabel.setAttribute("style"," text-align: left;float: left;margin: 3px;margin-top: 13px;");
                                    selectLabel.innerHTML = "Carrier balance ($):";

                                    content.appendChild(selectLabel);

                                    var input = document.createElement("input");
                                    input.className = "form-control";
                                    input.setAttribute("placeholder","0.00");
                                    input.setAttribute("min","0");
                                    input.type = "tel";

                                    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                                        input.addEventListener(event, function() {
                                            if (returnCurrencyFromString(this.value)) {
                                                this.oldValue = this.value;
                                                this.oldSelectionStart = this.selectionStart;
                                                this.oldSelectionEnd = this.selectionEnd;
                                            } else if (this.hasOwnProperty("oldValue")) {
                                                this.value = this.oldValue;
                                                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                                            } else {
                                                this.value = "";
                                            }
                                        });
                                    });

                                    content.appendChild(input);

                                    swal({
                                        content: content,
                                        buttons:{
                                            confirm:"Create Form"
                                        }
                                    }).then(
                                        function(resp) {
                                            if(resp){

                                                var strUrl = BASE_URL + '/console/actions/moving/leads/jobAcceptance/createForm.php',
                                                    strReturn = "";
                                                jQuery.ajax({
                                                    url: strUrl,
                                                    method: "POST",
                                                    data: {
                                                        leadId: leadId,
                                                        carrierId: select.value,
                                                        carrierBalance: input.value,
                                                        returnFormData:true
                                                    },
                                                    success: function (html) {
                                                        strReturn = html;
                                                    },
                                                    async: true
                                                }).done(function (data) {
                                                    try {
                                                        data = JSON.parse(data);
                                                        if (data != false) {
                                                            var carrierEmail = select.options[select.selectedIndex].getAttribute('data-email');
                                                            var carrierPhone = select.options[select.selectedIndex].getAttribute('data-phone');
                                                            var carrierName = select.options[select.selectedIndex].getAttribute('data-name');

                                                            leadController.moving.operations.jobAcceptance.sendFormLink(carrierEmail,data.id, carrierPhone,carrierName,data.secretKey,data.secretToken);

                                                            leadController.moving.operations.jobAcceptance.getForms();

                                                        } else {
                                                            toastr.success("Could not create job acceptance form", "Error");
                                                        }
                                                    } catch (e) {
                                                        toastr.success("Could not create job acceptance form", "Error");
                                                    }
                                                });
                                            }
                                        }
                                    );


                                }
                            }

                        }catch (e) {
                            toastr.success("Could not create job acceptance form","Error");
                        }
                    });

                },
                showNoSignedEstimateSwal:function(){
                    var content = document.createElement("div");

                    var div = document.createElement("div");
                    div.className = "alert alert-info";
                    div.setAttribute("style","margin-top: 20px;");
                    div.innerHTML = 'This lead must have a signed estimate to do this operation';

                    content.appendChild(div);

                    swal({
                        content: content
                    });
                },
                getForms:function(){
                    var strUrl = BASE_URL+'/console/actions/moving/leads/jobAcceptance/getForms.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method: "GET",
                        data: {
                            leadId: leadId
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function (data) {

                        $("#jobAcceptanceForms").html("");
                        try{
                            data = JSON.parse(data);

                            if(data.length == 0){

                                $("#deleteSelectedJobCarriers").hide();
                                $("#checkAllcarriers").prop("checked", false);

                                var tr = document.createElement("tr");

                                var td = document.createElement("td");
                                td.setAttribute("colspan","6");
                                td.setAttribute("style","text-align: center;");
                                td.innerHTML = "No forms sent yet - <a href='javascript:void(0)' onclick='leadController.moving.operations.jobAcceptance.createForm();'>Create a form</a>";

                                tr.appendChild(td);
                                document.getElementById("jobAcceptanceForms").appendChild(tr);
                            }

                            for(var i = 0;i<data.length;i++){

                                var singleForm = data[i];

                                var tr = document.createElement("tr");

                                var td = document.createElement("td");

                                var checkbox  = document.createElement("input")
                                checkbox.setAttribute('type', 'checkbox');
                                checkbox.setAttribute('value', singleForm.id);
                                checkbox.setAttribute('name', 'carrierCheckbox');
                                checkbox.classList.add("carrierCheckbox");

                                td.appendChild(checkbox);


                                tr.appendChild(td);

                                var td = document.createElement("td");
                                td.setAttribute("style","text-align:left;");

                                td.innerHTML = singleForm.carrierName;

                                tr.appendChild(td);

                                var td = document.createElement("td");
                                td.setAttribute("style","text-align:center;");
                                td.innerHTML = "$"+singleForm.balance;

                                tr.appendChild(td);

                                var td = document.createElement("td");
                                td.setAttribute("style","text-align:center;");

                                if(singleForm.signature == undefined || singleForm.signature == null || singleForm.signature == ""){
                                    td.innerHTML = '<span class="label label-warning" style="font-size: 10px;padding: 2px 6px;">Pending</span>';
                                }else{
                                    td.innerHTML = '<span class="label label-primary" style="font-size: 10px;padding: 2px 6px;">Signed</span>';
                                }
                                //send form link
                                tr.appendChild(td);

                                var td = document.createElement("td");
                                td.setAttribute("style","text-align:center;");

                                var sendBtn = document.createElement("button");
                                sendBtn.id = "";
                                if(singleForm.signature){
                                    sendBtn.className = "btn btn-success btn-xs";
                                    sendBtn.setAttribute("onClick","leadController.moving.operations.jobAcceptance.sendEstimateLink('"+ singleForm.carrierEmail+"',"+singleForm.id +",'" +singleForm.carrierPhone + "','" +singleForm.carrierName + "')");
                                    sendBtn.innerHTML = "Send job details";
                                }else{
                                    sendBtn.className = "btn btn-default btn-xs";
                                    sendBtn.setAttribute("onClick","leadController.moving.operations.jobAcceptance.sendFormLink('"+ singleForm.carrierEmail+"',"+singleForm.id +",'" +singleForm.carrierPhone + "','" +singleForm.carrierName + "','" + singleForm.secretKey + "','" + singleForm.secretToken +"')");
                                    sendBtn.innerHTML = "Send form link";
                                }


                                td.appendChild(sendBtn);

                                tr.appendChild(td);

                                //open form link
                                var td = document.createElement("td");
                                td.setAttribute("style","text-align:center;");
                                var openBtn = document.createElement("button");
                                openBtn.className = "btn btn-default btn-xs";
                                openBtn.id = "";
                                openBtn.setAttribute("onClick","leadController.moving.operations.jobAcceptance.openForm('"+ singleForm.secretKey+"','"+singleForm.secretToken + "')");
                                openBtn.innerHTML = "Open form link";

                                td.appendChild(openBtn);

                                tr.appendChild(td);

                                document.getElementById("jobAcceptanceForms").appendChild(tr);
                            }


                            if(data.length > 0) {
                                // Add the 'create form' button
                                var tr = document.createElement("tr");

                                var td = document.createElement("td");
                                td.setAttribute("colspan", "6");
                                td.setAttribute("style", "text-align: center;");
                                td.innerHTML = "<a href='javascript:void(0)' onclick='leadController.moving.operations.jobAcceptance.createForm();'>Create a form</a>";

                                tr.appendChild(td);
                                document.getElementById("jobAcceptanceForms").appendChild(tr);
                            }
                        }catch (e) {
                            toastr.error("Could not get job acceptance form","Error");
                        }
                    });

                },
                deleteCarrierJob:function(){
                    var carrierJobsId = [];
                    $.each($(".carrierCheckbox:checked"), function(){
                        carrierJobsId.push($(this).val());
                    });
                    swal({
                        title: "Are you sure?",
                        text: "This will permanently delete these jobs forms."  + "\n" + "If you are sure you want to to this please continue.\n",
                        icon: "warning",
                        dangerMode: true,
                        buttons: true,
                    }).then( function (isConfirm) {
                        if (isConfirm) {
                            var strUrl = BASE_URL + '/console/actions/leads/deleteCarrierJobForm.php', strReturn = "";
                            jQuery.ajax({
                                url: strUrl,
                                method: "post",
                                data: {
                                    carrierJobsId: carrierJobsId,
                                    leadId: leadId
                                },
                                success: function (html) {
                                    strReturn = html;
                                },
                                async: true
                            }).done(function (data) {
                                try {
                                    data = JSON.parse(data);
                                    if (data == true) {
                                        if(carrierJobsId.length == 1){
                                            toastr.success("Job acceptance form Deleted", "Deleted");
                                        }else if(carrierJobsId.length > 1){
                                            toastr.success("Job acceptance forms Deleted", "Deleted");
                                        }

                                        $("#myCustomModal").modal('hide');
                                        leadController.moving.operations.jobAcceptance.getForms();
                                        $("#deleteSelectedJobCarriers").hide();
                                    }
                                } catch (e) {
                                    toastr.error("Deleting Jobs Failed");
                                    $("#deleteSelectedJobCarriers").hide();
                                }
                            });
                        }
                    });
                },
                sendFormLink: function (email,jobAcceptanceId,phone,name,secretKey,secretToken) {
                    swal({
                        className: "CarrierSmsOrEmailSwal",
                        text: "Send job acceptance form to " + name + " by",
                        buttons:{
                            cancel:false,
                            SMS:true,
                            Email:true
                        },

                    }).then(function(value) {

                        switch (value) {

                            case "SMS":
                                if(leadData.leadData.jobNumber == null || leadData.leadData.jobNumber == ""){
                                    var jobNumberOrLeadId = leadData.leadData.id;
                                }else{
                                    var jobNumberOrLeadId = leadData.leadData.jobNumber;
                                }
                                var jobLink = leadController.moving.operations.jobAcceptance.setFormLink(secretKey, secretToken);
                                var txt = "Hi " + name + ", please follow this link to view your job acceptance form for job #" + jobNumberOrLeadId+ ". Thanks, " + orgName + "\n\n" + jobLink ;
                                sendSMScontroller.init(phone,undefined,txt);
                                break;

                            case "Email":
                                sendEmailsModal(null,email,[4,leadId,null,jobAcceptanceId]);
                                break;

                            default:
                        }
                    });

                },
                sendEstimateLink: function (email,jobAcceptanceId,phone,name) {
                    swal({
                        className: "CarrierSmsOrEmailSwal",
                        text: "Send job details to " + name + " by",
                        buttons:{
                            cancel:false,
                            SMS:true,
                            Email:true
                        },

                    }).then(function(value) {
                        var link = BASE_YMQ_URL+"/leads.php?key="+leadData.leadData.secretKey+"&page=estimates";

                        switch (value) {
                            case "SMS":
                                var txt = "Hi "+name+", here is your job estimate link: "+link;
                                sendSMScontroller.init(phone,undefined,txt);
                                break;
                            case "Email":
                                sendEmailsModal(null,email,[5,leadId,null,jobAcceptanceId]);
                                break;
                        }
                    });

                },
                openForm:function (secretKey, secretToken) {

                    var jobLink = leadController.moving.operations.jobAcceptance.setFormLink(secretKey, secretToken);

                    window.open(jobLink,"_blank");
                },
                setFormLink:function(secretKey, secretToken){

                    var link =  BASE_YMQ_URL+"/jobAcceptance.php?k=" + secretKey + "&t=" + secretToken;

                    return link;
                },
                getSingleJobByLeadId:function(){

                    var strUrl = BASE_URL + '/console/actions/moving/jobs/getSingleJobInBoardByLeadId.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method: "post",
                        data: {
                            leadId: leadId
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: true
                    }).done(function (data) {
                        try {
                            data = JSON.parse(data);

                            if(data.carrierBalance){
                                swal({
                                    className: "swal-update-carrier-balance",
                                    title: "Are you sure?",
                                    text: "This job is already in the board with a balance rate of $" + data.carrierBalance ,
                                    icon: "warning",
                                    dangerMode: true,
                                    buttons: {
                                        cancel: true,
                                        confirm: "Update balance",
                                    }
                                    }).then(
                                        function (resp) {
                                            if (resp) {
                                                leadController.moving.operations.jobAcceptance.updateJobBoard(data.carrierBalance)
                                            }
                                        }
                                        );

                                    }else{
                                        leadController.moving.operations.jobAcceptance.AddJobToBoard()
                            }
                        } catch (e) {
                            return false;
                        }
                    });

                },
                updateJobBoard:function (oldBalance) {

                    var content = document.createElement("div");

                    var selectLabel = document.createElement("span");
                    selectLabel.setAttribute("style", " text-align: left;float: left;margin: 3px;margin-top: 13px;");
                    selectLabel.innerHTML = "Carrier balance ($):";

                    content.appendChild(selectLabel);

                    var input = document.createElement("input");
                    input.className = "form-control";
                    input.setAttribute("placeholder", "0.00");
                    input.setAttribute("min","0");
                    input.type = "tel";
                    input.value = oldBalance;


                    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                        input.addEventListener(event, function() {
                            if (returnCurrencyFromString(this.value)) {
                                this.oldValue = this.value;
                                this.oldSelectionStart = this.selectionStart;
                                this.oldSelectionEnd = this.selectionEnd;
                            } else if (this.hasOwnProperty("oldValue")) {
                                this.value = this.oldValue;
                                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                            } else {
                                this.value = "";
                            }
                        });
                    });



                    content.appendChild(input);

                    swal({
                        content: content,
                        buttons: {
                            confirm: "Update job balance"
                        }
                    }).then(
                        function (resp) {
                            if (resp) {

                                var strUrl = BASE_URL+"/console/actions/moving/jobs/updateCarrierBalanceJob.php";
                                jQuery.ajax({
                                    url: strUrl,
                                    method: "POST",
                                    data: {
                                        leadId: leadId,
                                        carrierBalance:input.value
                                    },
                                    async: true
                                }).done(function (data) {
                                    try{
                                        data = JSON.parse(data);
                                        if(data.status == false && data.reason != ""){
                                            parent.swal("Oops", data.reason , "error");
                                        }else{

                                            swal({
                                                title: "Job balance updated",
                                                buttons: {
                                                    confirm: "Open job board",
                                                    ok: "OK"
                                                },
                                                icon:"success"
                                            }).then(
                                                function (resp) {
                                                    if (resp == true) {
                                                        window.open(BASE_URL+"/console/categories/leads/jobBoard.php","_blank");
                                                    }
                                                });

                                        }
                                    }catch (e) {
                                        parent.swal("Oops", "Couldn't update job balance, please try again later." , "error");
                                    }
                                });

                            }
                        }
                    );
                },
                AddJobToBoard:function () {


                    var doesHaveActiveEstimate = false;
                    for (var i = 0; i < movingLeadData.estimates.length; i++) {
                        if (movingLeadData.estimates[i].customerSignature != "" && movingLeadData.estimates[i].customerSignature != null && movingLeadData.estimates[i].isActive == "1") {
                            doesHaveActiveEstimate = true;
                        }
                    }
                    if (!doesHaveActiveEstimate) {
                        leadController.moving.operations.jobAcceptance.showNoSignedEstimateSwal();
                        return;
                    }

                    var content = document.createElement("div");

                    var selectLabel = document.createElement("span");
                    selectLabel.setAttribute("style", " text-align: left;float: left;margin: 3px;margin-top: 13px;");
                    selectLabel.innerHTML = "Carrier balance ($):";

                    content.appendChild(selectLabel);

                    var input = document.createElement("input");
                    input.className = "form-control";
                    input.setAttribute("placeholder", "0.00");
                    input.setAttribute("min","0");
                    input.type = "tel";

                        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                            input.addEventListener(event, function() {
                                if (returnCurrencyFromString(this.value)) {
                                    this.oldValue = this.value;
                                    this.oldSelectionStart = this.selectionStart;
                                    this.oldSelectionEnd = this.selectionEnd;
                                } else if (this.hasOwnProperty("oldValue")) {
                                    this.value = this.oldValue;
                                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                                } else {
                                    this.value = "";
                                }
                            });
                        });



                    content.appendChild(input);

                    swal({
                        content: content,
                        buttons: {
                            confirm: "Add job to board"
                        }
                    }).then(
                        function (resp) {
                            if (resp) {

                                var strUrl = BASE_URL+"/console/actions/moving/jobs/addToJobBoard.php";
                                jQuery.ajax({
                                    url: strUrl,
                                    method: "POST",
                                    data: {
                                        leadId: leadId,
                                        carrierBalance:input.value
                                    },
                                    async: true
                                }).done(function (data) {
                                    try{
                                        data = JSON.parse(data);
                                        if(data.status == false && data.reason != ""){
                                            parent.swal("Oops", data.reason , "error");
                                        }else{

                                            swal({
                                                title: "Job added to board",
                                                buttons: {
                                                    confirm: "Open job board",
                                                    ok: "OK"
                                                },
                                                icon:"success"
                                            }).then(
                                                function (resp) {
                                                    if (resp == true) {
                                                        window.open(BASE_URL+"/console/categories/leads/jobBoard.php","_blank");
                                                    }
                                                });

                                        }
                                    }catch (e) {
                                        parent.swal("Oops", "Couldn't add job to board, please try again later." , "error");
                                    }
                                });

                            }
                        }
                    );
                }
            }
        }
    },
    autoTransport: {
        get:function () {
            var arr = [];

            for (var i = 1; i <= countVehicles; i++) {
                if (document.getElementById("vehicleformGroup" + i) !== null && document.getElementById("make"+i).value !== "" && document.getElementById("model"+i).value !== "" && document.getElementById("year"+i).value !== "") {
                    var Obj = {
                        "make":document.getElementById("make"+i).value,
                        "model":document.getElementById("model"+i).value,
                        "year":document.getElementById("year"+i).value,
                        "runs":document.getElementById("runs"+i).value
                    };
                    arr.push(Obj);
                }
            }
            return arr;
        },
        addVehicle:function () {
            countVehicles++;

            var vehiclesDiv = document.getElementById('autoTransport');

            var divGroup = document.createElement("div");
            divGroup.className = "form-group";
            divGroup.id = "vehicleformGroup"+countVehicles;
            divGroup.setAttribute("style","margin-bottom: 3px;");

            var make = document.createElement("select");
            make.className = "form-control";
            make.name = "make[]";
            make.id = "make"+countVehicles;
            make.setAttribute("onChange","leadController.autoTransport.updateModel('"+countVehicles+"',this.value);");
            make.setAttribute("style","width: 30% !important;display: inline !important;");
            make.innerHTML = '<option value="">Make</option><option value="">-----------------</option><option value="Acura">Acura</option><option value="Alfa Romeo">Alfa Romeo</option><option value="AM General">AM General</option><option value="AMC">AMC</option><option value="Aptera">Aptera</option><option value="Aston Martin">Aston Martin</option><option value="Audi">Audi</option><option value="Austin">Austin</option><option value="Bentley">Bentley</option><option value="BMW">BMW</option><option value="BMW Alpina">BMW Alpina</option><option value="Bugatti">Bugatti</option><option value="Buick">Buick</option><option value="Cadillac">Cadillac</option><option value="Checker">Checker</option><option value="Chevrolet">Chevrolet</option><option value="Chrysler">Chrysler</option><option value="Citroën">Citroën</option><option value="Corbin">Corbin</option><option value="Daewoo">Daewoo</option><option value="Daihatsu">Daihatsu</option><option value="Datsun">Datsun</option><option value="DeLorean">DeLorean</option><option value="Dodge">Dodge</option><option value="Eagle">Eagle</option><option value="Fairthorpe">Fairthorpe</option><option value="Ferrari">Ferrari</option><option value="Fiat">Fiat</option><option value="Fillmore">Fillmore</option><option value="Foose">Foose</option><option value="Ford">Ford</option><option value="Freightliner">Freightliner</option><option value="Geo">Geo</option><option value="GM Electric Vehicle">GM Electric Vehicle</option><option value="GMC">GMC</option><option value="Hillman">Hillman</option><option value="Holden">Holden</option><option value="Honda">Honda</option><option value="Hudson">Hudson</option><option value="Hummer">Hummer</option><option value="Hyundai">Hyundai</option><option value="Infiniti">Infiniti</option><option value="International">International</option><option value="Isuzu">Isuzu</option><option value="Jaguar">Jaguar</option><option value="Jeep">Jeep</option><option value="Jensen">Jensen</option><option value="Kia">Kia</option><option value="Lamborghini">Lamborghini</option><option value="Land Rover">Land Rover</option><option value="Lexus">Lexus</option><option value="Lincoln">Lincoln</option><option value="Lotus">Lotus</option><option value="Maserati">Maserati</option><option value="Maybach">Maybach</option><option value="Mazda">Mazda</option><option value="McLaren">McLaren</option><option value="Mercedes-Benz">Mercedes-Benz</option><option value="Mercury">Mercury</option><option value="Merkur">Merkur</option><option value="MG">MG</option><option value="MINI">MINI</option><option value="Mitsubishi">Mitsubishi</option><option value="Morgan">Morgan</option><option value="Nash">Nash</option><option value="Nissan">Nissan</option><option value="Oldsmobile">Oldsmobile</option><option value="Opel">Opel</option><option value="Panoz">Panoz</option><option value="Peugeot">Peugeot</option><option value="Plymouth">Plymouth</option><option value="Pontiac">Pontiac</option><option value="Porsche">Porsche</option><option value="Ram">Ram</option><option value="Rambler">Rambler</option><option value="Renault">Renault</option><option value="Rolls-Royce">Rolls-Royce</option><option value="Saab">Saab</option><option value="Saturn">Saturn</option><option value="Scion">Scion</option><option value="Shelby">Shelby</option><option value="Smart">Smart</option><option value="Spyker">Spyker</option><option value="Spyker Cars">Spyker Cars</option><option value="Sterling">Sterling</option><option value="Studebaker">Studebaker</option><option value="Subaru">Subaru</option><option value="Suzuki">Suzuki</option><option value="Tesla">Tesla</option><option value="Toyota">Toyota</option><option value="Triumph">Triumph</option><option value="Volkswagen">Volkswagen</option><option value="Volvo">Volvo</option><option value="Yugo">Yugo</option>';

            var model = document.createElement("select");
            model.className = "form-control";
            model.name = "model[]";
            model.id = "model"+countVehicles;
            model.setAttribute("style","width: 27% !important;display: inline !important;margin-left: 2px;");
            model.innerHTML = '<option value="">Model</option>';

            var year = document.createElement("select");
            year.className = "form-control";
            year.name = "year[]";
            year.id = "year"+countVehicles;
            year.setAttribute("style","width: 18% !important;display: inline !important;margin-left: 2px;");
            year.innerHTML = '<option value="">Year</option><option value="">----</option><option value="1958">1958</option><option value="1959">1959</option><option value="1960">1960</option><option value="1961">1961</option><option value="1962">1962</option><option value="1963">1963</option><option value="1964">1964</option><option value="1965">1965</option><option value="1966">1966</option><option value="1967">1967</option><option value="1968">1968</option><option value="1969">1969</option><option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017" selected="">2017</option><option value="2018" selected="">2018</option><option value="2019" selected="">2019</option>';

            var runs = document.createElement("select");
            runs.className = "form-control";
            runs.name = "runs[]";
            runs.id = "runs"+countVehicles;
            runs.setAttribute("style","width: 20% !important;display: inline !important;margin-left: 2px;");
            runs.innerHTML = '<option value="1">Running</option><option value="0">Not Running</option>';

            var deleteIt = document.createElement("span");
            deleteIt.setAttribute("style","margin-left: 2px;");
            deleteIt.innerHTML = '<a href="javascript:void(0);leadController.autoTransport.removeVehicle('+countVehicles+');"><i class="fa fa-trash fa-lg"></i></a>';

            divGroup.appendChild(make);
            divGroup.appendChild(model);
            divGroup.appendChild(year);
            divGroup.appendChild(runs);
            divGroup.appendChild(deleteIt);

            vehiclesDiv.appendChild(divGroup);
        },
        updateModel:function (vehicleNumber,data) {
            var models = vehicles[data];
            var optionsForMake = '<option value="">Model</option><option value="">----</option>';
            for(var i = 0;i<models.length;i++){
                optionsForMake += '<option value="'+models[i]+'">'+models[i]+'</option>';
            }

            document.getElementById('model'+vehicleNumber).innerHTML = optionsForMake;
        },
        removeVehicle:function (vehicleNumber) {

            var vehicleformGroup = document.getElementById('vehicleformGroup'+vehicleNumber);
            vehicleformGroup.parentNode.removeChild(vehicleformGroup);
        }
    }
};

// Google Map Vars
var map, origin = null, destination = null, pickupBounds, routeBounds, destinationBounds, directionsService = new google.maps.DirectionsService, directionsDisplay;

// Inventory listener
$("#inventoryGroup").on("change",function(){
    $(".items").each(function(){
        this.style.display = "none";
    });
    document.getElementById("item-"+document.getElementById("inventoryGroup").value).style.display = "";
    leadController.moving.inventory.searchItem(document.getElementById("itemSearch"));
});

// Upload files listener
$('#uploadFile').on('change', leadController.files.prepareUpload);

// estimate change listener
$( ".estimateChangable" ).change(function() {
    // Every time an input in the estimate form changes - update the estimate
    leadController.moving.estimate.initMovingEstimate();
});
// estimate CF listener
$("#estimateCForLBS").on("change",function () {
    setPricePer();
});

// something to do with the inventory
$(document).mouseup(function(e)
{
    var container = $("#updateInventoryEstimateBTNDiv");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        leadController.moving.inventory.askToUpdateEstimateInventory(false);
    }
});

// GENERAL LISTENERS
$("#saveLeftSide").on("click",function(){
    estimateLadda = $("#saveLeftSide").ladda();
    estimateLadda.ladda("start");
    leadController.lead.updateLeadNotes();
    if ($(this).text() == "Save"){
        return true;
    } else{

        var estimateData = {
            'departmentId' : document.getElementById("department").value,
            'userHandeling' : document.getElementById("userHandelingSelect").value,
            'priority' : document.getElementById("priority").value,
            'reference' : document.getElementById("reference").value,
            'boxDeliveryDate' : document.getElementById("boxDate").innerText,
            'pickupDate' : document.getElementById("pickupDate").innerText,
            'requestedDeliveryDateStart' : document.getElementById("requestedDeliveryDateStart").innerText,
            'includeBoxDeliveryDate' : document.getElementById("includeBoxDeliveryDate").checked,
            'includePickupDate' : document.getElementById("includePickupDate").checked,
            'includeRequestDeliveryDate' : document.getElementById("includeRequestDeliveryDate").checked
        };

        // UPDATE ESTIMATE DATA
        var strUrl = BASE_URL+'/console/actions/moving/leads/updateEstimateDetails.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            method:"post",
            data:{
                id:leadId,
                estimateData: estimateData
            },
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data == true){
                    estimateLadda.ladda("stop");
                    $("#saveLeftSide").text("Saved");
                    $("#saveLeftSide").attr("disabled",true);
                    setTimeout(function(){
                        $("#saveLeftSide").text("Save Estimate Details");
                        $("#saveLeftSide").attr("disabled",false);
                    },1500);
                    if (document.getElementById("includeBoxDeliveryDate").checked == true){
                        $("#boxDatePicker").removeClass("disabled-picker");
                    }else{
                        $("#boxDatePicker").addClass("disabled-picker");
                    }
                    if (document.getElementById("includePickupDate").checked == true){
                        $("#pickupDatePicker").removeClass("disabled-picker");
                    }else{
                        $("#pickupDatePicker").addClass("disabled-picker");
                    }
                    if (document.getElementById("includeRequestDeliveryDate").checked == true){
                        $("#requestedDeliveryDateStartPicker").removeClass("disabled-picker");
                    }else{
                        $("#requestedDeliveryDateStartPicker").addClass("disabled-picker");
                    }

                    leadController.lead.getLeadDetails();
                }else{
                    estimateLadda.ladda("stop");
                    return false;
                }
            }catch (e) {
                estimateLadda.ladda("stop");
                return false;
            }
        });

    }

});

$("#leadEditBtn").on('click',function(){
    leadLadda.ladda("start");
    var leadData = {
        "firstname":document.getElementById("leadEditFirstname").value,
        "lastname":document.getElementById("leadEditLastname").value,
        "email":document.getElementById("leadEditEmail").value,
        "phone":document.getElementById("leadEditPhone").value,
        "phone2":document.getElementById("leadEditPhone2").value,
        "comments":document.getElementById("leadEditComments").value
    };

    leadController.lead.updateLeadData(leadData);

});

$("#moveEditBtn").on('click',function(){
    movingLadda.ladda("start");
    var moveDate = document.getElementById("moveEditDate").innerText;
    moveDate = moment(moveDate).format("YYYY/MM/DD");

    var autoTransport = leadController.autoTransport.get();
    if (autoTransport == undefined || autoTransport.length == 0){
        autoTransport = "";
    }

    if(document.getElementById("needStorage").checked === true){
        var needStorage = 1;
        $('#needStorageEstimate').css('display','');
    }else{

        var needStorage = 0;
        $('#needStorageEstimate').css('display','none');
    }

    var movingData = {
        "fromZip":document.getElementById("moveEditFromZip").value,
        "toZip":document.getElementById("moveEditToZip").value,
        "fromState":document.getElementById("moveEditFromState").value,
        "toState":document.getElementById("moveEditToState").value,
        "fromCity":document.getElementById("moveEditFromCity").value,
        "toCity":document.getElementById("moveEditToCity").value,
        "fromAddress":document.getElementById("moveEditFromAddress").value,
        "toAddress":document.getElementById("moveEditToAddress").value,
        "moveDate":moveDate,
        "moveSizeNumber":document.getElementById("moveEditSizeNumber").value,
        "moveSize":document.getElementById("moveEditSize").value,
        "fromLevel":document.getElementById("moveEditFromLevel").value,
        "fromFloor":document.getElementById("moveEditFromFloor").value,
        "fromAptNumber":document.getElementById("moveEditFromApartment").value,
        "toAptNumber":document.getElementById("moveEditToApartment").value,
        "fromAptType":document.getElementById("moveEditFromApartmentType").value,
        "toAptType":document.getElementById("moveEditToApartmentType").value,
        "toLevel":document.getElementById("moveEditToLevel").value,
        "toFloor":document.getElementById("moveEditToFloor").value,
        "typeOfMove":document.getElementById("moveEditType").value,
        "estimator":document.getElementById("onSiteEstimatorEdit").value,
        "bindingType":document.getElementById("binding").value,
        "needStorage":needStorage,
        "autoTransport":autoTransport
    };

    // UPDATE MOVING LEAD DATA
    var strUrl = BASE_URL+'/console/actions/moving/leads/updateDetails.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method:"post",
        data:{
            leadId:leadId,
            movingData: movingData
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            if (data == true){
                movingLadda.ladda("stop");

                toastr.success("Lead moving details updated","Saved");



                leadController.lead.getLeadMovingDetails();

                $("#moveDetailsEdit").hide();
                $("#moveDetails").show();

            }else{
                movingLadda.ladda("stop");
                toastr.error("Lead Moving Details Not Updated","Error");
                return false;
            }
        }catch (e) {
            movingLadda.ladda("stop");
            toastr.error("Lead Moving Details Not Updated","Error");
            return false;
        }
    });

});


// General Vars
var leadData = null;
var movingLeadData = null;

// Initialize
var movingLadda;
var leadLadda;
var estimateLadda;
var countVehicles = 0;
var items = [];
var totalCF = 0;
var allInventoryItems = [];
var currentEstimate = null; // The current estimate view (the 'i' of estimate in the estimates array)

function initLeadPage() {
    leadController.lead.getLeadDetails();
    leadController.lead.getLeadMovingDetails();
    leadController.lead.getLeadLogs();
    leadController.contact.getLeadEmailsData();
    leadController.files.get();
    leadController.lead.claims.getClaims();
}

$(document).ready(function(){

    l = $('.sendEmailLaddaButton').ladda();

    leadLadda = $("#leadEditBtn").ladda();
    movingLadda = $("#moveEditBtn").ladda();


    /*
    $('#paymentDatePicker #paymentDate').html(moment().format('MMMM D, YYYY'));

    $('#paymentDatePicker').daterangepicker({
        singleDatePicker: true,
        format: 'MM/DD/YYYY',
        startDate: moment(),
        endDate: moment(),
        dateLimit: {days: 365},
        showDropdowns: true,
        showWeekNumbers: false,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Go',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function (start, end, label) {
        $('#paymentDatePicker #paymentDate').html(end.format('MMMM D, YYYY'));
    });

    $("#paymentDate").on("DOMSubtreeModified", function () {

    });
     */
});

function getActiveEvents(){
    // This function is so it won't retur an error when in the lead page and trying to delete a reminder
    // The reminder modal is opening in the lead page and in the calendar page so in the calendar this function exists but not in the lead page
    return true;
}
function isaphonenumber(inputtxt) {
    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if(inputtxt.match(phoneno)) {
        return true;
    } else {
        return false;
    }
}
function htmlEscape(s) {
    return (s + '').replace(/&/g, '&')
        .replace(/</g, '<')
        .replace(/>/g, '>')
        .replace(/'/g, "'")
        .replace(/"/g, '"');
}
function decodeText(text){
    try {
        var elem = document.createElement('textarea');
        elem.innerHTML = text;
        var decoded = elem.value;
        return decoded;
    }catch (e) {return text;}
}
function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].id === nameKey) {
            return i;
        }
    }
}
function setTableItem(item){

    if(document.getElementById("amount-"+item.id) == null) {
        leadController.moving.inventory.setInventory({"id": item.id, "name": item.title, "cf": item.cf, "amount": 1});
        leadController.moving.inventory.calcCF();
    }else{
        document.getElementById("amount-"+item.id).value = parseInt(document.getElementById("amount-"+item.id).value) + 1;
        leadController.moving.inventory.calcCF();
    }
}
function downloadPDF(){
    // This is not is use anymore
    //window.open(BASE_URL+"/console/categories/leads/actions/downloadPDF.php?leadId="+leadId);
}

function updateClientInfo(type,val,elementToUpdate){

    if(type == 4){
        // set storage
        if(val == "1"){
            document.getElementById(elementToUpdate).checked = true;
        }else{
            document.getElementById(elementToUpdate).checked = false;
        }
        $("#saveLeftSide").click();

    }else {
        if (type == 3) {
            // set move Date
            document.getElementById(elementToUpdate).innerText = val;
            $("#moveEditBtn").click();
        } else {
            document.getElementById(elementToUpdate).value = val;
            if (type == 1) {
                // lead Data
                $("#leadEditBtn").click();
            } else if (type == 2) {
                // moving Data
                $("#moveEditBtn").click();
            }
        }
    }
}
function iframeLoaded() {
    var iFrameID = document.getElementById('previewContent');
    if(iFrameID) {
        // here you can make the height, I delete it first, then I make it again
        iFrameID.height = "";
        iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
    }
}
function setPricePer(){
    var value = $("#estimateCForLBS").val();
    switch (value) {
        case "0":
            $("#estimateCForLBS2").text("$ per LBS");
            $("#perCFdiv").css("display","inline-table");
            break;
        case "1":
            $("#estimateCForLBS2").text("$ per cf");
            $("#perCFdiv").css("display","inline-table");
            break;
        case "2":
            $("#estimateCForLBS2").text("$ per hour");
            $("#perCFdiv").css("display","inline-table");
            break;
        case "3":
            $("#estimateCForLBS2").text("$");
            $("#perCFdiv").css("display","none");
            break;
    }
}

leadController.tags.reload();
initLeadPage();

function returnCurrencyFromString(value) {
    return /^\d*[.,]?\d{0,2}$/.test(value);
}