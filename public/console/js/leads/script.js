var currentPage = 0;
var leadsData = [];
var theStartDate = moment();
var theEndDate = moment();
var selectedLeads = [];
var leadsFilters = [];

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.getElementById("topBTN").style.display = "block";
    } else {
        document.getElementById("topBTN").style.display = "none";
    }
}

function topFunction() {
    $('html, body').animate({scrollTop:0}, '300');
}

$(document).ready(function() {

    $('#reportrange span').html("Today");

    $('#reportrange').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment(),
        endDate: moment(),
        minDate: '01/01/2018',
        maxDate: '12/31/2020',
        dateLimit: { days: 365 },
        showDropdowns: true,
        showWeekNumbers: false,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Go',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function(start, end, label) {
        if (label != "Custom") {
            $('#reportrange span').html(label);
        }else{
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        if (label != "Last 7 Days" && label != "Last 30 Days" && label != "This Month" && label != "Last Month" && label != "Custom"){
            $("#backDay").attr("disabled",false);
            $("#nextDay").attr("disabled",false);
        }else{
            $("#backDay").attr("disabled",true);
            $("#nextDay").attr("disabled",true);
        }

        currentPage = 0;
        theStartDate = start;
        theEndDate = end;
        getLeads();
        hideLeadsActionIcons();

    });
    getLeads();

    $( "#backDay" ).click(function() {
        moveDay('-');
    });
    $( "#nextDay" ).click(function() {
        moveDay('+');
    });

});

function moveDay(action){
    if (action === "+"){
            $("#nextDay").attr("disabled",false);
            theStartDate = moment(theStartDate).add(1, "days");
            theEndDate = moment(theEndDate).add(1, "days");
    }else if (action === "-"){
        theStartDate = moment(theStartDate).subtract(1,"days");
        theEndDate = moment(theEndDate).subtract(1,"days");
    }



    currentPage = 0;
    if (moment().format("YYYY-MM-DD") == theStartDate.format("YYYY-MM-DD")) {
        $('#reportrange span').html("Today");
    }else if (moment().subtract(1,'days').format("YYYY-MM-DD") == theStartDate.format("YYYY-MM-DD")) {
        $('#reportrange span').html("Yesterday");
    }else if (moment().add(1,'days').format("YYYY-MM-DD") == theStartDate.format("YYYY-MM-DD")) {
        $('#reportrange span').html("Tomorrow");
    }else{
        $('#reportrange span').html(moment(theStartDate).format("YYYY-MM-DD"));
    }
    $('#reportrange').data('daterangepicker').setStartDate(moment(theStartDate));
    $('#reportrange').data('daterangepicker').setEndDate(moment(theEndDate));
    getLeads();
}

function getLeads(){

    // Set the 'get leads by' cookie
    getLeadsByCookie.setCookie($("#getLeadsBy").val());

    var strUrl = BASE_URL+'/console/actions/leads/getLeads.php', strReturn = "";
    loadingState();
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            page: currentPage,
            startDate:theStartDate.format('YYYY-MM-DD'),
            endDate:theEndDate.format('YYYY-MM-DD'),
            getLeadsBy:$("#getLeadsBy").val()
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (requestData) {
        stopLoadingState();

        // reset the selected leads
        selectedLeads = [];

        try {
            var data = JSON.parse(requestData);
            leadsData = data;

            setLeadsTable();
        }catch (e) {
            swal("Oops", "Please try again later", "error");
            Sentry.withScope(function(scope) {
                Sentry.setExtra("data", requestData);
                Sentry.setExtra("comments", "getLeads()");
                Sentry.captureException(e);
            });
        }
    });
}

function setLeadsTable(){

    if($('.dataTable').length){
        var table = $('.dataTables-leads').DataTable();
        table.destroy();
    }

    leadsTdsController.createLeadsTableHeader();
    
    document.getElementById("leadsTableHead").innerHTML = "";

    var totalLeads = 0;
    var filteredLeads = filterLeads();
    for(var i = 0;i<filteredLeads.length;i++){
        leadsTdsController.createLeadsTableRow(filteredLeads[i]);
        totalLeads++;
    }

    if(!leadsData.data || totalLeads == 0){
        setNoLeadsRow();
    }else {
        startTableSort();
    }
    $("#totalLeads").text(totalLeads);
}

function filterLeads(){
    var leads = [];
    if(leadsData.data){
        if (leadsData.data.length > 0) {
            for (var i = 0; i < leadsData.data.length; i++) {
                var checkFilter = doesLeadPassFilter(leadsData.data[i]);
                if(checkFilter == true){

                    var ifFilterIsStatus = false;
                    for(var d = 0; d < leadsFilters.length; d++){
                        if(leadsFilters[d].type == "status"){
                            ifFilterIsStatus = true;
                            break;
                        }
                    }
                    if (ifFilterIsStatus && leadsData.data[i].isBadLead == "1"){continue;}

                    leads.push(leadsData.data[i]);
                }
            }
        }
    }

    return leads;
}

function changePage(action){
    switch (action) {
        case "+":
            currentPage++;
            break;
        case "-":
            if (currentPage >= 1){
                currentPage--;
            }
            break;
    }
    getLeads();
}


function setNoLeadsRow(){
    var container = document.getElementById("leadsTableHead");

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    td.colSpan = "18";
    td.style.textAlign = "center";
    td.innerHTML = "No Leads To Show";
    td.style.backgroundColor = "#ffffff";

    tr.appendChild(td);
    container.appendChild(tr);
}

function showDuplicatePhone(id){
    showCustomModal("categories/iframes/leads/showDuplicate.php?value="+id+"&type=phone");
}

function showDuplicateEmail(id){
    showCustomModal("categories/iframes/leads/showDuplicate.php?value="+id+"&type=email");
}

function assignUser(leadId,userId){
    var strUrl = BASE_URL+'/console/actions/leads/assignUserToLead.php', strReturn = "";

    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            leadId:leadId,
            userId:userId
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        toastr.options = {
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true
        };
        try {
            data = JSON.parse(data);
            if (data == true){
                toastr.success("User assigned","Assigned");
            } else{
                toastr.error("Error","Error Assigning user");
            }
        }catch (e) {
            toastr.error("Error","Error Assigning user");
        }
    });
}

// ================================ FILTERS ==============================
function getLeadsByFilter(filterType,obj,text){

    if(obj.value == ""){
        obj.classList.remove("blueBorder");
        obj.classList.add("orangeBorder");
    }else{
        obj.classList.remove("orangeBorder");
        obj.classList.add("blueBorder");
    }

    currentPage = 0;

    for(var i = 0;i<leadsFilters.length;i++) {
        if(leadsFilters[i].type == filterType){
            leadsFilters.splice(i, 1);
        }
    }

    var filterData = [];
    filterData["type"] = filterType;
    filterData["value"] = obj.value;
    filterData["text"] = text;
    leadsFilters.push(filterData);

    if(obj.value == ""){
        removeFilter(filterType,obj.value);
    }

    setFilterLabel();
    setLeadsTable();
}

function setFilterLabel(){

    var container = document.getElementById("selectedFilters");
    container.innerHTML = "";

    for(var i = 0;i<leadsFilters.length;i++) {
        var name = leadsFilters[i].type;
        var value = leadsFilters[i].value;
        var additional = leadsFilters[i].text;

        var span = document.createElement("span");
        span.classList.add("tag");
        span.classList.add("label");
        span.classList.add("label-primary");
        span.id = "label-" + name;

        var spanText = document.createTextNode(additional);
        span.appendChild(spanText);

        var secondSpan = document.createElement("span");

        var closeI = document.createElement("i");
        closeI.classList.add("fa");
        closeI.classList.add("fa-remove");
        closeI.style.cursor = "pointer";
        closeI.setAttribute("onclick", "removeFilter('" + name + "','" + value + "')");

        secondSpan.appendChild(closeI);

        span.appendChild(secondSpan);
        container.appendChild(span);
    }

    if (leadsFilters.length > 0) {
        document.getElementById("selectedFilters").style.display = '';
    }else{
        document.getElementById("selectedFilters").style.display = 'none';
    }
}

function doesLeadPassFilter(leadData){
    // check if this lead can be shown based on selected filters
    var isValid = true;
    for(var i = 0;i<leadsFilters.length;i++){
        var singleFilter = leadsFilters[i];

        // STATUS
        if(singleFilter.type == "status"){
            if(leadData.status != singleFilter.value){
                isValid = false;
            }
        }

        // HOT LEAD
        if(singleFilter.type == "vip"){
            if(leadData.isVIP != singleFilter.value){
                isValid = false;
            }
        }

        // PROVIDER
        if(singleFilter.type == "provider"){

            if(singleFilter.value == "manual"){
                // if manual provider
                if(leadData.providerId != null){
                    isValid = false;
                }
            }else{
                // if regular provider
                if(leadData.providerId != singleFilter.value){
                    isValid = false;
                }
            }
        }

        // TAGS
        if(singleFilter.type == "tags"){
            var leadTags = leadData.tagsData;
            var doesHaveTag = false;
            for(var j = 0;j<leadTags.length;j++){
                if(leadTags[j].id == singleFilter.value){
                    doesHaveTag = true;
                }
            }
            if(!doesHaveTag){
                isValid = false;
            }
        }

        // USER
        if(singleFilter.type == "user"){

            if(singleFilter.value == "null"){singleFilter.value = null;}

            if(leadData.userIdAssigned != singleFilter.value){
                isValid = false;
            }
        }

        // PRIORITY
        if(singleFilter.type == "priority"){
            if(leadData.priority != singleFilter.value){
                isValid = false;
            }
        }
    }

    return isValid;
}

function removeFilter(type,value){

    if(value == "null"){value = null;}

    document.getElementById('filterSelected_'+type).value = "";
    document.getElementById('filterSelected_'+type).classList.remove("blueBorder");
    document.getElementById('filterSelected_'+type).classList.add("orangeBorder");


    for(var i = 0;i<leadsFilters.length;i++) {
        if(leadsFilters[i].type == type && leadsFilters[i].value == value){
            leadsFilters.splice(i, 1);
        }
    }

    setFilterLabel();
    setLeadsTable();
}
// ================================ End FILTERS ==============================

// ================================ START TOGGLE LEADS ROWS ==============================
function toggleLeadRow(leadId,forceState){

    if(forceState != undefined){
        // Force state of toggle
        // forceState = 1 (select row)
        // forceState = 0 (un-select row)

        if(forceState == 1){
            if(!selectedLeads.includes(leadId)){
                selectedLeads.push(leadId);
            }
        }else{
            if(selectedLeads.includes(leadId)){
                selectedLeads.remove(leadId);
            }
        }
    }else{
        // Regular toggle
        if(selectedLeads.includes(leadId)){
            selectedLeads.remove(leadId);
        }else{
            selectedLeads.push(leadId);
        }
    }

    toggleRowSelection(leadId,forceState);


    if (selectedLeads.length > 0){
        showLeadsActionIcons();
    }else{
        hideLeadsActionIcons();
    }
}

function deleteSelectedLeads() {
    var actionFailed = false;
    swal({
        title: "Are you sure?",
        text: "This will permanently delete these leads from your system. If you are sure you want to to this please continue.",
        icon: "warning",
        dangerMode: true,
        buttons: {
            ok: {
                text: "Delete",
                className: "btn-danger"
            },
            cancel: "Cancel"
        },
    }).then(
        function (isConfirm) {
            if (isConfirm == "ok"){
                $(".swal-modal .swal-footer .btn-danger").text("Wait, Removing");
                $(".swal-modal .swal-footer .btn-danger").attr("disabled, disabled");
                for (var i=0;i<selectedLeads.length;i++){
                    var strUrl = BASE_URL+'/console/actions/leads/deleteLead.php', strReturn = "";
                    jQuery.ajax({
                        url: strUrl,
                        method:"post",
                        data:{
                            leadId:selectedLeads[i]
                        },
                        success: function (html) {
                            strReturn = html;
                        },
                        async: false
                    }).done(function (data) {
                        try {
                            data = JSON.parse(data);
                            if (data != true){
                                actionFailed = true;
                            }
                        }catch (e) {
                            actionFailed = true;
                        }
                    });
                }
                if (actionFailed){
                    toastr.error("Removing some leads failed, try again later","Fail");
                    getLeads();
                    hideLeadsActionIcons();
                }else{
                    toastr.success("Leads successfully removed","Removed");
                    getLeads();
                   hideLeadsActionIcons();
                }
            }
        }

    );
}

function sendEmailToLead(){
    if (selectedLeads.length > 150){
        swal("Bulk email sending is limited to 150 emails simultaneously.");
    } else{
        sendEmailsModal(selectedLeads);
    }
}

function sendSMSToLead(){
    if (selectedLeads.length > 300){
        swal("Bulk sms sending is limited to 300 messages simultaneously.");
    } else{
        sendSMScontroller.init(undefined,selectedLeads);
    }
}
function openLeads(leads){
    for(var i = 0; i < leads.length; i++){
        window.open(
            BASE_URL+"/console/categories/leads/lead.php?leadId="+leads[i], '_blank');
    }
}
function checkLeadsCountBeforeOpen() {
    if( selectedLeads.length > 10){
        swal({
            title: "Are you sure?",
            text: "You are about to open " + selectedLeads.length + " tabs. Do you wish to continue?",
            icon: "warning",
            dangerMode: true,
            buttons: {
                ok: {
                    text: "Open",
                    className: "btn-success"
                },
                cancel: "Cancel"
            },
        }).then(
            function (isConfirm) {
                if (isConfirm == "ok"){
                    var openTheLeads = [];
                    for(var i = 0; i < selectedLeads.length; i++){
                        openTheLeads.push(selectedLeads[i]);
                    }
                    openLeads(openTheLeads);
                }
            }
        );
    }else{
        var openTheLeads = [];
        for(var i = 0; i < selectedLeads.length; i++){
            openTheLeads.push(selectedLeads[i]);
        }
        openLeads(openTheLeads);
    }
}
function hideLeadsActionIcons(){
    $("#emailToLeads").css("display",'none');
    $("#smsToLeads").css("display",'none');
    $("#deleteSelectedLeads").css("display",'none');
    $("#openLeads").css("display",'none');
    $("#theBTN").css("display",'block');
}
function showLeadsActionIcons() {
    $("#emailToLeads").css("display",'inline-block');
    $("#smsToLeads").css("display",'inline-block');
    $("#deleteSelectedLeads").css("display",'inline-block');
    $("#openLeads").css("display",'inline-block');
    $("#theBTN").css("display",'none');
}

function toggleRowSelection(leadId,forceState){

    if(forceState != undefined){
        if(forceState == 1){
            var row = ($("#selectLead-"+leadId)[0]);
            row.classList.add("selectedRow");
        }else{
            var row = ($("#selectLead-"+leadId)[0]);
            row.classList.remove("selectedRow");
        }
    }else{
        var row = ($("#selectLead-"+leadId)[0]);
        if(row.classList.contains("selectedRow")){
            row.classList.remove("selectedRow");
        }else{
            row.classList.add("selectedRow");
        }
    }
}

// prototype for removing from array by value
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
// ================================ END TOGGLE LEADS ROWS ==============================

// ================================ START 'GET LEADS BY' COOKIE ==============================

var getLeadsByCookie = {
    setCookie: function (cvalue){
        var cname = "getLeadsByMoveDate";
        var exdays = 365;
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    getCookie: function () {
        var name = "getLeadsByMoveDate=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}


// ================================ END 'GET LEADS BY' COOKIE ==============================

function showAssignInfo(){
    swal({
        text: "This feature allows you to allocate salesmen from your organization to specific leads and manage each sales person’s progress.",
        buttons: {
            skip:{
                text:"Got It"
            }
        }
    });
}