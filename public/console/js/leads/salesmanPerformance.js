var theStartDateSales;
var theEndDateSales;
$(document).ready(function() {

    $('#reportrange span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#reportrange').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment(),
        endDate: moment(),
        minDate: '01/01/2018',
        maxDate: '12/31/2020',
        dateLimit: { days: 365 },
        showDropdowns: true,
        showWeekNumbers: false,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Go',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function(start, end, label) {

        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        theStartDateSales = start.format('DD-MM-YYYY');
        theEndDateSales = end.format('DD-MM-YYYY');

        getSalesData();

    });
    theStartDateSales = moment().startOf('month').format('DD-MM-YYYY');
    theEndDateSales = moment().format('DD-MM-YYYY');

});

function getSalesData(){
    loadingState();
    var strUrl = BASE_URL+'/console/actions/leads/performance/getSalesData.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        method: "POST",
        data:{
            start: theStartDateSales,
            end: theEndDateSales
        },
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {

        data = JSON.parse(data);
        var container = document.getElementById("performance");
        container.innerHTML = "";
        if (data.length>0){
            for (var i = 0; i < data.length; i++) {
                setSalesTable(data[i],container);
            }
        }else{
            var tr = document.createElement("tr");

            var td = document.createElement("td");
            td.setAttribute("colspan",9);
            td.style.textAlign = "center";
            var tdText = document.createTextNode("No Data Yet");

            td.appendChild(tdText);
            tr.appendChild(td);
            container.appendChild(tr);
        }
        stopLoadingState();
    });
}

function setSalesTable(data,container){

    var tr = document.createElement("tr");

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.name);

    td.appendChild(tdText);
    tr.appendChild(td);


    var td = document.createElement("td");
    var tdText = document.createTextNode(data.books+" ("+data.bookPercentage+"%)");

    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.totalLeads);

    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.totalEmailsSent);

    td.appendChild(tdText);
    tr.appendChild(td);


    var td = document.createElement("td");
    var tdText = document.createTextNode("$"+data.payments);

    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(data.salesBonusPerc);

    td.appendChild(tdText);
    tr.appendChild(td);

    container.appendChild(tr);

}
