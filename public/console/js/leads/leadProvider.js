function getProviders() {
    loadingState();
    var strUrl = BASE_URL + '/console/actions/leadProviders/getProviders.php', strReturn = "";
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        async: true
    }).done(function (data) {
        try {
            data = JSON.parse(data);
            var container = document.getElementById("providersContainer");
            container.innerHTML = "";
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    setProviders(data[i],container);
                }
            }else{
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                td.setAttribute("colspan",7);
                var tdText = document.createTextNode("No Leads Providers Yet");
                td.appendChild(tdText);
                tr.appendChild(td);
                container.appendChild(tr);

            }
        }catch (e) {
            toastr.error("Error Loading Providers");
        }
        stopLoadingState();
    });
}
function setProviders(provider,container){
    if(provider['providerWebsite'].indexOf("www") > 0 || provider['providerWebsite'].indexOf("http") >= 0 || provider['providerWebsite'].indexOf("https:") >= 0 ){

    }else{
        if (provider['providerWebsite'] != "") {
            provider['providerWebsite'] = "www."+provider['providerWebsite'];
        }
    }
    var tr = document.createElement("tr");
    tr.id = "provider-"+provider['id'];
    tr.style.border = "1px solid #dadada";
    var td = document.createElement("td");
    td.style.verticalAlign = "middle";
    var span = document.createElement("span");
    span.id = "providerSpan-"+provider['id'];
    if(provider['isActive'] == 1){
        span.classList.add("label");
        span.classList.add("label-primary");
        var spanText = document.createTextNode("Active");
        span.appendChild(spanText);
    }else{
        tr.style.backgroundColor = "#F3F3F4";
        span.classList.add("label");
        span.classList.add("label-default");
        var spanText = document.createTextNode("Inactive");
        span.appendChild(spanText);
    }
    td.appendChild(span);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(provider['uniqueKey']);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(provider['providerName']);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(provider['providerEmail']);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    if (provider['providerWebsite'] != "") {
        var a = document.createElement("a");
        a.setAttribute("href", provider['providerWebsite']);
        a.setAttribute("target", "_blank");
        var aText = document.createTextNode(provider['providerWebsite']);
        a.appendChild(aText);
        td.appendChild(a);
    }
    tr.appendChild(td);

    var td = document.createElement("td");
    var tdText = document.createTextNode(provider['providerContactName']);
    td.appendChild(tdText);
    tr.appendChild(td);

    var td = document.createElement("td");
    var a = document.createElement("a");
    a.classList.add("btn");
    a.classList.add("btn-warning");
    a.classList.add("btn-sm");
    a.style.marginRight = "5px";
    a.id = "providerButton-"+provider['id'];

    if(provider['isActive'] == 1){
        a.setAttribute("onclick","disableProvider("+provider['id']+")");
        var aText = document.createTextNode("Disable");
    }else{
        a.setAttribute("onclick","reActivateProvider("+provider['id']+")");
        var aText = document.createTextNode("Re-Activate");
    }
    a.appendChild(aText);

    var aEdit = document.createElement("a");
    aEdit.classList.add("btn");
    aEdit.classList.add("btn-info");
    aEdit.classList.add("btn-sm");
    aEdit.style.marginRight = "5px";
    aEdit.setAttribute("onclick","editProvider("+provider['id']+")");
    var aEditText = document.createTextNode("View");
    aEdit.appendChild(aEditText);

    var aDel = document.createElement("a");
    aDel.classList.add("btn");
    aDel.classList.add("btn-danger");
    aDel.classList.add("btn-sm");
    aDel.setAttribute("onclick","deleteProvider("+provider['id']+")");
    var aDelText = document.createTextNode("Delete");
    aDel.appendChild(aDelText);

    td.appendChild(a);
    td.appendChild(aEdit);
    td.appendChild(aDel);
    tr.appendChild(td);

    container.appendChild(tr);

}
getProviders();
function disableProvider(providerId){
    swal({
        title: "Are you sure?",
        text: "This will set your provider as in-active",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/leadProviders/disableProvider.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    providerId: providerId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try{
                    data = JSON.parse(data);

                    if(data == true){
                        var button = document.getElementById("providerButton-"+providerId);
                        button.setAttribute("onclick","reActivateProvider("+providerId+")");
                        button.innerText = "Re-Activate";
                        var tr = document.getElementById("provider-"+providerId);
                        tr.style.backgroundColor = "#F3F3F4";
                        var span = document.getElementById("providerSpan-"+providerId);
                        span.classList.remove("label-primary");
                        span.classList.add("label-default");
                        span.innerText = "Inactive";
                        toastr.success('Provider is now in-active','Changes Saved');
                    }else{
                        toastr.error('Your changes were not saved. Please try again later.','Oops');
                    }
                }catch (e){
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }

            });
        }
    });
}

function reActivateProvider(providerId){
    swal({
        title: "Are you sure?",
        text: "This will set your provider as active",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/leadProviders/reActiveProvider.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    providerId: providerId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try{
                    data = JSON.parse(data);
                    if(data == true){
                        var button = document.getElementById("providerButton-"+providerId);
                        button.setAttribute("onclick","disableProvider("+providerId+")");
                        button.innerText = "Disable";
                        var tr = document.getElementById("provider-"+providerId);
                        tr.style.backgroundColor = "#fff";
                        var span = document.getElementById("providerSpan-"+providerId);
                        span.classList.remove("label-default");
                        span.classList.add("label-primary");
                        span.innerText = "Active";
                        toastr.success('Provider is now active','Changes Saved');
                    }else{
                        toastr.error('Your changes were not saved. Please try again later.','Oops');
                    }
                }catch (e){
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }

            });
        }
    });
}

function deleteProvider(providerId){
    swal({
        title: "Are you sure?",
        text: "This will delete this provider",
        icon: "warning",
        dangerMode: true,
        buttons: true,
    }).then(function(isConfirm){
        if (isConfirm) {
            var strUrl = BASE_URL+'/console/actions/leadProviders/deleteProvider.php', strReturn = "";
            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data: {
                    providerId: providerId
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                try{
                    data = JSON.parse(data);

                    if(data == true){
                        document.getElementById("provider-"+providerId).remove();
                        toastr.success('Provider is now deleted','Changes Saved');
                    }else{
                        toastr.error('Your changes were not saved. Please try again later.','Oops');
                    }
                }catch (e){
                    toastr.error('Your changes were not saved. Please try again later.','Oops');
                }

            });
        }
    });

}

function updateMailAccountPassword(accountId){
    showCustomModal('categories/mail/updateMailAccountPassword.php?accountId='+accountId);
}

function addProvider(){
    showCustomModal('categories/iframes/leadProviders/addProvider.php');
}
function editProvider(id){
    showCustomModal('categories/iframes/leadProviders/editProvider.php?id='+id);
}