var placeHolderSwalObject = [];

function createPlaceHolderSwal(obj,summerNote) {
    placeHolderSwalObject = obj;
    var content = document.createElement("div");
    var secondContainer = document.createElement("div");
    secondContainer.id = "placeHolderSwal";

    var select = document.createElement("select");
    select.setAttribute("onchange","setSwalSelect($(this),'"+summerNote+"')");
    select.classList.add("form-control");

    var option = document.createElement("option");
    var optionText = document.createTextNode("Select Tags Type");
    option.appendChild(optionText);
    select.appendChild(option);
    console.log(obj);
    for (var i in obj){
        var option = document.createElement("option");
        option.value = i;
        var optionText = document.createTextNode(obj[i].title);
        option.appendChild(optionText);
        select.appendChild(option);
    }

    content.appendChild(select);
    content.appendChild(secondContainer);

    parent.swal({
        title: "Select Tags",
        content: content,
        button: "Close",
    });
}

function setSwalSelect(obj,summerNote) {
    var container = document.getElementById("placeHolderSwal");
    container.innerHTML = "";

    var content = document.createElement("div");
    content.style.marginTop = "13px";

    var numOfRuns = 0;

    var runNumOfTimes = 5;

    if (obj.val() == "shipping"){
        runNumOfTimes = 3;
    }

    if (obj.val() == "company"){
        runNumOfTimes = 4;
    }

    for (var i in placeHolderSwalObject[obj.val()]['tags']){
        if (numOfRuns == runNumOfTimes){
            numOfRuns = 0;
            var br = document.createElement("br");
            content.appendChild(br);
        }
        var current = placeHolderSwalObject[obj.val()]['tags'][i];

        var span = document.createElement("span");
        span.classList.add("label");
        span.classList.add("label-primary");
        span.style.cursor = "pointer";
        span.style.marginRight = "5px";
        span.setAttribute("onclick","addTextFromSwal('"+current['tag']+"','"+summerNote+"');");
        var spanText = document.createTextNode(current['tagName']);
        span.appendChild(spanText);
        content.appendChild(span);
        numOfRuns++;
    }

    container.appendChild(content);

}

function addTextFromSwal(text,summerNote){

    $(summerNote).summernote('editor.restoreRange');
    $(summerNote).summernote('editor.focus');
    $(summerNote).summernote('editor.insertText', text);

    parent.swal.close();
}