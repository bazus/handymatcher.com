<?php
$pagePermissions = array(false,true);
$_SERVER['LOCAL_NL_PATH'] = '/var/www/handymatcher.com/public';
$_SERVER['LOCAL_NL_URL']  = 'http://handy.bazus.net/console';

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/firstSteps.php");

$firstSteps = new firstSteps($bouncer["organizationData"],$bouncer["credentials"]["userId"],$bouncer['isUserAnAdmin']);
$showFirstSteps = $firstSteps->showFirstSteps;
if ($showFirstSteps == true){
    $firstSteps->calcFirstSteps();
    $showProgressBar = $firstSteps->showProgressBar;
    $firstStepsCurrentStep = $firstSteps->firstStepsCurrentStep;
    $userDoneStep = $firstSteps->userDoneStep;
    $showFirstSteps = $firstSteps->showFirstSteps;
}else{
    $firstSteps->calcFirstSteps();
    $showProgressBar = $firstSteps->showProgressBar;
    $firstStepsCurrentStep = $firstSteps->firstStepsCurrentStep;
    $showFirstSteps = false;
}

$showAddProviders = false;

if ($showFirstSteps == true) {
    require_once("firstSteps.php");
}else{
    require_once("home.php");
}
