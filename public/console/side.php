<?php


require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

if (!isset($showFirstSteps)){
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/firstSteps.php");

    $firstSteps = new firstSteps($bouncer["organizationData"],$bouncer["credentials"]["userId"],$bouncer['isUserAnAdmin']);
    $showFirstSteps = $firstSteps->showFirstSteps;
    if ($showFirstSteps == true){
        $firstSteps->calcFirstSteps();
        $showProgressBar = $firstSteps->showProgressBar;
        $firstStepsCurrentStep = $firstSteps->firstStepsCurrentStep;
        $userDoneStep = $firstSteps->userDoneStep;
    }else{
        $firstSteps->calcFirstSteps();
        $showProgressBar = $firstSteps->showProgressBar;
        $firstStepsCurrentStep = $firstSteps->firstStepsCurrentStep;
        $showFirstSteps = false;
    }

    $showAddProviders = false;
}

$profilePicture = "";
if(isset($bouncer['userData']['profilePicture']) && $bouncer['userData']['profilePicture'] !== null && $bouncer['userData']['profilePicture'] !== "") {
    $profilePicture = $bouncer['userData']['profilePicture'];
}

// No longer needed, changed all the profile pics to global url and public
/*
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

$s3 = new s3();
if(isset($bouncer['userData']['profilePicture']) && $bouncer['userData']['profilePicture'] !== null && $bouncer['userData']['profilePicture'] !== ""){
    try{
        $profilePicture = $s3->getObjectURL($bucket = "thenetworkleads",$key = "profiles/".$bouncer['userData']['profilePicture']);
    }catch(Exception $e) {
        $profilePicture = "";
    }
}
*/

$url = explode("console/",$_SERVER["REQUEST_URI"]);

$eraselastpage = explode("?",$url[count($url)-1]);
$url[count($url)-1] = $eraselastpage[0];

$page = "";
$category = "";

if(isset($url[1])) {
    $URI = explode("/", $url[1]);


    if ($URI[0] == "index.php" || $URI[0] == "") {
        $page = "home";
    }
    if ($URI[0] == "categories" && $URI[1] == "user" && $URI[2] == "fileManager.php") {
        $page = "fileManager";
    }
    if ($URI[0] == "categories" && $URI[1] == "user" && $URI[2] == "calendar.php") {
        $page = "calendar";
    }
    if ($URI[0] == "categories" && $URI[1] == "user" && $URI[2] == "management.php") {
        $page = "management";
    }
    if ($URI[0] == "categories" && $URI[1] == "organization") {
        $page = "management";
    }
    if ($URI[0] == "categories" && $URI[1] == "moving" && $URI[2] == "inventory.php") {
        $page = "inventory";
    }
    if ($URI[0] == "categories" && $URI[1] == "leads" && $URI[2] == "leads.php") {
        $page = "leads";
    }
    if ($URI[0] == "categories" && $URI[1] == "leads" && $URI[2] == "jobBoard.php") {
        $page = "jobBoard";
    }
    if ($URI[0] == "categories" && $URI[1] == "leads" && $URI[2] == "leadProviders.php") {
        $page = "leadProviders";
    }
    if ($URI[0] == "categories" && $URI[1] == "leads" && $URI[2] == "lead.php") {
        $page = "lead";
    }
    if ($URI[0] == "categories" && $URI[1] == "reports" && $URI[2] == "reports.php") {
        $page = "reports";
    }
    if ($URI[0] == "categories" && $URI[1] == "moving" && $URI[2] == "partners.php") {
        $page = "partners";
    }
    if ($URI[0] == "categories" && $URI[1] == "moving" && $URI[2] == "home.php") {
        $page = "movingHome";
    }
    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "templates.php") {
        $page = "mail_templates";
    }

    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "outgoing.php") {
        $page = "mail_outgoing";
    }
    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "automation.php") {
        $page = "mail_auto";
    }
    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "compose.php") {
        $page = "mail_outgoing";
    }
    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "templater.php") {
        $page = "mail_templates";
    }
    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "viewEmail.php") {
        $page = "mail_outgoing";
    }
    if ($URI[0] == "categories" && $URI[1] == "mail" && $URI[2] == "mailSettings.php") {
        $page = "mail_settings";
    }
}

$organization = new organization($bouncer["credentials"]["orgId"]);
$organizationData = $organization->getData();
?>
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/dialer/dialer.css" rel="stylesheet">

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<style>
    body.mini-navbar .nav-header{
        padding: 0px !important;
    }

    .nav-header {
        padding: 0px;
    }
    .nav-header>.profile-element{
        padding: 13px 3px 13px 21px;
    }

    body.mini-navbar .logoahref{
            display: none;
    }

    .profile-element span:hover{
        color: #fff !important;
    }

    @media only screen and (max-width: 480px) {
        .logo-element {
            max-height: 84px;
        }
    }


    .swal-content{
        height: 100% !important;
    }
    .swal-content>div{
        height: 100% !important;
    }

     .swal-emails{
         width: 90% !important;
         height: 94% !important;
         overflow-y: hidden;
     }
    .swal-emails .swal-content{
        margin: 0px !important;
        padding: 0 13px 0 20px !important;
    }
    .swal-feature-request{
        height: 590px;
    }
    .swal-feature-request .swal-content{
        margin: 0;
        padding: 0;
    }
    #featureRequestBtn:hover{
        background: #0abb87;
    }
</style>

<div style="z-index: 200">
<nav class="navbar-default navbar-static-side" role="navigation" style="z-index: 100;">

    <div class="sidebar-collapse" style="z-index: 999;">
        <div id="logoDiv" style="text-align: center;">
        <?php if($organizationData["logoPath"] != "" && @getimagesize( $organizationData["logoPath"] ) > 0){ ?>
            <a class="logoahref" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/"><img src="<?php echo $organizationData["logoPath"]; ?>" style="max-width: 100%; max-height: 100px; padding: 10px 10px;" alt="<?php echo $organizationData["organizationName"]; ?>'s Logo"></a>
        <?php }else{ ?>
            <a class="logoahref" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/"><img src="<?php echo $_SERVER['CDN']."/images/whitelogo.png"; ?>" style="max-width: 100%; max-height: 100px; padding: 10px 10px;height: 60px;" alt="Network Leads Logo"></a>
        <?php } ?>
        </div>
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <div onclick="showCustomModal('categories/user/profile.php');" style="cursor: pointer;">
                        <?php if($bouncer['userData']['profilePicture'] && getimagesize($profilePicture)){ ?>
                            <img alt="image" id="profilePicture" class="img-circle" style="float: left;width: 40px;margin-left: -15px;" src="<?= $profilePicture ?>" />
                        <?php }else{ ?>
                            <img alt="image" id="profilePicture" class="img-circle" style="float: left;width: 40px;margin-left: -15px;" src="<?= $_SERVER['LOCAL_NL_URL']; ?>/console/img/icons/user.png" />
                        <?php } ?>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear" style="padding-left: 5%;">
                            <span class="block"> <strong class="font-bold"><?php echo $bouncer["userData"]["fullName"]; ?></strong></span>
                            <span class="text-muted text-xs block" onclick="console.log('aaa');">
                                <?php echo $bouncer["userData"]["organizationName"]; ?>
                                <b class="caret"></b>
                            </span>
                        </span>
                    </a>
                    </div>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs" style="width: 95%;margin-left: 5px;">
                        <li><a onclick="showCustomModal('categories/user/profile.php')">Profile</a>
                        <li><a href="http://help.network-leads.com" target="_blank" title="Network Leads Help Center">Help Center</a></li>
                        <!--
                        <li class="divider"></li>
                        <li><a onclick="showCustomModal('categories/iframes/todo/todo.php')">My Todo List</a></li>
                        -->
                        <li class="divider"></li>
                        <li><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/login/actions/logout.php">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <?php if($GLOBALS['mobileDetect']["isMobile"]){ ?>
                        <span class="center-block" style="cursor: pointer;" onclick="showCustomModal('categories/user/profile.php')">
                        <?php if($bouncer['userData']['profilePicture'] && getimagesize($profilePicture)){ ?>
                            <img alt="image" id="profilePicture" class="img-circle" style="width: 48px;" src="<?= $profilePicture ?>" />
                        <?php }else{ ?>
                            <img alt="image" id="profilePicture" class="img-circle" style="width: 48px;" src="<?= $_SERVER['LOCAL_NL_URL']; ?>/console/img/icons/user.png" />
                        <?php } ?>
                        </span>
                    <?php }else{ ?>
                        NL
                    <?php }?>
                </div>
            </li>

            <!--
            <li style="background: #1cc09f;color: white;">
                <?php
                    $orgsInPack = $bouncer["organization"]->getOrgsInPack();
                    for($i = 0;$i<count($orgsInPack);$i++){
                        ?>
                        <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/organization/switchOrgInPack.php?orgId=<?php echo $orgsInPack[$i]["id"]; ?>" style="color:#fff"><span class="nav-label"><?php echo $orgsInPack[$i]["organizationName"]; ?></span></a>
                        <?php
                    }
                ?>
            </li>
-->
            <?php
            if($bouncer["isUserAnAdmin"] == "1"){
            ?>
            <li <?php if($page == "home"){echo ' class="active"';} ?>>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
            </li>
            <?php } ?>


            <li id="side_leads" <?php if($page == "leads" || $page == "lead" || $page == "leadProviders"){echo ' class="active"';} ?>>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/leads/leads.php"><i class="fa fa-list-ul"></i> <span class="nav-label">Leads</span></a>
            </li>

            <?php if($bouncer["movingSettingsData"]["jobBoardFeature"] == true){
                ?>
                <li id="side_jobBoard" <?php if($page == "jobBoard"){echo ' class="active"';} ?>>
                    <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/leads/jobBoard.php"><i class="fa fa-list-alt"></i> <span class="nav-label">Job Board</span></a>
                </li>
                <?php } ?>


            <?php if ($bouncer["isUserAnAdmin"] || $bouncer["userAuthorization"]->isUserTypeMatch([2])){ ?>
            <li id="side_moving" <?php if($page == "movingHome"){echo ' class="active"';} ?>>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/moving/home.php"; ?>"><i class="fa fa-truck"></i> <span class="nav-label">Moving</span></a>
            </li>
            <?php } ?>

            <li id="side_mail" <?php if($page == "mail_outgoing" || $page == "mail_auto" || $page == "mail_templates" || $page == "mail_settings"){echo ' class="active"';} ?>>
                <a><i class="fa fa-bullhorn"></i> <span class="nav-label"> Marketing</span> <span class="fa arrow"></span></a>
                <ul id="side_mail_open" class="nav nav-second-level collapse <?php if(isset($URI[1]) && $URI[1] == "mail"){echo 'in';} ?>">
                    <li<?php if($page == "mail_outgoing"){echo ' class="active"';} ?>><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/outgoing.php">Outgoing Emails  </a></li>

                    <!--
                    <li<?php if($page == "mail_auto"){echo ' class="active"';} ?>><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/automation.php">Automation  </a></li>
-->
                    <li<?php if($page == "mail_templates"){echo ' class="active"';} ?>><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/templates.php">Emails Templates  </a></li>
                    <li<?php if($page == "mail_settings"){echo ' class="active"';} ?>><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/mailSettings.php">Settings  </a></li>
                </ul>
            </li>
            <?php if ($bouncer["isUserAnAdmin"]){ ?>
                <li id="side_moving" <?php if($page == "reports"){echo ' class="active"';} ?>>
                    <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/reports/reports.php"; ?>"><i class="fa fa-line-chart"></i> <span class="nav-label">Reports</span></a>
                </li>
            <?php } ?>
            <li id="side_calendar" <?php if($page == "calendar"){echo ' class="active"';} ?>>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/user/calendar.php"; ?>"><i class="fa fa-calendar"></i> <span class="nav-label">Calendar</span></a>
            </li>
            <li id="side_fileManager" <?php if($page == "fileManager"){echo ' class="active"';} ?>>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/user/fileManager.php"; ?>"><i class="fa fa-file"></i> <span class="nav-label">File Manager</span></a>
            </li>

            <!--
            <li id="side_tools" <?php if($page == "fileManager" || $page == "calendar"){echo ' class="active"';} ?>>
                <a><i class="fa fa-th-large"></i> <span class="nav-label">Tools</span> <span class="fa arrow"></span></a>
                <ul id="side_tools_open" class="nav nav-second-level collapse <?php if(isset($URI[1]) && $URI[1] == "user" && $URI[2] !== 'support.php' && $URI[2] !== 'management.php'){echo 'in';} ?>">
                    <li<?php if($page == "fileManager"){echo ' class="active"';} ?>><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/user/fileManager.php">File Manager</a></li>
                    <li<?php if($page == "calendar"){echo ' class="active"';} ?>><a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/user/calendar.php">Calendar</a></li>
                </ul>
            </li>
-->

            <?php
            if($bouncer['organizationData']['isNMpartner'] == 1){
                ?>

                <li id="side_moving" <?php if($page == "partners"){echo ' class="active"';} ?>>
                    <a href="<?php echo $_SERVER['LOCAL_NL_URL']."/console/categories/moving/partners.php"; ?>"><i class="fa fa-handshake-o"></i> <span class="nav-label">Network Moving</span></a>
                </li>

                <?php
            }
            ?>

            <?php if (isset($showProgressBar) && $showProgressBar){ if (!isset($firstStepsCurrentStep)){$firstStepsCurrentStep = 1;}  ?>
                <li class="nav-label">
                    <hr style="margin: 0px;border-top: 1px solid #ffffff30 !important;" />
                    <a onclick="getBackToSteps()">
                        <span class="nav-label text-center" style="color:#fff;display: block;margin-bottom: 4px;">Getting Started</span>
                        <div class="progress progress-striped nav-label" style="margin-bottom: 5px;height: 3px;">
                            <div style="width: <?= $firstStepsCurrentStep*25 ?>%;" class="progress-bar"></div>
                        </div>
                        <span class="nav-label text-center" style="color:#fff;display: block"><strong><?= $firstStepsCurrentStep ?>/4</strong> Steps Completed</span>
                    </a>
                    <hr style="margin: 0px;border-top: 1px solid #ffffff30 !important;">
                </li>
            <?php } ?>

        </ul>

    </div>
    <!-- ===== My Organization ===== -->
    <div id="side_organization" class="sidebar-collapse" style="z-index: 999;bottom: 0px;position: fixed;width: 219px;">
        <ul class="nav metismenu" id="bottom-left-menu">
            <li id="side_featureRequest" style="padding:13px;">
                <a style="padding:9px" id="featureRequestBtn" onclick="openFeatureRequest()" type="button" class="btn btn-primary btn-rounded"><i style="color:white;margin:0;" class="fa fa-lightbulb-o"></i><span style="color:white;margin-left:5px;" class="nav-label">Feature Request</span></a>
            </li>
            <li<?php if($page == "management"){echo ' class="active"';} ?>>
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/user/management.php"><i class="fa fa-gears"></i> <span class="nav-label">My Organization</span>  </a>
            </li>

        </ul>
    </div>
    <!-- ===== My Organization ===== -->
</nav>
</div>

<div id="customModal"></div>

<script src="<?= $_SERVER['CDN'] ?>/home/js/jquery-3.1.1.min.js"></script>

<!-- Ladda -->
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/ajaxForm/jquery.form.js"></script>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/system.min.js"></script>

<!-- custom modal -->
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/cusmod/cusmod.css" rel="stylesheet" />
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/cusmod/cusmod.min.js"></script>

<div id="cusmod"></div>

<script>

    function openFeatureRequest() {

        var iframe = document.createElement("iframe");
        iframe.src = BASE_URL + "/console/categories/user/FeatureRequest.php";

        iframe.setAttribute("style", "border:none;width: 100%;height: 590px;");
        iframe.setAttribute("name", "feature-request-iframe");
        iframe.onload = function () {
            //window.frames['lead-create-claim-iframe'].init("AAA");
        };

        swal({
            className: "swal-feature-request",
            content: iframe,
            buttons: false,
        });

    }

    function showCustomModal(pageUrl) {
        runModal(pageUrl);
    }
    var modalInProgress = false;
    var modalRunning = false;

    function runModal(pageUrl) {
        if(modalInProgress == true){return;}else{modalInProgress = true;}

        pageUrl = encodeURI("<?php echo $_SERVER['LOCAL_NL_URL'] . "/console/"; ?>" + pageUrl);

        var timeout = null;
        jQuery.ajax({
            url: pageUrl,
            beforeSend: function(xhr) {

            },
            error: function(xhr, textStatus, errorThrown) {
                if(xhr.status == 403){
                    unAuthorizedAction();
                }
                modalInProgress = false;
                // Handle other (non-timeout) errors
            },
            success: function(data, textStatus) {
                modalInProgress = false;
                // Handle the result
                $("#customModal").html($(data));
                $("#myCustomModal").modal("show");
            }
        });
    }

        function getBackToSteps() {
        document.cookie = "hideFirstSteps=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT";
        location.href = "<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/";
    }

    function startmodal() {
        var settings = {
            animation:"fadeInUp",
            //template: "cusmod-blue",
            hideCloseBTN:false,
            headerImage: "example2",
            header: "I am a title",
            subHeader: "I am a subtitle",
            content: "I am the content",
            footer: {
                "Close":null,
                "Got it":"success()"
            }
        };
        initCusmod("#cusmod", settings);
    }

</script>

<?php
if($_SERVER["ENVIRONMENT"] == "prod"){
?>
<!-- INTERCOM -->
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/ssslnlvm';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<script>

    window.intercomSettings = {
        app_id: "ssslnlvm",
        user_id:"<?php echo $bouncer['userData']["id"]; ?>",
        phone:"<?php echo $bouncer["userData"]["phoneNumber"]; ?>",
        name: "<?php echo $bouncer['userData']["fullName"]; ?>", // Full name
        email: "<?php echo $bouncer['userData']["email"]; ?>", // Email address
        company: {
            company_id: "<?php echo $bouncer['organizationData']["id"]; ?>",
            name: "<?php echo $bouncer["userData"]["organizationName"]; ?>",
            plan: "<?php echo $bouncer['organizationData']["organizationPackage"]; ?>",
            company_website: "<?php echo $bouncer['organizationData']["website"]; ?>",
            company_created_at: "<?php echo $bouncer['organizationData']["createdAt"]; ?>"
        },
        hide_default_launcher:true, // hide it by default
        custom_launcher_selector: '#triggerIntercom' // show it when clicking on something with this id
    };
</script>
<!-- INTERCOM -->

<!-- FULLSTORY PLUGIN -->
<script>
    window['_fs_debug'] = false;
    window['_fs_host'] = 'fullstory.com';
    window['_fs_script'] = 'edge.fullstory.com/s/fs.js';
    window['_fs_org'] = 'QKHT2';
    window['_fs_namespace'] = 'FS';
    (function(m,n,e,t,l,o,g,y){
        if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
        g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
        o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script;
        y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
        g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
        g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
        g.log = function(a,b) { g("log", [a,b]) };
        g.consent=function(a){g("consent",!arguments.length||a)};
        g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
        g.clearUserCookie=function(){};
    })(window,document,window['_fs_namespace'],'script','user');

    // This is an example script - don't forget to change it!
    FS.identify('<?php echo $bouncer['userData']["id"]; ?>', {
        displayName: '<?php echo $bouncer['userData']["fullName"]." [". $bouncer["userData"]["organizationName"]."]"; ?>',
        email: '<?php echo $bouncer['userData']["email"]; ?>'
        // TODO: Add your own custom user variables here, details at
        // https://help.fullstory.com/hc/en-us/articles/360020623294
    });
</script>
<!-- FULLSTORY PLUGIN -->

<?php } ?>

<!-- SENTRY (JS ERROR TRACKING) PLUGIN -->
<script src="https://browser.sentry-cdn.com/5.12.1/bundle.min.js" integrity="sha384-y+an4eARFKvjzOivf/Z7JtMJhaN6b+lLQ5oFbBbUwZNNVir39cYtkjW1r6Xjbxg3" crossorigin="anonymous"></script>
<script>
    <?php if($_SERVER["ENVIRONMENT"] == "prod"){ ?>
    Sentry.init({ dsn: 'https://44605ae60e414b238a957ea5109605b5@sentry.io/2524728',environment: 'production', });
    <?php }elseif($_SERVER["ENVIRONMENT"] == "stage"){ ?>
    Sentry.init({ dsn: 'https://44605ae60e414b238a957ea5109605b5@sentry.io/2524728',environment: 'staging', });
    <?php } ?>

    Sentry.configureScope(function(scope) {
        scope.setUser({
            "id": '<?php echo $bouncer["userData"]["id"]; ?>',
            "username":'<?php echo $bouncer['userData']["fullName"]." [". $bouncer["userData"]["organizationName"]."]"; ?>',
            "email": '<?php echo $bouncer['userData']["email"]; ?>'
        });
    });
</script>
<!-- SENTRY (JS ERROR TRACKING) PLUGIN -->

<!-- SEND SMS PLUGIN -->
<style>
    .swal-text:first-child {
        margin-top: 19px !important;
    }
</style>
<script>
    // This need to be somewhere globally in the website
    // Currently I put it in the side.php

    var sendSMScontroller = {
        init:function (toPhoneNumber,leadIds,text,callback) {
            var content = document.createElement("div");
            if(text == undefined){text = "";}

            var iframe = document.createElement("iframe");
            iframe.name = "sendSMSiframe";
            if(toPhoneNumber != undefined){
                iframe.src = BASE_URL+"/console/categories/iframes/system/sendSMS.php?toPhoneNumber="+toPhoneNumber;
            }
            if(leadIds != undefined){
                iframe.src = BASE_URL+"/console/categories/iframes/system/sendSMS.php?leadsIds[]="+leadIds.join('&leadsIds[]=');
            }

            iframe.setAttribute("style","border:none;width: 100%;height:600px");
            iframe.setAttribute("name","send-sms-controller");

            iframe.onload= function() {
                window.frames['send-sms-controller'].SMScontroller.setMessageText(text);
                window.frames['send-sms-controller'].SMScontroller.setupSendCallback(callback);
            };
            content.appendChild(iframe);

            swal({
                content: content,
                buttons:false,
                className: "swal-sms-contoller"
            });
        }
    };




    /* OLD
    var sendSMScontroller = {
        settings:{title:"Enter a phone number",content:"",leadIds:null},
        fromPhones:[],
        init: function(settings) {
            sendSMScontroller.fromPhones = [];

            // Get available phone number
            jQuery.ajax({
                url: "<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/twilio/getActivePhoneNumbers.php",
                method: "POST",
                data:{},
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                try{
                    data = JSON.parse(data);
                    for(var i = 0;i<data.length;i++){
                        sendSMScontroller.fromPhones.push(data[i]);
                    }

                }catch (e) {

                }
                if(settings != undefined){
                    sendSMScontroller.settings = settings;
                }

                if(sendSMScontroller.settings.leadIds == null){
                    sendSMScontroller.initToPhones();
                }else{
                    sendSMScontroller.initToLeads();
                }

            });

        },
        initToLeads:function() {
            var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/leads/getLeadsPhoneNumbers.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    leadsIds:sendSMScontroller.settings.leadIds
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                data = JSON.parse(data);

                var wrapper = sendSMScontroller.createFieldsForLeads(data);
                swal({
                    text: sendSMScontroller.settings.title,
                    content: wrapper,
                    button: {
                        text: "Send",
                        closeModal: false,
                    }
                }).then(phone => {
                    if (!phone) throw null;

                    sendSMScontroller.sendSMSToLeads();

                });

                sendSMScontroller.checkFields();


            });


        },
        initToPhones:function(){
            var wrapper = sendSMScontroller.createFieldsForPhones();

            swal({
                text: sendSMScontroller.settings.title,
                content: wrapper,
                button: {
                    text: "Send",
                    closeModal: false,
                }
            }).then(phone => {
                if (!phone) throw null;

                sendSMScontroller.sendSMSToSinglePhone();

            });

            sendSMScontroller.checkFields();

        },
        sendSMSToLeads: function(){
            var fromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
            var msg = document.getElementById("sendSMSModalMessage").value;

            var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/twilio/sendSMSToLeads.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    fromPhoneNumberId:fromPhoneNumberID,
                    leadsIds:sendSMScontroller.settings.leadIds,
                    msg:msg
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {

                swal.stopLoading();
                swal.close();

                console.log(data);
                data = JSON.parse(data);
                console.log(data);

            });

        },
        sendSMSToSinglePhone:function(){
            var fromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
            var toPhoneNumber = document.getElementById("sendSMSModalToPhoneNumber").value;
            var msg = document.getElementById("sendSMSModalMessage").value;

            var strUrl = '<?= $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/twilio/sendSMS.php', strReturn = "";

            jQuery.ajax({
                url: strUrl,
                method: "POST",
                data:{
                    fromPhoneNumberId:fromPhoneNumberID,
                    toPhoneNumber:toPhoneNumber,
                    msg:msg
                },
                success: function (html) {
                    strReturn = html;
                },
                async: true
            }).done(function (data) {
                swal.stopLoading();
                swal.close();
                try{
                    data = JSON.parse(data);
                    if(data.status == true){
                        swal("SMS sent", "SMS successfully sent to "+toPhoneNumber, "success");
                    }else{
                        if(data.resp == "balance"){
                            limitReached("balance");
                        }else {
                            swal("SMS not sent", "Please contact support", "error", {
                                buttons: {
                                    cancel: "Close",
                                    catch: {
                                        text: "Contact Support",
                                        value: "contact",
                                    }
                                },
                            }).then((value) => {
                                if (value == "contact") {
                                    window.open("<?= $_SERVER['LOCAL_NL_URL']; ?>/console/?needhelp=yes");
                                }
                            });
                        }
                    }
                }catch (e) {
                    swal("SMS not sent", "Please contact support", "error", {
                        buttons: {
                            cancel: "Close",
                            catch: {
                                text: "Contact Support",
                                value: "contact",
                            }
                        },
                    }).then((value) => {
                        if(value == "contact"){
                            window.open("<?= $_SERVER['LOCAL_NL_URL']; ?>/console/?needhelp=yes");
                        }
                    });
                }

            });
        },
        createFieldsForLeads:function(data){

            var div = document.createElement('div');

            var h3 = document.createElement("h3");
            h3.setAttribute("style","text-align: left;font-size: 13px;");
            h3.innerHTML = "From phone number";

            div.appendChild(h3);

            // ============== (START) FROM PHONES ==============
            var fromPhone = document.createElement("select");
            fromPhone.id = "sendSMSModalFromPhoneNumberID";
            fromPhone.className = "form-control";
            fromPhone.style.marginBottom = "13px";
            fromPhone.setAttribute("onChange","sendSMScontroller.checkFields()");

            if(sendSMScontroller.fromPhones.length == 0){
                var noPhonesOption = document.createElement("option");
                noPhonesOption.innerHTML = "No phones";
                fromPhone.disabled = true;
                fromPhone.readonly = true;

                fromPhone.appendChild(noPhonesOption);
            }else{
                for(var i = 0;i<sendSMScontroller.fromPhones.length;i++){
                    var phonesOption = document.createElement("option");
                    phonesOption.value = sendSMScontroller.fromPhones[i].id;
                    phonesOption.innerHTML = sendSMScontroller.fromPhones[i].prettyNumber;

                    fromPhone.appendChild(phonesOption);
                }
            }
            div.appendChild(fromPhone);
            // ============== (END) FROM PHONES ==============

            // ============== (START) TO LEADS ==============
            var h3 = document.createElement("h3");
            h3.setAttribute("style","text-align: left;font-size: 13px;");
            h3.innerHTML = "To leads";

            div.appendChild(h3);

            var select = document.createElement('select');
            select.className = "form-control m-b";
            select.setAttribute("multiple",true);
            select.id = "sendSMSModalToLeads";
            select.setAttribute("style","margin-bottom: 13px;");

            for(var i = 0; i < data.length; i++) {
                var leadId = data[i].leadId;
                var jobId = data[i].jobNumber;
                var leadPhone = data[i].phone;

                var optionElement = document.createElement("option");
                optionElement.textContent = "#"+ jobId+" - "+leadPhone;
                optionElement.value = leadId;
                select.appendChild(optionElement);
            }

            div.appendChild(select);
            // ============== (END) TO LEADS ==============

            var h3 = document.createElement("h3");
            h3.setAttribute("style","text-align: left;font-size: 13px;");
            h3.innerHTML = "Message";

            div.appendChild(h3);

            var textarea = document.createElement('textarea');
            textarea.id = "sendSMSModalMessage";
            textarea.className = "form-control";
            textarea.setAttribute("style","text-align: left;font-size: 81%;padding: 11px;height: 100px;");
            textarea.setAttribute("placeholder","Enter text");
            textarea.setAttribute("onKeyUp","sendSMScontroller.checkFields()");
            textarea.innerHTML = sendSMScontroller.settings.content;

            div.appendChild(textarea);

            if(sendSMScontroller.fromPhones.length == 0) {
                var assignPhone = document.createElement("div");
                var assignPhoneA = document.createElement("a");
                assignPhoneA.target = "_blank";
                assignPhoneA.href = "<?= $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/mailSettings.php";
                assignPhoneA.innerHTML = "Click here to assign new phone number";
                assignPhone.appendChild(assignPhoneA);

                assignPhone.setAttribute("style","background-color: rgb(254, 250, 227);padding: 17px;border: 1px solid rgb(240, 225, 161);display: block;margin-top: 13px;text-align: center;color: rgb(97, 83, 78);");
                div.appendChild(assignPhone);
            }else{

                var tagsDiv = document.createElement("div");
                tagsDiv.setAttribute("style","text-align: left;float: left;margin-top: 13px;margin-bottom: 13px;");

                var tag = document.createElement("span");
                tag.className = "badge";
                tag.setAttribute("style","cursor:pointer;margin-right: 3px;");
                tag.innerHTML = "First Name";
                tag.setAttribute("onClick","sendSMScontroller.updateMessageArea('[FirstName]');");
                tagsDiv.appendChild(tag);

                var tag = document.createElement("span");
                tag.className = "badge";
                tag.setAttribute("style","cursor:pointer;margin-right: 3px;");
                tag.innerHTML = "Last Name";
                tag.setAttribute("onClick","sendSMScontroller.updateMessageArea('[LastName]');");
                tagsDiv.appendChild(tag);

                var tag = document.createElement("span");
                tag.className = "badge";
                tag.setAttribute("style","cursor:pointer;margin-right: 3px;");
                tag.innerHTML = "Update inventory link";
                tag.setAttribute("onClick","sendSMScontroller.updateMessageArea('[inventorylink]');");
                tagsDiv.appendChild(tag);

                var tag = document.createElement("span");
                tag.className = "badge";
                tag.setAttribute("style","cursor:pointer;margin-right: 3px;");
                tag.innerHTML = "From state";
                tag.setAttribute("onClick","sendSMScontroller.updateMessageArea('[FromState]');");
                tagsDiv.appendChild(tag);

                var tag = document.createElement("span");
                tag.className = "badge";
                tag.setAttribute("style","cursor:pointer;margin-right: 3px;");
                tag.innerHTML = "To state";
                tag.setAttribute("onClick","sendSMScontroller.updateMessageArea('[ToState]');");
                tagsDiv.appendChild(tag);


                div.appendChild(tagsDiv);
            }



            return div;
        },
        createFieldsForPhones:function(){

            var div = document.createElement('div');

            var h3 = document.createElement("h3");
            h3.setAttribute("style","text-align: left;font-size: 13px;");
            h3.innerHTML = "From phone number";

            div.appendChild(h3);

            // ============== (START) FROM PHONES ==============
            var fromPhone = document.createElement("select");
            fromPhone.id = "sendSMSModalFromPhoneNumberID";
            fromPhone.className = "form-control";
            fromPhone.style.marginBottom = "13px";
            fromPhone.setAttribute("onChange","sendSMScontroller.checkFields()");

            if(sendSMScontroller.fromPhones.length == 0){
                var noPhonesOption = document.createElement("option");
                noPhonesOption.innerHTML = "No phones";
                fromPhone.disabled = true;
                fromPhone.readonly = true;

                fromPhone.appendChild(noPhonesOption);
            }else{
                for(var i = 0;i<sendSMScontroller.fromPhones.length;i++){
                    var phonesOption = document.createElement("option");
                    phonesOption.value = sendSMScontroller.fromPhones[i].id;
                    phonesOption.innerHTML = sendSMScontroller.fromPhones[i].prettyNumber;

                    fromPhone.appendChild(phonesOption);
                }
            }
            div.appendChild(fromPhone);

            // ============== (END) FROM PHONES ==============

            // ============== (START) TO PHONES ==============
            var h3 = document.createElement("h3");
            h3.setAttribute("style","text-align: left;font-size: 13px;");
            h3.innerHTML = "To phone number";

            div.appendChild(h3);

            var input = document.createElement('input');
            input.className = "form-control";
            input.id = "sendSMSModalToPhoneNumber";
            input.setAttribute("style","margin-bottom: 13px;");
            input.setAttribute("placeholder","123-456-7890");
            input.setAttribute("onKeyUp","sendSMScontroller.checkFields()");

            div.appendChild(input);
            // ============== (END) TO PHONES ==============

            var h3 = document.createElement("h3");
            h3.setAttribute("style","text-align: left;font-size: 13px;");
            h3.innerHTML = "Message";

            div.appendChild(h3);

            var textarea = document.createElement('textarea');
            textarea.id = "sendSMSModalMessage";
            textarea.className = "form-control";
            textarea.setAttribute("style","text-align: left;font-size: 81%;padding: 11px;height: 100px;");
            textarea.setAttribute("placeholder","Enter text");
            textarea.setAttribute("onKeyUp","sendSMScontroller.checkFields()");
            textarea.innerHTML = sendSMScontroller.settings.content;

            div.appendChild(textarea);

            if(sendSMScontroller.fromPhones.length == 0) {
                var assignPhone = document.createElement("div");
                var assignPhoneA = document.createElement("a");
                assignPhoneA.target = "_blank";
                assignPhoneA.href = "<?= $_SERVER['LOCAL_NL_URL']; ?>/console/categories/mail/mailSettings.php";
                assignPhoneA.innerHTML = "Click here to assign new phone number";
                assignPhone.appendChild(assignPhoneA);

                assignPhone.setAttribute("style","background-color: rgb(254, 250, 227);padding: 17px;border: 1px solid rgb(240, 225, 161);display: block;margin-top: 13px;text-align: center;color: rgb(97, 83, 78);");
                div.appendChild(assignPhone);
            }

            return div;
        },
        checkFields:function () {

            var fromPhoneNumberID = document.getElementById("sendSMSModalFromPhoneNumberID").value;
            if(sendSMScontroller.settings.leadIds != null){
                var toInput = "true";
            }else{
                var toInput = document.getElementById("sendSMSModalToPhoneNumber").value;
            }
            var msg = document.getElementById("sendSMSModalMessage").value;

            if(fromPhoneNumberID != "" && toInput != "" && msg != ""){
                $(".swal-button").attr('disabled', false);
                return true;
            }else{
                $(".swal-button").attr('disabled', 'disabled');
                return false;
            }
            return false;
        },
        updateMessageArea: function(text) {
        var txtarea = document.getElementById("sendSMSModalMessage");
        if (!txtarea) {
            return;
        }

        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false));
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            strPos = range.text.length;
        } else if (br == "ff") {
            strPos = txtarea.selectionStart;
        }

        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            var ieRange = document.selection.createRange();
            ieRange.moveStart('character', -txtarea.value.length);
            ieRange.moveStart('character', strPos);
            ieRange.moveEnd('character', 0);
            ieRange.select();
        } else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }

        txtarea.scrollTop = scrollPos;
            sendSMScontroller.checkFields();
        }
    }
    */


    function openSendingStats(data) {
        var content = document.createElement("div");

        var iframe = document.createElement("iframe");
        iframe.src = BASE_URL+"/console/categories/iframes/system/sendingStats.php";


        iframe.setAttribute("style","border:none;width: 100%;height:600px");
        iframe.setAttribute("name","sendingStatsIframe");
        iframe.onload= function() {

<?php
            /*
            var data = [];
            var responseTypes = [];
            var responses = [];

            var singleResponseType = [];
            singleResponseType["name"] = "Sent";
            singleResponseType["color"] = "#28a745";

            responseTypes[singleResponseType["name"]] = (singleResponseType);


            var singleResponseType = [];
            singleResponseType["name"] = "Failed";
            singleResponseType["color"] = "#dc3545";

            responseTypes[singleResponseType["name"]] = (singleResponseType);

            var singleResponseType = [];
            singleResponseType["name"] = "Maybe";
            singleResponseType["color"] = "#2196f3";

            responseTypes[singleResponseType["name"]] = (singleResponseType);

            var response = {};
            response.data = "Data1";
            response.text = "More info 1";
            response.responseType = "Sent";
            responses.push(response);

            var response = {};
            response.data = "Data2";
            response.text = "More info 2";
            response.responseType = "Sent";
            responses.push(response);

            var response = {};
            response.data = "Data3";
            response.text = "More info 3";
            response.responseType = "Failed";
            responses.push(response);

            var response = {};
            response.data = "Data4";
            response.text = "More info 4";
            response.responseType = "Maybe";
            responses.push(response);

            data.responses = responses;
            data.responseTypes = responseTypes;
*/

            ?>
            window.frames['sendingStatsIframe'].init(data);
        };

        content.appendChild(iframe);

        swal({
            content: content,
            buttons:{
                confirm: true
            }
        });
    }

    function sendEmailsModal(leadsIds,toEmail,customMailSettings,callback) {


        var iframe = document.createElement("iframe");
        if(leadsIds == undefined){
            iframe.src = BASE_URL+"/console/categories/iframes/emailer.php";
        }else{
            iframe.src = BASE_URL+"/console/categories/iframes/emailer.php?leadsIds[]="+leadsIds.join('&leadsIds[]=');
        }
        iframe.setAttribute("style","border:none;width: 100%;height:100%;height:fill-available; height: -webkit-fill-available;");
        iframe.setAttribute("name","emailer-iframe");
        iframe.onload= function() {

            if(customMailSettings != undefined && customMailSettings != false){

                var type = customMailSettings[0];
                var leadId = customMailSettings[1];
                var estimateId = customMailSettings[2];
                var jobAcceptanceId = customMailSettings[3];
                var claimId = customMailSettings[4];

                window.frames['emailer-iframe'].emailerController.getCustomEmail(type,leadId,estimateId,jobAcceptanceId,claimId);
            }

            if(toEmail != undefined && toEmail != null  && toEmail != ""){
                window.frames['emailer-iframe'].emailerController.setToEmail(toEmail);
            }

            if(callback != undefined){
                window.frames['emailer-iframe'].emailerController.callbackFn = callback;
            }
        };


        swal({
            content: iframe,
            buttons: false,
            className: "swal-emails"
        });

    }


</script>
<!-- SEND SMS PLUGIN -->