<?php
if(isset($_POST['id'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadDataConfirmation.php");

    $leadId = $_POST['id'];

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);

    $leadDataConfirmation = new leadDataConfirmation(false);

    $updatedInfo = $leadDataConfirmation->getData($leadId);
    
    $res = $movingLead->updateInventory(serialize($updatedInfo['inventory']));

    echo json_encode($res);
}else{
    echo json_encode(false);
}