<?php

$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadLog.php");

$start = isset($_GET['start']) ? $_GET['start'] : 0;
$total = isset($_GET['total']) ? $_GET['total'] : 10;
$singleLogId = isset($_GET['singleLogId']) ? $_GET['singleLogId'] : false;

$leadLog = new leadLog();
if($singleLogId == false){
    // get all logs for user
    $data = $leadLog->getLogsForUser($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$start,$total);
}else{
    // get a specific single log for user
    $data = $leadLog->getLogsForUser($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],0,0,$singleLogId);
}

echo json_encode($data);