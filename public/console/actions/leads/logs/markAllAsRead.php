<?php

$pagePermissions = array(false, true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadLog.php");

$leadLog = new leadLog();
$setAllLogsAsSeen = $leadLog->setAllLogsAsSeen($bouncer["credentials"]["orgId"],$bouncer["credentials"]["userId"]);

echo json_encode($setAllLogsAsSeen);
