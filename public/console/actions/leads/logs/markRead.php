<?php

if(isset($_POST['id']) && isset($_POST['leadId'])) {
    $pagePermissions = array(false, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadLog.php");

    $id = $_POST['id'];
    $leadId = $_POST['leadId'];

    $leadLog = new leadLog();
    $setLogAsSeen = $leadLog->setLogAsSeen($bouncer["credentials"]["orgId"],$bouncer["credentials"]["userId"], $id, $leadId);

    echo json_encode($setLogAsSeen);
}