<?php
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/lead.php");

$resp = [];
$resp['status'] = false;
$resp['msg'] = "";

if ($bouncer['organizationData']['isNMpartner'] != 1){
    $resp['msg'] = "Not Partner";
    echo json_encode($resp);
    exit;
}
if (isset($_POST['leadId']) && isset($_POST['reason'])){

    $leadId = $_POST['leadId'];
    $reason = $_POST['reason'];
    if (isset($_POST['comments']) && $_POST['comments']) {
        $comments = $_POST['comments'];
    }else{
        $comments = "---";
    }

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);

    $isLeadWithinHours = $lead->isLeadWithinHours();

    if ($isLeadWithinHours['valid'] == true){
        $errorVar = array("lead Class","getData()",4,"Notes",array());

        $isLeadHasRefundRequest = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_partners_db.refundRequests WHERE leadId=".$leadId,NULL,$errorVar);

        if ($isLeadHasRefundRequest){
            if ($GLOBALS['connector']->fetch_num_rows($isLeadHasRefundRequest) != 0){
                // lead already exists
                $resp['msg'] = "Lead already have refund request";
                echo json_encode($resp);
                exit;
            }
        }else{
            // query error
            $resp['msg'] = "Temporary error, please try again later";
            echo json_encode($resp);
            exit;
        }




        if ($reason && $comments && $leadId){
            $finalReason = "[$reason] $comments";
            $requestRefund = $lead->requestRefund($finalReason);
            if ($requestRefund){
                $msg = $bouncer['userData']["fullName"]." [".$bouncer["organizationData"]["organizationName"]."] requested a refund";
                shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$msg,"env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

                $resp['status'] = true;
                $resp['msg'] = "Request Sent";
                echo json_encode($resp);
                exit;
            }else{
                $msg = $bouncer['userData']["fullName"]." [".$bouncer["organizationData"]["organizationName"]."] failed to request a refund";
                shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$msg,"env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

                $resp['msg'] = "Request error, please try again later";
                echo json_encode($resp);
                exit;
            }
        }else{
            $resp['msg'] = "Request missing fields";
            echo json_encode($resp);
            exit;
        }
    }else{

        $resp['msg'] = "Lead date is over 72 hours, refund requests cannot be applied after that time";
        echo json_encode($resp);
        exit;
    }

}else{
    $resp['msg'] = "missing fields, all fields are required";
    echo json_encode($resp);
    exit;
}