<?php
if(isset($_GET['query'])){

    $pagePermissions = array(false,true);

    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

    $input = $_GET['input'];
    $query = $_GET['query'];

    $errorVar = array("","Type","Desc","Notes",array(),true);

    $results = array();

    if ($input == "city") {
        // =========== COMPLETE CITY ===========
        $cityResults = array();
        $allZips = array();

        $getIt = $GLOBALS['connector']->execute("SELECT State,City,ZipCode FROM networkleads_moving_db.ZIPCodes WHERE City LIKE '" . $query . "%'", NULL, $errorVar);
        if (!$getIt) {
            echo json_encode(false);
            exit;
        } else {
            while ($r = $GLOBALS['connector']->fetch($getIt)) {

                if (!array_key_exists($r["ZipCode"], $allZips) || (isset($allZips[$r["ZipCode"]]) && $allZips[$r["ZipCode"]] != $r["State"])) {
                    $allZips[$r["ZipCode"]] = $r["State"];

                    $singleResult = array();
                    $singleResult["value"] = $r["ZipCode"] . ", " . ucfirst(strtolower($r["City"])) . ", " . $r["State"];
                    $singleResult["data"] = array("city" => ucfirst(strtolower($r["City"])), "state" => $r["State"], "zip" => $r["ZipCode"]);

                    $cityResults[] = $singleResult;
                }
            }
        }

        $results = $cityResults;
        // =========== COMPLETE CITY ===========
    }

    if ($input == "state") {
        // =========== COMPLETE STATE ===========
        $stateResults = array();
        $allStates = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District Of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        );

        foreach($allStates as $state=>$fullState){

            if (strpos(strtolower($fullState), strtolower($query)) !== false) {

                $singleResult = array();
                $singleResult["value"] = ucfirst(strtolower($fullState));
                $singleResult["data"] = array("city" => "", "state" => strtoupper($state), "zip" => "");

                $stateResults[] = $singleResult;


            }
        }

        $results = $stateResults;
        // =========== COMPLETE CITY ===========
    }

    if ($input == "zip") {
        // =========== COMPLETE ZIP ===========
        $zipcodesResults = array();
        $allZipcodes = array();

        $getIt = $GLOBALS['connector']->execute("SELECT State,City,ZipCode FROM networkleads_moving_db.ZIPCodes WHERE ZipCode LIKE '" . $query . "%'", NULL, $errorVar);

        if (!$getIt) {
            echo json_encode(false);
            exit;
        } else {
            while ($r = $GLOBALS['connector']->fetch($getIt)) {

                if (!array_key_exists($r["ZipCode"], $allZipcodes) || (isset($allZipcodes[$r["ZipCode"]]) && $allZipcodes[$r["ZipCode"]] != $r["State"])) {
                    $allZipcodes[$r["ZipCode"]] = $r["State"];

                    $singleResult = array();
                    $singleResult["value"] = $r["ZipCode"] . ", " . ucfirst(strtolower($r["City"])) . ", " . $r["State"];
                    $singleResult["data"] = array("city" => ucfirst(strtolower($r["City"])), "state" => $r["State"], "zip" => ucfirst(strtolower($r["ZipCode"])));

                    $zipcodesResults[] = $singleResult;
                }
            }
        }
        $results = $zipcodesResults;
        // =========== COMPLETE ZIP ===========
    }

    echo json_encode(array("suggestions"=>$results));
}