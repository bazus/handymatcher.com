<?php
$pagePermissions = array(false,array(1),true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadIframe.php");

$leadIframe = new leadIframe($bouncer["credentials"]["orgId"]);
$getIframeData = $leadIframe->getIframeData(NULL,true);

echo json_encode($getIframeData);
