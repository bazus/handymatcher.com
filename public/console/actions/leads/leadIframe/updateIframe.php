<?php
if(isset($_POST['id'])){

    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadIframe.php");

    $id = $_POST['id'];

    $leadIframe = new leadIframe($bouncer["credentials"]["orgId"]);
    $updateIframe = $leadIframe->updateIframe($id);

    echo json_encode($updateIframe);
}
