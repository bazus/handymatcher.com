<?php
if(isset($_POST['iframeId'])){
    
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadIframe.php");

    $leadIframe = new leadIframe($bouncer["credentials"]["orgId"]);
    $res = $leadIframe->clearIframeImage($_POST['iframeId']);

    echo json_encode($res);
}else{
    echo json_encode(false);
}