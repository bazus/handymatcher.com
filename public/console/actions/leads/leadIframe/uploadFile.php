<?php
$pagePermissions = array(false,true,false,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

$s3 = new s3();

$final_target_file = "";
$uploadOk = 1;
$collectData = [];
try {
    if (intval($_SERVER['CONTENT_LENGTH']) > 0 && count($_POST) === 0) {
        $collectData['error'] = ["Sorry, your file was not uploaded."];
        $collectData['success'] = false;
        echo json_encode($collectData);
        exit;
    } else {
        if ($_FILES[0]["tmp_name"] != "") {

            $thisDate = date("Y.m.d.H-i-s", strtotime("now"));

            $filePathInfo = pathinfo($_FILES[0]["name"]);
            $newFileName = $bouncer["credentials"]["userId"] . "." . hash("md5", $filePathInfo["filename"]) . $thisDate . "." . $filePathInfo["extension"];

            $target_dir = $_SERVER['LOCAL_NL_PATH'] . "/console/files/leads/";
            $target_file = $target_dir . $newFileName;
            $final_target_file = $_SERVER['LOCAL_NL_PATH'] . "/console/files/leads/" . $newFileName;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

            // max file size in MB
            $maxFileSize = 10;
            if ($_FILES[0]['size'] > ($maxFileSize * 1024 * 1024)) {
                $collectData['error'] = ["Sorry, file Size Should Be Below $maxFileSize" . "MB."];
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG"
                && $imageFileType != "gif" && $imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "xls"
                && $imageFileType != "xlsx" && $imageFileType != "mp3" && $imageFileType != "wav" && $imageFileType != "numbers" && $imageFileType != "NUMBERS" && $imageFileType != "pages" && $imageFileType != "PAGES") {

                $collectData['error'] = ["Sorry, only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed."];
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                $collectData['success'] = false;
                echo json_encode($collectData);
                exit;
                // if everything is ok, try to upload file
            } else {
                $fileType = false;
                if ($imageFileType == "jpg" || $imageFileType == "JPG" || $imageFileType == "jpeg" || $imageFileType == "JPEG") {
                    $fileType = "image/jpeg";
                }
                if ($imageFileType == "png" || $imageFileType == "PNG") {
                    $fileType = "image/png";
                }


                $file_name = $newFileName;
                $temp_file_location = $_FILES[0]['tmp_name'];
                $result = $s3->putObject("thenetworkleads", "iframeBanners/" . $file_name, $temp_file_location, "public-read", $fileType);
                if ($result->get("@metadata")['statusCode'] == 200) {
                    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadIframe.php");

                    $leadIframe = new leadIframe($bouncer["credentials"]["orgId"]);
                    $res = $leadIframe->addFile($_POST['iframeId'], $result->get("@metadata")['effectiveUri']);
                    $collectData['success'] = true;
                    echo json_encode($collectData);
                    if ($res == true) {
                        exit;
                    } else {
                        $collectData['error'] = ["Error - Please Contact Support With 'Error 901'"];
                        $collectData['success'] = false;
                        echo json_encode($collectData);
                        exit;
                    }
                } else {
                    $collectData['error'] = ["Sorry, there was an error uploading your file."];
                    $collectData['success'] = false;
                    echo json_encode($collectData);
                    exit;
                }
            }
        }
    }
}catch (Exception $e){
    unset($e);
    $collectData['error'] = ["Sorry, there was an error uploading your file."];
    $collectData['success'] = false;
    echo json_encode($collectData);
    exit;
}
