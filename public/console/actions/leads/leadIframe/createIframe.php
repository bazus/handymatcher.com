<?php
if(isset($_POST['name'])){
    
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadIframe.php");

    $name = $_POST['name'];

    $leadIframe = new leadIframe($bouncer["credentials"]["orgId"]);
    $createIframe = $leadIframe->createIframe($name);

    echo json_encode($createIframe);
}
    