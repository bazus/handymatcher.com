<?php
if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}

require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

$client = new Client(new Version2X('https://chat.thenetworkleads.com'));
// open connection
$client->initialize();

if(isset($_POST['leadId']) && isset($_POST['userId'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");

    $leadId = $_POST['leadId'];
    $userId = $_POST['userId'];
    if (!$userId){
        $userId = NULL;
    }
    $leads = new leads($bouncer["credentials"]["orgId"]);
    $assignUser = $leads->assignUser($leadId,$userId);

    $result = array();
    $result["users"] = array($userId);
    $result["leadId"] = $leadId;

    if($userId != $bouncer["credentials"]["userId"]) {
        $client->emit('newLeadArrived', $result);
    }

    echo json_encode($assignUser);
}else{
    echo json_encode(false);
}