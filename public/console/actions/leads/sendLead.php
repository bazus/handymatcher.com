<?php
if(isset($_POST['data'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");

    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);

    $data = $_POST['data'];
    if ($data['firstname'] == "" || $data['phone'] == ""){
        echo json_encode(false);
    }else{
        $data['firstname'] = ucwords($data['firstname']);
        $data['lastname'] = ucwords($data['lastname']);
        $sendLead = false;
        if($leadProviders->isProviderFromOrganization($data['providerId']) || $data['providerId'] == "0"){
            $leads = new leads($bouncer["credentials"]["orgId"]);
            if (isset($_POST['movingData']) && $_POST['movingData']){
                $movingData = $_POST['movingData'];
                if (!$movingData['moveSize']){
                    $movingData['moveSize'] = "";
                }
                if (!$movingData['moveSizeLBS']){
                    $movingData['moveSizeLBS'] = 0;
                }
                if (!$movingData['moveDate']){
                    $movingData['moveDate'] = date("Y-m-d",strtotime("NOW"));
                }

                $data['userId'] = $bouncer["credentials"]["userId"];
                $sendLead = $leads->sendLead($data,true,true,$movingData);

            }else{
                $data['userId'] = $bouncer["credentials"]["userId"];
                $sendLead = $leads->sendLead($data,true);
            }
        }

        echo json_encode($sendLead);
    }
}else{
    echo json_encode(false);
}