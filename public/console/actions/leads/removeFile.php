<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 20/09/2018
 * Time: 21:33
 */
if(isset($_POST['fileId']) && isset($_POST['leadId'])){

    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $lead = new lead($_POST['leadId'],$bouncer["credentials"]["orgId"]);
    $fileId = $_POST['fileId'];

    $removeFile = $lead->removeFile($fileId,$bouncer["isUserAnAdmin"],$bouncer["credentials"]["userId"]);

    echo json_encode($removeFile);

}else{
    echo json_encode(false);
}
