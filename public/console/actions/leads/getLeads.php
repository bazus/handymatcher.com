<?php

if(isset($_POST['startDate']) && isset($_POST['endDate'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

    $sDate = $_POST['startDate'];
    $eDate = $_POST['endDate'];

    if (isset($_POST['page']) && $_POST['page'] >= 0){
        $page = $_POST['page'];
    }else{
        $page = 0;
    }

     if (isset($_POST['getLeadsBy']) && $_POST['getLeadsBy'] == "1"){
         $getLeadsByMovingDate = true;
     }else{
         $getLeadsByMovingDate = false;
     }

     // get the leads of this user is (if null, then it is an admin so get all the leads)
     $userIdLeads = NULL;
    if ($bouncer['isUserAnAdmin'] != true) {
        $userIdLeads = $bouncer["credentials"]["userId"];
    }

    $leads = new leads($bouncer["credentials"]["orgId"],$bouncer["credentials"]["userId"]);
    $leadsData = $leads->getLeadsListForLeadsPage($sDate,$eDate,$userIdLeads,$page,$getLeadsByMovingDate);

    echo json_encode($leadsData);
}else{
    echo json_encode(false);
}