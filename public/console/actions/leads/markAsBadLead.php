<?php

if(isset($_POST['leadId']) && isset($_POST['isBadLead']) && isset($_POST['badLeadText'])){

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['leadId'];

    $badLeadText = serialize($_POST['badLeadText']);

    $isBadLead = $_POST['isBadLead'];

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $setAsBadLead = $lead->setAsBadLead($isBadLead,$badLeadText);

    echo json_decode($setAsBadLead);

}else{
    echo json_encode(false);
}
