<?php
if(isset($_POST['leadsIds'])) {

    $pagePermissions = array(false, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");

    $leadsIds = $_POST['leadsIds'];

    $leads = new leads($bouncer["credentials"]["orgId"]);
    $phoneLeadPhoneNumbers = $leads->getLeadsPhoneNumber($leadsIds);

    echo json_encode($phoneLeadPhoneNumbers);
}else{
    echo json_encode(false);
}