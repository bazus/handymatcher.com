<?php

if(isset($_POST['leadId']) && isset($_POST['materialId'])){

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

    $leadId = $_POST['leadId'];
    $materialId = $_POST['materialId'];

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    $updateMaterials = $movingLead->removeMaterial($materialId);

    echo json_encode($updateMaterials);

}else{
    echo json_encode(false);
}
