<?php
if(isset($_POST['logText']) && $_POST['logText'] != "" && isset($_POST['leadId'])){

    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadLog.php");

    $logText = $_POST['logText'];
    $leadId = $_POST['leadId'];

    $leadLog = new leadLog();
    $customText = $leadLog->addLog($leadId, $logText,$bouncer["credentials"]["userId"],true);

    echo json_encode($customText);
}else{
    echo json_encode(false);
}