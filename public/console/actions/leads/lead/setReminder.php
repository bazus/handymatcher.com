<?php

if(isset($_POST['reminderId']) && isset($_POST['leadId']) && isset($_POST['subject']) && isset($_POST['dateStart']) && isset($_POST['dateEnd']) && isset($_POST['reminder']) && isset($_POST['reminder2']) && isset($_POST['color'])){
    $pagePermissions = array(false,array(1),true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadReminders.php");

    $reminderId = $_POST['reminderId'];
    $leadId = $_POST['leadId'];
    $subject = $_POST['subject'];
    $dateStart = date("Y-m-d H:i:s",strtotime($_POST['dateStart']));
    $dateEnd = date("Y-m-d H:i:s",strtotime($_POST['dateEnd']));
    $reminder = $_POST['reminder'];
    $reminder2 = $_POST['reminder2'];
    $color = $_POST['color'];
    $user = $bouncer["credentials"]["userId"];

    // check if admin assign the reminder to other user
    if ($bouncer['isUserAnAdmin'] == true && isset($_POST['user'])) {
        $user = $_POST['user'];
    }

    // check reminders
    if ($reminder != ""){
        $reminder = $reminder*60;
    }else{$reminder = NULL;}

    if ($reminder2 != ""){
        $reminder2 = $reminder2*60;
    }else{$reminder2 = NULL;}


    $leadReminders = new leadReminders($leadId,$bouncer["credentials"]["orgId"]);
    $setReminder = $leadReminders->setReminder($reminderId,$subject,$dateStart,$dateEnd,$reminder,$reminder2,$color,$user);
    echo json_encode($setReminder);
}else{
    echo json_encode(false);
}