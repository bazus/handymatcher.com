<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if(isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");

    $leadId = $_POST['leadId'];

    $leadData = [];

    // get lead payments
    $payments = new payments($bouncer["credentials"]['orgId']);
    $data = $payments->getPaymentsByLeads($leadId);

    $data["totalPayments"] = number_format($data["totalPayments"],2, '.', ',');
    $data["ccaf"] = $payments->getCreditCardAuthorizationByLeads($leadId);

    echo json_encode($data);
}else{
    echo json_encode(false);
}