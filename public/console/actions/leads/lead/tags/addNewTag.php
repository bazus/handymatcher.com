<?php
if(isset($_POST['name']) && isset($_POST['color'])){
    
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadTags.php");

    $name = $_POST['name'];
    $color = $_POST['color'];

    $leadTags = new leadTags($bouncer["credentials"]["orgId"]);
    $addNewTag = $leadTags->addNewTag($name,$color);

    echo json_encode($addNewTag);
}
    