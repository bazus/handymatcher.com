<?php
if(isset($_POST['tagId'])){

    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadTags.php");

    $tagId = $_POST['tagId'];

    $leadTags = new leadTags($bouncer["credentials"]["orgId"]);
    $deleteTag = $leadTags->deleteTag($tagId);

    echo json_encode($deleteTag);
}
    