<?php
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadTags.php");

    $leadTags = new leadTags($bouncer["credentials"]["orgId"]);
    $leadTagsData = $leadTags->getData();

    echo json_encode($leadTagsData);
