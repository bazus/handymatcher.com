<?php
if(isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['leadId'];
    if (isset($_POST['tags'])){
        $tags = $_POST['tags'];
    }else{
        $tags = NULL;
    }

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    if (count($tags) > 0){
        $updateTag = $lead->updateTag($tags);
    }else{
        $updateTag = $lead->updateTag(NULL);
    }


    echo json_encode($updateTag);
}else{
    echo json_encode(false);
}