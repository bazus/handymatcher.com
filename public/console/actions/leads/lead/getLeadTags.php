<?php
if(isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadTags.php");

    $leadId = $_POST['leadId'];

    $leadTags = new leadTags($bouncer["credentials"]["orgId"]);
    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);

    $activeTags = [];
    $getLeadTags = $lead->getLeadTags();

    if ($getLeadTags) {
        foreach ($getLeadTags as $tag) {
            $tagData = $leadTags->getDataById($tag);
            if($tagData != false) {
                $activeTags[] = $tagData;
            }
        }
        if(count($activeTags) > 0) {
            echo json_encode($activeTags);
        }else{
            echo json_encode(false);
        }
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}