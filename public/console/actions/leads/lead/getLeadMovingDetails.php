<?php
if(isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/operations.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/fullvalueprotection.php");

    $organization = new organization($bouncer["credentials"]["orgId"]);

    $leadId = $_POST['leadId'];
    $totalCF = 0;
    $data = [];
    $movingLead = new movingLead($leadId, $bouncer["credentials"]["orgId"]);
    
    $operations = new operations($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"],$leadId);

    // Get Departments
    $data['departments'] = $organization->getDepartments();

    // Get Lead Moving Data
    $data['movingLeadData'] = $movingLead->getData();

    // Get Lead Moving Trucks Assigned To Lead
    $data['movingLeadData']["operations"]["trucksAssigned"] = $operations->getTrucksAssigned();

    // Get Lead Moving Carriers Assigned To Lead
    $data['movingLeadData']["operations"]["carriersAssigned"] = $operations->getCarriersAssigned();

    // Get Lead Moving Crew Assigned To Lead
    $data['movingLeadData']["operations"]["crewAssigned"] = $operations->getCrewAssigned();

    // format the moveDate
    $data['movingLeadData']['moveDateFormated'] = date("Y-m-d",strtotime($data['movingLeadData']['moveDate']));
    $data['movingLeadData']['moveDate'] = date("F jS, Y",strtotime($data['movingLeadData']['moveDate']));
    // unserialize autoTranposrt Data
    $data['movingLeadData']['autoTransport'] = unserialize($data['movingLeadData']['autoTransport']);

    $fullvalueprotection = new fullvalueprotection($bouncer["credentials"]["orgId"]);
    $data['fullvalueprotection'] = $fullvalueprotection->getData();

    $data['estimates'] = $movingLead->getEstimates();

    $movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
    $data['movingSettingsData'] = $movingSettings->getData();

    if($data['movingSettingsData']["cflbsratio"] == false){
        $data['movingLeadData']["moveSizeCF"] = $data['movingLeadData']["moveSizeLBS"] / 7;
        $data['movingLeadData']["moveSizeCF"] = number_format((float)$data['movingLeadData']["moveSizeCF"], 2, '.', '');
    }else {
        $data['movingLeadData']["moveSizeCF"] = $data['movingLeadData']["moveSizeLBS"] / $data['movingSettingsData']["cflbsratio"];
        $data['movingLeadData']["moveSizeCF"] = number_format((float)$data['movingLeadData']["moveSizeCF"], 2, '.', '');
    }

    // format dates for box,pickup&delivery date
    if ($data['movingLeadData']['boxDeliveryDateStart'] != null){
        $data['movingLeadData']['boxDeliveryDateStart'] = date("Y/m/d H:i:s",strtotime($data['movingLeadData']['boxDeliveryDateStart']));
    }else{
        $data['movingLeadData']['boxDeliveryDateStart'] = date("Y/m/d H:i:s",strtotime("NOW"));
    }

    if ($data['movingLeadData']['boxDeliveryDateEnd'] != null){
        $data['movingLeadData']['boxDeliveryDateEnd'] = date("Y/m/d H:i:s",strtotime($data['movingLeadData']['boxDeliveryDateEnd']));
    }else{
        $data['movingLeadData']['boxDeliveryDateEnd'] = date("Y/m/d H:i:s",strtotime("NOW"));
    }

    if ($data['movingLeadData']['pickupDateStart'] != null){
        $data['movingLeadData']['pickupDateStart'] = date("Y/m/d H:i:s",strtotime($data['movingLeadData']['pickupDateStart']));
    }else{
        $data['movingLeadData']['pickupDateStart'] = date("Y/m/d H:i:s",strtotime("NOW"));
    }

    if ($data['movingLeadData']['pickupDateEnd'] != null){
        $data['movingLeadData']['pickupDateEnd'] = date("Y/m/d H:i:s",strtotime($data['movingLeadData']['pickupDateEnd']));
    }else{
        $data['movingLeadData']['pickupDateEnd'] = date("Y/m/d H:i:s",strtotime("NOW"));
    }

    if ($data['movingLeadData']['requestedDeliveryDateStart'] != null){
        $data['movingLeadData']['requestedDeliveryDateStart'] = date("Y/m/d H:i:s",strtotime($data['movingLeadData']['requestedDeliveryDateStart']));
    }else{
        $data['movingLeadData']['requestedDeliveryDateStart'] = date("Y/m/d H:i:s",strtotime("NOW"));
    }

    if ($data['movingLeadData']['requestedDeliveryDateEnd'] != null){
        $data['movingLeadData']['requestedDeliveryDateEnd'] = date("Y/m/d H:i:s",strtotime($data['movingLeadData']['requestedDeliveryDateEnd']));
    }else{
        $data['movingLeadData']['requestedDeliveryDateEnd'] = date("Y/m/d H:i:s",strtotime("NOW"));
    }

    $inventory = new inventory($bouncer["credentials"]["orgId"]);

    if($data['movingLeadData'] != false) {

        $data['estimateItems'] = [];

        if($data['movingLeadData']['moveInventory'] != NULL){
            $data['movingLeadData']['moveInventory'] = unserialize($data['movingLeadData']['moveInventory']);
        }
        if (isset($data['movingLeadData']['moveInventory']) && $data['movingLeadData']['moveInventory'] !== false && $data['movingLeadData']['moveInventory'] !== "" && count($data['movingLeadData']['moveInventory']) > 0) {
            $x = rand(100000000,999999999999);
            foreach ($data['movingLeadData']['moveInventory'] as $item) {
                if (!$item['id']){

                    //single item
                    $singleItem = array();
                    if ($item['id']){
                        $singleItem["id"] = $item['id'];
                    }else{
                        $singleItem["id"] = "$x";
                        $x++;
                    }

                    $singleItem["name"] = $item['name'];
                    $singleItem["cf"] = $item['cf'];
                    $singleItem["amount"] = $item['amount'];
                    if (isset($item['showCF'])){
                        $singleItem["showCF"] = $item['showCF'];
                    }else{
                        $item['showCF'] = "true";
                        $singleItem["showCF"] = "true";
                    }

                    $data['estimateItems'][] = $singleItem;
                    if ($item['showCF'] == "true") {
                        $totalCF = $totalCF + ($item['cf'] * $item['amount']);
                    }
                }else{
                    $itemData = $inventory->getEstimateItem($item['id']);

                    $singleItem = array();
                    $singleItem["id"] = $item['id'];
                    if ($itemData['title']) {
                        $singleItem["name"] = $itemData['title'];
                    }else{
                        $singleItem["name"] = $item['name'];
                    }
                    $singleItem["cf"] = $item['cf'];
                    $singleItem["amount"] = $item['amount'];
                    if (isset($item['showCF'])) {
                        $singleItem["showCF"] = $item['showCF'];
                    }else{
                        $singleItem["showCF"] = "true";
                    }

                    $data['estimateItems'][] = $singleItem;
                    if ($item['cf']) {
                        $totalCF = $totalCF + ($item['cf'] * $item['amount']);
                    }
                }
            }
        }
    }

    $data['totalCF'] = $totalCF;

    try{
        $inventory = new inventory($bouncer["credentials"]["orgId"]);
        $data['inventory'] = $inventory->getGroups();

        foreach ($data['inventory'] as &$inventorys){
            $inventorys['items'] = $inventory->getItems($inventorys['id']);
        }
    }catch (Exception $e){
        unset($e);
        $data['inventory'] = false;
    }

    $materials = new materials($bouncer["credentials"]["orgId"]);
    $data['materials'] = $materials->getMaterials();

    if(isset($data['movingLeadData']['moveMaterials']) && $data['movingLeadData']['moveMaterials'] != NULL){

        $data['movingLeadData']['moveMaterials'] = unserialize($data['movingLeadData']['moveMaterials']);

        if ($data['movingLeadData']['moveMaterials'] && $data['movingLeadData']['moveMaterials'] != "false") {

            foreach ($data['movingLeadData']['moveMaterials'] AS &$moveMaterial) {
                $materialData = $materials->getMaterial($moveMaterial['id'], true);
                $moveMaterial['name'] = $materialData['title'];
            }

        }else{
            $data['movingLeadData']['moveMaterials'] = false;
        }
    }



    $data['canRefund'] = false;
    $data['hasRefund'] = false;

    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);
    $leadData = $lead->getData();

    $leadIsWithin72Hours = $lead->isLeadWithinHours();

    if ($leadData['isNM'] == 1){ // lead is NM
        if ($bouncer['organizationData']['isNMpartner'] == 1){ // organization is NM
            if ($leadIsWithin72Hours['valid'] == true){ // lead is within 72 hours

                $data['canRefund'] = true;

                $errorVar = array("lead Class","getData()",4,"Notes",array());
                $isLeadHasRefundRequest = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_partners_db.refundRequests WHERE leadId=".$leadId,NULL,$errorVar);
                if ($isLeadHasRefundRequest) {
                    // check if lead already have request
                    if ($GLOBALS['connector']->fetch_num_rows($isLeadHasRefundRequest) == 1) {
                        $data['hasRefund'] = true;
                    }
                }

            }
        }
    }

    echo json_encode($data);
}else{
    echo json_encode(false);
}