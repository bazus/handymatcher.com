<?php
$resp = [];
$resp["status"] = false;
$resp["files"] = NULL;
$resp["error"] = NULL;

if(isset($_FILES["estimateFile"]) && count($_FILES["estimateFile"]) > 0) {

    $pagePermissions = array(false, [1], true, true, [2]);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require $_SERVER['LOCAL_NL_PATH'] . '/console/services/aws/aws-autoloader.php';
    require_once $_SERVER['LOCAL_NL_PATH'] . "/console/classes/files/s3.php";

    $s3 = new s3();

    $final_target_file = "";
    $uploadOk = 1;
    $collectData = [];
    try {

            if ($_FILES["estimateFile"]["tmp_name"] != "") {

                $thisDate = date("Y.m.d.H-i-s", strtotime("now"));

                $filePathInfo = pathinfo($_FILES["estimateFile"]["name"]);

                if($filePathInfo["extension"] == null){
                    $resp["error"] = 1;//"No extension";
                    echo json_encode($resp);
                    exit;
                }
                $newFileName = $bouncer["credentials"]["userId"] . "." . hash("md5", $filePathInfo["filename"]) . $thisDate . "." . $filePathInfo["extension"];

                $target_dir = $_SERVER['LOCAL_NL_PATH'] . "/console/files/leads/";
                $target_file = $target_dir . $newFileName;
                $final_target_file = $_SERVER['LOCAL_NL_PATH'] . "/console/files/leads/" . $newFileName;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

                // max file size in MB
                $maxFileSize = 30;
                if ($_FILES["estimateFile"]['size'] > ($maxFileSize * 1024 * 1024)) {
                    $resp["error"] = 2;//"Sorry, file size too big";
                    echo json_encode($resp);
                    exit;
                }
                if ($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG"
                    && $imageFileType != "gif" && $imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "xls"
                    && $imageFileType != "xlsx" && $imageFileType != "mp3" && $imageFileType != "wav" && $imageFileType != "numbers" && $imageFileType != "NUMBERS" && $imageFileType != "pages" && $imageFileType != "PAGES") {

                    $resp["error"] = 3;//"Sorry, only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed";
                    echo json_encode($resp);
                    exit;
                }
                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    $resp["error"] = 4;//"Couldn't upload file";
                    echo json_encode($resp);
                    exit;
                } else {
                    // if everything is ok, try to upload file
                    $fileType = false;
                    if ($imageFileType == "jpg" || $imageFileType == "JPG" || $imageFileType == "jpeg" || $imageFileType == "JPEG") {
                        $fileType = "image/jpeg";
                    }
                    if ($imageFileType == "png" || $imageFileType == "PNG") {
                        $fileType = "image/png";
                    }


                    $fileKey = "estimatesFiles/".$filePathInfo["filename"].".".time().".".$filePathInfo["extension"];

                    $file_name = $newFileName;
                    $temp_file_location = $_FILES["estimateFile"]['tmp_name'];

                    $result = $s3->putObject("thenetworkleads", $fileKey, $temp_file_location, "private", $fileType);
                    if ($result->get("@metadata")['statusCode'] == 200) {

                        $fileName = $filePathInfo["filename"] . "." . $filePathInfo["extension"];
                        $fileURL = $result->get("@metadata")['effectiveUri'];

                        $resp["status"] = true;

                        $singleFile = [];
                        $singleFile["fileName"] = $fileName;
                        $singleFile["fileKey"] = $fileKey;
                        $singleFile["fileURL"] = $fileURL;

                        $resp["files"][] = $singleFile;


                        echo json_encode($resp);
                        exit;

                    } else {
                        $resp["error"] = 5;//"Couldn't upload file";
                        echo json_encode($resp);
                        exit;
                    }

                }
            }

    } catch (Exception $e) {
        var_dump($e);
        $resp["error"] = 6;//"Couldn't upload file";
        echo json_encode($resp);
        exit;
    }
}else{
    $resp["error"] = 7;//"Couldn't upload file";
    echo json_encode($resp);
    exit;
}