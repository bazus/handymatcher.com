<?php

if(isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadDataConfirmation.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");

    $organization = new organization($bouncer["credentials"]["orgId"]);

    $leadId = $_POST['leadId'];
    $totalCF = 0;

    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);

    // get Lead Data
    $leadData = [];
    $leadData['leadData'] = $lead->getData();

    $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $isEmailUnsibscribed = $email->isEmailUnsibscribed($leadData['leadData']["email"]);

    $leadData["isEmailUnsibscribed"] = $isEmailUnsibscribed;

    // check if lead is from user org
    if ($lead->isLeadFromOrg() == false){
        header("Location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/leads/leads.php");
        exit;
    }

    // Get Users for 'user assigned'
    $usersToAssign = $organization->getUsersOfMyCompany();
    foreach ($usersToAssign AS &$user){
        if($user['id'] == $bouncer['credentials']["userId"]){
            $user["fullName"] = "Me";
        }
    }
    $leadData['usersToAssign'] = $usersToAssign;

    // get client updated details (email link)
    $leadDataConfirmation = new leadDataConfirmation(false);
    $leadData['updatedInfo'] = $leadDataConfirmation->getData($leadId);

    echo json_encode($leadData);
}else{
    echo json_encode(false);
}