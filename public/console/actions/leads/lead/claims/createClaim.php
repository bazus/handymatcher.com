<?php

if(isset($_POST['leadId'])) {
    $pagePermissions = array(false,array(1));

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadClaims.php");

    require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/uploadFile.php";


    function reArrayFiles($file)
    {
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);

        for($i=0;$i<$file_count;$i++)
        {
            foreach($file_key as $val)
            {
                $file_ary[$i][$val] = $file[$val][$i];
            }
        }
        return $file_ary;
    }

    $files = [];
    if(isset($_FILES) && count($_FILES) > 0){
        $files = reArrayFiles($_FILES['claimFiles']);
    }

    $leadId = $_POST['leadId'];
    $address = "";
    $city = "";
    $state = "";
    $zip = "";
    $firstName = "";
    $lastName = "";
    $email = "";
    $phone1 = "";
    $phone2 = "";
    $details = "";
    $amount = 0;

    if(isset($_POST['address'])){$address = $_POST['address'];}
    if(isset($_POST['city'])){$city = $_POST['city'];}
    if(isset($_POST['state'])){$state = $_POST['state'];}
    if(isset($_POST['zip'])){$zip = $_POST['zip'];}
    if(isset($_POST['firstName'])){$firstName = $_POST['firstName'];}
    if(isset($_POST['lastName'])){$lastName = $_POST['lastName'];}
    if(isset($_POST['email'])){$email = $_POST['email'];}
    if(isset($_POST['phone1'])){$phone1 = $_POST['phone1'];}
    if(isset($_POST['phone2'])){$phone2 = $_POST['phone2'];}
    if(isset($_POST['details'])){$details = $_POST['details'];}
    if(isset($_POST['amount'])){$amount = $_POST['amount'];}

    $resp = [];
    $resp["status"] = false;
    $resp["reason"] = "";

    $filesAttached = [];

    foreach($files as $file){
        $uploadFile = new uploadFile();
        $upload = $uploadFile->upload($file,"thenetworkleads","leadClaims");
        if(!$upload["status"]) {
            $resp["reason"] = $upload["reason"];
            echo json_encode($resp);
            exit;
        }else{
            $filesAttached[] = $upload["file"];
        }
    }

    $leadClaims = new leadClaims($bouncer["credentials"]["orgId"],$leadId);
    $createClaim = $leadClaims->createClaim($address,$city,$state,$zip,$firstName,$lastName,$email,$phone1,$phone2,$details,$amount,$filesAttached);

    if($createClaim){
        $resp["status"] = true;
        echo json_encode($resp);
        exit;
    }else{
        $resp["status"] = false;
        $resp["reason"] = "Failed creating claim";
        echo json_encode($resp);
        exit;
    }


}else{
    var_dump("[b]");
    echo json_encode(false);
    exit;
}