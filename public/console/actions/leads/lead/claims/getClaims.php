<?php

if(isset($_GET['leadId'])) {
    $pagePermissions = array(false, true, true, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadClaims.php");

    $leadId = $_GET['leadId'];

    $leadClaims = new leadClaims($bouncer["credentials"]["orgId"], $leadId);
    $claimsForLead = $leadClaims->getClaimsForLead();

    echo json_encode($claimsForLead);
    exit;
}else{
    echo json_encode(false);
    exit;
}