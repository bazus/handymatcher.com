<?php

if(isset($_POST['claimsIds']) && $_POST['leadId']) {
    $pagePermissions = array(false,array(1));

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadClaims.php");

    $claimsIds = $_POST['claimsIds'];
    $leadId = $_POST['leadId'];

    $leadClaims = new leadClaims($bouncer["credentials"]["orgId"],$leadId);
    $deleteClaims = $leadClaims->deleteClaims($claimsIds);

    echo json_decode($deleteClaims);

}else{
    echo json_encode(false);
}