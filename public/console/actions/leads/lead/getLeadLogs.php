<?php
if(isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadLog.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/timeController.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

    $leadId = $_POST['leadId'];

    $timeController = new timeController();

    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);
    $leadData = $lead->getData();

    $allLeadLogs = [];

    // Add the lead received
    $allLeadLogs[] = ["logText"=>"Lead received","dateAdded"=>$leadData["dateReceivedOriginal"],"dateAddedShort"=>$timeController->convertv2($leadData["dateReceivedOriginal"],NULL,["seconds"=>"3000","format"=>"h:ia, M jS"])];

    $leadLog = new leadLog();
    $leadLogs = $leadLog->getLogsForLead($leadId,$bouncer["credentials"]["orgId"],true);

    foreach($leadLogs as $leadLog){
        $leadLog["dateAddedShort"] = $timeController->convertv2($leadLog["dateAdded"],NULL,["seconds"=>"3000","format"=>"h:ia, M jS"]);
        if(isset($leadLog["userIdCreated"]) && $leadLog["userIdCreated"] != NULL){
            $user = new user($leadLog["userIdCreated"]);
            $userData = $user->getData();
            $leadLog["userIdCreatedName"] = $userData["fullName"];
        }else{
            $leadLog["userIdCreatedName"] = NULL;
        }

        $allLeadLogs[] = $leadLog;
    }

    echo json_encode($allLeadLogs);
    exit;
}else{
    echo json_encode(false);
    exit;
}