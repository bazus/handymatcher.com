<?php
if(isset($_POST['id']) && isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadReminders.php");

    $id = $_POST['id'];
    $leadId = $_POST['leadId'];

    $leadReminders = new leadReminders($leadId,$bouncer["credentials"]["orgId"]);
    $deleteReminder = $leadReminders->deleteReminder($id);

    echo json_encode($deleteReminder);
}else{
    echo json_encode(false);
}