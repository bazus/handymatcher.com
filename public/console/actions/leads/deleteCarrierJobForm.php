<?php

if(isset($_POST['carrierJobsId']) && $_POST['leadId']) {
    $pagePermissions = array(false,array(1));

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobAcceptanceForms.php");

    $carrierJobsId = $_POST['carrierJobsId'];
    $leadId = $_POST['leadId'];

    $jobAcceptanceForms = new jobAcceptanceForms($bouncer["credentials"]["orgId"]);
    $deleteForm = $jobAcceptanceForms->deleteForm($leadId, $carrierJobsId);

    echo json_decode($deleteForm);

}else{
    echo json_encode(false);
}