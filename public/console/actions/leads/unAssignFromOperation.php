<?php

if(isset($_GET['leadId']) && isset($_GET['type']) && isset($_GET['id'])){

    $pagePermissions = array(false,array(1));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/operations.php");

    $leadId = $_GET['leadId'];
    $type = $_GET['type'];
    $id = $_GET['id'];

    $operations = new operations($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"],$leadId);
    $assign = false;

   switch($type){
       case "truck":
           $assign = $operations->unAssignTruck($id);
           break;
       case "carrier":
           $assign = $operations->unAssignCarrier($id);
           break;
       case "crew":
           $assign = $operations->unAssignCrew($id);
           break;
   }
    echo json_encode($assign);

}else{
    echo json_encode(false);
}
