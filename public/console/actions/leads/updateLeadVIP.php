<?php

$pagePermissions = array(false,true,true,true);

if(isset($_POST['leadId']) && isset($_POST['vip'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['leadId'];
    $vip = $_POST['vip'];

    if($vip == 'true'){$vip=1;}else{$vip=0;}

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $updateVIP = $lead->updateVIP($vip);
    echo json_encode($updateVIP);

}else{
    echo json_encode(false);
}
