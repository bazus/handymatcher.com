<?php
if (isset($_POST['activityType'])){
    $pagePermissions = array(false,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");

    $leads = new leads($bouncer["credentials"]["orgId"]);
    $dateArray = [];

    $activityType = $_POST['activityType'];
    if ($activityType == 1){
        for ($i = 0;$i<18;$i++){
            $dateArray[] = ["start"=>date("Y-m-d 00:00:00",strtotime("-".$i." days")),"end"=>date("Y-m-d 23:59:59",strtotime("-".$i." days"))];
        }
    }else if ($activityType == 2){
        $dateArray[] = ["start"=>date("Y-m-d 00:00:00",strtotime("last Sunday")),"end"=>date("Y-m-d 23:59:59",strtotime("today"))];
        for ($i = 1;$i<19;$i++){
            $dateArray[] = ["start"=>date("Y-m-d 00:00:00",strtotime("last Sunday -".$i." weeks")),"end"=>date("Y-m-d 23:59:59",strtotime("last Sunday -".($i-1)." weeks -1 days"))];
        }
    }else if ($activityType == 3){
        $dateArray[] = ["start"=>date("Y-m-d 00:00:00",strtotime("first day of this month")),"end"=>date("Y-m-d 23:59:59",strtotime("today"))];
        for ($i = 1;$i<12;$i++){
            $dateArray[] = ["start"=>date("Y-m-d 00:00:00",strtotime("first day of this month -".$i." months")),"end"=>date("Y-m-d 23:59:59",strtotime("last day of this month -".($i)." months"))];
        }
    }

    $leadsPerDay = $leads->getLeadsDataPerDay($dateArray);

    echo json_encode($leadsPerDay);
}else{
    echo json_encode(false);
}