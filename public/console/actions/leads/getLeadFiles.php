<?php
if(isset($_POST['id'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['id'];

    $leads = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $data = [];
    $data['data'] = $leads->getLeadFiles($bouncer["isUserAnAdmin"],$bouncer["credentials"]["userId"]);
    $data['uploadAble'] = $leads->checkTotalLeadFiles();

    echo json_encode($data);
}else{
    echo json_encode(false);
}