<?php
/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 09/10/2018
 * Time: 19:36
 */

$pagePermissions = array(false,array(1));

if(isset($_POST['leadId']) && isset($_POST['notes'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['leadId'];
    $notes = $_POST['notes'];

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    if ($lead->isLeadFromOrg()){
        $updateLeadNotes = $lead->updateLeadNotes($notes);

        echo json_encode($updateLeadNotes);
    }else{
        echo json_encode(false);
    }

}else{
    echo json_encode(false);
}
