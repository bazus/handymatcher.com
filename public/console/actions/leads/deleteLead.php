<?php

$pagePermissions = array(false,array(1));

if(isset($_POST['leadId'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['leadId'];

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $deleteLead = $lead->deleteLead();

    echo json_decode($deleteLead);

}else{
    echo json_encode(false);
}
