<?php

if(isset($_GET['leadId'])){

    $pagePermissions = array(false,array(1));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/operations.php");

    $leadId = $_GET['leadId'];

    $operations = new operations($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"],$leadId);
    $operationsAssigned = $operations->getAssigned();

    echo json_encode($operationsAssigned);

}else{
    echo json_encode(false);
}
