<?php


if(isset($_POST['userId'])){
    $pagePermissions = array(true,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadSalesRotation.php");

    $userId = $_POST['userId'];

    $leadSalesRotation = new leadSalesRotation($userId);
    $checkForExistingRules = $leadSalesRotation->checkForExistingRules(true);
    if($checkForExistingRules){
        $toggleActive = $leadSalesRotation->toggleActive();
        echo json_encode($toggleActive);
    }else{
        echo json_encode(false);
    }

}else{
    echo json_encode(false);
}
