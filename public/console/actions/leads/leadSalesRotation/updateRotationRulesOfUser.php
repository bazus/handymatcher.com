<?php


if(isset($_POST['userId'])){
    $pagePermissions = array(true,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadSalesRotation.php");

    $userId = $_POST['userId'];
    $schedule = [];
    $providers = [];


    if(isset($_POST['schedule'])){$schedule = $_POST['schedule'];}
    if(isset($_POST['providers'])){$providers = $_POST['providers'];}

    $leadSalesRotation = new leadSalesRotation($userId);
    $updateRotationRulesOfUser = $leadSalesRotation->updateRotationRulesOfUser($providers,$schedule);

    echo json_encode($updateRotationRulesOfUser);

}else{
    echo json_encode(false);
}
