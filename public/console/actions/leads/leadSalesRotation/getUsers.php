<?php

$pagePermissions = array(true,true,true,true);

    $pagePermissions = array(true,array(1));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadSalesRotation.php");

    $organization = new organization($bouncer["credentials"]["orgId"]);
    $activeUsers = $organization->getUsersOfMyCompany($bouncer["credentials"]["userId"],true);

//    var_dump($activeUsers);

    $usersWithoutRule = array();
    $usersWithRule = array();

    foreach($activeUsers as $userId) {

        $user = new user($userId);
        $userData = $user->getData();

        $leadSalesRotation = new leadSalesRotation($userId);
        $rotationRulesOfUser = $leadSalesRotation->getRotationRulesOfUser();

        if($rotationRulesOfUser == false){
            $usersWithoutRule[] = $userData;
        }else{
            $userData['isRuleActive'] = $rotationRulesOfUser['isActive'];
            $usersWithRule[] = $userData;
        }

    }

    $data = array();
    $data["usersWithRule"] = $usersWithRule;
    $data["usersWithoutRule"] = $usersWithoutRule;

    echo json_encode($data);