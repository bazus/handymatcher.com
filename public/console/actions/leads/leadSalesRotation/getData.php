<?php

// TODO: check if user is inside my organization
$pagePermissions = array(true,true,true,true);

if(isset($_GET['userId'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadSalesRotation.php");

    $userId = $_GET['userId'];
    $leadSalesRotation = new leadSalesRotation($userId);

    $getRotationRulesOfUser = $leadSalesRotation->getRotationRulesOfUser();

    echo json_encode($getRotationRulesOfUser);

}else{
    echo json_encode(false);
}
