<?php

if(isset($_POST['leadId']) && isset($_POST['id']) ){

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

    $leadId = $_POST['leadId'];
    $id = $_POST['id'];

    $lead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    $leadData = $lead->getData();

    $materials = new materials($bouncer["credentials"]["orgId"]);
    $material = $materials->getMaterial($id);
    if ($material){
        $collectedData = [];
        $collectedData['name'] = $material['title'];
        if ($leadData['typeOfMove'] == 0){
            $collectedData['price'] = $material['localPrice'];
        }else{
            $collectedData['price'] = $material['longPrice'];
        }
        echo json_encode($collectedData);
    }else{
        echo json_encode($material);
    }


}else{
    echo json_encode(false);
}
