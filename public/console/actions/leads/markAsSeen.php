<?php

if(isset($_POST['leadId'])){

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadDataConfirmation.php");

    $leadId = $_POST['leadId'];

    $leadDataConfirmation = new leadDataConfirmation(false);

    $didSee = $leadDataConfirmation->didSee($leadId);

    echo json_decode($didSee);

}else{
    echo json_encode(false);
}
