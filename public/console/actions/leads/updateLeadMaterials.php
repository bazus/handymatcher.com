<?php

if(isset($_POST['leadId']) && isset($_POST['dataToSave']) ){

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

    $leadId = $_POST['leadId'];
    $dataToSave = $_POST['dataToSave'];

    $lead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    $updateMaterials = $lead->updateMaterials($dataToSave);

    echo json_encode($updateMaterials);

}else{
    echo json_encode(false);
}
