<?php

if(isset($_POST['leadId']) ){

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

    $leadId = $_POST['leadId'];

    $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
    $getBadLeadData = $lead->getBadLeadData();
    if ($getBadLeadData['badLeadText'] == ""){
        $getBadLeadData['badLeadText'] = [];
    }
    echo json_encode($getBadLeadData['badLeadText']);

}else{
    echo json_encode(false);
}
