<?php

    $pagePermissions = array(true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");

    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $providers = $leadProviders->getProviders(true);

    echo json_encode($providers);
