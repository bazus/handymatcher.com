<?php
$pagePermissions = array(true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/payments.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leads.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingLead.php");

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadProviders.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");

$orgId = $bouncer["credentials"]["orgId"];

$leadProviders = new leadProviders($orgId);

$leads = new leads($orgId);
$movingLead = new movingLead(NULL,$orgId);
$payments = new payments($bouncer["credentials"]['orgId']);
$mailCenter = new mailCenter($orgId);

if (isset($_POST['start'])){
    $start = $_POST['start'];
}else{
    $start = false;
}
if (isset($_POST['end'])){
    $end = $_POST['end'];
}else{
    $end = false;
}
if (isset($_POST['label'])){
    $label = $_POST['label'];
}else{
    $label = "This Month";
}

///////////////////////////////////////////////////////////////////////////
//              LEAD SMS

function getTotalLeadSmsSentByDate($orgId = null, $compare = false,$sDate = false,$eDate = false,$label = "Custom"){

    $errorVar = array("getIndexStats.php","getTotalLeadSmsSentByDate()",4,"Notes",array());

    if ($sDate == false){
        $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
    }else{
        $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
    }
    if ($eDate == false){
        $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
    }else{
        $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
    }

    $binds = [];
    $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
    $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
    $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
    $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.twilio_subAccounts AS SA INNER JOIN networkleads_db.twilio_phoneNumbers AS PN ON PN.subAccountId=SA.id INNER JOIN networkleads_db.twilio_outgoingLeadsSMS AS OLS ON OLS.fromNumber=PN.number WHERE SA.orgId=:orgId AND CTZ(OLS.sentAt,:OFFSET)>=:startDate AND CTZ(OLS.sentAt,:OFFSET)<=:endDate", $binds,$errorVar);
    if(!$getIt){
        return false;
    }else{
        if ($compare == false) {
            return $GLOBALS['connector']->fetch_num_rows($getIt);

        }else{
            $totalPerThisDate = $GLOBALS['connector']->fetch_num_rows($getIt);

            $data = [];
            $data['totalLeadSmsSent'] = $totalPerThisDate;
            $data['totalLeadSmsSentCustom'] = false;
            if ($label == "Today"){
                $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 day"));
            }else if ($label == "Yesterday"){
                $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                $eDate = date("Y-m-d 23:59:59",strtotime("-2 days"));
            }else if ($label == "Last Week"){
                $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
            }else if ($label == "Last 30 Days"){
                $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
            }else if ($label == "This Month"){
                $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
            }else if ($label == "Last Month"){
                $sDate = date("Y-m-d 00:00:00",strtotime("first day of -2 month"));
                $eDate = date("Y-m-d 23:59:59",strtotime("last day of -2 month"));
            }else if ($label == "Custom"){
                $data['totalLeadSmsSentChange'] = 0;
                $data['totalLeadSmsSentCustom'] = true;

                return $data;
            }

            $binds = [];
            $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
            $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
            $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.twilio_subAccounts AS SA INNER JOIN networkleads_db.twilio_phoneNumbers AS PN ON PN.subAccountId=SA.id INNER JOIN networkleads_db.twilio_outgoingLeadsSMS AS OLS ON OLS.fromNumber=PN.number WHERE SA.orgId=:orgId AND CTZ(OLS.sentAt,:OFFSET)>=:startDate AND CTZ(OLS.sentAt,:OFFSET)<=:endDate", $binds,$errorVar);

            if (!$getIt){
                return false;
            }else{
                $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                    $data['totalLeadSmsSentChange'] = 100;
                }else if ($totalPerThisDate == $totalPerNewDate) {
                    $data['totalLeadSmsSentChange'] = 0;
                }else{
                    $data['totalLeadSmsSentChange'] = (($totalPerThisDate-$totalPerNewDate)/$totalPerNewDate)*100;
                }
                return $data;
            }
        }
    }
    return false;
}
///////////////////////////////////////////////////////////////////////////////

$getTotalLeadSmsByDate = getTotalLeadSmsSentByDate($orgId,true,$start,$end,$label);
$getTopProviders = $leadProviders->getTopProviders($start,$end,$label);

$getTotalMailSentByDate = $mailCenter->getTotalEmailsbyDate(true,$start,$end,$label);
$getTotalLeadsByDate = $leads->getTotalLeadsByDate(true,$start,$end,$label);

$getTotalFinishedJobsByDate = $movingLead->getTotalFinishedJobsByDate(true,$start,$end,$label);
$getTotalBookedEstimatesByDate = $movingLead->getTotalBookedEstimatesByDate(true,$start,$end,$label);
$getTotalLeadsStatusByDate = $movingLead->getTotalLeadsStatusByDate($start,$end);

$getTotalPaymentsByDate = $payments->getTotalPaymentsByDate($orgId,true,$start,$end,$label);

$data = ["topProviders"=>$getTopProviders['providerData'],"haveProviders"=>$getTopProviders['haveProviders'],"totalLeads"=>$getTotalLeadsByDate,"totalFinished"=>$getTotalFinishedJobsByDate,"totalBooked"=>$getTotalBookedEstimatesByDate,"totalPayments"=>$getTotalPaymentsByDate,"totalEmails"=>$getTotalMailSentByDate,"totalLeadSms"=>$getTotalLeadSmsByDate,"totalLeadsStatus"=>$getTotalLeadsStatusByDate];

echo json_encode($data);