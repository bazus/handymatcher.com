<?php
$pagePermissions = array(false,true,true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/system/todo/checkList.php";

$checkList = new checkList($bouncer["credentials"]["userId"]);
$checkListData = $checkList->getData();

if ($checkListData){
    echo json_encode($checkListData);
}else{
    echo json_encode(false);
}