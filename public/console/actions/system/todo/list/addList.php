<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 19/10/2018
 * Time: 0:23
 */
if (isset($_POST['title'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/system/todo/checkList.php";

    $checkList = new checkList($bouncer["credentials"]["userId"]);
    $title = $_POST['title'];
    echo json_encode($checkList->setData($title));
}else{
    echo json_encode(false);
}