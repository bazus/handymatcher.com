<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 19/10/2018
 * Time: 0:23
 */

if (isset($_POST['id']) && isset($_POST['active'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/system/todo/checkTask.php";

    $checkTask = new checkTask($bouncer["credentials"]["userId"]);

    $id = $_POST['id'];
    $active = $_POST['active'];

    if($active == 'true'){$active=1;}else{$active=0;}

    echo json_encode($checkTask->activeId($id,$active));
}else{
    echo json_encode(false);
}