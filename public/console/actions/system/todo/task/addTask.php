<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 19/10/2018
 * Time: 0:23
 */

if (isset($_POST['list']) && isset($_POST['title'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/system/todo/checkTask.php";

    $checkTask = new checkTask($bouncer["credentials"]["userId"]);
    $list = $_POST['list'];
    $title = $_POST['title'];
    echo json_encode(['status'=>$checkTask->setData($title,$list),'id'=>$list]);
}else{
    echo json_encode(false);
}