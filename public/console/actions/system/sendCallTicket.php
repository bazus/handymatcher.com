<?php

require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

$SesClient = new SesClient([
    'credentials' => array(
        'key' => $_SERVER['AWS_S3_KEY'],
        'secret' => $_SERVER['AWS_S3_SECRET'],
    ),
    'version' => '2010-12-01',
    'region'  => 'us-east-1'
]);

if (isset($_POST['reason']) && isset($_POST['phone'])) {
    $pagePermissions = array(false, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/callUsTicket.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");

    $phone = $_POST['phone'];
    $reason = $_POST['reason'];
    if ($phone && $reason && trim($phone," ") != "" && trim($reason," ") != ""){

        $callUsTicket = new callUsTicket($bouncer["credentials"]["userId"]);
        $sendTicket = $callUsTicket->sendTicket($phone,$reason);

        // send sms to niv
        $sendSMSNotification = shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>"New Call Ticket [".$reason."]"))) . ""); // > /dev/null 2>/dev/null &

        // send email to support
        $email = new email(0);
        $text = "User ID: ".$bouncer['userData']["fullName"]." (#".$bouncer["credentials"]["userId"].") <br>
            Company : ".$bouncer['userData']['organizationName']." <br>
            Phone: ".$phone."<br>
            Reason: ".$reason;

        $response = $email->sendMailFromUs($SesClient,"","support@network-leads.com",["support@network-leads.com"],"New call ticket",$text,$text,1,true);


        if ($sendTicket){
            echo json_encode(true);
        }else{
            echo json_encode(false);
        }
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}