<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 20/09/2018
 * Time: 21:33
 */
if(isset($_GET['imageURL'])){

    $pagePermissions = array(false,[1],true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

    $key = $_GET['imageURL'];

    try{
        $s3 = new s3();
        $url = $s3->getLimitedTimeUrl("thenetworkleads",$key,20,$bouncer["credentials"]["userId"],true,$bouncer['isUserAnAdmin']);
    }
    catch (Exception $e){
        $url = false;
    }

    if($url){
        header("location: ".$url);
    }else{
        header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/user/fileManager.php");
    };

}
