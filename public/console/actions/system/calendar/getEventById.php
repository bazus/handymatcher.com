<?php
if (isset($_POST['id'])) {
    $pagePermissions = array(false, true,true, array(["calendar", 1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

    $calendar = new calendar($bouncer["credentials"]["userId"]);

    $eventData = $calendar->getEventByID($_POST['id'],$bouncer["credentials"]["orgId"],true);

    if ($eventData){
        echo json_encode($eventData);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}