<?php

if(isset($_POST['id']) && isset($_POST['dateS']) && isset($_POST['type'])) {
    $pagePermissions = array(false,true,true,array(["calendar",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

    $id = $_POST['id'];
    $dateS = date("Y-m-d H:i:s",strtotime($_POST['dateS']));
    $dateE = date("Y-m-d H:i:s",strtotime($_POST['dateE']));
    $type = $_POST['type'];

    $movingLead = new movingLead($id,$bouncer["credentials"]["orgId"]);
    $updateOperationDate = $movingLead->updateOperationDate($dateS,$dateE,$type);

    echo json_encode($updateOperationDate);
}else{
    echo json_encode(false);
}