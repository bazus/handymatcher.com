<?php
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

if(isset($_POST['fromDate']) && isset($_POST['toDate'])) {

    $pagePermissions = array(false, true, true, array(["calendar", 1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendarSettings.php");

    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $fromDate = date("Y-m-d 00:00:00",strtotime($fromDate));
    $toDate = date("Y-m-d 23:59:59",strtotime($toDate));

    $showPersonal = false;
    $showOrganization = false;
    $showOperations = false;
    $showHolidays = false;

    if (isset($_POST['showPersonal']) && $_POST['showPersonal'] == "true") {
        $showPersonal = true;
    }
    if (isset($_POST['showOrganization']) && $_POST['showOrganization'] == "true") {
        $showOrganization = true;
    }
    if (isset($_POST['showOperations']) && $_POST['showOperations'] == "true") {
        $showOperations = true;
    }
    if (isset($_POST['showHolidays']) && $_POST['showHolidays'] == "true") {
        $showHolidays = true;
    }

    $calendar = new calendar($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $calendarSettings = new calendarSettings($bouncer["credentials"]["userId"]);

    // All the events
    $calendars = [];

    if ($showOperations === true) {
        $operationEvents = $calendar->getOperationsData($fromDate,$toDate);
        $eventsColor = $calendarSettings->getCalendarColor("operationsCheckbox");

        for($i = 0 ; $i < count($operationEvents); $i++){
            $operationEvents[$i]["backgroundColor"] = $eventsColor;
            $operationEvents[$i]["borderColor"] = $eventsColor;

        }

        $singleCalendar = [];
        $singleCalendar["id"] = "operationsCheckbox";
        $singleCalendar["type"] = "regular";
        $singleCalendar["name"] = "Operations";
        $singleCalendar["events"] = $operationEvents;
        $singleCalendar["color"] = $eventsColor;


        $calendars[] = $singleCalendar;
    }
    if ($showPersonal === true) {
        $personalEvents = $calendar->getEvents(true,$fromDate,$toDate);
        $eventsColor = $calendarSettings->getCalendarColor("personalCheckbox");



        for($i = 0 ; $i < count($personalEvents); $i++){
            if($personalEvents[$i]["color"] == NULL) {
                $personalEvents[$i]["backgroundColor"] = $eventsColor;
                $personalEvents[$i]["borderColor"] = $eventsColor;
            }
        }

        $singleCalendar = [];
        $singleCalendar["id"] = "personalCheckbox";
        $singleCalendar["type"] = "regular";
        $singleCalendar["name"] = "Personal";
        $singleCalendar["events"] = $personalEvents;
        $singleCalendar["color"] = $eventsColor;


        $calendars[] = $singleCalendar;
    }
    if ($showOrganization === true) {
        $organizationEvents = $calendar->getEvents(false,$fromDate,$toDate);
        $eventsColor = $calendarSettings->getCalendarColor("organizationCheckbox");

        for($i = 0 ; $i < count($organizationEvents); $i++){
            $organizationEvents[$i]["backgroundColor"] = $eventsColor;
            $organizationEvents[$i]["borderColor"] = $eventsColor;

        }

        $singleCalendar = [];
        $singleCalendar["id"] = "organizationCheckbox";
        $singleCalendar["type"] = "regular";
        $singleCalendar["name"] = "Organization";
        $singleCalendar["events"] = $organizationEvents;
        $singleCalendar["color"] = $eventsColor;


        $calendars[] = $singleCalendar;
    }
    if ($showHolidays === true) {
        $holidayEvents = $calendar->getHolidays($fromDate,$toDate);
        $eventsColor = $calendarSettings->getCalendarColor("holidays");

        for($i = 0 ; $i < count($holidayEvents); $i++){
            $holidayEvents[$i]["backgroundColor"] = $eventsColor;
            $holidayEvents[$i]["borderColor"] = $eventsColor;

        }

        $singleCalendar = [];
        $singleCalendar["id"] = "holidays";
        $singleCalendar["type"] = "regular";
        $singleCalendar["name"] = "Holidays";
        $singleCalendar["events"] = $holidayEvents;
        $singleCalendar["color"] = $eventsColor;


        $calendars[] = $singleCalendar;
    }


    echo json_encode($calendars);
}else{
    echo json_encode(false);
}