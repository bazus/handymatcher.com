<?php

if(isset($_POST['calendarId']) && isset($_POST['color'])) {

    $pagePermissions = array(false,true,true,array(["calendar",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendarSettings.php");

    $calendarId = $_POST['calendarId'];
    $color = $_POST['color'];

    $calendarColor = new calendarSettings($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $setCalendarColor = $calendarColor->updateCalendarColor($calendarId,$color);

    echo json_encode($setCalendarColor);
}else{
    echo json_encode(false);
}