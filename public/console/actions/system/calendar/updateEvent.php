<?php
if(isset($_POST['id']) && isset($_POST['eventType']) && isset($_POST['title'])  && isset($_POST['description'])  && isset($_POST['place']) && isset($_POST['dateS']) && isset($_POST['dateE'])) {
    $pagePermissions = array(false,true,true,array(["calendar",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

    $id = $_POST['id'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $place = $_POST['place'];
    $dateS = date("Y-m-d H:i:s",strtotime($_POST['dateS']));
    $dateE = date("Y-m-d H:i:s",strtotime($_POST['dateE']));

    $orgEvent = NULL;
    if($_POST['eventType'] == 2){
        $orgEvent = 1;
    }

    $calendar = new calendar($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $event = $calendar->updateEvent($id,$title,$description,$place,$dateS,$dateE,$orgEvent);

    echo json_encode($event);
}else{
    echo json_encode(false);
}