<?php
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

if(isset($_POST['fromDate']) && isset($_POST['toDate'])) {

    $pagePermissions = array(false, true, true, array(["calendar", 1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/googlecalendar.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/googleAPI.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendarSettings.php");

    $fromDate = $_POST['fromDate'];
    $toDate = $_POST['toDate'];

    $fromDate = date("Y-m-d 00:00:00",strtotime($fromDate));
    $toDate = date("Y-m-d 23:59:59",strtotime($toDate));

    $calendarsList = [];

    // ===================== GET GOOGLE CALENDAR EVENTS =====================
    $calendarSettings = new calendarSettings($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $calendarSettingsData = $calendarSettings->getData();
    if(isset($_POST["googlecalendars"])) {
        $isAuthorized = false;
        $googleCalendarSyncAccount = unserialize($calendarSettingsData["googleCalendarSyncAccount"]);
        if ($googleCalendarSyncAccount != NULL && $googleCalendarSyncAccount != false) {
            // Check the token
            $googleAPI = new googleAPI($googleCalendarSyncAccount["refresh_token"]);
            $isAuthorized = $googleAPI->isAuthorized();
        }

        if ($isAuthorized) {
            $service = new Google_Service_Calendar($googleAPI->client);

            $googlecalendar = new googlecalendar($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"], $service);

            $calendars = $_POST["googlecalendars"];
            for ($i = 0;$i<count($calendars);$i++) {

                $googlecalendarEvents = $googlecalendar->getEvents($fromDate, $toDate,$calendars[$i]);
                if (is_array($googlecalendarEvents)) {

                    $singleCalendar = [];
                    $singleCalendar["id"] = $calendars[$i];
                    $singleCalendar["name"] = $calendars[$i];
                    $singleCalendar["events"] = $googlecalendarEvents;

                    $calendarsList[] = $singleCalendar;
                }
            }
        }
    }
    // ===================== GET GOOGLE CALENDAR EVENTS =====================


    echo json_encode($calendarsList);
}else{
    echo json_encode(false);
}