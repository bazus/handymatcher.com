<?php

if(isset($_POST['eventId'])) {

    $pagePermissions = array(false,true,true,array(["calendar",4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

    $eventId = $_POST['eventId'];

    $calendar = new calendar($bouncer["credentials"]["userId"]);
    $deleteEvent = $calendar->deleteEvent($eventId,$bouncer["credentials"]["orgId"]);

    echo json_encode($deleteEvent);

}else{
    echo json_encode(false);
}