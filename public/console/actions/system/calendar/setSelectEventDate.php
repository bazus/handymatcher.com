<?php

if(isset($_POST['eventId']) && isset($_POST['theDate']) && isset($_POST['dateS']) && isset($_POST['dateE']) && isset($_POST['orgEvent'])) {
    $pagePermissions = array(false,true,true,array(["calendar",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

    $eventId = $_POST['eventId'];
    $theDate = $_POST['theDate'];
    $timeStart = preg_replace('/\s+/', '', $_POST['dateS']);
    $timeEnd = preg_replace('/\s+/', '', $_POST['dateE']);
    $orgEvent = $_POST['orgEvent'];

    $dateS = date("Y-m-d H:i:s",strtotime($theDate." ".$timeStart));
    $dateE = date("Y-m-d H:i:s",strtotime($theDate." ".$timeEnd));

    $calendar = new calendar($bouncer["credentials"]["userId"]);
    $event = $calendar->setEventDate($eventId,$dateS,$dateE,$bouncer["credentials"]["orgId"],$orgEvent);

    echo json_encode($event);

}else{
    echo json_encode(false);
}