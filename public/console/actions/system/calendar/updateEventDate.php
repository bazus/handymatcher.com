<?php

if(isset($_POST['eventId']) && isset($_POST['dateS']) && isset($_POST['dateE'])) {
    $pagePermissions = array(false,true,true,array(["calendar",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

    $eventId = $_POST['eventId'];
    $dateS = date("Y-m-d H:i:s",strtotime($_POST['dateS']));
    $dateE = date("Y-m-d H:i:s",strtotime($_POST['dateE']));

    $calendar = new calendar($bouncer["credentials"]["userId"]);
    $event = $calendar->updateEventDate($eventId,$dateS,$dateE,$bouncer["credentials"]["orgId"]);

    echo json_encode($event);
}else{
    echo json_encode(false);
}