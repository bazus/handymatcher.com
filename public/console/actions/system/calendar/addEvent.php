<?php

if(isset($_POST['eventTitle'])) {

    $pagePermissions = array(false,true,true,array(["calendar",3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendar.php");

    $eventTitle = $_POST['eventTitle'];
    $eventDescription = $_POST['eventDescription'];
    $eventPlace = $_POST['eventPlace'];
    $sDate = $_POST['theStartDate'];
    $eDate = $_POST['theEndDate'];

    $orgEvent = false;

    if (isset($_POST['usersToSend']) && $_POST['usersToSend']){
        $orgEvent = 1;
        // Use $_POST['usersToSend'] to get the ids to send notification to
    }elseif(isset($_POST['eventType']) && $_POST['eventType'] == 2){
        $orgEvent = 1;
    }

    $dateS = date("Y-m-d H:i:s",strtotime($sDate));
    $dateE = date("Y-m-d H:i:s",strtotime($eDate));


    $calendar = new calendar($bouncer["credentials"]["userId"]);
    $createEvent = $calendar->addEvent($eventTitle,$orgEvent,$bouncer["credentials"]["orgId"],$dateS,$dateE,$eventDescription,$eventPlace);

    echo json_encode($createEvent);

}else{
    echo json_encode(false);
}