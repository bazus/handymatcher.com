<?php
if (isset($_POST['calendarId'])) {
    $pagePermissions = array(false, true,true, array(["calendar", 1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendarSettings.php");

    $calendarId = $_POST['calendarId'];

    $calendar = new calendarSettings($bouncer["credentials"]["userId"]);
    $calendarColor = $calendar->getCalendarColor($calendarId);

    echo json_encode($calendarColor);
}else{
    echo json_encode(false);
}