<?php

if(isset($_POST['isDeleted']) && $_POST['isDeleted'] == 1) {

    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/calendarSettings.php");

    $calendarSettings = new calendarSettings($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $removeGoogleCalendarAccount = $calendarSettings->removeGoogleCalendarSyncAccount();

    echo json_encode($removeGoogleCalendarAccount);
}else{
    echo json_encode(false);
}

