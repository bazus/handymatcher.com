<?php

if(isset($_POST['organizationName']) && isset($_POST['organizationNameKey']) && $_POST['organizationNameKey'] != "" && isset($_POST['address']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zip']) && isset($_POST['phone']) && isset($_POST['website']) && isset($_POST['facebook']) && isset($_POST['instagram']) && isset($_POST['twitter']) && isset($_POST['linkedin'])) {

    $pagePermissions = array(false,true,true,array(["organization",2]));

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

    $organization = new organization($bouncer["credentials"]["orgId"]);

    $organizationName = $_POST['organizationName'];
    $organizationNameKey = $_POST['organizationNameKey'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $website = $_POST['website'];
    $facebook = $_POST['facebook'];
    $instagram = $_POST['instagram'];
    $twitter = $_POST['twitter'];
    $linkedin = $_POST['linkedin'];


    $updateData = $organization->updateData($organizationName,$organizationNameKey,$address,$city,$state,$zip,$phone,$website,$facebook,$instagram,$twitter,$linkedin);

    if(isset($_POST['usdot']) && isset($_POST['mcc']) && isset($_POST['registration'])){

        require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingCompany.php");

        $usdot = $_POST['usdot'];
        $mcc = $_POST['mcc'];
        $registration = $_POST['registration'];

        $movingCompany = new movingCompany($bouncer["credentials"]["orgId"]);
        $updateLicenses = $movingCompany->updateLicenses($usdot,$mcc,$registration);

        if($updateData == true && $updateLicenses == true){
            echo json_encode(true);
            exit;
        }else{
            echo json_encode(false);
            exit;
        }

    }

    echo json_encode($updateData);
}else{
    echo json_encode(false);
}

