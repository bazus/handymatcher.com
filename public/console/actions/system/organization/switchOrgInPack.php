<?php
if(isset($_GET["orgId"])) {
    $pagePermissions = array(false, true, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

    $orgId = $_GET["orgId"];

    $organization = new organization($bouncer["credentials"]["orgId"]);
    $organization->switchToOrg($orgId);
}