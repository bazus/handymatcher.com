<?php

$pagePermissions = array(false,true,true,array(["organization",3]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

$s3 = new s3();

$organization = new organization($bouncer["credentials"]["orgId"]);

// ================ FILE UPLOAD ================
$final_target_file = "";
$uploadOk = 1;
if($_FILES["fileToUpload"]["name"] != ""){

    $thisDate = date("Ymdhis", strtotime("now"));

    $filePathInfo = pathinfo($_FILES["fileToUpload"]["name"]);
    $newFileName = $filePathInfo["filename"] . $thisDate . "." . $filePathInfo["extension"];

    $target_dir = $_SERVER['LOCAL_NL_PATH']."/console/files/organizations/";

    $target_file = $target_dir . $newFileName;
    $final_target_file = $_SERVER['LOCAL_NL_PATH']."/console/files/organizations/" . $newFileName;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    // Check if file already exists
    if (file_exists($target_file)) {
        //echo "Sorry, file already exists.<br>";
        $uploadOk = 0;
    }

    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg"  && $imageFileType != "JPEG"
        && $imageFileType != "gif"  && $imageFileType != "pdf"  && $imageFileType != "doc"  && $imageFileType != "docx"  && $imageFileType != "xls"  && $imageFileType != "xlsx" ) {
        //echo "Sorry, only JPG, JPEG, PNG, PDF, DOC, XLS & GIF files are allowed.<br>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //echo "Sorry, your file was not uploaded.<br>";
        // if everything is ok, try to upload file
    } else {
        $file_name = $newFileName;
        $temp_file_location = $_FILES['fileToUpload']['tmp_name'];
        $result = $s3->putObject("thenetworkleads","organizationProfiles/".$file_name,$temp_file_location,"public-read");
        if ($result->get("@metadata")['statusCode'] == 200) {
            $organization->saveNewLogo( $result->get("@metadata")['effectiveUri']);
        } else {
            $collectData['error'][] = ["Sorry, there was an error uploading your file."];
            $collectData['success'] = false;
            echo json_encode($collectData);
            exit;
        }
    }
}
// ================ FILE UPLOAD ================

header('Location: '.$_SERVER['LOCAL_NL_URL']."/console/categories/user/management.php");
exit;