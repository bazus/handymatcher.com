<?php
$pagePermissions = array(false,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

$organization = new organization($bouncer["credentials"]["orgId"]);

$orgData = $organization->getData();
$data = ["name"=>$orgData['organizationName'],"address"=>$orgData['address'],"city"=>$orgData['city'],"zip"=>$orgData['zip'],"state"=>$orgData['state'],"phone"=>$orgData['phone']];
echo json_encode($data);