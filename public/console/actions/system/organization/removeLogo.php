<?php

$pagePermissions = array(false,true,true,array(["organization",4]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

$s3 = new s3();

$organization = new organization($bouncer["credentials"]["orgId"]);
$organization->saveNewLogo("");

header('Location: '.$_SERVER['LOCAL_NL_URL']."/console/categories/user/management.php");
exit;