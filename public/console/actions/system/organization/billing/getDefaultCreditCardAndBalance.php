<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

$pagePermissions = array(true, true, true, array(["organization", 2]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

$billing = new billing($bouncer["credentials"]["orgId"]);
$currentBalance = $billing->getCurrentBalance();

$autoRecharge = $billing->getAutoRecharge();

$stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
$defaultCreditCard = $stripeBilling->getDefaultCreditCard($bouncer["organizationData"]["stripeAccountId"]);

$resp = [];
$resp["currentBalance"] = $currentBalance;
$resp["defaultcreditcard"] = $defaultCreditCard;
$resp["autoRecharge"] = $autoRecharge;

echo json_encode($resp);