<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

if(isset($_GET["chargeId"])) {

    $pagePermissions = array(true, true, true, array(["organization", 2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

    $chargeId = $_GET["chargeId"];

    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $charge = $stripeBilling->retrieveCharge($bouncer["organizationData"]["stripeAccountId"],$chargeId);


    var_dump($charge["response"]->receipt_url);
    echo json_encode($charge);
    exit;
}else{
    echo json_encode(false);
    exit;
}