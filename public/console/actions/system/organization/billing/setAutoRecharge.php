<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

if(isset($_POST['isOn']) && isset($_POST['balanceBelow']) && isset($_POST['balanceTo']) && $_POST['balanceTo'] >= 10) {
    $pagePermissions = array(true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

    $isOn = $_POST['isOn'];
    $balanceBelow = $_POST['balanceBelow'];
    $balanceTo = $_POST['balanceTo'];

    if ($balanceBelow > $balanceTo){
        echo json_encode(false);
        exit;
    }
    $billing = new billing($bouncer["credentials"]["orgId"]);

    if ($isOn === true || $isOn === "true"){
        $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
        $resp["defaultcc"] = $stripeBilling->getDefaultCreditCard($bouncer["organizationData"]["stripeAccountId"]);
        if (count($resp["defaultcc"]['response']) == 0 || $resp["defaultcc"]['response'] == false){
            echo json_encode('noCC');
            exit;
        }
        $setAutoRecharge = $billing->setAutoRecharge($isOn,$balanceBelow,$balanceTo);
    }else{
        $setAutoRecharge = $billing->setAutoRecharge($isOn,$balanceBelow,$balanceTo);
    }
    echo json_encode($setAutoRecharge);
}else{
    echo json_encode(false);
}