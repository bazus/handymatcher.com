<?php

if(isset($_POST["cancel"]) && $_POST["cancel"] == "true") {

    require_once($_SERVER['LOCAL_NL_PATH'] . '/console/vendor/autoload.php');

    $stripe = [
        "secret_key" => $_SERVER["STRIPE_SECRET"],
        "publishable_key" => $_SERVER["STRIPE_PK"],
    ];

    \Stripe\Stripe::setApiKey($stripe['secret_key']);

    $pagePermissions = array(false,true,true,array(["organization",4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/classes/plans.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);

    $organizationData = $bouncer["organization"]->getData();

    $stripeError = false;

    $plans = new plans();
    $specificPlan = $plans->getSpecificPlan($organizationData["organizationPackage"]);

    $organization = new organization($bouncer["credentials"]["orgId"]);
    $organizationData = $organization->getData();
    $stripeAccountId = $organizationData["stripeAccountId"];

    if ($stripeAccountId == null || $stripeAccountId == "") {
        $stripeError = true;
    }

    // If has an error - exit and return false
    if ($stripeError == true) {
        echo json_encode(false);
        exit;
    }

    $customerSubs = $stripeBilling->getCustomerSubscriptions($stripeAccountId, 10);
    if($customerSubs == false){
        // no customer subscription found (may happen if customer exists in prod/stage environment and current environment is not prod/stage)
        echo json_encode(false);
        exit;
    }
    $customerSubscriptionId = $customerSubs["data"][0]->id;

    $subscription = \Stripe\Subscription::retrieve($customerSubscriptionId);
    $subEndDate = date("Y-m-d H:i:s",$subscription->current_period_end);
    
    // Cancel Plan
    $cancelPlan = $stripeBilling->cancelPlan($customerSubscriptionId);

    // If has an error - exit and return false
    if ($cancelPlan == false) {
        echo json_encode(false);
        exit;
    }

    $changeOrganizationPlanStatus = $bouncer["organization"]->changeOrganizationPlanStatus(0,$subEndDate);
    echo json_encode($changeOrganizationPlanStatus);

    shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$bouncer["userData"]["fullName"]." from ".$organizationData["organizationName"]." canceled their subscription","env"=>$_SERVER["ENVIRONMENT"])))); // > /dev/null 2>/dev/null &



}