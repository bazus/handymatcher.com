<?php

if(isset($_POST["reactivate"]) && $_POST["reactivate"] == "true") {

    require_once($_SERVER['LOCAL_NL_PATH'] . '/console/vendor/autoload.php');

    $stripe = [
        "secret_key" => $_SERVER["STRIPE_SECRET"],
        "publishable_key" => $_SERVER["STRIPE_PK"],
    ];

    \Stripe\Stripe::setApiKey($stripe['secret_key']);

    $pagePermissions = array(false,true,true,array(["organization",3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/classes/plans.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);

    $organizationData = $bouncer["organization"]->getData();

    $now = date("Y-m-d H:i:s",strtotime("now"));
    $cancellationDate = date("Y-m-d H:i:s",strtotime($organizationData["subscriptionEndDate"]));
    if($cancellationDate < $now){
        // Can not re-activate plan. It is already cancelled
        // User need to create a new plan
        echo json_encode(false);
        exit;
    }

    $stripeError = false;

    $plans = new plans();
    $specificPlan = $plans->getSpecificPlan($organizationData["organizationPackage"]);

    $organization = new organization($bouncer["credentials"]["orgId"]);
    $organizationData = $organization->getData();
    $stripeAccountId = $organizationData["stripeAccountId"];

    if ($stripeAccountId == null || $stripeAccountId == "") {
        $stripeError = true;
    }

    // If has an error - exit and return false
    if ($stripeError == true) {
        echo json_encode(false);
        exit;
    }

    // Get Customer Current Subscription Plan
    $customerSubs = $stripeBilling->getCustomerSubscriptions($stripeAccountId, 10);
    $customerSubscriptionId = $customerSubs["data"][0]->id;

    // Re-Activate Plan
    $cancelPlan = $stripeBilling->reactivatePlan($customerSubscriptionId);

    // If has an error - exit and return false
    if ($cancelPlan == false) {
        echo json_encode(false);
        exit;
    }

    $subscription = \Stripe\Subscription::retrieve($customerSubscriptionId);
    $subEndDate = date("Y-m-d H:i:s",$subscription->current_period_end);

    $changeOrganizationPlanStatus = $bouncer["organization"]->changeOrganizationPlanStatus(1,$subEndDate);
    echo json_encode($changeOrganizationPlanStatus);


}