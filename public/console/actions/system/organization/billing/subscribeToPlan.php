<?php

if(isset($_POST['ignoreWelcomeMode'])){
    $ignoreWelcomeMode = true;
}
$ignoreRenewMode = true;

if(isset($_POST["planId"])) {

    require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');
    
    $stripe = [
        "secret_key"      => $_SERVER["STRIPE_SECRET"],
        "publishable_key" => $_SERVER["STRIPE_PK"],
    ];

    \Stripe\Stripe::setApiKey($stripe['secret_key']);

    $pagePermissions = array(false,true,true,array(["organization",3]));

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/classes/plans.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $planId = $_POST["planId"];
    $stripeToken = NULL;

    if(isset($_POST["stripeToken"])) {
        $stripeToken = $_POST["stripeToken"];
    }

    $coupon = "";
    $stripeClientId = NULL;

    // A coupon
    if (isset($_POST['coupon'])){
        $coupon = $_POST['coupon'];
    }

    $organizationData = $bouncer["organization"]->getData();
    if($organizationData["stripeAccountId"] != NULL && $organizationData["stripeAccountId"] != ""){
        $stripeClientId = $organizationData["stripeAccountId"];
    }

    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);

    $user = new user($bouncer["credentials"]["userId"]);
    $userData = $user->getData();

    $plans = new plans();
    $specificPlan = $plans->getSpecificPlan($planId);

    // get plans ids
    if($_SERVER["ENVIRONMENT"] == "prod"){
        $stripePlanId = $specificPlan["stripe_id"];
        $stripePlanPrice = $specificPlan["price"];
        if(isset($_POST['payByMonthly']) && $_POST['payByMonthly'] == "false"){
            $stripePlanId = $specificPlan["stripe_id_yearly"];
            $stripePlanPrice = $specificPlan["yearlyPrice"];
        }
    }else{
        $stripePlanId = $specificPlan["stripe_id_test"];
        $stripePlanPrice = $specificPlan["price"];
        if(isset($_POST['payByMonthly']) && $_POST['payByMonthly'] == "false"){
            $stripePlanId = $specificPlan["stripe_id_yearly_test"];
            $stripePlanPrice = $specificPlan["yearlyPrice"];
        }
    }

    // Check if has a stripe client already
    if($stripeClientId === NULL) {

        // Create a new customer in stripe
        $createNewCustomer = $stripeBilling->createNewCustomer($organizationData["organizationName"] . " [" . $organizationData["id"] . "]", $stripeToken, $userData["email"]);

        // Checking if create new customer successsfully
        if ($createNewCustomer["status"] == false) {
            echo json_encode(false);
            exit;
        }

        $stripeClientId = $createNewCustomer["customer"]->id;

        // save stripe customer id to our system
        $updateStripeClientId = $bouncer["organization"]->updateStripeClientId($stripeClientId);

    }

    if($stripeToken !== NULL) {
        // Update default payment method
        $updateDefaultPayment = $stripeBilling->updateDefaultPayment($stripeClientId, $stripeToken);
    }

    // check if a plan exists
    $customerSubscriptions = $stripeBilling->getCustomerSubscriptions($stripeClientId);
    $subscribedPlans = count($customerSubscriptions["data"]);

    // if a subscription does not exist - create a new one, id exists - update it
    if($subscribedPlans == 0){
        // Subscribe to stripe plan
        $subscribeToPlan = $stripeBilling->subscribeToPlan($stripeClientId,$stripePlanId,$coupon);

        // Checking if subscribed to new plan successsfully
        if($subscribeToPlan["status"] == false){echo json_encode(false);exit;}

    }else{

        //  Update the current subscription
        $customerSubs = $stripeBilling->changePlan($customerSubscriptions["data"][0]->id,$stripePlanId);

        // Checking if updates subscription successsfully
        if($customerSubs == false){echo json_encode(false);exit;}
    }


    // if all went ok, change organization plan in out database
    $changeOrganizationPlan = $bouncer["organization"]->changeOrganizationPlan($planId);
    if($changeOrganizationPlan){

        // Get Customer Current Subscription Plan
        $customerSubs = $stripeBilling->getCustomerSubscriptions($stripeClientId, 10);
        $customerSubscriptionId = $customerSubs["data"][0]->id;

        $subscription = \Stripe\Subscription::retrieve($customerSubscriptionId);
        $subEndDate = date("Y-m-d H:i:s",$subscription->current_period_end);

        // activate the organization package
        $changeOrganizationPlanStatus = $bouncer["organization"]->changeOrganizationPlanStatus(1,$subEndDate);
        echo json_encode($changeOrganizationPlanStatus);

        $payType = "Monthly";
        if(isset($_POST['payByMonthly']) && $_POST['payByMonthly'] == "false") {
            $payType = "Annual";
        }

        shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$organizationData["organizationName"]." subscribed to plan #".$planId." [".$payType."] ","env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

    }else{
        echo json_encode(false);
    }

}