<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

if(isset($_POST["amount"]) && $_POST["amount"] >= 10) {

    $pagePermissions = array(true, true, true, array(["organization", 2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");

    $amount = $_POST["amount"];
    $chargeDescription = "Add Funds";
    $type = 1;
    $refId = NULL;
    $notes = "";
    $other = "";

    if($amount <= 0){
        // amount to charge can't be below 0
        echo json_encode(false);
        exit;
    }

    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $charge = $stripeBilling->charge($bouncer["organizationData"]["stripeAccountId"],$amount,$chargeDescription);

    if($charge["status"] == true){
        // if successfully charges in stripe - add balance in our db

        $refId = $charge["response"]->id;
        $other = json_encode($charge);

        $billing = new billing($bouncer["credentials"]["orgId"]);
        $create = $billing->create($amount,0,$type,$refId,$notes,$other,true);

        $msg = $bouncer["organizationData"]["organizationName"]." added $".$amount;
        shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$msg,"env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

        echo json_encode($create);
        exit;
    }else{
        try{
            $data = [];
            $data['status'] = false;
            $data['error'] = $charge['response'];
            echo json_encode($data);
            die;
        }catch (Exception $e){
            unset($e);
        }
    }

    echo json_encode(false);
    exit;
}else{
    echo json_encode(false);
    exit;
}