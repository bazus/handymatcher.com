<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);


if (isset($_GET['startDate']) && isset($_GET['endDate'])) {
    $pagePermissions = array(true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

    $billing = new billing($bouncer["credentials"]["orgId"]);
    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);

    $getOrganizationBilling = $billing->getOrganizationBilling($_GET['startDate'],$_GET['endDate']);

    if ($getOrganizationBilling && count($getOrganizationBilling) > 0) {
        foreach ($getOrganizationBilling as &$billing) {

            $chargeData = json_decode($billing["other"]);

            if($chargeData != false){
                $billing['receipt_url'] = $chargeData->response->receipt_url;
            }else{
                $billing['receipt_url'] = "#";
            }

            $billing['changeDate'] = date("D, M d, Y h:i a", strtotime($billing['changeDate']));
        }
    }
    echo json_encode($getOrganizationBilling);
}else{
    echo json_encode(false);
}