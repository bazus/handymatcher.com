<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

$pagePermissions = array(true, true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

$resp = [];
$resp["autoRecharge"] = false;
$resp["defaultcc"] = false;

$billing = new billing($bouncer["credentials"]["orgId"]);

$resp["autoRecharge"] = $billing->getAutoRecharge();

$stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
$resp["defaultcc"] = $stripeBilling->getDefaultCreditCard($bouncer["organizationData"]["stripeAccountId"]);

echo json_encode($resp);
