<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => "sk_live_WztOcLSQ6QkgmHtXb5HlRlOW",
    "publishable_key" => "pk_live_aHYmXy1TiZT1d9T9708qDAPm",
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);


if(isset($argv[1])) {

    require_once($_SERVER['LOCAL_NL_PATH']."/console/connect/connect.php");
    $database = new connect();

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");

    $data = unserialize($argv[1]);
    $organizationId = $data["organizationId"];
    $amount = $data["amount"];
    $chargeDescription = $data["chargeDescription"];
    $type = $data["type"];
    $refId = $data["refId"];
    $notes = $data["notes"];

    if($amount <= 0){
        // amount to charge can't be below 0
        echo json_encode(false);
        exit;
    }

    $organization = new organization($organizationId);
    $organizationData = $organization->getData();

    $stripeBilling = new stripeBilling(NULL, $organizationId);
    $charge = $stripeBilling->charge($organizationData["stripeAccountId"],$amount,$chargeDescription);

    if($charge["status"] == true){
        // if successfully charges in stripe - add balance in our db
        
        $billing = new billing($organizationId);
        $create = $billing->create($amount,0,$type,$refId,$notes,"",true);

        $msg = $organizationData["organizationName"]." added $".$amount;
        shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$msg))) . ""); // > /dev/null 2>/dev/null &


        echo json_encode($create);
        exit;
    }

    echo json_encode(false);
    exit;
}else{
    echo json_encode(false);
    exit;
}