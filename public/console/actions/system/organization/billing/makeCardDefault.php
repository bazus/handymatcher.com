<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

if(isset($_POST["source"])) {
    $pagePermissions = array(true, true, true, array(["organization", 2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

    $source = $_POST["source"];
    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $makeCreditCardDefault = $stripeBilling->makeCreditCardDefault($bouncer["organizationData"]["stripeAccountId"], $source);

    echo json_encode($makeCreditCardDefault);
}