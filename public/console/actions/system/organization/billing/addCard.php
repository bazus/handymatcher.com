<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/vendor/autoload.php');

$stripe = [
    "secret_key"      => $_SERVER["STRIPE_SECRET"],
    "publishable_key" => $_SERVER["STRIPE_PK"],
];

\Stripe\Stripe::setApiKey($stripe['secret_key']);

if(isset($_POST["stripeToken"])) {
    $pagePermissions = array(true, true, true, array(["organization", 2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/stripeBilling.php");

    $source = $_POST["stripeToken"];
    $stripeBilling = new stripeBilling($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $addCreditCard = $stripeBilling->addCreditCard($bouncer["organizationData"]["stripeAccountId"], $source);

    echo json_encode($addCreditCard);

    shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$bouncer["userData"]['fullName']." from ".$bouncer["organizationData"]["organizationName"]." added a credit card","env"=>$_SERVER["ENVIRONMENT"])))); // > /dev/null 2>/dev/null &

}