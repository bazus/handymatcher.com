<?php
// Currently this page can only redeem a 'freeaccount' coupon which will give the client free access to the software for a year

if(isset($_GET["coupon"])) {
    $coupon = $_GET['coupon'];

    $availableCoupons = [];
    $availableCoupons[] = "trial2020";
    $availableCoupons[] = "partner20";
    $availableCoupons[] = "booming20";

    if (in_array(strtolower($coupon),$availableCoupons)){

        $ignoreWelcomeMode = true;
        $ignoreRenewMode = true;

        $pagePermissions = array(true,true,true,true);
        require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

        // activate the organization package
        $subEndDate = date("Y-m-d H:i:s",strtotime("+12 months"));
        $changeOrganizationPlan = $bouncer["organization"]->changeOrganizationPlan(1);

        if($changeOrganizationPlan){
            $changeOrganizationPlanStatus = $bouncer["organization"]->changeOrganizationPlanStatus(1,$subEndDate);

            if($changeOrganizationPlanStatus){
                $msg = $bouncer["organizationData"]["organizationName"]." activated his free account";
                $user = new user($bouncer["credentials"]["userId"]);
                $disableWelcomeMode = $user->disableWelcomeMode();
            }else{
                $msg = "[FAILED 1] ".$bouncer["organizationData"]["organizationName"]." activated his free account";

            }
        }else{
            $msg = "[FAILED 2] ".$bouncer["organizationData"]["organizationName"]." activated his free account";
        }
        shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>$msg,"env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

        echo json_encode($changeOrganizationPlanStatus);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}