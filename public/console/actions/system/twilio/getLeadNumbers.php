<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

if(isset($_GET['leadId'])) {

    $pagePermissions = array(false,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $leadId = $_GET['leadId'];

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);

    $leadNumbers = $organizationTwilio->getLeadNumbers($leadId);

    $numers = [];
    foreach ($leadNumbers as $leadNumber){
        $numers[] = ["number"=>$leadNumber,"prettyNumber"=>$organizationTwilio->prettifyPhone($leadNumber)];
    }

    echo json_encode($numers);
    exit;
}

echo json_encode(false);
exit;