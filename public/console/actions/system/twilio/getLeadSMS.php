<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

if(isset($_GET['fromPhoneNumberId']) && isset($_GET['toNumber'])) {

    $pagePermissions = array(false, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $fromPhoneNumberId = $_GET['fromPhoneNumberId'];
    $toNumber = $_GET['toNumber'];

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $subAccountIdByPhoneId = $organizationTwilio->getSubAccountIdByPhoneId($fromPhoneNumberId);
    $phoneNumber = $organizationTwilio->getPhoneNumbers($subAccountIdByPhoneId,$fromPhoneNumberId);

    if ($phoneNumber && count($phoneNumber) > 0) {
        $conversation = $organizationTwilio->getConversation($phoneNumber["number"], $toNumber);

        foreach ($conversation as &$item) {

            $msgDay = date("Y-m-d", strtotime($item["sentAt"]));
            $todayDay = date("Y-m-d", strtotime("now"));

            if ($msgDay == $todayDay) {
                $item["sentAt"] = date("H:i", strtotime($item["sentAt"])) . ", Today";
            } else {
                $item["sentAt"] = date("H:i, M jS", strtotime($item["sentAt"]));
            }
        }

        echo json_encode($conversation);
        exit;
    }else{
        echo json_encode([]);
        exit;
    }
}

echo json_encode(false);
exit;