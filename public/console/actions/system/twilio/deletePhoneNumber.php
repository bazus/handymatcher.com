<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

if(isset($_GET['phoneNumberId'])) {
    $pagePermissions = array(true, true,false);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    if(isset($_GET['orgId'])) {
        $orgId = $_GET['orgId'];
    }else{
        $orgId = $bouncer["credentials"]["orgId"];
    }
    $phoneNumberId = $_GET['phoneNumberId'];

    $organizationTwilio = new organizationTwilio($orgId,$bouncer["credentials"]["userId"]);

    $subAccountData = $organizationTwilio->getSubAccount();
    $phoneNumberData = $organizationTwilio->getPhoneNumbers($subAccountData["id"],$phoneNumberId);

    $client = new Client($subAccountData["SID"], $subAccountData["token"]);
    $twilio = new twilio($client);

    // delete the number from twilio
    $deletePhoneNumber = $twilio->deletePhoneNumber($phoneNumberData["SID"]);

    if($deletePhoneNumber == true){

        // save the number in our db
        $deletePhoneNumber = $organizationTwilio->deletePhoneNumber($subAccountData["id"],$phoneNumberData["SID"]);

        echo json_encode($deletePhoneNumber);
        exit;
    }
}

echo json_encode(false);
exit;