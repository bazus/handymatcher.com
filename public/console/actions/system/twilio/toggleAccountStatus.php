<?php
require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

$pagePermissions = array(true,true,false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organizationTwilio.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/twilio/twilio.php");

if(isset($_POST['status'])) {
    $orgId = $bouncer["credentials"]["orgId"];

    $organizationTwilio = new organizationTwilio($orgId, NULL);

    // get subaccount data by orgId
    $subAccountData = $organizationTwilio->getSubAccount();

    $client = new Client("ACf47e9b171a873d75115b08e5a0b0479c", "7396715a2433ee5cc491d7bf30ab1c9c");
    $twilio = new twilio($client);

    if ($_POST['status'] === "true" || $_POST['status'] === true) {
        // activate the subaccount
        $activateSubAccount = $twilio->activateSubAccount($subAccountData["SID"]);
        // activate the subaccount on our db
        $activateSubAccount = $organizationTwilio->activateSubAccount($subAccountData["SID"], $subAccountData["token"]);
    }else{
        // suspend the subaccount in twilio
        $suspendSubAccount = $twilio->suspendSubAccount($subAccountData["SID"]);
        // disable the subaccount on our db
        $disableSubAccount = $organizationTwilio->disableSubAccount($subAccountData["SID"],$subAccountData["token"]);
    }

    $subAccountData = $organizationTwilio->getSubAccount();

    echo json_encode($subAccountData['isActive']);
}else{
    echo json_encode(false);
}