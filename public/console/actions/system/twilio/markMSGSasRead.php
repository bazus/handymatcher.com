<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

if(isset($_POST['fromPhoneNumberId']) && isset($_POST['toNumber'])) {

    $pagePermissions = array(false, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $fromPhoneNumberId = $_POST['fromPhoneNumberId'];
    $toNumber = $_POST['toNumber'];

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $subAccountIdByPhoneId = $organizationTwilio->getSubAccountIdByPhoneId($fromPhoneNumberId);
    $phoneNumber = $organizationTwilio->getPhoneNumbers($subAccountIdByPhoneId,$fromPhoneNumberId);

    $markMSGSasRead = $organizationTwilio->markMSGSasRead($phoneNumber["number"],$toNumber);

    echo json_encode($markMSGSasRead);
    exit;
}

echo json_encode(false);
exit;