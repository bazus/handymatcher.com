<?php
require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organizationTwilio.php");


//This code is not available anymore since we remove the 'organizationPackageBillingDate' from database.



$organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"],$bouncer["credentials"]["userId"]);
$subAccountsData = $organizationTwilio->getSubAccount();
if($subAccountsData != false) {

    //$sDate = $bouncer['organizationData']['organizationPackageBillingDate'];
    //$eDate = $bouncer['organizationData']['subscriptionEndDate'];

    $phoneNumbersOfClient = $organizationTwilio->getPhoneNumbers($subAccountsData['id']);
    foreach ($phoneNumbersOfClient as &$phoneNumber){
        $phoneNumber['number'] = $organizationTwilio->prettifyPhone($phoneNumber['number']);
    }

    $client = new Client($subAccountsData["SID"], $subAccountsData["token"]);
    $twilio = new twilio($client);

    $subAccountsData['phoneNumbers'] = $phoneNumbersOfClient;


    foreach ($subAccountsData['phoneNumbers'] as &$phoneNumberOfClient){
        $phoneNumberOfClient['sms'] = 0; //$twilio->getTotalSmsByNumberPerDate($phoneNumberOfClient['number'],$sDate,$eDate);
    }

}

echo json_encode($subAccountsData);