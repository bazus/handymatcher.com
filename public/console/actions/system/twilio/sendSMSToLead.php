<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');


if(isset($_POST['leadId']) && isset($_POST['fromPhoneNumberId']) && isset($_POST['toNumber']) && isset($_POST['msg'])) {
    $pagePermissions = array(false, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");

    $leadId = $_POST['leadId'];
    $fromPhoneNumberId = $_POST['fromPhoneNumberId'];
    $toNumber = $_POST['toNumber'];
    $msg = $_POST['msg'];

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $sendSMS = $organizationTwilio->sendSMS($fromPhoneNumberId,$toNumber,$msg,$leadId);

    echo json_encode($sendSMS);
    exit;
}else{
    echo json_encode(false);
    exit;
}