<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

    if(isset($_GET['phoneNumber'])) {
    $pagePermissions = array(true, true,false);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $phoneNumber = $_GET['phoneNumber'];
    if (isset($_GET['state'])){
        $state = $_GET['state'];
    }else{
        $state = "";
    }
    if (isset($_GET['postalCode'])){
        $postalCode = $_GET['postalCode'];
    }else{
        $postalCode = "";
    }

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $subAccountData = $organizationTwilio->getSubAccount();

    $resp = [];
    $resp["status"] = false;
    $resp["resp"] = NULL;

    // check if has had more than 3 number already
    $purchasedPhones = $organizationTwilio->getPhoneNumbers($subAccountData["id"]);
    if(count($purchasedPhones) >= 3){
        $resp["status"] = false;
        $resp["resp"] = "maxphones";
        echo json_encode($resp);
        exit;
    }


    $client = new Client($subAccountData["SID"], $subAccountData["token"]);
    $twilio = new twilio($client);

    // purchase the number from twilio
    $r = $twilio->purchasePhoneNumber($phoneNumber);

    // save the number in our db
    $addNewPhoneNumber = $organizationTwilio->addNewPhoneNumber($subAccountData["id"],$phoneNumber,$r->sid,$state,$postalCode);

    // notify us by app about purchase
    shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => $bouncer["userData"]["fullName"]." from ".$bouncer["organizationData"]["organizationName"] . " purchased a new phone number","env"=>$_SERVER["ENVIRONMENT"]))) . "> /dev/null 2>/dev/null &");

    $resp["status"] = true;
    $resp["resp"] = $addNewPhoneNumber;
    echo json_encode($resp);
    exit;
}

echo json_encode(false);
exit;