<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

$pagePermissions = array(true, true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

$organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
$subAccountData = $organizationTwilio->getSubAccount();

$client = new Client($subAccountData["SID"], $subAccountData["token"]);
$twilio = new twilio($client);

$startDate = date("Y-m-d",strtotime("first day of this month"));
$endDate = date("Y-m-d",strtotime("last day of this month"));

//$r = $twilio->getTotalBillingPerDate($startDate,$endDate);
//var_dump($r);