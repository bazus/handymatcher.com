<?php
    $pagePermissions = array(false, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);

    $phoneNumbers = $organizationTwilio->getPhoneNumbers();
    foreach($phoneNumbers as &$phoneNumber){
        $phoneNumber["prettyNumber"] = $organizationTwilio->prettifyPhone($phoneNumber["number"]);
    }

    echo json_encode($phoneNumbers);
    exit;

echo json_encode(false);
exit;