<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

$pagePermissions = array(true, true,false);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

$byNumber = NULL;
$byAreaCode = NULL;
$searchByState = NULL;

if(isset($_GET['byNumber'])){$byNumber = $_GET['byNumber'];}
if(isset($_GET['byAreaCode'])){$byAreaCode = $_GET['byAreaCode'];}
if(isset($_GET['searchState'])){$searchByState = $_GET['searchState'];}

$client = new Client($_SERVER["TWILIO_SID"], $_SERVER["TWILIO_TOKEN"]);
$twilio = new twilio($client);

$availableNumbers = $twilio->searchAvailableNumbers($byNumber,$byAreaCode,$searchByState);

echo json_encode($availableNumbers);
exit;