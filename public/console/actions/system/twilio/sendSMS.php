<?php

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');

if(isset($_POST['fromPhoneNumberId']) && isset($_POST['toPhoneNumber']) && isset($_POST['msg'])) {
    $pagePermissions = array(false, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");

    $fromPhoneNumberId = $_POST['fromPhoneNumberId'];
    $toPhoneNumber = $_POST['toPhoneNumber'];
    $msg = $_POST['msg'];

    // ==== (START) check if has balance ====
    $totalBalanceNeeded = 0.01;
    $billing = new billing($bouncer["credentials"]["orgId"]);
    $doesHaveBalance = $billing->doesHaveBalance($totalBalanceNeeded); // The cost for a single outgoing sms
    // ==== (END) check if has balance ====


    $resp = [];
    $resp["balance"] = false;
    $resp["responses"] = false;

    if(!$doesHaveBalance){
        echo json_encode($resp);
        exit;
    }else{
        $resp["balance"] = true;
    }

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $sendSMS = $organizationTwilio->sendSMS($fromPhoneNumberId,$toPhoneNumber,$msg);

    $resp["responses"] = [$sendSMS];
    echo json_encode($resp);
    exit;
}else{
    echo json_encode(false);
    exit;
}