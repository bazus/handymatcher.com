<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
use Twilio\Rest\Client;

    $pagePermissions = array(true, true,false);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/twilio/twilio.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationTwilio.php");

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);

        $client = new Client($_SERVER["TWILIO_SID"], $_SERVER["TWILIO_TOKEN"]);
        $twilio = new twilio($client);

        $accountName = $bouncer["organizationData"]["organizationName"]." [".$bouncer["credentials"]["orgId"]."]";

        // create the sub account in twilio
        $account = $twilio->createSubAccount($accountName);

        if ($account->sid && $account->authToken) {
            // create the sub account in our database based on the data created in twilio
            $createSubAccount = $organizationTwilio->createSubAccount($account->sid, $account->authToken, $accountName);

            if($createSubAccount == true) {

                $activateSubAccount = $organizationTwilio->activateSubAccount($account->sid, $account->authToken);

                // will display true/false
                echo json_encode($activateSubAccount);
                exit;
            }
        }

echo json_encode(false);
exit;