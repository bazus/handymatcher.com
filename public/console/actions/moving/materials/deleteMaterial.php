<?php
if(isset($_POST['id'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

    $id = $_POST['id'];

    $materials = new materials($bouncer["credentials"]["orgId"]);
    $materials->deleteMaterial($id);

    echo json_encode(true);
}else{
    echo json_encode(false);
}