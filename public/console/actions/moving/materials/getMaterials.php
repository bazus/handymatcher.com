<?php

$pagePermissions = array(false,[1],true,true,[2]);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

$materials = new materials($bouncer["credentials"]["orgId"]);
$data = $materials->getMaterials();

echo json_encode($data);