<?php
if(isset($_POST['id']) && isset($_POST['title']) && isset($_POST['cost']) && isset($_POST['local']) && isset($_POST['long'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

    $id = $_POST['id'];
    $title = $_POST['title'];
    $cost = $_POST['cost'];
    $local = $_POST['local'];
    $long = $_POST['long'];

    $materials = new materials($bouncer["credentials"]["orgId"]);
    $updateMaterial = $materials->updateMaterial($id,$title,$cost,$local,$long);

    echo json_encode($updateMaterial);
}else{
    echo json_encode(false);
}
