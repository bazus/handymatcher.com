<?php
if(isset($_POST['id']) && isset($_POST['isActive'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/storage.php");

    $id = $_POST['id'];
    $isActive = $_POST['isActive'];

    $storage = new storage($bouncer["credentials"]["orgId"]);

    $storage->activeStorage($id,$isActive);
    echo json_encode(true);
}else{

}