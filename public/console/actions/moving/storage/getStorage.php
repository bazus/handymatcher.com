<?php
if(isset($_POST['storageId'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/storage.php");

    $id = $_POST['storageId'];
    $storage = new storage($bouncer["credentials"]["orgId"]);
    $data = $storage->getStorage($id);
    echo json_encode($data);
}else{
    echo json_encode(false);
}