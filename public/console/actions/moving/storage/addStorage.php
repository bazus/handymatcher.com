<?php
if(isset($_POST['title']) && isset($_POST['address']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zip'])
    && isset($_POST['phone']) && isset($_POST['contact']) && isset($_POST['email'])
    && isset($_POST['pricePer100']) && isset($_POST['pricePer1000'])
    && isset($_POST['pricePerCrate']) && isset($_POST['tax'])&& isset($_POST['lateFee']) && isset($_POST['flatRate'])){

    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/storage.php");


    $title = $_POST['title'];
    $address = $_POST['address'];
    $state = $_POST['state'];
    $city = $_POST['city'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $contact = $_POST['contact'];
    $email = $_POST['email'];
    $pricePer100 = $_POST['pricePer100'];
    if (!$pricePer100){$pricePer100 = "0.00";}
    $pricePer1000 = $_POST['pricePer1000'];
    if (!$pricePer1000){$pricePer1000 = "0.00";}
    $pricePerCrate = $_POST['pricePerCrate'];
    if (!$pricePerCrate){$pricePerCrate = "0.00";}
    $tax = $_POST['tax'];
    if (!$tax){$tax = "0.00";}
    $lateFee = $_POST['lateFee'];
    if (!$lateFee){$lateFee = "0.00";}
    $flatRate = $_POST['flatRate'];
    if (!$flatRate){$flatRate = "0.00";}

    $storage = new storage($bouncer["credentials"]["orgId"]);
    $addStorage = $storage->addStorage($bouncer["credentials"]["userId"],$title,$address,$state,$city,$zip,$phone,$contact,$email,$pricePer100,$pricePer1000,$pricePerCrate,$tax,$lateFee,$flatRate);
    echo json_encode($addStorage);
}else{
    echo json_encode(false);
}