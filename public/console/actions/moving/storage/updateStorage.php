<?php
if(isset($_POST['id']) && isset($_POST['title']) && isset($_POST['address']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zip'])
    && isset($_POST['phone']) && isset($_POST['contact']) && isset($_POST['email'])
    && isset($_POST['pricePer100']) && isset($_POST['pricePer1000'])
    && isset($_POST['pricePerCrate']) && isset($_POST['tax'])&& isset($_POST['lateFee'])&& isset($_POST['comment']) && isset($_POST['flatRate'])){

    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/storage.php");

    $id = $_POST['id'];
    $title = $_POST['title'];
    $address = $_POST['address'];
    $state = $_POST['state'];
    $city = $_POST['city'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $contact = $_POST['contact'];
    $email = $_POST['email'];
    $pricePer100 = $_POST['pricePer100'];
    $pricePer1000 = $_POST['pricePer1000'];
    $pricePerCrate = $_POST['pricePerCrate'];
    $tax = $_POST['tax'];
    $lateFee = $_POST['lateFee'];
    $comment = $_POST['comment'];
    $flatRate = $_POST['flatRate'];

    $storage = new storage($bouncer["credentials"]["orgId"]);
    $newStorageId = $storage->updateStorage($id,$title,$address,$state,$city,$zip,$phone,$contact,$email,$pricePer100,$pricePer1000,$pricePerCrate,$tax,$lateFee,$comment,$flatRate);
    echo json_encode($newStorageId);
}else{
    echo json_encode(false);
}