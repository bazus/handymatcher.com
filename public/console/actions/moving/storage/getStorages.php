<?php
$pagePermissions = array(false,[1],true,true,[2]);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/storage.php");

$storage = new storage($bouncer["credentials"]["orgId"]);
$data = $storage->getStorages();

echo json_encode($data);