<?php

if(isset($_POST['leadId']) && isset($_POST['crewId'])){

    $pagePermissions = array(false,[1],true,true,[2]);


    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/operations.php");
    $leadId = $_POST['leadId'];
    $crewId = $_POST['crewId'];

    $operations = new operations($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"],$leadId);
    $crewAvailable = $operations->checkIfCrewAssignedToJob($crewId);

    $resp = [];
    $resp["status"] = $crewAvailable;
    echo json_encode($resp);

}else {
    echo json_encode(false);
}