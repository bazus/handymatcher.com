<?php
if(isset($_POST['crewId'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/crew.php");

    $id = $_POST['crewId'];

    $crew = new crew($bouncer["credentials"]["orgId"]);
    $deleteCrewById = $crew->deleteCrewById($id);
    echo json_encode($deleteCrewById);
}else{
    echo json_encode(false);
}