<?php
if(isset($_POST['crewId']) && isset($_POST['fullname']) && $_POST['fullname'] != "" && isset($_POST['email']) && isset($_POST['birthDate']) && isset($_POST['phone']) && isset($_POST['address']) && isset($_POST['type']) && $_POST['type'] != "" && isset($_POST['comments'])){
    $pagePermissions = array(false,[1],true,true,[2]);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/crew.php");

    $memberId = $_POST['crewId'];
    $memberFullname = $_POST['fullname'];
    $memberEmail = $_POST['email'];
    $memberBirthDate = $_POST['birthDate'];
    $memberPhone = $_POST['phone'];
    $memberAddress = $_POST['address'];
    $memberType = $_POST['type'];
    $memberComments = $_POST['comments'];

    $crew = new crew($bouncer["credentials"]["orgId"]);
    $updateCrew = $crew->updateCrew($memberId,$memberFullname,$memberEmail,$memberBirthDate,$memberPhone,$memberAddress,$memberType,$memberComments);

    echo json_encode($updateCrew);

}else{
    echo json_encode(false);
}