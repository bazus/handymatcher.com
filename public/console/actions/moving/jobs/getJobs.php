<?php

$pagePermissions = array(false,true,true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobBoard.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");

$jobBoard = new jobBoard($bouncer["credentials"]["orgId"]);
$jobs = $jobBoard->getJobsForBoard();

echo json_encode($jobs);
exit;