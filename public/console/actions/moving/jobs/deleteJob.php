<?php
if (isset($_POST['jobId'])){

    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobBoard.php");

    $jobId = $_POST['jobId'];
    $jobBoard = new jobBoard($bouncer["credentials"]['orgId']);

    $deleteJob = $jobBoard->setJobInBoardAsDeleted($jobId);
    echo json_encode($deleteJob);

}else{
    echo json_encode(false);
}