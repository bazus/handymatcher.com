<?php
if (isset($_POST['leadId']) && isset($_POST['carrierBalance'])){

    $leadId = $_POST['leadId'];
    $carrierBalance = $_POST['carrierBalance'];

    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobBoard.php");

    $leadId = $_POST['leadId'];
    $jobBoard = new jobBoard($bouncer["credentials"]['orgId']);

    $updateCarrierBalance = $jobBoard->updateCarrierBalanceJobInBoard($leadId,$carrierBalance);
    echo json_encode($updateCarrierBalance);

}else{
    echo json_encode(false);
}