<?php
if (isset($_POST['leadId'])){

    $leadId = $_POST['leadId'];

    $pagePermissions = array(false,true,true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobBoard.php");

    $jobBoard = new jobBoard($bouncer["credentials"]["orgId"]);
    $singleJobInBoard = $jobBoard->getSingleJobInBoardByLeadId($leadId);

    echo json_encode($singleJobInBoard);

}else{
    echo json_encode(false);
}