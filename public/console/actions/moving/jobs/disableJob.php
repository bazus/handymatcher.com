<?php
if (isset($_POST['jobId'])){

    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobBoard.php");

    $jobId = $_POST['jobId'];
    $jobBoard = new jobBoard($bouncer["credentials"]['orgId']);

    $disableJob = $jobBoard->setJobInBoardAsDisabled($jobId);
    echo json_encode($disableJob);

}else{
    echo json_encode(false);
}