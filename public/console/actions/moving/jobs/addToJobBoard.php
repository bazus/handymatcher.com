<?php

$resp = [];
$resp["status"] = false;
$resp["reason"] = "";


$pagePermissions = array(false,true,true,true);
if(isset($_POST['leadId']) && isset($_POST['carrierBalance'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobBoard.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");

    $leadId = $_POST['leadId'];
    $carrierBalance = $_POST['carrierBalance'];

    // Check if has active estimate
    $estimateCalculation = new estimateCalculation($leadId,$bouncer["credentials"]["orgId"]);
    $estimateCalculationData = $estimateCalculation->getData();

    if(!$estimateCalculationData){
        $resp["status"] = false;
        $resp["reason"] = "You must have a signed estimate in order to add a job to the board";
        echo json_encode($resp);
        exit;
    }

    // Add job to board
    $jobBoard = new jobBoard($bouncer["credentials"]["orgId"]);
    $addToJobBoard = $jobBoard->addToJobBoard($leadId,$carrierBalance);

    if($addToJobBoard){
        $resp["status"] = true;
        echo json_encode($resp);
        exit;
    }else{
        $resp["status"] = false;
        $resp["reason"] = "";
        echo json_encode($resp);
        exit;
    }

    echo json_encode($addToJobBoard);

}else{
    $resp["status"] = false;
    $resp["reason"] = "General error";
    echo json_encode($resp);
    exit;
}