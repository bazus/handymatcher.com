<?php
if(isset($_POST['itemId']) && isset($_POST['title']) && isset($_POST['cf']) && isset($_POST['groupId'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

    $itemId = $_POST['itemId'];
    $title = $_POST['title'];
    $cf = $_POST['cf'];
    $groupId = $_POST['groupId'];

    $inventory = new inventory($bouncer["credentials"]["orgId"]);
    $item = $inventory->updateItem($itemId,$title,$cf,$groupId);

    echo json_encode($item);
}else{
    echo json_encode(false);
}
