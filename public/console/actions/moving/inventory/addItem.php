<?php
if(isset($_POST['groupId']) && isset($_POST['title']) && isset($_POST['cf'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

    $groupId = $_POST['groupId'];
    $title = $_POST['title'];
    $cf = $_POST['cf'];
    $userId = $bouncer["credentials"]["userId"];

    $inventory = new inventory($bouncer["credentials"]["orgId"]);
    $item = $inventory->addItem($groupId,$title,$cf,$userId);

    echo json_encode($item);
}else{
    echo json_encode(false);
}