<?php
$pagePermissions = array(false,[1],true,true,[2]);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

$inventory = new inventory($bouncer["credentials"]["orgId"]);
$groups = $inventory->getGroups();

echo json_encode($groups);