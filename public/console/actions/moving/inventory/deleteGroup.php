<?php
if(isset($_POST['id'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

    $id = $_POST['id'];

    $inventory = new inventory($bouncer["credentials"]["orgId"]);
    $item = $inventory->deleteGroup($id);

    echo json_encode($item);
}else{
    echo json_encode(false);
}