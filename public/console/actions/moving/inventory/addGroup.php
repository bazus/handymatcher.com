<?php
if(isset($_POST['title'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

    $title = $_POST['title'];

    $inventory = new inventory($bouncer["credentials"]["orgId"]);
    $group = $inventory->addGroup($title,$bouncer["credentials"]["userId"]);

    echo json_encode($group);
}else{
    echo json_encode(false);
}