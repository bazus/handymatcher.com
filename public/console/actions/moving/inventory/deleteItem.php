<?php
if(isset($_POST['itemId'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

    $itemId = $_POST['itemId'];

    $inventory = new inventory($bouncer["credentials"]["orgId"]);
    $item = $inventory->deleteItem($itemId);

    echo json_encode($item);
}else{
    echo json_encode(false);
}