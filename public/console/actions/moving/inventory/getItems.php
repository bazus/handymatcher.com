<?php
if(isset($_POST['id'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");

    $id = $_POST['id'];

    $inventory = new inventory($bouncer["credentials"]["orgId"]);
    $items = $inventory->getItems($id);

    echo json_encode($items);
}else{
    echo json_encode(false);
}