<?php
if (isset($_POST['data']) && isset($_POST['leadId'])){
    $pagePermissions = array(false,array(1));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/passwords.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");

    $passwords = new passwords();

    $data = $_POST['data'];
    $leadId = $_POST['leadId'];

    $payments = new payments($bouncer["credentials"]['orgId']);
    if ($data['method'] == 1){
        if ($data['cardHolder'] == ""){
            echo json_encode(false);
            exit;
        }
        if ($data['creditCardNumber'] == ""){
            echo json_encode(false);
            exit;
        }
        if ($data['securityCode'] == ""){
            echo json_encode(false);
            exit;
        }
    }
    $data['creditCardNumber'] = ($passwords->hashCC($data['creditCardNumber']));

    $saveData = $payments->saveData($data,$bouncer["credentials"]["userId"],$leadId);
    if ($saveData){
        echo json_encode($saveData);
    }else{
        echo json_encode(false);
    }

}else{
    echo json_encode(false);
}