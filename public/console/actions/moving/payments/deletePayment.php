<?php
if (isset($_POST['id'])){

    $pagePermissions = array(false,array(1));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");


    $paymentId = $_POST['id'];
    $payments = new payments($bouncer["credentials"]['orgId']);

    $deletePayment = $payments->deletePayment($paymentId);
    echo json_encode($deletePayment);


}else{
    echo json_encode(false);
}