<?php

if (isset($_POST['data']) && isset($_POST['id'])){
    $pagePermissions = array(false,array(1));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");

    $data = $_POST['data'];

    $payments = new payments($bouncer["credentials"]['orgId']);

    $updateData = $payments->updateData($data,$_POST['id']);
    if ($updateData){
        echo json_encode($updateData);
    }else{
        echo json_encode(false);
    }

}else{
    echo json_encode(false);
}