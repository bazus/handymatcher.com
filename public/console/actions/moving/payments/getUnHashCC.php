<?php

if(isset($_POST['card']) && $_POST['card'] != ""){
    $pagePermissions = array(false,array(1));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'].'/console/classes/security/passwords.php');

    $cardNumber = $_POST['card'];

    $passwords = new passwords();
    $unHashedCC = $passwords->unHashCC($cardNumber);

    echo json_encode($unHashedCC);

}else{
    echo json_encode(false);
}