<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 01/10/2018
 * Time: 23:26
 */
$pagePermissions = array(false,array(1));

if(isset($_POST['id']) && isset($_POST['status'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadLog.php");

    $leadId = $_POST['id'];
    $status = $_POST['status'];
    $statusText = "";

    switch($status){
        case "0":
            $statusText = "New";
            break;
        case "1":
            $statusText = "Pending";
            break;
        case "2":
            $statusText = "Quoted";
            break;
        case "3":
            $statusText = "Booked";
            break;
        case "4":
            $statusText = "In Progress";
            break;
        case "5":
            $statusText = "Storage/Transit";
            break;
        case "6":
            $statusText = "Completed";
            break;
        case "7":
            $statusText = "Cancelled";
            break;
    }

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    if ($movingLead->checkLeadOrganization()){
        if($movingLead->setLeadStatus($status)){

            // Update the lead log
            $leadLog = new leadLog();
            $leadLog->addLog($leadId,"Changed status to ".$statusText,$bouncer["credentials"]["userId"],true);

            echo json_encode(true);
        }else{
            echo json_encode(false);
        }
    }else{
        echo json_encode(false);
    }

}else{
    echo json_encode(false);
}
