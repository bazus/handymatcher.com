<?php
if(isset($_POST['formId'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobAcceptanceForms.php");

    $formId = $_POST['formId'];

    $jobAcceptanceForms = new jobAcceptanceForms($bouncer["credentials"]["orgId"]);
    $deleteForm = $jobAcceptanceForms->deleteForm($formId);

    echo json_encode($deleteForm);
}else{
    echo json_encode(false);
}