<?php
if(isset($_POST['leadId']) && isset($_POST['carrierId']) && isset($_POST['carrierBalance'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobAcceptanceForms.php");

    $leadId = $_POST['leadId'];
    $carrierId = $_POST['carrierId'];
    $carrierBalance = $_POST['carrierBalance'];

    $jobAcceptanceForms = new jobAcceptanceForms($bouncer["credentials"]["orgId"]);
    $createForm = $jobAcceptanceForms->createForm($leadId,$carrierId,$carrierBalance);

    if(isset($_POST["returnFormData"])){
        $formData = $jobAcceptanceForms->getSingleJobAcceptanceForm($createForm);
        echo json_encode($formData);
        exit;
    }else{
        echo json_encode($createForm);
        exit;
    }
}else{
    echo json_encode(false);
    exit;
}