<?php
if(isset($_GET['leadId'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobAcceptanceForms.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

    $leadId = $_GET['leadId'];

    $jobAcceptanceForms = new jobAcceptanceForms($bouncer["credentials"]["orgId"]);
    $forms = $jobAcceptanceForms->getJobAcceptanceForms($leadId);

    /*
    $carriers = new carriers($bouncer["credentials"]["orgId"]);

    for($i = 0;$i<count($forms);$i++){
        $carrierData = $carriers->getSingleCarrier($forms[$i]["carrierId"]);
        $forms[$i]["carrierData"] = $carrierData;
    }
    */
    echo json_encode($forms);
}else{
    echo json_encode(false);
}