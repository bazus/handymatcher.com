<?php

$pagePermissions = array(false,true,true,true);
if(isset($_POST['estimateId']) && isset($_POST['leadId'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");

    $leadId = $_POST['leadId'];
    $estimateId = $_POST['estimateId'];

    $estimateCalculation = new estimateCalculation($leadId,$bouncer["credentials"]["orgId"]);
    $disableEstimate = $estimateCalculation->disableEstimate($estimateId);

    echo json_encode($disableEstimate);

}else{
    echo json_encode(false);
}
