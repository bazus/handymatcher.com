<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 01/10/2018
 * Time: 23:26
 */
$pagePermissions = array(false,array(1));

if(isset($_POST['leadId']) && isset($_POST['movingData'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");

    $leadId = $_POST['leadId'];
    $movingData = $_POST['movingData'];
    $movingData['autoTransport'] = serialize($movingData['autoTransport']);

    $movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
    $movingSettingsData = $movingSettings->getData();
    if ($movingData['moveSizeNumber']){
        $moveSizeLBS = $movingData['moveSizeNumber'];
    }else{
        $moveSizeLBS = 0;
    }
    if($movingSettingsData["calculateBy"] == "1" && $movingData['moveSizeNumber'] && $movingData['moveSizeNumber'] > 0){
        $moveSizeLBS = $movingData['moveSizeNumber'] * $movingSettingsData["cflbsratio"];
    }else{
        $moveSizeLBS = 0;
    }

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    if ($movingLead->checkLeadOrganization()){
        if($movingLead->updateMovingDetails($movingData['fromZip'],$movingData['toZip'],$movingData['fromState'],$movingData['toState'],$movingData['fromCity'],$movingData['toCity'],$movingData['fromAddress'],$movingData['toAddress'],$movingData['moveDate'],$movingData['moveSize'],$moveSizeLBS,$movingData['fromLevel'],$movingData['fromFloor'],$movingData['fromAptNumber'],$movingData['toAptNumber'],$movingData['fromAptType'],$movingData['toAptType'],$movingData['toLevel'],$movingData['toFloor'],$movingData['typeOfMove'],$movingData['autoTransport'],$movingData["estimator"],$movingData["needStorage"],$movingData["bindingType"])){
            echo json_encode(true);
        }else{
            echo json_encode(false);
        }
    }else{
        echo json_encode(false);
    }

}else{
    echo json_encode(false);
}
