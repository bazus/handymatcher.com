<?php

$pagePermissions = array(false,true,true,true);

if(isset($_POST['leadId']) && isset($_POST['data'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");

    $leadId = $_POST['leadId'];

    if(!isset($_POST['estimateId']) || $_POST['estimateId'] == "" || $_POST['estimateId'] == "false" || $_POST['estimateId'] == false){
        $estimateId = NULL;
    }else{
        $estimateId = $_POST['estimateId'];
    }

    $data = $_POST['data'];

    if(isset($data['extraCharges'])) {
        $data['extraCharges'] = serialize($data['extraCharges']);
    }else{
        $data['extraCharges'] = serialize(array());
    }

    if(isset($data['materials'])) {
        $data['materials'] = serialize($data['materials']);
    }else{
        $data['materials'] = serialize(array());
    }

    if(isset($data['attachedFiles'])) {
        $data['attachedFiles'] = serialize($data['attachedFiles']);
    }else{
        $data['attachedFiles'] = serialize(array());
    }

    // no coupons
    if ($data['coupon'] == null){
        $data['coupon'] = NULL;
    }
    if ($data['senior'] == null){
        $data['senior'] = NULL;
    }
    if ($data['veteranDiscount'] == null){
        $data['veteranDiscount'] = NULL;
    }

    // no packers
    if ($data['packingPackers'] == null){
        $data['packingPackers'] = NULL;
    }
    if ($data['packingHours'] == null){
        $data['packingHours'] = NULL;
    }
    if ($data['packingPerHour'] == null){
        $data['packingPerHour'] = NULL;
    }

    if ($data['unpackingPackers'] == null){
        $data['unpackingPackers'] = NULL;
    }
    if ($data['unpackingHours'] == null){
        $data['unpackingHours'] = NULL;
    }
    if ($data['unpackingPerHour'] == null){
        $data['unpackingPerHour'] = NULL;
    }
    if (!isset($data['customerSignature']) || $data['customerSignature'] == null){
        $data['customerSignature'] = NULL;
    }
    if (!isset($data['customerName']) || $data['customerName'] == null){
        $data['customerName'] = NULL;
    }
    if (!isset($data['dateSigned']) || $data['dateSigned'] == null){
        $data['dateSigned'] = NULL;
    }
    if (!isset($data['showEstimateTerms']) || $data['showEstimateTerms'] == null){
        $data['showEstimateTerms'] = false;
    }
    if (!isset($data['showEstimateSign']) || $data['showEstimateSign'] == null){
        $data['showEstimateSign'] = false;
    }
    if (!isset($data['showEstimateInventory']) || $data['showEstimateInventory'] == null){
        $data['showEstimateInventory'] = false;
    }
    if (!isset($data['showEstimatePayments']) || $data['showEstimatePayments'] == null){
        $data['showEstimatePayments'] = false;
    }
    if (!isset($data['estimateComments']) || $data['estimateComments'] == null){
        $data['estimateComments'] = "";
    }

    $estimateCalculation = new estimateCalculation($leadId,$bouncer["credentials"]["orgId"]);
    $setData = $estimateCalculation->setData($estimateId,$data);
    echo json_encode($setData);
}else{
    echo json_encode(false);
}
