<?php

if(isset($_POST['leadId'])){

    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

    $leadId = $_POST['leadId'];

    $materials = new materials($bouncer["credentials"]["orgId"]);

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    $leadMaterials = $movingLead->getMaterials();


    foreach ($leadMaterials as &$moveMaterial) {
        $materialData = $materials->getMaterial($moveMaterial['id'], true);
        $moveMaterial['name'] = $materialData['title'];
    }

    echo json_encode($leadMaterials);

}else{
    echo json_encode(false);
}
