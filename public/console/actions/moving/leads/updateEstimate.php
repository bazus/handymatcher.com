<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 01/10/2018
 * Time: 23:26
 */
$pagePermissions = array(false,array(1));

if(isset($_POST['leadId'])) {

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");

    $orgId = $bouncer["credentials"]["orgId"];
    $leadId = $_POST['leadId'];

    $movingLead = new movingLead($leadId,$orgId);

    $data = ["updateLBS"=>false,"updateInventory"=>false];
    $items = "";
    $totalCF = 0;

    if(isset($_POST['items'])) {
        $items = serialize($_POST['items']);
    }
    if (isset($_POST['totalCF'])){
        $totalCF = $_POST['totalCF'];
    }

    // convert the weight from cd to LBS
    $movingSettings = new movingSettings($orgId);

    $data['updateInventory'] = $movingLead->updateInventory($items);
    $data['cf'] = $totalCF;
    $data['lbs'] = $movingSettings->convertWeightToLBS($totalCF);
    $data['updateLBS'] = $movingLead->setLeadWeight($data['lbs']);


    echo json_encode($data);
}else{
    echo json_encode(false);
}