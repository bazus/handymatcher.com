<?php

if(isset($_POST['leadId'])){

    $pagePermissions = array(false,array(1),true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");

    $leadId = $_POST['leadId'];
    $data = [];

    $materials = new materials($bouncer["credentials"]["orgId"]);
    $data['organizationMaterials'] = $materials->getMaterials();

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);
    $leadMaterials = $movingLead->getMaterials();


    foreach ($leadMaterials as &$moveMaterial) {
        $materialData = $materials->getMaterial($moveMaterial['id'], true);
        $moveMaterial['name'] = $materialData['title'];
    }

    $data['leadMaterials'] = $leadMaterials;

    echo json_encode($data);

}else{
    echo json_encode(false);
}
