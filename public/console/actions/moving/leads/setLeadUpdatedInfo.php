<?php

$pagePermissions = array(false,array(1));

if(isset($_POST['leadId']) && isset($_POST['type'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadDataConfirmation.php");

    $leadId = $_POST['leadId'];
    $type = $_POST['type'];
    $orgId = $bouncer["credentials"]["orgId"];
    $movingLead = new movingLead($leadId,$orgId);
    $leads = new leads($orgId);
    $leadDataConfirmation = new leadDataConfirmation(false);

    $updatedData = $leadDataConfirmation->getData($leadId);

    if ($type == "lead"){
        $leads->setUpdateInfo($updatedData['firstname'],$updatedData['lastname'],$updatedData['phone'],$updatedData['email'],$updatedData['comments'],$leadId);
    }elseif ($type == "moving"){
        $movingLead->setUpdateInfo($updatedData['fromState'],$updatedData['toState'],$updatedData['fromCity'],$updatedData['toCity'],$updatedData['fromZip'],$updatedData['toZip'],$updatedData['fromAddress'],$updatedData['toAddress'],$updatedData['fromLevel'],$updatedData['toLevel'],$updatedData['fromFloor'],$updatedData['toFloor'],$updatedData['fromAptNumber'],$updatedData['toAptNumber'],$updatedData['movingDate'],$updatedData['needStorage']);
    }
    echo json_encode(true);

}else{
    echo json_encode(false);
}
