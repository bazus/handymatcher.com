<?php
/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 17/03/2019
 * Time: 17:49
 */
$pagePermissions = array(false,array(1));

if(isset($_POST['leadId'])) {

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");

    $orgId = $bouncer["credentials"]["orgId"];
    $leadId = $_POST['leadId'];

    // get inventory weight in CF
    $movingLead = new movingLead($leadId,$orgId);
    $inventoryTotalWeight = $movingLead->getInventoryTotalWeight();

    // convert the weight from cf to LBS
    $movingSettings = new movingSettings($orgId);
    $weight = $movingSettings->convertWeightToLBS($inventoryTotalWeight);

    // update the lead weight
    $setLeadWeight = $movingLead->setLeadWeight($weight);


    echo json_encode($setLeadWeight);
}else{
    echo json_encode(false);
}