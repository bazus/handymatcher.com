<?php
// NOTE : this is updating the left side of the lead page - not the estimate
// The file name is misleading

$pagePermissions = array(false,true,true,true);

if(isset($_POST['id']) && isset($_POST['estimateData'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leads.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

    $leadId = $_POST['id'];

    $estimateData = $_POST['estimateData'];

    if ($estimateData['includeBoxDeliveryDate'] == "true"){
        $estimateData['includeBoxDeliveryDate'] = 1;
    }else{
        $estimateData['includeBoxDeliveryDate'] = 0;
    }
    if ($estimateData['includePickupDate'] == "true"){
        $estimateData['includePickupDate'] = 1;
    }else{
        $estimateData['includePickupDate'] = 0;
    }
    if ($estimateData['includeRequestDeliveryDate'] == "true"){
        $estimateData['includeRequestDeliveryDate'] = 1;
    }else{
        $estimateData['includeRequestDeliveryDate'] = 0;
    }

    $estimateData['boxDeliveryDate'] = explode("-",$estimateData['boxDeliveryDate']);
    $estimateData['boxDeliveryDateStart'] = date("Y-m-d H:i:s",strtotime($estimateData['boxDeliveryDate'][0]));
    $estimateData['boxDeliveryDateEnd'] = date("Y-m-d H:i:s",strtotime($estimateData['boxDeliveryDate'][1]));
    unset($estimateData['boxDeliveryDate']);

    $estimateData['pickupDate'] = explode("-",$estimateData['pickupDate']);
    $estimateData['pickupDateStart'] = date("Y-m-d H:i:s",strtotime($estimateData['pickupDate'][0]));
    $estimateData['pickupDateEnd'] = date("Y-m-d H:i:s",strtotime($estimateData['pickupDate'][1]));
    unset($estimateData['pickupDate']);

    $estimateData['requestedDeliveryDateStart'] = explode("-",$estimateData['requestedDeliveryDateStart']);
    $estimateData['requestedDeliveryDateEnd'] = date("Y-m-d H:i:s",strtotime($estimateData['requestedDeliveryDateStart'][1]));
    $estimateData['requestedDeliveryDateStart'] = date("Y-m-d H:i:s",strtotime($estimateData['requestedDeliveryDateStart'][0]));

    if ($estimateData['departmentId'] == ""){$estimateData['departmentId'] = null;}

    $movingLead = new movingLead($leadId,$bouncer["credentials"]["orgId"]);

    if(isset($estimateData["userHandeling"]) && $bouncer["isUserAnAdmin"] == "1"){
        if ($estimateData['userHandeling'] == ""){$estimateData['userHandeling'] = null;}

        $lead = new lead($leadId,$bouncer["credentials"]["orgId"]);
        $updateUserIdAssigned = $lead->updateUserIdAssigned($estimateData['userHandeling']);
    }

    echo json_encode($movingLead->updateEstimateDetails($estimateData));



}