<?php
if(isset($_POST['terms'])) {
    $pagePermissions = array(false, [1], true, true, [2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");

    $terms = $_POST['terms'];
    $movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
    $res = $movingSettings->setJobAcceptanceTerms($terms);

    echo json_encode($res);
}