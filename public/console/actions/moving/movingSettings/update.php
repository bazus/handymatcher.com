<?php
$pagePermissions = array(false,[1],true,true,[2]);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingSettings.php");

$data = $_POST['data'];
if (!$data['estimateEmailAddressId']){
    $data['estimateEmailAddressId'] = NULL;
}
$movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
$res = $movingSettings->setMovingSettings($data);

echo json_encode($res);