<?php

if(isset($_POST['states'])) {
    $pagePermissions = array(false, [1], true, true, [2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");

    $states = $_POST['states'];

    $data = [];
    $data["AL"] = ["cf"=>0];
    $data["AK"] = ["cf"=>0];
    $data["AZ"] = ["cf"=>0];
    $data["AR"] = ["cf"=>0];
    $data["CA"] = ["cf"=>0];
    $data["CO"] = ["cf"=>0];
    $data["CT"] = ["cf"=>0];
    $data["DC"] = ["cf"=>0];
    $data["DE"] = ["cf"=>0];
    $data["FL"] = ["cf"=>0];
    $data["GA"] = ["cf"=>0];
    $data["HI"] = ["cf"=>0];
    $data["IA"] = ["cf"=>0];
    $data["ID"] = ["cf"=>0];
    $data["IL"] = ["cf"=>0];
    $data["IN"] = ["cf"=>0];
    $data["KS"] = ["cf"=>0];
    $data["KY"] = ["cf"=>0];
    $data["LA"] = ["cf"=>0];
    $data["MA"] = ["cf"=>0];
    $data["MD"] = ["cf"=>0];
    $data["ME"] = ["cf"=>0];
    $data["MI"] = ["cf"=>0];
    $data["MN"] = ["cf"=>0];
    $data["MO"] = ["cf"=>0];
    $data["MS"] = ["cf"=>0];
    $data["MT"] = ["cf"=>0];
    $data["NC"] = ["cf"=>0];
    $data["ND"] = ["cf"=>0];
    $data["NE"] = ["cf"=>0];
    $data["NH"] = ["cf"=>0];
    $data["NJ"] = ["cf"=>0];
    $data["NM"] = ["cf"=>0];
    $data["NV"] = ["cf"=>0];
    $data["NY"] = ["cf"=>0];
    $data["OH"] = ["cf"=>0];
    $data["OK"] = ["cf"=>0];
    $data["OR"] = ["cf"=>0];
    $data["PA"] = ["cf"=>0];
    $data["RI"] = ["cf"=>0];
    $data["SC"] = ["cf"=>0];
    $data["SD"] = ["cf"=>0];
    $data["TN"] = ["cf"=>0];
    $data["TX"] = ["cf"=>0];
    $data["UT"] = ["cf"=>0];
    $data["VA"] = ["cf"=>0];
    $data["VT"] = ["cf"=>0];
    $data["WA"] = ["cf"=>0];
    $data["WI"] = ["cf"=>0];
    $data["WV"] = ["cf"=>0];
    $data["WY"] = ["cf"=>0];

    foreach ($states as $state=>$prices){
        $data[$state]["cf"] = $prices["cfPrice"];
    }

    $movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
    $updateTariff = $movingSettings->updateTariff($data);

    echo json_encode($updateTariff);
}