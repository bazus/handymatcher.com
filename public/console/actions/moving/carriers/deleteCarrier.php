<?php
if(isset($_POST['carrierId'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

    $id = $_POST['carrierId'];

    $carriers = new carriers($bouncer["credentials"]["orgId"]);
    $deleteCarrierById = $carriers->deleteCarrierById($id);
    echo json_encode($deleteCarrierById);
}else{
    echo json_encode(false);
}