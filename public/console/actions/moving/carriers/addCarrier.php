<?php
if(isset($_POST['carrierName']) && isset($_POST['contactName']) && isset($_POST['carrierEmail']) && isset($_POST['carrierAddress']) && isset($_POST['carrierCity']) && isset($_POST['carrierZip']) && isset($_POST['carrierState']) && isset($_POST['carrierCountry']) && isset($_POST['carrierDOT']) && isset($_POST['carrierICCMC']) && isset($_POST['carrierRegistration']) && isset($_POST['carrierPhone1']) && isset($_POST['carrierPhone2']) && isset($_POST['carrierFax']) && isset($_POST['carrierWebsite']) && isset($_POST['carrierType']) && isset($_POST['carrierComments'])){
    $pagePermissions = array(false,[1],true,true,[2]);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

    require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

    $s3 = new s3();

    $carrierName = $_POST['carrierName'];
    $contactName = $_POST['contactName'];
    $carrierEmail = $_POST['carrierEmail'];
    $carrierAddress = $_POST['carrierAddress'];
    $carrierCity = $_POST['carrierCity'];
    $carrierZip = $_POST['carrierZip'];
    $carrierState = $_POST['carrierState'];
    $carrierCountry = $_POST['carrierCountry'];
    $carrierDOT = $_POST['carrierDOT'];
    $carrierICCMC = $_POST['carrierICCMC'];
    $carrierRegistration = $_POST['carrierRegistration'];
    $carrierPhone1 = $_POST['carrierPhone1'];
    $carrierPhone2 = $_POST['carrierPhone2'];
    $carrierFax = $_POST['carrierFax'];
    $carrierWebsite = $_POST['carrierWebsite'];
    $carrierType = $_POST['carrierType'];
    $carrierComments = $_POST['carrierComments'];

    $agreementFileName = "";
    $agreementFileKey = "";
    $agreementFileURL = "";
    $resp = [];
    $resp["status"] = false;
    $resp["reason"] = "";

    $final_target_file = "";
    $uploadOk = 1;
    $collectData = [];
    try {

        if(isset($_FILES["agreemtnFile"]) && count($_FILES["agreemtnFile"]) > 0) {

            if (intval($_SERVER['CONTENT_LENGTH']) > 0 && count($_POST) === 0) {
                $resp["reason"] = "Couldn't upload file";
                echo json_encode($resp);
                exit;
            } else {

                if ($_FILES["agreemtnFile"]["tmp_name"] != "") {

                    $thisDate = date("Y.m.d.H-i-s", strtotime("now"));

                    $filePathInfo = pathinfo($_FILES["agreemtnFile"]["name"]);
                    $newFileName = $bouncer["credentials"]["userId"] . "." . hash("md5", $filePathInfo["filename"]) . $thisDate . "." . $filePathInfo["extension"];

                    $target_dir = $_SERVER['LOCAL_NL_PATH'] . "/console/files/leads/";
                    $target_file = $target_dir . $newFileName;
                    $final_target_file = $_SERVER['LOCAL_NL_PATH'] . "/console/files/leads/" . $newFileName;
                    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

                    // max file size in MB
                    $maxFileSize = 10;
                    if ($_FILES["agreemtnFile"]['size'] > ($maxFileSize * 1024 * 1024)) {
                        $resp["reason"] = "Sorry, file size too big";
                        echo json_encode($resp);
                        exit;
                    }
                    if ($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG"
                        && $imageFileType != "gif" && $imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "xls"
                        && $imageFileType != "xlsx" && $imageFileType != "mp3" && $imageFileType != "wav" && $imageFileType != "numbers" && $imageFileType != "NUMBERS" && $imageFileType != "pages" && $imageFileType != "PAGES") {

                        $resp["reason"] = "Sorry, only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed";
                        echo json_encode($resp);
                        exit;
                    }
                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        $resp["reason"] = "Couldn't upload file";
                        echo json_encode($resp);
                        exit;
                    } else {
                        // if everything is ok, try to upload file
                        $fileType = false;
                        if ($imageFileType == "jpg" || $imageFileType == "JPG" || $imageFileType == "jpeg" || $imageFileType == "JPEG") {
                            $fileType = "image/jpeg";
                        }
                        if ($imageFileType == "png" || $imageFileType == "PNG") {
                            $fileType = "image/png";
                        }


                        $file_name = $newFileName;
                        $temp_file_location = $_FILES["agreemtnFile"]['tmp_name'];

                        $result = $s3->putObject("thenetworkleads", "carriersAgreements/" . $file_name, $temp_file_location, "private", $fileType);
                        if ($result->get("@metadata")['statusCode'] == 200) {

                            $agreementFileName = $filePathInfo["filename"].".".$filePathInfo["extension"];
                            $agreementFileKey = "carriersAgreements/" . $file_name;
                            $agreementFileURL = $result->get("@metadata")['effectiveUri'];

                        } else {
                            $resp["reason"] = "Couldn't upload file";
                            echo json_encode($resp);
                            exit;
                        }

                    }
                }
            }
        }
    }catch (Exception $e){
        $resp["reason"] = "Couldn't upload file";
        echo json_encode($resp);
        exit;
    }



    $carriers = new carriers($bouncer["credentials"]["orgId"]);
    $addCarrier = $carriers->addCarrier($carrierName,$contactName,$carrierAddress,$carrierCity,$carrierState,$carrierZip,$carrierCountry,$carrierPhone1,$carrierPhone2,$carrierFax,$carrierEmail,$carrierWebsite,$carrierDOT,$carrierICCMC,$carrierRegistration,$carrierType,$carrierComments,$agreementFileName,$agreementFileKey,$agreementFileURL);

    if(!$addCarrier){
        $resp["reason"] = "Error adding carrier";
    }else{
        $resp["status"] = true;
    }
    echo json_encode($resp);

}else{
    echo json_encode(false);
}