<?php
$pagePermissions = array(false,[1],true,true,[2]);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

$onlyActives = false;
if(isset($_GET["onlyActives"])){
    $onlyActives = true;
}
$trucks = new carriers($bouncer["credentials"]["orgId"]);
$data = $trucks->getCarriers($onlyActives);
echo json_encode($data);