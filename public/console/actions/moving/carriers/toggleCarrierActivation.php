<?php
if(isset($_POST['id']) && isset($_POST['isActive'])){

    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

    $id = $_POST['id'];
    $isActive = ($_POST['isActive'] == 'true') ? true : false;

    $trucks = new carriers($bouncer["credentials"]["orgId"]);
    $setCarrierStatus = $trucks->setCarrierStatus($id,$isActive);
    echo json_encode($setCarrierStatus);
}else{

}