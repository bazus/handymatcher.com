<?php

if(isset($_POST["check"]) && $_POST["check"] == "1") {

    // The $_POST["check"] is to make sure this request was called on purpose by an ajax and not by mistake.

    $pagePermissions = array(false, true, true, true, [2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/fullvalueprotection.php");

    $fullvalueprotection = new fullvalueprotection($bouncer["credentials"]["orgId"]);

    $data = $fullvalueprotection->getDefaultData();
    $update = $fullvalueprotection->update($data["deductiblelevels"], $data["amountofliability"], $data["rates"]);

    echo json_encode($update);
    exit;

}