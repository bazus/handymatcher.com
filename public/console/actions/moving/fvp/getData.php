<?php
    $pagePermissions = array(false,true,true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/fullvalueprotection.php");

    $fullvalueprotection = new fullvalueprotection($bouncer["credentials"]["orgId"]);
    $fullvalueprotectionData = $fullvalueprotection->getData();

    echo json_encode($fullvalueprotectionData);
    exit;