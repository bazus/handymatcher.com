<?php

if(isset($_POST["levels"]) && isset($_POST["aol"]) && isset($_POST["rate"])) {

    $pagePermissions = array(false, true, true, true, [2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/fullvalueprotection.php");

    $fullvalueprotection = new fullvalueprotection($bouncer["credentials"]["orgId"]);

    $deductiblelevels = $_POST["levels"];
    $amountofliability = $_POST["aol"];
    $rates = $_POST["rate"];

    $update = $fullvalueprotection->update($deductiblelevels,$amountofliability,$rates);

    echo json_encode($update);
    exit;
}