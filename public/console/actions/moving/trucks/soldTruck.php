<?php
if(isset($_POST['id']) && isset($_POST['isSold'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");

    $id = $_POST['id'];
    $isSold = $_POST['isSold'];

    $trucks = new trucks($bouncer["credentials"]["orgId"]);
    $trucks->soldTruck($id,$isSold);
    echo json_encode(true);
}else{

}