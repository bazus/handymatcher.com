<?php
if(isset($_POST['id']) && isset($_POST['title']) && isset($_POST['description']) && isset($_POST['model']) && isset($_POST['year']) && isset($_POST['vin']) && isset($_POST['plate']) && isset($_POST['tire']) && isset($_POST['state']) && isset($_POST['comment']) && isset($_POST['capacity']) && isset($_POST['purchaseDate']) && isset($_POST['purchasePrice']) && isset($_POST['purchaseMiles']) && isset($_POST['soldDate']) && isset($_POST['soldPrice']) && isset($_POST['soldMiles'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");

    $id = $_POST['id'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $model = $_POST['model'];
    $year = $_POST['year'];
    $vin = $_POST['vin'];
    $plate = $_POST['plate'];
    $tire = $_POST['tire'];
    $state = $_POST['state'];
    $comment = $_POST['comment'];
    $capacity = $_POST['capacity'];
    $purchaseDate = $_POST['purchaseDate'];
    $purchasePrice = $_POST['purchasePrice'];
    $purchaseMiles = $_POST['purchaseMiles'];
    $soldDate = $_POST['soldDate'];
    $soldPrice = $_POST['soldPrice'];
    $soldMiles = $_POST['soldMiles'];

    $trucks = new trucks($bouncer["credentials"]["orgId"]);
    $truck = $trucks->updateTruck($id,$title,$description,$model,$year,$vin,$plate,$tire,$state,$comment,$capacity,$purchaseDate,$purchasePrice,$purchaseMiles,$soldDate,$soldPrice,$soldMiles);
    echo json_encode($truck);
}else{
    echo json_encode("asd");
}