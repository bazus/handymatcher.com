<?php
if(isset($_POST['title']) && isset($_POST['description']) && isset($_POST['model']) && isset($_POST['year']) && isset($_POST['vin']) && isset($_POST['plate']) && isset($_POST['tire']) && isset($_POST['state']) && isset($_POST['capacity']) && isset($_POST['purchaseDate']) && isset($_POST['purchasePrice']) && isset($_POST['purchaseMiles'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");

    $title = $_POST['title'];
    $description = $_POST['description'];
    $model = $_POST['model'];
    $year = $_POST['year'];
    $vin = $_POST['vin'];
    $plate = $_POST['plate'];
    $tire = $_POST['tire'];
    $state = $_POST['state'];
    $capacity = $_POST['capacity'];
    $purchaseDate = $_POST['purchaseDate'];
    $purchasePrice = $_POST['purchasePrice'];
    $purchaseMiles = $_POST['purchaseMiles'];

    $trucks = new trucks($bouncer["credentials"]["orgId"]);
    $truck = $trucks->addTruck($bouncer["credentials"]["userId"],$title,$description,$model,$year,$vin,$plate,$tire,$state,$capacity,$purchaseDate,$purchasePrice,$purchaseMiles);
    echo json_encode($truck);
}else{
    echo json_encode(false);
}