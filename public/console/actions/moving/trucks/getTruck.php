<?php
if(isset($_POST['truckId'])){
    $pagePermissions = array(false,[1],true,true,[2]);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");

    $id = $_POST['truckId'];
    $trucks = new trucks($bouncer["credentials"]["orgId"]);
    $data = $trucks->getTruck($id);
    echo json_encode($data);
}else{
    echo json_encode(false);
}