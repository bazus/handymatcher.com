<?php
$pagePermissions = array(false,[1],true,true,[2]);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");

$trucks = new trucks($bouncer["credentials"]["orgId"]);
$data = $trucks->getTrucks();
echo json_encode($data);