<?php

if(isset($_POST["templateId"]) && isset($_POST["templateText"])) {
    $pagePermissions = array(false, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/smsTemplates.php");

    $templateId = $_POST["templateId"];
    $text = $_POST["templateText"];

    $smsTemplates = new smsTemplates($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $updateTemplate = $smsTemplates->updateTemplate($templateId,$text);

    echo json_encode($updateTemplate);
    exit;
}else{
    echo json_encode(false);
    exit;
}