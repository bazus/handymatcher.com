<?php

if(isset($_POST["templateId"])) {
    $pagePermissions = array(false, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/smsTemplates.php");

    $templateId = $_POST["templateId"];

    $smsTemplates = new smsTemplates($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $deleteTemplate = $smsTemplates->deleteTemplate($templateId);

    echo json_encode($deleteTemplate);
    exit;
}else{
    echo json_encode(false);
    exit;
}