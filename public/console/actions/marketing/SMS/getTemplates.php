<?php

$pagePermissions = array(false, true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/smsTemplates.php");

$smsTemplates = new smsTemplates($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
$templates = $smsTemplates->getTemplates();

echo json_encode($templates);
exit;
