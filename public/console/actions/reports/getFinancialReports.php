<?php
if (isset($_POST['start']) && isset($_POST['end'])) {
    $pagePermissions = array(true, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/reports/financial.php");

    $start = $_POST['start'];
    $end = $_POST['end'];

    $financial = new financial($bouncer["credentials"]['orgId']);
    $totalPaymentsByDate = $financial->getTotalPaymentsByDate($bouncer["credentials"]["orgId"], $start, $end);
    echo json_encode($totalPaymentsByDate);
}else{
    echo json_encode(false);
}
