<?php
if (isset($_POST['start']) && isset($_POST['end'])) {

    $pagePermissions = array(true, true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/reports/financial.php");

    $start = $_POST['start'];
    $end = $_POST['end'];

    $financial = new financial($bouncer["credentials"]['orgId']);
    $paymentsByDate = $financial->getPaymentsByDate($bouncer["credentials"]["orgId"], $start, $end);

    $payments = [];
    $payment = [];
    foreach ($paymentsByDate as $x => $x_value) {
        $payment["paymentsDate"] = $x;
        $payment["payment"] = $x_value;

        $payments[] = $payment;
    }
    echo json_encode($payments);
} else{
    echo json_encode(false);
}
