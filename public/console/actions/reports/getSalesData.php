<?php
$pagePermissions = array(true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
if (isset($_POST['start']) && isset($_POST['end'])) {
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/reports/sales.php");

    if ($_POST['start'] !== "undefined"){
        $start = date("Y-m-d 00:00:00",strtotime($_POST['start']));
    }else{
        $start = date("Y-m-d 00:00:00",strtotime("-1 month"));
    }
    if ($_POST['end'] !== "undefined"){
        $end = date("Y-m-d 23:59:59",strtotime($_POST['end']));
    }else{
        $end = date("Y-m-d 23:59:59",strtotime("Today"));
    }

    $sales = new sales($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $data = $sales->getSalesPerformanceData($start,$end);

    echo json_encode($data);
}else{
    echo json_encode(false);
}