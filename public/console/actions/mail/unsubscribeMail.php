<?php

if(isset($_POST['orgId']) && ($_POST['email']) && ($_POST['recaptcharesponse'])){


    // first - check RECAPTCHA
    $recaptcharesponse = $_POST["recaptcharesponse"];

    $url = "https://www.google.com/recaptcha/api/siteverify";
    $fields = [];
    $fields["secret"] = "6LfmucQUAAAAAILIIY-yFXdTWBaAr7RBvZeE96zy";
    $fields["response"] = $recaptcharesponse;

    //open connection
    $ch_new = curl_init();
    //set the url, number of POST vars, POST data
    curl_setopt($ch_new,CURLOPT_URL,$url);
    curl_setopt($ch_new,CURLOPT_POST,count($fields));
    curl_setopt($ch_new,CURLOPT_POSTFIELDS,http_build_query($fields));
    curl_setopt($ch_new, CURLOPT_RETURNTRANSFER, 1);
    // ================================ //
    curl_setopt($ch_new, CURLOPT_CONNECTTIMEOUT ,0);
    curl_setopt($ch_new, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch_new, CURLOPT_TIMEOUT, 180); //timeout in seconds
    // ================================ //


    //execute post
    $data = curl_exec($ch_new);

    if(curl_errno($ch_new)){
        $errorExist = true;
        $errorText = htmlspecialchars(curl_error($ch_new));
    }

    $code = curl_getinfo($ch_new, CURLINFO_HTTP_CODE);

    $data = json_decode($data);

    // check if recaptcha is good
    if($data->success != true || $data->score <= 0.3) {
        $msg = "Unsubscribe error [".$_POST["orgId"].",".$_POST["email"]."]";
        shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => $msg, "mode" => "debug","env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

        echo json_encode(false);
    }

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/connect/connect.php");
    $database = new connect();

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailCenter.php");

    $orgId =  $_POST['orgId'];
    $email =  $_POST['email'];
    if(isset($_POST['reason'])) {
        $reason = $_POST['reason'];
    } else{
        $reason = "";
    }
    $emailUnsubscribe = new mailCenter($orgId);
    $createEmailUnsubscribe = $emailUnsubscribe->addMailToBlockList($email,$reason);

    echo json_encode($createEmailUnsubscribe);
    }
    else {
        echo json_encode(false);
    }