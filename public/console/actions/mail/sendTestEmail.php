<?php
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

$SesClient = new SesClient([
    'credentials' => array(
        'key' => $_SERVER['AWS_S3_KEY'],
        'secret' => $_SERVER['AWS_S3_SECRET'],
    ),
    'version' => '2010-12-01',
    'region'  => 'us-east-1'
]);
if (isset($_POST['html'])&& isset($_POST['emailAddress'])){
    $pagePermissions = array(false, true, true, array(['mailcenter',1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");


    $html = $_POST['html'];
    $emailAddress = $_POST['emailAddress'];
    if (isset($_POST['subject'])){
        $subject = $_POST['subject'];
    }else {
        $subject = "Network Leads Test Email";
    }

    $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $response = $email->sendMailFromUs($SesClient,$bouncer['userData']['fullName'],"no-reply@network-leads.com",[$emailAddress],$subject,$html,$html,"google",false);

    if ($response['status'] == true){
        echo json_encode(true);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}