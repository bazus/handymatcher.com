<?php

if(isset($_POST['accountId'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");

    $accountId = $_POST['accountId'];

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"]);
    $disableAccount = $mailaccounts->disableAccount($accountId,$bouncer["credentials"]["orgId"]);

    echo json_encode($disableAccount);
}else{
    echo json_encode(false);
}