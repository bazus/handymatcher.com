<?php
if (isset($_POST['leadId'])){
    $pagePermissions = array(false, true, true, array(['mailcenter',1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");

    $leadId = $_POST['leadId'];

    $mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);

    $emails = $mailCenter->getEmailsToLead($leadId);

    echo json_encode($emails);
}else{
    echo json_encode(false);
}