<?php
require $_SERVER['LOCAL_NL_PATH'] . '/vendor/autoload.php';

//    require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

$SesClient = new SesClient([
    'credentials' => array(
        'key' => $_SERVER['AWS_S3_KEY'],
        'secret' => $_SERVER['AWS_S3_SECRET'],
    ),
    'version' => '2010-12-01',
    'region' => 'us-east-1'
]);

if(isset($_POST['featureDescription']) && $_POST['featureDescription'] != "" && isset($_POST['featurePhone'])) {

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    $userId = $bouncer["credentials"]["userId"];
    $featureDescription = $_POST['featureDescription'];
    $phone = $_POST['featurePhone'];

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");

    $fullName = "Network Leads - Do Not Reply";
    $from = "no-reply@network-leads.com";
    $to = ["support@network-leads.com"];
    $subject = "New Feature Request";

    $plaincontent = "User Id: ". $userId . "\n\r Feature Description: " . $featureDescription . "\n\r Phone: ". $phone;

    $mailType = "google";

    $email = new email();
    $content = "User Id: ". $userId . "<br> Feature Description: " . $featureDescription . "<br> Phone: ". $phone;
    $sendMail = $email->sendMailFromUs($SesClient, $fullName, $from, $to, $subject, $plaincontent, $content, $mailType);

    echo json_encode($sendMail);
}else{
    echo json_encode(false);
}