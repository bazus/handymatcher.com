<?php

if(isset($_POST['accountId']) && isset($_POST['password'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");

    $accountId = $_POST['accountId'];
    $password = $_POST['password'];

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"]);
    $updateAccountPassword = $mailaccounts->updateAccountPassword($accountId,$password);

    echo json_encode($updateAccountPassword);
}else{
    echo json_encode(false);
}