<?php

if(isset($_POST['id'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

    $id = $_POST['id'];
    $organization = new organization($bouncer["credentials"]["orgId"]);

    $update = $organization->setDefaultMailAccount($id);

    echo json_encode($update);
}else{
    echo json_encode(false);
}