<?php
// Send email to multiple leads
if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

if (isset($_POST['leadsIds'])&& isset($_POST['subject'])&& isset($_POST['content']) && isset($_POST['emailAccountId'])){
    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingCompany.php");

    $movingCompany = new movingCompany($bouncer["credentials"]["orgId"]);
    $organizationCompanyData = $movingCompany->getLicenses();

    $leadsIds = $_POST['leadsIds'];
    $content = $_POST['content'];
    $subject = $_POST['subject'];

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);

    $resp = [];
    $resp["limit"] = false;
    $resp["responses"] = [];

    foreach ($leadsIds as $leadId) {
        $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);

        $emailAccountId = $_POST['emailAccountId'];

        $leadData = $lead->getData();

        $toMailAccount = $leadData['email'];

        $response = $email->sendMailFromMailAccount($bouncer["credentials"]["userId"], $emailAccountId, $toMailAccount, $subject, $content, $bouncer["credentials"]["orgId"],$leadId);
        $response["email"] = $toMailAccount;

        $resp["responses"][] = $response;
    }

    echo json_encode($resp);

}else{
    echo json_encode(false);
}