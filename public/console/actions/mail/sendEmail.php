<?php

if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';


if (isset($_POST['to'])&& isset($_POST['subject'])&& isset($_POST['content'])&& isset($_POST['from'])){
    $pagePermissions = array(false, true, true, array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");

    $to = explode(",",$_POST['to']);
    $fromMailAccount = $_POST['from'];
    $subject = $_POST['subject'];
    $content = $_POST['content'];

    $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $emailLimit = $email->isLimitValid($bouncer["credentials"]["orgId"],false,count($to));

    $resp = [];
    $resp["limit"] = !$emailLimit;
    $resp["responses"] = [];

    if($emailLimit == false){
        echo json_encode($resp);
        exit;
    }

    for($i = 0;$i<count($to);$i++){
        $toEmail = $to[$i];
        $response = $email->sendMailFromMailAccount($bouncer["credentials"]["userId"],$fromMailAccount,$toEmail,$subject,$content,$bouncer["credentials"]["orgId"]);
        $response["email"] = $toEmail;

        $resp["responses"][] = $response;
    }

    echo json_encode($resp);
    exit;

}else{
    echo json_encode(false);
    exit;
}