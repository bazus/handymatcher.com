<?php

if(isset($_POST['accountId'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");

    $accountId = $_POST['accountId'];

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer['organizationData']['id']);
    if ($bouncer['isUserAnAdmin'] == true) {
        $deleteAccount = $mailaccounts->deleteAccount($accountId,true);
    }else{
        $deleteAccount = $mailaccounts->deleteAccount($accountId);
    }

    echo json_encode($deleteAccount);
}else{
    echo json_encode(false);
}