<?php
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

if (isset($_POST['leadId']) && isset($_POST['emailAccountId']) && isset($_POST['subjectEmail'])){

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingCompany.php");

    $movingCompany = new movingCompany($bouncer["credentials"]["orgId"]);
    $organizationCompanyData = $movingCompany->getLicenses();

    $organization = new organization($bouncer["credentials"]['orgId']);
    $organizationData = $organization->getData();

    $leadId = $_POST['leadId'];
    $subject = $_POST['subjectEmail'];
    $emailAccountId = $_POST['emailAccountId'];

    $file = $_SERVER['LOCAL_NL_URL']."/console/categories/iframes/sendEstimateEmailText.php?leadId=".$leadId."&userId=".$bouncer["credentials"]["userId"]."&orgId=".$bouncer["credentials"]['orgId'];
    $content = file_get_contents($file);

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $lead = new lead($leadId, $bouncer["credentials"]["orgId"]);

    //$movingLead = new movingLead($leadId, $bouncer["credentials"]["orgId"]);
    $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);

    $leadData = $lead->getData();

    $toMailAccount = $leadData['email'];


    $response = $email->sendMailFromMailAccount($bouncer["credentials"]["userId"], $emailAccountId, $toMailAccount, $subject, $content, $bouncer["credentials"]["orgId"],$leadId);

    echo json_encode($response);

}else{
    echo json_encode(false);
}