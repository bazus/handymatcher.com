<?php
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

if (isset($_POST['paymentId']) && isset($_POST['emailAccountId']) && isset($_POST['toEmail']) && isset($_POST['subjectEmail'])){

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingCompany.php");

    $movingCompany = new movingCompany($bouncer["credentials"]["orgId"]);
    $organizationCompanyData = $movingCompany->getLicenses();

    $organization = new organization($bouncer["credentials"]['orgId']);
    $organizationData = $organization->getData();

    $paymentId = $_POST['paymentId'];
    $subject = $_POST['subjectEmail'];
    $toEmail = $_POST['toEmail'];
    $emailAccountId = $_POST['emailAccountId'];

    $file = $_SERVER['LOCAL_NL_URL']."/console/categories/iframes/sendPaymentReceiptEmailText.php?paymentId=".$paymentId."&userId=".$bouncer["credentials"]["userId"]."&orgId=".$bouncer["credentials"]['orgId'];
    $content = file_get_contents($file);

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);

    $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);

    $response = $email->sendMailFromMailAccount($bouncer["credentials"]["userId"], $emailAccountId, $toEmail, $subject, $content, $bouncer["credentials"]["orgId"]);

    echo json_encode($response);

}else{
    echo json_encode(false);
}