<?php

if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}

require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

$pagePermissions = array(false, true, true, array(['mailcenter',1]));
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/googleAPI.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/microsoftAPI.php");

$mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
$myAccounts = $mailaccounts->getMyAccounts(true,false,$bouncer['organizationData']['mailAccount']);
foreach ($myAccounts AS &$account){
    $account['dateCreated'] = date("m-d-Y",strtotime($account['dateCreated']));

    // if Gmail - check if account is validated
    if($account["accountType"] == "4"){
        $otherData = unserialize($account["otherData"]);
        if(isset($otherData["refresh_token"])){
            $googleAPI = new googleAPI($otherData["refresh_token"]);
            $account["isAuthorized"] = $googleAPI->isAuthorized();
        }

    }

    // if Microsoft - check if account is validated
    if($account["accountType"] == "5"){
        $otherData = unserialize($account["otherData"]);
        if(isset($otherData["refresh_token"])){
            $microsoftAPI = new microsoftAPI($otherData);
            $account["isAuthorized"] = $microsoftAPI->isAuthorized();
        }

    }
}

echo json_encode($myAccounts);