<?php
if (isset($_POST['startDate']) && isset($_POST['endDate'])) {

    $pagePermissions = array(false, true, true, array(['mailcenter', 1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailCenter.php");

    $mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);

    $start = $_POST['startDate'];
    $end = $_POST['endDate'];

    if ($bouncer['isUserAnAdmin'] == true) {
        if (isset($_POST['userId']) && $_POST['userId'] != "") {
            $emails = $mailCenter->getEmails(null, $_POST['userId'],$start,$end);
        } else {
            $emails = $mailCenter->getEmails(null, null,$start,$end);
        }
    } else {
        $emails = $mailCenter->getEmails($bouncer["credentials"]["userId"],null,$start,$end);
    }
    echo json_encode($emails);
} else{
    echo json_encode(false);
}