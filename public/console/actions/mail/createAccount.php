<?php

if(isset($_POST['outgoingmailserver']) && isset($_POST['fullName']) && isset($_POST['emailAddress']) && isset($_POST['emailPassword']) && isset($_POST['emailPort']) && isset($_POST['accountType'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");

    $outgoingmailserver = $_POST['outgoingmailserver'];
    $fullName = $_POST['fullName'];
    $emailAddress = $_POST['emailAddress'];
    $emailPassword = $_POST['emailPassword'];
    $emailPort = $_POST['emailPort'];
    $accountType = $_POST['accountType'];
    $requiredSSL = $_POST['requiredSSL'];

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
    $createAccount = $mailaccounts->createAccount($outgoingmailserver,$fullName,$emailAddress,$emailPassword,$emailPort,$accountType,NULL,$requiredSSL);

    if($createAccount["status"] == true) {
        $update = $bouncer["organization"]->setDefaultMailAccount($createAccount["accountId"]);
        if($update){
            echo json_encode($createAccount);
        }else{
            echo json_encode(false);
        }
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}