<?php

if(isset($_POST['accountId'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");

    $accountId = $_POST['accountId'];

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"]);
    $togglePrivateAccount = $mailaccounts->togglePrivateAccount($accountId);

    echo json_encode($togglePrivateAccount);

}else{
    echo json_encode(false);
}