<?php

if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}
require $_SERVER['LOCAL_NL_PATH'].'/vendor/autoload.php';

if (isset($_POST['userIds'])&& isset($_POST['subject'])&& isset($_POST['content']) && isset($_POST['emailAccountId'])){
    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $userIds = $_POST['userIds'];
    $isValid = true;

    $subject = $_POST['subject'];
    $content = $_POST['content'];
    $emailAccountId = $_POST['emailAccountId'];

    foreach ($userIds as $userId) {

        $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
        $user = new user($userId);
        $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);

        $userData = $user->getData();

        $toMailAccount = $userData['email'];

        $response = $email->sendMailFromMailAccount($bouncer["credentials"]["userId"], $emailAccountId, $toMailAccount, $subject, $content, $bouncer["credentials"]["orgId"]);

        if ($response['status'] == false){
            $isValid = false;
            break;
        }
    }
    echo json_encode($response);

}else{
    echo json_encode(false);
}