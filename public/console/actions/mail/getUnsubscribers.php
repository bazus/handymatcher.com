<?php
if (isset($_POST['page'])){

    $pagePermissions = array(false, true, true, array(['mailcenter',1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");

    $unsubscribersPage = $_POST['page'];

    $mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);

    $unsubscribers = [];

    $unsubscribers["unsubscribersList"] = $mailCenter->getAllUnsubscribers($unsubscribersPage);
    $unsubscribers["numberOfUnsubscribers"] = $mailCenter->getNumberOfAllUnsubscribers();

    echo json_encode($unsubscribers);
} else{
    echo json_encode(false);
}