<?php
if (isset($_POST['emailData'])){
    $pagePermissions = array(false, true, true, array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);

    $emailData = $_POST['emailData'];
    if ($emailData['title'] == "" || $emailData['subject'] == ""){
        return json_encode(false);
    }
    if ($emailData['department'] == "" || $emailData['department'] == "null"){$emailData['department'] = NULL;}

    $setData = $preEmails->setData($emailData);

    echo json_encode($setData);

}else{
    echo json_encode(false);
}