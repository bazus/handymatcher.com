<?php
if (isset($_POST['id'])) {
    $pagePermissions = array(false, true, true, array(['mailcenter',1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);
    $id = $_POST['id'];

    $data = array();
    $data["organizationName"] = $bouncer["organizationData"]["organizationName"];
    $data["organizationLogo"] = $bouncer["organizationData"]["logoPath"];
    $data["departmentTitle"] = $bouncer["userData"]["departmentTitle"];
    $data["userName"] = $bouncer['userData']["fullName"];
    $data["organizationPhone"] = $bouncer["organizationData"]["phone"];
    $data["organizationAddress"] = $bouncer["organizationData"]["address"];
    $data["organizationCity"] = $bouncer["organizationData"]["city"];
    $data["organizationState"] = $bouncer["organizationData"]["state"];
    $data["organizationZip"] = $bouncer["organizationData"]["zip"];
    $data["organizationWebsite"] = $bouncer["organizationData"]["website"];


    $preEmailData = array();
    switch($id){
        case "pre-1":
            $preEmailData = $preEmails->getPreEmail(1,$data);
            break;
        default:
        $preEmailData = $preEmails->getDataById($id);
    }

    echo json_encode($preEmailData);
}else{
    echo json_encode(false);
}