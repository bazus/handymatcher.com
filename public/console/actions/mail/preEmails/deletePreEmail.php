<?php
if (isset($_POST['id'])){
    $pagePermissions = array(false,true,true,array(['mailcenter',4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);

    $id = $_POST['id'];

    $setAsDeleted = $preEmails->setAsDeleted($id);

    echo json_encode($setAsDeleted);

}else{
    echo json_encode(false);
}