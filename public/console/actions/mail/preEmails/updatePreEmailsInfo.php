<?php
if (isset($_POST['id']) && isset($_POST['title']) && isset($_POST['subject']) && isset($_POST['department'])){

    $pagePermissions = array(false, true, true, array(['mailcenter',2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);

    $id = $_POST['id'];
    $title = $_POST['title'];
    $subject = $_POST['subject'];
    $department = $_POST['department'];

    $updateData = $preEmails->updateInfoData($id,$title,$subject,$department);
    echo json_encode($updateData);

}else{
    var_dump($_POST);
    echo json_encode(false);
}