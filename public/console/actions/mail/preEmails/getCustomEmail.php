<?php

if(isset($_POST['leadId']) && isset($_POST['type'])){

    $pagePermissions = array(false,true,true,array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

    $leadId = $_POST['leadId'];
    $type = $_POST['type'];

    $data = [];
    $data["subject"] = "";
    $data["content"] = "";

    if($type == "1"){

        if($leadId == false || $leadId == "false" || $leadId == NULL){
            echo json_encode(false);
            exit;
        }

        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

        $orgId = $bouncer["credentials"]['orgId'];

        $lead = new lead($leadId,$orgId);
        $leadData = $lead->getData();

        $organization = new organization($orgId);
        $organizationData = $organization->getData();
        $organizationPicture = $organizationData['logoPath'];

        $logoImage = '';
        if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){
            $logoImage = '<img class="logoimg" src="'.$organizationPicture.'" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }else{
            $logoImage = '<img class="logoimg" src="https://d2o9xrcicycxrg.cloudfront.net/home/images/NLwebsiteFL2.png" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }

        if(isset($_POST['estimateId'])){
            $customLink = $_SERVER["YMQ_URL"]."/leads.php?key=".$leadData['secretKey']."&page=estimates&estimate=".$_POST['estimateId'];
        }else{
            $customLink = $_SERVER["YMQ_URL"]."/leads.php?key=".$leadData['secretKey']."&page=estimates";
        }

        $data["subject"] = 'Moving Estimate From '.$bouncer['organizationData']['organizationName'];
        $data["content"] = '<!DOCTYPE html><html>

<head>
    <style>
body{
    margin: 0;
}
        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top;display:block;width:600px;max-width:600px;margin:0 auto!important" valign="top" width="600">

            <div style="box-sizing:border-box;display:block;max-width:600px;margin:0 auto;padding:10px">

                <div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                    <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                        <tbody>

                        <tr>

                            <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>

                                          '.$logoImage.'
                                       
                                    </span>

                            </td>

                        </tr>

                        </tbody>

                    </table>

                </div>


            <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                    <tbody>

                    <tr>

                        <td style="box-sizing:border-box;font-size:16px;vertical-align:top;padding:30px" valign="top">

                            <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                <tbody>

                                <tr>

                                    <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top" valign="top">


                                        Dear '.$leadData['firstname'].',<br>

                                        <p style="margin:0;margin-bottom:30px;color:#294661;font-size:16px;font-weight:300">

                                            Your free moving estimate is ready for you to review.<br>
                                            Please <a href="'.$customLink.'" target="_blank">click here</a> to go over the inclusions and parameters and if you wish, you can e-sign and lock your great price in!<br>
                                            <br>
                                            Sincerely,<br>
                                            '.$organizationData['organizationName'].'
                                        </p>

                                    </td>

                                </tr>

                                </tbody>

                            </table>
                        </td>

                    </tr>

                    </tbody>

                </table>

            </div>
</div>

</td>


</tr>

</tbody>

</table>
</div>

</body>

</html>';

    }

    if($type == "2"){

        if($leadId == false || $leadId == "false" || $leadId == NULL){
            echo json_encode(false);
            exit;
        }
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

        $orgId = $bouncer["credentials"]['orgId'];

        $lead = new lead($leadId,$orgId);
        $leadData = $lead->getData();

        $organization = new organization($orgId);
        $organizationData = $organization->getData();
        $organizationPicture = $organizationData['logoPath'];

        $logoImage = '';
        if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){
            $logoImage = '<img class="logoimg" src="'.$organizationPicture.'" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }else{
            $logoImage = '<img class="logoimg" src="https://d2o9xrcicycxrg.cloudfront.net/home/images/NLwebsiteFL2.png" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }

        $data["subject"] = 'Credit card authorization form - '.$bouncer['organizationData']['organizationName'];
        $data["content"] = '<!DOCTYPE html><html>

<head>
    <style>
body{
    margin: 0;
}
        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top;display:block;width:600px;max-width:600px;margin:0 auto!important" valign="top" width="600">

            <div style="box-sizing:border-box;display:block;max-width:600px;margin:0 auto;padding:10px">

                <div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                    <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                        <tbody>

                        <tr>

                            <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>

                                          '.$logoImage.'
                                       
                                    </span>

                            </td>

                        </tr>

                        </tbody>

                    </table>

                </div>


            <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                    <tbody>

                    <tr>

                        <td style="box-sizing:border-box;font-size:16px;vertical-align:top;padding:30px" valign="top">

                            <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                <tbody>

                                <tr>

                                    <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top" valign="top">


                                        Dear '.$leadData['firstname'].',<br>

                                        <p style="margin:0;margin-bottom:30px;color:#294661;font-size:16px;font-weight:300">

                                           Please click on the button below to access your secure credit card authorization form.<br><br>
                                           <a href="'.$_SERVER["YMQ_URL"].'/leads.php?page=cc&key='.$leadData['secretKey'].'&add=1" style="box-sizing:border-box;border-color:#348eda;font-weight:400;text-decoration:none;display:inline-block;margin:0;color:#ffffff;background-color:#348eda;border:solid 1px #348eda;border-radius:2px;font-size:14px;padding:12px 45px" target="_blank">Open credit card authorization form</a>
                                            <br>
                                            <br>
                                            <div style="font-size: 11px">
                                            If this does not work, please copy and paste the below link in your browser:<br><br>
                                            '.$_SERVER["YMQ_URL"].'/leads.php?page=cc&key='.$leadData['secretKey'].'&add=1
</div>
                                            <br>
                                            Sincerely,<br>
                                            '.$organizationData['organizationName'].'
                                        </p>

                                    </td>

                                </tr>

                                </tbody>

                            </table>
                        </td>

                    </tr>

                    </tbody>

                </table>

            </div>
</div>

</td>


</tr>

</tbody>

</table>
</div>

</body>

</html>';

    }

    if($type == "3"){
        if($leadId == false || $leadId == "false" || $leadId == NULL){
            echo json_encode(false);
            exit;
        }

        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/payments.php");

        $orgId = $bouncer["credentials"]['orgId'];
        if(isset($_POST['estimateId'])) {
            $estimateId = $_POST['estimateId'];
        }else{
            exit;
        }

        $lead = new lead($leadId,$orgId);
        $leadData = $lead->getData();

        $organization = new organization($orgId);
        $organizationData = $organization->getData();
        $organizationPicture = $organizationData['logoPath'];


        $movingLead = new movingLead($leadId, $orgId);
        $movingEstimate = $movingLead->getEstimates($estimateId);

        if($movingEstimate == false || $movingEstimate == null || $movingEstimate[0] == null){
            $data["subject"] = "";
            $data["content"] = "";
            echo json_encode($data);
            exit;
        }
        $estimateData = $movingEstimate[0];

        $payments = new payments($orgId);
        $leadPayments = $payments->getPaymentsByLeads($leadId);


        $data["subject"] = 'Invoice #E'.$estimateId.' from '.$organizationData['organizationName'];



        $logoImage = '';
        if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){
            $logoImage = '<img class="logoimg" src="'.$organizationPicture.'" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }

        $organizationName = $organizationData["organizationName"].'<br>';
        if($organizationData["address"] != ""){$organizationAddress = $organizationData["address"].'<br>';}else{$organizationAddress = "";}
        if($organizationData["city"] != "" || $organizationData["state"] != "" || $organizationData["zip"] != ""){$organizationLocation = $organizationData["city"].','.$organizationData["state"].', '.$organizationData["zip"].'<br>';}else{$organizationLocation = "";}

        $leadName = $leadData["firstname"]." ".$leadData["lastname"]."<br>";
        $leadEmail = $leadData["email"]."<br>";

        $rows = [];

        $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">1</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">Moving Services</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($estimateData["estimateCalculationData"]["initPrice"],2, '.', ',').'</td></tr>';

        if (isset($estimateData["estimateCalculationData"]["fuel"]) && $estimateData["estimateCalculationData"]["fuel"] > 0) {
            $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">1</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">Fuel</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($estimateData["estimateCalculationData"]["fuel"],2, '.', ',').'</td></tr>';
        }
        if (isset($estimateData["estimateCalculationData"]["totalPacking"]) && $estimateData["estimateCalculationData"]["totalPacking"] > 0) {
            $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">1</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">Packing + Unpacking</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($estimateData["estimateCalculationData"]["totalPacking"],2, '.', ',').'</td></tr>';
        }
        if (isset($estimateData["estimateCalculationData"]["agentFee"]) && $estimateData["estimateCalculationData"]["agentFee"] > 0) {
            $agentFeePerc = '';
            if($estimateData["agentFeeType"] == "0"){
                $agentFeePerc = " (".$estimateData["agentFee"]."%)";
            }
            $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">1</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">Agent Fee'.$agentFeePerc.'</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($estimateData["estimateCalculationData"]["agentFee"],2, '.', ',').'</td></tr>';
        }
        if(isset($estimateData["estimateCalculationData"]["extraCharges"])){
            foreach ($estimateData["estimateCalculationData"]["extraCharges"] as $charge){
                $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">1</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">'.$charge["title"].'</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($charge["price"],2, '.', ',').'</td></tr>';
            }
        }
        if (isset($estimateData["estimateCalculationData"]["materials"])){
            foreach ($estimateData["estimateCalculationData"]["materials"] as $material){
                $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">'.$material["amount"].'</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">'.$material["title"].' ($'.$material["pricePerItem"].')</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($material["totalPrice"],2, '.', ',').'</td></tr>';
            }
        }

        if (isset($estimateData["estimateCalculationData"]["valueProtectionAdded"]) && $estimateData["estimateCalculationData"]["valueProtectionAdded"] == "1") {
            $FVPtext = "";
            if($estimateData["estimateCalculationData"]["valueProtectionType"] == "1"){
                $FVPtext = "Full value protection";
            }else{
                $FVPtext = "Basic value protection";
            }
            $rows[] = '<tr class="item"><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">1</td><td style="border-bottom: 1px solid #eee;padding: 5px;vertical-align: top;">'.$FVPtext.'</td><td style="border-bottom: 1px solid #eee;text-align: right;padding: 5px;vertical-align: top;">'."$".number_format($estimateData["estimateCalculationData"]["valueProtectionCharge"],2, '.', ',').'</td></tr>';
        }

        $rows[] = '<tr><td colspan="3" style="padding: 9px;"></td></tr>';

        if(isset($estimateData["estimateCalculationData"]["discount"]) && $estimateData["estimateCalculationData"]["discount"] > 0){
            $discountPerc = "";
            if($estimateData["discountType"] == "0"){
                $discountPerc = " (".$estimateData["discount"]."%)";
            }
            $rows[] = '<tr class="subItem"><td></td><td></td><td style="text-align: right;font-size: 13px;">Discount'.$discountPerc.': '."-$".number_format($estimateData["estimateCalculationData"]["discount"],2, '.', ',').'</td></tr>';
        }

        if(isset($estimateData["estimateCalculationData"]["coupon"]) && $estimateData["estimateCalculationData"]["coupon"] > 0){
            $discountPerc = "";
            if($estimateData["couponType"] == "0"){
                $discountPerc = " (".$estimateData["coupon"]."%)";
            }
            $rows[] = '<tr class="subItem"><td></td><td></td><td style="text-align: right;font-size: 13px;">Coupon Discount'.$discountPerc.': '."-$".number_format($estimateData["estimateCalculationData"]["coupon"],2, '.', ',').'</td></tr>';
        }

        if(isset($estimateData["estimateCalculationData"]["senior"]) && $estimateData["estimateCalculationData"]["senior"] > 0){
            $discountPerc = "";
            if($estimateData["seniorType"] == "0"){
                $discountPerc = " (".$estimateData["senior"]."%)";
            }
            $rows[] = '<tr class="subItem"><td></td><td></td><td style="text-align: right;font-size: 13px;">Senior Discount'.$discountPerc.': '."-$".number_format($estimateData["estimateCalculationData"]["senior"],2, '.', ',').'</td></tr>';
        }

        if(isset($estimateData["estimateCalculationData"]["veteran"]) && $estimateData["estimateCalculationData"]["veteran"] > 0){
            $discountPerc = "";
            if($estimateData["veteranDiscountType"] == "0"){
                $discountPerc = " (".$estimateData["veteranDiscount"]."%)";
            }
            $rows[] = '<tr class="subItem"><td></td><td></td><td style="text-align: right;font-size: 13px;">Veteran Discount'.$discountPerc.': '."-$".number_format($estimateData["estimateCalculationData"]["veteran"],2, '.', ',').'</td></tr>';
        }

        $rows[] = '<tr class="total subItem"><td></td><td></td><td style="text-align: right;font-size: 13px;font-weight: bold;">Sub-total: $'.number_format($estimateData["totalEstimate"],2, '.', ',').'</td></tr>';

        if(isset($leadPayments["totalPayments"])){
            $rows[] = '<tr class="subItem"><td></td><td></td><td style="text-align: right;font-size: 13px;">Payments '."-$".number_format($leadPayments["totalPayments"],2, '.', ',').'</td></tr>';
        }

        // Left balance

        $balance = round($estimateData["totalEstimate"]-$leadPayments["totalPayments"]);
        if($balance < 0){$balance = 0;}
        $rows[] = '<tr class="total subItem"><td></td><td></td><td style="font-size: 16px;text-align: right;font-weight: bold;">Balance: $'.number_format($balance,2, '.', ',').'</td></tr>';

        // Payments Data
        $paymentsTitle = "";
        if(count($leadPayments["paymentData"]) > 0){$paymentsTitle = '<h2 style="font-size: 1.3em;">Payments</h2>';}
        $paymentsRows = [];

        if(isset($leadPayments["paymentData"])){
            foreach($leadPayments["paymentData"] as $payment){

                if($payment["method"] == "1"){
                    // Credit card
                    $paymentsRows[] = '<div>$'.number_format($payment["totalAmount"],2, '.', ',').' was paid on '.date("F jS, Y",strtotime($payment["paymentDate"])).' by credit card ending in '.$payment["creditCardNumber"].'</div>';
                }else{
                    // Other
                    $paidBy = "";
                    switch($payment["method"]){
                        case "2":
                            $paidBy = " by PayPal";
                            break;
                        case "3":
                            $paidBy = " by Check";
                            break;
                        case "4":
                            $paidBy = " by Wire-Transfer";
                            break;
                        case "5":
                            $paidBy = " by Cash";
                            break;

                    }
                    $paymentsRows[] = '<div>$'.number_format($payment["totalAmount"],2, '.', ',').' was paid on '.date("F jS, Y",strtotime($payment["paymentDate"])).$paidBy.'</div>';

                }
            }
        }

        $isInvoicePaid = "";
        if($balance <= 0){
            $isInvoicePaid = "<span style='color: #4CAF50;'>PAID</span>";
        }else{
            $isInvoicePaid = "<span style='color: #e8473b;'>NOT PAID</span>";
        }


        $data["content"] = '<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>

    <style>
       
    </style>
</head>

<body style="background-color: rgb(237, 237, 237);font-size: 16px;">
<div class="invoice-box" style="max-width: 800px;margin: auto;padding: 30px;border: 1px solid #eee;background-color: #fff;font-size: 16px;line-height: 24px;font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;color: #555;">
    <table cellpadding="0" cellspacing="0" style="width: 100%;line-height: inherit;text-align: left;">
        <tr class="top">
            <td colspan="3" style="padding: 5px;vertical-align: top;">
                <table style="width: 100%;line-height: inherit;text-align: left;">
                    <tr>
                        <td class="title" style="padding: 5px;vertical-align: top;padding-bottom: 20px;font-size: 45px;line-height: 45px;color: #333;">
                              '.$logoImage.'
                        </td>
                        <td style="padding: 5px;vertical-align: top;padding-bottom: 20px;"></td>
                        <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 20px;">
                            Invoice #E'.$estimateId.'<br>
                            Date: '.date('F jS, Y',strtotime('now')).'<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="3" style="padding: 5px;vertical-align: top;">
                <table style="width: 100%;line-height: inherit;text-align: left;">
                    <tr>
                        <td style="padding: 5px;vertical-align: top;padding-bottom: 40px;">
                            '.$organizationName.$organizationAddress.$organizationLocation.'
                        </td>
                        <td style="padding: 5px;vertical-align: top;padding-bottom: 40px;"></td>
                        <td style="padding: 5px;vertical-align: top;text-align: right;padding-bottom: 40px;">
                            '.$leadName.'
                            '.$leadEmail.'
                            '.$isInvoicePaid.'
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr class="heading">
            <td style="width: 79px;padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                Qty
            </td>
              <td style="padding: 5px;vertical-align: top;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                Description
            </td>

            <td style="padding: 5px;vertical-align: top;text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                Price
            </td>
        </tr>

        '.implode('',$rows).'

        
        <tr>
            <td colspan="3" style="padding: 5px;vertical-align: top;">
               '.$paymentsTitle.'
               '.implode('',$paymentsRows).'
            </td>
        </tr>
        
    </table>
</div>
</body>
</html>';
    }

    if($type == "4"){
        if(isset($_POST['jobAcceptanceId'])) {
            $jobAcceptanceId = $_POST['jobAcceptanceId'];
        }else{
            exit;
        }

        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobAcceptanceForms.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");


        $lead = new lead($leadId,$bouncer["credentials"]['orgId']);
        $leadData = $lead->getData();

        $organization = new organization($bouncer["credentials"]['orgId']);
        $organizationData = $organization->getData();
        $organizationPicture = $organizationData['logoPath'];

        $jobAcceptanceForms = new jobAcceptanceForms($bouncer["credentials"]['orgId']);
        $jobAcceptanceFormData = $jobAcceptanceForms->getSingleJobAcceptanceForm($jobAcceptanceId);

        $logoImage = '';
        if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){
            $logoImage = '<img class="logoimg" src="'.$organizationPicture.'" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }else{
            $logoImage = '<img class="logoimg" src="https://d2o9xrcicycxrg.cloudfront.net/home/images/NLwebsiteFL2.png" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }

        if($leadData["jobNumber"] == null || $leadData["jobNumber"] == ""){
            $leadData["jobNumber"] = $leadData["id"];
        }

        $data["subject"] = 'Job acceptance form from '.$organizationData['organizationName'];

        $data["content"] = '<!DOCTYPE html><html>

<head>
    <style>
body{
    margin: 0;
}
        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top;display:block;width:600px;max-width:600px;margin:0 auto!important" valign="top" width="600">

            <div style="box-sizing:border-box;display:block;max-width:600px;margin:0 auto;padding:10px">

                <div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                    <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                        <tbody>

                        <tr>

                            <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>

                                          '.$logoImage.'
                                       
                                    </span>

                            </td>

                        </tr>

                        </tbody>

                    </table>

                </div>


            <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                    <tbody>

                    <tr>

                        <td style="box-sizing:border-box;font-size:16px;vertical-align:top;padding:30px" valign="top">

                            <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                <tbody>

                                <tr>

                                    <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top" valign="top">


                                        Dear '.$jobAcceptanceFormData["carrierName"].',<br><br>

                                        <p style="margin:0;margin-bottom:30px;color:#294661;font-size:16px;font-weight:300">

                                            Please follow the link below to access Job #'.$leadData["jobNumber"].'\'s acceptance form.<br><br>
                                           <a href="'.$_SERVER["YMQ_URL"].'/jobAcceptance.php?k='.$jobAcceptanceFormData["secretKey"].'&t='.$jobAcceptanceFormData["secretToken"].'" style="box-sizing:border-box;border-color:#348eda;font-weight:400;text-decoration:none;display:inline-block;margin:0;color:#ffffff;background-color:#348eda;border:solid 1px #348eda;border-radius:2px;font-size:14px;padding:12px 45px" target="_blank">Open Job acceptance form</a>
                                            <br>
                                            <br>
                                            Sincerely,<br>
                                            '.$organizationData['organizationName'].'
                                        </p>

                                    </td>

                                </tr>

                                </tbody>

                            </table>
                        </td>

                    </tr>

                    </tbody>

                </table>

            </div>
</div>

</td>


</tr>

</tbody>

</table>
</div>

</body>

</html>';

    }

    if($type == "5"){

        if($leadId == false || $leadId == "false" || $leadId == NULL || !isset($_POST['jobAcceptanceId'])){
            echo json_encode(false);
            exit;
        }

        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/jobAcceptanceForms.php");

        $jobAcceptanceId = $_POST['jobAcceptanceId'];

        $orgId = $bouncer["credentials"]['orgId'];

        $lead = new lead($leadId,$orgId);
        $leadData = $lead->getData();

        $organization = new organization($orgId);
        $organizationData = $organization->getData();
        $organizationPicture = $organizationData['logoPath'];

        $jobAcceptanceForms = new jobAcceptanceForms($bouncer["credentials"]['orgId']);
        $jobAcceptanceFormData = $jobAcceptanceForms->getSingleJobAcceptanceForm($jobAcceptanceId);

        $logoImage = '';
        if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){
            $logoImage = '<img class="logoimg" src="'.$organizationPicture.'" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }else{
            $logoImage = '<img class="logoimg" src="https://d2o9xrcicycxrg.cloudfront.net/home/images/NLwebsiteFL2.png" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }

        if(isset($_POST['estimateId'])){
            $customLink = $_SERVER["YMQ_URL"]."/leads.php?key=".$leadData['secretKey']."&page=estimates&estimate=".$_POST['estimateId'];
        }else{
            $customLink = $_SERVER["YMQ_URL"]."/leads.php?key=".$leadData['secretKey']."&page=estimates";
        }

        if($leadData["jobNumber"] == null || $leadData["jobNumber"] == ""){
            $leadData["jobNumber"] = $leadData["id"];
        }

        $data["subject"] = 'Job details (#'.$leadData["jobNumber"].') from '.$bouncer['organizationData']['organizationName'];
        $data["content"] = '<!DOCTYPE html><html>

<head>
    <style>
body{
    margin: 0;
}
        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top;display:block;width:600px;max-width:600px;margin:0 auto!important" valign="top" width="600">

            <div style="box-sizing:border-box;display:block;max-width:600px;margin:0 auto;padding:10px">

                <div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                    <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                        <tbody>

                        <tr>

                            <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>

                                          '.$logoImage.'
                                       
                                    </span>

                            </td>

                        </tr>

                        </tbody>

                    </table>

                </div>


            <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                    <tbody>

                    <tr>

                        <td style="box-sizing:border-box;font-size:16px;vertical-align:top;padding:30px" valign="top">

                            <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                <tbody>

                                <tr>

                                    <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top" valign="top">


                                        Hi '.$jobAcceptanceFormData["carrierName"].',<br><br>
                                        <p style="margin:0;margin-bottom:30px;color:#294661;font-size:16px;font-weight:300">

                                        You have an accepted job coming up!<br>
                                        Please follow <a href="'.$customLink.'" target="_blank">this link</a> to view the full job details.
                                        <br><br>
                                        Sincerely,<br>
                                        '.$organizationData['organizationName'].'
                                        </p>

                                    </td>

                                </tr>

                                </tbody>

                            </table>
                        </td>

                    </tr>

                    </tbody>

                </table>

            </div>
</div>

</td>


</tr>

</tbody>

</table>
</div>

</body>

</html>';

    }

    if($type == "6"){

        if($leadId == false || $leadId == "false" || $leadId == NULL || !isset($_POST['claimId'])){
            echo json_encode(false);
            exit;
        }

        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
        require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadClaims.php");

        $claimId = $_POST['claimId'];

        $orgId = $bouncer["credentials"]['orgId'];

        $lead = new lead($leadId,$orgId);
        $leadData = $lead->getData();

        $organization = new organization($orgId);
        $organizationData = $organization->getData();
        $organizationPicture = $organizationData['logoPath'];

        $leadClaims = new leadClaims($orgId,$leadId);
        $singleClaim = $leadClaims->getSingleClaim($claimId);

        $logoImage = '';
        if($organizationPicture != "" && @getimagesize( $organizationPicture ) > 0){
            $logoImage = '<img class="logoimg" src="'.$organizationPicture.'" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }else{
            $logoImage = '<img class="logoimg" src="https://d2o9xrcicycxrg.cloudfront.net/home/images/NLwebsiteFL2.png" style="height: 55px;" alt="'.$organizationData["organizationName"].'\'s Logo">';
        }

        $customLink = $_SERVER["YMQ_URL"]."/leads.php?key=".$leadData["secretKey"]."&page=claims&id=".$singleClaim["id"];

        if($leadData["jobNumber"] == null || $leadData["jobNumber"] == ""){
            $leadData["jobNumber"] = $leadData["id"];
        }

        $data["subject"] = 'Claim for job #'.$leadData["jobNumber"].' from '.$bouncer['organizationData']['organizationName'];
        $data["content"] = '<!DOCTYPE html><html>

<head>
    <style>
body{
    margin: 0;
}
        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top;display:block;width:600px;max-width:600px;margin:0 auto!important" valign="top" width="600">

            <div style="box-sizing:border-box;display:block;max-width:600px;margin:0 auto;padding:10px">

                <div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                    <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                        <tbody>

                        <tr>

                            <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>

                                          '.$logoImage.'
                                       
                                    </span>

                            </td>

                        </tr>

                        </tbody>

                    </table>

                </div>


            <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                    <tbody>

                    <tr>

                        <td style="box-sizing:border-box;font-size:16px;vertical-align:top;padding:30px" valign="top">

                            <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                <tbody>

                                <tr>

                                    <td style="box-sizing:border-box;padding:0;font-size:16px;vertical-align:top" valign="top">


                                        Hi '.$singleClaim["firstName"].'<br><br>
                                        <p style="margin:0;margin-bottom:30px;color:#294661;font-size:16px;font-weight:300">

                                        Please follow <a href="'.$customLink.'" target="_blank">this link</a> to view your claim for job #'.$leadData["jobNumber"].'
                                        <br><br>
                                        Sincerely,<br>
                                        '.$organizationData['organizationName'].'
                                        </p>

                                    </td>

                                </tr>

                                </tbody>

                            </table>
                        </td>

                    </tr>

                    </tbody>

                </table>

            </div>
</div>

</td>


</tr>

</tbody>

</table>
</div>

</body>

</html>';

    }

    echo json_encode($data);
}else{
    echo json_encode(false);
}