<?php
if (isset($_POST['name'])){
    $pagePermissions = array(false,true,true,array(['mailcenter',4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);

    $name = $_POST['name'];

    $createFolder = $preEmails->createFolder($name);

    echo json_encode($createFolder);

}else{
    echo json_encode(false);
}