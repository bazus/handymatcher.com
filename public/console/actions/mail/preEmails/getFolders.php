<?php
$pagePermissions = array(false, true, true, array(['mailcenter',1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$getData = $preEmails->getFoldersData();

echo json_encode($getData);