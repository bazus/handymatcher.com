<?php
$pagePermissions = array(false, true, true, array(['mailcenter',1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/connect/connect.php");
$database = new connect();
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");



$preEmails = new preEmails($bouncer["credentials"]["orgId"]);
$getData = $preEmails->getDataById($_GET['id']);
$getData['content'] = html_entity_decode($getData['content']);


?>
<html>

    <body>
    <style>
        *{
            animation-play-state: paused;
            overflow-y: hidden;
        }
    </style>
        <div id="image"><?php echo $getData['content']; ?></div>
    </body>
</html>