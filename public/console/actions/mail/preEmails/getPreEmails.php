<?php
$pagePermissions = array(false, true, true, array(['mailcenter', 1]));
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

if (isset($_POST['folderId'])) {
    $folderId = $_POST['folderId'];
}else{
    $folderId = NULL;
}

$preEmails = new preEmails($bouncer["credentials"]["orgId"]);

if ($folderId == "all"){
    $getData = $preEmails->getData($folderId,true);
}else{
    $getData = $preEmails->getData($folderId);
}

echo json_encode($getData);