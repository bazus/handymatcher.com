<?php
if (isset($_POST['id']) && isset($_POST['content']) && isset($_POST['contentJson'])){

    $pagePermissions = array(false, true, true, array(['mailcenter',2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);

    $id = $_POST['id'];
    $content = $_POST['content'];
    $contentJson = $_POST['contentJson'];

    $updateData = $preEmails->updateData($id,$content,$contentJson);
    echo json_encode($updateData);

}else{
    var_dump($_POST);
    echo json_encode(false);
}