<?php
if (isset($_POST['folder']) && isset($_POST['id'])){
    $pagePermissions = array(false,true,true,array(['mailcenter',4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/preEmails.php");

    $preEmails = new preEmails($bouncer["credentials"]["orgId"]);

    $folder = $_POST['folder'];
    $id = $_POST['id'];

    $changeFolder = $preEmails->changeFolder($folder,$id);

    echo json_encode($changeFolder);

}else{
    echo json_encode(false);
}