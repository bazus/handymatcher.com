<?php

$pagePermissions = array(false,array(1),true,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/autoCampaigns.php");

$autoCampaigns = new autoCampaigns($bouncer["credentials"]["orgId"]);
$campaigns = $autoCampaigns->getCampaigns();

echo json_encode($campaigns);