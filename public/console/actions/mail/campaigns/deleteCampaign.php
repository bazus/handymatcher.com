<?php

if(isset($_POST["campaignId"])){

    $pagePermissions = array(false,array(1),true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/autoCampaigns.php");

    $campaignId = $_POST["campaignId"];

    $autoCampaigns = new autoCampaigns($bouncer["credentials"]["orgId"]);
    $deleteCampaign = $autoCampaigns->deleteCampaign($campaignId);

    echo json_encode($deleteCampaign);
}