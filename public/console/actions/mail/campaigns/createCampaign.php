<?php
if(isset($_POST["campaignName"]) && isset($_POST["emailAccountId"]) && isset($_POST["emailTemplateId"]) && isset($_POST["actionType"])){
    $pagePermissions = array(false,array(1),true,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/autoCampaigns.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/autoCampaignsFilters.php");

    $campaignName = $_POST["campaignName"];
    $fromPhoneNumberId = $_POST["fromPhoneNumberId"];
    $smsTextMessage = $_POST["smsTextMessage"];
    $emailAccountId = $_POST["emailAccountId"];
    $emailTemplateId = $_POST["emailTemplateId"];
    $actionType = $_POST["actionType"];
    $daysAfter = $_POST["daysAfter"];

    $ruleSelector = [];
    $ruleApply1 = [];
    $ruleApply2 = [];
    $ruleValue1 = [];
    $ruleValue2 = [];

    if(isset($_POST["ruleSelector"]) && isset($_POST["ruleApply1"]) && isset($_POST["ruleApply2"]) && isset($_POST["ruleValue1"]) && isset($_POST["ruleValue2"])){
        $ruleSelector = $_POST["ruleSelector"];
        $ruleApply1 = $_POST["ruleApply1"];
        $ruleApply2 = $_POST["ruleApply2"];
        $ruleValue1 = $_POST["ruleValue1"];
        $ruleValue2 = $_POST["ruleValue2"];
    }

    $filters = [];
    for($i = 0;$i<count($ruleSelector);$i++){
        $singleFilter = [];
        $singleFilter["key"] = $ruleSelector[$i];

        if($ruleSelector[$i] == "leadProvider"){
            $singleFilter["type"] = $ruleApply2[$i];
            $singleFilter["value"] = $ruleValue2[$i];
        }else{
            $singleFilter["type"] = $ruleApply1[$i];
            $singleFilter["value"] = $ruleValue1[$i];
        }
        $filters[] = $singleFilter;
    }

    $autoCampaignsFilters = new autoCampaignsFilters($bouncer["credentials"]["orgId"]);
    $leadFilters = $autoCampaignsFilters->createLeadFilters($filters);

    if($leadFilters != false && $leadFilters != 0){
        $autoCampaigns = new autoCampaigns($bouncer["credentials"]["orgId"]);
        $createAutoCampaign = $autoCampaigns->createAutoCampaign($campaignName,$fromPhoneNumberId,$smsTextMessage,$emailAccountId,$emailTemplateId,$actionType,$daysAfter,$leadFilters);

        echo json_encode($createAutoCampaign);
        exit;
    }


    echo json_encode(false);
    exit;

}else{
    echo json_encode(false);
}