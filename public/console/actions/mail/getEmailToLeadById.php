<?php
if (isset($_POST['id']) && isset($_POST['userSent'])){
    $pagePermissions = array(false, true, true, array(['mailcenter',1]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailCenter.php");

    $mailCenter = new mailCenter($bouncer["credentials"]["orgId"]);
    $id = $_POST['id'];
    if ($bouncer["credentials"]["userId"] !== $_POST['userSent']){
        if ($bouncer['isUserAnAdmin']){
            require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

            $userClass = new user($_POST['userSent'],false);
            if ($userClass->isValid == true){
                $email = $mailCenter->getEmail($id,$_POST['userSent']);
                if (!$email){
                    echo json_encode(false);
                    exit;
                }
            }else{
                echo json_encode(false);
                exit;
            }

        }else{
            echo json_encode(false);
            exit;
        }
    }else{
        if ($id){
            $email = $mailCenter->getEmail($id,$bouncer["credentials"]["userId"]);
        }else{
            echo json_encode(false);
            exit;
        }
    }
    $email['timeSent'] = date("H:i F jS",strtotime($email['sentDate']));
    echo json_encode($email);

}else{
    echo json_encode(false);
}