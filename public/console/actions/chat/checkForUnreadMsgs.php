<?php

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $chat = new chat($bouncer["credentials"]["orgId"]);
    $unreadMsgs = $chat->getUnreadMsgs($bouncer["credentials"]["userId"]);
    $getOpenChats = $chat->getOpenChats($bouncer["credentials"]["userId"]);

    $result = array_unique(array_merge($unreadMsgs,$getOpenChats));

    echo json_encode($result);