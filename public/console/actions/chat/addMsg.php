<?php

if(isset($_POST['toUserId']) && isset($_POST['msg'])) {

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $toUserId = $_POST['toUserId'];
    $msg = $_POST['msg'];

    $chat = new chat($bouncer["credentials"]["orgId"]);
    $addMsg = $chat->addMsg($bouncer["credentials"]["userId"],$toUserId,$msg);

    echo json_encode($addMsg);

}else{
    echo json_encode(false);
}