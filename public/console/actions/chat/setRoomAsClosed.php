<?php

if(isset($_POST['userId'])) {

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $userId = $_POST['userId'];

    $chat = new chat($bouncer["credentials"]["orgId"]);
    $setRoomAsClosed = $chat->setRoomAsClosed($bouncer["credentials"]["userId"],$userId);

    echo json_encode($setRoomAsClosed);

}else{
    echo json_encode(false);
}