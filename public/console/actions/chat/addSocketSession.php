<?php

if(isset($_REQUEST['socketId']) && isset($_REQUEST['myUserId'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/connect/connect.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $database = new connect();

    $socketId = $_REQUEST['socketId'];
    $myUserId = $_REQUEST['myUserId'];

    $chat = new chat();
    $addSocketSession = $chat->addSocketSession($socketId,$myUserId);

    echo json_encode($addSocketSession);

}else{
    echo json_encode(false);
}