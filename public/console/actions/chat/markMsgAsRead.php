<?php

if(isset($_GET['userId']) && isset($_GET['msgId'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/connect/connect.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $database = new connect();

    $userId = $_GET['userId'];
    $msgId = $_GET['msgId'];

    $chat = new chat();
    $markMsgAsRead = $chat->markMsgAsRead($userId,$msgId);

    echo json_encode($markMsgAsRead);


}else{
    echo json_encode(false);
}