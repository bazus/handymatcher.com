<?php

if(isset($_GET['userId'])) {

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $userId = $_GET['userId'];

    $s3 = new s3();

    $chat = new chat($bouncer["credentials"]["orgId"]);
    $userData = $chat->getUserData($userId);

    $userData["profilePicture"]  = $_SERVER['LOCAL_NL_URL']."/console/img/icons/user.png";
    try{
        $userData["profilePicture"] = $s3->getObjectURL($bucket = "thenetworkleads",$key = "profiles/".$userData['profilePicture']);
    }catch (Exception $e){

    }

    echo json_encode($userData);
}