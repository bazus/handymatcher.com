<?php

$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/chat/chat.php");

$s3 = new s3();

$chat = new chat($bouncer["credentials"]["orgId"]);
$usersInOrganization = $chat->getUsersInOrganization($bouncer["credentials"]["userId"]);


echo json_encode($usersInOrganization);