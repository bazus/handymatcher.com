<?php

if(isset($_GET['userId'])){

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/connect/connect.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");

    $database = new connect();

    $userId = $_GET['userId'];

    $chat = new chat();
    $getUserSocketSession = $chat->getUserSocketSession($userId);

    echo json_encode($getUserSocketSession);
    
}elseif(isset($_GET['logId'])){
    // get all users sockets sessions that gets updates for this lead ID of the log (admins and the user that is set to the lead)

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/connect/connect.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/chat/chat.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

    $database = new connect();

    $leadId = NULL;
    $logId = $_GET['logId'];

    // Get log data by log ID (no need to worry about security since it will only send socket to whoever this lead belongs to)
    $errorVar = array("leadLog Class","getNumberOfUnreadLogs()",4,"Notes",array());

    $binds = [];
    $binds[] = array(':id', $logId, PDO::PARAM_INT);

    $getIt = $GLOBALS['connector']->execute("SELECT leadId FROM networkleads_db.leadLog WHERE id=:id",$binds,$errorVar);
    if(!$getIt){
        return false;
    }else{
        $r = $GLOBALS['connector']->fetch($getIt);
        $leadId = $r["leadId"];
    }

    $users = [];

    // get organization ID from lead
    $lead = new lead($leadId,NULL);
    $leadData = $lead->getData();
    $orgId = $leadData['orgId'];

    // add the user assigned to lead to the list
    $users[] = $leadData["userIdAssigned"];

    $organization = new organization($orgId);
    $admins = $organization->getUsersOfMyCompany("",true,true);

    $finalUsers = array_unique(array_merge($users,$admins));

    $chat = new chat();

    // loop through all the users and get their socket sessions
    $usersSocketSessions = [];
    foreach ($finalUsers as $finalUser){
        $getUserSocketSession = $chat->getUserSocketSession($finalUser);
        $usersSocketSessions = array_merge($getUserSocketSession,$usersSocketSessions);
    }
    echo json_encode(array_unique($usersSocketSessions));

}else{
    echo json_encode(false);
}