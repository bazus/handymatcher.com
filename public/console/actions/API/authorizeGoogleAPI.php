<?php

if(isset($_GET['app'])) {

    if (!isset($_SERVER['LOCAL_NL_PATH'])) {
        $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
    }

    require $_SERVER['LOCAL_NL_PATH'] . '/vendor/autoload.php';

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/googleAPI.php");

    $app = $_GET['app'];

    $googleAPI = new googleAPI(NULL);
    $authUrl = $googleAPI->createAuthUrl($app);

    header('Location: ' . $authUrl);
    exit;
}