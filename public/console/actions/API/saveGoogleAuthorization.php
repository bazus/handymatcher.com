<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}
require $_SERVER['LOCAL_NL_PATH'] . '/vendor/autoload.php';

if(isset($_GET["code"]) && isset($_GET["app"])) {

    $pagePermissions = array(false, true, true, array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/API/googleAPI.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/calendarSettings.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/googlecalendar.php");


    $googleAPI = new googleAPI(NULL);

    $authCode = $_GET['code'];
    $app = $_GET['app'];

    $accessToken = $googleAPI->convertCodeToAccessToken($authCode,$app);

    if($app == "gmail") {
        $outgoingmailserver = "";
        $fullName = "";
        $emailAddress = "";
        $emailPassword = "";
        $emailPort = "";
        $accountType = "4";


        $profileData = $googleAPI->getProfileData();

        $fullName = $profileData->name;
        $emailAddress = $profileData->email;

        $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
        $doesAccountExistsAndActive = $mailaccounts->doesAccountExistsAndActive($emailAddress, "4");

        if ($doesAccountExistsAndActive == false) {
            // account does not exists - create a new one
            $createAccount = $mailaccounts->createAccount($outgoingmailserver, $fullName, $emailAddress, $emailPassword, $emailPort, $accountType, serialize($accessToken));
            if ($createAccount["status"] == true) {
                $update = $bouncer["organization"]->setDefaultMailAccount($createAccount["accountId"]);
                header('Location: ' . $_SERVER['LOCAL_NL_URL'] . '/console/categories/mail/mailSettings.php?newAccount=added');
                exit;
            }else{
                header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/categories/mail/mailSettings.php');
                exit;
            }
        } else {
            // account exists - update the existing account
            $updateAccountOtherData = $mailaccounts->updateAccountOtherData($doesAccountExistsAndActive, serialize($accessToken));
            if ($mailaccounts == true) {
                header('Location: ' . $_SERVER['LOCAL_NL_URL'] . '/console/categories/mail/mailSettings.php?newAccount=authorized');
                exit;
            }else{
                header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/categories/mail/mailSettings.php');
                exit;
            }
        }
    }

    if($app == "calendar"){
        $profileData = $googleAPI->getProfileData();

        $emailAddress = $profileData->email;

        $accessToken["email"] = $emailAddress;

        $service = new Google_Service_Calendar($googleAPI->client);

        $googlecalendar = new googlecalendar($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"], $service);
        $googleCalendarNetworkLeads_ID = $googlecalendar->createCalendar();

        $accessToken["baseCalendarID"] = $googleCalendarNetworkLeads_ID;

        $calendarSettings = new calendarSettings($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
        $updateGoogleCalendarAccount = $calendarSettings->updateGoogleCalendarAccount(serialize($accessToken));

        header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/categories/user/calendar.php');
        exit;
    }


}

// Fallback - code should not reach this point
header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/');
exit;