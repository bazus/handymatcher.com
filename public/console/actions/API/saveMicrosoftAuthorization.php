<?php


if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}
require $_SERVER['LOCAL_NL_PATH'] . '/vendor/autoload.php';

if(isset($_GET["code"])) {

    $pagePermissions = array(false, true, true, array(['mailcenter',3]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/API/microsoftAPI.php");

    $microsoftAPI = new microsoftAPI();

    $authCode = $_GET['code'];

    $tokens = $microsoftAPI->convertCodeToAccessToken($authCode);

    $profileData = $microsoftAPI->getProfileData();

    $mailaccounts = new mailaccounts($bouncer["credentials"]["userId"], $bouncer["credentials"]["orgId"]);
    $doesAccountExistsAndActive = $mailaccounts->doesAccountExistsAndActive($profileData["email"], "5");

    if ($doesAccountExistsAndActive == false) {
        // account does not exists - create a new one
        $createAccount = $mailaccounts->createAccount("", $profileData["fullName"], $profileData["email"], "", "", "5", serialize($tokens));
        if ($createAccount["status"] == true) {
            $update = $bouncer["organization"]->setDefaultMailAccount($createAccount["accountId"]);
            header('Location: ' . $_SERVER['LOCAL_NL_URL'] . '/console/categories/mail/mailSettings.php?newAccount=added');
            exit;
        }else{
            header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/categories/mail/mailSettings.php');
            exit;
        }
    } else {
        // account exists - update the existing account
        $updateAccountOtherData = $mailaccounts->updateAccountOtherData($doesAccountExistsAndActive, serialize($tokens));
        if ($mailaccounts == true) {
            header('Location: ' . $_SERVER['LOCAL_NL_URL'] . '/console/categories/mail/mailSettings.php?newAccount=authorized');
            exit;
        }else{
            header('Location: '.$_SERVER['LOCAL_NL_URL'].'/console/categories/mail/mailSettings.php');
            exit;
        }
    }
}
