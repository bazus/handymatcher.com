<?php

if (!isset($_SERVER['LOCAL_NL_PATH'])) {
    $_SERVER['LOCAL_NL_PATH'] = "/var/www/html/networkleads/public_html/current";
}

require $_SERVER['LOCAL_NL_PATH'] . '/vendor/autoload.php';

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/microsoftAPI.php");

$microsoftAPI = new microsoftAPI();
$authUrl = $microsoftAPI->createAuthUrl();

header('Location: ' . $authUrl);
exit;
