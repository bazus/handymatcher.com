<?php

if(isset($_POST['id']) && isset($_POST['providerName']) && isset($_POST['emailAddress']) && isset($_POST['website']) && isset($_POST['address']) && isset($_POST['phone']) && isset($_POST['fax']) && isset($_POST['contactName']) && isset($_POST['pricePerLead'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");

    $id = $_POST['id'];
    $providerName = $_POST['providerName'];
    $emailAddress = $_POST['emailAddress'];
    $website = $_POST['website'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $fax = $_POST['fax'];
    $contactName = $_POST['contactName'];
    $pricePerLead = $_POST['pricePerLead'];

    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $updateProvider = $leadProviders->updateProvider($id,$providerName,$emailAddress,$website,$address,$phone,$fax,$contactName,$pricePerLead);

    echo json_encode($updateProvider);
}else{
    echo json_encode(false);
}