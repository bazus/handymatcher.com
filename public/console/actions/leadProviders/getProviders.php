<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 27/09/2018
 * Time: 21:37
 */
$pagePermissions = array(false,true);

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadProviders.php");

$leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);

echo json_encode($leadProviders->getProviders());