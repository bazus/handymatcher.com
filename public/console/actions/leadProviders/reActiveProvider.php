<?php
if(isset($_POST['providerId'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");

    $providerId = $_POST['providerId'];

    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $reActiveProvider = $leadProviders->toggleProvider($providerId,1);

    echo json_encode($reActiveProvider);
}else{
    echo json_encode(false);
}