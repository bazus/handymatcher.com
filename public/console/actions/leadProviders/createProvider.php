<?php
$pagePermissions = array(false,true,true,true);
if(isset($_POST['providerName']) && isset($_POST['emailAddress']) && isset($_POST['website']) && isset($_POST['address']) && isset($_POST['phone']) && isset($_POST['fax']) && isset($_POST['contactName']) && isset($_POST['pricePerLead'])) {
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");

    $providerName = $_POST['providerName'];
    $emailAddress = $_POST['emailAddress'];
    $website = $_POST['website'];
    $address = $_POST['address'];
    $phone = $_POST['phone'];
    $fax = $_POST['fax'];
    $contactName = $_POST['contactName'];
    $pricePerLead = $_POST['pricePerLead'];

    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $createProvider = $leadProviders->createProvider($providerName,$emailAddress,$website,$address,$phone,$fax,$contactName,$pricePerLead);

    echo json_encode($createProvider);
}else{
    echo json_encode(false);
}