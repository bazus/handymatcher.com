<?php
$pagePermissions = array(false,true);
if(isset($_POST['providerId'])) {
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadProviders.php");

    $providerId = $_POST['providerId'];

    $leadProviders = new leadProviders($bouncer["credentials"]["orgId"]);
    $removeProvider = $leadProviders->removeProvider($providerId);

    echo json_encode($removeProvider);
}else{
    echo json_encode(false);
}