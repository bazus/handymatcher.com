<?php
if(isset($_POST['userId']) && isset($_POST['comments']) && isset($_POST['fullName']) && isset($_POST['email']) && isset($_POST['phoneCC']) && isset($_POST['phone']) && isset($_POST['jobTitle']) && isset($_POST['department']) && isset($_POST['userType'])) {
    $pagePermissions = array(false,true,true,array(["users",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $userId = $_POST['userId'];
    $comments = $_POST['comments'];
    $fullName = $_POST['fullName'];
    $email = $_POST['email'];
    $phoneCC = $_POST['phoneCC'];
    $phone = $_POST['phone'];
    $jobTitle = $_POST['jobTitle'];
    $department = $_POST['department'];
    $userType = $_POST['userType'];
    if (isset($_POST['userSalesBonusPerc']) && $_POST['userSalesBonusPerc'] > 0 && $_POST['userSalesBonusPerc'] < 100){
        $userSalesBonusPerc = $_POST['userSalesBonusPerc'];
    }else{
        $userSalesBonusPerc = 0;
    }

    $user = new user($_POST['userId']);

    $updateFullName = $user->updateFullName($fullName);
    $updatePhone = $user->updatePhone($phoneCC,$phone);
    $updateJobTitle = $user->updateJobTitle($jobTitle);
    $updateDepartment = $user->updateDepartment($department);
    $updateUserType = $user->updateUserType($userType);
    $updateSalesBonusPerc = $user->updateSalesBonusPerc($userSalesBonusPerc);
    $updateComments = $user->updateComments($comments);

    if($updateFullName && $updatePhone && $updateJobTitle && $updateDepartment && $updateComments && $updateUserType && $updateSalesBonusPerc){
        echo json_encode(true);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}