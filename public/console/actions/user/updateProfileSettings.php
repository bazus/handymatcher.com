<?php

    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $user = new user($bouncer["credentials"]["userId"]);

    $allOK = true;

    if(isset($_POST['emailnewlead']) && isset($_POST['emailnewlead_email'])) {

        $emailnewlead = $_POST['emailnewlead'];
        $emailnewlead_email = $_POST['emailnewlead_email'];
        $updateemailnewlead = $user->updateemailnewlead($emailnewlead,$emailnewlead_email);

        if(!$updateemailnewlead) {
            $allOK = false;
        }
    }


    if (isset($_POST['smsnewlead'])) {

         $smsnewlead = $_POST['smsnewlead'];
         $updatesmsnewlead = $user->updatesmsnewlead($smsnewlead);

         if (!$updatesmsnewlead) {
             $allOK = false;
         }
     }

    if(isset($_POST['helpMode'])){
        $helpMode = $_POST['helpMode'];

        $updateHelpMode = $user->updateHelpMode($helpMode);
        if(!$updateHelpMode) {
            $allOK = false;
        }
    }

    if(isset($_POST['leadGoogleMaps'])){
        $leadGoogleMaps = $_POST['leadGoogleMaps'];

        $showleadGoogleMaps = $user->showleadGoogleMaps($leadGoogleMaps);
        if(!$showleadGoogleMaps) {
            $allOK = false;
        }
    }

    if(isset($_POST['weeklyEmail'])){
        $weeklyEmail = $_POST['weeklyEmail'];

        $receiveWeeklyEmail = $user->updateReceiveWeeklyReportByEmail($weeklyEmail);
        if(!$receiveWeeklyEmail) {
            $allOK = false;
        }
    }


    if(isset($_POST['localTimeZone'])){
        $localTimeZone = $_POST['localTimeZone'];

        $updateLocalTimeZone = $user->updateLocalTimeZone($localTimeZone);
        if(!$updateLocalTimeZone) {
            $allOK = false;
        }
    }


    if($allOK){
        echo json_encode(true);
        exit;
    }else{
        echo json_encode(false);
        exit;
    }
