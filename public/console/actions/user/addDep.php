<?php

if(isset($_POST['title']) && isset($_POST['description']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zip']) && isset($_POST['phone']) && isset($_POST['owner']) && isset($_POST['address'])){

    $pagePermissions = array(false,true,false,array(["departments",3]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

    $title = $_POST['title'];
    $description = $_POST['description'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $owner = $_POST['owner'];

    $organization = new organization($bouncer["credentials"]["orgId"]);
    $department = $organization->addDepartment($title,$description,$address,$city,$state,$zip,$phone,$owner);

    echo json_encode($department);

}else{
    echo json_encode(false);
}