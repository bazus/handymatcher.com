<?php
$pagePermissions = array(false,true);

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/userSettings.php");

$userSettings = new userSettings($bouncer["credentials"]["userId"]);
$toggle_openSideBar = $userSettings->toggle_openSideBar();

echo json_encode($toggle_openSideBar);