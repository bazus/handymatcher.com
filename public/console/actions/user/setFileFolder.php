<?php

if(isset($_POST['folderID']) && isset($_POST['fileID'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $folder = $_POST['folderID'];
    $file = $_POST['fileID'];

    if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
        $user = new user($_POST['userIdSelected']);
        $changeFolder = $user->setImageFolder($_POST['userIdSelected'],$folder,$file);
    }else {
        $user = new user($bouncer["credentials"]["userId"]);
        $changeFolder = $user->setImageFolder($bouncer["credentials"]["userId"],$folder,$file);
    }
    echo json_encode($changeFolder);

}else{
    echo json_encode(false);
}