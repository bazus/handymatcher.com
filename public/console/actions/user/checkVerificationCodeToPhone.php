<?php
if(isset($_POST['code'])) {

    $pagePermissions = array(false, true, true, true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/phoneVerification.php");

    $code = $_POST['code'];

    $phoneVerification = new phoneVerification($bouncer["credentials"]["userId"]);

    // Important for checking if a code was sent in the last 3 minutes
    date_default_timezone_set("America/New_York");

    $checkVerificationCode = $phoneVerification->verifyPhone($bouncer["userData"]["phoneNumber"],$code);

    echo json_encode($checkVerificationCode);
}