<?php

if(isset($_POST['enable'])) {
    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $enable = $_POST['enable'];

    $user = new user($bouncer["credentials"]["userId"]);

    $updateFirstTutorial = $user->updateFirstTutorial($enable);
    if ($updateFirstTutorial){
        echo json_encode(true);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}