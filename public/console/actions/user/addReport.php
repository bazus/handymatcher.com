<?php

if(isset($_POST['issueType']) && isset($_POST['issueTitle']) && isset($_POST['description'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/support.php");

    $issueType = $_POST['issueType'];
    $issueTitle = $_POST['issueTitle'];
    $description = $_POST['description'];

    $support = new support($bouncer["credentials"]["userId"]);
    $createReport = $support->createReport($issueTitle,$issueType,$description);

    echo json_encode($createReport);

}