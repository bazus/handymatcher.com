<?php

if(isset($_POST['folderID'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $folder = $_POST['folderID'];

    if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
        $user = new user($_POST['userIdSelected']);
        $folders = $user->getUserFiles($_POST['userIdSelected'],$folder);
    }else {
        $user = new user($bouncer["credentials"]["userId"]);
        $folders = $user->getUserFiles($bouncer["credentials"]["userId"],$folder);
    }
    echo json_encode($folders);

}else{
    echo json_encode(false);
}