<?php

    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

    $user = new user($bouncer["credentials"]["userId"]);
    $turnOffFirstTutorial = $user->turnOffFirstTutorial();

    echo json_encode($turnOffFirstTutorial);
