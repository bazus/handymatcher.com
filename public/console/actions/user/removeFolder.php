<?php

if(isset($_POST['folderID'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $folderID = $_POST['folderID'];

    if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
        $user = new user($_POST['userIdSelected']);
        $folder = $user->removeFolder($_POST['userIdSelected'],$folderID);
    }else {
        $user = new user($bouncer["credentials"]["userId"]);
        $folder = $user->removeFolder($bouncer["credentials"]["userId"],$folderID);
    }

    echo json_encode($folder);

}