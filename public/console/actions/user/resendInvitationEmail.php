<?php


require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

$SesClient = new SesClient([
    'credentials' => array(
        'key' => $_SERVER['AWS_S3_KEY'],
        'secret' => $_SERVER['AWS_S3_SECRET'],
    ),
    'version' => '2010-12-01',
    'region'  => 'us-east-1'
]);

if(isset($_POST["userId"]) && isset($_POST["email"])) {
    $pagePermissions = array(true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/classes/register.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");

    $userId = $_POST["userId"];
    $email = $_POST['email'];

    $register = new register();
    $sendInvitation = $register->reSendInvitationEmail($SesClient,$userId,$email);

    echo json_encode($sendInvitation["status"]);
}else{
    echo json_encode(false);
}