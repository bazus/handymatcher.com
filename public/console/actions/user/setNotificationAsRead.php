<?php

if(isset($_POST['postId'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/notifications.php");

    $postId = $_POST['postId'];

    $notifications = new notifications($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$bouncer["userData"]["isAdmin"]);
    $notifications->setNotificationAsRead($postId);

    echo json_encode(true);

}