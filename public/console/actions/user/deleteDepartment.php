<?php
if(isset($_POST['depId'])) {

    $pagePermissions = array(false,true,true,array(['organization',4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/department.php");

    $depId = $_POST['depId'];

    $department = new department($depId,$bouncer["credentials"]["orgId"]);
    $deleteDepartment = $department->deleteDepartment();

    if($deleteDepartment){
        echo json_encode(true);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}