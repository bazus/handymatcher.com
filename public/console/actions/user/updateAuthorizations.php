<?php

if(isset($_POST['userId'])){
    $pagePermissions = array(false,true,true,array(["users",2]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");

    $userId = $_POST['userId'];
    unset($_POST['userId']);
    $userAuthorizations = $_POST;

    $singleUserAuthorization = new userAuthorization($userId);

    $isAllOK = true;
    foreach ($userAuthorizations as $k=>$v){
        $updateUserAuthorization = $singleUserAuthorization->updateUserAuthorization($k,$v);
        if(!$userAuthorizations){$isAllOK = false;}
    }

    echo json_encode($isAllOK);

}else{
    echo json_encode(false);
}