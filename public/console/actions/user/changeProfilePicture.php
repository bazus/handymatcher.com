<?php

$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

$s3 = new s3();

$final_target_file = "";
$uploadOk = 1;
$collectData = [];

if(isset($_FILES) && isset($_FILES['file']) && $_FILES["file"]["tmp_name"] != ""){

    $thisDate = date("Y.m.d.H-i-s", strtotime("now"));

    $filePathInfo = pathinfo($_FILES["file"]["name"]);
    $newFileName = $bouncer["credentials"]["userId"]. "." .hash("md5",$filePathInfo["filename"]) . $thisDate . ".jpeg" ;

    $target_dir = $_SERVER['LOCAL_NL_PATH']."/console/files/profilePictures/";
    $target_file = $target_dir . $newFileName;
    $final_target_file = $_SERVER['LOCAL_NL_PATH']."/console/files/profilePictures/" . $newFileName;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);


    // Check if file already exists
    if (file_exists($target_file)) {
        $collectData['error'] = ["Sorry, file already exists."];
        $uploadOk = 0;
    }
    // max file size in MB
    $maxFileSize = 5;
    if ($_FILES['file']['size'] > ($maxFileSize*1024*1024)) {
        $collectData['error'] = ["Sorry, file Size Should Be Below $maxFileSize"."MB."];
        $uploadOk = 0;
    }
    if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg"  && $imageFileType != "JPEG" && $imageFileType != "gif") {
        $collectData['error'] = ["Sorry, only JPG, JPEG, PNG, PDF & GIF files are allowed."];
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $collectData['error'][] = ["Sorry, your file was not uploaded."];
        $collectData['success'] = false;
        echo json_encode($collectData);
        exit;
        // if everything is ok, try to upload file
    } else {
        $file_name = $newFileName;
        $temp_file_location = $_FILES['file']['tmp_name'];
        $result = $s3->putObject("thenetworkleads","profiles/".$file_name,$temp_file_location,"public-read");
        if ($result->get("@metadata")['statusCode'] == 200) {
            require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

            $user = new user($bouncer["credentials"]["userId"]);
            $res = $user->changeProfileImage($result->get("@metadata")['effectiveUri']);
            if($res == true){
                $collectData['success'] = true;
                $collectData['destination'] = $result->get("@metadata")['effectiveUri'];
                echo json_encode($collectData);
                exit;
            }else{
                $collectData['error'][] = ["Error - Please Contact Support With 'Error 901'"];
                $collectData['success'] = false;
                echo json_encode($collectData);
                exit;
            }
        } else {
            $collectData['error'][] = ["Sorry, there was an error uploading your file."];
            $collectData['success'] = false;
            echo json_encode($collectData);
            exit;
        }
    }
}else{
    $collectData['error'][] = ["Sorry, file Size Should Be Below 5 MB."];
    $collectData['error'][] = ["Sorry, there was an error uploading your file."];
    $collectData['success'] = false;
    echo json_encode($collectData);
    exit;
}