<?php
$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

$s3 = new s3();

$final_target_file = "";
$uploadOk = 1;
$collectData = [];
if(intval($_SERVER['CONTENT_LENGTH'])>0 && count($_POST)===0) {
    header("location: ".$_SERVER['LOCAL_NL_URL']."/console/categories/user/fileManager.php?error=1");
}else{
    if($_FILES["file"]["tmp_name"] != ""){

        $thisDate = date("Y.m.d.H-i-s", strtotime("now"));

        $filePathInfo = pathinfo($_FILES["file"]["name"]);
        $newFileName = $bouncer["credentials"]["userId"]. "." .hash("md5",$filePathInfo["filename"]) . $thisDate . "." . $filePathInfo["extension"];

        $target_dir = $_SERVER['LOCAL_NL_PATH']."/console/files/profilePictures/";
        $target_file = $target_dir . $newFileName;
        $final_target_file = $_SERVER['LOCAL_NL_PATH']."/console/files/profilePictures/" . $newFileName;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);


        // Check if file already exists
        if (file_exists($target_file)) {
            $collectData['error'][] = ["Sorry, file already exists."];
            $uploadOk = 0;
        }
        // max file size in MB
        $maxFileSize = 10;
        if ($_FILES['file']['size'] > ($maxFileSize*1024*1024)) {
            $collectData['error'][] = ["Sorry, file Size Should Be Below $maxFileSize"."MB."];
            $uploadOk = 0;
        }

        // Why you check both uppercase and lower case instead of just changing it to lowercase and checking only the lowercase?
        // for an example : if(strtolower($imageFileType) == "png")
        if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg"  && $imageFileType != "JPEG"
            && $imageFileType != "gif"  && $imageFileType != "pdf"  && $imageFileType != "doc"  && $imageFileType != "docx"  && $imageFileType != "xls"
            && $imageFileType != "xlsx" && $imageFileType != "mp3" && $imageFileType != "wav" && $imageFileType != "PAGES" && $imageFileType != "pages" && $imageFileType != "numbers" && $imageFileType != "NUMBERS" ) {

            $collectData['error'][] = ["Sorry, only jpg, jpeg, png, pdf, numbers, pages, doc, docx, mp3, wav, xls, xlsx & gif files are allowed."];
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $collectData['error'][] = ["Sorry, your file was not uploaded."];
            $collectData['success'] = false;
            echo json_encode($collectData);
            exit;
            // if everything is ok, try to upload file
        } else {
            $fileType = false;
            if ($imageFileType == "jpg" || $imageFileType == "JPG" || $imageFileType == "jpeg" || $imageFileType == "JPEG"){
                $fileType = "image/jpeg";
            }
            if ($imageFileType == "png" || $imageFileType == "PNG"){
                $fileType = "image/png";
            }


            $file_name = $newFileName;
            $temp_file_location = $_FILES['file']['tmp_name'];
            $result = $s3->putObject("thenetworkleads","filesystem/".$file_name,$temp_file_location,"private",$fileType);
            if ($result->get("@metadata")['statusCode'] == 200) {
                require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

                if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
                    $user = new user($_POST['userIdSelected']);
                    $res = $user->addFile($_POST['userIdSelected'],$imageFileType,"filesystem/".$newFileName,$filePathInfo["filename"].'.'.$filePathInfo["extension"],$_POST['folderId']);
                    $collectData['success'] = true;
                    echo json_encode($collectData);
                }else {
                    $user = new user($bouncer["credentials"]["userId"]);
                    $res = $user->addFile($bouncer["credentials"]["userId"],$imageFileType,"filesystem/".$newFileName,$filePathInfo["filename"].'.'.$filePathInfo["extension"],$_POST['folderId']);
                    $collectData['success'] = true;
                    echo json_encode($collectData);
                }
                if($res == true){
                    exit;
                }else{
                    $collectData['error'][] = ["Error - Please Contact Support With 'Error 901'"];
                    $collectData['success'] = false;
                    echo json_encode($collectData);
                    exit;
                }
            } else {
                $collectData['error'][] = ["Sorry, there was an error uploading your file."];
                $collectData['success'] = false;
                echo json_encode($collectData);
                exit;
            }
        }
    }
}
