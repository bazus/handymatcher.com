<?php
$ignoreWelcomeMode = true;
$ignoreRenewMode = true;

$pagePermissions = array(true,true);
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

$user = new user($bouncer["credentials"]["userId"]);
$disableWelcomeMode = $user->disableWelcomeMode();
header('location: '.$_SERVER['LOCAL_NL_URL']."/console/");