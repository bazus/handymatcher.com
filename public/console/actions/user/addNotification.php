<?php

if(isset($_POST['group']) && isset($_POST['id']) && isset($_POST['title']) && isset($_POST['description'])) {
    $pagePermissions = array(false,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/notifications.php");

    $type = $_POST['group'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $id = $_POST['id'];

    $notifications = new notifications($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"],$bouncer["userData"]["isAdmin"]);
    $result = $notifications->sendNotification($type,$title,$description,$id);

    echo json_encode($result);

}else{
    echo json_encode(false);
}