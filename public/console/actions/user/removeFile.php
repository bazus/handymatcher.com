<?php

if(isset($_POST['files'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
        $user = new user($_POST['userIdSelected']);
    }else {
        $user = new user($bouncer["credentials"]["userId"]);
    }
    $files = json_decode($_POST['files']);

    if(is_array($files)){
        // ajax call function removeFiles()

        $files = json_decode($_POST['files']);

        foreach ($files as $file){

            $remove = $user->removeFile($file);

        }

    }else{
        // ajax call function removeFile()
        $file = $_POST['files'];
        $remove = $user->removeFile($file);

    }

    echo json_encode($remove);

}