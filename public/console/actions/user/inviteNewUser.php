<?php

require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

$SesClient = new SesClient([
    'credentials' => array(
        'key' => $_SERVER['AWS_S3_KEY'],
        'secret' => $_SERVER['AWS_S3_SECRET'],
    ),
    'version' => '2010-12-01',
    'region'  => 'us-east-1'
]);

$ignoreWelcomeMode = true;
$ignoreRenewMode = true;

if(isset($_POST["fullName"]) && isset($_POST["email"]) && isset($_POST["jobTitle"]) && isset($_POST["countryCodeState"])  && isset($_POST["phoneNumber"]) && isset($_POST["message"])) {
    $pagePermissions = array(false,true,true,array(["users",3]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/classes/register.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userAuthorization.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");

    $fullName = $_POST["fullName"];
    $email = $_POST["email"];
    $jobTitle = $_POST["jobTitle"];
    $countryCodeState = $_POST["countryCodeState"];
    $phoneNumber = $_POST["phoneNumber"];
    $message = $_POST["message"];
    if (isset($_POST['department']) && $_POST['department'] != ""){
        $department = $_POST['department'];
    }else{
        $department = NULL;
    }

    if (isset($_POST['userTypes']) && $_POST['userTypes'] != ""){
        $userType = $_POST['userTypes'];
    }else{
        $userType = 1;
    }

    $organizationId = $bouncer["credentials"]["orgId"];
    $organization = new organization($organizationId);
    $organizationData = $organization->getData();
    $totalUsersInOrganization = $organization->getTotalUsersInOrganization();

    $canAddMoreUsers = $organization->canAddUsers();

    if(!$canAddMoreUsers){
        echo json_encode(false);
        exit;
        die();
    }

    $status = false;

    $register = new register();
    $registerUserId = $register->registerUser($fullName, $email, $countryCodeState, $phoneNumber, $organizationId,$jobTitle,true,false,false,$department,$userType);


    if($registerUserId != false) {
        $userAuthorizations = $_POST['auth'];

        $userAuthorization = new userAuthorization($registerUserId);

        $isAllOK = true;
        $params = array();

        parse_str($userAuthorizations, $params);

        foreach ($params as $k=>$v){
            $updateUserAuthorization = $userAuthorization->updateUserAuthorization($k,$v);
            if(!$params){$isAllOK = false;}
        }

        if($isAllOK == true){
            $createInvitation = $register->createInvitation($bouncer["credentials"]["userId"], $registerUserId, $email, $message);
            if ($createInvitation["status"]) {
                $sendInvitation = $register->sendInvitationEmail($SesClient,$bouncer["userData"]["fullName"], $email, $message, $createInvitation["key"]);
                $status = $createInvitation["status"];
            }
        }
    }
    echo json_encode($status);
}else{
    echo json_encode(false);
}