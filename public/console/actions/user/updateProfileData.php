<?php

if(isset($_POST['fullName']) && isset($_POST['countryCodeState']) && isset($_POST['phoneNumber']) && isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year'])) {
    $pagePermissions = array(false,true,true,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $fullName = $_POST['fullName'];
    $countryCodeState = $_POST['countryCodeState'];
    $phoneNumber = $_POST['phoneNumber'];

    $day = $_POST['day'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    if ( ($day && $month && $year) && ($day != "" && $month != "" && $year != "")) {
        $birthdate = date("Y-m-d", strtotime($year . "-" . $month . "-" . $day));
    }else{
        $birthdate = NULL;
    }

    $user = new user($bouncer["credentials"]["userId"]);

    $updateFullName = $user->updateFullName($fullName);
    $updatePhone = $user->updatePhone($countryCodeState,$phoneNumber);
    if ($birthdate) {
        $updateBirthDate = $user->updateBirthdate($birthdate);
    }else{
        $updateBirthDate = true;
    }

    if(isset($_POST['smsnewlead'])){
        $smsnewlead = $_POST['smsnewlead'];
        $updatesmsnewlead = $user->updatesmsnewlead($smsnewlead);
    }

    if($updateFullName && $updatePhone && $updateBirthDate){
        echo json_encode(true);
        exit;
    }

    echo json_encode(true);
}