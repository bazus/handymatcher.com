<?php

if(isset($_POST['userId'])){
    $pagePermissions = array(false,true,true,array(["users",2]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

    $userId = $_POST['userId'];

    $userId = new user($userId);

    $unsetAnAdmin = $userId->unsetAnAdmin();

    echo json_encode($unsetAnAdmin);

}else{
    echo json_encode(false);
}