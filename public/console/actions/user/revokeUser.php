<?php

if(isset($_POST['userId']) && isset($_POST['type'])){
    $pagePermissions = array(false,true,true,array(["users",4]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/userManagement.php");

    $userId = $_POST['userId'];
    $type = $_POST['type'];

    $userManagement = new userManagement($bouncer["credentials"]["orgId"]);
    $userRevoke = $userManagement->revokeUser($userId,$type);
    echo json_encode($userRevoke);

}else{
    echo json_encode(false);
}