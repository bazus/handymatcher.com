<?php
if(isset($_POST['featureDescription']) && $_POST['featureDescription'] != "" && isset($_POST['featurePhone'])) {

    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
    require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/uploadFile.php";

    function reArrayFiles($file)
    {
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);

        for($i=0;$i<$file_count;$i++)
        {
            foreach($file_key as $val)
            {
                $file_ary[$i][$val] = $file[$val][$i];
            }
        }
        return $file_ary;
    }

    $files = [];
    if(isset($_FILES) && count($_FILES) > 0){
        $files = reArrayFiles($_FILES['featureRequestsFiles']);
    }
    $featureDescription = $_POST['featureDescription'];
    $featurePhone = $_POST['featurePhone'];

    $resp = [];
    $resp["status"] = false;
    $resp["reason"] = "";

    $filesAttached = [];

    foreach($files as $file){
        $uploadFile = new uploadFile();
        $upload = $uploadFile->upload($file,"thenetworkleads","featureRequests");
        if(!$upload["status"]) {
            $resp["reason"] = $upload["reason"];
            echo json_encode($resp);
            exit;
        }else{
            $filesAttached[] = $upload["file"];
        }
    }

    $user = new user($bouncer["credentials"]["userId"]);
    $featureRequest = $user->setFeatureRequest($featureDescription,$featurePhone,$filesAttached);

    if($featureRequest){
        $resp["status"] = true;
        echo json_encode($resp);
        exit;
    }else{
        $resp["status"] = false;
        $resp["reason"] = "Failed sending feature request";
        echo json_encode($resp);
        exit;
    }
}else{
    echo json_encode(false);
}