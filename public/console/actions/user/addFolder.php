<?php

if(isset($_POST['folderName'])) {

    $pagePermissions = array(false,true);

    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $folderName = $_POST['folderName'];

    if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
        $user = new user($_POST['userIdSelected']);
        $folder = $user->addFolder($_POST['userIdSelected'],$folderName);
    }else {
        $user = new user($bouncer["credentials"]["userId"]);
        $folder = $user->addFolder($bouncer["credentials"]["userId"],$folderName);
    }

    echo json_encode($folder);

}