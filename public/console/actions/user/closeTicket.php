<?php

if(isset($_POST['ticketId'])) {

    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/support.php");

    $ticketId = $_POST['ticketId'];

    $support = new support();
    $closeTicket = $support->closeTicket($ticketId);

    echo json_encode($closeTicket);

}