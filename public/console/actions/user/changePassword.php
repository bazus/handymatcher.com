<?php
require $_SERVER['LOCAL_NL_PATH'].'/console/services/aws/aws-autoloader.php';
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

$SesClient = new SesClient([
    'credentials' => array(
        'key' => "AKIAI3WT2MQEWYGX6E7A",
        'secret' => "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",
    ),
    'version' => '2010-12-01',
    'region'  => 'us-east-1'
]);
if(isset($_POST['password']) && isset($_POST['repassword'])) {
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/passwords.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/email.php");

    $user = new user($bouncer["credentials"]["userId"]);

    $checkIfUserSignupWithGoogleAndPasswordIsNull = false;

    if (!isset($_POST['passwordVerify'])){
        $checkIfUserSignupWithGoogleAndPasswordIsNull = $user->checkIfUserSignupWithGoogleAndPasswordIsNull();
        if (!$checkIfUserSignupWithGoogleAndPasswordIsNull){
            echo json_encode(false);
            exit;
        }
    }

    $password = $_POST['password'];
    $repassword = $_POST['repassword'];
    if (!$checkIfUserSignupWithGoogleAndPasswordIsNull) {
        $passwordVerify = $_POST['passwordVerify'];
    }


    $passwords = new passwords();
    $isPasswordValid = $passwords->isPasswordValid($password, $repassword);

    if (!$isPasswordValid) {
        echo json_encode(false);
        exit;
    } else {
        // Change the password
        if ($checkIfUserSignupWithGoogleAndPasswordIsNull){
            $verify = [];
            $verify['valid'] = true;
        }else {
            $verify = $user->didPasswordVerifySuccess($passwordVerify);
        }
        if ($verify['valid'] == true){
            $changePassword = $user->changePassword($password);
            echo json_encode($changePassword);
            if ($changePassword === true){

                $email = new email($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);
                $title = "Password Changed";
                $title2 = "Password changed";
                $userEmail = $bouncer['userData']['email'];
                $msg = "The password for your account ($userEmail) has been changed.<br>If this action was not made by you please <a href='".$_SERVER['LOCAL_NL_URL']."/index.php?contactUs=1'>contact us</a> immediately.";

                $content = $email->getHTMLContent($title,$title2 = "",$msg,array());
                $email->sendMailFromUs($SesClient,"Network Leads", "no-reply@network-leads.com",  [$userEmail],$title, $title2, $content, "google");

            }
            exit;
        }else{
            echo json_encode(false);
            exit;
        }
    }

    echo json_encode(false);
}