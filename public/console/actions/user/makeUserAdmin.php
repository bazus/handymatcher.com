<?php

if(isset($_POST['userId'])){
    $pagePermissions = array(true,true,true,array(["users",2]));
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

    $userId = $_POST['userId'];

    $userId = new user($userId);

    $makeAnAdmin = $userId->makeAnAdmin();

    echo json_encode($makeAnAdmin);

}else{
    echo json_encode(false);
}