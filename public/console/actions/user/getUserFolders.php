<?php
$pagePermissions = array(false,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

$folderId = $_POST['folderID'];
if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
    $user = new user($_POST['userIdSelected']);
}else {
    $user = new user($bouncer["credentials"]["userId"]);
}

$folders = $user->getUserFolders($folderId);

echo json_encode($folders);
