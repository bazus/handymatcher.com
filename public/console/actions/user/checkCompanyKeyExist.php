<?php

$pagePermissions = array(true,true);

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

if(isset($_POST['companyKey']) || isset($_POST['organizationNameKey'])) {

    if(isset($_POST['companyKey'])){
        $companyKey = $_POST['companyKey'];
    }
    if(isset($_POST['organizationNameKey'])){
        $companyKey = $_POST['organizationNameKey'];
    }


    $organization = new organization($bouncer["credentials"]["orgId"]);
    $checkCompanyKey = $organization->checkIfCompanyKeyExist($companyKey);

    if($checkCompanyKey){
        echo 'true';
    } else {
        echo 'false';
    }
}