<?php
if(isset($_POST['depId']) && isset($_POST['title']) && isset($_POST['address']) && isset($_POST['city']) && isset($_POST['state']) && isset($_POST['zip']) && isset($_POST['phone']) && isset($_POST['owner']) && isset($_POST['description'])) {
    $pagePermissions = array(false,true,true,array(["departments",2]));


    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/department.php");

    $depId = $_POST['depId'];
    $title = $_POST['title'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $owner = $_POST['owner'];
    $description = $_POST['description'];

    $department = new department($depId,$bouncer["credentials"]["orgId"]);
    $updateData = $department->updateData($title,$address,$city,$state,$zip,$phone,$owner,$description);

    if($updateData){
        echo json_encode(true);
    }else{
        echo json_encode(false);
    }
}else{
    echo json_encode(false);
}