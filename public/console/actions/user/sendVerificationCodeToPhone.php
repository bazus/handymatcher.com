<?php
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require_once($_SERVER['LOCAL_NL_PATH'].'/console/services/twilio-php-master/Twilio/autoload.php');
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

$client = new Client("ACf47e9b171a873d75115b08e5a0b0479c", "7396715a2433ee5cc491d7bf30ab1c9c");

$pagePermissions = array(false,true,true,true);
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/twilioPhoneNotifications.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/phoneVerification.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organizationTwilio.php");

$phoneVerification = new phoneVerification($bouncer["credentials"]["userId"]);

// Important for checking if a code was sent in the last 3 minutes
date_default_timezone_set("America/New_York");

$createVerificationCode = $phoneVerification->createVerificationCode($bouncer["userData"]["phoneNumber"]);

if($createVerificationCode["status"] == true){

    // Send SMS
    $code = $createVerificationCode["code"];

    $organizationTwilio = new organizationTwilio($bouncer["credentials"]["orgId"], $bouncer["credentials"]["userId"]);
    $userPhoneNumber = $organizationTwilio->unPrettifyPhone($bouncer["userData"]["phoneCountryCode"].$bouncer["userData"]["phoneNumber"]);

    $twilioPhoneNotifications = new twilioPhoneNotifications($client);
    $response = $twilioPhoneNotifications->sendSMS("",$userPhoneNumber,"Network Leads Code : ".$code,false);

    echo json_encode(true);

}else{
    echo json_encode(false);
}