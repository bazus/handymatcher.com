<?php

if(isset($_POST['id']) && isset($_POST['name'])){
    $pagePermissions = array(false,true);
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $id = $_POST['id'];
    $name = $_POST['name'];

    if (isset($_POST['userIdSelected']) && $_POST['userIdSelected'] && $bouncer['isUserAnAdmin'] == true){
        $user = new user($_POST['userIdSelected']);
        $setFolderName = $user->setFolderName($id,$name);
    }else {
        $user = new user($bouncer["credentials"]["userId"]);
        $setFolderName = $user->setFolderName($id,$name);
    }
    echo json_encode($setFolderName);

}else{
    echo json_encode(false);
}