<?php

if(isset($_POST['password']) && isset($_POST['repassword']) && isset($_POST['userId'])) {

    $pagePermissions = array(false,true,true,array(["users",2]));
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/passwords.php");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");

    $password = $_POST['password'];
    $repassword = $_POST['repassword'];
    $userId = $_POST['userId'];

    $passwords = new passwords();
    $isPasswordValid = $passwords->isPasswordValid($password, $repassword);

    if (!$isPasswordValid) {
        echo json_encode(false);
        exit;
    } else {
        // Change the password
        $user = new user($userId);
        $changePassword = $user->changeUserPassword($password);
        echo json_encode($changePassword);
        exit;
    }

    echo json_encode(false);
}