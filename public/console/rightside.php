<?php
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/userSettings.php");
    require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/calendar.php");

    $calendarSideBer = true;
    $calendar = new calendar($bouncer["credentials"]["userId"],$bouncer["credentials"]["orgId"]);

    $fromDate = date("Y-m-d H:i:s",strtotime("now"));
    $toDate = date("Y-m-d H:i:s",strtotime("+7 days"));

    $events = [];

    $operationEvents = $calendar->getOperationsData($fromDate,$toDate);
    if(is_array($operationEvents)){$events = array_merge($events, $operationEvents);}

    $personalEvents = $calendar->getEvents(true,$fromDate,$toDate);
    if(is_array($personalEvents)){$events = array_merge($events, $personalEvents);}

    $organizationEvents = $calendar->getEvents(false,$fromDate,$toDate);
    if(is_array($organizationEvents)){$events = array_merge($events, $organizationEvents);}

    $holidayEvents = $calendar->getHolidays($fromDate,$toDate);
    if(is_array($holidayEvents)){$events = array_merge($events, $holidayEvents);}

    $birthdayEvents = $calendar->getBirthdays($fromDate,$toDate);
    if(is_array($birthdayEvents)){$events = array_merge($events, $birthdayEvents);}


if($events != false){
        usort($events, function($a, $b) {
            return strtotime($b['start']) >= strtotime($a['start']);
        });
    }
?>

<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/css/plugins/chat/style.css" rel="stylesheet">
  <style>
      .sidebar-message{
          border-bottom: 1px solid #f1f1f1;
      }
      .sidebar-message:hover{
          background-color: #f1f1f1;
      }
      .sidebar-container ul.nav-tabs.navs-1 li {
          width: 100%;
      }
      .eventSwal .swal-content {
          background-color: #7cd1f94a;
          padding: 17px;
          border: 1px solid #7cd1f9bf;
          display: block;
          margin: 22px;
          text-align: center;
          color: #61534e;
      }
      .eventSwal .swal-footer {
          background-color: rgb(245, 248, 250);
          margin-top: 32px;
          border-top: 1px solid #E9EEF1;
          overflow: hidden;
      }
      .eventSwal .editEventBtn,.eventSwal .editEventBtn:hover,.eventSwal .editEventBtn:active{
          background: none;
          color: #337ab7;
          border-radius: unset;
          font-weight: 100;
          font-size: 12px;
      }
  </style>
    <div id="right-sidebar" class="animated">
    <div class="sidebar-container">

        <?php
            $calculateRightTabs = 3;
            if(!$calendarSideBer){$calculateRightTabs--;}
            if($GLOBALS['mobileDetect']["isMobile"] ){$calculateRightTabs--;}
        ?>

        <ul class="nav nav-tabs navs-<?php echo $calculateRightTabs; ?>">
            <?php
                if($calendarSideBer) {
            ?>
                <li class="active">
                    <a data-toggle="tab" href="#calendarTab">
                        <i class="fa fa-calendar"></i> <?= (count($events) > 10) ? "10+" : "<small>Cal</small> (".count($events).")" ?>
                    </a>
                </li>
            <?php } ?>
            <?php if(!$GLOBALS['mobileDetect']["isMobile"]){?>
            <li id="rightside_toolbar_chat" class="<?php if(!$calendarSideBer) { ?> active <?php } ?>">
                <a data-toggle="tab" href="#chatTab">
                    <i class="fa fa-commenting"></i> Chat
                </a>
            </li>
            <?php } ?>
            <li <?= ($calculateRightTabs == 1) ? 'class="active"' : ''?>>
                <a data-toggle="tab" href="#settingsTab">
                    <i class="fa fa-gear"></i>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <?php
                if($calendarSideBer) {
            ?>
                <div id="calendarTab" class="tab-pane active">

                <div class="sidebar-title">
                    <h3><i class="fa fa-calendar-check-o"></i> Calendar Events</h3>
                    <small>The Next Week Events.</small><br>
                </div>

                <div id="rightside_toolbar_calendar">
                    <?php
                    $events = array_reverse($events);
                    $eventCounter = 10;
                    if($events){
                        $limitNumberOfEvents = 10;
                        if(count($events)<=10){$limitNumberOfEvents = count($events);}
                        for($i = 0;$i<$limitNumberOfEvents;$i++){
                            $event = $events[$i];
                            if(($event["type"] == "operation") && ($fromDate <= $event["start"]) && ($toDate >= $event["start"]) && $eventCounter > 0){
                                $eventCounter--;
                                ?>
                                <a style="color: unset" href="<?php echo $_SERVER["LOCAL_NL_URL"]."/console/categories/leads/lead.php?leadId=".$event["id"]; ?>" target="_blank">
                                    <div class="sidebar-message" style="cursor: pointer;">
                                        <div class="media-body">
                                            <b>
                                                <?php
                                                    if($event["fromState"] && $event["toState"]){
                                                        echo $event["title"] . " " . '<div class="calendar-move-info">' . $event["fullName"] . '</div>' . " " . '<div class="calendar-move-info">' . " [" . $event["fromState"] . "-" . $event["toState"] . "]" .'</div>';
                                                    }else{
                                                        echo $event["title"] . " " . '<div class="calendar-move-info">' . $event["fullName"] . '</div>';
                                                    }
                                                ?>
                                            </b>
                                            <br>
                                            <small class="text-muted" style="font-size: 0.8em;"><?= date("h:ia",strtotime($event["start"])) ." on ". date("l, F jS",strtotime($event["start"])); ?></small>
                                        </div>
                                    </div>
                                </a>
                                <?php
                            }else{
                                if($limitNumberOfEvents < count($events))
                                    $limitNumberOfEvents++;
                            }
                            if($event["type"] == "event"  && $eventCounter > 0){
                                $eventCounter--;
                                ?>
                                <div class="sidebar-message" style="cursor: pointer;" onclick="showEventDetails('<?= $event['id'] ?>');">
                                    <div class="media-body">
                                        <b><?= $event["title"]?></b>
                                        <br>
                                        <small class="text-muted" style="font-size: 0.8em;"><?= date("h:ia",strtotime($event["start"])) ." on ". date("l, F jS",strtotime($event["start"])); ?></small>
                                    </div>
                                </div>
                                <?php
                            }
                            if($event["type"] == "holiday"  && $eventCounter > 0){
                                $eventCounter--;
                                ?>
                                <div class="sidebar-message">
                                    <div class="media-body">
                                        <b><?= $event["title"]; ?></b>
                                        <br>
                                        <small class="text-muted" style="font-size: 0.8em;"><?= date("h:ia",strtotime($event["start"])) ." on ". date("l, F jS",strtotime($event["start"])); ?></small>
                                    </div>
                                </div>
                                <?php
                            }
                            if($event["type"] == "birthday"  && $eventCounter > 0){
                                $eventCounter--;
                                ?>
                                <div class="sidebar-message">
                                    <div class="media-body">
                                        <b><?= $event["title"]; ?></b>
                                        <br>
                                        <small class="text-muted" style="font-size: 0.8em;"><?= date("l, F jS",strtotime($event["start"])); ?></small>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    } ?>
                </div>
            </div>
            <?php } ?>

            <div id="chatTab" class="tab-pane <?php if(!$calendarSideBer) { ?> active <?php } ?>">
                <div class="sidebar-title" style="padding-top: 9px;">
                    <h3><i class="fa fa-commenting-o"></i> Chat</h3>
                    <small>Click On A User To Start Chatting</small><br>
                    <small id="usersInRoom"></small>
                </div>
                <div class="ibox" id="registered">

                    <div class="ibox-content" id="chatScroll" style="overflow: auto; padding: 0;">

                        <div>
                            <div class="chat-activity-list">
                                <div id="chatUsers" style="height: 100%; cursor: pointer;">
                                </div>
                                <div id="chatContainer" style="height: 100%">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="settingsTab" class="tab-pane <?php if($calculateRightTabs == '1'){echo 'active';}?>">
                <div class="sidebar-title">
                    <h3><i class="fa fa-gears"></i> Settings</h3>
                    <small><i class="fa fa-tim"></i> Manage your account settings.</small>
                </div>

                <div class="setings-item">
                    <span>
                        Mini Sidebar
                    </span>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox" name="openSideBar" class="onoffswitch-checkbox" id="openSideBar" <?php if($bouncer["userSettingsData"]["openSideBar"] == "0"){ ?>checked<?php } ?> onchange="toggle_openSideBar(this)">
                            <label class="onoffswitch-label" for="openSideBar">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="sidebar-content">
                    <h4>Settings</h4>
                    <div class="small">
                        Manage your account settings with this quick one-click options above.
                        Customize you experience with Network Leads and keep your account safe and secure.
                    </div>
                    <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/login/actions/logout.php">
                        <input class="btn-danger" type="button" style="width: 100%;height: 23px;margin-top: 13px;" value="Log out">
                    </a>

                </div>

            </div>
        </div>

    </div>
</div>
<div id="chatsContainer"></div>

<?php if(!$GLOBALS['mobileDetect']["isMobile"]){ ?>
<div id="small-chat">
    <span class="badge badge-warning pull-right" id="chatsCount"></span>
    <a class="open-small-chat" id="open-small-chat" style="display: none;">
        <i class="fa fa-comments"></i>
    </a>
</div>
<?php } ?>


<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/jquery-3.1.1.min.js"></script>
<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";
</script>
    <script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/js/plugins/sweetalert/sweetalertNew.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/inspinia/Static_Full_Version/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script>
    function toggle_openSideBar(obj){
        obj.checked = !obj.checked;

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/user/settings/toggle_openSideBar.php', strReturn = "";
        jQuery.ajax({
            url: strUrl,
            success: function (html) {
                strReturn = html;
            },
            async: true
        }).done(function (data) {
            data = JSON.parse(data);

            if(!data || data.status == false){
                var title = "Oops";
                var msg = "Something went wrong.\n" +
                    "Please try again later.";

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "7000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                var $toast = toastr["error"](msg, title);
                $toastlast = $toast;
            }else{
                event.preventDefault();

                if(data.option == "0"){
                    obj.checked = true;
                    if(!document.body.classList.contains("mini-navbar")){
                        $("body").toggleClass("mini-navbar");
                        SmoothlyMenu();
                    }
                }else{
                    obj.checked = false;
                    if(document.body.classList.contains("mini-navbar")){
                        $("body").toggleClass("mini-navbar");
                        SmoothlyMenu();
                    }
                }
            }
        });
    }
    
    function showEventDetails(id) {

        var strUrl = '<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/actions/system/calendar/getEventById.php';
        jQuery.ajax({
            url: strUrl,
            method: "post",
            data:{
                id:id
            },
            async: true
        }).done(function (data) {
            try {
                data = JSON.parse(data);
                if (data != false){
                    var content = createEventSwal(data);

                    swal({
                        title: data.title,
                        content: content,
                        buttons: {
                            editEvent:{
                                text:"Edit event",
                                className:"editEventBtn"
                            },
                            close:{
                                text:"Close",
                                className:"center-btn"
                            }
                        },
                        className: "eventSwal"
                    }).then((isConfirm)=>{
                        if (isConfirm == "editEvent"){
                            showCustomModal('categories/iframes/showEvent.php?eventId='+id);
                        }
                    });

                }else{
                    toastr.error("Cannot load this event, please try again later","Fail");
                }
            }catch (e) {
                toastr.error("Cannot load this event, please try again later","Fail");
            }
        });

    }

    function createEventSwal(data){
        var content = document.createElement("div");

        var contentDescription = document.createElement("div");
        contentDescription.style.marginTop = "15px";
        var eventDescription = document.createElement("span");
        eventDescription.style.textOverflow = "ellipsis";
        eventDescription.style.overflow = "hidden";
        eventDescription.style.whiteSpace = "nowrap";
        eventDescription.style.maxWidth = "234px";
        eventDescription.style.display = "inline-block";
        var eventDescriptionLabel = document.createElement("label");
        eventDescriptionLabel.classList.add("pull-left");
        var eventDescriptionLabelText = document.createTextNode("Description");
        if (data.description){
            eventDescription.title = data.description;
            var eventDescriptionText = document.createTextNode(data.description);
        }else{
            var eventDescriptionText = document.createTextNode("Empty");
        }
        eventDescriptionLabel.appendChild(eventDescriptionLabelText);
        contentDescription.appendChild(eventDescriptionLabel);
        eventDescription.appendChild(eventDescriptionText);
        contentDescription.appendChild(eventDescription);
        content.appendChild(contentDescription);

        if (data.start_date){
            var contentStartDate = document.createElement("div");
            if (data.formatted_start_date){
                contentStartDate.title = data.formatted_start_date;
            }
            contentStartDate.style.marginTop = "15px";
            var eventStartDateLabel = document.createElement("label");
            eventStartDateLabel.classList.add("pull-left");
            var eventStartDateLabelText = document.createTextNode("Start At");
            var eventStartDate = document.createElement("span");
            var eventStartDateText = document.createTextNode(data.start_date);
            eventStartDateLabel.appendChild(eventStartDateLabelText);
            contentStartDate.appendChild(eventStartDateLabel);
            eventStartDate.appendChild(eventStartDateText);
            contentStartDate.appendChild(eventStartDate);
            content.appendChild(contentStartDate);
        }

        if (data.end_date){
            var contentEndDate = document.createElement("div");
            if (data.formatted_end_date){
                contentEndDate.title = data.formatted_end_date;
            }
            contentEndDate.style.marginTop = "15px";
            var eventEndDateLabel = document.createElement("label");
            eventEndDateLabel.classList.add("pull-left");
            var eventEndDateLabelText = document.createTextNode("End At");
            var eventEndDate = document.createElement("span");
            var eventEndDateText = document.createTextNode(data.end_date);
            eventEndDateLabel.appendChild(eventEndDateLabelText);
            contentEndDate.appendChild(eventEndDateLabel);
            eventEndDate.appendChild(eventEndDateText);
            contentEndDate.appendChild(eventEndDate);
            content.appendChild(contentEndDate);
        }
        if (data.place){
            var contentPlace = document.createElement("div");
            contentPlace.style.marginTop = "15px";
            var eventPlaceLabel = document.createElement("label");
            eventPlaceLabel.classList.add("pull-left");
            var eventPlaceLabelText = document.createTextNode("Location");
            var eventPlace = document.createElement("span");
            var eventPlaceText = document.createTextNode(data.place);
            eventPlaceLabel.appendChild(eventPlaceLabelText);
            contentPlace.appendChild(eventPlaceLabel);
            eventPlace.appendChild(eventPlaceText);
            contentPlace.appendChild(eventPlace);
            content.appendChild(contentPlace);
        }
        return content;
    }

</script>
<script type="text/javascript" src="https://media.twiliocdn.com/sdk/js/client/v1.9/twilio.min.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/dialer/dialer.js"></script>
<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/dialer/dialerApi.js"></script>

<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/categories/user/chat.php");
?>