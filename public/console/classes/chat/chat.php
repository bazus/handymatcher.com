<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 30/08/2018
 * Time: 20:59
 */

class chat
{
    protected $orgId;

    public function __construct($orgId = "")
    {
        $this->orgId = $orgId;
    }

    public function getUsersInOrganization($myUserId = ""){
        $errorVar = array("chat Class","getUsersInOrganization()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':myId', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,profilePicture FROM networkleads_db.users WHERE organizationId=:organizationId AND id!=:myId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $users = array();
            while($user = $GLOBALS['connector']->fetch($getIt)){
                if($user["profilePicture"] == "" || $user["profilePicture"] == NULL){
                    $user["profilePicture"] = $_SERVER['LOCAL_NL_URL']."/console/img/icons/user.png";
                }
                $users[] = $user;
            }
            return $users;
        }

        return false;
    }

    public function getUserData($userId = ""){
        $errorVar = array("chat Class","getUserData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,profilePicture FROM networkleads_db.users WHERE organizationId=:organizationId AND id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $user = $GLOBALS['connector']->fetch($getIt);
            return $user;
        }

        return false;
    }

    public function addMsg($myUserId = "",$toUserId = "",$msg = ""){

        $resp = array();
        $resp["status"] = false;
        $resp["msgId"] = false;

        $errorVar = array("chat Class","addMsg()",4,"Notes",array());

        $binds = [];
        //$binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':myUserId', $myUserId, PDO::PARAM_INT);
        $binds[] = array(':toUserId', $toUserId, PDO::PARAM_INT);
        $binds[] = array(':msg', $msg, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.chat(fromUserId,toUserId,message,sentTime) VALUES(:myUserId,:toUserId,:msg,NOW())",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            $resp["status"] = true;
            $resp["msgId"] = $GLOBALS['connector']->last_insert_id();
            return $resp;
        }

        return false;
    }

    public function getConversation($myUserId = "",$toUserId = ""){
        $errorVar = array("chat Class","getConversation()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':myUserId', $myUserId, PDO::PARAM_INT);
        $binds[] = array(':toUserId', $toUserId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT *,CTZ(sentTime,:OFFSET) AS sentTime FROM networkleads_db.chat WHERE (fromUserId=:myUserId AND toUserId=:toUserId) OR (fromUserId=:toUserId AND toUserId=:myUserId) ORDER BY id DESC LIMIT 10",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $chat = array();
            while($msg = $GLOBALS['connector']->fetch($getIt)){
                if($msg["fromUserId"] == $myUserId){
                    $msg["type"] = "right";
                }else{
                    $msg["type"] = "left";
                }
                $chat[] = $msg;
            }

            // reset isRead
            $binds = [];
            $binds[] = array(':myUserId', $myUserId, PDO::PARAM_INT);
            $binds[] = array(':toUserId', $toUserId, PDO::PARAM_INT);
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.chat SET didRead=1 WHERE fromUserId=:toUserId AND toUserId=:myUserId AND didRead=0",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                return array_reverse($chat);
            }
        }

        return false;
    }

    public function getUnreadMsgs($myUserId = ""){
        $errorVar = array("chat Class","getUnreadMsgs()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':myUserId', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT m1.fromUserId FROM networkleads_db.chat m1 LEFT JOIN networkleads_db.chat m2 ON (m1.fromUserId = m2.fromUserId AND m1.id < m2.id) WHERE m1.toUserId=:myUserId AND m1.didRead=0 AND m2.id IS NULL;",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $msgsFrom = array();
            while($msg = $GLOBALS['connector']->fetch($getIt)){
                $msgsFrom[] = $msg["fromUserId"];
            }
            return $msgsFrom;

        }

        return false;
    }

    public function getOpenChats($myUserId = ""){
        $errorVar = array("chat Class","getOpenChats()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT openChatBoxes FROM networkleads_db.users WHERE id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);

            if($r["openChatBoxes"] == ""){
                $openChatRooms = array();
            }else {
                $openChatRooms = unserialize($r["openChatBoxes"]);
            }
            return $openChatRooms;
        }

        return false;
    }

    public function addSocketSession($socketId,$myUserId){

        $allSocketSessions = [];

        $errorVar = array("chat Class","addSocketSession()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':userId', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.chatSessions WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $totalSockets = $GLOBALS['connector']->fetch_num_rows($getIt);
            if($totalSockets > 0) {
                $getIt = $GLOBALS['connector']->execute("SELECT sockets FROM networkleads_db.chatSessions WHERE userId=:userId", $binds, $errorVar);

                $r = $GLOBALS['connector']->fetch($getIt);
                $allSocketSessions = unserialize($r["sockets"]);
            }
        }

        if(!in_array($socketId,$allSocketSessions)) {
            $allSocketSessions[] = $socketId;

            $binds = array();
            $binds[] = array(':userId', $myUserId, PDO::PARAM_INT);
            $binds[] = array(':sockets', serialize($allSocketSessions), PDO::PARAM_STR);

            if($totalSockets == 0){
                $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.chatSessions(userId,sockets) VALUES(:userId,:sockets)", $binds, $errorVar);
            }else{
                $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.chatSessions SET sockets=:sockets WHERE userId=:userId", $binds, $errorVar);
            }

            if (!$getIt) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    public function deleteSocketSession($socketId,$myUserId){

        $errorVar = array("Sockets","add sockets session",4,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT sockets FROM networkleads_db.chatSessions WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            $allSocketSessions = unserialize($r["sockets"]);

            $key = array_search($socketId, $allSocketSessions);
            unset($allSocketSessions[$key]);
        }


        $binds = array();
        $binds[] = array(':sockets', serialize($allSocketSessions), PDO::PARAM_STR);
        $binds[] = array(':userId', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.chatSessions SET sockets=:sockets WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

    }

    public function getUserSocketSession($userId){

        $errorVar = array("Sockets","add sockets session",4,"Notes",array());


        $binds = array();
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.chatSessions WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $totalSockets = $GLOBALS['connector']->fetch_num_rows($getIt);
            if($totalSockets > 0) {
                $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.chatSessions WHERE userId=:userId", $binds, $errorVar);

                $r = $GLOBALS['connector']->fetch($getIt);
                $allSocketSessions = unserialize($r["sockets"]);
                $allSocketSessions = array_values($allSocketSessions);
                return $allSocketSessions;
            }else{
                return false;
            }
        }

    }

    public function markMsgAsRead($userId,$msgId){

        $errorVar = array("Sockets","",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $msgId, PDO::PARAM_INT);
        $binds[] = array(':toUserId', $userId, PDO::PARAM_INT);

        // reset isRead
        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.chat SET didRead=1 WHERE id=:id AND toUserId=:toUserId",$binds,$errorVar);
        if(!$setIt){
            echo json_encode(false);
        }else{
            echo json_encode(true);
        }

    }

    public function setRoomAsOpen($myUserId,$userId){

        $errorVar = array("chat","setRoomAsOpen()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT openChatBoxes FROM networkleads_db.users WHERE id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            if($r["openChatBoxes"] == ""){
                $openChatRooms = array();
            }else {
                $openChatRooms = unserialize($r["openChatBoxes"]);
            }

            if(!in_array($userId,$openChatRooms)) {
                $openChatRooms[] = $userId;

                $binds = array();
                $binds[] = array(':id', $myUserId, PDO::PARAM_INT);
                $binds[] = array(':openChatBoxes', serialize($openChatRooms), PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET openChatBoxes=:openChatBoxes WHERE id=:id", $binds, $errorVar);
                if (!$setIt) {
                    return false;
                } else {
                    return true;
                }
            }else{
                return true;
            }
        }
        return false;
    }


    public function setRoomAsClosed($myUserId,$userId){

        $errorVar = array("chat","setRoomAsClosed()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $myUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT openChatBoxes FROM networkleads_db.users WHERE id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            if($r["openChatBoxes"] == ""){
                $openChatRooms = array();
            }else {
                $openChatRooms = unserialize($r["openChatBoxes"]);
                $key = array_search($userId, $openChatRooms);
                unset($openChatRooms[$key]);
            }

            $binds = array();
            $binds[] = array(':id', $myUserId, PDO::PARAM_INT);
            $binds[] = array(':openChatBoxes', serialize($openChatRooms), PDO::PARAM_STR);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET openChatBoxes=:openChatBoxes WHERE id=:id",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                return true;
            }
        }
        return false;
    }


}