<?php
/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 2019-10-30
 * Time: 23:52
 */

class googleAPI
{
    // Config
    public $googleClientId = "618065205316-h7hsk3qvc2q16uvaobqh1550f95pl1cl.apps.googleusercontent.com";
    public $googleClientSecret = "vGZlBW8JoiliOJmVjJ79xLKE";

    public $refreshToken = NULL;
    public $client = NULL;

    function __construct($refreshToken = NULL){

        $this->client = new Google_Client(['client_id' => $this->googleClientId]);  // Specify the CLIENT_ID of the app that accesses the backend
        $this->client->setApplicationName("Client_Library_Examples");
        $this->client->setClientSecret($this->googleClientSecret);

        $this->refreshToken = $refreshToken;
    }

    function getScopesForApp($app = NULL){
        $scopes = array();
        $scopes[] = "https://www.googleapis.com/auth/userinfo.email";

        if($app === NULL){return $scopes;}

        if($app == "gmail"){
            $scopes[] = "https://www.googleapis.com/auth/gmail.send";
        }
        if($app == "calendar"){
            $scopes[] = "https://www.googleapis.com/auth/calendar";
        }

        return $scopes;
    }

    public function createAuthUrl($app = NULL){

        $scopes = $this->getScopesForApp($app);

        $this->client->setRedirectUri($_SERVER['LOCAL_NL_URL'] . "/console/actions/API/saveGoogleAuthorization.php?app=".$app);
        $this->client->setScopes($scopes);
        $this->client->setAccessType('offline'); // Gets us our refreshtoken
        $this->client->setPrompt('consent');

        $authUrl = $this->client->createAuthUrl();

        return $authUrl;
    }

    public function convertCodeToAccessToken($authCode = "",$app = NULL)
    {

        $scopes = $this->getScopesForApp($app);

        $this->client->setRedirectUri($_SERVER['LOCAL_NL_URL'] . "/console/actions/API/saveGoogleAuthorization.php?app=".$app);
        $this->client->setScopes($scopes);
        $this->client->setAccessType('offline'); // Gets us our refreshtoken
        $this->client->setPrompt('consent');

        $accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
        $this->client->setAccessToken($accessToken["access_token"]);

        return $accessToken;
    }

    public function getProfileData()
    {
        $oauth = new Google_Service_Oauth2($this->client);
        $profileData = $oauth->userinfo->get();

        return $profileData;
    }

    public function isAuthorized(){
        if($this->client === NULL){return false;}

        $isAuthorized = false;

        if($this->refreshToken == "") {
            return false;
        }else{
            $this->client->fetchAccessTokenWithRefreshToken($this->refreshToken);
            $accessToken = $this->client->getAccessToken();
        }

        $url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=".$accessToken["access_token"];
        $fields = [];

        $ch_new = curl_init( $url );
        $payload = json_encode( array( "data"=> $fields ) );
        curl_setopt( $ch_new, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch_new, CURLOPT_RETURNTRANSFER, true );

        //execute post
        $resp = curl_exec($ch_new);
        $data = json_decode($resp);

        // check if doesnt have an error
        if(!isset($data->error)){
            $isAuthorized = true;
        }

        return $isAuthorized;
    }

    public function sendEmailFromGmail($from = "",$to = "",$cc = "",$subject = "",$description = "",$contentsdata = "")
    {
        $response = array();
        $response['debugOutput'] = "";
        $response['debugCode'] = "";
        $response['debugMessage'] = "";
        $response['status'] = false;

        if(!$this->isAuthorized()){
            shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>"Gmail not authorized - email not sent","mode"=>"debug","env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

            $response['debugOutput'] = "Gmail Not Authorized";
            return $response;
        }

        try {

            $envelope["from"]= "";

            if(is_array($to)){
                $envelope["to"]  = implode(",",$to);
            }else{
                $envelope["to"]  = $to;
            }

            $envelope["cc"]  = $cc;
            $envelope["subject"]  = $subject;

            $part1["type"] = "text";
            $part1["subtype"] = "html";
            $part1["charset"] = "UTF-8";
            $part1["description"] = $description;
            $part1["contents.data"] = $contentsdata;

            $body[1] = $part1;

            $mime = imap_mail_compose ($envelope , $body);
            $mime = rtrim(strtr(base64_encode($mime), '+/', '-_'), '=');

            $service = new Google_Service_Gmail($this->client);

            $message = new Google_Service_Gmail_Message();
            $message->setRaw($mime);


            $message = $service->users_messages->send($from, $message);
            $response['debugOutput'] = json_encode($message);
            $response['debugCode'] = 200;
            $response['debugMessage'] = "OK";
            $response['status'] = true;
        } catch (Exception $e) {
            $response['debugOutput'] = $e->getMessage();
            $response['status'] = false;
            $response['debugCode'] = 500;
            $response['debugMessage'] = "Failed";
        }

        return $response;
    }
}