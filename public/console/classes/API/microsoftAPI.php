<?php
/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 2019-10-30
 * Time: 23:52
 */

use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class microsoftAPI
{
    // Config
    public $clientId = "ac08c34e-ee7f-4747-8f86-06878ae425ca";
    public $clientSecret = "RyB0p8GFmAx4DZAY_8dfOw]KjTQKul_=";

    public $tokens = ["accessToken"=>null,"refresh_token"=>null,"expires"=>null];
    public $oauthClient = NULL;

    function __construct($tokens = NULL){
        if($tokens != NULL){
            if(isset($tokens["accessToken"]) && isset($tokens["refresh_token"]) && isset($tokens["expires"])){
                $this->tokens["accessToken"] = $tokens["accessToken"];
                $this->tokens["refresh_token"] = $tokens["refresh_token"];
                $this->tokens["expires"] = $tokens["expires"];
            }
        }
        $this->init();
    }

    function init(){
        $this->oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => $this->clientId,
            'clientSecret' => $this->clientSecret,
            'redirectUri' => $_SERVER['LOCAL_NL_URL'] . "/console/actions/API/saveMicrosoftAuthorization.php",
            'urlAuthorize' => "https://login.microsoftonline.com/common/oauth2/v2.0/authorize",
            'urlAccessToken' => "https://login.microsoftonline.com/common/oauth2/v2.0/token",
            'urlResourceOwnerDetails' => '',
            'scopes' => "openid offline_access User.Read Mail.Send",
        ]);
    }

    public function createAuthUrl()
    {
        return $this->oauthClient->getAuthorizationUrl(["prompt"=>'login']);
    }

    public function convertCodeToAccessToken($code = ""){
        try {
            // Make the token request
            $accessTokenObj = $this->oauthClient->getAccessToken('authorization_code', [
                'code' => $code
            ]);

            $tokens = [];
            $tokens["accessToken"] = $accessTokenObj->getToken();
            $tokens["refresh_token"] = $accessTokenObj->getRefreshToken();
            $tokens["expires"] = $accessTokenObj->getExpires();

            $this->tokens = $tokens;
            return $this->tokens;
        } catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            //exit('ERROR getting tokens: '.$e->getMessage());
            return false;
        }
    }

    public function isAuthorized(){
        try{
            $this->oauthClient->getAccessToken('refresh_token', [
                'refresh_token' => $this->tokens['refresh_token']
            ]);
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    public function verifyTokens(){
        $now = time() + 300;
        if ($this->tokens['expires'] <= $now) {
            try {
                $newToken = $this->oauthClient->getAccessToken('refresh_token', [
                    'refresh_token' => $this->tokens['refresh_token']
                ]);

                $tokens = [];
                $tokens["accessToken"] = $newToken->getToken();
                $tokens["refresh_token"] = $newToken->getRefreshToken();
                $tokens["expires"] = $newToken->getExpires();

                $this->tokens = $tokens;
                return true;
            }
            catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                return false;
            }
        }else{
            return true;
        }
        return false;
    }

    public function getProfileData(){

        // First - verify the access token
        $this->verifyTokens();

        $graph = new Graph();
        $graph->setAccessToken($this->tokens["accessToken"]);

        $user = $graph->createRequest('GET', '/me')
            ->setReturnType(Model\User::class)
            ->execute();

        $name = $user->getDisplayName();
        $email = $user->getUserPrincipalName();

        return ["fullName"=>$name,"email"=>$email];
    }

    public function sendEmail($from = "",$to = "",$cc = "",$subject = "",$description = "",$contentsdata = "")
    {

        $response = array();
        $response['debugOutput'] = "";
        $response['status'] = false;

        if(!$this->verifyTokens()){
            shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg"=>"Microsoft account not authorized - email not sent","mode"=>"debug","env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &

            $response['debugOutput'] = "Account Not Authorized";
            return $response;
        }

        $graph = new Graph();
        $graph->setAccessToken($this->tokens["accessToken"]);

        $mailBody = array( "Message" => array(
            "subject" => $subject,
            "body" => array(
                "contentType" => "html",
                "content" => $contentsdata
            ),
            "from" => array(
                "emailAddress" => array(
                    "name" => 'TEST',
                    "address" => $from
                )
            ),
            "toRecipients" => array(
                array(
                    "emailAddress" => array(
                        "address" => $to
                    )
                )
            )
        )
        );

        try {
            $sendEmail = $graph->createRequest('POST', '/me/sendMail')
                ->attachBody($mailBody)
                ->setReturnType(Model\Message::class)
                ->execute();

            $response['debugOutput'] = "";
            $response['debugCode'] = 200;
            $response['debugMessage'] = "OK";
            $response['status'] = true;

        } catch (Exception $e) {

            $response['debugOutput'] = $e;
            $response['debugCode'] = $e->getCode();
            $response['debugMessage'] = $e->getMessage();
            $response['status'] = false;
        }
        return $response;
    }
}