<?php
// All variables are unique - all vars created in this file starts with 'bouncer'

if (!isset($isCalledFromModal)){
    $isCalledFromModal = false; // when the bouncer is called from the modal we need to give him different redirection if the user is not logged in
}

require_once($_SERVER['LOCAL_NL_PATH']."/console/connect/connect.php");
$boncerDatabase = new connect("partners");

if(!isset($pagePermissions)){
    $pagePermissions = array(false,array(0),true,true,true);

    // position 0 - is admin | true/false
    // position 1 - organization types allowed | array(0,1)
    // position 2 - is enabled for free user
    // position 3 - specific user authorizations assigned by the organization admin | array(["type",0])
    // position 4 - user type (standard,sales,manager...)

}else{

    if(!isset($pagePermissions[0])){ $pagePermissions[0] = false;}
    if(!isset($pagePermissions[1])){ $pagePermissions[1] = array(0);}
    if(!isset($pagePermissions[2])){ $pagePermissions[2] = true;}
    if(!isset($pagePermissions[3])){ $pagePermissions[3] = true;}
    if(!isset($pagePermissions[4])){ $pagePermissions[4] = true;}
}

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userLogIn.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/credentials.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userAuthorization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/userPackage.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/userSettings.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/movingSettings.php");
require_once $_SERVER['LOCAL_NL_PATH']."/console/mobile/mobile.php"; // Require the mobile code

$bouncerLogIn = new userLogIn();

$bouncer = array();

function unAuthorizedAccess(){
    http_response_code(403);
    $contents = file_get_contents($_SERVER['LOCAL_NL_URL'].'/console/unAuthorize.php', TRUE);
    exit($contents);
}

// ========== Check if user is logged in ==========
$loggedInUserId = $bouncerLogIn->getUserIdBySession();
$loggedInOrgId = $bouncerLogIn->getOrgIdBySession();
if(!$loggedInUserId || !$loggedInOrgId){
    if ($isCalledFromModal){
        http_response_code(403);
        exit;
    }else{
        header('Location: '.$_SERVER['LOCAL_NL_URL'].'/login/');
        exit;
    }
}
// ========== Check if user is logged in ==========

$bouncer["credentials"]["orgId"] = $loggedInOrgId;
$bouncer["credentials"]["userId"] = $loggedInUserId;

// ========== Get User's Basic Data From Session ==========
$bouncer['isFromMobile'] = (strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobile') || strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android'));

$user = new user($bouncer["credentials"]["userId"]);

$bouncer["userData"] = $user->getData();
$bouncer["userData"]["fullName"] = ucwords($bouncer["userData"]["fullName"]);
// ========== Get User's Basic Data From Session ==========

// ========== Check if user is deleted ==========
// Used to send every client that is deleted to a 'login' page
if($bouncer["userData"]["isDeleted"] == "1"){
    // User is deleted
    $bouncerLogIn->logOut();
    header('Location: '.$_SERVER['LOCAL_NL_URL']);
    exit;
}

// ========== Check if user is deleted ==========

$userPackage = new userPackage($bouncer["credentials"]["userId"]);

$userSettings = new userSettings($bouncer["credentials"]["userId"]);
$bouncer["userSettingsData"] = $userSettings->getUserSettings();
$showWelcomeBackMsg = false;

if(isset($bouncer["userSettingsData"]["localTimeZone"]) && $bouncer["userSettingsData"]["localTimeZone"] != NULL && $bouncer["userSettingsData"]["localTimeZone"] != "" ){
    // Set php local time zone & cookie
    $timeZoneCode = $bouncer["userSettingsData"]["localTimeZone"];

    date_default_timezone_set($timeZoneCode);

    if(!isset($_COOKIE['localTimeZone']) || $_COOKIE['localTimeZone'] != $bouncer["userSettingsData"]["localTimeZone"]){
        setcookie('localTimeZone', $bouncer["userSettingsData"]["localTimeZone"], time() + (30 * 86400 * 30), "/"); // 86400 = 1 day
    }
}

// User Authorization Class
$bouncer["userAuthorization"] = new userAuthorization($bouncer["credentials"]["userId"]);
$bouncer["isUserAnAdmin"] = $bouncer["userAuthorization"]->isUserAnAdmin();

// Check if page requires an admin
if($pagePermissions[0] == true && $bouncer["isUserAnAdmin"] == false){
    // Page requires an admin but user is not an admin
    unAuthorizedAccess();
}

// ========== Check user organization type ==========
$bouncer["organization"] = new organization($bouncer["credentials"]["orgId"]);
if(!$bouncer["organization"]->isFromOrganizationType($pagePermissions[1])){
    // User organization is not allowed for this page
    unAuthorizedAccess();
}

$bouncer["isNotFreeUser"] =  $bouncer["organization"]->isFeatureAvailableForFreeUser();
// ========== Check user organization type ==========

// ========== Check Organization package ==========
if($pagePermissions[2] == true){
 // the feauture shouldnt be disabled for free user

}else {
    // the feauture should be disabled for free user
    if (!$bouncer["isNotFreeUser"]) {

        // User does not have the package for this page
        unAuthorizedAccess();
    }
}
// ========== Check Organization package ==========

// ========== Check user specific authorizations set by an admin ==========
$checkAuths = $bouncer["userAuthorization"]->checkAuthorizations($pagePermissions[3]);
if(!$checkAuths) {
    // User does not have authorization for this page
    unAuthorizedAccess();
}
// ========== Check user specific authorizations set by an admin ==========


// ========== Check user type id ==========
$checkUserTypeId = $bouncer["userAuthorization"]->isUserTypeMatch($pagePermissions[4],$bouncer["userData"]);
if(!$checkUserTypeId && $bouncer["isUserAnAdmin"] != true) {
// User does not have authorization for this page
    unAuthorizedAccess();
}
// ========== Check user type id ==========


// ========== Check if user is in welcome mode ==========
// Used to send every client that is in this mode to a 'welcome' page

    if ($bouncer["userData"]["welcomeMode"] == "1" && $bouncer["isUserAnAdmin"] == true) {
        // User is in welcome mode

        if((!isset($ignoreWelcomeMode))) {
            header('Location: ' . $_SERVER['LOCAL_NL_URL'] . "/console/welcome.php");
            exit;
        }
    }
// ========== Check if user is in welcome mode ==========

$bouncer['organizationData'] = $bouncer["organization"]->getData();

// ========== Check if organization is deleted ==========
if($bouncer['organizationData']["isDeleted"] == "1"  && !isset($ignoreDisabledMode)){
    header('Location: ' . $_SERVER['LOCAL_NL_URL'] . '/unActiveOrganization.php');
    exit;
}
// ========== Check if organization is deleted ==========

// ========== Check if organization subscription is valid ==========
$now = date("Y-m-d H:i:s", strtotime("now"));
$cancellationDate = date("Y-m-d H:i:s", strtotime($bouncer['organizationData']["subscriptionEndDate"]));

if($cancellationDate < $now && $bouncer["isUserAnAdmin"] == "1" && !isset($ignoreRenewMode)) {
    // Organization Plan is not valid (it is cancelled already)
    header('Location: ' . $_SERVER['LOCAL_NL_URL'] . "/console/welcome.php?type=renew");
    exit;
}


if($cancellationDate < $now && $bouncer["isUserAnAdmin"] == "0" && !isset($ignoreDisabledMode)) {
    // Organization Plan is not valid (it is cancelled already)
    // But the user is not an admin - go to 'un active organization' page
    header('Location: ' . $_SERVER['LOCAL_NL_URL'] . "/unActiveOrganization.php");
    exit;
}
// ========== Check if organization subscription is valid ==========

// ========== Check if organization is disabled with a note ==========
if($bouncer['organizationData']["disableNote"] != NULL && !isset($ignoreDisabledMode)){
    header('Location: ' . $_SERVER['LOCAL_NL_URL'] . '/unActiveOrganization.php');
    exit;
}
// ========== Check if organization is disabled with a note ==========

// ========== Get organization settings for type moving ==========
$movingSettings = new movingSettings($bouncer["credentials"]["orgId"]);
$bouncer["movingSettingsData"] = $movingSettings->getData();
// ========== Get organization settings for type moving ==========

$boncerDatabase->close();