<?php

/**
 * Passwords
 *
 * PHP Versions  7
 *
 * @category  Bouncer
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2015 Niv Apo
 * @license   ---
 */

/*
 *
 *	MUST CONTAIN :
 */

class passwords{

	const salt = 'nivapo0022886644';
    private $cipher = "aes-128-gcm";
    private $key;

    // ==================================== START CONSTRUCTOR ======================================
	function __construct() {
	}
	// ==================================== END CONSTRUCTOR ======================================


    // ==================================== START HASH PASSWORD ======================================
    public function hashPass($password = "") {

        $salt = base64_encode(self::salt);
        $passwordEncryptPass = hash('sha512', $salt . $password);

        return $passwordEncryptPass;
    }
    // ==================================== END HASH PASSWORD ======================================

    // ==================================== START HASH CC ======================================
    public function hashCC($plaintext = "") {
    // doc: http://php.net/manual/en/function.openssl-encrypt.php
    // maor: the preview function didnt work on php 7.1 so we use the code of 5.6+ as in the docs

        if (in_array($this->cipher, openssl_get_cipher_methods())) {

            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $this->key, $options=OPENSSL_RAW_DATA, $iv);
            $hmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);
            $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

            return ($ciphertext);
        }
        return false;

    }
    // ==================================== END HASH CC ======================================

    // ==================================== START unHashCC ======================================
    public function unHashCC($ciphertext) {

        $c = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $this->key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $this->key, $as_binary=true);
        if (hash_equals($hmac, $calcmac)){
            return ($original_plaintext);
        }

        return false;

    }
    // ==================================== END unHashCC ======================================

    // ==================================== START isPasswordValid ======================================
    public function isPasswordValid($password = "",$repassword = "") {
        // Is passwords format valid

        $isMatch = false;
        $isLengthValid = false;
        $isOneLowerCaseValid = false;
        $isOneUpperCaseValid = false;

        if($password == $repassword){
            $isMatch = true;
        }else{return false;}
        if(strlen($password) >= 8){
            $isLengthValid = true;
        }else{return false;}
        if(preg_match('/[a-z]/', $password)) {
            $isOneLowerCaseValid = true;
        }else{return false;}
        if(preg_match('/[A-Z]/', $password)) {
            $isOneUpperCaseValid = true;
        }else{return false;}

        if($isMatch && $isLengthValid && $isOneLowerCaseValid && $isOneUpperCaseValid){
	        return true;
        }
        return false;
	}
    // ==================================== END isPasswordValid ======================================

    // ==================================== START isEncryptedMatches ======================================
    public function isEncryptedMatches($password = "",$encryptedPassword = "") {

        $salt = base64_encode(self::salt);
        $passwordEncryptPass = hash('sha512', $salt . $password);

        if($passwordEncryptPass == $encryptedPassword){
            return true;
        }else{
            return false;
        }
    }
    // ==================================== END isEncryptedMatches ======================================
	
	// ==================================== START encryptText ======================================
	public function encryptText($text = ""){
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, self::salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
	}
	// ==================================== END encryptText ======================================
	
	// ==================================== START encryptText ======================================
	public function decryptText($text = ""){
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, self::salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}
	// ==================================== END encryptText ======================================
}