<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/passwords.php");

/**
 * Passwords
 *
 * PHP Versions  5
 *
 * @category  Bouncer
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2018 Niv Apo
 * @license   ---
 */

class userLogIn{
	
	// ==================================== START CONSTRUCTOR ======================================
    public function __construct() {
        if(!isset($_SESSION)) {
            session_start();
        }
	}
	// ==================================== END CONSTRUCTOR ======================================

	private function get_client_ip_env() {

        if (getenv('HTTP_CLIENT_IP'))
            return getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            return getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            return getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            return getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            return getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            return getenv('REMOTE_ADDR');
        else
            return 'UNKNOWN';
    }

	// ==================================== START isUserLoggedIn() ======================================
    public function isUserLoggedIn() {

       if (isset($_SESSION['userIsLoggedIn']) && ($_SESSION['userIsLoggedIn'] == 1) && isset($_SESSION['sessionToken']) && isset($_SESSION['userId']) && $_SESSION['sessionToken'] == session_id()) {

           $errorVar = array("userLogIn","isUserLoggedIn()",5,"Notes",array());

           $binds = array();
           $binds[] = array(':sessionToken', $_SESSION['sessionToken'], PDO::PARAM_STR);
           $binds[] = array(':userId', $_SESSION['userId'], PDO::PARAM_INT);

           $get_session = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.logins WHERE sessionToken=:sessionToken AND userId=:userId AND isValid=1",$binds,$errorVar);
			if(!$get_session){

            }else{
			    $totalSessions = $GLOBALS['connector']->fetch_num_rows($get_session);
			    if($totalSessions == 0){
                    return false;
                }else{
                    return true;
                }
			}
       }
	   return false;
    }
	// ==================================== END isUserLoggedIn() ======================================
	
	// ==================================== START logIn() ======================================
    public function logIn($email,$password,$hideLogIn = false,$didRegisterByGoogle = false,$googleuserid = "") {
        $errorVar = array("userLogIn","logIn()",5,"Notes",array());

		$isUserAuthenticated = $this->didLoginVerifySuccess($email,$password,true,$didRegisterByGoogle,$googleuserid);

		if($this->isUserLoggedIn() == false && $isUserAuthenticated['status'] == true){
            // User does exists

            $ip = $this->get_client_ip_env();
            // ===========================  User Exists And Signed Up - successful login - Connect Him  ========================================
            session_regenerate_id();
            $sessionToken = session_id ();
            if($hideLogIn == true){$hideLogIn = 1;}else{$hideLogIn = 0;}

            $binds = array();
            $binds[] = array(':userId', $isUserAuthenticated['id'], PDO::PARAM_STR);
            $binds[] = array(':sessionToken', $sessionToken, PDO::PARAM_STR);
            $binds[] = array(':hideLogIn', $hideLogIn, PDO::PARAM_BOOL);
            $binds[] = array(':ip', $ip, PDO::PARAM_STR);

            $addData = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.logins (userId,sessionToken,isValid,hideLogIn,ip) values(:userId,:sessionToken,1,:hideLogIn,:ip)",$binds,$errorVar);
            if(!$addData){
                return false;
            }else{
                /* ===== CONNECT ===== */
                    $_SESSION['userIsLoggedIn'] = 1;
                    $_SESSION['sessionToken'] = $sessionToken;
                    $_SESSION['userId'] = $isUserAuthenticated['id'];
                    $_SESSION['orgId'] = $isUserAuthenticated['orgId'];
                /* ===== CONNECT ===== */

                return true;
            }
            // ===========================  User Exists And Signed Up - successful login - Connect Him  ========================================

			return false;
		}
		return false;

    }
	// ==================================== END logIn() ======================================

	// ==================================== START didLoginVerifySuccess() ======================================
    public function didLoginVerifySuccess($email,$password,$returnUserId = false,$didRegisterByGoogle = false,$googleuserid = "") {

        // the $googleuserid should come from google directly.
        // it's like a password. the user must NOT be able to pass whatever id he wants.
        // only google should verify that the user logged in to his google account and pass us the google user id

        $errorVar = array("userLogIn","didLoginVerifySuccess()",5,"Notes",array());
        $data = [];
        $data['id'] = false; // id - user id (id asked to return the user id)
        $data['status'] = false; // status - user exist
        $data['valid'] = false; // valid - user has activated he's account
        $data['isRevoked'] = false;
        $data["hiddenLogin"] = false; // hiddenLogin - if a user logged in with hashed password hide his log in
        $data["didRegisterByGoogle"] = false; // didRegisterByGoogle - if a user registered with google


        $binds = array();
        $binds[] = array(':email', $email, PDO::PARAM_STR);

        // check if user is registered by google
        $getIt = $GLOBALS['connector']->execute("SELECT didRegisterByGoogle FROM networkleads_db.users WHERE email=:email",$binds,$errorVar);
        if ($getIt){
            $returnedData = $GLOBALS['connector']->fetch($getIt);
            if ($returnedData['didRegisterByGoogle'] == 1){
                $data['didRegisterByGoogle'] = true;
            }
        }

        $passwords = new passwords();
		$clean_password = $passwords->hashPass($password);

        $binds = array();
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':userPass', $clean_password, PDO::PARAM_STR);
        $binds[] = array(':masterPass', $clean_password, PDO::PARAM_STR);
        $binds[] = array(':googleUserId', $googleuserid, PDO::PARAM_STR);

        $checkCredentials = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE email=:email AND ((userPass=:userPass OR masterPass=:masterPass) OR (didRegisterByGoogle=1 AND googleUserId=:googleUserId))",$binds,$errorVar);
		if(!$checkCredentials || $GLOBALS['connector']->fetch_num_rows($checkCredentials) != 1){
            return $data;
		}else{

            // CHECK HIDDEN LOGIN
            $binds = array();
            $binds[] = array(':email', $email, PDO::PARAM_STR);
            $binds[] = array(':masterPass', $clean_password, PDO::PARAM_STR);

            if($didRegisterByGoogle == false) {
                $checkHiddenLogIn = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE email=:email AND masterPass=:masterPass", $binds, $errorVar);
                if (!$checkHiddenLogIn) {
                    return false;
                } else {
                    if ($GLOBALS['connector']->fetch_num_rows($checkHiddenLogIn) == 1) {
                        $data["hiddenLogin"] = true;
                    }
                }
            }
            // CHECK HIDDEN LOGIN

            $binds = array();
            $binds[] = array(':email', $email, PDO::PARAM_STR);
            $binds[] = array(':userPass', $clean_password, PDO::PARAM_STR);
            $binds[] = array(':masterPass', $clean_password, PDO::PARAM_STR);
            $binds[] = array(':googleUserId', $googleuserid, PDO::PARAM_STR);

		    $data['status'] = true;
            $checkValid = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE email=:email AND ((userPass=:userPass OR masterPass=:masterPass) OR (didRegisterByGoogle=1 AND googleUserId=:googleUserId)) AND isActive=1",$binds,$errorVar);
            if($GLOBALS['connector']->fetch_num_rows($checkValid) != 1){
                $data['valid'] = false;
                return $data;
            }else{
                $data['valid'] = true;
                $checkRevoke = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE email=:email AND ((userPass=:userPass OR masterPass=:masterPass) OR (didRegisterByGoogle=1 AND googleUserId=:googleUserId)) AND isDeleted=0",$binds,$errorVar);
                if($GLOBALS['connector']->fetch_num_rows($checkRevoke) != 1){
                    $data['isRevoked'] = true;
                    return $data;
                }else {
                    if ($returnUserId) {
                        $checkCredentials = $GLOBALS['connector']->execute("SELECT id,organizationId FROM networkleads_db.users WHERE email=:email AND ((userPass=:userPass OR masterPass=:masterPass) OR (didRegisterByGoogle=1 AND googleUserId=:googleUserId)) AND isDeleted=0", $binds, $errorVar);

                        $userData = $GLOBALS['connector']->fetch($checkCredentials);
                        $data['id'] = $userData["id"];
                        $data['orgId'] = $userData["organizationId"];
                        return $data;
                    } else {
                        return $data;
                    }
                }
            }
		}
		return false;
    }
	// ==================================== END didLoginVerifySuccess() ======================================

	// ==================================== START logOut() ======================================
    public function logOut() {
		if(isset($_SESSION)){
            $errorVar = array("userLogIn","logOut()",5,"Notes",array());

            $binds = array();
            $binds[] = array(':userId', $this->getUserIdBySession(), PDO::PARAM_INT);
            $binds[] = array(':sessionToken', session_id(), PDO::PARAM_STR);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.logins SET isValid=0,logoutTime=CURRENT_TIMESTAMP WHERE userId=:userId AND sessionToken=:sessionToken",$binds,$errorVar);
            if(!$setIt){
				return false;
			}else{
				$_SESSION = array();
                session_destroy();
                return true;
            }
		}
		return false;
    }
	// ==================================== END logOut() ======================================

    // ==================================== START getUserIdBySession() ======================================
    public function getUserIdBySession() {
        $errorVar = array("userLogIn","getUserIdBySession()",5,"Notes",array());

        if($this->isUserLoggedIn()){
            $binds = array();
            $binds[] = array(':sessionToken', $_SESSION['sessionToken'], PDO::PARAM_STR);
            $binds[] = array(':userId', $_SESSION['userId'], PDO::PARAM_INT);

            $get_session_user = $GLOBALS['connector']->execute("SELECT id,userId FROM networkleads_db.logins WHERE sessionToken=:sessionToken AND userId=:userId AND isValid=1 ORDER BY id DESC LIMIT 1",$binds,$errorVar);
            if(!$get_session_user){

            }else{
                $userIdBySession = $GLOBALS['connector']->fetch($get_session_user);
                if($_SESSION['userId'] == $userIdBySession["userId"]){
                    return $_SESSION['userId'];
                }
            }
        }

        return false;
    }
    // ==================================== END getUserIdBySession() ======================================

    // ==================================== START getOrgIdBySession() ======================================
    public function getOrgIdBySession() {
        if (isset($_SESSION['orgId'])){
            return $_SESSION['orgId'];
        }
        //fallback
        $this->logOut();
        return false;
    }
    // ==================================== END getOrgIdBySession() ======================================

    // ==================================== START saveFailedAttemptToLog() ======================================
    public function saveFailedAttemptToLog($email = null,$password = null) {

        $errorVar = array("logInLog","saveFailedAttemptToLog()",5,"Notes",array());
        
        $passwords = new passwords();
        $clean_password = $passwords->hashPass($password);

        $binds = array();
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':password', $clean_password, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.failedAttemptLogins (enteredEmail,enteredPassword) values(:email,:password)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END saveFailedAttemptToLog() ======================================

}