<?php

/**
 * Passwords
 *
 * PHP Versions  7
 *
 * @category  Bouncer
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 22.12.2019 Niv Apo
 * @license   ---
 */


class credentials{

    protected $ORGID = NULL;
    protected $USERID = NULL;

    // ==================================== START CONSTRUCTOR ======================================
	function __construct() {

	    if(isset($_SESSION)){
            $this->ORGID = $_SESSION["orgId"];
            $this->USERID = $_SESSION["userId"];
        }
	}
	// ==================================== END CONSTRUCTOR ======================================
}