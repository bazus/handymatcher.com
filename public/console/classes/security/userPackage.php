<?php
/**
 * Permissions Security Functions
 *
 * PHP Versions 7
 *
 * @category  Security
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2018 Niv Apo
 * @license   ---
 * @Date Create   27.6.18
 */

/**
 * Permissions Security Functions class
 *
 * This class provides methods to :
 *  - String Security functions
 *
 * @category  Security
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2018 Niv Apo
 * @license   ---
 *
 *
 * ---------- Functions Contained ----------
 *
 *	__construct($userId)
 *
 * ---------- Functions Contained ----------
 *
 *
 */

class userPackage{

    private $userId;
    private $userPackageLevel;

    // ==================================== START CONSTRUCTOR ======================================
    function __construct($userId = "") {
        $this->userId = $userId;


    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getAccess() ======================================
    public function getAccess($levelsAllowd = array()){
        if(in_array($this->userPackageLevel,$levelsAllowd)){
            return true;
        }
        return false;
    }
    // ==================================== END getAccess() ======================================
}