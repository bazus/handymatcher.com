<?php

/**
 * Passwords
 *
 * PHP Versions  7
 *
 * @category  Bouncer
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (14.8.18) 2018 Niv Apo
 * @license   ---
 */

class userAuthorization{


    private $userId;
    private $userData = NULL;

	// ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = "") {
        $this->userId = $userId;
	}
	// ==================================== END CONSTRUCTOR ======================================

    // ==================================== START checkAuthorization() ======================================
    public function checkAuthorizations($auths = array()){
        if($auths === true){return true;}

        $isAuthorized = false;
        foreach($auths as $auth){

            $check = $this->checkAuthorization($auth[0],$auth[1]);
            if($check == true){
                $isAuthorized = true;
            }
        }
        return $isAuthorized;
    }
    // ==================================== END checkAuthorization() ======================================


    // ==================================== START checkAuthorization() ======================================
    public function checkAuthorization($type = "",$permission = "") {

        // $permission types :
        // 1 - view
        // 2 - edit/update
        // 3 - create
        // 4 - delete (setting isDeleted to false!)

        $errorVar = array("userAuthorization","checkAuthorization()",5,"Notes",array(),false);

        // ===== CHECK INPUTS =====
        if($permission != "1" && $permission != "2" && $permission != "3" && $permission != "4"){return false;}

        $types = array();
        $types["organization"] = "organization";
        $types["users"] = "users";
        $types["departments"] = "departments";
        $types["filemanager"] = "filemanager";
        $types["calendar"] = "calendar";
        $types["mailcenter"] = "mailcenter";
        $types["financials"] = "financials";

        if(!array_key_exists($type,$types)){return false;}

        $typeColumn = $types[$type];
        // ===== CHECK INPUTS =====

        // ===== CHECK IF ADMIN =====
            if($this->isUserAnAdmin()){return true;}
        // ===== CHECK IF ADMIN =====

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

           $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.usersAuthorizations WHERE userId=:userId AND ".$typeColumn.">=".$permission,$binds,$errorVar);
			if(!$getIt){
			    return false;
			}else{

				if($GLOBALS['connector']->fetch_num_rows($getIt) == 1){
                    return true;
				}
			}

	   return false;
    }
	// ==================================== END checkAuthorization() ======================================

    // ==================================== START isUserAnAdmin() ======================================
    public function isUserAnAdmin(){
        $errorVar = array("userAuthorization","isUserAnAdmin()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE id=:userId AND isAdmin=1",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) == 1){
                // The user is an admin
                return true;
            }
        }
        return false;
    }
    // ==================================== END isUserAnAdmin() ======================================

    // ==================================== START isUserTypeMatch() ======================================
    public function isUserTypeMatch($userTypesAllowed = array(),$userData = NULL){
        if($userTypesAllowed === true){return true;}

        if($userData !== NULL){
            $this->userData = $userData;
        }

        $userTypeId = NULL;

        if($this->userData != NULL){
            $userTypeId = $this->userData["userTypeId"];
        }else{
            $errorVar = array("userAuthorization","isUserTypeMatch()",5,"Notes",array(),false);

            $binds = array();
            $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT userTypeId FROM networkleads_db.users WHERE id=:userId",$binds,$errorVar);
            if(!$getIt){
                return false;
            }else{
                $r = $GLOBALS['connector']->fetch($getIt);
                $userTypeId = $r["userTypeId"];
            }
        }

        if($userTypeId == NULL){return false;}
        if(in_array($userTypeId,$userTypesAllowed)){
            return true;
        }
        return false;
    }
    // ==================================== END isUserTypeMatch() ======================================

    // ==================================== START getUserAuthorizations() ======================================
    public function getUserAuthorizations(){

        $authorizations = array();

        $errorVar = array("userAuthorization","getUserAuthorizations()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.usersAuthorizations WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) == 0) {
                $getIt = $GLOBALS['connector']->execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='networkleads_db' AND TABLE_NAME='usersAuthorizations'",NULL,$errorVar);
                if(!$getIt){
                    return false;
                }else{
                    while($r = $GLOBALS['connector']->fetch($getIt)){
                        if ($r["COLUMN_NAME"] == "id" || $r["COLUMN_NAME"] == "userId") {
                            continue;
                        }

                        $authorizationDetails = $this->getAuthorizationDetails($r["COLUMN_NAME"]);

                        $authorization = array();
                        $authorization["name"] = $r["COLUMN_NAME"];
                        $authorization["value"] = 0;
                        $authorization["title"] = $authorizationDetails["title"];
                        $authorization["description"] = $authorizationDetails["description"];

                        $levels = array();
                        $levels["0"] = "No Access";
                        $levels["1"] = "View";
                        $levels["2"] = "View/Edit";
                        $levels["3"] = "View/ Edit/Create";
                        $levels["4"] = "View/Edit/Create/Delete";

                        $authorization["levels"] = $levels;

                        $authorizations[] = $authorization;
                    }
                    return $authorizations;
                }
            }else {
                $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.usersAuthorizations WHERE userId=:userId", $binds, $errorVar);

                $r = $GLOBALS['connector']->fetch($getIt);
                foreach ($r as $k => $v) {
                    if ($k == "id" || $k == "userId") {
                        continue;
                    }

                    $authorizationDetails = $this->getAuthorizationDetails($k);

                    $authorization = array();
                    $authorization["name"] = $k;
                    $authorization["value"] = $v;
                    $authorization["title"] = $authorizationDetails["title"];
                    $authorization["description"] = $authorizationDetails["description"];

                    $levels = array();
                    $levels["0"] = "No Access";
                    $levels["1"] = "View";
                    $levels["2"] = "View/Edit";
                    $levels["3"] = "View/ Edit/Create";
                    $levels["4"] = "View/Edit/Create/Delete";

                    $authorization["levels"] = $levels;

                    $authorizations[] = $authorization;
                }
                return $authorizations;
            }
        }
        return false;
    }
    // ==================================== END getUserAuthorizations() ======================================

    // ==================================== START getUserAuthorizations() ======================================
    public function getAuthorizations(){

        $authorizations = array();

        $errorVar = array("userAuthorization","getUserAuthorizations()",5,"Notes",array(),false);

        $getIt = $GLOBALS['connector']->execute("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='networkleads_db' AND TABLE_NAME='usersAuthorizations'",NULL,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while($r = $GLOBALS['connector']->fetch($getIt)){
                if ($r["COLUMN_NAME"] == "id" || $r["COLUMN_NAME"] == "userId") {
                    continue;
                }

                $authorizationDetails = $this->getAuthorizationDetails($r["COLUMN_NAME"]);

                $authorization = array();
                $authorization["name"] = $r["COLUMN_NAME"];
                $authorization["value"] = 0;
                $authorization["title"] = $authorizationDetails["title"];
                $authorization["description"] = $authorizationDetails["description"];

                $levels = array();
                $levels["0"] = "No Access";
                $levels["1"] = "View";
                $levels["2"] = "View/Edit";
                $levels["3"] = "View/ Edit/Create";
                $levels["4"] = "View/Edit/Create/Delete";

                $authorization["levels"] = $levels;

                $authorizations[] = $authorization;
            }
            return $authorizations;
        }

        return false;
    }
    // ==================================== END getUserAuthorizations() ======================================

    // ==================================== START getAuthorizationDetails() ======================================
    public function getAuthorizationDetails($authorizationName = ""){
        $details = array();

        switch($authorizationName){
            case "organization":
                $details["title"] = "Organization";
                $details["description"] = "Organization management. The option to update logo, information and description of an organization.";
                break;
            case "users":
                $details["title"] = "Users";
                $details["description"] = "Managing the users section of an organization.";
                break;
            case "departments":
                $details["title"] = "Departments";
                $details["description"] = "Managing the departments section of an organization.";
                break;
            case "filemanager":
                $details["title"] = "File Manager";
                $details["description"] = "Access to the file system management. You can upload and download files directly to your organization account.";
                break;
            case "calendar":
                $details["title"] = "Calendar";
                $details["description"] = "Full Calendar Feature.";
                break;
            case "mailcenter":
                $details["title"] = "Marketing Center";
                $details["description"] = "This will allow/deny users access to the marketing section of the software.";
                break;
            case "financials":
                $details["title"] = "Financial Data";
                $details["description"] = "Access to full financial data of the organization.";
                break;
            default:
                $details["title"] = "";
                $details["description"] = "";
        }

        return $details;
    }
    // ==================================== START getAuthorizationDetails() ======================================

    // ==================================== START isAuthorizationNameExists() ======================================
    public function isAuthorizationNameExists($authorizationName = ""){
        $errorVar = array("userAuthorization","isAuthorizationNameExists()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':COLUMN_NAME', $authorizationName, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'networkleads_db' AND TABLE_NAME = 'usersAuthorizations' AND COLUMN_NAME=:COLUMN_NAME",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) == 1){
                // The authorization exists
                return true;
            }
        }
        return false;
    }
    // ==================================== END isAuthorizationNameExists() ======================================

    // ==================================== START updateUserAuthorization() ======================================
    public function updateUserAuthorization($authorizationName = "",$authorizationValue = ""){
        if (!$this->isAuthorizationNameExists($authorizationName)) {
            return false;
        }

        $errorVar = array("userAuthorization","updateUserAuthorization()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.usersAuthorizations WHERE userId=:userId", $binds, $errorVar);
        if(!$getIt){
            return false;
        }else {

            if($GLOBALS['connector']->fetch_num_rows($getIt) == 0){
                // The authorization does not exists

                $binds = array();
                $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
                if ($authorizationValue == 0) {
                    $binds[] = array(':authorizationValue', null, PDO::PARAM_INT);
                } else {
                    $binds[] = array(':authorizationValue', $authorizationValue, PDO::PARAM_INT);
                }
                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.usersAuthorizations(userId,$authorizationName) VALUES(:userId,:authorizationValue)", $binds, $errorVar);
                if(!$setIt){
                    return false;
                }else{
                    return true;
                }
            }else {
                $binds = array();
                $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
                if ($authorizationValue == 0) {
                    $binds[] = array(':authorizationValue', null, PDO::PARAM_INT);
                } else {
                    $binds[] = array(':authorizationValue', $authorizationValue, PDO::PARAM_INT);
                }


                $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.usersAuthorizations SET " . $authorizationName . "=:authorizationValue WHERE userId=:userId", $binds, $errorVar);
                if (!$setIt) {
                    return false;
                } else {
                    return true;
                }
                return false;
            }
        }
    }
    // ==================================== END updateUserAuthorization() ======================================
}