<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 19/10/2018
 * Time: 0:10
 */

class checkList
{
    private $userId;

    function __construct($id){$this->userId = $id;}

    public function getData(){

        $errorVar = array("checkList Class","getData()",4,"Notes",array());

        $lists = [];

        $binds = [];
        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT id,title FROM networkleads_db.checkList WHERE userId=:userId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while ($list = $GLOBALS['connector']->fetch($getIt)){
                $lists[] = $list;
            }
        }
        return $lists;

    }

    public function setData($title){

        $errorVar = array("checkList Class","setData()",4,"Notes",array());

        $binds = [];
        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":title",$title,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.checkList(title,userId) VALUES(:title,:userId)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;

    }

    public function deleteId($id){

        $errorVar = array("checkList Class","deleteId()",4,"Notes",array());

        $binds = [];
        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":id",$id,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.checkList SET isDeleted=1 WHERE id=:id AND userId=:userId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;

    }

}