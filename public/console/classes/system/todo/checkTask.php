<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 19/10/2018
 * Time: 0:10
 */

class checkTask
{
    private $userId;

    function __construct($id){$this->userId = $id;}

    public function getData($listId){

        $errorVar = array("checkTask Class","getData()",4,"Notes",array());

        $lists = [];

        $binds = [];

        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":listId",$listId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.checkTask WHERE userId=:userId AND listId=:listId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while ($list = $GLOBALS['connector']->fetch($getIt)){
                $lists[] = $list;
            }
        }
        return $lists;

    }
    public function setData($title,$listId){

        $errorVar = array("checkTask Class","setData()",4,"Notes",array());

        $binds = [];

        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":title",$title,PDO::PARAM_INT];
        $binds[] = [":listId",$listId,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.checkTask(title,userId,listId) VALUES(:title,:userId,:listId)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;

    }
    public function deleteId($id){

        $errorVar = array("checkTask Class","deleteId()",4,"Notes",array());

        $binds = [];

        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":id",$id,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.checkTask SET isDeleted=1 WHERE id=:id AND userId=:userId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;

    }

    public function activeId($id,$type){

        $errorVar = array("checkTask Class","activeId()",4,"Notes",array());

        $binds = [];

        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":id",$id,PDO::PARAM_INT];
        $binds[] = [":isActive",$type,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.checkTask SET isActive=:isActive WHERE id=:id AND userId=:userId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;

    }

    public function unactiveId($id){

        $errorVar = array("checkTask Class","unactiveId()",4,"Notes",array());

        $binds = [];

        $binds[] = [":userId",$this->userId,PDO::PARAM_INT];
        $binds[] = [":id",$id,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.checkTask SET isActive=0 WHERE id=:id AND userId=:userId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;

    }

}