<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 30/07/2018
 * Time: 18:44
 */

class search
{

    public function searchResult($orgID,$search,$userID,$admin){
        $data = [];
        if ($search) {
            if (is_numeric($search)) {
                $this->searchByNumber($orgID, $search, $userID, $data, $admin);
            } else if (filter_var($search, FILTER_VALIDATE_EMAIL)) {
                $this->searchByEmail($orgID, $search, $userID, $data, $admin);
            } else if (strpos($search, " ") != false || is_string($search)) {
                $this->searchByString($orgID, $search, $userID, $data, $admin);
            }
        }
        return $data;

    }

    public function searchByNumber($orgID,$search,$userID,&$data,$admin){
        $errorVar = array("Search","searchByNumber()",3,"Notes",array(),false);

        $users = [];
        $leads = [];
        $leadsProvider = [];

        // =================== Users START ===================
        $binds = [ [":orgID",$orgID,PDO::PARAM_INT] , [":search","%".$search."%",PDO::PARAM_STR] , [":userID",$userID,PDO::PARAM_INT] ];

        $getUsers = $GLOBALS['connector']->execute("SELECT id,fullName,email,phoneNumber,phoneCountryCodeState,jobTitle FROM networkleads_db.users WHERE organizationId=:orgID AND isDeleted=0 AND (phoneNumber LIKE :search AND id!=:userID) ORDER BY id",$binds,$errorVar);

        if(!$getUsers){
            return false;
        }else{
            while($user = $GLOBALS['connector']->fetch($getUsers)){
                $users[] = $user;
            }
            $data['users'] = $users;
        }
        // =================== Users END ===================

        // =================== Leads START ===================
        $q = $search;
        if ($admin) {
            $binds = [];
            $binds[] = [":orgID", $orgID, PDO::PARAM_INT];
            $binds[] = [":leadId", $search, PDO::PARAM_INT];
            $binds[] = [":search", "%" . $search . "%", PDO::PARAM_STR];

            $getLeads = $GLOBALS['connector']->execute("SELECT id,jobNumber,firstname,lastname,email,phone,phone2 FROM networkleads_db.leads WHERE orgId=:orgID AND isDeleted=0 AND (id=:leadId OR phone LIKE :search OR phone2 LIKE :search OR only_digits(phone)=only_digits(:search) OR only_digits(phone2)=only_digits(:search)) ORDER BY id", $binds, $errorVar);
        }else{
            $binds = [];
            $binds[] = [":orgID", $orgID, PDO::PARAM_INT];
            $binds[] = [":leadId", $search, PDO::PARAM_INT];
            $binds[] = [":search", "%" . $search . "%", PDO::PARAM_STR];
            $binds[] = [":userId",$userID,PDO::PARAM_INT];

            $getLeads = $GLOBALS['connector']->execute("SELECT id,jobNumber,firstname,lastname,email,phone,phone2 FROM networkleads_db.leads WHERE orgId=:orgID AND userIdAssigned=:userId AND isDeleted=0 AND (id=:leadId OR phone LIKE :search OR phone2 LIKE :search OR only_digits(phone)=only_digits(:search) OR only_digits(phone2)=only_digits(:search)) ORDER BY id", $binds, $errorVar);
        }

        if(!$getLeads){
            return false;
        }else{
            while($lead = $GLOBALS['connector']->fetch($getLeads)){
                $leads[] = $lead;
            }
            $data['leads'] = $leads;
        }
        // =================== Leads END ===================

        // =================== Leads Providers START ===================
        $binds = [ [":orgID",$orgID,PDO::PARAM_INT] , [":search","%".$search."%",PDO::PARAM_STR] ];

        $getLeadsProviders = $GLOBALS['connector']->execute("SELECT id,providerName,providerEmail,providerWebsite,providerAddress,providerPhone,providerFax,providerContactName,uniqueKey FROM networkleads_db.leadProviders WHERE orgId=:orgID AND isDeleted=0 AND (providerPhone LIKE :search OR providerFax LIKE :search) ORDER BY id",$binds,$errorVar);

        if(!$getLeadsProviders){
            return false;
        }else{
            while($leadsProviders = $GLOBALS['connector']->fetch($getLeadsProviders)){
                $leadsProvider[] = $leadsProviders;
            }
            $data['leadsProvider'] = $leadsProvider;
        }
        // =================== Leads Providers END ===================
    }

    public function searchByEmail($orgID,$search,$userID,&$data,$admin){
        $errorVar = array("Search","searchByEmail()",3,"Notes",array(),false);

        $users = [];
        $leads = [];
        $leadsProvider = [];

        // =================== Users START ===================
        $binds = [ [":orgID",$orgID,PDO::PARAM_INT] ,[":search","%".$search."%",PDO::PARAM_STR] , [":userID",$userID,PDO::PARAM_INT] ];

        $getUsers = $GLOBALS['connector']->execute("SELECT id,fullName,email,phoneNumber,phoneCountryCodeState,jobTitle FROM networkleads_db.users WHERE organizationId=:orgID AND isDeleted=0 AND (email LIKE :search AND id!=:userID) ORDER BY id",$binds,$errorVar);

        if(!$getUsers){
            return false;
        }else{
            while($user = $GLOBALS['connector']->fetch($getUsers)){
                $users[] = $user;
            }
            $data['users'] = $users;
        }
        // =================== Users END ===================

        // =================== Leads START ===================
        if ($admin) {
            $binds = [[":orgID", $orgID, PDO::PARAM_INT], [":search", "%" . $search . "%", PDO::PARAM_STR]];

            $getLeads = $GLOBALS['connector']->execute("SELECT id,jobNumber,firstname,lastname,email,phone,phone2 FROM networkleads_db.leads WHERE orgId=:orgID AND isDeleted=0 AND (email LIKE :search) ORDER BY id", $binds, $errorVar);
        }else{
            $binds = [[":orgID", $orgID, PDO::PARAM_INT], [":search", "%" . $search . "%", PDO::PARAM_STR], [":userId",$userID,PDO::PARAM_INT] ];

            $getLeads = $GLOBALS['connector']->execute("SELECT id,jobNumber,firstname,lastname,email,phone,phone2 FROM networkleads_db.leads WHERE orgId=:orgID AND userIdAssigned=:userId AND isDeleted=0 AND (email LIKE :search) ORDER BY id", $binds, $errorVar);
        }

        if(!$getLeads){
            return false;
        }else{
            while($lead = $GLOBALS['connector']->fetch($getLeads)){
                $leads[] = $lead;
            }
            $data['leads'] = $leads;
        }
        // =================== Leads END ===================

        // =================== Leads Providers START ===================
        $binds = [ [":orgID",$orgID,PDO::PARAM_INT] , [":search","%".$search."%",PDO::PARAM_STR] ];

        $getLeadsProviders = $GLOBALS['connector']->execute("SELECT id,providerName,providerEmail,providerWebsite,providerAddress,providerPhone,providerFax,providerContactName,uniqueKey FROM networkleads_db.leadProviders WHERE orgId=:orgID AND isDeleted=0 AND (providerEmail LIKE :search) ORDER BY id",$binds,$errorVar);

        if(!$getLeadsProviders){
            return false;
        }else{
            while($leadsProviders = $GLOBALS['connector']->fetch($getLeadsProviders)){
                $leadsProvider[] = $leadsProviders;
            }
            $data['leadsProvider'] = $leadsProvider;
        }
        // =================== Leads Providers END ===================
    }

    public function searchByString($orgID,$search,$userID,&$data,$admin){

        $errorVar = array("Search","searchByEmail()",3,"Notes",array(),false);

        $users = [];
        $leads = [];
        $leadsProvider = [];

        // =================== Users START ===================
        $binds = [ [":orgID",$orgID,PDO::PARAM_INT] , [":search","%".$search."%",PDO::PARAM_STR] , [":userID",$userID,PDO::PARAM_INT] ];

        $getUsers = $GLOBALS['connector']->execute("SELECT id,fullName,email,phoneNumber,phoneCountryCodeState,jobTitle FROM networkleads_db.users WHERE organizationId=:orgID AND isDeleted=0 AND (fullName LIKE :search OR email LIKE :search AND id!=:userID) ORDER BY id",$binds,$errorVar);

        if(!$getUsers){
            return false;
        }else{
            while($user = $GLOBALS['connector']->fetch($getUsers)){
                $users[] = $user;
            }
            $data['users'] = $users;
        }
        // =================== Users END ===================

        // =================== Leads START ===================
        $q = $search;
        if ($admin) {
            $binds = [];
            $binds[] = [":orgID", $orgID, PDO::PARAM_INT];
            $binds[] = [":search", "%" . $q . "%", PDO::PARAM_STR];

            $getLeads = $GLOBALS['connector']->execute("SELECT id,jobNumber,firstname,lastname,email,phone,phone2,orgId FROM networkleads_db.leads WHERE orgId=:orgID AND isDeleted=0 AND (email LIKE :search OR (CONCAT(firstname, ' ', lastname) LIKE :search) OR firstname LIKE :search OR lastname LIKE :search OR id LIKE :search OR jobNumber LIKE :search OR only_digits(phone)=only_digits(:search)) ORDER BY id", $binds, $errorVar);
        }else{
            $binds = [];
            $binds[] = [":orgID", $orgID, PDO::PARAM_INT];
            $binds[] = [":search", "%" . $q . "%", PDO::PARAM_STR];
            $binds[] = [":userId",$userID,PDO::PARAM_INT];

            $getLeads = $GLOBALS['connector']->execute("SELECT id,jobNumber,firstname,lastname,email,phone,phone2,orgId FROM networkleads_db.leads WHERE orgId=:orgID AND isDeleted=0 AND (email LIKE :search OR (CONCAT(firstname, ' ', lastname)=:search) OR firstname LIKE :search OR lastname LIKE :search OR id LIKE :search OR jobNumber LIKE :search) AND userIdAssigned=:userId ORDER BY id", $binds, $errorVar);
        }
        if(!$getLeads){
            return false;
        }else{
            while($lead = $GLOBALS['connector']->fetch($getLeads)){
                $leads[] = $lead;
            }
            $data['leads'] = $leads;
        }
        // =================== Leads END ===================

        // =================== Leads Providers START ===================
        $binds = [ [":orgID",$orgID,PDO::PARAM_INT] , [":search","%".$search."%",PDO::PARAM_STR] ];

        $getLeadsProviders = $GLOBALS['connector']->execute("SELECT id,providerName,providerEmail,providerWebsite,providerAddress,providerPhone,providerFax,providerContactName,uniqueKey FROM networkleads_db.leadProviders WHERE orgId=:orgID AND isDeleted=0 AND (providerEmail LIKE :search OR providerName LIKE :search OR providerWebsite LIKE :search OR providerAddress LIKE :search OR providerContactName LIKE :search OR uniqueKey LIKE :search) ORDER BY id",$binds,$errorVar);

        if(!$getLeadsProviders){
            return false;
        }else{
            while($leadsProviders = $GLOBALS['connector']->fetch($getLeadsProviders)){
                $leadsProvider[] = $leadsProviders;
            }
            $data['leadsProvider'] = $leadsProvider;
        }
        // =================== Leads Providers END ===================
    }

}