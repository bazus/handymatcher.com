<?php
/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (3.10.18) 2018 Niv Apo
 * @license   ---
 */
class twilioPhoneNotifications
{
    private $client;

    public function __construct($client) {
        $this->client = $client;
    }

    public function sendSMS($fromNumber = "",$toNumber = "",$msg = "",$is2FA = false){

        if($fromNumber == ""){
            //$fromNumber = "+18338171835"; // OLD - Our defualt number
            $fromNumber = "+14242381648"; // Our defualt number
        }

        // fix to number
        $toNumber = trim($toNumber);

        if(substr( $toNumber, 0, 1 ) !== "+"){
            $toNumber = "+1".$toNumber;
        }

        if($toNumber == "+" || $toNumber == "" || preg_match("/[a-z]/i", $toNumber)){return false;}

        if($is2FA == true) {
            $sendingArray = array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => "NetworkLead",
                // the body of the text message you'd like to send
                'body' => $msg,
                'messagingServiceSid' => 'MGe98d046cc3742f8dad532f928edccd1c'
            );
        }else{
            $sendingArray = array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $fromNumber,
                // the body of the text message you'd like to send
                'body' => $msg
            );
        }

        // Use the client to do fun stuff like send text messages!
        $resp = $this->client->messages->create(
        // the number you'd like to send the message to
            $toNumber,$sendingArray
        );


        return $resp;
    }

}