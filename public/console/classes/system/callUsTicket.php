<?php


class callUsTicket
{
    private $userId;

    function __construct($id){   $this->userId = $id;    }

    public function sendTicket($phone,$reason){

        $errorVar = array("callUsTicket","sendTicket()",5,"Notes",array());

        $binds = [];

        $binds[] = [':userId',$this->userId,PDO::PARAM_INT];
        $binds[] = [':phone',$phone,PDO::PARAM_INT];
        $binds[] = [':reason',$reason,PDO::PARAM_STR];

        $addIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.callUsTicket(userId,sentReason,phoneNumber) VALUES(:userId,:reason,:phone)",$binds,$errorVar);
        if (!$addIt){
            return false;
        }else{
            return true;
        }
    }
}