<?php

class timeController
{

    const SECONDS = 1;
    const MINUTE = 60 * self::SECONDS;
    const HOUR = 60 * self::MINUTE;
    const DAY = 24 * self::HOUR;
    const MONTH = 30 * self::DAY;

    public function __construct(){


    }

/*
    public function convertTimeZonesFromDB($timeText = NULL,$returnFormat = NULL) {
        if($timeText === NULL){return false;}

        $fromTimeZone = "America/New_York"; // The timezone of the RDS database


        $d = new DateTime($timeText, new DateTimeZone($fromTimeZone));
        $fromTimestamp = $d->format('U');

        $date = new DateTime();
        $date->setTimestamp($fromTimestamp);
        $date->setTimezone(new DateTimeZone($this->localTimeZone));

        if($returnFormat === NULL){
            return $date->getTimestamp();
        }else{
            return $date->format($returnFormat);
        }

        return false;
    }

    public function convertTimeZonesToDB($timeText = NULL,$returnFormat = NULL) {
        if($timeText === NULL){return false;}

        $toTimeZone = "America/New_York"; // The timezone of the RDS database

        $d = new DateTime($timeText, new DateTimeZone($this->localTimeZone));
        $fromTimestamp = $d->format('U');

        $date = new DateTime();
        $date->setTimestamp($fromTimestamp);
        $date->setTimezone(new DateTimeZone($toTimeZone));

        if($returnFormat === NULL){
            return $date->getTimestamp();
        }else{
            return $date->format($returnFormat);
        }

        return false;
    }
*/

    public function convertv2($pastDate = NULL,$defaultFormat = NULL,$maxTimeToFormat = NULL){
        // Accepts the following format "Y-m-d H:i:s"

        if($pastDate === NULL){return false;}

        $time = DateTime::createFromFormat("Y-m-d H:i:s", $pastDate);
        $now = new DateTime;
        $diff = $time->diff($now);

        $totalSecondsDiff = strtotime("now")-strtotime($pastDate);

        if($maxTimeToFormat != NULL && is_array($maxTimeToFormat) && key_exists("seconds",$maxTimeToFormat) && key_exists("format",$maxTimeToFormat)){
            if($totalSecondsDiff >= $maxTimeToFormat["seconds"]){
                return date($maxTimeToFormat["format"],strtotime($pastDate));
            }
        }

        if ($totalSecondsDiff < 1 * self::MINUTE) {
            return $diff->s < 30 ? "now" : $diff->s." seconds ago";
        }
        if ($totalSecondsDiff < 2 * self::MINUTE) {
            return "a minute ago";
        }
        if ($totalSecondsDiff < 60 * self::MINUTE) {
            return $diff->i." minutes ago";
        }
        if ($totalSecondsDiff < 90 * self::MINUTE) {
            return "an hour ago";
        }
        if ($totalSecondsDiff < 24 * self::HOUR) {
            return $diff->h." hours ago";
        }
        if ($totalSecondsDiff < 48 * self::HOUR) {
            return "yesterday";
        }

        if ($totalSecondsDiff < 60 * self::DAY) {
            return ($diff->m*30)+($diff->d)." days ago";
        }

        // If more than 30 days ago - return the original date time
        if($defaultFormat === NULL){
            return $pastDate;
        }else{
            return date($defaultFormat,strtotime($pastDate));
        }
    }

    public function convert($tm,$rcs = 0) {
        $tm = strtotime($tm);
        $cur_tm = strtotime("NOW"); $dif = $cur_tm-$tm;
        $pds = array('Second','Minute','Hour','Day','Week','Month','Year','Decade');
        $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);

        for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);
        $no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
        if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);

        // Fall Back if number is -
        if (substr($x, 0, 1) === '-') { $x="1 Seconds"; }

        return trim($x)." ago";
    }

}