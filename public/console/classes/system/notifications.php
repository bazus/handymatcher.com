<?php

function sortByDate($a, $b) {
    return strtotime($b['created_at']) - strtotime($a['created_at']);
}

class notifications
{

    private $userId;
    private $userOrg;
    private $isAdmin;

    public function __construct($userId,$orgId,$isAdmin)
    {
        $this->userId = $userId;
        $this->userOrg = $orgId;
        $this->isAdmin = $isAdmin;
    }


    public function getTotalPendingNotifications(){

        $errorVar = array("Notifications","getTotalPendingNotifications()",3,"Notes",array());

        $totalNotification = 0;

        // check for user notifications
        $userData = "user:".$this->userId;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $userQuery = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.notification WHERE to_user_data=:userData AND is_read=0 ORDER BY id DESC",$binds,$errorVar);

        if(!$userQuery){}else{

            if($userNof = $GLOBALS['connector']->fetch_num_rows($userQuery)){

                $totalNotification += $userNof;

            }

        }

        // check for organization notifications
        $userData = "org:".$this->userOrg;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $orgQuery = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.notification WHERE to_user_data=:userData AND is_read=0 ORDER BY id DESC",$binds,$errorVar);

        if(!$orgQuery){}else{

            if($orgNof = $GLOBALS['connector']->fetch_num_rows($orgQuery)){

                $totalNotification += $orgNof;

            }

        }

        // check for Level notifications
        $userData = "level:".$this->isAdmin;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $levelQuery = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.notification WHERE to_user_data=:userData AND is_read=0 ORDER BY id DESC",$binds,$errorVar);

        if(!$levelQuery){}else{

            if($levelNof = $GLOBALS['connector']->fetch_num_rows($levelQuery)){

                $totalNotification += $levelNof;

            }

        }

        return $totalNotification;

    }

    public function getAllNotification(){

        $errorVar = array("Notifications","getAllNotification()",3,"Notes",array());

        $allNotification = [];

        // check for user notifications
        $userData = "user:".$this->userId;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $userQuery = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.to_user_data=:userData AND n.is_read=0 ORDER BY n.id DESC",$binds,$errorVar);

        if(!$userQuery){}else{

            while($userNof = $GLOBALS['connector']->fetch($userQuery)){

                $allNotification[] = $userNof;

            }

        }

        // check for organization notifications
        $userData = "org:".$this->userOrg;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $orgQuery = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.to_user_data=:userData AND n.is_read=0 ORDER BY n.id DESC",$binds,$errorVar);

        if(!$orgQuery){}else{

            while($orgNof = $GLOBALS['connector']->fetch($orgQuery)){

                $allNotification[] = $orgNof;

            }

        }

        // check for Level notifications
        $userData = "level:".$this->isAdmin;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $levelQuery = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.to_user_data=:userData AND n.is_read=0 ORDER BY n.id DESC",$binds,$errorVar);

        if(!$levelQuery){}else{

            while($levelNof = $GLOBALS['connector']->fetch($levelQuery)){

                $allNotification[] = $levelNof;

            }

        }
        usort($allNotification, 'sortByDate');
        return $allNotification;


    }

    public function getAllSeenNotification(){

        $errorVar = array("Notifications","getAllSeenNotification()",3,"Notes",array());

        $allSeenNotification = [];

        // check for user notifications
        $userData = "user:".$this->userId;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $userQuery = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.to_user_data=:userData AND n.is_read=1 ORDER BY n.id DESC",$binds,$errorVar);

        if(!$userQuery){}else{

            while($userNof = $GLOBALS['connector']->fetch($userQuery)){

                $allSeenNotification[] = $userNof;

            }

        }

        // check for organization notifications
        $userData = "org:".$this->userOrg;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $orgQuery = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.to_user_data=:userData AND n.is_read=1 ORDER BY n.id DESC",$binds,$errorVar);

        if(!$orgQuery){}else{

            while($orgNof = $GLOBALS['connector']->fetch($orgQuery)){

                $allSeenNotification[] = $orgNof;

            }

        }

        // check for Level notifications
        $userData = "level:".$this->isAdmin;

        $binds = [];
        $binds[] = array(":userData",$userData,PDO::PARAM_STR);

        $levelQuery = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.to_user_data=:userData AND n.is_read=1 ORDER BY n.id DESC",$binds,$errorVar);

        if(!$levelQuery){}else{

            while($levelNof = $GLOBALS['connector']->fetch($levelQuery)){

                $allSeenNotification[] = $levelNof;

            }

        }

        usort($allSeenNotification, 'sortByDate');
        return $allSeenNotification;


    }

    public function sendNotification($type,$title,$description,$toId){

        $errorVar = array("Notifications","sendNotification()",3,"Notes",array());


        // check for notification type (1 = to-user, 2 = to-org, 3 = to-admins-user-level)
        switch ($type){
            case 1:

                $userData = "user:".$toId;
                break;

            case 2:

                $userData = "org:".$toId;
                break;

            case 3:

                $userData = "level:".$toId;
                break;

        }
        if(isset($userData)){

            $date = date('Y-m-d H:i:s',strtotime('NOW'));

            $binds = [];

            $binds[] = array(":title",$title,PDO::PARAM_STR);
            $binds[] = array(":n_type",$type,PDO::PARAM_INT);
            $binds[] = array(":userId",$this->userId,PDO::PARAM_INT);
            $binds[] = array(":userData",$userData,PDO::PARAM_STR);
            $binds[] = array(":createDate",$date,PDO::PARAM_STR);
            $binds[] = array(":des",$description,PDO::PARAM_STR);


            $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.notification (title,n_type,user_id,to_user_data,is_read,created_at,description) VALUES(:title,:n_type,:userId,:userData,0,:createDate,:des)",$binds,$errorVar);

            if(!$query){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }

    }

    public function getNotification($id,$type){

        $errorVar = array("Notifications","getNotification()",3,"Notes",array());

        switch ($type){
            case 1:

                $userData = "user:".$this->userId;
                break;

            case 2:

                $userData = "org:".$this->userOrg;
                break;

            case 3:

                $userData = "level:".$this->isAdmin;
                break;

        }

        if(isset($userData)){

            $binds = [];

            $binds[] = array(":id",$id,PDO::PARAM_INT);
            $binds[] = array(":userData",$userData,PDO::PARAM_STR);

            $query = $GLOBALS['connector']->execute("SELECT n.*,u.fullName FROM networkleads_db.notification AS n INNER JOIN users AS u ON u.id=n.user_id WHERE n.id=:id AND n.to_user_data=:userData",$binds,$errorVar);

            if(!$query){
                return false;
            }else{

                while( $res = $GLOBALS['connector']->fetch($query) ){

                    return $res;

                }

            }

        }else{
            return false;
        }

    }

    public function setNotificationAsRead($id){

        $errorVar = array("Notifications","setNotificationAsRead()",3,"Notes",array());


        $binds = [];

        $binds[] = array(":id",$id,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.notification SET is_read=1 WHERE id=:id",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }

}