<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadProviders.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/mailaccounts.php");

class firstSteps
{
    public $secondsSinceRegister;
    public $isAdmin = false;
    public $showFirstSteps = false;
    public $firstStepsCurrentStep = 1;
    public $showProgressBar = false;
    public $userDoneStep = ['leadProvider'=>false,'mailInbox'=>false,'manuallyLead'=>false];
    public $orgId = NULL;
    public $userId = NULL;

    function __construct($orgData,$userId,$isAdmin)
    {
        $this->isAdmin = $isAdmin;
        $this->userId = $userId;
        $this->orgId = $orgData['id'];
        $this->secondsSinceRegister = (strtotime("now") - strtotime($orgData['createdAt']));
        $secondsInWeek = (60*60*24*7);

        if ($this->secondsSinceRegister < $secondsInWeek) {
            if ($this->isAdmin) {
                if (!isset($_COOKIE['hideFirstSteps'])) {
                    $this->showFirstSteps = true;
                    $this->showProgressBar = true;
                    $this->userDoneStep = ['leadProvider'=>false,'mailInbox'=>false,'manuallyLead'=>false];
                }else{
                    if (!isset($_COOKIE['firstStepsCompleted'])){
                        $this->showProgressBar = true;
                    }
                }
            }
        }
    }

    public function calcFirstSteps(){

        $leadProviders = new leadProviders($this->orgId);
        $totalLeadProviders = $leadProviders->getTotalProvidersActive();

        if($totalLeadProviders > 0){
            $this->firstStepsCurrentStep++;
            $this->userDoneStep['leadProvider'] = true;
        }

        $mailAccounts = new mailAccounts($this->userId,$this->orgId);
        if ($mailAccounts->doesHaveActiveAccount()){
            $this->firstStepsCurrentStep++;
            $this->userDoneStep['mailInbox'] = true;
        }

        $this->calcLeads();
        if (!isset($this->userDoneStep["leadProvider"]) || !isset($this->userDoneStep["mailInbox"]) || !isset($this->userDoneStep["manuallyLead"])){
            $this->userDoneStep["leadProvider"] = false;
            $this->userDoneStep["mailInbox"] = false;
            $this->userDoneStep["manuallyLead"] = false;
        }
        if($this->userDoneStep["leadProvider"] == true && $this->userDoneStep["mailInbox"] == true && $this->userDoneStep["manuallyLead"] == true){
            $this->showFirstSteps = false;
            $this->showProgressBar = false;

            if (!isset($_COOKIE['hideFirstSteps'])) {
                $cookie_name = "hideFirstSteps";
                $cookie_value = "1";
                setcookie($cookie_name, $cookie_value, time() + (86400 * 14), "/");
            }
            if (!isset($_COOKIE['firstStepsCompleted'])) {
                $cookie_name = "firstStepsCompleted";
                $cookie_value = "1";
                setcookie($cookie_name, $cookie_value, time() + (86400 * 14), "/");
            }
        }

    }

    private function calcLeads(){
        $errorVar = array("firstSteps","calcLeads",3,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){

        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                $this->firstStepsCurrentStep++;
                $this->userDoneStep['manuallyLead'] = true;
            }
        }
    }

}