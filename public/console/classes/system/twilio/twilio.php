<?php
/**
 * User
 *
 * PHP Versions  7
 *
 *
 * This class if for integrations with the TWILIP REST API
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (15.5.19) 2019 Niv Apo
 * @license   ---
 */

class twilio
{
    private $client;

    public function __construct($client) {
        $this->client = $client;
    }

    public function createSubAccount($accountName = NULL){

        if($accountName === NULL){return false;}

        $account = $this->client->api->v2010->accounts
            ->create(array("friendlyName" => $accountName));

        // $account->sid
        return $account;
    }

    public function deleteSubAccount($accountSID = NULL){

        if($accountSID === NULL){return false;}

        $this->client->api->v2010->accounts($accountSID)->update(array("status" => "closed"));

        return true;
    }

    public function suspendSubAccount($accountSID = NULL){

        if($accountSID === NULL){return false;}

        $this->client->api->v2010->accounts($accountSID)->update(array("status" => "suspended"));

        return true;
    }

    public function activateSubAccount($accountSID = NULL){

        if($accountSID === NULL){return false;}

        $this->client->api->v2010->accounts($accountSID)->update(array("status" => "active"));

        return true;
    }

    public function sendSMS($fromNumber = NULL,$toNumber = NULL,$msg = ""){

        if($fromNumber === NULL || $toNumber === NULL || $msg == ""){
            return false;
        }

        // fix to number
        $toNumber = trim($toNumber);

        if(substr( $toNumber, 0, 1 ) !== "+"){
            $toNumber = "+1".$toNumber;
        }

        if($toNumber == "+" || $toNumber == "" || preg_match("/[a-z]/i", $toNumber)){return false;}

        $sendingArray = array(
            // A Twilio phone number you purchased at twilio.com/console
            'from' => $fromNumber,
            // the body of the text message you'd like to send
            'body' => $msg,
            "statusCallback" => $_SERVER['LOCAL_NL_URL']."/api/leads/updateSMSStatus.php"
        );

        // Use the client to do fun stuff like send text messages!
        $resp = $this->client->messages->create(
        // the number you'd like to send the message to
            $toNumber,$sendingArray
        );


        return $resp;
    }

    public function searchAvailableNumbers($pattern = NULL,$areaCode = NULL,$searchByState = NULL){

        $filters = ["smsEnabled"=>"true"];
        if($pattern != "" && $pattern !== NULL){
            $filters["Contains"] = $pattern;
        }
        if($areaCode != "" && $areaCode !== NULL){
            $filters["areaCode"] = $areaCode;
        }
        if($searchByState != "" && $searchByState !== NULL){
            $filters["inRegion"] = $searchByState;
        }

        $numbers = $this->client->availablePhoneNumbers('US')->local->read(
            $filters
        );

        // $numbers[0]->friendlyName
        // $numbers[0]->phoneNumber
        // $numbers[0]->capabilities
        // $numbers[0]->region
        // $numbers[0]->postalCode

        $numbersResults = array();
        foreach($numbers as $number){
            $singleNumber = [];
            $singleNumber["friendlyName"] = $number->friendlyName;
            $singleNumber["phoneNumber"] = $number->phoneNumber;
            $singleNumber["region"] = $number->region;
            $singleNumber["postalCode"] = $number->postalCode;

            $numbersResults[] = $singleNumber;
        }

        return $numbersResults;
    }

    public function purchasePhoneNumber($number = NULL){

        // $number should be in the following format : +15106740910


        if($_SERVER["ENVIRONMENT"] == "local"){
            $params = array(
                "phoneNumber" => $number
            );
        }else {
            $params = array(
                "phoneNumber" => $number,
                "smsUrl" => $_SERVER['LOCAL_NL_URL'] . "/api/twilio/incomingSMS.php"
            );
        }

        // Purchase the phone number.
        $number = $this->client->incomingPhoneNumbers
            ->create(
                $params
            );

        //return $number->sid;
        return $number;
    }

    public function activateIncomingSMS($numberPN = NULL){

        // $number should be in the following format : +15106740910

        if($_SERVER["ENVIRONMENT"] == "local"){
            $params = array(

            );
        }else {
            $params = array(
                "smsUrl" => $_SERVER['LOCAL_NL_URL'] . "/api/twilio/incomingSMS.php"
            );
        }
        // Update the phone number.
        $number = $this->client->incomingPhoneNumbers($numberPN)
            ->update(
                $params
            );

        //return $number->sid;
        return $number;
    }

    public function deactivateIncomingSMS($numberPN = NULL){

        // $number should be in the following format : +15106740910

        // Update the phone number.
        $number = $this->client->incomingPhoneNumbers($numberPN)
            ->update(array(
                    "smsUrl" => ""
                )
            );

        //return $number->sid;
        return $number;
    }

    public function deletePhoneNumber($numberPN = NULL){

        if($numberPN === NULL){return false;}

        // Purchase the phone number.

        $deleteNumber = $this->client->incomingPhoneNumbers($numberPN)->delete();

        return $deleteNumber;
    }

    public function getPhoneNumber($numberPN = NULL){

        if($numberPN === NULL){return false;}

        // Purchase the phone number.

        $phoneNumber = $this->client->incomingPhoneNumbers($numberPN)->fetch();

        return $phoneNumber;
    }

    public function getTotalBillingPerDate($fromDate = NULL,$toDate = NULL){

        if($fromDate === NULL || $toDate === NULL){return false;}

        $records = $this->client->usage->records->thisMonth->read(["category"=>"totalprice","startDate"=>$fromDate,"endDate"=>$toDate]);

        $totalPrice = 0;
        foreach($records as $record){
            $totalPrice += $record->price;
        }
        return $totalPrice;
    }

    public function getTotalSmsByNumberPerDate($myNumber,$fromDate,$toDate){

        $toDate = date("Y-m-d 23:59:59",strtotime($toDate));
        $fromDate = date("Y-m-d 00:00:00",strtotime($fromDate));

        $data = [];
        $outgoingSMS = 0;
        $incomingSMS = 0;
        $messages = $this->client->messages->read(
            [
                "dateSentBefore" => new \DateTime($toDate),
                "dateSentAfter" => new \DateTime($fromDate)
            ]);
        foreach ($messages as $record) {
            if($record->from == $myNumber){
                $outgoingSMS++;
            }else if ($record->to == $myNumber){
                $incomingSMS++;
            }
        }

        $data['outgoing'] = $outgoingSMS;
        $data['incoming'] = $incomingSMS;

        return $data;

    }

    public function prettifyPhone($phone_number) {

        // phone should income in the following format: +11234567890

        if ((substr($phone_number, 0, strlen("+1")) === "+1") && strlen($phone_number) == 12) {

            $phone_number = str_replace("+1", "", $phone_number);
            if (strlen($phone_number) == 10) {
                $phone_number = "(" . $phone_number[0] . $phone_number[1] . $phone_number[2] . ") " . $phone_number[3] . $phone_number[4] . $phone_number[5] . " " . $phone_number[6] . $phone_number[7] . $phone_number[8] . $phone_number[9];
            }
            return $phone_number;
        }
        return $phone_number;
    }

}