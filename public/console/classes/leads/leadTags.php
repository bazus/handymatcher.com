<?php
/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 14/03/2019
 * Time: 18:45
*/

class leadTags
{

    private $orgId;

    function __construct($orgId){
        $this->orgId = $orgId;
    }

    public function getData(){

        $errorVar = array("leadTags Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadTags WHERE orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $tags = array();
            while($tag = $GLOBALS['connector']->fetch($getIt)){
                $tags[] = $tag;
            }

            return $tags;
        }

        return false;
    }
    public function getDataById($id){

        $errorVar = array("leadTags Class","getDataById()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadTags WHERE orgId=:orgId AND id=:id AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->fetch($getIt);
        }

        return false;
    }

    public function addNewTag($name = "",$color = ""){

        $errorVar = array("leadTags Class","addNewTag()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $name, PDO::PARAM_STR);
        $binds[] = array(':color', $color, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadTags(orgId,name,color) VALUES(:orgId,:name,:color)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateTag($tagId = NULL,$name = "",$color = ""){

        if($tagId === NULL){return false;}

        $errorVar = array("leadTags Class","updateTag()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $tagId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $name, PDO::PARAM_STR);
        $binds[] = array(':color', $color, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadTags SET name=:name,color=:color WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function deleteTag($tagId = ""){

        $errorVar = array("leadTags Class","deleteTag()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $tagId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadTags SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
}