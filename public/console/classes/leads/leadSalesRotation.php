<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 11/03/2019
 * Time: 16:30
 */

class leadSalesRotation
{

    private $userId;
    private $orgId;

    function __construct($userId = NULL,$orgId = NULL){
        $this->userId = $userId;
        $this->orgId = $orgId;
    }

    public function getRotationRulesOfUser(){
        if($this->userId === NULL){return false;}

        $errorVar = array("leadSalesRotation Class","getRotationRulesOfUser()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadsSalesRotations WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) == 0){return false;}

            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadsSalesRotations WHERE userId=:userId",$binds,$errorVar);
            $r = $GLOBALS['connector']->fetch($getIt);
            $r["acceptProviders"] = unserialize($r["acceptProviders"]);
            $r["acceptSchedule"] = unserialize($r["acceptSchedule"]);
            return $r;
        }

        return false;

    }

    public function checkForExistingRules($createNewIfNotExist = false){
        // This function checks if a user already has a rule (a row in the table in db)
        // Id passed true in $createNewIfNotExist, then create a new row if not exists

        $errorVar = array("leadSalesRotation Class","checkForExistingRules()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadsSalesRotations WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $doesExist = false;
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){$doesExist = true;}
            if($createNewIfNotExist == false){return $doesExist;}

            if($doesExist == 0) {
                // create a new rule
                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadsSalesRotations (userId) VALUES(:userId)", $binds, $errorVar);
                if (!$setIt) {
                    return false;
                } else {
                    return true;
                }
            }


            return true;
        }

        return false;

    }

    public function updateRotationRulesOfUser($acceptProviders = NULL,$scheduleList = NULL){
        if($this->userId === NULL){return false;}

        // First, check if a rule exists, if not then create it
        if(!$this->checkForExistingRules(true)){return false;}


        // Filter all the schedule with no times
        $finalSchedule = array();
        for($i = 0;$i<count($scheduleList);$i++){
            $singleSchedule = $scheduleList[$i];

            if($singleSchedule["isAllDay"] == "true"){
                $finalSchedule[] = $singleSchedule;
            }else {
                // The 'all day' checkbox is not selected so check that hours format are ok
                if ($singleSchedule["S"] != "" && $singleSchedule["E"] != "" && preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $singleSchedule["S"]) && preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $singleSchedule["E"])) {
                    $finalSchedule[] = $singleSchedule;
                }
            }
        }


        $errorVar = array("leadSalesRotation Class","updateRotationRulesOfUser()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':acceptProviders', serialize($acceptProviders), PDO::PARAM_INT);
        $binds[] = array(':acceptSchedule', serialize($finalSchedule), PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsSalesRotations SET acceptProviders=:acceptProviders,acceptSchedule=:acceptSchedule WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    public function toggleActive(){
        if($this->userId === NULL){return false;}

        $errorVar = array("leadSalesRotation Class","toggleActive()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsSalesRotations SET isActive=NOT isActive WHERE userId=:userId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT isActive FROM networkleads_db.leadsSalesRotations WHERE userId=:userId",$binds,$errorVar);
            $r = $GLOBALS['connector']->fetch($getIt);
            return $r['isActive'];
        }

        return false;

    }

    public function doesUserWantThisLead($leadId = ""){

        $wantByProviders = false;
        $wantBySchedule = false;

        $rules = $this->getRotationRulesOfUser();

        // If user rule is not active
        if($rules["isActive"] != "1"){return false;}

        $acceptProviders = $rules["acceptProviders"];
        $acceptSchedule = $rules["acceptSchedule"];

        $lead = new lead($leadId,$this->orgId);
        $leadData = $lead->getData();

        if($acceptProviders != null) {
            if (in_array($leadData["providerId"], $acceptProviders)) {
                $wantByProviders = true;
            }
        }

        if(count($acceptSchedule) == 0){
            $wantBySchedule = true;
        }else{

            $today = date("l",strtotime("now"));
            $totime = date("H:i",strtotime("now"));

            foreach($acceptSchedule as $acceptScheduleSingle){
                if($acceptScheduleSingle["day"] == $today){

                    if($acceptScheduleSingle["isAllDay"] == "true"){
                        $wantBySchedule = true;
                    }else {
                        if ($acceptScheduleSingle["S"] < $acceptScheduleSingle["E"]) {
                            if ($totime >= $acceptScheduleSingle["S"] && $totime <= $acceptScheduleSingle["E"]) {
                                $wantBySchedule = true;
                            }
                        }
                    }

                }
            }
        }

        if($wantByProviders == true && $wantBySchedule == true){
            return true;
        }

        return false;
    }

}