<?php

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Maor Tamir
 * @copyright 21/08/2018
 * @license   ---
 */

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/system/timeController.php");


class leadProviders
{

    protected $orgId;

    function __construct($orgId){$this->orgId = $orgId;}

    public function getProviders($getActive = false){
        $errorVar = array("leadProviders Class","getProviders()",4,"Notes",array());

        $providers = [];

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        if ($getActive == true){
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadProviders WHERE orgId=:orgId AND isDeleted=0 AND isActive=1 ORDER BY id DESC LIMIT 100",$binds,$errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadProviders WHERE orgId=:orgId AND isDeleted=0 ORDER BY id DESC LIMIT 100",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            while($provider = $GLOBALS['connector']->fetch($getIt)){
                $provider['providerAddedDate'] = date("F jS, Y",strtotime($provider['providerAddedDate']));
                $providers[] = $provider;
            }
            return $providers;
        }
        return false;
    }

    public function getProvider($id){
        $errorVar = array("leadProviders Class","getProviders()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadProviders WHERE id=:id AND orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->fetch($getIt);
        }
        return false;
    }

    public function createProvider($providerName,$emailAddress,$website,$address,$phone,$fax,$contactName,$pricePerLead){
        $errorVar = array("leadProviders Class","createProvider()",4,"Notes",array());

        $uniqueKey = "";

        $nameArray = explode(" ",$providerName);
        if(count($nameArray) > 1){
            for ($i=0;$i<count($nameArray);$i++){
                if($i<=3){
                    $uniqueKey .= substr($nameArray[$i],0,1);
                }
            }
        }else{
            $uniqueKey .= substr($nameArray[0],0,2);
        }
        $uniqueKey = strtoupper($uniqueKey);

        $isNM = false;
        if($providerName == "Network Moving"){$isNM = true;}
        $uniqueKey .= "-";

        if(strpos($website, "http://") !== false || strpos($website, "https://") !== false){

        }else if ($website !== ""){
            $website = "http://".$website;
        }

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':providerName', $providerName, PDO::PARAM_STR);
        $binds[] = array(':emailAddress', $emailAddress, PDO::PARAM_STR);
        $binds[] = array(':website', $website, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':fax', $fax, PDO::PARAM_STR);
        $binds[] = array(':contactName', $contactName, PDO::PARAM_STR);
        $binds[] = array(':pricePerLead', $pricePerLead, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadProviders (orgId,providerName,providerEmail,providerWebsite,providerAddress,providerPhone,providerFax,providerContactName,pricePerLead) VALUES(:orgId,:providerName,:emailAddress,:website,:address,:phone,:fax,:contactName,:pricePerLead)",$binds,$errorVar);
        if(!$setIt){
            var_dump($GLOBALS['connector']->getLastError()); //get error from queries
            return false;
        }else{
            $id = $GLOBALS['connector']->last_insert_id();
            $uniqueKey .= $id;

            $binds = [];
            $binds[] = array(':id', $id, PDO::PARAM_INT);
            $binds[] = array(':uniqueKey', $uniqueKey, PDO::PARAM_STR);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadProviders SET uniqueKey=:uniqueKey WHERE id=:id",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                // ============ SEND NOTIFY ============
                    $binds = [];
                    $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

                    $getIt = $GLOBALS['connector']->execute("SELECT organizationName FROM networkleads_db.organizations WHERE id=:orgId", $binds, $errorVar);
                    if (!$getIt) {
                        //return false;
                    } else {
                        $r = $GLOBALS['connector']->fetch($getIt);
                        $orgName = $r["organizationName"];
                        shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => $orgName . " Added a new provider [" . $uniqueKey . "]","env"=>$_SERVER["ENVIRONMENT"]))) . "> /dev/null 2>/dev/null &");
                    }
                // ============ SEND NOTIFY ============

                return true;
            }
        }
        return false;
    }

    public function toggleProvider($providerId,$type){
        $errorVar = array("leadProviders Class","toggleProvider()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $providerId, PDO::PARAM_INT);
        $binds[] = array(':toggle', $type, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadProviders SET isActive=:toggle WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }

    public function removeProvider($providerId){
        $errorVar = array("leadProviders Class","removeProvider()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $providerId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadProviders SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }

    public function updateProvider($id,$providerName,$emailAddress,$website,$address,$phone,$fax,$contactName,$pricePerLead){
        $errorVar = array("leadProviders Class","updateProvider()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':providerName', $providerName, PDO::PARAM_STR);
        $binds[] = array(':emailAddress', $emailAddress, PDO::PARAM_STR);
        $binds[] = array(':website', $website, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':fax', $fax, PDO::PARAM_STR);
        $binds[] = array(':contactName', $contactName, PDO::PARAM_STR);
        $binds[] = array(':pricePerLead', $pricePerLead, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadProviders SET providerName=:providerName,providerEmail=:emailAddress,providerWebsite=:website,providerAddress=:address,providerPhone=:phone,providerFax=:fax,providerContactName=:contactName,pricePerLead=:pricePerLead WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }

    public function getTotalProvidersActive(){
        $errorVar = array("leadProviders Class","getTotalProvidersActive()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadProviders WHERE orgId=:orgId AND isDeleted=0 AND isActive=1",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->fetch_num_rows($getIt);
        }

        return false;
    }

    public function getTotalOpenedEmailsPercentage($providerId = null,$providerName = null,$sDate = false,$eDate = false){
        $errorVar = array("leadProviders Class","getTotalOpenedEmailsPercentage()",4,"Notes",array());

        if($providerId == "IS NULL" && $providerName == "Manual Lead") {
            //search by manual provider

            $binds = [];
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
            $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR, true);

            $allMails = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mails INNER JOIN networkleads_db.leads AS leads ON mails.leadId = leads.id  WHERE leads.orgId =:orgId AND leads.providerId IS NULL AND leads.iframeId IS NULL AND mails.status = 1 AND CTZ(leads.dateReceived,:OFFSET)>=:startDate AND CTZ(leads.dateReceived,:OFFSET)<=:endDate", $binds, $errorVar);
            $openMails = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mails INNER JOIN networkleads_db.leads AS leads ON mails.leadId = leads.id WHERE leads.orgId =:orgId AND leads.providerId IS NULL AND leads.iframeId IS NULL AND mails.status = 1 AND mails.didOpen = 1 AND CTZ(leads.dateReceived,:OFFSET)>=:startDate AND CTZ(leads.dateReceived,:OFFSET)<=:endDate", $binds, $errorVar);
        } elseif ($providerId == "IS NULL" && $providerName == "Iframes"){
            //search by iframe provider

            $binds = [];
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
            $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR, true);

            $allMails = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mails INNER JOIN networkleads_db.leads AS leads ON mails.leadId = leads.id  WHERE leads.orgId =:orgId AND leads.providerId IS NULL AND leads.iframeId IS NOT NULL AND mails.status = 1 AND CTZ(leads.dateReceived,:OFFSET)>=:startDate AND CTZ(leads.dateReceived,:OFFSET)<=:endDate", $binds, $errorVar);
            $openMails = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mails INNER JOIN networkleads_db.leads AS leads ON mails.leadId = leads.id WHERE leads.orgId =:orgId AND leads.providerId IS NULL AND leads.iframeId IS NOT NULL AND mails.status = 1 AND mails.didOpen = 1 AND CTZ(leads.dateReceived,:OFFSET)>=:startDate AND CTZ(leads.dateReceived,:OFFSET)<=:endDate", $binds, $errorVar);
        } else{
            //search by provider id
            $binds = [];
            $binds[] = array(':providerId', $providerId, PDO::PARAM_INT);
            $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
            $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

            $allMails = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mails INNER JOIN networkleads_db.leads AS leads ON mails.leadId = leads.id WHERE leads.providerId=:providerId AND mails.status = 1 AND CTZ(leads.dateReceived,:OFFSET)>=:startDate AND CTZ(leads.dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
            $openMails = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mails INNER JOIN networkleads_db.leads AS leads ON mails.leadId = leads.id WHERE leads.providerId=:providerId AND mails.status = 1 AND mails.didOpen = 1 AND CTZ(leads.dateReceived,:OFFSET)>=:startDate AND CTZ(leads.dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
        }

        if(!$allMails || !$openMails){
            return false;
        }else{
            $allMailsNum =  $GLOBALS['connector']->fetch_num_rows($allMails);
            $openMailsNum =  $GLOBALS['connector']->fetch_num_rows($openMails);

            //percentage calculation
            $allMailsNumInt = (int)$allMailsNum;
            $openMailsNumInt = (int)$openMailsNum;
            if($allMailsNum !=0) {
                return ($openMailsNumInt / $allMailsNumInt) * 100;
            }else{
                return 0;
            }
        }

        return false;
    }

    public function isProviderFromOrganization($providerId){
        $errorVar = array("leadProviders Class","getTotalProvidersActive()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':providerId', $providerId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadProviders WHERE id=:providerId AND orgId=:orgId AND isDeleted=0 AND isActive=1",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }else{
                return false;
            }
        }

        return false;
    }

    public function getTopProviders($sDate = false,$eDate = false,$label = ""){

        $timeController = new timeController();

        $errorVar = array("leadProviders Class","getTopProviders()",4,"Notes",array());

        $orgProviders = [];
        $providersData = [];
        $data = [];

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,providerName FROM networkleads_db.leadProviders WHERE orgId=:orgId AND isDeleted=0 ",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while($provider = $GLOBALS['connector']->fetch($getIt)){
                $orgProviders[] = $provider;
            }
            if (count($orgProviders) > 0){
                $data['haveProviders'] = true;
            }else{
                $data['haveProviders'] = false;
            }

            $orgProviders[] = ["id"=>"IS NULL","providerName"=>"Manual Lead"];
            $orgProviders[] = ["id"=>"IS NULL","providerName"=>"Iframes"];
            foreach ($orgProviders as &$provider){

                if ($sDate == false){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
                }else{
                    $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
                }
                if ($eDate == false){
                    $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
                }else{
                    $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
                }

                $binds = [];
                $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                if ($provider['id'] == "IS NULL" && $provider['providerName'] != 'Iframes'){
                    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId IS NULL AND iframeId IS NULL AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                }elseif ($provider['id'] == "IS NULL" && $provider['providerName'] == 'Iframes'){
                    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId IS NULL AND iframeId IS NOT NULL AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                }else{
                    $binds[] = array(':pId', $provider['id'], PDO::PARAM_INT);
                    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId=:pId AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                }

                if(!$getIt){
                    return false;
                }else{
                    $total = $GLOBALS['connector']->fetch_num_rows($getIt);

                    if ($total > 0){
                        $tempData = ["total"=>$total,"name"=>$provider['providerName']];
                        
                        $binds = [];
                        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                        if ($provider['id'] == "IS NULL"){
                            $getIt = $GLOBALS['connector']->execute("SELECT CTZ(dateReceived,:OFFSET) AS dateReceived FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId IS NULL ORDER BY dateReceived DESC LIMIT 1",$binds,$errorVar);
                        }else{
                            $binds[] = array(':pId', $provider['id'], PDO::PARAM_INT);
                            $getIt = $GLOBALS['connector']->execute("SELECT CTZ(dateReceived,:OFFSET) AS dateReceived FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId=:pId ORDER BY dateReceived DESC LIMIT 1",$binds,$errorVar);
                        }

                        if(!$getIt){
                            return false;
                        }else{
                            $leadsDate = $GLOBALS['connector']->fetch($getIt);

                            $convertedLastLead = $timeController->convert($leadsDate['dateReceived']);

                            $totalPerThisDate = $total;

                            $sDateOfLastMonth = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                            $eDateOfLastMonth = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));

                            $binds = [];
                            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                            $binds[] = array(':startDate', $sDateOfLastMonth, PDO::PARAM_STR);
                            $binds[] = array(':endDate', $eDateOfLastMonth, PDO::PARAM_STR);
                            $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                            if ($provider['id'] == "IS NULL"){
                                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId IS NULL AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                            }else{
                                $binds[] = array(':pId', $provider['id'], PDO::PARAM_INT);
                                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND providerId=:pId AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                            }
                            if (!$getIt){
                                return false;
                            }else{
                                $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);
                                if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                                    $tempData['totalChange'] = 100;
                                }else if ($totalPerNewDate == $totalPerThisDate) {
                                    $tempData['totalChange'] = 0;
                                }else{
                                    $tempData['totalChange'] = ($totalPerThisDate/$totalPerNewDate)*100;
                                }
                                if ($label == "Today"){
                                    $tempData['totalChange'] = "";
                                }else if ($label == "Yesterday"){
                                    $tempData['totalChange'] = "";
                                }else if ($label == "Last Week"){
                                    $tempData['totalChange'] = "";
                                }else if ($label == "Last 30 Days"){
                                    $tempData['totalChange'] = "";
                                }else if ($label == "This Month"){

                                }else if ($label == "Last Month"){
                                    $tempData['totalChange'] = "";
                                }else if ($label == "Custom"){
                                    $tempData['totalChange'] = "";
                                }

                                $emailOpenRatePrec = $this->getTotalOpenedEmailsPercentage($provider['id'],$provider['providerName'],$sDate,$eDate);
                                $providersData[] = ["total"=>$tempData['total'],"name"=>$tempData['name'],"lastLead"=>$leadsDate['dateReceived'],"convertedLastLead"=>$convertedLastLead,"LastMonthChange"=>$tempData['totalChange'],"emailOpenRatePrec"=>$emailOpenRatePrec];
                            }
                        }
                    }
                }
            }

            usort($providersData, function($a, $b) {
                return $b['total'] <=> $a['total'];
            });
            $data['providerData'] = $providersData;

            return $data;
        }

        return false;
    }

}