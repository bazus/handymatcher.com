<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/timeController.php");

/**
 * DateTime Created: 2019-08-28 18:44 (ISRAEL TIME)
 */

class leadLog
{

    function __construct(){

    }

    function addLog($leadId = NULL,$logText = NULL,$userIdCreated = NULL,$displayInLeadStream = false){

        if($leadId === NULL || $logText === NULL){return false;}

        $errorVar = array("leadLog Class","addLog()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':logText', $logText, PDO::PARAM_STR);
        if($userIdCreated === NULL){
            $binds[] = array(':userIdCreated', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':userIdCreated', $userIdCreated, PDO::PARAM_INT);
        }
        $binds[] = array(':displayInLeadStream', $displayInLeadStream, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadLog(leadId,logText,userIdCreated,displayInLeadStream) VALUES(:leadId,:logText,:userIdCreated,:displayInLeadStream)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id();
        }

        return false;
    }

    function setAllLogsAsSeen($orgId = NULL,$userId = NULL){

        if($orgId === NULL || $userId === NULL){return false;}

        $errorVar = array("leadLog Class","setAllLogsAsSeen()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadLog AS ll INNER JOIN networkleads_db.leads AS l ON l.id=ll.leadId INNER JOIN networkleads_db.users AS u ON u.id=:userId SET isSeen=1 WHERE l.orgId=:orgId AND (l.userIdAssigned=:userId OR u.isAdmin=1)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function setLogAsSeen($orgId = NULL,$userId = NULL,$logId = NULL,$leadId = NULL){

        if($orgId === NULL || $userId === NULL || $logId === NULL || $leadId === NULL){return false;}

        $errorVar = array("leadLog Class","setLogAsSeen()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $logId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadLog AS ll INNER JOIN networkleads_db.leads AS l ON l.id=ll.leadId INNER JOIN networkleads_db.users AS u ON u.id=:userId SET isSeen=1 WHERE l.orgId=:orgId AND (l.userIdAssigned=:userId OR u.isAdmin=1) AND ll.id=:id AND ll.leadId=:leadId ",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function setLogAsUnSeen($logId = NULL,$leadId = NULL){

        if($logId === NULL || $leadId === NULL){return false;}

        $errorVar = array("leadLog Class","setLogAsUnSeen()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $logId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadLog SET isSeen=0 WHERE id=:id AND leadId=:leadId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function getLogsForLead($leadId = NULL,$orgId = NULL,$orderByDate = true){

        if($leadId === NULL || $orgId === NULL){return false;}

        $errorVar = array("leadLog Class","getLogsForLead()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_INT,TRUE);
        
        $getIt = $GLOBALS['connector']->execute("SELECT ll.leadId,ll.logText,ll.userIdCreated,CTZ(ll.dateAdded,:OFFSET) AS dateAdded FROM networkleads_db.leadLog AS ll INNER JOIN networkleads_db.leads AS l ON l.id=ll.leadId WHERE ll.leadId=:leadId AND l.orgId=:orgId AND displayInLeadStream=1 ORDER BY ll.dateAdded ASC",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $data = [];
            while($r = $GLOBALS['connector']->fetch($getIt)){
                $data[] = $r;
            }
            return $data;
        }

        return false;
    }

    function getLogsForUser($userId = NULL,$orgId = NULL,$start = 0,$total = 10,$singleLogId = NULL){

        if($userId === NULL || $orgId === NULL){return false;}

        $errorVar = array("leadLog Class","getLogsForUser()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_INT,TRUE);

        if($singleLogId === NULL){
            $getIt = $GLOBALS['connector']->execute("SELECT ll.id,ll.leadId,l.jobNumber,ll.logText,CTZ(ll.dateAdded,:OFFSET) AS dateAdded,ll.isSeen FROM networkleads_db.leadLog AS ll INNER JOIN networkleads_db.leads AS l ON l.id=ll.leadId INNER JOIN networkleads_db.users AS u ON u.id=:userId WHERE l.orgId=:orgId AND (l.userIdAssigned=:userId OR u.isAdmin=1) AND ll.userIdCreated IS NULL ORDER BY ll.id DESC LIMIT ".$start.",".$total,$binds,$errorVar);

        }else{
            $binds[] = array(':logId', $singleLogId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT ll.id,ll.leadId,l.jobNumber,ll.logText,CTZ(ll.dateAdded,:OFFSET) AS dateAdded,ll.isSeen FROM networkleads_db.leadLog AS ll INNER JOIN networkleads_db.leads AS l ON l.id=ll.leadId INNER JOIN networkleads_db.users AS u ON u.id=:userId WHERE l.orgId=:orgId AND (l.userIdAssigned=:userId OR u.isAdmin=1) AND ll.userIdCreated IS NULL AND ll.id=:logId",$binds,$errorVar);
        }

        if(!$getIt){
            return false;
        }else{

            $timeController = new timeController();
            $logs = [];
            while($r = $GLOBALS['connector']->fetch($getIt)){

                if($r["jobNumber"] == NULL || $r["jobNumber"] == ""){
                    $r["jobNumber"] = $r["leadId"];
                }

                $r["dateAddedText"] = $timeController->convertv2($r["dateAdded"]);
                $r["dateAdded"] = date("Y-m-d H:i",strtotime($r["dateAdded"]));

                $logs[] = $r;
            }
        }

        $unReadRows = $this->getNumberOfUnreadLogs($userId,$orgId);
        return ["logs"=>$logs,"unread"=>$unReadRows];
    }

    function getNumberOfUnreadLogs($userId = NULL,$orgId = NULL){

        if($userId === NULL || $orgId === NULL){return false;}

        $errorVar = array("leadLog Class","getNumberOfUnreadLogs()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadLog AS ll INNER JOIN networkleads_db.leads AS l ON l.id=ll.leadId INNER JOIN networkleads_db.users AS u ON u.id=:userId WHERE l.orgId=:orgId AND (l.userIdAssigned=:userId OR u.isAdmin=1) AND ll.userIdCreated IS NULL AND ll.isSeen=0 ORDER BY ll.id DESC",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $unReadRows = $GLOBALS['connector']->fetch_num_rows($getIt);
            return $unReadRows;
        }
        return false;
    }

}