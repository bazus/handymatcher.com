<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 21/08/2018
 * Time: 22:56
 */

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/timeController.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/leads/leadTags.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organizationTwilio.php");

class leads
{
    protected $userId;
    protected $orgId;

    function __construct($orgId,$userId = NULL){
        $this->orgId = $orgId;
        $this->userId = $userId;
    }

    public function getLeadsListForLeadsPage($sDate = "-7 days",$eDate = "today",$userId = null,$page = 0,$getByMovingDate = false){

        // This function gets the leads for the leads page table
        $errorVar = array("leads Class","getLeads()",4,"Notes",array());

        $timeController = new timeController();

        $leads = [];

        $sDate = date("Y-m-d 00:00:00", strtotime($sDate));
        $eDate = date("Y-m-d 23:59:59", strtotime($eDate));

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':sDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':eDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':page', intval($page) ,PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        // If true, select the leads by the moving date
        // If false, select the leads by the receive date
        if($getByMovingDate == true) {
            if ($userId == null) {
                $getIt = $GLOBALS['connector']->execute("SELECT lp.id as providerId,lp.providerName,leads.id,leads.jobNumber,leads.firstname,leads.lastname,leads.email,leads.phone,CTZ(leads.dateReceived, :OFFSET) AS dateReceived,leads.isBadLead,leads.badLeadText,leads.userIdAssigned,leads.isNM,e.moveDate,e.priority,e.status,e.fromCity,e.fromState,e.fromZip,e.toCity,e.toState,e.toZip,leads.tags,leads.iframeId,li.name AS iframeName,leads.isVIP FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_moving_db.leads AS e ON e.leadId=leads.id WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND CTZ(e.moveDate,:OFFSET)>=:sDate AND CTZ(e.moveDate,:OFFSET)<=:eDate ORDER BY e.moveDate DESC,leads.id DESC LIMIT :page,500", $binds, $errorVar);
            } else {
                $binds[] = array(':userId', $userId, PDO::PARAM_INT);
                $getIt = $GLOBALS['connector']->execute("SELECT lp.id as providerId,lp.providerName,leads.id,leads.jobNumber,leads.firstname,leads.lastname,leads.email,leads.phone,CTZ(leads.dateReceived, :OFFSET) AS dateReceived,leads.isBadLead,leads.badLeadText,leads.userIdAssigned,leads.isNM,e.moveDate,e.priority,e.status,e.fromCity,e.fromState,e.fromZip,e.toCity,e.toState,e.toZip,leads.tags,leads.iframeId,li.name AS iframeName,leads.isVIP FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_moving_db.leads AS e ON e.leadId=leads.id WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND CTZ(e.moveDate,:OFFSET)>=:sDate AND CTZ(e.moveDate,:OFFSET)<=:eDate AND leads.userIdAssigned=:userId ORDER BY e.moveDate DESC,leads.id DESC LIMIT :page,500", $binds, $errorVar);
            }
        }else{
            if ($userId == null) {
                $getIt = $GLOBALS['connector']->execute("SELECT lp.id as providerId,lp.providerName,leads.id,leads.jobNumber,leads.firstname,leads.lastname,leads.email,leads.phone,CTZ(leads.dateReceived, :OFFSET) AS dateReceived,leads.isBadLead,leads.badLeadText,leads.userIdAssigned,leads.isNM,e.moveDate,e.priority,e.status,e.fromCity,e.fromState,e.fromZip,e.toCity,e.toState,e.toZip,leads.tags,leads.iframeId,li.name AS iframeName,leads.isVIP FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_moving_db.leads AS e ON e.leadId=leads.id WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND CTZ(leads.dateReceived,:OFFSET)>=:sDate AND CTZ(leads.dateReceived,:OFFSET)<=:eDate ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT :page,500", $binds, $errorVar);
            } else {
                $binds[] = array(':userId', $userId, PDO::PARAM_INT);
                $getIt = $GLOBALS['connector']->execute("SELECT lp.id as providerId,lp.providerName,leads.id,leads.jobNumber,leads.firstname,leads.lastname,leads.email,leads.phone,CTZ(leads.dateReceived, :OFFSET) AS dateReceived,leads.isBadLead,leads.badLeadText,leads.userIdAssigned,leads.isNM,e.moveDate,e.priority,e.status,e.fromCity,e.fromState,e.fromZip,e.toCity,e.toState,e.toZip,leads.tags,leads.iframeId,li.name AS iframeName,leads.isVIP FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_moving_db.leads AS e ON e.leadId=leads.id WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND CTZ(leads.dateReceived,:OFFSET)>=:sDate AND CTZ(leads.dateReceived,:OFFSET)<=:eDate AND leads.userIdAssigned=:userId ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT :page,500", $binds, $errorVar);
            }
        }

        if (!$getIt) {
            return false;
        } else {
            $organizationTwilio = new organizationTwilio($this->orgId, $userId);

            while ($lead = $GLOBALS['connector']->fetch($getIt)) {

                // Convert lead status to text
                $lead["statusText"] = "";
                switch($lead["status"]){
                    case "0":
                        $lead["statusText"] = "New";
                        break;
                    case "1":
                        $lead["statusText"] = "Pending";
                        break;
                    case "2":
                        $lead["statusText"] = "Quoted";
                        break;
                    case "3":
                        $lead["statusText"] = "Booked";
                        break;
                    case "4":
                        $lead["statusText"] = "In progress";
                        break;
                    case "5":
                        $lead["statusText"] = "Storage/Transit";
                        break;
                    case "6":
                        $lead["statusText"] = "Completed";
                        break;
                    case "7":
                        $lead["statusText"] = "Cancelled";
                        break;
                }

                $totalUnread = $organizationTwilio->getTotalUnreadMSGS($lead["id"]);
                $lead['unreadsms'] = $totalUnread;

                if ($lead['providerId'] == NULL) {
                    if ($lead['iframeId'] == NULL) {
                        $lead['providerName'] = "Manual";
                    }else{
                        $lead['providerName'] = "iframe (".$lead['iframeName'].")";
                    }
                }
                $lead['dateReceivedTimestamp'] = strtotime($lead['dateReceived']);
                $lead['dateReceivedAgo'] = $timeController->convert($lead['dateReceived']);
                $lead['dateReceived'] = date("F jS, Y \a\\t h:i A", strtotime($lead['dateReceived']));

                $lead['moveDateTimestamp'] = strtotime($lead['moveDate']);
                $lead['moveDateText'] = date("F jS, Y", strtotime($lead['moveDate']));

                $lead['duplicateEmail'] = false;
                $lead['duplicatePhone'] = false;
                if ($lead['email'] != "" && $this->leadDuplicated($lead['email'], 1)) {
                    $lead['duplicateEmail'] = true;
                }
                if ($lead['phone'] != "" && $this->leadDuplicated($lead['phone'], 2)) {
                    $lead['duplicatePhone'] = true;
                }
                if ($lead['isBadLead'] == 1) {
                    $lead['badLeadText'] = unserialize($lead['badLeadText']);
                }

                $tagsList = unserialize($lead["tags"]);
                $tags = array();

                $leadTags = new leadTags($this->orgId);
                if($tagsList != false) {
                    foreach ($tagsList as $leadTag) {
                        $singleTag = $leadTags->getDataById($leadTag);
                        if ($singleTag != false) {
                            $tags[] = $singleTag;
                        }
                    }
                }

                $lead["tagsData"] = $tags;
                $leads['data'][] = $lead;
            }
            return $leads;
        }

        return false;

    }

    private function queryBuilder($sDate = "-7 days",$eDate = "today",$userId = null,$by = [],$page = 0,$count = false){
        if ($page == 0){
            $page = 0;
        }else{
            $page = ($page * 500);
        }
        $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        $eDate = date("Y-m-d 23:59:59",strtotime($eDate));

        if ($count){
            $q = "SELECT COUNT(*) FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId LEFT JOIN networkleads_moving_db.leads as ml ON ml.leadId=leads.id WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND leads.dateReceived>=:sDate AND leads.dateReceived<=:eDate";

            $binds = [];
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':sDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':eDate', $eDate, PDO::PARAM_STR);

            // =========================== Departments ===========================
            if ($by['departments']['status'] === "true"){
                $binds[] = array(':filterDepartments', $by['departments']['filter'], PDO::PARAM_STR);
                $q.= " AND ml.departmentId=:filterDepartments";
            }

            // =========================== Priority ===========================
            if ($by['priority']['status'] === "true"){
                $binds[] = array(':filterPriority', $by['priority']['filter'], PDO::PARAM_INT);
                $q.= " AND ml.priority=:filterPriority";
            }

            // =========================== Provider ===========================
            if ($by['provider']['status'] === "true"){
                $binds[] = array(':filterProvider', $by['provider']['filter'], PDO::PARAM_STR);
                $q.= " AND leads.providerId=:filterProvider";
            }

            // =========================== Type Of Move ===========================
            if ($by['type']['status'] === "true"){
                $binds[] = array(':filterType', $by['type']['filter'], PDO::PARAM_INT);
                $q.= " AND ml.typeOfMove=:filterType";
            }

            // =========================== User Handling ===========================
            if ($by['user']['status'] === "true"){
                if ($by['user']['filter'] == "noAssign"){
                    $q.= " AND leads.userIdAssigned IS NULL";
                }else{
                    $binds[] = array(':filterUser', $by['user']['filter'], PDO::PARAM_INT);
                    $q.= " AND leads.userIdAssigned=:filterUser";
                }
            }
            // =========================== VIP ===========================
            if ($by['vip']['status'] === "true"){
                $binds[] = array(':filterVip', $by['vip']['filter'], PDO::PARAM_INT);
                $q.= " AND leads.isVIP=:filterVip";
            }
            // =========================== Status ===========================
            if ($by['status']['status'] === "true"){
                $binds[] = array(':filterStatus', $by['status']['filter'], PDO::PARAM_INT);
                $q.= " AND ml.status=:filterStatus";
            }
            if ($userId == null) {

            }else{
                $binds[] = array(':userId', $userId, PDO::PARAM_INT);
                $q .= " AND leads.userIdAssigned=:userId";
            }
            return ['binds'=>$binds,'query'=>$q];
        }else{
            // search by filter
            $q = "SELECT lp.id as providerId,lp.providerName,leads.id,leads.firstname,leads.lastname,leads.email,leads.phone,leads.dateReceived,leads.isBadLead,leads.badLeadText,leads.isNM,leads.userIdAssigned,ml.priority,ml.status,leads.tags,leads.iframeId,li.name AS iframeName,leads.isVIP FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_moving_db.leads as ml ON ml.leadId=leads.id WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND leads.dateReceived>=:sDate AND leads.dateReceived<=:eDate";

            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));

            $binds = [];
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':sDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':eDate', $eDate, PDO::PARAM_STR);
            $binds[] = array(':page', intval($page), PDO::PARAM_INT);

            // =========================== Departments ===========================
            if ($by['departments']['status'] === "true"){
                $binds[] = array(':filterDepartments', $by['departments']['filter'], PDO::PARAM_STR);
                $q.= " AND ml.departmentId=:filterDepartments";
            }

            // =========================== Priority ===========================
            if ($by['priority']['status'] === "true"){
                $binds[] = array(':filterPriority', $by['priority']['filter'], PDO::PARAM_INT);
                $q.= " AND ml.priority=:filterPriority";
            }

            // =========================== Provider ===========================
            if ($by['provider']['status'] === "true"){
                $binds[] = array(':filterProvider', $by['provider']['filter'], PDO::PARAM_STR);
                $q.= " AND leads.providerId=:filterProvider";
            }

            // =========================== Type Of Move ===========================
            if ($by['type']['status'] === "true"){
                $binds[] = array(':filterType', $by['type']['filter'], PDO::PARAM_INT);
                $q.= " AND ml.typeOfMove=:filterType";
            }

            // =========================== User Handling ===========================
            if ($by['user']['status'] === "true"){
                if ($by['user']['filter'] == "noAssign"){
                    $q.= " AND leads.userIdAssigned IS NULL";
                }else{
                    $binds[] = array(':filterUser', $by['user']['filter'], PDO::PARAM_INT);
                    $q.= " AND leads.userIdAssigned=:filterUser";
                }
            }
            // =========================== VIP ===========================
            if ($by['vip']['status'] === "true"){
                $binds[] = array(':filterVip', $by['vip']['filter'], PDO::PARAM_INT);
                $q.= " AND leads.isVIP=:filterVip";
            }
            // =========================== Status ===========================
            if ($by['status']['status'] === "true"){
                $binds[] = array(':filterStatus', $by['status']['filter'], PDO::PARAM_INT);
                $q.= " AND ml.status=:filterStatus";
            }
            if ($userId == null) {
                $q .= " ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT :page,500";
            }else{
                $binds[] = array(':userId', $userId, PDO::PARAM_INT);
                $q .= " AND leads.userIdAssigned=:userId ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT :page,500";
            }
            return ['binds'=>$binds,'query'=>$q];
        }
    }

    public function leadDuplicated($val,$type){
        $errorVar = array("leads Class","leadDuplicated()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        switch ($type){
            case 1:
                $binds[] = array(':email', $val, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE email=:email AND orgId=:orgId AND isDeleted=0",$binds,$errorVar);
                break;
            case 2:
                $binds[] = array(':phone', $val, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE phone=:phone AND orgId=:orgId AND isDeleted=0",$binds,$errorVar);
                break;
        }
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 1){
                return true;
            }
        }

        return false;
    }

    public function getLeadById($leadId){

        $errorVar = array("leads Class","getLead()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT l.*,lp.providerName FROM networkleads_db.leads AS l LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=l.providerId  WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.id=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $lead = $GLOBALS['connector']->fetch($getIt);
            if($lead){
                $lead['dateReceived'] = date("F jS, Y",strtotime($lead['dateReceived']));
            }

            return $lead;
        }

        return false;
    }

    public function updateLead($leadId,$lead){

        $errorVar = array("leads Class","getLead()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        foreach ($lead as $key=>$value){
            $binds[] = [":".$key,$value,PDO::PARAM_STR];
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET firstname=:firstname,lastname=:lastname,email=:email,phone=:phone,phone2=:phone2,comments=:comments WHERE id=:leadId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function sendLeadPost($req,$orgId,$providerId){

        $errorVar = array("leads Class","sendLeadPost()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':firstname', $req['firstname'], PDO::PARAM_STR);
        $binds[] = array(':lastname', $req['lastname'], PDO::PARAM_STR);
        $binds[] = array(':email', $req['email'], PDO::PARAM_STR);
        $binds[] = array(':phone', $req['phone1'], PDO::PARAM_STR);
        if(isset($req['phone2'])){
            $binds[] = array(':phone2',$req['phone2'], PDO::PARAM_STR);
        }else{
            $binds[] = array(':phone2',"", PDO::PARAM_STR);
        }
        $binds[] = array(':comments', "", PDO::PARAM_STR);
        $binds[] = array(':orgId',$orgId, PDO::PARAM_INT);
        $binds[] = array(':ip',$req['ipaddress'], PDO::PARAM_STR);
        $binds[] = array(':providerId',$providerId, PDO::PARAM_STR);
        $key = md5(date("Y-m-d H:i:s",strtotime("NOW")).$providerId);
        $binds[] = [":secretKey",$key,PDO::PARAM_STR];

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leads (firstname,lastname,email,phone,phone2,comments,isDeleted,orgId,ipAddress,providerId,secretKey) VALUES(:firstname,:lastname,:email,:phone,:phone2,:comments,0,:orgId,:ip,:providerId,:secretKey)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            $leadId = $GLOBALS['connector']->last_insert_id($setIt);

            $binds = [];
            $binds[] = array(':leadId',$leadId, PDO::PARAM_INT);
            $binds[] = array(':firstname', $req['firstname'], PDO::PARAM_STR);
            $binds[] = array(':lastname', $req['lastname'], PDO::PARAM_STR);
            $binds[] = array(':email', $req['email'], PDO::PARAM_STR);
            $binds[] = array(':phone', $req['phone1'], PDO::PARAM_STR);
            if(isset($req['phone2'])){
                $binds[] = array(':phone2',$req['phone2'], PDO::PARAM_STR);
            }else{
                $binds[] = array(':phone2',"", PDO::PARAM_STR);
            }
            $binds[] = array(':comments', "", PDO::PARAM_STR);
            $binds[] = array(':orgId',$orgId, PDO::PARAM_INT);
            $binds[] = array(':ip',$req['ipaddress'], PDO::PARAM_STR);
            $binds[] = array(':providerId',$providerId, PDO::PARAM_STR);

            $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadsCopy (leadId,firstname,lastname,email,phone,phone2,comments,isDeleted,orgId,ipAddress,providerId) VALUES(:leadId,:firstname,:lastname,:email,:phone,:phone2,:comments,0,:orgId,:ip,:providerId)",$binds,$errorVar);

            $binds = [];
            $binds[] = array(':leadId',$leadId, PDO::PARAM_STR);
            $binds[] = array(':fromZip',$req['fromzip'], PDO::PARAM_STR);
            $binds[] = array(':toZip',$req['tozip'], PDO::PARAM_STR);
            $binds[] = array(':moveDate',date("Y-m-d H:i:s",strtotime($req['movedate'])), PDO::PARAM_STR);
            $binds[] = array(':moveSize',$req['movesize'], PDO::PARAM_STR);

            $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leads (leadId,fromZip,toZip,moveDate,moveSize) VALUES(:leadId,:fromZip,:toZip,:moveDate,:moveSize)",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leadsCopy (leadId,fromZip,toZip,moveDate,moveSize) VALUES(:leadId,:fromZip,:toZip,:moveDate,:moveSize)",$binds,$errorVar);
                return $leadId;
            }
        }
        return false;

    }

    public function sendLead($data,$userManual = false,$sendMoving = false,$movingData = []){

        if($data["providerId"] == "0"){
            $data["providerId"] = NULL;
        }
        $key = md5(date("Y-m-d H:i:s",strtotime("NOW")).$data['providerId']);

        $errorVar = array("leads Class","sendLeadPost()",4,"Notes",array());

        $binds = [];
        $binds[] = [":orgId",$this->orgId,PDO::PARAM_INT];
        $binds[] = [":firstname",$data["firstname"],PDO::PARAM_STR];
        $binds[] = [":lastname",$data["lastname"],PDO::PARAM_STR];
        $binds[] = [":email",$data["email"],PDO::PARAM_STR];
        $binds[] = [":phone",$data["phone"],PDO::PARAM_STR];
        $binds[] = [":phone2",$data["phone2"],PDO::PARAM_STR];
        $binds[] = [":comments",$data["comments"],PDO::PARAM_STR];
        $binds[] = [":providerId",$data["providerId"],PDO::PARAM_STR];
        $binds[] = [":secretKey",$key,PDO::PARAM_STR];
        if ($userManual){
            $binds[] = [":userId",$data["userId"],PDO::PARAM_INT];
            $binds[] = [":isVIP",$data["vipLead"],PDO::PARAM_BOOL];
            $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leads (firstname,lastname,email,phone,phone2,comments,isDeleted,orgId,ipAddress,providerId,secretKey,userIdAssigned,isVIP) VALUES(:firstname,:lastname,:email,:phone,:phone2,:comments,0,:orgId,'',:providerId,:secretKey,:userId,:isVIP)",$binds,$errorVar);
        }else{
            $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leads (firstname,lastname,email,phone,phone2,comments,isDeleted,orgId,ipAddress,providerId,secretKey) VALUES(:firstname,:lastname,:email,:phone,:phone2,:comments,0,:orgId,'',:providerId,:secretKey)",$binds,$errorVar);
        }
        if(!$setIt){
            echo 1;
            echo $GLOBALS['connector']->getLastError();
            return false;
        }else{
            $leadId = $GLOBALS['connector']->last_insert_id($setIt);
            try {
                $binds = [];
                $binds[] = [":leadId", $leadId, PDO::PARAM_INT];
                $binds[] = [":orgId", $this->orgId, PDO::PARAM_INT];
                $binds[] = [":firstname", $data["firstname"], PDO::PARAM_STR];
                $binds[] = [":lastname", $data["lastname"], PDO::PARAM_STR];
                $binds[] = [":email", $data["email"], PDO::PARAM_STR];
                $binds[] = [":phone", $data["phone"], PDO::PARAM_STR];
                $binds[] = [":phone2", $data["phone2"], PDO::PARAM_STR];
                $binds[] = [":comments", $data["comments"], PDO::PARAM_STR];
                $binds[] = [":providerId", $data["providerId"], PDO::PARAM_STR];

                $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadsCopy (leadId,firstname,lastname,email,phone,phone2,comments,orgId,ipAddress,providerId) VALUES(:leadId,:firstname,:lastname,:email,:phone,:phone2,:comments,:orgId,'',:providerId)", $binds, $errorVar);
            }catch (Exception $e){unset($e);}
            if ($sendMoving) {

                if ($movingData['fromState'] == $movingData['toState']){
                    $typeOfMove = 0;
                }else{
                    $typeOfMove = 1;
                }

                $binds = [];
                $binds[] = array(':leadId', $leadId, PDO::PARAM_STR);
                $binds[] = array(':fromState', $movingData['fromState'], PDO::PARAM_STR);
                $binds[] = array(':toState', $movingData['toState'], PDO::PARAM_STR);
                $binds[] = array(':fromCity', $movingData['fromCity'], PDO::PARAM_STR);
                $binds[] = array(':toCity', $movingData['toCity'], PDO::PARAM_STR);
                $binds[] = array(':fromZip', $movingData['fromZip'], PDO::PARAM_STR);
                $binds[] = array(':toZip', $movingData['toZip'], PDO::PARAM_STR);
                $binds[] = array(':fromAddress', $movingData['fromAddress'], PDO::PARAM_STR);
                $binds[] = array(':toAddress', $movingData['toAddress'], PDO::PARAM_STR);
                $binds[] = array(':fromLevel', $movingData['fromLevel'], PDO::PARAM_STR);
                $binds[] = array(':toLevel', $movingData['toLevel'], PDO::PARAM_STR);
                $binds[] = array(':fromFloor', $movingData['fromFloor'], PDO::PARAM_STR);
                $binds[] = array(':toFloor', $movingData['toFloor'], PDO::PARAM_STR);
                $binds[] = array(':fromApartment', $movingData['fromApartment'], PDO::PARAM_STR);
                $binds[] = array(':toApartment', $movingData['toApartment'], PDO::PARAM_STR);
                $binds[] = array(':fromApartmentType', $movingData['fromApartmentType'], PDO::PARAM_STR);
                $binds[] = array(':toApartmentType', $movingData['toApartmentType'], PDO::PARAM_STR);
                $binds[] = array(':typeOfMove', $typeOfMove, PDO::PARAM_INT);
                $binds[] = array(':moveDate', date("Y-m-d H:i:s", strtotime($movingData['moveDate'])), PDO::PARAM_STR);
                $binds[] = array(':pickupDateStart',date("Y-m-d H:i:s",strtotime($movingData['moveDate'])), PDO::PARAM_STR);
                $binds[] = array(':pickupDateEnd',date("Y-m-d H:i:s",strtotime($movingData['moveDate'])), PDO::PARAM_STR);
                $binds[] = array(':moveSize', $movingData['moveSize'], PDO::PARAM_STR);
                $binds[] = array(':moveSizeLBS', $movingData['moveSizeLBS'], PDO::PARAM_INT);
                $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leads (leadId,fromState,toState,fromCity,toCity,fromZip,toZip,fromAddress,toAddress,fromLevel,toLevel,fromFloor,toFloor,fromAptNumber,toAptNumber,fromAptType,toAptType,moveDate,pickupDateStart,pickupDateEnd,moveSize,typeOfMove,moveSizeLBS) VALUES(:leadId,:fromState,:toState,:fromCity,:toCity,:fromZip,:toZip,:fromAddress,:toAddress,:fromLevel,:toLevel,:fromFloor,:toFloor,:fromApartment,:toApartment,:fromApartmentType,:toApartmentType,CTZ_TDB(:moveDate,:OFFSET),CTZ_TDB(:pickupDateStart,:OFFSET),CTZ_TDB(:pickupDateEnd,:OFFSET),:moveSize,:typeOfMove,:moveSizeLBS)", $binds, $errorVar);

                if (!$setIt) {
                    echo 2;
                    echo $GLOBALS['connector']->getLastError();
                    return false;
                } else {
                    try {
                        $binds = [];
                        $binds[] = array(':leadId', $leadId, PDO::PARAM_STR);
                        $binds[] = array(':fromState', $movingData['fromState'], PDO::PARAM_STR);
                        $binds[] = array(':toState', $movingData['toState'], PDO::PARAM_STR);
                        $binds[] = array(':fromCity', $movingData['fromCity'], PDO::PARAM_STR);
                        $binds[] = array(':toCity', $movingData['toCity'], PDO::PARAM_STR);
                        $binds[] = array(':fromZip', $movingData['fromZip'], PDO::PARAM_STR);
                        $binds[] = array(':toZip', $movingData['toZip'], PDO::PARAM_STR);
                        $binds[] = array(':fromAddress', $movingData['fromAddress'], PDO::PARAM_STR);
                        $binds[] = array(':toAddress', $movingData['toAddress'], PDO::PARAM_STR);
                        $binds[] = array(':moveDate', date("Y-m-d H:i:s", strtotime($movingData['moveDate'])), PDO::PARAM_STR);
                        $binds[] = array(':moveSize', $movingData['moveSize'], PDO::PARAM_STR);
                        $binds[] = array(':moveSizeLBS', $movingData['moveSizeLBS'], PDO::PARAM_INT);
                        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

                        $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leadsCopy (leadId,fromState,toState,fromCity,toCity,fromZip,toZip,fromAddress,toAddress,moveDate,moveSize,moveSizeLBS) VALUES(:leadId,:fromState,:toState,:fromCity,:toCity,:fromZip,:toZip,:fromAddress,:toAddress,CTZ_TDB(:moveDate,:OFFSET),:moveSize,:moveSizeLBS)", $binds, $errorVar);
                    } catch (Exception $e) {
                        unset($e);
                    }
                    return $leadId;
                }
            }else{
                $binds = [];
                $binds[] = array(':leadId',$leadId, PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leads (leadId,fromZip,toZip,moveDate,moveSize,typeOfMove) VALUES(:leadId,'','','".date('Y-m-d',strtotime('now'))."','',1)",$binds,$errorVar);
                $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leadsCopy (leadId,fromZip,toZip,moveDate,moveSize) VALUES(:leadId,'','','".date('Y-m-d',strtotime('now'))."','')",$binds,$errorVar);
                if(!$setIt) {
                    echo 3;
                    echo $GLOBALS['connector']->getLastError();
                    return false;
                }else{
                    return $leadId;
                }
            }
        }
        return false;

    }

    public function getDuplicate($by,$val,$userId,$admin){

        $errorVar = array("leads Class","getDuplicate()",4,"Notes",array());

        $duplicates = [];

        $lead = $this->getLeadById($val);

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        if($by == "phone"){
            $binds[] = array(':phone', $lead['phone'], PDO::PARAM_STR);
            if ($admin) {
                $getIt = $GLOBALS['connector']->execute("SELECT lp.providerName,leads.id,leads.firstname,leads.lastname,leads.email,leads.phone,leads.dateReceived,leads.providerId,leads.iframeId,li.name AS iframeName FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND leads.phone=:phone ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT 30", $binds, $errorVar);
            }else{
                $binds[] = array(':userId', $userId, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT lp.providerName,leads.id,leads.firstname,leads.lastname,leads.email,leads.phone,leads.dateReceived,leads.providerId,leads.iframeId,li.name AS iframeName FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND leads.phone=:phone AND leads.userIdAssigned=:userId ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT 30", $binds, $errorVar);
            }
        }elseif($by == "email"){
            $binds[] = array(':email', $lead['email'], PDO::PARAM_STR);
            if ($admin) {
                $getIt = $GLOBALS['connector']->execute("SELECT lp.providerName,leads.id,leads.firstname,leads.lastname,leads.email,leads.phone,leads.dateReceived,leads.providerId,leads.iframeId,li.name AS iframeName FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND leads.email=:email ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT 30", $binds, $errorVar);
            }else{
                $binds[] = array(':userId', $userId, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT lp.providerName,leads.id,leads.firstname,leads.lastname,leads.email,leads.phone,leads.dateReceived,leads.providerId,leads.iframeId,li.name AS iframeName FROM networkleads_db.leads AS leads LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=leads.iframeId LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=leads.providerId WHERE leads.orgId=:orgId AND leads.isDeleted=0 AND leads.email=:email AND leads.userIdAssigned=:userId ORDER BY leads.dateReceived DESC,leads.id DESC LIMIT 30", $binds, $errorVar);
            }
        }

        if(!$getIt){
            return false;
        }else{
            while($lead = $GLOBALS['connector']->fetch($getIt)){
                if ($lead['providerId'] == NULL) {
                    if ($lead['iframeId'] == NULL) {
                        $lead['providerName'] = "Manual";
                    }else{
                        $lead['providerName'] = "iframe (".$lead['iframeName'].")";
                    }
                }
                $lead['dateReceived'] = date("F jS, Y",strtotime($lead['dateReceived']));
                $duplicates[] = $lead;
            }
        }

        return $duplicates;

    }

    public function getLeadsUsers(){

        $errorVar = array("leads Class","getLeadsUsers()",4,"Notes",array());

        $leads = [];

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT DISTINCT(u.fullName),u.id FROM networkleads_db.leads AS l INNER JOIN networkleads_db.users AS u ON u.id=l.userIdAssigned WHERE u.organizationId=:orgId",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            while($lead = $GLOBALS['connector']->fetch($getIt)){
                $leads[] = $lead;
            }
        }

        return $leads;

    }

    public function getTotalLeadsByDate($compare = false,$sDate = false,$eDate = false,$label){

        $errorVar = array("leads Class","getTotalForTheMonth()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                $totalPerThisDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalLeads'] = $totalPerThisDate;
                $data['totalLeadsIsCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 day"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("-2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of -2 month"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("last day of -2 month"));
                }else if ($label == "Custom"){
                    $data['totalLeadsChange'] = 0;
                    $data['totalLeadsIsCustom'] = true;
                    return $data;
                }

                $binds = [];
                $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND CTZ(dateReceived,:OFFSET)>=:startDate AND CTZ(dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);
                    if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                        $data['totalLeadsChange'] = 100;
                    }else if ($totalPerNewDate == $totalPerThisDate) {
                        $data['totalLeadsChange'] = 0;
                    }else{
                        $data['totalLeadsChange'] = (($totalPerThisDate-$totalPerNewDate)/$totalPerNewDate)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }

    public function getLeadsDataPerDay($dateArray){

        $errorVar = array("leads Class","getLeadsDataPerDay()",4,"Notes",array());

        $data = [];


        $leads = [];
        $booked = [];
        $payments = 0;

        foreach($dateArray AS $theDate){
            $sDay = $theDate['start'];
            $eDay = $theDate['end'];

            $binds = [];
            $binds[] = [":orgId",$this->orgId,PDO::PARAM_INT];
            $binds[] = [":sDay",$sDay,PDO::PARAM_STR];
            $binds[] = [":eDay",$eDay,PDO::PARAM_STR];
            $binds[] = [":OFFSET",NULL,PDO::PARAM_STR,true];

            // booked jobs
            $getIt1 = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS e INNER JOIN networkleads_db.leads AS l ON l.id=e.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND CTZ(l.dateReceived,:OFFSET)>=:sDay AND CTZ(l.dateReceived,:OFFSET)<=:eDay AND (e.status>=3 AND e.status!=7)",$binds,$errorVar);
            if ($getIt1){
                $date = date("Y-m-d",strtotime($sDay));
                $booked[] = ["date"=>$date,"total"=>$GLOBALS['connector']->fetch_num_rows($getIt1)];
            }
            // leads
            $getIt2 = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND CTZ(dateReceived,:OFFSET)>=:sDay AND CTZ(dateReceived,:OFFSET)<=:eDay",$binds,$errorVar);
            if ($getIt2){
                $date = date("Y-m-d",strtotime($sDay));
                $leads[] = ["date"=>$date,"total"=>$GLOBALS['connector']->fetch_num_rows($getIt2)];
            }
            // total payments
            $getIt3 = $GLOBALS['connector']->execute("SELECT SUM(p.totalAmount) AS totalAmount FROM networkleads_moving_db.payments AS p INNER JOIN networkleads_db.leads AS l ON l.id=p.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND CTZ(l.dateReceived,:OFFSET)>=:sDay AND CTZ(l.dateReceived,:OFFSET)<=:eDay AND p.isDeleted=0",$binds,$errorVar);
            if ($getIt3){
                $payment = $GLOBALS['connector']->fetch_num_rows($getIt3);
                if ($payment){
                    $payments += $payment;
                }
            }

        }

        $data['leads'] = $leads;
        $data['bookedJobs'] = $booked;
        $data['payments'] = number_format($payments, 2, '.', ',');

        return $data;

    }

    public function assignUser($leadId,$userId){


        $errorVar = array("leads Class","assignUser()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_STR);
        $binds[] = array(':userId', $userId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET userIdAssigned=:userId WHERE orgId=:orgId AND id=:leadId",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    public function setUpdateInfo($firstName,$lastName,$phone,$email,$comments,$leadId){

        $errorVar = array("leads Class","setUpdateInfo()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':firstName', $firstName, PDO::PARAM_STR);
        $binds[] = array(':lastName', $lastName, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':comments', $comments, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET firstname=:firstName,lastname=:lastName,phone=:phone,email=:email,comments=:comments WHERE id=:leadId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;

    }
    public function getLeadsPhoneNumber($leadsId){

        if(count($leadsId) == 0){return [];}
        $errorVar = array("leads Class","getLeadsPhoneNumber()",4,"Notes",array());

        $leadsPhoneNumbers = [];

        $binds = [];
        $binds[] = array(':leadId', $leadsId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT jobNumber,id,phone FROM networkleads_db.leads WHERE networkleads_db.leads.id IN (" . implode(',', $leadsId) . ")",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            while($leadPhone = $GLOBALS['connector']->fetch($getIt)){

                $leadPhoneNumber = [];

                if($leadPhone["jobNumber"] == "" || $leadPhone["jobNumber"] == NULL){
                    $leadPhoneNumber["jobNumber"] = $leadPhone["id"];
                }else{
                    $leadPhoneNumber["jobNumber"] = $leadPhone["jobNumber"];
                }
                $leadPhoneNumber["phone"] = $leadPhone["phone"];
                $leadPhoneNumber["leadId"] = $leadPhone["id"];

                $leadsPhoneNumbers[] = $leadPhoneNumber;
            }
        }

        return $leadsPhoneNumbers;

    }

}