<?php

/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 23/06/2019
 * Time: 20:07
*/

class leadIframe
{
    private $orgId;

    function __construct($orgId){
        $this->orgId = $orgId;
    }

    public function createIframe($name = NULL){
        if($name === NULL || $name == ""){return false;}

        $uniqueKey = bin2hex(openssl_random_pseudo_bytes(10));

        $errorVar = array("leadIframe Class","createIframe()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $name, PDO::PARAM_STR);
        $binds[] = array(':uniqueKey', $uniqueKey, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadsIframe(orgId,uniqueKey,name,isActive) VALUES(:orgId,:uniqueKey,:name,1)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function getIframeIdByKey($uniqueKey = NULL,$secretToken = NULL,$getBanner = false){

        if($uniqueKey === NULL || $uniqueKey == "" || $secretToken === NULL || $secretToken == ""){return false;}

        $errorVar = array("leadIframe Class","getIframeIdByKey()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':uniqueKey', $uniqueKey, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $secretToken, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadsIframe AS li INNER JOIN networkleads_db.organizations AS org ON org.id=li.orgId WHERE li.uniqueKey=:uniqueKey AND li.isDeleted=0 AND org.secretToken=:secretToken",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                if ($getBanner){
                    $getIt = $GLOBALS['connector']->execute("SELECT li.id,li.bannerUrl FROM networkleads_db.leadsIframe AS li INNER JOIN networkleads_db.organizations AS org ON org.id=li.orgId WHERE li.uniqueKey=:uniqueKey AND li.isDeleted=0 AND org.secretToken=:secretToken",$binds,$errorVar);

                    $r = $GLOBALS['connector']->fetch($getIt);
                    return $r;
                }else{
                    $getIt = $GLOBALS['connector']->execute("SELECT li.id FROM networkleads_db.leadsIframe AS li INNER JOIN networkleads_db.organizations AS org ON org.id=li.orgId WHERE li.uniqueKey=:uniqueKey AND li.isDeleted=0 AND org.secretToken=:secretToken",$binds,$errorVar);

                    $r = $GLOBALS['connector']->fetch($getIt);
                    return $r["id"];
                }
            }else{
                return false;
            }
        }

        return false;
    }

    public function updateIframe($iframeId = NULL,$name = NULL){
        if($iframeId === NULL || $name === NULL || $name == ""){return false;}

        $errorVar = array("leadIframe Class","updateIframe()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $iframeId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $name, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsIframe SET name=:name WHERE id=:id AND orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function addFile($iframeId = NULL,$fileName = NULL){
        if($iframeId === NULL || $fileName === NULL || $fileName == ""){return false;}

        $errorVar = array("leadIframe Class","addFile()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $iframeId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':bannerUrl', $fileName, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsIframe SET bannerUrl=:bannerUrl WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    public function clearIframeImage($iframeId = NULL){
        if($iframeId === NULL){return false;}

        $errorVar = array("leadIframe Class","addFile()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $iframeId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsIframe SET bannerUrl = NULL WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function toggleIframeStatus($iframeId = NULL){
        if($iframeId === NULL){return false;}

        $errorVar = array("leadIframe Class","updateIframe()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $iframeId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsIframe SET isActive = !isActive WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function getIframeData($iframeId = NULL,$getAll = false){

        $errorVar = array("leadIframe Class","getIframeData()",4,"Notes",array());

        $iframeData = [];

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':dateS', date("Y-m-d 00:00:00",strtotime("Today")), PDO::PARAM_STR);
        $binds[] = array(':dateE', date("Y-m-d 23:59:59",strtotime("Today")), PDO::PARAM_STR);

        if ($iframeId == NULL && $getAll == true){
            $getIt = $GLOBALS['connector']->execute("SELECT li.*,(SELECT COUNT(l.id) FROM networkleads_db.leads AS l WHERE l.iframeId = li.id AND dateReceived>=:dateS AND dateReceived<=:dateE AND l.isDeleted=0) AS LeadsRecevied FROM networkleads_db.leadsIframe li WHERE orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        }else{
            $binds = [];
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':id', $iframeId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadsIframe WHERE orgId=:orgId AND id=:id AND isDeleted=0",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            if ($iframeId == NULL && $getAll == true){
                while ($data = $GLOBALS['connector']->fetch($getIt)){
                    $iframeData[] = $data;
                }
            }else{
                $iframeData = $GLOBALS['connector']->fetch($getIt);
            }
            return $iframeData;
        }

        return false;
    }

    public function delete($iframeId = NULL){
        if($iframeId === NULL){return false;}

        $errorVar = array("leadIframe Class","delete()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $iframeId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadsIframe SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

}