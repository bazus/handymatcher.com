<?php

/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 19/11/2018
 * Time: 19:25
 */

class leadDataConfirmation
{
    public $data;

    function __construct($key,$check = true){

        $errorVar = array("leadDataConfirmation Class","Construct",4,"Notes",array());
        if ($key){
            if ($check){
                $binds = [];
                $binds[] = array(':theKey', $key, PDO::PARAM_STR);

                $getIt = $GLOBALS['connector']->execute("SELECT leads.id,leads.dateReceived,leads.jobNumber,leads.firstname,leads.lastname,leads.email,leads.comments,leads.phone,mLeads.fromZip,mLeads.toZip,mLeads.moveDate,mLeads.fromLevel,mLeads.fromFloor,mLeads.fromAptNumber,mLeads.toLevel,mLeads.toFloor,mLeads.toAptNumber,org.id as orgId,org.logoPath,org.organizationName,mLeads.fromState,mLeads.toState,mLeads.fromCity,mLeads.toCity,mLeads.fromAddress,mLeads.toAddress,mLeads.moveInventory,leads.userIdAssigned,org.userIdCreated,mLeads.needStorage FROM networkleads_db.leads AS leads INNER JOIN networkleads_db.organizations AS org ON leads.orgId=org.id INNER JOIN networkleads_moving_db.leads AS mLeads ON leads.id=mLeads.leadId WHERE leads.secretKey=:theKey AND leads.isDeleted=0 LIMIT 1",$binds,$errorVar);
                if(!$getIt){
                    return false;
                }else{
                    $this->data = $GLOBALS['connector']->fetch($getIt);
                }

                return false;
            }else{
                $binds = [];
                $binds[] = array(':theKey', $key, PDO::PARAM_STR);

                $getIt = $GLOBALS['connector']->execute("SELECT leads.id,leads.dateReceived,leads.jobNumber,leads.firstname,leads.lastname,leads.email,leads.phone,leads.comments,mLeads.fromZip,mLeads.toZip,mLeads.moveDate,mLeads.fromLevel,mLeads.fromFloor,mLeads.fromAptNumber,mLeads.toLevel,mLeads.toFloor,mLeads.toAptNumber,org.id as orgId,org.logoPath,org.organizationName,mLeads.fromState,mLeads.toState,mLeads.fromCity,mLeads.toCity,mLeads.fromAddress,mLeads.toAddress,org.userIdCreated,mLeads.needStorage FROM networkleads_db.leads AS leads INNER JOIN networkleads_db.organizations AS org ON leads.orgId=org.id INNER JOIN networkleads_moving_db.leads AS mLeads ON leads.id=mLeads.leadId WHERE leads.secretKey=:theKey AND leads.isDeleted=0 LIMIT 1",$binds,$errorVar);
                if(!$getIt){
                    return false;
                }else{
                    $this->data = $GLOBALS['connector']->fetch($getIt);
                }
            }
        }else{
            $this->data = false;
        }
    }

    public function saveData($id,$firstname,$lastname,$email,$phone,$comments,$fromZip,$fromLevel,$fromFloor,$fromAptNumber,$toZip,$toLevel,$toFloor,$toAptNumber,$movingDate,$items,$ip,$fromCity,$toCity,$fromState,$toState,$fromAddress,$toAddress,$needStorage){
        $errorVar = array("leadDataConfirmation Class","saveData()",4,"Notes",array());

        $binds = [];
        $binds[] = [":leadId",$id,PDO::PARAM_INT];
        $binds[] = [":fName",$firstname,PDO::PARAM_STR];
        $binds[] = [":lName",$lastname,PDO::PARAM_STR];
        $binds[] = [":email",$email,PDO::PARAM_STR];
        $binds[] = [":phone",$phone,PDO::PARAM_STR];
        $binds[] = [":comments",$comments,PDO::PARAM_STR];
        $binds[] = [":ip",$ip,PDO::PARAM_STR];

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadDataConfirmation(leadId,firstname,lastname,email,phone,comments,ip,dateUpdated) VALUES (:leadId,:fName,:lName,:email,:phone,:comments,:ip,NOW())",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $binds = [];
            $binds[] = [":leadId",$id,PDO::PARAM_INT];
            $binds[] = [":fZip",$fromZip,PDO::PARAM_INT];
            $binds[] = [":fLevel",$fromLevel,PDO::PARAM_STR];
            $binds[] = [":fFloor",$fromFloor,PDO::PARAM_STR];
            $binds[] = [":fAptNum",$fromAptNumber,PDO::PARAM_STR];
            $binds[] = [":tZip",$toZip,PDO::PARAM_INT];
            $binds[] = [":tLevel",$toLevel,PDO::PARAM_STR];
            $binds[] = [":tFloor",$toFloor,PDO::PARAM_STR];
            $binds[] = [":tAptNum",$toAptNumber,PDO::PARAM_STR];
            $binds[] = [":movingDate",$movingDate,PDO::PARAM_STR];
            $binds[] = [":items",$items,PDO::PARAM_STR];
            $binds[] = [":ip",$ip,PDO::PARAM_STR];
            $binds[] = [":fCity",$fromCity,PDO::PARAM_STR];
            $binds[] = [":tCity",$toCity,PDO::PARAM_STR];
            $binds[] = [":fState",$fromState,PDO::PARAM_STR];
            $binds[] = [":tState",$toState,PDO::PARAM_STR];
            $binds[] = [":fAddress",$fromAddress,PDO::PARAM_STR];
            $binds[] = [":tAddress",$toAddress,PDO::PARAM_STR];
            $binds[] = [":needStorage",$needStorage,PDO::PARAM_STR];

            $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.leadDataConfirmation(leadId,fromZip,fromLevel,fromFloor,fromAptNumber,toZip,toLevel,toFloor,toAptNumber,movingDate,inventory,ip,dateUpdated,fromState,toState,fromCity,toCity,fromAddress,toAddress,needStorage) VALUES (:leadId,:fZip,:fLevel,:fFloor,:fAptNum,:tZip,:tLevel,:tFloor,:tAptNum,:movingDate,:items,:ip,NOW(),:fState,:tState,:fCity,:tCity,:fAddress,:tAddress,:needStorage)",$binds,$errorVar);
            if(!$getIt) {
                return false;
            }else{
                return true;
            }
        }

        return false;
    }

    public function updateData($id,$firstname,$lastname,$email,$phone,$comments,$fromZip,$fromLevel,$fromFloor,$fromAptNumber,$toZip,$toLevel,$toFloor,$toAptNumber,$movingDate,$items,$ip,$fromCity,$toCity,$fromState,$toState,$fromAddress,$toAddress,$needStorage){
        $errorVar = array("leadDataConfirmation Class","saveData()",4,"Notes",array());

        $binds = [];
        $binds[] = [":leadId",$id,PDO::PARAM_INT];
        $binds[] = [":fName",$firstname,PDO::PARAM_STR];
        $binds[] = [":lName",$lastname,PDO::PARAM_STR];
        $binds[] = [":email",$email,PDO::PARAM_STR];
        $binds[] = [":phone",$phone,PDO::PARAM_STR];
        $binds[] = [":comments",$comments,PDO::PARAM_STR];
        $binds[] = [":ip",$ip,PDO::PARAM_STR];

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadDataConfirmation SET firstname=:fName,lastname=:lName,email=:email,phone=:phone,comments=:comments,ip=:ip,dateUpdated=CURRENT_TIMESTAMP ,isSeen=0 WHERE leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return $GLOBALS['connector']->getLastError();
            return false;
        }else{
            $binds = [];
            $binds[] = [":leadId",$id,PDO::PARAM_INT];
            $binds[] = [":fZip",$fromZip,PDO::PARAM_INT];
            $binds[] = [":fLevel",$fromLevel,PDO::PARAM_STR];
            $binds[] = [":fFloor",$fromFloor,PDO::PARAM_STR];
            $binds[] = [":fAptNum",$fromAptNumber,PDO::PARAM_STR];
            $binds[] = [":fCity",$fromCity,PDO::PARAM_STR];
            $binds[] = [":fState",$fromState,PDO::PARAM_STR];
            $binds[] = [":fAddress",$fromAddress,PDO::PARAM_STR];
            $binds[] = [":tZip",$toZip,PDO::PARAM_INT];
            $binds[] = [":tLevel",$toLevel,PDO::PARAM_STR];
            $binds[] = [":tFloor",$toFloor,PDO::PARAM_STR];
            $binds[] = [":tAptNum",$toAptNumber,PDO::PARAM_STR];
            $binds[] = [":tCity",$toCity,PDO::PARAM_STR];
            $binds[] = [":tState",$toState,PDO::PARAM_STR];
            $binds[] = [":tAddress",$toAddress,PDO::PARAM_STR];
            $binds[] = [":movingDate",$movingDate,PDO::PARAM_STR];
            $binds[] = [":items",$items,PDO::PARAM_STR];
            $binds[] = [":needStorage",$needStorage,PDO::PARAM_STR];
            $binds[] = [":ip",$ip,PDO::PARAM_STR];

            $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leadDataConfirmation SET fromZip=:fZip,fromLevel=:fLevel,fromFloor=:fFloor,fromAptNumber=:fAptNum,fromCity=:fCity,fromState=:fState,fromAddress=:fAddress,toZip=:tZip,toLevel=:tLevel,toFloor=:tFloor,toAptNumber=:tAptNum,toCity=:tCity,toState=:tState,toAddress=:tAddress,movingDate=:movingDate,inventory=:items,ip=:ip,dateUpdated=CURRENT_TIMESTAMP,needStorage=:needStorage WHERE leadId=:leadId",$binds,$errorVar);
            if(!$getIt) {
                return $GLOBALS['connector']->getLastError();
                return false;
            }else{
                return true;
            }
        }

        return false;
    }

    public function didUpdatedLeadData($key){


        $errorVar = array("leadDataConfirmation Class","didUpdatedLeadData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':theKey', $key, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads as l INNER JOIN networkleads_db.leadDataConfirmation AS ldc ON ldc.leadId=l.id WHERE l.secretKey=:theKey",$binds,$errorVar);
        if(!$getIt){
            return NULL;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }else{
                return false;
            }
        }

        return false;


    }

    public function getData($leadId){
        $errorVar = array("leadDataConfirmation Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT l.*,ldc.fromZip,ldc.fromLevel,ldc.fromFloor,ldc.fromAptNumber,ldc.toZip,ldc.toLevel,ldc.toFloor,ldc.toAptNumber,ldc.movingDate,ldc.inventory,ldc.ip,ldc.fromState,ldc.toState,ldc.fromCity,ldc.toCity,ldc.fromAddress,ldc.toAddress,ldc.needStorage FROM networkleads_db.leadDataConfirmation as l INNER JOIN networkleads_moving_db.leadDataConfirmation AS ldc ON ldc.leadId=l.leadId WHERE l.leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $data = $GLOBALS['connector']->fetch($getIt);
            if ($data){
                $data['formattedMovingDate'] = $data['movingDate'];
                $data['inventory'] = unserialize($data['inventory']);
                $data['movingDate'] = date("F j, Y",strtotime($data['movingDate']));
            }
            return $data;
        }
        return false;


    }

    public function didSee($id){
        $errorVar = array("leadDataConfirmation Class","didSee()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $id, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadDataConfirmation SET isSeen=1 WHERE leadId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }
}