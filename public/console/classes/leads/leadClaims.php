<?php

/**
 * Created by PhpStorm.
 * User: nivapo
 * Date: 24/01/2020
 * Time: 23:13
 */

class leadClaims
{

    private $leadId;
    private $orgId;

    function __construct($orgId,$leadId){
        $this->orgId = $orgId;
        $this->leadId = $leadId;
    }

    public function createClaim($address = "",$city = "",$state = "",$zip = "",$firstName = "",$lastName = "",$email = "",$phone1 = "",$phone2 = "",$details = "",$amount = 0,$filesAttached = [],$userIdCreated = NULL,$signature = NULL,$signatureIP = NULL){

        $errorVar = array("leadClaims Class","createClaim()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':firstName', $firstName, PDO::PARAM_STR);
        $binds[] = array(':lastName', $lastName, PDO::PARAM_STR);
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':phone1', $phone1, PDO::PARAM_STR);
        $binds[] = array(':phone2', $phone2, PDO::PARAM_STR);
        $binds[] = array(':details', $details, PDO::PARAM_STR);
        if(!$amount || $amount == "" || is_null($amount)){
            $binds[] = array(':amount', 0, PDO::PARAM_STR);
        }else{
            $binds[] = array(':amount', $amount, PDO::PARAM_STR);
        }

        if(!$userIdCreated || $userIdCreated == "" || $userIdCreated === NULL){
            $binds[] = array(':userIdCreated', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':userIdCreated', $userIdCreated, PDO::PARAM_INT);
        }


        if(!is_array($filesAttached)){
            $filesAttached = [];
        }else{
            $filesAttached = serialize($filesAttached);
        }
        $binds[] = array(':filesAttached', $filesAttached, PDO::PARAM_STR);

        $randomNumber = rand(0,999999); // To make it impossible to get two identical secret keys for leads
        $secretKey = md5(date("Y-m-d H:i:s",strtotime("NOW")).$this->orgId.$randomNumber);

        $randomNumber = rand(0,999999); // To make it impossible to get two identical secret keys for leads
        $secretToken = md5(date("Y-m-d H:i:s",strtotime("NOW")).$this->orgId.$randomNumber);
        $binds[] = array(':secretKey', $secretKey, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $secretToken, PDO::PARAM_STR);


    if($signature === NULL || $signatureIP === NULL) {
        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadClaims(leadId,orgId,secretKey,secretToken,address,city,state,zip,firstName,lastName,email,phone1,phone2,details,amount,filesAttached,userIdCreated) VALUES(:leadId,:orgId,:secretKey,:secretToken,:address,:city,:state,:zip,:firstName,:lastName,:email,:phone1,:phone2,:details,:amount,:filesAttached,:userIdCreated)",$binds,$errorVar);
    }else {
        $binds[] = array(':signature', $signature, PDO::PARAM_STR);
        $binds[] = array(':signatureIP', $signatureIP, PDO::PARAM_STR);
        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadClaims(leadId,orgId,secretKey,secretToken,address,city,state,zip,firstName,lastName,email,phone1,phone2,details,amount,filesAttached,userIdCreated,signature,signatureIP,signedAt) VALUES(:leadId,:orgId,:secretKey,:secretToken,:address,:city,:state,:zip,:firstName,:lastName,:email,:phone1,:phone2,:details,:amount,:filesAttached,:userIdCreated,:signature,:signatureIP,NOW())",$binds,$errorVar);
    }
     if(!$getIt){
         var_dump($GLOBALS['connector']->getLastError());
            return false;
        }else{
            return true;
        }
        return false;
    }

    public function getClaimsForLead(){

        $claims = [];

        $errorVar = array("leadClaims Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadClaims WHERE orgId=:orgId AND leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            while($claim = $GLOBALS['connector']->fetch($getIt)){
                $claim["fullName"] = $claim["firstName"] . " " . $claim["lastName"];
                $ret = @unserialize($claim["filesAttached"]);
                if($ret == false || $ret === null){
                    //Error case
                    $claim["filesAttached"] = [];
                }else{
                    $claim["filesAttached"] = unserialize($claim["filesAttached"]);
                }
                $claims[] = $claim;
            }
        }
        return $claims;
    }

    public function deleteClaims($claimsIds){
        $errorVar = array("leadClaims Class","deleteClaims()",4,"Notes",array());
        $isOk = true;

        for($i = 0; $i < count($claimsIds); $i++ ){

            if($claimsIds[$i] == "" || $claimsIds[$i] == false || $claimsIds[$i] == null){continue;}

            $binds = array();
            $binds[] = array(':id', $claimsIds[$i], PDO::PARAM_INT);
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadClaims SET isDeleted=1 WHERE id=:id AND leadId=:leadId AND orgId=:orgId", $binds, $errorVar);
            if (!$setIt) {
                $isOk = false;
            }
        }

        return $isOk;
    }

    public function getSingleClaimByKey($key,$token){
        $errorVar = array("leadClaims Class","getSingleClaimByKey()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':secretKey', $key, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $token, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadClaims WHERE secretKey=:secretKey AND secretToken=:secretToken", $binds, $errorVar);
        if (!$getIt) {
            return false;
        }else{
            $claim = $GLOBALS['connector']->fetch($getIt);
            return $claim;
        }


        return false;
    }

    public function signClaimByKey($key,$token,$signature,$userIP){
        $errorVar = array("leadClaims Class","signClaimByKey()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':secretKey', $key, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $token, PDO::PARAM_STR);
        $binds[] = array(':signature', $signature, PDO::PARAM_STR);
        $binds[] = array(':signatureIP', $userIP, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadClaims SET signature=:signature,signatureIP=:signatureIP,signedAt=NOW() WHERE secretKey=:secretKey AND secretToken=:secretToken", $binds, $errorVar);
        if (!$getIt) {
            return false;
        }else{
            return true;
        }


        return false;
    }

    public function getSingleClaim($claimsId){
        $errorVar = array("leadClaims Class","getSingleClaim()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $claimsId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadClaims WHERE id=:id AND orgId=:orgId", $binds, $errorVar);
        if (!$getIt) {
            return false;
        }else{
            $claim = $GLOBALS['connector']->fetch($getIt);
            return $claim;
        }


        return false;
    }

  }