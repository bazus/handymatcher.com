<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/leadTags.php");

/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 21/08/2018
 * Time: 22:56
 */

class lead
{

    private $leadId;
    private $orgId;
    private $data = NULL;

    function __construct($leadId,$orgId){
        $this->leadId = $leadId;
        $this->orgId = $orgId;
    }

    public function getData(){

        $errorVar = array("lead Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        if($this->orgId === NULL){
            $getIt = $GLOBALS['connector']->execute("SELECT l.*,lp.providerName,u.fullName AS userCreated,li.name AS iframeName,CTZ(l.dateReceived,:OFFSET) AS dateReceived,lpr.request FROM networkleads_db.leads AS l LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=l.iframeId LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=l.providerId LEFT JOIN networkleads_db.users AS u ON u.id=l.userIdAssigned LEFT JOIN networkleads_db.leadsPostResponse AS lpr ON lpr.leadId=l.id WHERE l.isDeleted=0 AND l.id=:leadId",$binds,$errorVar);

        }else{
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT l.*,lp.providerName,u.fullName AS userCreated,li.name AS iframeName,CTZ(l.dateReceived,:OFFSET) AS dateReceived,lpr.request FROM networkleads_db.leads AS l LEFT JOIN networkleads_db.leadsIframe AS li ON li.id=l.iframeId LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=l.providerId LEFT JOIN networkleads_db.users AS u ON u.id=l.userIdAssigned LEFT JOIN networkleads_db.leadsPostResponse AS lpr ON lpr.leadId=l.id WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.id=:leadId",$binds,$errorVar);
        }

        if(!$getIt){
            return false;
        }else{

            $lead = $GLOBALS['connector']->fetch($getIt);

            if($lead){
                $lead["tags"] = unserialize($lead["tags"]);

                if ($lead['providerId'] == NULL) {
                    if ($lead['iframeId'] == NULL) {
                        $lead['providerName'] = "Manual";
                    }else{
                        $lead['providerName'] = "iframe (".$lead['iframeName'].")";
                    }
                }
                if($lead['badLeadText']){
                    $lead['badLeadText'] = unserialize($lead['badLeadText']);
                }
                if($lead['request']){
                    $lead['request'] = unserialize($lead['request']);
                }
                $lead['dateReceivedOriginal'] = $lead['dateReceived'];
                $lead['dateReceived'] = date("F jS, Y \\a\\t h:i A",strtotime($lead['dateReceived']));

                $this->data = $lead;
                return $lead;
            }else{
                return false;
            }
        }

        return false;
    }

    public function getDataByKey($secretKey){

        $errorVar = array("lead Class","getLeadIdByKey()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':secretKey', $secretKey, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT id,firstname,lastname,orgId FROM networkleads_db.leads WHERE secretKey=:secretKey",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $lead = $GLOBALS['connector']->fetch($getIt);
            return $lead;
        }

        return false;
    }

    public function updateLead($lead){

        $errorVar = array("lead Class","getLead()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        foreach ($lead as $key=>$value){
            $binds[] = [":".$key,$value,PDO::PARAM_STR];
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET firstname=:firstname,lastname=:lastname,email=:email,phone=:phone,phone2=:phone2,comments=:comments WHERE id=:leadId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateLeadNotes($notes = ""){
        $errorVar = array("lead Class","updateLeadNotes()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':notes', $notes, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET notes=:notes WHERE id=:leadId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateVIP($vip)
    {
        $errorVar = array("lead Class", "updateVIP()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':vip', $vip, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET isVIP=:vip WHERE id=:leadId AND orgId=:orgId", $binds, $errorVar);
        if (!$setIt) {
            return false;
        } else {
            return true;
        }

        return false;
    }

    public function updateUserIdAssigned($userId)
    {
        $errorVar = array("lead Class", "updateUserIdAssigned()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':userIdAssigned', $userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET userIdAssigned=:userIdAssigned WHERE id=:leadId AND orgId=:orgId", $binds, $errorVar);
        if (!$setIt) {
            return false;
        } else {
            return true;
        }

        return false;
    }

    public function setAsBadLead($isBadLead,$badLeadText)
    {
        $errorVar = array("lead Class", "badEstimate()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':id', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':isBadLead', $isBadLead, PDO::PARAM_INT);
        $binds[] = array(':badLeadText', $badLeadText, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET isBadLead=:isBadLead,badLeadText=:badLeadText WHERE id=:id", $binds, $errorVar);
        if (!$setIt) {
            return false;
        } else {
            return true;
        }

        return false;
    }

    public function deleteLead()
    {
        $errorVar = array("lead Class", "deleteLead()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET isDeleted=1 WHERE id=:leadId AND orgId=:orgId", $binds, $errorVar);
        if (!$setIt) {
            return false;
        } else {
            return true;
        }

        return false;
    }
    
    public function getBadLeadData(){

        $errorVar = array("lead Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT badLeadText FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND id=:leadId LIMIT 1",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $lead = $GLOBALS['connector']->fetch($getIt);
            if($lead){
                if($lead['badLeadText']){
                    $lead['badLeadText'] = unserialize($lead['badLeadText']);
                }
            }

            return $lead;
        }

        return false;
    }

    public function isLeadFromOrg($checkUser = false,$userId = NULL){
        $errorVar = array("lead Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        if ($checkUser){
            $binds[] = array(':userId', $userId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND userIdAssigned=:userId AND isDeleted=0 AND id=:leadId",$binds,$errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE orgId=:orgId AND isDeleted=0 AND id=:leadId",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }else{
                return false;
            }
        }
    }

    public function getLeadFiles($isAdmin,$userId){
        $errorVar = array("lead Class","getLeadFiles()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT lf.* FROM networkleads_db.leadFiles AS lf INNER JOIN networkleads_db.leads AS l ON l.id=lf.leadId WHERE lf.leadId=:leadId AND l.orgId=:orgId AND lf.isDeleted=0",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            $data = [];
            while($res = $GLOBALS['connector']->fetch($getIt)){
                if (!$isAdmin){
                    if ($res['userIdCreated'] == $userId){
                        $res['deleteAble'] = true;
                    }else{
                        $res['deleteAble'] = false;
                    }
                }else{
                    $res['deleteAble'] = true;
                }
                $res['CreatedAt'] = date("F j, Y h:i A",strtotime($res['CreatedAt']));
                $data[] = $res;
            }
            return $data;
        }

    }

    public function saveUploadFile($userId,$url,$fileName){
        $errorVar = array("lead Class","saveUploadFile()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':url', $url, PDO::PARAM_STR);
        $binds[] = array(':fileName', $fileName, PDO::PARAM_STR);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadFiles(leadId,url,originalFileName,userIdCreated,createdAt,isDeleted) VALUES(:leadId,:url,:fileName,:userId,NOW(),0)",$binds,$errorVar);
        if (!$setIt){
            return false;
        }else{
            return true;
        }
    }

    public function removeFile($fileId,$isAdmin,$userId){
        if($this->isLeadFromOrg()){
            $errorVar = array("lead Class","removeFile()",4,"Notes",array());

            $binds = [];
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
            $binds[] = array(':fileId', $fileId, PDO::PARAM_INT);

            if ($isAdmin){
                $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadFiles SET isDeleted=1 WHERE leadId=:leadId AND id=:fileId",$binds,$errorVar);
            }else{
                $binds[] = array(':userId', $userId, PDO::PARAM_INT);
                $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadFiles SET isDeleted=1 WHERE leadId=:leadId AND id=:fileId AND userIdCreated=:userId",$binds,$errorVar);
            }
            if (!$setIt){
                return false;
            }else{
                return true;
            }
        }
    }

    public function checkTotalLeadFiles(){
        if($this->isLeadFromOrg()){
            $errorVar = array("lead Class","removeFile()",4,"Notes",array());

            $binds = [];
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leadFiles WHERE leadId=:leadId AND isDeleted=0",$binds,$errorVar);

            if (!$getIt){
                return false;
            }else{
                $total = $GLOBALS['connector']->fetch_num_rows($getIt);
                if ($total < 9){
                    return true;
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }

    public function requestRefund($reason){
        if($this->isLeadFromOrg()){
            $errorVar = array("lead Class","requestRefund()",4,"Notes",array());

            $binds = [];
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':reason', $reason, PDO::PARAM_STR);

            $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_partners_db.refundRequests(organizationId,leadId,reason) VALUES(:orgId,:leadId,:reason)",$binds,$errorVar);

            if (!$getIt){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    public function isLeadWithinHours($hoursToCheck = 80){

        $resp = [];
        $resp['status'] = false; // response from server - false for bad response and true for good response
        $resp['valid'] = null; // lead is over the $hoursToCheck hours - null for server error true for lead past the $hoursToCheck hours and false for lead is within that range
        $resp['msg'] = 'Server Error not found in SQL or is not partner';

        if($this->isLeadFromOrg()) {
            $errorVar = array("lead Class", "isLeadWithinHours()", 4, "Notes", array());

            $binds = [];
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT dateReceived FROM networkleads_db.leads WHERE id=:leadId AND isDeleted=0 AND isNM=1",$binds,$errorVar);
            if (!$getIt){
                return $resp;
            }else{
                $leadData = $GLOBALS['connector']->fetch($getIt);

                $resp['status'] = true;

                $lead = strtotime($leadData['dateReceived']); // the lead date time in seconds
                $now = strtotime("NOW"); // current date time in seconds

                $timePassInSec = $now - $lead; // time difference between the dates in seconds

                $secInHour = 3600; // seconds in an hour
                $checkSec = ($secInHour*$hoursToCheck); // number of days to check , we added another 8 hours (adir request)

                if ($timePassInSec >= $checkSec){
                    // by default time is bigger or equal to $hoursToCheck hours
                    $resp['valid'] = false;
                    $resp['msg'] = "Time difference between dates in seconds are: ".$timePassInSec." time allowed by function in seconds: ".$checkSec;

                    return $resp;
                }else{
                    // by default time is within the $hoursToCheck hours limit
                    $resp['valid'] = true;
                    $resp['msg'] = "Time difference between dates in seconds are: ".$timePassInSec." time allowed by function in seconds: ".$checkSec;

                    return $resp;
                }
            }
        }else{
            $resp['msg'] = 'Lead is not from user organization';

            return $resp;
        }
    }

    public function updateTag($tags = NULL){

        $errorVar = array("lead Class","addTag()",3,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        if ($tags){
            $binds[] = array(':tags', serialize($tags), PDO::PARAM_STR);
        }else{
            $binds[] = array(':tags', NULL, PDO::PARAM_STR);
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leads SET tags=:tags WHERE id=:leadId AND orgId=:orgId", $binds, $errorVar);
        if (!$setIt){
            return false;
        }else{
            return true;
        }
    }

    public function getLeadTags(){

        $errorVar = array("lead Class","addTag()",3,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT tags FROM networkleads_db.leads WHERE id=:leadId AND orgId=:orgId", $binds, $errorVar);
        if (!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            if ($r){
                $tags = unserialize($r['tags']);
                $activeTags = array();
                $leadTags = new leadTags($this->orgId);
                if($tags == false){return false;}
                foreach ($tags as $tag){
                    $singleTag = $leadTags->getDataById($tag);
                    if($singleTag != false) {
                        $activeTags[] = $tag;
                    }
                }
                return $activeTags;
            }else{
                return false;
            }
        }
    }

    public function getRawPostData(){
        $errorVar = array("lead Class","getRawPostData()",3,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT lpr.request FROM networkleads_db.leadsPostResponse AS lpr INNER JOIN networkleads_db.leads AS l ON l.id=lpr.leadId WHERE l.id=:leadId AND l.orgId=:orgId", $binds, $errorVar);
        if (!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            return $r["request"];
        }
        return false;
    }

}