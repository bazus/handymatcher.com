<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 2019-03-14
 * Time: 19:53
 */

class leadReminders
{
    private $leadId;
    private $isValid = false; // is lead from my org
    private $orgId;

    function __construct($leadId,$orgId){
        $errorVar = array("leadReminders Class","Construct",4,"Notes",array());
        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE id=:leadId AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $count = $GLOBALS['connector']->fetch_num_rows($getIt);
            if ($count == 1){
                $this->orgId = $orgId;
                $this->leadId = $leadId;
                $this->isValid = true;
            }
        }
    }

    public function __call($method,$arguments) {
        // This function is called when any private function is triggered.
        // This checks if the 'isValid' var is true, if not - the function will not be called.
        if($this->isValid == true) {
            return call_user_func_array(array($this, $method), $arguments);
        }
        return false;
    }

    public function getData(){
        $errorVar = array("leadReminders Class","getData()",4,"Notes",array());

        $data = [];

        $binds = [];
        $binds[] = [":leadId",$this->leadId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.leadReminders WHERE leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            $reminder = $GLOBALS['connector']->fetch($getIt);

            // if no reminder, return false
            if(!$reminder){return false;}

            $binds = [];
            $binds[] = [":reminderId",$reminder['id'],PDO::PARAM_INT];

            $getCalendarData = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.calendarEvents WHERE reminderId=:reminderId AND isDeleted=0",$binds,$errorVar);
            if (!$getCalendarData){
                return false;
            }else{
                $data['calendarData'] = $GLOBALS['connector']->fetch($getCalendarData);
                $data['reminderData'] = $reminder;
                return $data;
            }
        }
    }

    public function setReminder($reminderId = NULL,$subject = NULL,$dateStart = NULL,$dateEnd = NULL,$reminder = NULL,$reminder2 = NULL,$color = NULL,$userId = NULL){
        $errorVar = array("leadReminders Class","setReminder()",4,"Notes",array());



        if($reminderId == "" || $reminderId == NULL){
            // No reminder, create one


            $binds = [];
            $binds[] = [":leadId",$this->leadId,PDO::PARAM_INT];
            $binds[] = [":reminder",$reminder,PDO::PARAM_INT];
            $binds[] = [":reminder2",$reminder2,PDO::PARAM_INT];
            $binds[] = [":color",$color,PDO::PARAM_STR];

            $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.leadReminders (leadId,reminderTime,secondReminder,color) VALUES(:leadId,:reminder,:reminder2,:color)",$binds,$errorVar);
            if (!$setIt){
                return false;
            }else{
                $reminderId = $GLOBALS['connector']->last_insert_id($setIt);

                $binds = [];
                $binds[] = [":title",$subject,PDO::PARAM_STR];
                $binds[] = [":userId",$userId,PDO::PARAM_INT];
                $binds[] = [":dateS",$dateStart,PDO::PARAM_STR];
                $binds[] = [":dateE",$dateEnd,PDO::PARAM_STR];
                $binds[] = [":reminderId",$reminderId,PDO::PARAM_INT];

                $setCalendar = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.calendarEvents (title,userId,startDate,endDate,isReminder,reminderId) VALUES(:title,:userId,:dateS,:dateE,1,:reminderId)",$binds,$errorVar);
                if (!$setCalendar){
                    return false;
                }else{
                    return true;
                }
            }


        }else{
            // Reminder exists, update it

            $binds = [];
            $binds[] = [":reminderId",$reminderId,PDO::PARAM_INT];
            $binds[] = [":reminder",$reminder,PDO::PARAM_INT];
            $binds[] = [":reminder2",$reminder2,PDO::PARAM_INT];
            $binds[] = [":color",$color,PDO::PARAM_STR];

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadReminders SET reminderTime=:reminder,secondReminder=:reminder2,color=:color WHERE id=:reminderId",$binds,$errorVar);
            if (!$setIt){
                return false;
            }else{

                $binds = [];
                $binds[] = [":title",$subject,PDO::PARAM_STR];
                $binds[] = [":userId",$userId,PDO::PARAM_INT];
                $binds[] = [":dateS",$dateStart,PDO::PARAM_STR];
                $binds[] = [":dateE",$dateEnd,PDO::PARAM_STR];
                $binds[] = [":reminderId",$reminderId,PDO::PARAM_INT];

                $setCalendar = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET title=:title,startDate=:dateS,endDate=:dateE,userId=:userId WHERE reminderId=:reminderId",$binds,$errorVar);
                if (!$setCalendar){
                    return false;
                }else{
                    return true;
                }
            }

        }


    }


    public function deleteReminder($reminderId){
        $errorVar = array("leadReminders Class","deleteReminder()",4,"Notes",array());

        $binds = [];
        $binds[] = [":reminderId",$reminderId,PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.leadReminders SET isDeleted=1 WHERE id=:reminderId",$binds,$errorVar);
        if (!$setIt){
            return false;
        }else{

            $setCalendar = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET isDeleted=1 WHERE reminderId=:reminderId",$binds,$errorVar);
            if (!$setCalendar){
                return false;
            }else{
                return true;
            }
        }
    }

}