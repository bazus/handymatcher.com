<?php

/**
 * Amazon s3
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2018 Niv Apo
 * @license   ---
 */


class s3
{
    protected $s3;

    // ==================================== START __construct ======================================
    function __construct(){
        $this->s3 = new Aws\S3\S3Client([
            'version' => '2006-03-01',
            'region'  => 'us-east-1',
            'credentials' => array(
                'key' => $_SERVER['AWS_S3_KEY'],
                'secret' => $_SERVER['AWS_S3_SECRET'],
            )
        ]);
    }
    // ==================================== END __construct ======================================

    // ==================================== START getObject ======================================
    public function getObjectURL($bucket = "",$key = "",$getAllObject = false)
    {
        $result = $this->s3->getObject(array(
            'Bucket' => $bucket,
            'Key'    => $key
        ));

        if(!$getAllObject){
            return $result["@metadata"]["effectiveUri"];
        }else {
            return $result;
        }
    }
    // ==================================== END getObject ======================================

    // ==================================== START putObject ======================================
    public function putObject($bucket = "",$key = "",$temp_file_location = "",$acp = "private" /* permissions */, $contentType = false){

        /* Permissions list */

        /*
         *
         * private
         * public-read
         * public-read-write
         * aws-exec-read
         * authenticated-read
         * bucket-owner-read
         * bucket-owner-full-control
         *
         */

        if($contentType == false){
            return $this->s3->putObject([
                'Bucket' => $bucket,
                'Key' => $key,
                'SourceFile' => $temp_file_location,
                'ACL' => $acp,
            ]);
        }else {
            return $this->s3->putObject([
                'Bucket' => $bucket,
                'Key' => $key,
                'SourceFile' => $temp_file_location,
                'ContentType' => $contentType,
                'ACL' => $acp,
            ]);
        }


    }
    // ==================================== END putObject ======================================

    // ==================================== START getLimitedTimeUrl ======================================
    public function getLimitedTimeUrl($bucket = "",$key = "",$minutes = "0",$userId,$isFileManager = true,$isAdmin = false){
        if ($isFileManager){
            if ($isAdmin){
                //Creating a presigned URL for a private object
                $cmd = $this->s3->getCommand('GetObject', [
                    'Bucket' => $bucket,
                    'Key' => $key
                ]);

                $request = $this->s3->createPresignedRequest($cmd, '+' . $minutes . ' minutes');

                // Get the actual presigned-url
                $presignedUrl = (string)$request->getUri();

                return $presignedUrl;
            }else {
                if ($this->userOwnFile($userId, $key)) {
                    //Creating a presigned URL for a private object
                    $cmd = $this->s3->getCommand('GetObject', [
                        'Bucket' => $bucket,
                        'Key' => $key
                    ]);

                    $request = $this->s3->createPresignedRequest($cmd, '+' . $minutes . ' minutes');

                    // Get the actual presigned-url
                    $presignedUrl = (string)$request->getUri();

                    return $presignedUrl;
                } else {
                    return false;
                }
            }
        }else{
            $cmd = $this->s3->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);

            $request = $this->s3->createPresignedRequest($cmd, '+'.$minutes.' minutes');

            // Get the actual presigned-url
            $presignedUrl = (string) $request->getUri();

            return $presignedUrl;
        }

    }
    // ==================================== END getLimitedTimeUrl ======================================

    // ==================================== START userOwnFile() ======================================
    private function userOwnFile($userID,$file){

        $errorVar = array("Files","userOwnFile()",3,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);
        $binds[] = array(':file',$file,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.usersFiles WHERE user_id=:userID AND file_url=:file AND is_deleted=0",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($query) > 0){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    // ==================================== END userOwnFile() ======================================

    /*
     *
     *  DO NOT USE ! ONLY FOR EMERGENCY.
     *
     *

    // ==================================== START createBucket ======================================
    public function createBucket($bucketName = ""){

        $this->s3->createBucket(['Bucket' => $bucketName]);
        // Wait until the bucket is created
        $this->s3->waitUntil('BucketExists', ['Bucket' => $bucketName]);

    }
    // ==================================== END createBucket ======================================

    // ==================================== START deleteAllObjectsInBucket ======================================
    public function deleteAllObjectsInBucket($bucket = ""){
        $batch = Aws\S3\BatchDelete::fromListObjects($this->s3, ['Bucket' => $bucket]);
        return $batch->delete();
    }
    // ==================================== END deleteAllObjectsInBucket ======================================

    // ==================================== START deleteBucket ======================================
    public function deleteBucket($bucket = ""){
        return $this->s3->deleteBucket(['Bucket' => $bucket]);
    }
    // ==================================== END deleteBucket ======================================
    */
}