<?php
require_once $_SERVER['LOCAL_NL_PATH']."/console/classes/files/s3.php";

/**
 * Upload File To Amazon s3
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2020 (25.2.2020) Niv Apo
 * @license   ---
 */


class uploadFile
{
    protected $s3;

    // ==================================== START __construct ======================================
    function __construct(){
        $this->s3 = new s3();
    }
    // ==================================== END __construct ======================================

    // ==================================== START upload ======================================
    public function upload($file,$bucket = "thenetworkleads",$folder = "",$maxFileSize = 10 /* in MB */,$typesAllowed = ["jpg","png","jpeg","gif","pdf","doc","docx","xls","xlsx","mp3","wav","numbers","pages"]){
        $resp = [];
        $resp["status"] = false;
        $resp["reason"] = "";

        try {

            if(isset($file) && count($file) > 0) {

                if ($file["tmp_name"] != "") {

                    $thisDate = date("Y.m.d.H-i-s", strtotime("now"));

                    $filePathInfo = pathinfo($file["name"]);
                    $newFileName =  hash("md5", $filePathInfo["filename"]) . $thisDate . "." . $filePathInfo["extension"];

                    $imageFileType = pathinfo($newFileName, PATHINFO_EXTENSION);

                    // max file size in MB
                    if ($file['size'] > ($maxFileSize * 1024 * 1024)) {
                        $resp["reason"] = "Sorry, file size too big";
                        return $resp;
                    }

                    if(!in_array(strtolower($imageFileType),$typesAllowed)){
                        $resp["reason"] = "Sorry, only the following file types are allowed: ".implode(",",$typesAllowed);
                        return $resp;
                    }

                    // if everything is ok, try to upload file
                    $fileType = false;
                    if ($imageFileType == "jpg" || $imageFileType == "JPG" || $imageFileType == "jpeg" || $imageFileType == "JPEG") {
                        $fileType = "image/jpeg";
                    }

                    if ($imageFileType == "png" || $imageFileType == "PNG") {
                        $fileType = "image/png";
                    }

                    $file_name = $newFileName;
                    $temp_file_location = $file['tmp_name'];

                    $fileKey = $folder."/".$file_name;

                    $result = $this->s3->putObject($bucket, $fileKey, $temp_file_location, "private", $fileType);
                    if ($result->get("@metadata")['statusCode'] == 200) {

                        $singleFile = [];
                        $singleFile["name"] = $filePathInfo["filename"].".".$filePathInfo["extension"];
                        $singleFile["key"] = $fileKey;
                        $singleFile["url"] = $result->get("@metadata")['effectiveUri'];

                        $resp["status"] = true;
                        $resp["file"] = $singleFile;

                        return $resp;
                    } else {
                        $resp["reason"] = "Couldn't upload file";
                        return $resp;
                    }

                }

            }
        }catch (Exception $e){
            $resp["reason"] = "Couldn't upload file";
            return $resp;
        }

        return $resp;
    }
    // ==================================== END upload ======================================

}