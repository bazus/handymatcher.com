<?php

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/reports/reports.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/estimateCalculation.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/moving/payments.php");

class financial extends reports
{

    public function getTotalPaymentsByDate($orgId,$sDate,$eDate){

        $errorVar = array("leads Class","getTotalPaymentsByDate()",4,"Notes",array());

        $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        $eDate = date("Y-m-d 23:59:59",strtotime($eDate));

        $leadPayments = [];

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT l.id,l.jobNumber FROM networkleads_db.leads AS l WHERE l.orgId=:orgId AND CTZ(l.dateReceived,:OFFSET)>=:startDate AND CTZ(l.dateReceived,:OFFSET)<=:endDate AND l.isDeleted=0  ORDER BY l.dateReceived",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $totalPerThisDate = 0;
            while($x = $GLOBALS['connector']->fetch($getIt)){
                //$totalPerThisDate = $totalPerThisDate+$x['totalAmount'];

                $singleLead = [];

                // ======== (START) GET TOTAL ESTIMATE ========
                $estimateCalculation = new estimateCalculation($x["id"], $this->orgId);
                $estimateData = $estimateCalculation->getData();

                if ($estimateData["totalEstimate"] == "" || $estimateData["totalEstimate"] == false) {
                    $singleLead['totalEstimate'] = "N/A";
                } else {
                    $singleLead['totalEstimate'] = number_format($estimateData["totalEstimate"], 2, '.', ',');
                }
                // ======== (END) GET TOTAL ESTIMATE ========


                // ======== (START) GET PAYMENTS (TOTAL & BALANCE) ========
                $payments = new payments($orgId);
                $paymentsData = $payments->getPaymentsByLeads($x["id"]);

                $singleLead["balance"] = $paymentsData["paymentsLeft"];
                $singleLead["totalPayments"] = number_format($paymentsData["totalPayments"], 2, '.', ',');
                // ======== (END) GET PAYMENTS (TOTAL & BALANCE) ========

                if ($x["jobNumber"] == "" || $x["jobNumber"] == false) {
                    $singleLead['jobNumber'] = "#" . $x["id"];
                } else {
                    $singleLead['jobNumber'] = "#" . $x["jobNumber"];
                }
                $singleLead['leadId'] = $x["id"];

                $leadPayments[] = $singleLead;
            }
            return $leadPayments;

        }

        return false;

    }

    public function getPaymentsByDate($orgId,$sDate,$eDate){

        $errorVar = array("leads Class","getPaymentsByDate()",4,"Notes",array());

        $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        $eDate = date("Y-m-d 23:59:59",strtotime($eDate));

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT p.totalAmount,CTZ(p.paymentDate,:OFFSET) AS paymentDate FROM networkleads_moving_db.payments AS p INNER JOIN networkleads_db.leads AS l on l.id=p.leadId WHERE l.orgId=:orgId AND CTZ(p.paymentDate,:OFFSET)>=:startDate AND CTZ(p.paymentDate,:OFFSET)<=:endDate AND l.isDeleted=0 AND p.isDeleted=0",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            $payments = [];
            while($x = $GLOBALS['connector']->fetch($getIt)){

                $paymentDate =  date("Y-m-d",strtotime($x['paymentDate']));

                if(array_key_exists($paymentDate,$payments)){
                    $payments[$paymentDate] += (float)$x['totalAmount'];
                }else{
                    $payments[$paymentDate]= (float)$x['totalAmount'];
                }
            }
            $sDate = date("Y-m-d",strtotime($sDate));
            while($sDate <= $eDate){
                if(!array_key_exists($sDate,$payments)){
                    $payments[date("Y-m-d",strtotime($sDate))] = 0;
//                    $payments[$sDate] = 0;
                }
                $sDate = date("Y-m-d",strtotime($sDate." +1 day"));
            }
            ksort($payments);

            return $payments;
        }

        return false;

    }


}