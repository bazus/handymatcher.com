<?php

require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/reports/reports.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");
class sales extends reports
{
    protected $orgId;
    protected $currentUserId;
    function __construct($id,$userId){$this->orgId = $id;$this->currentUserId = $userId;}

    function getSalesPerformanceData($start,$end){
        $errorVar = array("sales Class","getSalesPerformanceData()",4,"Notes",array());

        $data = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
//        $binds[] = array(':userId', $this->currentUserId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,salesBonusPerc FROM networkleads_db.users WHERE organizationId=:orgId AND userTypeId=3 AND isDeleted=0 ORDER BY id LIMIT 100",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            while ($user = $GLOBALS['connector']->fetch($getIt)){

                $binds = [];
                $binds[] = [":userId",$user['id'],PDO::PARAM_INT];
                $binds[] = [":sDate",$start,PDO::PARAM_STR];
                $binds[] = [":eDate",$end,PDO::PARAM_STR];
                $binds[] = [':OFFSET', NULL, PDO::PARAM_STR,true];

                $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS e INNER JOIN networkleads_db.leads AS l on l.id=e.leadid WHERE l.userIdAssigned=:userId AND CTZ(e.dateReceived,:OFFSET)>=:sDate AND CTZ(e.dateReceived,:OFFSET)<=:eDate AND l.isDeleted=0",$binds,$errorVar);
                $totalLeads = $GLOBALS['connector']->fetch_num_rows($query);

                $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS etl INNER JOIN networkleads_db.leads AS l on l.id=etl.leadid WHERE etl.userSent=:userId AND CTZ(etl.sentDate,:OFFSET)>=:sDate AND CTZ(etl.sentDate,:OFFSET)<=:eDate AND l.isDeleted=0",$binds,$errorVar);
                $totalEmailsSent = $GLOBALS['connector']->fetch_num_rows($query);

                $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS e INNER JOIN networkleads_db.leads AS l on l.id=e.leadid WHERE l.userIdAssigned=:userId AND CTZ(e.dateReceived,:OFFSET)>=:sDate AND CTZ(e.dateReceived,:OFFSET)<=:eDate AND (e.status>=3 AND e.status!=7) AND l.isDeleted=0",$binds,$errorVar);
                $totalBooked = $GLOBALS['connector']->fetch_num_rows($query);

                $query = $GLOBALS['connector']->execute("SELECT SUM(totalAmount) AS totalAmount FROM networkleads_moving_db.payments WHERE userIdCreated=:userId AND CTZ(paymentDate,:OFFSET)>=:sDate AND CTZ(paymentDate,:OFFSET)<=:eDate AND isDeleted=0",$binds,$errorVar);
                $payments = $GLOBALS['connector']->fetch($query);

                $bonusAmount = ($payments['totalAmount']/100)*$user['salesBonusPerc'];
                $totalbonus = "$".number_format($bonusAmount, 2, '.', ',')." (".$user['salesBonusPerc']."%)";
                if ($payments['totalAmount'] > 0) {
                    $payments = number_format($payments['totalAmount'], 2, '.', ',');
                }else{
                    $payments = number_format(0, 2, '.', ',');
                }


                // calculate the booking percentage
                $bookPerc = 0;
                if($totalBooked > 0 && $totalLeads > 0){
                    $bookPerc = (100/$totalLeads)*$totalBooked;
                }
                $bookPerc = number_format($bookPerc, 2, '.', ',');


                $data[] = ["id"=>$user['id'],"name"=>$user['fullName'],"totalLeads"=>$totalLeads,"payments"=>$payments,"books"=>$totalBooked,'totalEmailsSent'=>$totalEmailsSent,'salesBonusPerc'=>$totalbonus,'salesBonus'=>$bonusAmount,'bookPercentage'=>$bookPerc];

            }
            return $data;
        }
    }
    function getAdvertisingPerformanceData($start,$end){
        $errorVar = array("sales Class","getAdvertisingPerformanceData()",4,"Notes",array());

        $data = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,providerName,pricePerLead FROM networkleads_db.leadProviders WHERE orgId=:orgId AND isDeleted=0 ORDER BY id LIMIT 100",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            while ($leadProvider = $GLOBALS['connector']->fetch($getIt)){

                $id = $leadProvider['id'];
                $name = $leadProvider['providerName'];

                $totalLeads = $this->getTotalLeads($id,$start,$end);

                // ===== (START) TOTAL LEADS SPENT =====
                if($leadProvider['pricePerLead'] > 0){
                    $totalLeadsSpent = number_format($totalLeads * $leadProvider['pricePerLead'], 2, '.', ',');
                }else{
                    $totalLeadsSpent = number_format(0, 2, '.', ',');
                }
                // ===== (END) TOTAL LEADS SPENT =====

                $totalBooked = $this->getTotalBookedJobs($id,$start,$end);
                if ($totalLeads > 0){
                    $bookChange = number_format( ($totalBooked/$totalLeads) * 100, 2 );
                }else{
                    $bookChange = number_format(0, 2, '.', ',');
                }
                $bookedEstimate = $this->getTotalEstimate($id,$start,$end);
                $bookedEstimate = number_format($bookedEstimate, 2, '.', ',');
                $customerPayments = $this->getCustomerPayments($id,$start,$end);
                $customerPayments = number_format($customerPayments, 2, '.', ',');

                $data[] = ['name'=>$name,'totalLeads'=>$totalLeads,'totalLeadsSpent'=>$totalLeadsSpent,'totalBooked'=>$totalBooked,'bookChange'=>$bookChange,'bookedEstimate'=>$bookedEstimate,'customerPayments'=>$customerPayments];

            }
            return $data;
        }
    }

    private function getTotalLeads($id,$start,$end){

        $errorVar = array("sales Class","getTotalLeads()",4,"Notes",array());

        $binds = [];
        $binds[] = [":pId",$id,PDO::PARAM_INT];
        $binds[] = [":sDate",$start,PDO::PARAM_STR];
        $binds[] = [":eDate",$end,PDO::PARAM_STR];
        $binds[] = [':OFFSET', NULL, PDO::PARAM_STR,true];
       $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE providerId=:pId AND CTZ(dateReceived,:OFFSET)>=:sDate AND CTZ(dateReceived,:OFFSET)<=:eDate AND isDeleted=0",$binds,$errorVar);
        if (!$query){
            return false;
        }else{
            return $GLOBALS['connector']->fetch_num_rows($query);
        }

    }

    private function getTotalBookedJobs($id,$start,$end){

        $errorVar = array("sales Class","getTotalBookedJobs()",4,"Notes",array());

        $binds = [];
        $binds[] = [":pId",$id,PDO::PARAM_INT];
        $binds[] = [":sDate",$start,PDO::PARAM_STR];
        $binds[] = [":eDate",$end,PDO::PARAM_STR];
        $binds[] = [':OFFSET', NULL, PDO::PARAM_STR,true];

        $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS e INNER JOIN networkleads_db.leads AS l on l.id=e.leadid WHERE l.providerId=:pId AND CTZ(l.dateReceived,:OFFSET)>=:sDate AND CTZ(l.dateReceived,:OFFSET)<=:eDate AND (e.status>=3 AND e.status!=7) AND l.isDeleted=0",$binds,$errorVar);
        if (!$query){
            return false;
        }else{
            return $GLOBALS['connector']->fetch_num_rows($query);
        }

    }

    private function getCustomerPayments($id,$start,$end){

        $errorVar = array("sales Class","getCustomerPayments()",4,"Notes",array());

        $totalPayments = 0;

        $binds = [];
        $binds[] = [":pId",$id,PDO::PARAM_INT];
        $binds[] = [":sDate",$start,PDO::PARAM_STR];
        $binds[] = [":eDate",$end,PDO::PARAM_STR];
        $binds[] = [':OFFSET', NULL, PDO::PARAM_STR,true];

        $query = $GLOBALS['connector']->execute("SELECT id FROM networkleads_db.leads WHERE providerId=:pId AND CTZ(dateReceived,:OFFSET)>=:sDate AND CTZ(dateReceived,:OFFSET)<=:eDate AND isDeleted=0",$binds,$errorVar);
        if (!$query){
            return false;
        }else{

            while($tempData = $GLOBALS['connector']->fetch($query)) {
                $binds = [];
                $binds[] = [":id",$tempData['id'],PDO::PARAM_INT];
                $getIt = $GLOBALS['connector']->execute("SELECT SUM(totalAmount) AS totalAmount FROM networkleads_moving_db.payments WHERE leadId=:id AND isDeleted=0",$binds,$errorVar);
                if ($getIt){
                    $payment = $GLOBALS['connector']->fetch($getIt);
                    $totalPayments += $payment['totalAmount'];
                }
            }

            return $totalPayments;

        }

    }

    private function getTotalEstimate($id,$start,$end){
        $errorVar = array("sales Class","getTotalEstimate()",4,"Notes",array());

        $totalEstimate = 0;

        $binds = [];
        $binds[] = [":id",$id,PDO::PARAM_INT];
        $binds[] = [":sDate",$start,PDO::PARAM_STR];
        $binds[] = [":eDate",$end,PDO::PARAM_STR];
        $binds[] = [':OFFSET', NULL, PDO::PARAM_STR,true];

        $query = $GLOBALS['connector']->execute("SELECT id FROM networkleads_db.leads WHERE providerId=:id AND CTZ(dateReceived,:OFFSET)>=:sDate AND CTZ(dateReceived,:OFFSET)<=:eDate AND isDeleted=0",$binds,$errorVar);
        if (!$query){
            return $totalEstimate;
        }else{
            while($tempData = $GLOBALS['connector']->fetch($query)){

                $estimateCalculation = new estimateCalculation($tempData['id']);
                $estimate = $estimateCalculation->getData();
                $totalEstimate += $estimate['totalEstimate'];

            }
            return $totalEstimate;
        }

    }
}