<?php

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (30.6.18) 2018 Niv Apo
 * @license   ---
 */


class support extends user {

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId) {
        parent::__construct($userId);
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START createReport() ======================================
    public function createReport($issueTitle = "",$issueType = "",$description = "") {

        $errorVar = array("Support Class","createReport()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':issueTitle', $issueTitle, PDO::PARAM_STR);
        $binds[] = array(':issueType', $issueType, PDO::PARAM_STR);
        $binds[] = array(':description', $description, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.userTickets(userId,issueTitle,issueType,issueDescription,submittedDate) VALUES(:userId,:issueTitle,:issueType,:description,NOW())",$binds,$errorVar);
        if(!$setIt){
            return $GLOBALS['connector']->getLastError();
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START createReport() ======================================

    // ==================================== START getLastReports() ======================================
    public function getLastReports($totalReports = 10) {

        $errorVar = array("Support Class","getLastReports()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':limit', $totalReports, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.userTickets WHERE userId=:userId ORDER BY submittedDate ASC LIMIT :limit",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $reports = array();
            while($r = $GLOBALS['connector']->fetch($getIt)){
                $reports[] = $r;
            }
            return $reports;
        }

        return false;
    }
    // ==================================== START getLastReports() ======================================


    // ==================================== START closeTicket() ======================================
    public function closeTicket($ticketId = false) {
        if(!$ticketId){return false;}

        $errorVar = array("Support Class","closeTicket()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':ticketId', $ticketId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userTickets SET isClosed=1 WHERE userId=:userId AND id=:ticketId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START closeTicket() ======================================


}