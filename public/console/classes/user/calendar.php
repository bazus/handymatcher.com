<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/passwords.php");

class calendar{

    protected $userId;
    protected $orgId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = NULL,$orgId = NULL) {
        if($userId === NULL){return;}
        $this->userId = $userId;
        $this->orgId = $orgId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getActiveEvents() ======================================
    public function getEvents($getPrsonalOnly = false,$fromDate = NULL,$toDate = NULL){
        if($fromDate === NULL || $toDate === NULL){return false;}

        $errorVar = array("Calendar","getEvents()",3,"Notes",array(),false);

        $events = [];

        $binds = [];
        $binds[] = array(':fromDate',$fromDate,PDO::PARAM_STR);
        $binds[] = array(':toDate',$toDate,PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        if ($getPrsonalOnly == true){
            $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
            $query = $GLOBALS['connector']->execute("SELECT uc.*,lr.color,lr.leadId,CTZ(uc.startDate,:OFFSET) AS startDate,CTZ(uc.endDate,:OFFSET) AS endDate FROM networkleads_db.calendarEvents AS uc LEFT JOIN networkleads_db.leadReminders AS lr ON lr.id=uc.reminderId WHERE CTZ(uc.startDate,:OFFSET)>=:fromDate AND CTZ(uc.endDate,:OFFSET)<=:toDate AND uc.isDeleted=0 AND uc.userId=:userId",$binds,$errorVar);
        }else{
            $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
            $query = $GLOBALS['connector']->execute("SELECT uc.*,lr.color,lr.leadId,CTZ(uc.startDate,:OFFSET) AS startDate,CTZ(uc.endDate,:OFFSET) AS endDate FROM networkleads_db.calendarEvents AS uc LEFT JOIN networkleads_db.leadReminders AS lr ON lr.id=uc.reminderId WHERE CTZ(uc.startDate,:OFFSET)>=:fromDate AND CTZ(uc.endDate,:OFFSET)<=:toDate AND uc.isDeleted=0 AND uc.orgId=:orgId",$binds,$errorVar);
        }

        if(!$query){
            return [];
        }else {
            while ($r = $GLOBALS['connector']->fetch($query)) {

                $event = [];
                $event["id"] = $r['id'];
                $event["type"] = "event";
                $event["title"] = $r['title'];
                $event["start"] = date("Y-m-d\TH:i:s", strtotime($r['startDate']));
                $event["end"] = date("Y-m-d\TH:i:s", strtotime($r['endDate']));
                $event["location"] = $r["location"];
                $event["color"] = $r["color"];


                $event["durationEditable"] = true;

                $events[] = $event;
            }

        }
        return $events;

    }
    // ==================================== END getActiveEvents() ======================================

    // ==================================== START getOperationsData() ======================================
    public function getOperationsData($fromDate = NULL,$toDate = NULL){
        if($fromDate === NULL || $toDate === NULL){return false;}

        $errorVar = array("Calendar Class","getOperationsData()",4,"Notes",array());

        $events = [];

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':fromDate', $fromDate, PDO::PARAM_STR);
        $binds[] = array(':toDate', $toDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT l.id,ml.moveDate,l.jobNumber,l.firstname,l.lastname,ml.moveInventory,ml.fromState,ml.toState,CTZ(ml.requestedDeliveryDateStart,:OFFSET) AS requestedDeliveryDateStart,CTZ(ml.boxDeliveryDateStart,:OFFSET) AS boxDeliveryDateStart,CTZ(ml.pickupDateStart,:OFFSET) AS pickupDateStart,CTZ(ml.requestedDeliveryDateEnd,:OFFSET) AS requestedDeliveryDateEnd,CTZ(ml.boxDeliveryDateEnd,:OFFSET) AS boxDeliveryDateEnd,CTZ(ml.pickupDateEnd,:OFFSET) AS pickupDateEnd,ml.includeBoxDeliveryDate,ml.includePickupDate,ml.includeRequestDeliveryDate FROM networkleads_db.leads AS l INNER JOIN networkleads_moving_db.leads AS ml ON ml.leadId=l.id WHERE l.orgId=:orgId AND l.isDeleted=0 AND (ml.status>=3 AND ml.status!=7) AND (((CTZ(ml.requestedDeliveryDateStart,:OFFSET)>=:fromDate AND CTZ(ml.requestedDeliveryDateStart,:OFFSET)<=:toDate) OR (CTZ(ml.boxDeliveryDateStart,:OFFSET)>=:fromDate AND CTZ(ml.boxDeliveryDateStart,:OFFSET)<=:toDate) OR (CTZ(ml.pickupDateStart,:OFFSET)>=:fromDate AND CTZ(ml.pickupDateStart,:OFFSET)<=:toDate)) OR ((CTZ(ml.requestedDeliveryDateEnd,:OFFSET)>=:fromDate AND CTZ(ml.requestedDeliveryDateEnd,:OFFSET)<=:toDate) OR (CTZ(ml.boxDeliveryDateEnd,:OFFSET)>=:fromDate AND CTZ(ml.boxDeliveryDateEnd,:OFFSET)<=:toDate) OR (CTZ(ml.pickupDateEnd,:OFFSET)>=:fromDate AND CTZ(ml.pickupDateEnd,:OFFSET)<=:toDate)))",$binds,$errorVar);
        if(!$getIt){
            return [];
        }else{
            while ($r = $GLOBALS['connector']->fetch($getIt)){

                if($r["jobNumber"] == "" || $r["jobNumber"] == NULL){
                    $title = "#".$r['id'];
                }else{
                    $title = "#".$r['jobNumber'];
                }

                $event = [];

                // Operation Data
                $event["fullName"] = $r["firstname"]." ".$r["lastname"];
                $event["fromState"] = $r["fromState"];
                $event["toState"] = $r["toState"];
                $event["location"] = "";

                if ($r['includeRequestDeliveryDate'] == "1") {

                    $event["id"] = $r['id'];
                    $event["type"] = "operation";
                    $event["title"] = "Delivery - " . $title;
                    $event["start"] = date("Y-m-d H:i:s",strtotime($r['requestedDeliveryDateStart']));
                    $event["end"] = date("Y-m-d H:i:s",strtotime($r['requestedDeliveryDateEnd']));

                    // Style
                    $event["editable"] = false;
                    $event["classNames"] = [];

                    $events[] = $event;
                }
                if ($r['includeBoxDeliveryDate'] == "1") {
                    $event["id"] = $r['id'];
                    $event["type"] = "operation";
                    $event["title"] = "Box Delivery - " . $title;
                    $event["start"] = date("Y-m-d H:i:s",strtotime($r['boxDeliveryDateStart']));
                    $event["end"] = date("Y-m-d H:i:s",strtotime($r['boxDeliveryDateEnd']));

                    // Operation Data
                    $event["fullName"] = $r["firstname"]." ".$r["lastname"];
                    $event["fromState"] = $r["fromState"];
                    $event["toState"] = $r["toState"];

                    // Style
                    $event["editable"] = false;
                    $event["classNames"] = [];

                    $events[] = $event;
                }
                if ($r['includePickupDate'] == "1") {
                    $event["id"] = $r['id'];
                    $event["type"] = "operation";
                    $event["title"] = "Pickup - " . $title;
                    $event["start"] = date("Y-m-d H:i:s",strtotime($r['pickupDateStart']));
                    $event["end"] = date("Y-m-d H:i:s",strtotime($r['pickupDateEnd']));

                    // Operation Data
                    $event["fullName"] = $r["firstname"]." ".$r["lastname"];
                    $event["fromState"] = $r["fromState"];
                    $event["toState"] = $r["toState"];

                    // Style
                    $event["editable"] = false;
                    $event["classNames"] = [];

                    $events[] = $event;
                }

            }
        }

        return $events;
    }
    // ==================================== END getOperationsData() ======================================

    // ==================================== START getHolidays() ======================================
    public function getHolidays($fromDate = NULL,$toDate = NULL){
        if($fromDate === NULL || $toDate === NULL){return false;}

        $errorVar = array("Calendar","getHolidays()",3,"Notes",array(),false);

        $events = [];

        $binds = [];
        $binds[] = array(':fromDate',$fromDate,PDO::PARAM_STR);
        $binds[] = array(':toDate',$toDate,PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);


        $query = $GLOBALS['connector']->execute("SELECT id,holidayDate,holidayName FROM networkleads_db.holidaysCalendar WHERE CTZ(holidayDate,:OFFSET)>=:fromDate AND CTZ(holidayDate,:OFFSET)<=:toDate", $binds, $errorVar);
        if (!$query){
            return [];
        }else{
            while ($r = $GLOBALS['connector']->fetch($query)){
                $event = [];
                $event["id"] = $r['id'];
                $event["type"] = "holiday";
                $event["title"] = $r['holidayName'];
                $event["start"] = date("Y-m-d",strtotime($r['holidayDate']));
                $event["end"] = date("Y-m-d",strtotime($r['holidayDate']));
                $event["allDay"] = true;
                $event["location"] = "";

                // Style
                $event["editable"] = false;

                $events[] = $event;
            }
        }

        return $events;

    }
    // ==================================== END getHolidays() ======================================

    // ==================================== START getBirthdays() ======================================
    public function getBirthdays($fromDate = NULL,$toDate = NULL){
        if($fromDate === NULL || $toDate === NULL){return false;}

        $errorVar = array("Calendar","getBirthdays()",3,"Notes",array(),false);

        $events = [];

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(':fromDate',$fromDate,PDO::PARAM_STR);
        $binds[] = array(':toDate',$toDate,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("SELECT id,fullName,birthDate FROM networkleads_db.users WHERE organizationId=:orgId AND isActive=1 AND isDeleted=0 AND date(CONCAT(YEAR(CURDATE()),'-',month(birthDate),'-',day(birthDate)))>=:fromDate AND date(CONCAT(YEAR(CURDATE()),'-',month(birthDate),'-',day(birthDate)))<=:toDate", $binds, $errorVar);

        if (!$query){
            return [];
        }else{
            while ($r = $GLOBALS['connector']->fetch($query)){

                if ($r['birthDate'] != NULL) {
                    $event = [];
                    $event["id"] = $r['id'];
                    $event["type"] = "birthday";
                    $event["title"] = $r['fullName']."'s Birthday";
                    $event["start"] = date("Y", strtotime("NOW")) . date("-m-d", strtotime($r['birthDate']));
                    $event["end"] = date("Y", strtotime("NOW")) . date("-m-d", strtotime($r['birthDate']));
                    $event["location"] = "";

                    // Style
                    $event["editable"] = false;
                    $event["classNames"] = [];

                    $events[] = $event;
                }
            }
        }

        return $events;

    }
    // ==================================== END getBirthdays() ======================================

    // ==================================== START addEvent() ======================================
    public function addEvent($title,$orgEvent,$orgId,$dateS,$dateE,$eventDescription,$eventLocation){

        $errorVar = array("Calendar","addEvent()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':title',$title,PDO::PARAM_STR);
        $binds[] = array(':dateS',$dateS,PDO::PARAM_STR);
        $binds[] = array(':dateE',$dateE,PDO::PARAM_STR);
        $binds[] = array(':eventDescription',$eventDescription,PDO::PARAM_STR);
        $binds[] = array(':eventLocation',$eventLocation,PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        if ($orgEvent == 1){
            $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
        }else{
            $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        }

        if ($orgEvent == 1){
            $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.calendarEvents (title,description,location,orgId,startDate,endDate) VALUES(:title,:eventDescription,:eventLocation,:orgId,CTZ_TDB(:dateS,:OFFSET),CTZ_TDB(:dateE,:OFFSET))",$binds,$errorVar);
        }else {
            $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.calendarEvents (title,description,location,userId,startDate,endDate) VALUES(:title,:eventDescription,:eventLocation,:userID,CTZ_TDB(:dateS,:OFFSET),CTZ_TDB(:dateE,:OFFSET))",$binds,$errorVar);
        }
        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END addEvent() ======================================

    // ==================================== START updateEvent() ======================================
    public function updateEvent($id,$title,$description,$location,$dateS,$dateE,$orgEvent){

        $errorVar = array("Calendar","updateEvent()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':id',$id,PDO::PARAM_INT);
        $binds[] = array(':title',$title,PDO::PARAM_STR);
        $binds[] = array(':description',$description,PDO::PARAM_STR);
        $binds[] = array(':location',$location,PDO::PARAM_STR);
        $binds[] = array(':dateS',$dateS,PDO::PARAM_STR);
        $binds[] = array(':dateE',$dateE,PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        if ($orgEvent == 1){
            $binds[] = array(':newOrgId',$this->orgId,PDO::PARAM_INT);
            $binds[] = array(':newUserId',NULL,PDO::PARAM_NULL);

            $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
            $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        }else{
            $binds[] = array(':newOrgId',NULL,PDO::PARAM_NULL);
            $binds[] = array(':newUserId',$this->userId,PDO::PARAM_INT);

            $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
            $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        }
        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET title=:title,description=:description,location=:location,startDate=CTZ_TDB(:dateS,:OFFSET),endDate=CTZ_TDB(:dateE,:OFFSET),userId=:newUserId,orgId=:newOrgId WHERE id=:id AND (orgId=:orgId OR userId=:userId)",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END updateEvent() ======================================

    // ==================================== START addSelectEvent() ======================================
    public function addSelectEvent($title,$startDate,$endDate,$orgEvent,$orgId){

        $errorVar = array("Calendar","addSelectEvent()",3,"Notes",array(),false);

        $binds = [];
        if ($orgEvent == '2'){
            $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
        }else{
            $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        }
        $binds[] = array(':dateS',$startDate,PDO::PARAM_STR);
        $binds[] = array(':dateE',$endDate,PDO::PARAM_STR);
        $binds[] = array(':title',$title,PDO::PARAM_INT);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        if ($orgEvent == '2'){
            $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.calendarEvents (title,orgId,startDate,endDate) VALUES(:title,:orgId,CTZ_TDB(:dateS,:OFFSET),CTZ_TDB(:dateE,:OFFSET))",$binds,$errorVar);
        }else{
            $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.calendarEvents (title,userId,startDate,endDate) VALUES(:title,:userID,CTZ_TDB(:dateS,:OFFSET),CTZ_TDB(:dateE,:OFFSET))",$binds,$errorVar);
        }
        if(!$query){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id();
        }

    }
    // ==================================== END addSelectEvent() ======================================

    // ==================================== START setEventDate() ======================================
    public function setEventDate($eventId,$dateS,$dateE,$orgId,$orgEvent){

        $errorVar = array("Calendar","setEventDate()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':eventID',$eventId,PDO::PARAM_INT);
        $binds[] = array(':dateS',$dateS,PDO::PARAM_STR);
        $binds[] = array(':dateE',$dateE,PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);


        if ($orgEvent == "2") {
            $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
            $update = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET startDate=CTZ_TDB(:dateS,:OFFSET),endDate=CTZ_TDB(:dateE,:OFFSET) WHERE id=:eventID AND orgId=:orgId", $binds, $errorVar);
        }else{
            $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
            $update = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET startDate=CTZ_TDB(:dateS,:OFFSET),endDate=CTZ_TDB(:dateE,:OFFSET) WHERE id=:eventID AND userId=:userID", $binds, $errorVar);
        }
        if(!$update){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END setEventDate() ======================================

    // ==================================== START updateEventDate() ======================================
    public function updateEventDate($eventId,$dateS,$dateE,$orgId){

        $errorVar = array("Calendar","updateEventDate()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
        $binds[] = array(':eventID',$eventId,PDO::PARAM_INT);
        $binds[] = array(':dateS',$dateS,PDO::PARAM_STR);
        $binds[] = array(':dateE',$dateE,PDO::PARAM_STR);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        $update = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET startDate=CTZ_TDB(:dateS,:OFFSET),endDate=CTZ_TDB(:dateE,:OFFSET) WHERE id=:eventID AND (userId=:userID OR orgId=:orgId)",$binds,$errorVar);
        if(!$update){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END updateEventDate() ======================================

    // ==================================== START getEventByID() ======================================
    public function getEventByID($id,$orgId,$dateFormatted = false){

        $errorVar = array("Calendar","getActiveEvents()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
        $binds[] = array(':eventID',$id,PDO::PARAM_INT);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        $query = $GLOBALS['connector']->execute("SELECT title,description,location,CTZ(startDate,:OFFSET) AS startDate,CTZ(endDate,:OFFSET) AS endDate,userId,orgId FROM networkleads_db.calendarEvents WHERE id=:eventID AND (userId=:userID OR orgId=:orgId)",$binds,$errorVar);
        if(!$query){
            return false;
        }else{
            if ($dateFormatted){
                $data = $GLOBALS['connector']->fetch($query);
                if ($data['startDate']){
                    $data['formatted_start_date'] = date("m-d-Y h:i:s",strtotime($data['startDate']));
                    $data['startDate'] = date('g:ia \o\n l F jS',strtotime($data['startDate']));
                }
                if ($data['endDate']){
                    $data['formatted_end_date'] = date("m-d-Y h:i:s",strtotime($data['endDate']));
                    $data['endDate'] = date('g:ia \o\n l F jS',strtotime($data['endDate']));
                }
                return $data;
            }else{
                return $GLOBALS['connector']->fetch($query);
            }
        }

    }
    // ==================================== END getEventByID() ======================================

    // ==================================== START deleteEvent() ======================================
    public function deleteEvent($id,$orgId){

        $errorVar = array("Calendar","deleteEvent()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':eventID',$id,PDO::PARAM_INT);
        $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarEvents SET isDeleted=1 WHERE id=:eventID AND (userId=:userID OR orgId=:orgId)",$binds,$errorVar);
        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END deleteEvent() ======================================

}