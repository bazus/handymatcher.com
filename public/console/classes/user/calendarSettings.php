<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/passwords.php");

class calendarSettings{

    protected $userId;
    protected $orgId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = NULL,$orgId = NULL) {
        if($userId === NULL){return;}
        $this->userId = $userId;
        $this->orgId = $orgId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getData() ======================================
    public function getData(){

        $errorVar = array("calendarSettings","getData()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.calendarSettings WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return [];
        }else {
            $r = $GLOBALS['connector']->fetch($getIt);
            return $r;
        }
        return false;
    }
    // ==================================== END getActiveEvents() ======================================

    // ==================================== START checkIfSettingsExists() ======================================
    public function checkIfSettingsExists(){

        $errorVar = array("calendarSettings","checkIfSettingsExists()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_db.calendarSettings WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return [];
        }else {
            if($GLOBALS['connector']->fetch_num_rows($getIt) == 0){
                // Create settings

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.calendarSettings(userId) VALUES(:userId)",$binds,$errorVar);
                if(!$setIt){
                    return false;
                }else{
                    return true;
                }

            }else{
                return true;
            }
        }
        return false;
    }
    // ==================================== END checkIfSettingsExists() ======================================

    // ==================================== START updateGoogleCalendarAccount() ======================================
    public function updateGoogleCalendarAccount($newData = NULL){
        if($newData === NULL){return false;}
        $errorVar = array("userSettings","updateGoogleCalendarAccount()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':googleCalendarSyncAccount', $newData, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarSettings SET googleCalendarSyncAccount=:googleCalendarSyncAccount WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START updateGoogleCalendarAccount() ======================================

    // ==================================== START removeGoogleCalendarSyncAccount() ======================================
    public function removeGoogleCalendarSyncAccount(){

        $errorVar = array("userSettings","removeGoogleCalendarSyncAccount()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':googleCalendarSyncAccount', NULL, PDO::PARAM_NULL);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarSettings SET googleCalendarSyncAccount=:googleCalendarSyncAccount WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START removeGoogleCalendarSyncAccount() ======================================

    // ==================================== START updateCalendarColor() ======================================
    public function updateCalendarColor($calendarId,$color){

        $errorVar = array("userSettings","updateCalendarColor()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':color', $color, PDO::PARAM_STR);

        if($calendarId == "personalCheckbox"){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarSettings SET privateCalendarColor=:color WHERE userId=:id",$binds,$errorVar);
        }else if($calendarId == "organizationCheckbox"){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarSettings SET organizationCalendarColor=:color WHERE userId=:id",$binds,$errorVar);
        }else if($calendarId == "operationsCheckbox"){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarSettings SET operationsCalendarColor=:color WHERE userId=:id",$binds,$errorVar);
        }else if($calendarId == "holidays"){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.calendarSettings SET holidaysCalendarColor=:color WHERE userId=:id",$binds,$errorVar);
        }
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START updateCalendarColor() ======================================
    // ==================================== START getCalendarColor() ======================================
    public function getCalendarColor($calendarId){

        $errorVar = array("calendarSettings","getCalendarColor()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);

        if($calendarId == "personalCheckbox"){
            $getIt = $GLOBALS['connector']->execute("SELECT privateCalendarColor FROM networkleads_db.calendarSettings WHERE userId=:userId",$binds,$errorVar);
        }else if($calendarId == "organizationCheckbox"){
            $getIt = $GLOBALS['connector']->execute("SELECT organizationCalendarColor FROM networkleads_db.calendarSettings WHERE userId=:userId",$binds,$errorVar);
        }else if($calendarId == "operationsCheckbox"){
            $getIt = $GLOBALS['connector']->execute("SELECT operationsCalendarColor FROM networkleads_db.calendarSettings WHERE userId=:userId",$binds,$errorVar);
        }else if($calendarId == "holidays"){
            $getIt = $GLOBALS['connector']->execute("SELECT holidaysCalendarColor FROM networkleads_db.calendarSettings WHERE userId=:userId",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else {
            $r = $GLOBALS['connector']->fetch($getIt);
            return array_values($r);
        }
        return false;
    }
    // ==================================== END getCalendarColor() ======================================

}