<?php


class userReports
{
    private $orgId = NULL;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    public function getUserLoginReport($dateStart = "",$dateEnd = "",$getFullData = true,$byUserId = false){

        // Data to collect
        $collectedData = [];

        $collectedData['status'] = false;
        $collectedData['data'] = [];
        $collectedData['calc'] = [];

        $errorVar = array("userReports Class","getUserLoginReport()",5,"Notes",array());

        $binds = [];

        $binds[] = [":orgId",$this->orgId,PDO::PARAM_INT];
        $binds[] = [":sDate",date("Y-m-d 00:00:00",strtotime($dateStart)),PDO::PARAM_STR];
        $binds[] = [":eDate",date("Y-m-d 23:59:59",strtotime($dateEnd)),PDO::PARAM_STR];

        // Check if to look for by user id
        if ($byUserId){

            $binds[] = [":userId",$byUserId,PDO::PARAM_INT];
            $getIt = $GLOBALS['connector']->execute("SELECT l.loginTime,l.logoutTime,u.id,u.fullName FROM networkleads_db.logins AS l INNER JOIN networkleads_db.users AS u ON u.id=l.userId WHERE l.loginTime>=:sDate AND l.loginTime<=:eDate AND u.organizationId=:orgId AND u.id=:userId", $binds, $errorVar);

        }else {

            $getIt = $GLOBALS['connector']->execute("SELECT l.loginTime,l.logoutTime,u.id,u.fullName FROM networkleads_db.logins AS l INNER JOIN networkleads_db.users AS u ON u.id=l.userId WHERE l.loginTime>=:sDate AND l.loginTime<=:eDate AND u.organizationId=:orgId", $binds, $errorVar);

        }

        if (!$getIt){
            $collectedData['status'] = false;
            return $collectedData;
        }else{

            $collectedData['status'] = true;

            while ($data = $GLOBALS['connector']->fetch($getIt)){
                $collectedData['data'][] = $data;
            }

            $collectedData['calc'] = $this->calcData($getFullData,$collectedData['data']);

            if (!$getFullData){
                unset($collectedData['data']);
            }

            return $collectedData;
        }

    }

    public function calcData($getFullData,$data){
        if (count($data) == 0){return false;}

        $collectedData = [];

        foreach ($data AS $userData){

            if (!isset($collectedData[$userData['id']]['fullName'])) {
                $collectedData[$userData['id']]['fullName'] = $userData['fullName'];
            }

            if (isset($collectedData[$userData['id']]['count'])){
                $collectedData[$userData['id']]['count']++;
            }else{
                $collectedData[$userData['id']]['count'] = 1;
            }

            if ($getFullData) {
                if ($userData['logoutTime']) {
                    $collectedData[$userData['id']]['userLogins'][date("m-d-Y", strtotime($userData['loginTime']))][] = (strtotime($userData['logoutTime']) - strtotime($userData['loginTime'])) / 3600;
                } else {
                    $collectedData[$userData['id']]['userLogins'][date("m-d-Y", strtotime($userData['loginTime']))][] = 0;
                }
            }

            if ($userData['logoutTime']) {
                if (isset($collectedData[$userData['id']]['totalLoginTime'])){
                    $collectedData[$userData['id']]['totalLoginTime'] += (strtotime($userData['logoutTime']) - strtotime($userData['loginTime'])) / 3600;
                }else{
                    $collectedData[$userData['id']]['totalLoginTime'] = (strtotime($userData['logoutTime']) - strtotime($userData['loginTime'])) / 3600;
                }
            }else{
                if (!isset($collectedData[$userData['id']]['totalLoginTime'])) {
                    $collectedData[$userData['id']]['totalLoginTime'] = 0;
                }
            }

        }

        // Format the final numbers
        foreach ($collectedData AS &$myData){
            $myData['totalLoginTime'] = number_format($myData['totalLoginTime'],2);

            if ($getFullData) {
                foreach ($myData['userLogins'] AS &$userLogins){
                    $total = 0;
                    for ($i = 0;$i<count($userLogins);$i++){
                        $total += $userLogins[$i];
                    }
                    $userLogins = number_format($total,2);
                }
            }
        }

        return $collectedData;
    }

    public function topUsersLoggedIn($dateS,$dateE){

        $errorVar = array("userReports Class","topUsersLoggedIn()",5,"Notes",array());

        $binds = [];
        $binds[] = [':dateS',$dateS,PDO::PARAM_STR];
        $binds[] = [':dateE',$dateE,PDO::PARAM_STR];
        $users = [];

        $getIt = $GLOBALS['connector']->execute("SELECT l.id,l.userId,l.loginTime,u.fullName,org.organizationName FROM networkleads_db.logins AS l INNER JOIN networkleads_db.users AS u ON u.id=l.userId INNER JOIN networkleads_db.organizations AS org ON org.id=u.organizationId WHERE l.loginTime>=:dateS AND l.loginTime<=:dateE AND u.id!=360 AND u.id!=191 AND u.id!=407", $binds, $errorVar);
        if (!$getIt){
            return false;
        }else{
            while ($data = $GLOBALS['connector']->fetch($getIt)){
                if(isset($users[$data['userId']])){
                    $users[$data['userId']] = ['total'=>$users[$data['userId']]['total'] +1,'name'=>$data['fullName'],'organizationName'=>$data['organizationName']];
                }else{
                    $users[$data['userId']] = ['total'=> 1,'name'=>$data['fullName']];
                }
            }
            usort($users,'sortByTotal');
            $users = array_slice($users,0,20);
            return $users;
        }
    }

    public function topOrganizationsEstimates($dateS,$dateE){

        $errorVar = array("userReports Class","topOrganizationsEstimates()",5,"Notes",array());

        $orgData = [];

        $getIt = $GLOBALS['connector']->execute("SELECT id,organizationName FROM networkleads_db.organizations AS org WHERE org.isDeleted=0 AND org.id!=127 AND org.id!=223", NULL, $errorVar);
        if (!$getIt){
            return false;
        }else{
            $data = [];
            while ($data = $GLOBALS['connector']->fetch($getIt)){

                $binds = [];
                $binds[] = [':dateS',$dateS,PDO::PARAM_STR];
                $binds[] = [':dateE',$dateE,PDO::PARAM_STR];
                $binds[] = [':orgId',$data['id'],PDO::PARAM_STR];

                $getLeads = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads AS l INNER JOIN networkleads_moving_db.estimateCalculations AS ml ON ml.leadId=l.id WHERE l.orgId=:orgId AND l.dateReceived>=:dateS AND l.dateReceived<=:dateE AND l.isDeleted=0", $binds, $errorVar);
                if ($getLeads){
                    $orgData[] = ['name'=>$data['organizationName'],'totalEstimates'=>$GLOBALS['connector']->fetch_num_rows($getLeads)];
                }
            }
            usort($orgData,'sortByTotalEstimates');
            $orgData = array_slice($orgData,0,20);
            return $orgData;
        }
    }

    public function topOrganizationsLeads($dateS,$dateE){

        $errorVar = array("userReports Class","topOrganizationsLeads()",5,"Notes",array());

        $orgData = [];

        $getIt = $GLOBALS['connector']->execute("SELECT id,organizationName FROM networkleads_db.organizations AS org WHERE org.isDeleted=0 AND org.id!=127 AND org.id!=223", NULL, $errorVar);
        if (!$getIt){
            return false;
        }else{
            $data = [];
            while ($data = $GLOBALS['connector']->fetch($getIt)){

                $binds = [];
                $binds[] = [':dateS',$dateS,PDO::PARAM_STR];
                $binds[] = [':dateE',$dateE,PDO::PARAM_STR];
                $binds[] = [':orgId',$data['id'],PDO::PARAM_STR];

                $getLeads = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads AS l WHERE l.orgId=:orgId AND l.dateReceived>=:dateS AND l.dateReceived<=:dateE AND l.isDeleted=0", $binds, $errorVar);
                if ($getLeads){
                    $orgData[] = ['name'=>$data['organizationName'],'totalLeads'=>$GLOBALS['connector']->fetch_num_rows($getLeads)];
                }
            }
            usort($orgData,'sortByTotalLeads');
            $orgData = array_slice($orgData,0,20);
            return $orgData;
        }
    }

}
