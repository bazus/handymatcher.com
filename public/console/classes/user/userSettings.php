<?php
require_once $_SERVER['LOCAL_NL_PATH']."/console/mobile/mobile.php"; // Require the mobile code



/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2018 Niv Apo
 * @license   ---
 */


class userSettings{


    private $userId;
    private $isFromMobile = false;
    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = ""){
        $this->userId = $userId;
        $this->isFromMobile = (strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobile') || strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android'));
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START createSettings() ======================================
    public function createSettings($timeZone = NULL) {

        $errorVar = array("userSettings","getUserSettings()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.userSettings WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) == 0) {
                $binds = array();
                $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

                if(($timeZone === NULL || $timeZone == "" || $timeZone == false) || ($timeZone != "America/New_York" && $timeZone != "America/Chicago" && $timeZone != "America/Denver" && $timeZone != "America/Los_Angeles" && $timeZone != "Asia/Jerusalem" )){
                    $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.userSettings(userId) VALUES(:userId)", $binds, $errorVar);
                }else{
                    $binds[] = array(':timeZone', $timeZone, PDO::PARAM_STR);
                    $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.userSettings(userId, localTimeZone) VALUES(:userId, :timeZone)", $binds, $errorVar);
                }
                if (!$setIt) {
                    return false;
                } else {
                    return true;
                }
            }
        }


        return false;
    }
    // ==================================== START createSettings() ======================================

    // ==================================== START getUserIdBySession() ======================================
    public function getUserSettings() {

        $errorVar = array("userSettings","getUserSettings()",5,"Notes",array(),false);

        // This will check if settings exists and if not - it will create them
        $this->createSettings();

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.userSettings WHERE userId=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $settings = $GLOBALS['connector']->fetch($getIt);
            return $settings;
        }


        return false;
    }
    // ==================================== START getUserIdBySession() ======================================

    // ==================================== START toggle_openSideBar() ======================================
    public function toggle_openSideBar() {
        $data = array();
        $data['status'] = false;
        $data['option'] = 0;

        $errorVar = array("userSettings","toggle_openSideBar()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET openSideBar= NOT openSideBar WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT openSideBar FROM networkleads_db.userSettings WHERE userId=:id",$binds,$errorVar);
            $r = $GLOBALS['connector']->fetch($getIt);

            $data["status"] = true;
            $data["option"] = $r['openSideBar'];
            return $data;
        }

        return $data;
    }
    // ==================================== START toggle_openSideBar() ======================================

}