<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/organization/organization.php");

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (30.6.18) 2018 Niv Apo
 * @license   ---
 */


// =================== THE INTERCOM CLASS SUPPORTS PHP 7.1+ (!) ===================
//
//
// use Intercom\IntercomClient;
// $intercom = new IntercomClient($_SERVER["INTERCOM_KEY"]);
//
//
//

class intercom{

    private $userId = NULL;
    private $orgId = NULL;
    private $intercom = NULL;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = NULL,$orgId = NULL,$intercom = NULL) {
        $this->userId = $userId;
        $this->orgId = $orgId;
        $this->intercom = $intercom;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START createUser() ======================================
    public function createUser() {

        $user = new user($this->userId);
        $userData = $user->getData();

            $create = $this->intercom->users->create([
                "user_id" => $this->userId,
                "email" => $userData["email"],
                "name" => $userData["fullName"],
                "phone" => $userData["phoneNumber"],
                "companies" => [
                    [
                        "company_id" => $this->orgId
                    ]
                ]
            ]);

        return true;
    }
    // ==================================== START createUser() ======================================

    // ==================================== START createCompany() ======================================
    public function createCompany() {

        $organization = new organization($this->orgId);
        $organizationData = $organization->getData();


        $create = $this->intercom->companies->create([
            "name" => $organizationData["organizationName"],
            "id" => $organizationData["id"]
        ]);

        return $create->id;
    }
    // ==================================== START createCompany() ======================================
}