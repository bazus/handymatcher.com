<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (4.7.18) 2018 Niv Apo
 * @license   ---
 */


class userManagement{


    private $organizationId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($organizationId) {
        $this->organizationId = $organizationId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getOrganizationData() ======================================
    public function getOrganizationData() {

        $errorVar = array("userManagement Class","getOrganizationData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,organizationName,createdAt FROM networkleads_db.organizations WHERE id=:organizationId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            return $r;
        }

        return false;
    }
    // ==================================== START getOrganizationData() ======================================

    // ==================================== START getTotalUsersInOrganization() ======================================
    public function getTotalUsersInOrganization() {

        $errorVar = array("userManagement Class","getTotalUsersInOrganization()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE organizationId=:organizationId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $total = $GLOBALS['connector']->fetch_num_rows($getIt);
            return $total;
        }

        return false;
    }
    // ==================================== START getTotalUsersInOrganization() ======================================

    // ==================================== START getUsersOfMyCompany() ======================================
    public function getUsersOfMyCompany($userId = "") {

        $errorVar = array("userManagement Class","getUsersOfMyCompany()",4,"Notes",array());

            $binds = array();
            $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
            $binds[] = array(':userId', $userId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,jobTitle,email,phoneCountryCodeState,phoneNumber,signUpDate,isActive,isAdmin,isDeleted,revokedDate FROM networkleads_db.users WHERE organizationId=:organizationId AND id!=:userId",$binds,$errorVar);
            if(!$getIt){
                return false;
            }else{
                $users = array();
                while($user = $GLOBALS['connector']->fetch($getIt)){
                    // check if user is revoked
                    if($user['isDeleted'] == 1){

                        $dateRange = date("Y-m-d",strtotime("-1 month"));
                        // check if revoke last more than 1 month
                        if($user['revokedDate'] >= $dateRange){

                            $userClass = new user($user["id"]);
                            $countryCode = $userClass->convertCountryCode($user["phoneCountryCodeState"]);
                            $user["phoneCountryCode"] = $countryCode;
                            $users[] = $user;

                        }
                    }else{
                        
                        $userClass = new user($user["id"]);
                        $countryCode = $userClass->convertCountryCode($user["phoneCountryCodeState"]);
                        $user["phoneCountryCode"] = $countryCode;
                        $users[] = $user;

                    }
                }
                return $users;

            }

        return false;
    }
    // ==================================== END getUsersOfMyCompany() ======================================

    // ==================================== START revokeUser() ======================================
    public function revokeUser($userId,$type) {

        $errorVar = array("userManagement Class","revokeUser()",4,"Notes",array());

            $binds = array();
            $binds[] = array(':dType', $type, PDO::PARAM_INT);
            $binds[] = array(':userId', $userId, PDO::PARAM_STR);

            $getIt = $GLOBALS['connector']->execute("UPDATE users SET isDeleted=:dType, revokedDate=NOW() WHERE id=:userId",$binds,$errorVar);

            if(!$getIt){
                return false;
            }else{
                return true;
            }

    }
    // ==================================== END revokeUser() ======================================

}