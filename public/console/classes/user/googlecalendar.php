<?php

class googlecalendar{

    protected $userId;
    protected $orgId;
    protected $service;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = NULL,$orgId = NULL,$service = NULL) {
        if($userId === NULL){return;}
        $this->userId = $userId;
        $this->orgId = $orgId;
        $this->service = $service;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getEvents() ======================================
    public function getEvents($fromDate = NULL,$toDate = NULL,$calendarId = "primary"){
        if($fromDate === NULL || $toDate === NULL){return false;}

        $calendarData = $this->getCalendarById($calendarId);


        $optParams = array(
            'maxResults' => 300,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date("c", strtotime($fromDate)),
            'timeMax' => date("c", strtotime($toDate))
        );

        $results = $this->service->events->listEvents($calendarId, $optParams);
        $googleevents = $results->getItems();

        $events = [];
        foreach($googleevents as $googleevent){

            $event = [];
            $event["id"] = $calendarId;
            $event["type"] = "custom";
            $event["title"] = $googleevent['summary'];
            $event["color"] = $calendarData->backgroundColor;
//            var_dump($events);

            // Google data
            $event["htmlLink"] = $googleevent['htmlLink'];

            if(isset($googleevent['start']['date']) && $googleevent['start']['date'] != NULL){
                $event["start"] = date("Y-m-d",strtotime($googleevent['start']['date']));
            }
            if(isset($googleevent['start']['dateTime']) && $googleevent['start']['dateTime'] != NULL){
                $event["start"] = date("Y-m-d",strtotime($googleevent['start']['dateTime']));
            }

            if(isset($googleevent['end']['date']) && $googleevent['end']['date'] != NULL){
                $event["end"] = date("Y-m-d",strtotime($googleevent['end']['date']));
            }
            if(isset($googleevent['end']['dateTime']) && $googleevent['end']['dateTime'] != NULL){
                $event["end"] = date("Y-m-d",strtotime($googleevent['end']['dateTime']));
            }

            // Style
            $event["editable"] = false;
            $event["className"] = ["googleCalendar"];

            $events[] = $event;
        }

        return $events;
    }
    // ==================================== END getEvents() ======================================

    // ==================================== START getCalendars() ======================================
    public function getCalendars(){

        $calendarList = $this->service->calendarList->listCalendarList();

        $calendars = [];

        foreach ($calendarList as $singleCalendar){

            $calendarItem = [];
            $calendarItem["id"] = $singleCalendar->id;
            $calendarItem["name"] = $singleCalendar->summary;
            $calendarItem["backgroundColor"] = $singleCalendar->backgroundColor;
            $calendarItem["foregroundColor"] = $singleCalendar->foregroundColor;
            $calendarItem["timeZone"] = $singleCalendar->timeZone;

            $calendars[] = $calendarItem;
        }
        return $calendars;
    }
    // ==================================== END getCalendars() ======================================

    // ==================================== START getCalendarById() ======================================
    public function getCalendarById($calendarId = NULL){

        if($calendarId === NULL){return false;}

        return $this->service->calendarList->get($calendarId);

    }
    // ==================================== END getCalendarById() ======================================

    // ==================================== START createCalendar() ======================================
    public function createCalendar(){

        $calendar = new Google_Service_Calendar_Calendar();
        $calendar->setSummary('Network Leads');
        $calendar->setTimeZone('America/Los_Angeles');

        $createdCalendar = $this->service->calendars->insert($calendar);

        return $createdCalendar->getId();

    }
    // ==================================== END createCalendar() ======================================

}