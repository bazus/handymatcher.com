<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");


/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (4.10.18) 2018 Niv Apo
 * @license   ---
 */


class phoneVerification{

    protected $userId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = "") {
        $this->userId = $userId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START isPhoneVerified() ======================================
    public function isPhoneVerified($phoneNumber = ""){

        $errorVar = array("phoneVerification","isPhoneVerified()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':phoneNumber',$phoneNumber,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM verifiedPhoneNumbers WHERE userId=:userId AND phoneNumber=:phoneNumber AND isVerified=1",$binds,$errorVar);
        if(!$query){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($query) > 0) {
                return true;
            }
        }

        return false;
    }
    // ==================================== END isPhoneVerified() ======================================

    // ==================================== START createVerificationCode() ======================================
    public function createVerificationCode($phoneNumber = ""){

        $result = array();
        $result["status"] = false;
        $result["code"] = "";

        if($this->didSendVerificationInLast3Minutes($phoneNumber) != false){return $result;}

        $errorVar = array("phoneVerification","createVerificationCode()",3,"Notes",array(),false);

        $six_digit_random_number = mt_rand(100000, 999999);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':phoneNumber',$phoneNumber,PDO::PARAM_STR);
        $binds[] = array(':verificationCode',$six_digit_random_number,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("INSERT INTO verifiedPhoneNumbers(userId,phoneNumber,verificationCode,sentVerificationTime) VALUES (:userId,:phoneNumber,:verificationCode,NOW())",$binds,$errorVar);
        if(!$query){
            $result["status"] = false;
        }else{
            $result["status"] = true;
            $result["code"] = $six_digit_random_number;
        }

        return $result;
    }
    // ==================================== END createVerificationCode() ======================================

    // ==================================== START checkVerificationCode() ======================================
    public function verifyPhone($phoneNumber = "",$code = ""){

        $errorVar = array("phoneVerification","verifyPhone()",3,"Notes",array(),false);

        $last3minutes = date("Y-m-d H:i:s",strtotime("-3 minutes"));

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':phoneNumber',$phoneNumber,PDO::PARAM_STR);
        $binds[] = array(':verificationCode',$code,PDO::PARAM_STR);
        $binds[] = array(':sentVerificationTime',$last3minutes,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM verifiedPhoneNumbers WHERE userId=:userId AND phoneNumber=:phoneNumber AND verificationCode=:verificationCode AND sentVerificationTime>=:sentVerificationTime",$binds,$errorVar);
        if(!$query){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($query) > 0) {
                $query = $GLOBALS['connector']->execute("UPDATE verifiedPhoneNumbers SET isVerified=1,verifiedDate=NOW() WHERE userId=:userId AND phoneNumber=:phoneNumber AND verificationCode=:verificationCode AND sentVerificationTime>=:sentVerificationTime",$binds,$errorVar);
                if(!$query){
                    return false;
                }else {

                    $user = new user($this->userId);
                    $userData = $user->getData();

                    $organization = new organization($userData["organizationId"]);
                    $organizationData = $organization->getData();

                    $msg = $userData["fullName"]." from ".$organizationData["organizationName"]." verified their phone number";
                    shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => $msg))) . " > /dev/null 2>/dev/null &");
                    return true;
                }
            }else{
                return false;
            }
        }

        return false;
    }
    // ==================================== END checkVerificationCode() ======================================


    // ==================================== START didSendVerificationInLast3Minutes() ======================================
    public function didSendVerificationInLast3Minutes($phoneNumber = ""){

        $errorVar = array("phoneVerification","didSendVerificationInLast3Minutes()",3,"Notes",array(),false);

        $last3minutes = date("Y-m-d H:i:s",strtotime("-3 minutes"));

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':phoneNumber',$phoneNumber,PDO::PARAM_STR);
        $binds[] = array(':sentVerificationTime',$last3minutes,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM verifiedPhoneNumbers WHERE userId=:userId AND phoneNumber=:phoneNumber AND sentVerificationTime>=:sentVerificationTime",$binds,$errorVar);
        if(!$query){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($query) > 0) {
                return true;
            }else{
                return false;
            }
        }

        return NULL;
    }
    // ==================================== END didSendVerificationInLast3Minutes() ======================================

}