<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/passwords.php");


/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright 2018 Niv Apo
 * @license   ---
 */


class user{

    public $isValid = false; // is user is under my organization. Check __construct
    protected $userId;
    protected $userData = NULL;

    // ==================================== START CONSTRUCTOR ======================================
    function __construct($userId = "",$skipValid = false) {
        $this->userId = $userId;

        if ($skipValid){
            $this->isValid = true;
        }else {
            if (isset($_SESSION)) {

                $myUserId = $_SESSION["userId"];

                $errorVar = array("user class", "__construct()", 5, "Notes", array(), false);

                $binds = array();
                $binds[] = array(':userId', $myUserId, PDO::PARAM_INT);

                $getMyOrgId = $GLOBALS['connector']->execute("SELECT id,organizationId FROM networkleads_db.users WHERE id=:userId", $binds, $errorVar);
                if (!$getMyOrgId) {
                    return false;
                } else {
                    $r = $GLOBALS['connector']->fetch($getMyOrgId);
                    $myOrgId = $r["organizationId"];

                    $binds = array();
                    $binds[] = array(':userId', $userId, PDO::PARAM_INT);
                    $binds[] = array(':organizationId', $myOrgId, PDO::PARAM_INT);

                    $getUser = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE id=:userId AND organizationId=:organizationId", $binds, $errorVar);
                    if (!$getUser) {
                        return false;
                    } else {
                        $totalUsers = $GLOBALS['connector']->fetch_num_rows($getUser);
                        if ($totalUsers == 1) {
                            $this->isValid = true;
                        }

                    }

                }
            }
        }
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getUserDataBySession() ======================================
    public function getData() {

        if($this->isValid != true) {
            return false;
        }

        $errorVar = array("user class","getData()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getUserData = $GLOBALS['connector']->execute("SELECT dep.title AS departmentTitle,users.id,users.isAdmin,users.welcomeMode,users.fullName,users.email,users.organizationId,users.phoneNumber,users.userTypeId,users.secretKey,users.isActive,users.isVerified,users.phoneCountryCodeState,users.isDeleted,org.organizationName,org.organizationTypeId,users.jobTitle,users.revokedDate,users.depId,users.comment,users.signUpDate,users.profilePicture,users.welcomeMode,org.organizationPackage,users.birthDate,users.didRegisterByGoogle,ugh.googleAccessToken,ugh.googleAccessToken,ugh.email AS googleCalendarEmail FROM networkleads_db.users AS users INNER JOIN networkleads_db.organizations AS org ON users.organizationId=org.id LEFT JOIN networkleads_db.departments AS dep ON dep.id=users.depId LEFT JOIN networkleads_db.userGoogleAuth AS ugh ON ugh.userId=users.id WHERE users.id=:userId ORDER BY users.id DESC LIMIT 1",$binds,$errorVar);
        if(!$getUserData){
            return false;
        }else {
            $theUserData = $GLOBALS['connector']->fetch($getUserData);
            $theUserData["phoneCountryCode"] = $this->convertCountryCode($theUserData["phoneCountryCodeState"]);

            $this->userData = $theUserData;

            return $theUserData;
        }

        return false;
    }
    // ==================================== END getUserDataBySession() ======================================

    // ==================================== START convertCountryCode() ======================================
    public function convertCountryCode($country = "")
    {
        $countryCodes = array(
            "AF"=> "+93",
            "AL"=> "+355",
            "DZ"=> "+213",
            "AS"=> "+1",
            "AD"=> "+376",
            "AO"=> "+244",
            "AI"=> "+1",
            "AG"=> "+1",
            "AR"=> "+54",
            "AM"=> "+374",
            "AW"=> "+297",
            "AU"=> "+61",
            "AT"=> "+43",
            "AZ"=> "+994",
            "BS"=> "+1",
            "BH"=> "+973",
            "BD"=> "+880",
            "BB"=> "+1",
            "BY"=> "+375",
            "BE"=> "+32",
            "BZ"=> "+501",
            "BJ"=> "+229",
            "BM"=> "+1",
            "BT"=> "+975",
            "BO"=> "+591",
            "BA"=> "+387",
            "BW"=> "+267",
            "BR"=> "+55",
            "IO"=> "+246",
            "BN"=> "+673",
            "BG"=> "+359",
            "BF"=> "+226",
            "BI"=> "+257",
            "KH"=> "+855",
            "CM"=> "+237",
            "CA"=> "+1",
            "CV"=> "+238",
            "KY"=> "+1",
            "CF"=> "+236",
            "TD"=> "+235",
            "CL"=> "+56",
            "CN"=> "+86",
            "CO"=> "+57",
            "KM"=> "+269",
            "CK"=> "+682",
            "CR"=> "+506",
            "CI"=> "+225",
            "HR"=> "+385",
            "CU"=> "+53",
            "CW"=> "+599",
            "CY"=> "+357",
            "CZ"=> "+420",
            "DK"=> "+45",
            "DJ"=> "+253",
            "DM"=> "+1",
            "DO"=> "+1",
            "EC"=> "+593",
            "EG"=> "+20",
            "SV"=> "+503",
            "GQ"=> "+240",
            "ER"=> "+291",
            "EE"=> "+372",
            "ET"=> "+251",
            "FK"=> "+500",
            "FO"=> "+298",
            "FJ"=> "+679",
            "FI"=> "+358",
            "FR"=> "+33",
            "GF"=> "+594",
            "PF"=> "+689",
            "GA"=> "+241",
            "GM"=> "+220",
            "GE"=> "+995",
            "DE"=> "+49",
            "GH"=> "+233",
            "GI"=> "+350",
            "GR"=> "+30",
            "GL"=> "+299",
            "GD"=> "+1",
            "GP"=> "+590",
            "GU"=> "+1",
            "GT"=> "+502",
            "HT"=> "+509",
            "HN"=> "+504",
            "HK"=> "+852",
            "HU"=> "+36",
            "IS"=> "+354",
            "IN"=> "+91",
            "ID"=> "+62",
            "IR"=> "+98",
            "IQ"=> "+964",
            "IE"=> "+353",
            "IL"=> "+972",
            "IT"=> "+39",
            "JM"=> "+1",
            "JP"=> "+81",
            "JO"=> "+962",
            "KZ"=> "+7",
            "KE"=> "+254",
            "KI"=> "+686",
            "KW"=> "+965",
            "KG"=> "+996",
            "LV"=> "+371",
            "LB"=> "+961",
            "LS"=> "+266",
            "LR"=> "+231",
            "LY"=> "+218",
            "LI"=> "+423",
            "LT"=> "+370",
            "LU"=> "+352",
            "MO"=> "+853",
            "MK"=> "+389",
            "MG"=> "+261",
            "MW"=> "+265",
            "MY"=> "+60",
            "MV"=> "+960",
            "ML"=> "+223",
            "MT"=> "+356",
            "MH"=> "+692",
            "MQ"=> "+596",
            "MR"=> "+222",
            "MU"=> "+230",
            "MX"=> "+52",
            "FM"=> "+691",
            "MD"=> "+373",
            "MC"=> "+377",
            "MN"=> "+976",
            "ME"=> "+382",
            "MS"=> "+1",
            "MA"=> "+212",
            "MZ"=> "+258",
            "MM"=> "+95",
            "NA"=> "+264",
            "NR"=> "+674",
            "NP"=> "+977",
            "NL"=> "+31",
            "NC"=> "+687",
            "NZ"=> "+64",
            "NI"=> "+505",
            "NE"=> "+227",
            "NG"=> "+234",
            "NU"=> "+683",
            "NF"=> "+672",
            "NO"=> "+47",
            "OM"=> "+968",
            "PK"=> "+92",
            "PW"=> "+680",
            "PA"=> "+507",
            "PG"=> "+675",
            "PY"=> "+595",
            "PE"=> "+51",
            "PH"=> "+63",
            "PL"=> "+48",
            "PT"=> "+351",
            "PR"=> "+1",
            "QA"=> "+974",
            "RE"=> "+262",
            "RO"=> "+40",
            "RU"=> "+7",
            "RW"=> "+250",
            "WS"=> "+685",
            "SM"=> "+378",
            "ST"=> "+239",
            "SA"=> "+966",
            "SN"=> "+221",
            "RS"=> "+381",
            "SC"=> "+248",
            "SL"=> "+232",
            "SG"=> "+65",
            "SX"=> "+1",
            "SK"=> "+421",
            "SI"=> "+386",
            "SB"=> "+677",
            "SO"=> "+252",
            "ZA"=> "+27",
            "SS"=> "+211",
            "ES"=> "+34",
            "LK"=> "+94",
            "SD"=> "+249",
            "SR"=> "+597",
            "SZ"=> "+268",
            "SE"=> "+46",
            "CH"=> "+41",
            "SY"=> "+963",
            "TW"=> "+886",
            "TJ"=> "+992",
            "TZ"=> "+255",
            "TH"=> "+66",
            "TL"=> "+670",
            "TG"=> "+228",
            "TK"=> "+690",
            "TO"=> "+676",
            "TT"=> "+1",
            "TN"=> "+216",
            "TR"=> "+90",
            "TM"=> "+993",
            "TC"=> "+1",
            "TV"=> "+688",
            "UG"=> "+256",
            "UA"=> "+380",
            "AE"=> "+971",
            "GB"=> "+44",
            "US"=> "+1",
            "UY"=> "+598",
            "UZ"=> "+998",
            "VU"=> "+678",
            "VE"=> "+58",
            "VN"=> "+84",
            "WF"=> "+681",
            "YE"=> "+967",
            "ZM"=> "+260",
            "ZW"=> "+263");
        if(isset($countryCodes[$country])) {
            return $countryCodes[$country];
        }
        return NULL;
    }
    // ==================================== START convertCountryCode() ======================================

    // ==================================== START changePassword() ======================================
    public function changePassword($newPassword = "")
    {

        if($this->isValid != true) {
            return false;
        }

        $passwords = new passwords();
        $newPassword = $passwords->hashPass($newPassword);

        $errorVar = array("User","changePassword()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userPass', $newPassword, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET userPass=:userPass WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START changePassword() ======================================

    // ==================================== START changeUserPassword() ======================================
    public function changeUserPassword($newPassword = "")
    {
        if($this->isValid != true) {
            return false;
        }

        if(is_null($this->userData)){
            $this->getData();
        }
        if ($this->checkIfUserIsOwner()){
            return false;
        }

        $passwords = new passwords();
        $newPassword = $passwords->hashPass($newPassword);

        $errorVar = array("User","changePassword()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userPass', $newPassword, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->userData['organizationId'], PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET userPass=:userPass WHERE id=:id AND organizationId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START changePassword() ======================================

    // ==================================== START makeAnAdmin() ======================================
    public function makeAnAdmin(){
        if($this->isValid != true) {
            return false;
        }
        if(is_null($this->userData)){
            $this->getData();
        }

        $errorVar = array("User","makeAnAdmin()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->userData['organizationId'], PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET isAdmin=1 WHERE id=:id AND organizationId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START changePassword() ======================================

    // ==================================== START makeAnAdmin() ======================================
    public function unsetAnAdmin(){
        if($this->isValid != true) {
            return false;
        }
        if(is_null($this->userData)){
            $this->getData();
        }
        if ($this->checkIfUserIsOwner()){
            return false;
        }

        $errorVar = array("User","unsetAnAdmin()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->userData['organizationId'], PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET isAdmin=0 WHERE id=:id AND organizationId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START changePassword() ======================================

    // ==================================== START updateFullName() ======================================
    public function updateFullName($newFullName = ""){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':fullName', $newFullName, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET fullName=:fullName WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }


        return false;
    }
    // ==================================== START updateFullName() ======================================

    // ==================================== START updatePhone() ======================================
    public function updatePhone($phoneCountryCodeState = "", $phoneNumber = "")
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':phoneCountryCodeState', $phoneCountryCodeState, PDO::PARAM_STR);
        $binds[] = array(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET phoneCountryCodeState=:phoneCountryCodeState,phoneNumber=:phoneNumber WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updatePhone() ======================================

    // ==================================== START updateBirthdate() ======================================
    public function updateBirthdate($birthDate)
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':birthDate', $birthDate, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET birthDate=:birthDate WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateBirthdate() ======================================

    // ==================================== START updateHelpMode() ======================================
    public function updateHelpMode($helpMode)
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':helpMode', $helpMode, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET helpMode=:helpMode WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateHelpMode() ======================================

    // ==================================== START showleadGoogleMaps() ======================================
    public function showleadGoogleMaps($leadGoogleMaps)
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':showMapsInLeads', $leadGoogleMaps, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET showMapsInLeads=:showMapsInLeads WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END showleadGoogleMaps() ======================================

    // ==================================== START updateReceiveWeeklyReportByEmail() ======================================
    public function updateReceiveWeeklyReportByEmail($weeklyEmail){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':receiveWeeklyEmail', $weeklyEmail, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET receiveWeeklyEmail=:receiveWeeklyEmail WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateReceiveWeeklyReportByEmail() ======================================

    // ==================================== START updateLocalTimeZone() ======================================
    public function updateLocalTimeZone($localTimeZoneOffset = "America/New_York")
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':localTimeZone', $localTimeZoneOffset, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET localTimeZone=:localTimeZone WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            // Update the cookie
            setcookie('localTimeZone', $localTimeZoneOffset, time() + (30 * 86400 * 30), "/"); // 86400 = 1 day
            return true;
        }

        return false;
    }
    // ==================================== END updateLocalTimeZone() ======================================

    // ==================================== START updateHelpMode() ======================================
    public function updateFirstTutorial($enable)
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","updateFirstTutorial()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':enable', $enable, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET firstTutorial=:enable WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateHelpMode() ======================================

    // ==================================== START updateemailnewlead() ======================================
    public function updateemailnewlead($isOn = "false",$email = "")
    {
        if($this->isValid != true) {
            return false;
        }

        if($isOn == "false"){
            $isOn = "0";
        }else{
            $isOn = "1";
        }

        $errorVar = array("Title","updateemailnewlead()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':EMAILWhenNewLeadArrives', $email, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET receiveEMAILWhenNewLeadArrives=".$isOn.",EMAILWhenNewLeadArrives=:EMAILWhenNewLeadArrives WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateemailnewlead() ======================================

    // ==================================== START updatesmsnewlead() ======================================
    public function updatesmsnewlead($isOn = "false")
    {
        if($this->isValid != true) {
            return false;
        }

        if($isOn == "false"){
            $isOn = "0";
        }else{
            $isOn = "1";
        }

        $errorVar = array("Title","Description",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET receiveSMSWhenNewLeadArrives=".$isOn." WHERE userId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updatesmsnewlead() ======================================

    // ==================================== START updateJobTitle() ======================================
    public function updateJobTitle($newJobTitle = "")
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","updateJobTitle()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':jobTitle', $newJobTitle, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET jobTitle=:jobTitle WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateJobTitle() ======================================

    // ==================================== START updateDepartment() ======================================
    public function updateDepartment($newDepartmentId = "")
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","updateDepartment()",5,"Notes",array(),false);

        $binds = array();
        if($newDepartmentId == ""){
            $binds[] = array(':depId', NULL, PDO::PARAM_STR);
        }else{
            $binds[] = array(':depId', $newDepartmentId, PDO::PARAM_STR);
        }
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET depId=:depId WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateDepartment() ======================================

    // ==================================== START updateDepartment() ======================================
    public function updateUserType($newUserType = NULL)
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","updateDepartment()",5,"Notes",array(),false);

        $binds = array();
        if($newUserType == ""){
            $binds[] = array(':userType', NULL, PDO::PARAM_STR);
        }else{
            $binds[] = array(':userType', $newUserType, PDO::PARAM_STR);
        }
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET userTypeId=:userType WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateDepartment() ======================================

    // ==================================== START updateuserSalesBonusPerc() ======================================
    public function updateSalesBonusPerc($salesBonusPerc = 0)
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","updateuserSalesBonusPerc()",5,"Notes",array(),false);

        $binds = array();
        if($salesBonusPerc == ""){
            $binds[] = array(':salesBonusPerc', 0, PDO::PARAM_STR);
        }else{
            $binds[] = array(':salesBonusPerc', $salesBonusPerc, PDO::PARAM_STR);
        }
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET salesBonusPerc=:salesBonusPerc WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateDepartment() ======================================

    // ==================================== START updateComments() ======================================
    public function updateComments($newComments = "")
    {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","updateComments()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':comment', $newComments, PDO::PARAM_STR);
        $binds[] = array(':id', $this->userId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET comment=:comment WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateComments() ======================================

    // ==================================== START getUserFolders() ======================================
    public function getUserFolders($folderId){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","getUserFolders()",3,"Notes",array(),false);

        $data = array();
        $folders = [];

        $binds = array();
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':folderId',$folderId,PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM userFolders WHERE user_id=:userID AND is_deleted=0 AND parent_folder=:folderId ORDER BY folder_id ASC",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            while($folder = $GLOBALS['connector']->fetch($getIt)){
                $folder['create_date'] = date("F j, Y",strtotime($folder['create_date']));
                $folders[] = $folder;
            }
            $data["folders"] = $folders;
        }
        // get parent id
        $getIt = $GLOBALS['connector']->execute("SELECT * FROM userFolders WHERE user_id=:userID AND is_deleted=0 AND folder_id=:folderId",$binds,$errorVar);
        if(!$getIt){

        }else{
            $res = $GLOBALS['connector']->fetch($getIt);
            $data["parentFolderId"] = $res['parent_folder'];
            $data['parentFolder'] = $res;
        }

        return $data;


    }
    // ==================================== END getUserFolders() ======================================

    // ==================================== START getUserFiles() ======================================
    public function getUserFiles($userID,$folder){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","getUserFiles()",3,"Notes",array(),false);

        $files = [];

        $binds = array();
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);
        $binds[] = array(':folder',$folder,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("SELECT * FROM usersFiles WHERE user_id=:userID AND folder_id=:folder AND is_deleted=0 ORDER BY id ASC",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            while($file = $GLOBALS['connector']->fetch($query)){
                $file['date_created'] = date("F j, Y",strtotime($file['date_created']));
                $files[] = $file;
            }
            return $files;
        }


    }
    // ==================================== END getUserFiles() ======================================

    // ==================================== START getUserFilesByAdmin() ======================================
    public function getUserFilesByAdmin($orgId,$userId){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","getUserFilesByAdmin()",3,"Notes",array(),false);

        $files = [];

        $binds = array();
        $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
        $binds[] = array(':userId',$userId,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("SELECT uf.* FROM usersFiles AS uf INNER JOIN networkleads_db.users AS u on u.id=uf.user_id WHERE u.organizationId=:orgId AND uf.user_id=:userId AND is_deleted=0 ORDER BY id ASC",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            while($file = $GLOBALS['connector']->fetch($query)){
                $file['date_created'] = date("F j, Y",strtotime($file['date_created']));
                $files[] = $file;
            }
            return $files;
        }


    }
    // ==================================== END getUserFilesByAdmin() ======================================

    // ==================================== START setImageFolder() ======================================
    public function setImageFolder($userID,$folder,$file){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","setImageFolder()",3,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);
        $binds[] = array(':folder',$folder,PDO::PARAM_INT);
        $binds[] = array(':file',$file,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("UPDATE usersFiles SET folder_id=:folder WHERE user_id=:userID AND id=:file",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }


    }
    // ==================================== END setImageFolder() ======================================

    // ==================================== START setImageFolder() ======================================
    public function setFolderName($id,$name){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","setFolderName()",3,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':id',$id,PDO::PARAM_INT);
        $binds[] = array(':folderName',$name,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.userFolders SET folder_name=:folderName WHERE user_id=:userID AND id=:id",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }


    }
    // ==================================== END setImageFolder() ======================================

    // ==================================== START getRootFiles() ======================================
    public function getRootFiles($userID){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","getRootFiles()",3,"Notes",array(),false);

        $files = [];

        $binds = array();
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("SELECT * FROM usersFiles WHERE user_id=:userID AND folder_id=0 AND is_deleted=0 ORDER BY folder_id ASC",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            while($file = $GLOBALS['connector']->fetch($query)){
                $file['date_created'] = date("F j, Y",strtotime($file['date_created']));

                // check if file exists
                if( file_exists($_SERVER['LOCAL_NL_PATH']."/console/files/user/".$file['file_url'] ) ){}else{
                    $file['file_url'] = "no-image.png";
                    $file['file_error'] = true;
                }

                $files[] = $file;
            }

            return $files;
        }


    }
    // ==================================== END getRootFiles() ======================================

    // ==================================== START addFolder() ======================================
    public function addFolder($userID,$folderName,$parentFolder = 0){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","addFolder()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);

        $ids = [];

        $query = $GLOBALS['connector']->execute("SELECT folder_id FROM userFolders WHERE user_id=:userID ORDER BY folder_id DESC",$binds,$errorVar);

        if(!$query){

        }else{
            while ($id = $GLOBALS['connector']->fetch($query)){
                $ids[] = $id;
            }
        }

        if(isset($ids) && isset($ids[0]['folder_id'])){
            $newID = $ids[0]['folder_id']+1;
        }else{
            $newID = 1;
        }

        $date = date("Y-m-d H:i:s",strtotime("now"));

        $binds = [];
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);
        $binds[] = array(':folderName',$folderName,PDO::PARAM_STR);
        $binds[] = array(':folderID',$newID,PDO::PARAM_INT);
        $binds[] = array(':todayDate',$date,PDO::PARAM_STR);
        $binds[] = array(':pFolder',$parentFolder,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("INSERT INTO userFolders (user_id,folder_id,folder_name,create_date,is_deleted,parent_folder) VALUES(:userID,:folderID,:folderName,:todayDate,0,:pFolder)",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id($query);
        }


    }
    // ==================================== END addFolder() ======================================

    // ==================================== START removeFolder() ======================================
    public function removeFolder($userID,$folderID){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","removeFolder()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);
        $binds[] = array(':folderID',$folderID,PDO::PARAM_INT);


        $query = $GLOBALS['connector']->execute("UPDATE userFolders SET is_deleted=1 WHERE user_id=:userID AND id=:folderID",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END removeFolder() ======================================

    // ==================================== START addFile() ======================================
    public function addFile($userID,$fileType,$fileName,$orginalName,$folderId){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","addFile()",3,"Notes",array(),false);

        $date = date("Y-m-d H:i:s",strtotime("now"));

        $binds = [];
        $binds[] = array(':userID',$userID,PDO::PARAM_INT);
        $binds[] = array(':fileType',$fileType,PDO::PARAM_STR);
        $binds[] = array(':fileName',$fileName,PDO::PARAM_STR);
        $binds[] = array(':orgName',$orginalName,PDO::PARAM_STR);
        $binds[] = array(':todayDate',$date,PDO::PARAM_STR);
        $binds[] = array(':folderId',$folderId,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("INSERT INTO usersFiles (user_id,file_url,date_created,folder_id,file_type,is_deleted,file_name) VALUES(:userID,:fileName,:todayDate,:folderId,:fileType,0,:orgName)",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END addFile() ======================================

    // ==================================== START changeProfileImage() ======================================
    public function changeProfileImage($fileName){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","changeProfileImage()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':fileName',$fileName,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET profilePicture=:fileName WHERE id=:userId",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END changeProfileImage() ======================================

    // ==================================== START removeFile() ======================================
    public function removeFile($fileID){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Files","removeFolder()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':fileID',$fileID,PDO::PARAM_INT);


        $query = $GLOBALS['connector']->execute("UPDATE usersFiles SET is_deleted=1 WHERE user_id=:userID AND id=:fileID",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END removeFile() ======================================

    // ==================================== START disableWelcomeMode() ======================================
    public function disableWelcomeMode(){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("User Class","disableWelcomeMode()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.users SET welcomeMode=0 WHERE id=:userID",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END disableWelcomeMode() ======================================

    // ==================================== START turnOffFirstTutorial() ======================================
    public function turnOffFirstTutorial(){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("User Class","turnOffFirstTutorial()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userID',$this->userId,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.userSettings SET firstTutorial=0 WHERE userId=:userID",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END turnOffFirstTutorial() ======================================

    // ==================================== START getTotalUsersActivityByDate() ======================================
    public function getTotalUsersActivityByDate($orgId,$compare = false,$sDate = false,$eDate = false){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("User Class","getTotalUsersActivityByDate()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.logins AS l INNER JOIN networkleads_db.users AS u ON u.id=l.userId WHERE u.organizationId=:orgId AND l.loginTime>=:startDate AND l.loginTime<=:endDate",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                $totalThisMonth = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalUsersActivity'] = $totalThisMonth;
                $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                $eDate = date("Y-m-d 23:59:59",strtotime("Today -1 Month"));

                $binds = [];
                $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.logins AS l INNER JOIN networkleads_db.users AS u ON u.id=l.userId WHERE u.organizationId=:orgId AND l.loginTime>=:startDate AND l.loginTime<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalLastMonth = $GLOBALS['connector']->fetch_num_rows($getIt);
                    if ($totalLastMonth == 0 && $totalThisMonth >= 0){
                        $data['totalUsersActivityChange'] = 100;
                    }else{
                        $data['totalUsersActivityChange'] = ($totalThisMonth/$totalLastMonth)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }
    // ==================================== End getTotalUsersActivityByDate() ======================================

    // ==================================== START getUserDataByKey() ======================================
    public function getUserDataByKey($key){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("user class","getUserDataByKey()",5,"Notes",array(),false);

        $binds = array();
        $binds[] = array(':secretKey', $key, PDO::PARAM_STR);

        $getUserData = $GLOBALS['connector']->execute("SELECT u.fullName,u.email,u.phoneNumber FROM networkleads_db.users AS u INNER JOIN networkleads_db.newUserInvitations AS nui ON nui.userIdInvited=u.id WHERE nui.invitationKey=:secretKey LIMIT 1",$binds,$errorVar);
        if(!$getUserData){
            return false;
        }else {
            $data = $GLOBALS['connector']->fetch($getUserData);
            return $data;
        }

        return false;
    }
    // ==================================== END getUserDataByKey() ======================================

    // ==================================== START didPasswordVerifySuccess() ======================================
    public function didPasswordVerifySuccess($password,$returnUserId = false) {
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array());
        $data = [];
        $data['status'] = false;
        $data['valid'] = false;
        $data['isRevoked'] = false;

        $passwords = new passwords();
        $clean_password = $passwords->hashPass($password);

        $userId = $this->userId;

        $checkCredentials = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE id='".$userId."' AND (userPass='".$clean_password."' OR userPass='".$password."')",NULL,$errorVar);
        if(!$checkCredentials || $GLOBALS['connector']->fetch_num_rows($checkCredentials) != 1){
            return false;
        }else{
            $data['status'] = true;
            $checkValid = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.users WHERE id='".$userId."' AND (userPass='".$clean_password."' OR userPass='".$password."') AND isDeleted=0 AND isActive=1",NULL,$errorVar);
            if($GLOBALS['connector']->fetch_num_rows($checkValid) != 1){
                $data['isRevoked'] = true;
                return $data;
            }else{
                $data['valid'] = true;
                if($returnUserId){
                    $checkCredentials = $GLOBALS['connector']->execute("SELECT id FROM networkleads_db.users WHERE id='".$userId."' AND (userPass='".$clean_password."' OR userPass='".$password."') AND isDeleted=0",NULL,$errorVar);

                    $userData = $GLOBALS['connector']->fetch($checkCredentials);
                    $data['id'] = $userData["id"];
                    return $data;
                }else{
                    return $data;
                }
            }
        }
        return false;
    }
    // ==================================== END didPasswordVerifySuccess() ======================================

    // ==================================== END checkIfUserIsOwner() ======================================
    private function checkIfUserIsOwner(){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array());

        $binds = [];
        $binds[] = [':orgId',$this->userData['organizationId'],PDO::PARAM_INT];
        $binds[] = [':userId',$this->userId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.organizations WHERE id=:orgId AND userIdCreated=:userId",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }else{
                return false;
            }
        }
    }
    // ==================================== END checkIfUserIsOwner() ======================================

    // ==================================== END checkIfUserSignupWithGoogle() ======================================
    public function checkIfUserSignupWithGoogle(){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array());

        $binds = [];
        $binds[] = [':userId',$this->userId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT didRegisterByGoogle FROM networkleads_db.users WHERE id=:userId",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            $didRegisterByGoogle = $GLOBALS['connector']->fetch($getIt);
            if($didRegisterByGoogle['didRegisterByGoogle'] == 1){
                return true;
            }else{
                return false;
            }

        }
    }
    // ==================================== END checkIfUserSignupWithGoogle() ======================================

    // ==================================== END checkIfUserSignupWithGoogleAndPasswordIsNull() ======================================
    public function checkIfUserSignupWithGoogleAndPasswordIsNull(){
        if($this->isValid != true) {
            return false;
        }
        $errorVar = array("Title","Description",5,"Notes",array());

        $binds = [];
        $binds[] = [':userId',$this->userId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT didRegisterByGoogle,userPass FROM networkleads_db.users WHERE id=:userId",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            $userData = $GLOBALS['connector']->fetch($getIt);
            if($userData['didRegisterByGoogle'] == 1){
                if ($userData['userPass'] == NULL){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }

        }
    }
    // ==================================== END checkIfUserSignupWithGoogleAndPasswordIsNull() ======================================

    // ==================================== END getOrgId() ======================================
    public function getOrgId(){
        if($this->isValid != true) {
            return false;
        }

        if($this->userData === NULL){
            $this->getData();
        }

        return $this->userData["organizationId"];

    }
    // ==================================== END getOrgId() ======================================
    public function setFeatureRequest($featureDescription,$featurePhone,$filesAttached){

        $errorVar = array("user class","setFeatureRequest()",5,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':userId',$this->userId,PDO::PARAM_INT);
        $binds[] = array(':featureDescription',$featureDescription,PDO::PARAM_STR);
        $binds[] = array(':featurePhone',$featurePhone,PDO::PARAM_STR);
        if(!is_array($filesAttached)){
            $filesAttached = [];
        }else{
            $filesAttached = serialize($filesAttached);
        }
        $binds[] = array(':filesAttached', $filesAttached, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.featureRequests (userId,featureDescription,phone,filesAttached) VALUES(:userId,:featureDescription,:featurePhone,:filesAttached)",$binds,$errorVar);

        if(!$setIt){
            return false;
        }else{

            $user = new user($this->userId,true);
            $userData = $user->getData();

            $organization = new organization($userData["organizationId"]);
            $organizationData = $organization->getData();

            $msg = "New feature request from [" . $userData["fullName"] . " from ".$organizationData["organizationName"]."]";
            shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => $msg))) . ""); // > /dev/null 2>/dev/null &
            return true;
        }

    }
}