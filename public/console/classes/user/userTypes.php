<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 2018-12-26
 * Time: 20:05
 */

class userTypes
{
    public function getData(){
        $errorVar = array("userTypes Class","getData()",4,"Notes",array());

        $userTypes = [];

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.userTypes",NULL,$errorVar);

        if(!$getIt){
            return false;
        }else{
            while($res = $GLOBALS['connector']->fetch($getIt)){
                $userTypes[] = $res;
            }
        }

        return $userTypes;
    }
}