<?php

class smsTemplates
{

    private $userId;
    private $orgId;

    function __construct($userId,$orgId)
    {
        $this->userId = $userId;
        $this->orgId = $orgId;
    }

    function getTemplates(){

        // Check if ever had any templates
        $this->addDefaultTemplates();

        $errorVar = array("smsTemplates Class","getTemplates()",4,"Notes",array());

        $templates = [];

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,userId,text FROM networkleads_db.smsTemplates WHERE userId=:userId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while($template = $GLOBALS['connector']->fetch($getIt,true)){
                $templates[] = $template;
            }
        }

        return $templates;
    }

    function addDefaultTemplates(){
        $errorVar = array("smsTemplates Class","addDefaultTemplates()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id FROM networkleads_db.smsTemplates WHERE userId=:userId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $total =  $GLOBALS['connector']->fetch_num_rows($getIt);
            if($total == 0){
                $this->updateTemplate(0,"Hi [FirstName] Thank you for chosing [CompanyName] for your move. If you could find the time to review our service it would be very much appreciated.");
                $this->updateTemplate(0,"Hi [FirstName], we have a great move crew in [FromCity], [FromState] to get a great deal please follow this link to update your details. [inventorylink]");
            }
        }

        return false;
    }

    function updateTemplate($templateId,$text = ""){

        $errorVar = array("smsTemplates Class","updateTemplate()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':templateId', $templateId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.smsTemplates WHERE userId=:userId AND id=:templateId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $total =  $GLOBALS['connector']->fetch_num_rows($getIt);

            if($total == 0){
                // Create
                $binds = array();
                $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
                $binds[] = array(':text', $text, PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.smsTemplates(userId,text) VALUES(:userId,:text)",$binds,$errorVar);
                if(!$setIt){
                    return false;
                }else{
                    return true;
                }

            }else{
                // Update
                $binds = array();
                $binds[] = array(':id', $templateId, PDO::PARAM_INT);
                $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
                $binds[] = array(':text', $text, PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.smsTemplates SET text=:text WHERE id=:id AND userId=:userId",$binds,$errorVar);
                if(!$setIt){
                    return false;
                }else{
                    return true;
                }
            }
        }

        return false;
    }

    function deleteTemplate($templateId){

        $errorVar = array("smsTemplates Class","deleteTemplatedeleteTemplate()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':templateId', $templateId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.smsTemplates SET isDeleted=1 WHERE userId=:userId AND id=:templateId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
           return true;
        }

        return false;
    }

}