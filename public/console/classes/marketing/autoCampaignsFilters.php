<?php

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");

class autoCampaignsFilters
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getFilterData($autoResponseFilterDataId){

        $errorVar = array("autoCampaignsFilters Class","getFilterData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $autoResponseFilterDataId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.autoCampaignsFilters WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $data = $GLOBALS['connector']->fetch($getIt);
            return $data;
        }

        return false;

    }

    function createLeadFilters($filters = []){

        $errorVar = array("autoCampaignsFilters Class","createLeadFilters()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $columns = [];
        for($i = 0;$i<count($filters);$i++){
            $filter = $filters[$i];

            if($filter["key"] == "fromCity"){
                $columns[] = "fromCity";
                $binds[] = array(':fromCity', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':fromCityType', $filter["type"], PDO::PARAM_INT);
            }

            if($filter["key"] == "fromState"){
                $columns[] = "fromState";
                $binds[] = array(':fromState', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':fromStateType', $filter["type"], PDO::PARAM_INT);
            }

            if($filter["key"] == "fromZip"){
                $columns[] = "fromZip";
                $binds[] = array(':fromZip', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':fromZipType', $filter["type"], PDO::PARAM_INT);
            }

            if($filter["key"] == "toCity"){
                $columns[] = "toCity";
                $binds[] = array(':toCity', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':toCityType', $filter["type"], PDO::PARAM_INT);
            }

            if($filter["key"] == "toState"){
                $columns[] = "toState";
                $binds[] = array(':toState', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':toStateType', $filter["type"], PDO::PARAM_INT);
            }

            if($filter["key"] == "toZip"){
                $columns[] = "toZip";
                $binds[] = array(':toZip', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':toZipType', $filter["type"], PDO::PARAM_INT);
            }
            if($filter["key"] == "leadProvider"){
                $columns[] = "leadProviderId";
                $binds[] = array(':leadProviderId', $filter["value"], PDO::PARAM_STR);
                $binds[] = array(':leadProviderIdType', $filter["type"], PDO::PARAM_INT);
            }
        }

        if(!in_array("fromCity",$columns)){
            $binds[] = array(':fromCity', NULL, PDO::PARAM_NULL);
            $binds[] = array(':fromCityType', NULL, PDO::PARAM_NULL);
        }
        if(!in_array("fromState",$columns)){
            $binds[] = array(':fromState', NULL, PDO::PARAM_NULL);
            $binds[] = array(':fromStateType', NULL, PDO::PARAM_NULL);
        }
        if(!in_array("fromZip",$columns)){
            $binds[] = array(':fromZip', NULL, PDO::PARAM_NULL);
            $binds[] = array(':fromZipType', NULL, PDO::PARAM_NULL);
        }
        if(!in_array("toCity",$columns)){
            $binds[] = array(':toCity', NULL, PDO::PARAM_NULL);
            $binds[] = array(':toCityType', NULL, PDO::PARAM_NULL);
        }
        if(!in_array("toState",$columns)){
            $binds[] = array(':toState', NULL, PDO::PARAM_NULL);
            $binds[] = array(':toStateType', NULL, PDO::PARAM_NULL);
        }
        if(!in_array("toZip",$columns)){
            $binds[] = array(':toZip', NULL, PDO::PARAM_NULL);
            $binds[] = array(':toZipType', NULL, PDO::PARAM_NULL);
        }
        if(!in_array("leadProviderId",$columns)){
            $binds[] = array(':leadProviderId', NULL, PDO::PARAM_NULL);
            $binds[] = array(':leadProviderIdType', NULL, PDO::PARAM_NULL);
        }

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.autoCampaignsFilters(orgId,fromCity,fromCityType,fromState,fromStateType,fromZip,fromZipType,toCity,toCityType,toState,toStateType,toZip,toZipType,leadProviderId,leadProviderIdType) VALUES(:orgId,:fromCity,:fromCityType,:fromState,:fromStateType,:fromZip,:fromZipType,:toCity,:toCityType,:toState,:toStateType,:toZip,:toZipType,:leadProviderId,:leadProviderIdType)",$binds,$errorVar);
        if(!$setIt){
            var_dump($GLOBALS['connector']->getLastError());
            return false;
        }else{
            $filtersId = $GLOBALS['connector']->last_insert_id($setIt);
            return $filtersId;
        }

        return false;
    }

    function doesFilterMatch($leadId,$autoResponseFilterDataId){

        $isFiltersGood = true;

        $filterData = $this->getFilterData($autoResponseFilterDataId);
        if($filterData == false){return true;}

        $lead = new lead($leadId,$this->orgId);
        $leadData = $lead->getData();

        $movingLead = new movingLead($leadId,$this->orgId);
        $movingLeadData = $movingLead->getData();

        foreach($filterData as $k=>$v){

            $v = strtolower($v);

            // From City
            if($k == "fromCity" && $v != NULL){
                if(strtolower($movingLeadData["fromCity"]) != $v){
                    $isFiltersGood = false;
                }
            }

            // From State
            if($k == "fromState" && $v != NULL){
                if(strtolower($movingLeadData["fromState"]) != $v){
                    $isFiltersGood = false;
                }
            }

            // From Zip
            if($k == "fromZip" && $v != NULL){
                if(strtolower($movingLeadData["fromZip"]) != $v){
                    $isFiltersGood = false;
                }
            }

            // To City
            if($k == "toCity" && $v != NULL){
                if(strtolower($movingLeadData["toCity"]) != $v){
                    $isFiltersGood = false;
                }
            }

            // To State
            if($k == "toState" && $v != NULL){
                if(strtolower($movingLeadData["toState"]) != $v){
                    $isFiltersGood = false;
                }
            }

            // To Zip
            if($k == "toZip" && $v != NULL){
                if(strtolower($movingLeadData["toZip"]) != $v){
                    $isFiltersGood = false;
                }
            }

            // Provider
            if($k == "providerId" && $v != NULL){
                if(strtolower($leadData["providerId"]) != $v){
                    $isFiltersGood = false;
                }
            }


        }

        return $isFiltersGood;

    }

}