<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/marketing/autoCampaignsFilters.php");

class autoCampaigns
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getCampaigns($leadId = NULL,$actionType = NULL,$isActiveOnly = false){

        $campaigns = [];

        $errorVar = array("autoCampaigns Class","getCampaigns()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        if($actionType === NULL){
            if($isActiveOnly){
                $getIt = $GLOBALS['connector']->execute("SELECT id,isActive,orgId,name,isEmail,fromEmailAccountId,emailTemplateId,isSMS,fromPhoneNumberId,smsMessage,actionType,daysAfter,leadFilterDataId FROM networkleads_db.autoCampaigns WHERE orgId=:orgId AND isDeleted=0 AND isActive=1",$binds,$errorVar);
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT id,isActive,orgId,name,isEmail,fromEmailAccountId,emailTemplateId,isSMS,fromPhoneNumberId,smsMessage,actionType,daysAfter,leadFilterDataId FROM networkleads_db.autoCampaigns WHERE orgId=:orgId AND isDeleted=0",$binds,$errorVar);
            }
        }else{
            $binds[] = array(':actionType', $actionType, PDO::PARAM_INT);
            if($isActiveOnly){
                $getIt = $GLOBALS['connector']->execute("SELECT id,isActive,orgId,name,isEmail,fromEmailAccountId,emailTemplateId,isSMS,fromPhoneNumberId,smsMessage,actionType,daysAfter,leadFilterDataId FROM networkleads_db.autoCampaigns WHERE orgId=:orgId AND isDeleted=0 AND actionType=:actionType AND isActive=1",$binds,$errorVar);
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT id,isActive,orgId,name,isEmail,fromEmailAccountId,emailTemplateId,isSMS,fromPhoneNumberId,smsMessage,actionType,daysAfter,leadFilterDataId FROM networkleads_db.autoCampaigns WHERE orgId=:orgId AND isDeleted=0 AND actionType=:actionType",$binds,$errorVar);
            }
        }
        if(!$getIt){
            return false;
        }else{
            $autoCampaignsFilters = new autoCampaignsFilters($this->orgId);
            while($r = $GLOBALS['connector']->fetch($getIt)){
                if($r["leadFilterDataId"] != null && $r["leadFilterDataId"] != "" && $leadId != NULL){
                    $doesFilterMatch = $autoCampaignsFilters->doesFilterMatch($leadId,$r["leadFilterDataId"]);
                   if($doesFilterMatch){
                       $campaigns[] = $r;
                    }
                }else{
                    $campaigns[] = $r;
                }
            }
        }

        return $campaigns;
    }

    function createAutoCampaign($campaignName,$fromPhoneNumberId,$smsTextMessage,$fromEmailAccountId,$emailTemplateId,$actionType,$daysAfter = 0,$leadFilterDataId = NULL){


        $errorVar = array("autoCampaigns Class","createAutoCampaign()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $campaignName, PDO::PARAM_STR);

        // Email data
        if($fromEmailAccountId == "" || $fromEmailAccountId == null || $emailTemplateId == "" || $emailTemplateId == null){
            $binds[] = array(':isEmail', 0, PDO::PARAM_INT);
            $binds[] = array(':fromEmailAccountId', NULL, PDO::PARAM_NULL);
            $binds[] = array(':emailTemplateId', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':isEmail', 1, PDO::PARAM_INT);
            $binds[] = array(':fromEmailAccountId', $fromEmailAccountId, PDO::PARAM_INT);
            $binds[] = array(':emailTemplateId', $emailTemplateId, PDO::PARAM_INT);
        }

        // SMS data
        if($fromPhoneNumberId == "" || $fromPhoneNumberId == null || $smsTextMessage == "" || $smsTextMessage == null){
            $binds[] = array(':isSMS', 0, PDO::PARAM_INT);
            $binds[] = array(':fromPhoneNumberId', NULL, PDO::PARAM_NULL);
            $binds[] = array(':smsMessage', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':isSMS', 1, PDO::PARAM_INT);
            $binds[] = array(':fromPhoneNumberId', $fromPhoneNumberId, PDO::PARAM_INT);
            $binds[] = array(':smsMessage', $smsTextMessage, PDO::PARAM_STR);
        }

        $binds[] = array(':actionType', $actionType, PDO::PARAM_INT);
        $binds[] = array(':daysAfter', $daysAfter, PDO::PARAM_INT);
        $binds[] = array(':leadFilterDataId', $leadFilterDataId, PDO::PARAM_INT);


        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.autoCampaigns(orgId,name,isEmail,isSMS,fromPhoneNumberId,smsMessage,fromEmailAccountId,emailTemplateId,actionType,daysAfter,leadFilterDataId,isActive) VALUES(:orgId,:name,:isEmail,:isSMS,:fromPhoneNumberId,:smsMessage,:fromEmailAccountId,:emailTemplateId,:actionType,:daysAfter,:leadFilterDataId,1)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function deleteCampaign($campaignId){


        $errorVar = array("autoCampaigns Class","deleteCampaign()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $campaignId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.autoCampaigns SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function pauseCampaign($campaignId){

        $errorVar = array("autoCampaigns Class","pauseCampaign()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $campaignId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.autoCampaigns SET isActive=0 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function activateCampaign($campaignId){

        $errorVar = array("autoCampaigns Class","pauseCampaign()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $campaignId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.autoCampaigns SET isActive=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

}