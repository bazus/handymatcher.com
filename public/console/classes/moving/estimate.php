<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/inventory/inventory.php");


class estimate
{
    private $leadId;
    private $estimateId;
    private $estimateData = NULL;

    function __construct($estimateId = NULL,$leadId = NULL){

        // Either $estimateId OR $leadId is REQUIRED!
        $this->extractIds($estimateId,$leadId);
    }

    /*
    public function extractIds($estimateId = NULL,$leadId = NULL)
    {

        // The fullowing code extracts the "id" or "leadId" of an estimate based on available parameters ($estimateId or $leadId);
        if ($estimateId != NULL) {
            $this->estimateId = $estimateId;

            $errorVar = array("estimate Class", "__construct()", 4, "Notes", array());

            $binds = [];
            $binds[] = array(':id', $estimateId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates WHERE id=:id", $binds, $errorVar);
            if (!$getIt) {
                return false;
            } else {
                if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                    $getIt = $GLOBALS['connector']->execute("SELECT id,leadId FROM networkleads_moving_db.estimates WHERE id=:id", $binds, $errorVar);
                    $r = $GLOBALS['connector']->fetch($getIt);

                    $this->leadId = $r["leadId"];
                }
            }
        } elseif ($leadId != NULL) {
            $this->leadId = $leadId;

            $errorVar = array("estimate Class", "__construct()", 4, "Notes", array());

            $binds = [];
            $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates WHERE leadId=:leadId", $binds, $errorVar);
            if (!$getIt) {
                return false;
            } else {
                if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                    $getIt = $GLOBALS['connector']->execute("SELECT id,leadId FROM networkleads_moving_db.estimates WHERE leadId=:leadId", $binds, $errorVar);
                    $r = $GLOBALS['connector']->fetch($getIt);

                    $this->estimateId = $r["id"];
                }
            }
        }
    }

    public function checkIfEstimateExists($ifNotThenCreate = false){

        $response = array();
        $response["isExists"] = false;

        // Check if there is an estimate, if not, create one.
        $errorVar = array("estimate Class","checkIfEstimateExists()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates WHERE leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $totalEstimate = $GLOBALS['connector']->fetch_num_rows($getIt);
            if($totalEstimate == 0){
                if($ifNotThenCreate == true) {
                    $this->createEstimate();
                }
            }else{
                $response["isExists"] = true;
            }
        }

        return $response;

    }

    public function getData(){
        $errorVar = array("estimate Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates as e INNER JOIN networkleads_db.leads as l ON l.id=e.leadId WHERE e.leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {

                $getIt = $GLOBALS['connector']->execute("SELECT e.* FROM networkleads_moving_db.estimates as e INNER JOIN networkleads_db.leads AS l ON l.id=e.leadId WHERE e.leadId=:leadId", $binds, $errorVar);

                $data = $GLOBALS['connector']->fetch($getIt);
                $data['moveInventory'] = unserialize($data['moveInventory']);

                if (!isset($data['bindingType']) || $data['bindingType'] == "null"){$data['bindingType'] = null;}
                $this->estimateData = $data;
                return $data;
            }
        }

        return false;
    }

    public function createEstimate(){

        $result = array();
        $result["status"] = false;
        $result["estimateId"] = "";

        $errorVar = array("estimate Class","createEstimate()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.estimates (leadId,status,requestedDeliveryDateStart) VALUES(:leadId,1,NOW())",$binds,$errorVar);
        if(!$setIt){
            $result["status"] = false;
        }else{
            $result["status"] = true;
            $result["newEstimate"] = true;
            $result["estimateId"] = $GLOBALS['connector']->last_insert_id();

            $this->extractIds(NULL,$this->leadId);
        }

        return $result;
    }

    public function updateEstimate($items){
        $this->checkIfEstimateExists(true);

        // Updates the items in estimate inventory
        $errorVar = array("estimate Class","updateEstimate()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $this->estimateId, PDO::PARAM_INT);
        $binds[] = array(':items', $items, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimates SET moveInventory=:items WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateEstimateDetails($estimateData){
        $this->checkIfEstimateExists(true);

        $errorVar = array("estimate Class","updateEstimateDetails()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $this->estimateId, PDO::PARAM_INT);
        foreach ($estimateData as $k=>$v){
            $binds[] = [":".$k,$v,PDO::PARAM_STR];
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimates SET needStorage=:needStorage,departmentId=:departmentId,priority=:priority,bindingType=:bindingType,reference=:reference,boxDeliveryDate=:boxDeliveryDate,pickupDate=:pickupDate,requestedDeliveryDateStart=:requestedDeliveryDateStart,estimator=:estimator,didVisit=:didVisit WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function getTotalCF($orgId){

        $total = 0;

        if($this->estimateData == NULL){
            $estimateData = $this->getData();
        }else{
            $estimateData = $this->estimateData;
        }

        $inventory = new inventory($orgId);
        foreach ($estimateData["moveInventory"] as $inventoryItem){
            $estimateItem = $inventory->getEstimateItem($inventoryItem["id"]);
            $total += $estimateItem["cf"];
        }

        return $total;
    }

    public function bookJob($type){
        $errorVar = array("estimate Class","bookJob()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $this->estimateId, PDO::PARAM_INT);
        $binds[] = array(':isBooked', $type, PDO::PARAM_INT);
        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimates SET isBooked=:isBooked WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function getTotalEstimatesByDate($orgId,$compare = false,$sDate = false,$eDate = false,$label){

        $errorVar = array("estimate Class","getTotalEstimatesByDate()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates AS e INNER JOIN networkleads_db.leads AS l ON l.id=e.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.isBadLead=0 AND e.createdDate>=:startDate AND e.createdDate<=:endDate",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                $totalThisMonth = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalEstimates'] = $totalThisMonth;
                $data['totalEstimatesIsCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW yesterday"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-1 months"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -2 months"));
                }else if ($label == "Custom"){
                    $data['totalEstimatesChange'] = 0;
                    $data['totalEstimatesIsCustom'] = true;
                    return $data;
                }
                $binds = [];
                $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates AS e INNER JOIN networkleads_db.leads AS l ON l.id=e.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.isBadLead=0 AND e.createdDate>=:startDate AND e.createdDate<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalLastMonth = $GLOBALS['connector']->fetch_num_rows($getIt);
                    if ($totalLastMonth == 0 && $totalThisMonth > 0){
                        $data['totalEstimatesChange'] = 100;
                    }else if ($totalLastMonth == $totalThisMonth) {
                        $data['totalEstimatesChange'] = 0;
                    }else{
                        $data['totalEstimatesChange'] = ($totalThisMonth/$totalLastMonth)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }

    public function getTotalBookedEstimatesByDate($orgId,$compare = false,$sDate = false,$eDate = false,$label){

        $errorVar = array("estimate Class","getTotalBookedEstimatesByDate()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates AS e INNER JOIN networkleads_db.leads AS l ON l.id=e.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.isBadLead=0 AND e.isBooked=1 AND e.createdDate>=:startDate AND e.createdDate<=:endDate",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                $totalPerThisDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalBookedEstimates'] = $totalPerThisDate;
                $data['totalBookedEstimatesIsCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW yesterday"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-1 months"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -2 months"));
                }else if ($label == "Custom"){
                    $data['totalBookedEstimatesChange'] = 0;
                    $data['totalBookedEstimatesIsCustom'] = true;
                    return $data;
                }

                $binds = [];
                $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimates AS e INNER JOIN networkleads_db.leads AS l ON l.id=e.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.isBadLead=0 AND e.isBooked=1 AND e.createdDate>=:startDate AND e.createdDate<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);
                    if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                        $data['totalBookedEstimatesChange'] = 100;
                    }else if ($totalPerThisDate == $totalPerNewDate) {
                        $data['totalBookedEstimatesChange'] = 0;
                    }else{
                        $data['totalBookedEstimatesChange'] = ($totalPerThisDate/$totalPerNewDate)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }

    function updateOperationDate($dateS){
        $this->checkIfEstimateExists(true);

        $errorVar = array("estimate Class","updateOperationDate()",4,"Notes",array());

        $binds = [];
        $binds[] = [":dateS",$dateS,PDO::PARAM_STR];
        $binds[] = [':leadId', $this->leadId, PDO::PARAM_INT];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimates SET requestedDeliveryDateStart=:dateS WHERE leadId=:leadId",$binds,$errorVar);
        if (!$setIt){
            return false;
        }else{
            return true;
        }
    }
    */
}