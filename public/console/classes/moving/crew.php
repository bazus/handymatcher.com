<?php

class crew
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getCrew(){

        $errorVar = array("crew Class","getCrew()",4,"Notes",array());

        $crew = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);


        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.crew WHERE orgId=:orgId AND isDeleted=0 ORDER BY id",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            while($member = $GLOBALS['connector']->fetch($getIt,true)){
                $crew[] = $member;
            }
        }

        return $crew;

    }

    function deleteCrewById($crewId = NULL){
        if($crewId === NULL){return;}
        $errorVar = array("crew Class","deleteCrewById()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $crewId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.crew SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function getSingleCrew($crewId = NULL){
        if($crewId === NULL){return;}


        $errorVar = array("crew Class","getSingleCrew()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $crewId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.crew WHERE orgId=:orgId AND isDeleted=0 AND id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $crew = $GLOBALS['connector']->fetch($getIt,true);
            if(!$crew){
                return false;
            }
            return $crew;
        }

        return false;
    }

    function addCrew($memberFullname = "",$memberEmail = "",$memberBirthDate = "",$memberPhone = "",$memberAddress = "",$memberType = "",$memberComments = ""){

        $errorVar = array("crew Class","addCrew()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $memberFullname, PDO::PARAM_STR);
        $binds[] = array(':email', $memberEmail, PDO::PARAM_STR);
        $binds[] = array(':birthDate', $memberBirthDate, PDO::PARAM_STR);
        $binds[] = array(':phone', $memberPhone, PDO::PARAM_STR);
        $binds[] = array(':address', $memberAddress, PDO::PARAM_STR);
        $binds[] = array(':comments', $memberComments, PDO::PARAM_STR);

        if($memberType == "1" || $memberType == "2" || $memberType == "3"){
            $binds[] = array(':type', $memberType, PDO::PARAM_INT);
        }else{
            $binds[] = array(':type', NULL, PDO::PARAM_NULL);
        }

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.crew (orgId,name,email,birthDate,phone,address,comments,type) VALUES(:orgId,:name,:email,:birthDate,:phone,:address,:comments,:type)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function updateCrew($crewId = NULL,$memberFullname = "",$memberEmail = "",$memberBirthDate = "",$memberPhone = "",$memberAddress = "",$memberType = "",$memberComments = ""){

        if($crewId === NULL){return;}

        $errorVar = array("crew Class","updateCrew()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $crewId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $memberFullname, PDO::PARAM_STR);
        $binds[] = array(':email', $memberEmail, PDO::PARAM_STR);
        $binds[] = array(':birthDate', $memberBirthDate, PDO::PARAM_STR);
        $binds[] = array(':phone', $memberPhone, PDO::PARAM_STR);
        $binds[] = array(':address', $memberAddress, PDO::PARAM_STR);
        $binds[] = array(':comments', $memberComments, PDO::PARAM_STR);

        if($memberType == "1" || $memberType == "2" || $memberType == "3"){
            $binds[] = array(':type', $memberType, PDO::PARAM_INT);
        }else{
            $binds[] = array(':type', NULL, PDO::PARAM_NULL);
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.crew SET name=:name,email=:email,birthDate=:birthDate,phone=:phone,address=:address,comments=:comments,type=:type WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }

}