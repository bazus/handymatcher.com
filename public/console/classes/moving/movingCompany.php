<?php

class movingCompany
{

    protected $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getLicenses(){

        $errorVar = array("movingCompany Class","getLicenses()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.licenses WHERE organizationId=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $licenses = $GLOBALS['connector']->fetch($getIt,true);
            return $licenses;
        }

        return false;
    }

    function updateLicenses($usdot,$mcc,$registration){

        $errorVar = array("movingCompany Class","updateLicenses()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.licenses WHERE organizationId=:organizationId",$binds,$errorVar);
        if($GLOBALS['connector']->fetch_num_rows($getIt) == 0) {
            // Create new company licenses

            $binds = array();
            $binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':DOT', $usdot, PDO::PARAM_STR);
            $binds[] = array(':ICCMC', $mcc, PDO::PARAM_STR);
            $binds[] = array(':registration', $registration, PDO::PARAM_STR);

            $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.licenses(organizationId,DOT,ICCMC,registration) VALUES(:organizationId,:DOT,:ICCMC,:registration)", $binds, $errorVar);
            if (!$getIt) {
                return false;
            } else {
                return true;
            }

        }else {
            // Update existing company licenses

            $binds = array();
            $binds[] = array(':organizationId', $this->orgId, PDO::PARAM_INT);
            $binds[] = array(':DOT', $usdot, PDO::PARAM_STR);
            $binds[] = array(':ICCMC', $mcc, PDO::PARAM_STR);
            $binds[] = array(':registration', $registration, PDO::PARAM_STR);

            $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.licenses SET DOT=:DOT,ICCMC=:ICCMC,registration=:registration WHERE organizationId=:organizationId", $binds, $errorVar);
            if (!$getIt) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }



}