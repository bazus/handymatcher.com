<?php

class trucks
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getTrucks($getOnlyActive = false){

        $errorVar = array("trucks Class","getTrucks()",4,"Notes",array());

        $trucks = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        if ($getOnlyActive){
            $getIt = $GLOBALS['connector']->execute("SELECT id,title,isActive FROM networkleads_moving_db.trucks WHERE orgId=:orgId AND isDeleted=0 AND isActive=1 ORDER BY id",$binds,$errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT id,title,isActive FROM networkleads_moving_db.trucks WHERE orgId=:orgId AND isDeleted=0 ORDER BY id",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            while($truck = $GLOBALS['connector']->fetch($getIt,true)){
                $trucks[] = $truck;
            }
        }

        return $trucks;

    }

    function deleteTruck($id){
        $errorVar = array("trucks Class","deleteTruck()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.trucks SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function activeTruck($id,$active){
        $errorVar = array("trucks Class","deleteTruck()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':active', $active, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.trucks SET isActive=:active WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function soldTruck($id,$isSold){
        $errorVar = array("trucks Class","deleteTruck()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':isSold', $isSold, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        if ($isSold == 0){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.trucks SET isSold=:isSold WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        }else{
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.trucks SET isSold=:isSold,isActive=0 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        }
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function getTruck($id){

        $errorVar = array("trucks Class","getTruck()",4,"Notes",array());

        $trucks = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.trucks WHERE orgId=:orgId AND isDeleted=0 AND id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
                $truck = $GLOBALS['connector']->fetch($getIt,true);
                if(!$truck){
                    return false;
                }else{
                    $truck['purchaseDate'] = date("Y-m-d",strtotime($truck['purchaseDate']));
                    if($truck['soldDate']){
                        $truck['soldDate'] = date("Y-m-d",strtotime($truck['soldDate']));
                    }
                    $trucks = $truck;
                }

        }

        return $trucks;

    }

    function addTruck($userId,$title,$description,$model,$year,$vin,$plate,$tire,$state,$capacity,$purchaseDate,$purchasePrice,$purchaseMiles){

        $errorVar = array("trucks Class","addTruck()",4,"Notes",array());

        if(!$purchaseDate){
            $purchaseDate = "NOW";
        }
        $capacity = $this->checkBeforeSend($capacity);
        $purchasePrice = $this->checkBeforeSend($purchasePrice);
        $purchaseMiles = $this->checkBeforeSend($purchaseMiles);

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':description', $description, PDO::PARAM_STR);
        $binds[] = array(':model', $model, PDO::PARAM_STR);
        $binds[] = array(':mfYear', $year, PDO::PARAM_STR);
        $binds[] = array(':vin', $vin, PDO::PARAM_STR);
        $binds[] = array(':plate', $plate, PDO::PARAM_STR);
        $binds[] = array(':tire', $tire, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':capacity', $capacity, PDO::PARAM_STR);
        $binds[] = array(':purchaseDate', date("Y-m-d",strtotime($purchaseDate)), PDO::PARAM_STR);
        $binds[] = array(':purchasePrice', $purchasePrice, PDO::PARAM_STR);
        $binds[] = array(':purchaseMiles', $purchaseMiles, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.trucks (title,description,model,mfYear,vin,plateNumber,tireSize,state,orgId,comment,capacity,purchaseDate,purchasePrice,purchaseMiles,isSold,isActive,isDeleted,userCreated) VALUES(:title,:description,:model,:mfYear,:vin,:plate,:tire,:state,:orgId,'',:capacity,:purchaseDate,:purchasePrice,:purchaseMiles,0,1,0,:userId)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id();
        }

        return false;

    }

    function updateTruck($id,$title,$description,$model,$year,$vin,$plate,$tire,$state,$comment,$capacity,$purchaseDate,$purchasePrice,$purchaseMiles,$soldDate,$soldPrice,$soldMiles){

        $errorVar = array("trucks Class","updateTruck()",4,"Notes",array());

        if(!$purchaseDate){
            $purchaseDate = "NOW";
        }
        if(!$soldDate){
            $soldDate = "NOW";
        }
        $capacity = $this->checkBeforeSend($capacity);
        $purchasePrice = $this->checkBeforeSend($purchasePrice);
        $purchaseMiles = $this->checkBeforeSend($purchaseMiles);
        $soldPrice = $this->checkBeforeSend($soldPrice);
        $soldMiles = $this->checkBeforeSend($soldMiles);

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':truckId', $id, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR); 
        $binds[] = array(':description', $description, PDO::PARAM_STR); 
        $binds[] = array(':model', $model, PDO::PARAM_STR); 
        $binds[] = array(':mfYear', $year, PDO::PARAM_STR); 
        $binds[] = array(':vin', $vin, PDO::PARAM_STR); 
        $binds[] = array(':plate', $plate, PDO::PARAM_STR); 
        $binds[] = array(':tire', $tire, PDO::PARAM_STR); 
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':comment', $comment, PDO::PARAM_STR);
        $binds[] = array(':capacity', $capacity, PDO::PARAM_STR);
        $binds[] = array(':purchaseDate', date("Y-m-d",strtotime($purchaseDate)), PDO::PARAM_STR);
        $binds[] = array(':purchasePrice', $purchasePrice, PDO::PARAM_STR);
        $binds[] = array(':purchaseMiles', $purchaseMiles, PDO::PARAM_STR);
        $binds[] = array(':soldDate', date("Y-m-d",strtotime($soldDate)), PDO::PARAM_STR);
        $binds[] = array(':soldPrice', $soldPrice, PDO::PARAM_STR);
        $binds[] = array(':soldMiles', $soldMiles, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.trucks SET title=:title,description=:description,model=:model,mfYear=:mfYear,vin=:vin,plateNumber=:plate,tireSize=:tire,state=:state,comment=:comment,capacity=:capacity,purchaseDate=:purchaseDate,purchasePrice=:purchasePrice,purchaseMiles=:purchaseMiles,soldDate=:soldDate,soldPrice=:soldPrice,soldMiles=:soldMiles WHERE id=:truckId AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    private function checkBeforeSend(&$var){
        if ($var < 0){
            $var = 0;
        }
        return $var;
    }

}