<?php

class carriers
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getCarriers($getOnlyActive = false){

        $errorVar = array("carriers Class","getCarriers()",4,"Notes",array());

        $trucks = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        if ($getOnlyActive){
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.carriers WHERE orgId=:orgId AND isDeleted=0 AND isActive=1 ORDER BY id",$binds,$errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.carriers WHERE orgId=:orgId AND isDeleted=0 ORDER BY id",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            while($truck = $GLOBALS['connector']->fetch($getIt,true)){
                $trucks[] = $truck;
            }
        }

        return $trucks;

    }

    function deleteCarrierById($carrierId = NULL){
        if($carrierId === NULL){return;}
        $errorVar = array("carriers Class","deleteCarrier()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $carrierId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.carriers SET isDeleted=1,dateDeleted=NOW() WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function setCarrierStatus($carrierId = NULL,$status = false){
        if($carrierId === NULL){return;}

        $errorVar = array("carriers Class","setCarrierStatus()",4,"Notes",array());

        if($status == false){
            $status = 0;
        }else{
            $status = 1;
        }

        $binds = array();
        $binds[] = array(':id', $carrierId, PDO::PARAM_INT);
        $binds[] = array(':isActive', $status, PDO::PARAM_BOOL);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.carriers SET isActive=:isActive WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function getSingleCarrier($carrierId = NULL){
        if($carrierId === NULL){return;}


        $errorVar = array("carriers Class","getSingleCarrier()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $carrierId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.carriers WHERE orgId=:orgId AND isDeleted=0 AND id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $carrier = $GLOBALS['connector']->fetch($getIt,true);
            if(!$carrier){
                return false;
            }
            return $carrier;
        }

        return false;
    }

    function addCarrier($name = "",$contact = "",$address = "",$city = "",$state = "",$zip = "",$country = "",$phone1 = "",$phone2 = "",$fax = "",$email = "",$website = "",$dot = "",$iccmc = "",$registration = "",$type = "",$comments = "",$agreementFileName = "",$agreementFileKey = "",$agreementFileURL = ""){

        $errorVar = array("trucks Class","addCarrier()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $name, PDO::PARAM_STR);
        $binds[] = array(':contactName', $contact, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':country', $country, PDO::PARAM_STR);
        $binds[] = array(':phone1', $phone1, PDO::PARAM_STR);
        $binds[] = array(':phone2', $phone2, PDO::PARAM_STR);
        $binds[] = array(':fax', $fax, PDO::PARAM_STR);
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':website', $website, PDO::PARAM_STR);
        $binds[] = array(':dot', $dot, PDO::PARAM_STR);
        $binds[] = array(':iccmc', $iccmc, PDO::PARAM_STR);
        $binds[] = array(':registration', $registration, PDO::PARAM_STR);
        $binds[] = array(':comments', $comments, PDO::PARAM_STR);
        $binds[] = array(':agreementFileName', $agreementFileName, PDO::PARAM_STR);
        $binds[] = array(':agreementFileKey', $agreementFileKey, PDO::PARAM_STR);
        $binds[] = array(':agreementFileURL', $agreementFileURL, PDO::PARAM_STR);

        if($type == "1" || $type == "2" || $type == "3" || $type == "4" || $type == "5" || $type == "6"){
            $binds[] = array(':type', $type, PDO::PARAM_INT);
        }else{
            $binds[] = array(':type', NULL, PDO::PARAM_NULL);
        }

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.carriers (orgId,name,contactName,address,city,state,zip,country,phone1,phone2,fax,email,website,dot,iccmc,registration,type,comments,isActive,agreementFileURL,agreementFileName,agreementFileKey) VALUES(:orgId,:name,:contactName,:address,:city,:state,:zip,:country,:phone1,:phone2,:fax,:email,:website,:dot,:iccmc,:registration,:type,:comments,1,:agreementFileURL,:agreementFileName,:agreementFileKey)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function updateCarrier($carrierId = NULL,$name = "",$contact = "",$address = "",$city = "",$state = "",$zip = "",$country = "",$phone1 = "",$phone2 = "",$fax = "",$email = "",$website = "",$dot = "",$iccmc = "",$registration = "",$type = "",$comments = ""){

        if($carrierId === NULL){return;}

        $errorVar = array("trucks Class","updateCarrier()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $carrierId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':name', $name, PDO::PARAM_STR);
        $binds[] = array(':contactName', $contact, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':country', $country, PDO::PARAM_STR);
        $binds[] = array(':phone1', $phone1, PDO::PARAM_STR);
        $binds[] = array(':phone2', $phone2, PDO::PARAM_STR);
        $binds[] = array(':fax', $fax, PDO::PARAM_STR);
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':website', $website, PDO::PARAM_STR);
        $binds[] = array(':dot', $dot, PDO::PARAM_STR);
        $binds[] = array(':iccmc', $iccmc, PDO::PARAM_STR);
        $binds[] = array(':registration', $registration, PDO::PARAM_STR);
        $binds[] = array(':comments', $comments, PDO::PARAM_STR);

        if($type != "1" && $type != "1" && $type != "1" && $type != "1" && $type != "1" && $type != "1"){
            $binds[] = array(':type', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':type', $type, PDO::PARAM_INT);
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.carriers SET name=:name,contactName=:contactName,address=:address,city=:city,state=:state,zip=:zip,country=:country,phone1=:phone1,phone2=:phone2,fax=:fax,email=:email,website=:website,dot=:dot,iccmc=:iccmc,registration=:registration,type=:type,comments=:comments WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }

    function updateCarrierAgreementFile($carrierId = NULL,$agreementFileName = "",$agreementFileKey = "",$agreementFileURL = ""){
        if($carrierId === NULL){return;}

        $errorVar = array("trucks Class","addCarrier()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $carrierId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':agreementFileName', $agreementFileName, PDO::PARAM_STR);
        $binds[] = array(':agreementFileKey', $agreementFileKey, PDO::PARAM_STR);
        $binds[] = array(':agreementFileURL', $agreementFileURL, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.carriers SET agreementFileName=:agreementFileName,agreementFileKey=:agreementFileKey,agreementFileURL=:agreementFileURL WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }


}