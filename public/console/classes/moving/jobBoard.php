<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");

class jobBoard
{

    private $orgId = NULL;

    function __construct($orgId = NULL)
    {
        $this->orgId = $orgId;
    }

    function addToJobBoard($leadId,$carrierBalance){

        if($this->orgId === NULL){return false;}

        $errorVar = array("jobBoard Class","addToJobBoard()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_moving_db.jobBoard  WHERE orgId=:orgId AND leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) == 0) {
                $binds[] = array(':carrierBalance', $carrierBalance, PDO::PARAM_INT);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.jobBoard (orgId,leadId,isActive,carrierBalance) VALUES(:orgId,:leadId,1,:carrierBalance)", $binds, $errorVar);
                if (!$setIt) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    function getSingleJobInBoard($jobId){

        if($this->orgId === NULL){return false;}

        $errorVar = array("jobBoard Class","getSingleJobInBoard()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $jobId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.jobBoard  WHERE orgId=:orgId AND id=:id AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $job = $GLOBALS['connector']->fetch($getIt);
            return $job;
        }
        return false;
    }

    function getSingleJobInBoardByLeadId($leadId){

        if($this->orgId === NULL){return false;}

        $errorVar = array("jobBoard Class","getSingleJobInBoardByLeadId()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.jobBoard  WHERE orgId=:orgId AND leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $job = $GLOBALS['connector']->fetch($getIt);
            return $job;
        }
        return false;
    }

    function updateCarrierBalanceJobInBoard($leadId,$carrierBalance){

        if($this->orgId === NULL){return false;}
        if (!is_numeric($carrierBalance)){return false;}

        $errorVar = array("jobBoard Class","updateCarrierBalanceJobInBoard()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':carrierBalance', $carrierBalance, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.jobBoard SET carrierBalance=:carrierBalance WHERE leadId=:leadId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
    }

    function setJobInBoardAsActive($jobId){

        if($this->orgId === NULL){return false;}

        $errorVar = array("jobBoard Class","setJobInBoardAsActive()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':jobId', $jobId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.jobBoard SET isActive=1 WHERE id=:jobId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
    }

    function setJobInBoardAsDisabled($jobId){

        if($this->orgId === NULL){return false;}

        $errorVar = array("jobBoard Class","setJobInBoardAsDisabled()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':jobId', $jobId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.jobBoard SET isActive=0 WHERE id=:jobId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
    }
    function setJobInBoardAsDeleted($jobId){

        if($this->orgId === NULL){return false;}

        $errorVar = array("jobBoard Class","setJobInBoardAsDisabled()",1,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':jobId', $jobId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.jobBoard SET isDeleted=1 WHERE id=:jobId AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
    }

    function getJobsForBoard($activeOnly = false){
        $errorVar = array("jobBoard Class","getJobsForBoard()",4,"Notes",array());

        $jobs = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        if($activeOnly == true){
            $getIt = $GLOBALS['connector']->execute("SELECT id,leadId,dateAdded,isActive,carrierBalance FROM networkleads_moving_db.jobBoard WHERE orgId=:orgId AND isDeleted=0 AND isActive=1",$binds,$errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT id,leadId,dateAdded,isActive,carrierBalance FROM networkleads_moving_db.jobBoard WHERE orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            while($job = $GLOBALS['connector']->fetch($getIt)){

                $binds = array();
                $binds[] = array(':leadId', $job["leadId"], PDO::PARAM_INT);

                $getIt2 = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.jobAcceptanceForms WHERE leadId=:leadId AND signature IS NOT NULL AND isDeleted=0",$binds,$errorVar);
                if(!$getIt2){
                    return false;
                }else{
                    $totalSignatures = $GLOBALS['connector']->fetch_num_rows($getIt2);
                    if($totalSignatures > 0){
                        $job["totalSignatures"] = $totalSignatures;
                    }else{
                        $job["totalSignatures"] = "0";
                    }
                }

                $estimateCalculation = new estimateCalculation($job["leadId"],$this->orgId);
                $estimateCalculationData = $estimateCalculation->getData();

                if(!$estimateCalculationData){continue;}

                $estimateData = [];

                if($estimateCalculationData["calcType"] == "0"){
                    $estimateData["size"] = $estimateCalculationData["initPrice"]." lbs";
                    $estimateData["sizeNumber"] = $estimateCalculationData["initPrice"] / 7;
                }elseif($estimateCalculationData["calcType"] == "1"){
                    $estimateData["size"] = $estimateCalculationData["initPrice"]." cf";
                    $estimateData["sizeNumber"] = $estimateCalculationData["initPrice"];
                }else{
                    $estimateData["size"] = "N/A";
                    $estimateData["sizeNumber"] = 0;
                }

                $movingLead = new movingLead($estimateCalculationData["leadId"],$this->orgId);
                $movingLeadData = $movingLead->getData();
                if(!$movingLeadData){continue;}

                $lead = new lead($estimateCalculationData["leadId"],$this->orgId);
                $leadData = $lead->getData();
                if(!$leadData){continue;}

                if($leadData["jobNumber"] == "" || $leadData["jobNumber"] == false){
                    $jobNumber = "#".$leadData["id"];
                }else{
                    $jobNumber = "#".$leadData["jobNumber"];
                }

                $leadData = [];
                $leadData["jobNumber"] = $jobNumber;

                // move date [TEXT]
                $leadData['pickupDateTimestamp'] = strtotime($movingLeadData["pickupDateStart"]);
                $pickupDateStart = date("F jS, Y",strtotime($movingLeadData["pickupDateStart"]));
                $pickupDateEnd = date("F jS, Y",strtotime($movingLeadData["pickupDateEnd"]));
                if($movingLeadData["includePickupDate"] == "1"){
                    if($pickupDateStart == $pickupDateEnd){
                        $leadData["pickupDate"] = $pickupDateStart;
                    }else{
                        $leadData["pickupDate"] = $pickupDateStart." - ".$pickupDateEnd;
                    }
                }else{
                    $leadData["pickupDate"] = "N/A";
                }

                // from & to [TEXT]
                $fromText = "";
                if($movingLeadData["fromCity"] != ""){
                    $fromText .= $movingLeadData["fromCity"];

                    if($movingLeadData["fromState"] != ""){
                        $fromText .= ", ".$movingLeadData["fromState"];
                    }
                    if($movingLeadData["fromZip"] != ""){
                        $fromText .= " ".$movingLeadData["fromZip"];
                    }
                }else{
                    if($movingLeadData["fromState"] != ""){
                        $fromText .= $movingLeadData["fromState"];
                    }
                    if($movingLeadData["fromZip"] != ""){
                        $fromText .= " ".$movingLeadData["fromZip"];
                    }
                }
                if($fromText == ""){$fromText = "N/A";}

                $toText = "";
                if($movingLeadData["toCity"] != ""){
                    $toText .= $movingLeadData["toCity"];

                    if($movingLeadData["toState"] != ""){
                        $toText .= ", ".$movingLeadData["toState"];
                    }
                    if($movingLeadData["toZip"] != ""){
                        $toText .= " ".$movingLeadData["toZip"];
                    }
                }else{
                    if($movingLeadData["toState"] != ""){
                        $toText .= $movingLeadData["toState"];
                    }
                    if($movingLeadData["toZip"] != ""){
                        $toText .= " ".$movingLeadData["toZip"];
                    }
                }
                if($toText == ""){$toText = "N/A";}

                $leadData["from"] = $fromText;
                $leadData["to"] = $toText;

                $job["estimateData"] = $estimateData;
                $job["leadData"] = $leadData;

                $jobs[] = $job;
            }
        }
        return $jobs;
    }

}