<?php

class fullvalueprotection
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getData(){

        $errorVar = array("fullvalueprotection Class","getData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.fullvalueprotection WHERE orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);

            if(!$r){

                // Create the full value protection
                $this->createData();

                $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.fullvalueprotection WHERE orgId=:orgId",$binds,$errorVar);
                if(!$getIt){
                    return false;
                }else {
                    $r = $GLOBALS['connector']->fetch($getIt);
                    return $this->generateDataToTable($r);
                }


            }else{
                return $this->generateDataToTable($r);
            }
        }

        return false;
    }

    function update($deductiblelevels,$amountofliability,$rates){

        $errorVar = array("fullvalueprotection Class","update()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':deductiblelevels', serialize($deductiblelevels), PDO::PARAM_STR);
        $binds[] = array(':amountofliability', serialize($amountofliability), PDO::PARAM_STR);
        $binds[] = array(':rates', serialize($rates), PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.fullvalueprotection SET deductiblelevels=:deductiblelevels,amountofliability=:amountofliability,rates=:rates WHERE orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function generateDataToTable($data){

        $deductiblelevels = unserialize($data["deductiblelevels"]);
        $amountofliability = unserialize($data["amountofliability"]);
        $rates = unserialize($data["rates"]);

        if(!$deductiblelevels || !$amountofliability || !$rates){return false;}

        $data = [];
        $data["levels"] = $deductiblelevels;
        $data["data"] = $deductiblelevels;

        $results = [];

        try{
            for($i = 0;$i<count($amountofliability);$i++){
                $aol = $amountofliability[$i];

                $singleAOL = [];

                for($j = 0;$j<count($deductiblelevels);$j++) {
                    if(isset($rates[$i][$j])){
                        $singleAOL[] = $rates[$i][$j];
                    }else{
                        $singleAOL[] = 0;
                    }
                }
                $results[$aol] = $singleAOL;
            }

            $data["data"] = $results;

            return $data;
        }catch (Exception $e){
            return false;
        }

        return false;
    }

    function getDefaultData(){
        $data = [];
        $data["deductiblelevels"] = ["100", "200", "300", "400", "500", "600"];
        $data["amountofliability"] = ["6000", "10000", "15000", "20000", "250000", "30000", "40000", "50000", "60000", "75000", "100000", "125000", "150000", "175000", "200000", "225000", "250000", "500000"];
        $data["rates"] = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]];

        return $data;
    }

    function createData(){
        $data = $this->getDefaultData();

        $errorVar = array("fullvalueprotection Class","createData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':deductiblelevels', serialize($data["deductiblelevels"]), PDO::PARAM_STR);
        $binds[] = array(':amountofliability', serialize($data["amountofliability"]), PDO::PARAM_STR);
        $binds[] = array(':rates', serialize($data["rates"]), PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.fullvalueprotection(orgId,deductiblelevels,amountofliability,rates) VALUES(:orgId,:deductiblelevels,:amountofliability,:rates)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

}