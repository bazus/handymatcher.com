<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");

class jobAcceptanceForms
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getJobAcceptanceForms($leadId){

        $errorVar = array("jobAcceptanceForms Class","getJobAcceptanceForms()",4,"Notes",array());

        $forms = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.jobAcceptanceForms WHERE orgId=:orgId AND leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            while($form = $GLOBALS['connector']->fetch($getIt,true)){
                $forms[] = $form;
            }
        }
        return $forms;
    }

    function getSingleJobAcceptanceForm($formId){

        $errorVar = array("jobAcceptanceForms Class","getSingleJobAcceptanceForm()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $formId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.jobAcceptanceForms WHERE id=:id AND orgId=:orgId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $form = $GLOBALS['connector']->fetch($getIt);
            return $form;
        }
        return false;
    }

    function getSingleJobAcceptanceFormByKey($secretKey,$secretToken){

        $errorVar = array("jobAcceptanceForms Class","getSingleJobAcceptanceFormByKey()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':secretKey', $secretKey, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $secretToken, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.jobAcceptanceForms WHERE secretKey=:secretKey AND secretToken=:secretToken AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $form = $GLOBALS['connector']->fetch($getIt);
            return $form;
        }
        return false;
    }

    function addSignature($secretKey,$secretToken,$signature = "",$userIP = ""){

        $errorVar = array("jobAcceptanceForms Class","addSignature()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':secretKey', $secretKey, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $secretToken, PDO::PARAM_STR);
        $binds[] = array(':signature', $signature, PDO::PARAM_STR);
        $binds[] = array(':signatureIP', $userIP, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.jobAcceptanceForms SET signature=:signature,signedDate=NOW(),signatureIP=:signatureIP WHERE secretKey=:secretKey AND secretToken=:secretToken AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }
        return false;
    }

    function createForm($leadId,$carrierId = NULL,$carrierBalance = 0,$carrierName = "",$carrierPhone = "",$carrierEmail = "",$carrierDOT = "",$carrierMC = ""){

        $errorVar = array("jobAcceptanceForms Class","createForm()",4,"Notes",array());
        
        if($carrierId != NULL) {
            $carriers = new carriers($this->orgId);
            $carrierData = $carriers->getSingleCarrier($carrierId);
            if ($carrierData == false) {
                return false;
            }

            $carrierName = $carrierData["name"];
            $carrierPhone = $carrierData["phone1"];
            $carrierEmail = $carrierData["email"];
            $carrierDOT = $carrierData["dot"];
            $carrierMC = $carrierData["iccmc"];
        }

        $randomNumber = rand(0,999999); // To make it impossible to get two identical secret keys for leads
        $secretKey = md5(date("Y-m-d H:i:s",strtotime("NOW")).$this->orgId.$randomNumber);

        $randomNumber = rand(0,999999); // To make it impossible to get two identical secret keys for leads
        $secretToken = md5(date("Y-m-d H:i:s",strtotime("NOW")).$this->orgId.$randomNumber);

        $binds = array();

        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        if($carrierId != NULL){
            $binds[] = array(':carrierId', $carrierId, PDO::PARAM_INT);
        }else{
            $binds[] = array(':carrierId', NULL, PDO::PARAM_NULL);
        }
        $binds[] = array(':carrierName', $carrierName, PDO::PARAM_STR);
        $binds[] = array(':carrierPhone', $carrierPhone, PDO::PARAM_STR);
        $binds[] = array(':carrierEmail', $carrierEmail, PDO::PARAM_STR);
        $binds[] = array(':carrierDOT', $carrierDOT, PDO::PARAM_STR);
        $binds[] = array(':carrierMC', $carrierMC, PDO::PARAM_STR);
        $binds[] = array(':secretKey', $secretKey, PDO::PARAM_STR);
        $binds[] = array(':secretToken', $secretToken, PDO::PARAM_STR);
        $binds[] = array(':balance', $carrierBalance, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.jobAcceptanceForms(leadId,orgId,carrierId,carrierName,carrierPhone,carrierEmail,carrierDOT,carrierMC,secretKey,secretToken,balance) VALUES(:leadId,:orgId,:carrierId,:carrierName,:carrierPhone,:carrierEmail,:carrierDOT,:carrierMC,:secretKey,:secretToken,:balance)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id($setIt);
        }

        return false;
    }

    function deleteForm($leadId, $carrierJobsId = []){

        $errorVar = array("jobAcceptanceForms Class","deleteForm()",4,"Notes",array());
        $isOk = true;
        for($i = 0; $i < count($carrierJobsId); $i++ ){

            if($carrierJobsId[$i] == "" || $carrierJobsId[$i] == false || $carrierJobsId[$i] == null){continue;}
            $binds = array();
            $binds[] = array(':id', $carrierJobsId[$i], PDO::PARAM_INT);
            $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.jobAcceptanceForms SET isDeleted=1 WHERE id=:id AND leadId=:leadId AND orgId=:orgId", $binds, $errorVar);
            if (!$setIt) {
                $isOk = false;
            }
        }

        return $isOk;
    }

}