<?php

class movingSettings
{
    protected $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getData($unsecureLoad = false){

        $errorVar = array("movingSettings Class","getData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.movingSettings WHERE orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $data = false;
            if ($unsecureLoad){
                $data = $GLOBALS['connector']->fetch($getIt,false);
            }else{
                $data = $GLOBALS['connector']->fetch($getIt,true);
            }

            if($data["tariff"] != "" && $data["tariff"] != NULL) {
                $data["tariff"] = unserialize(base64_decode($data["tariff"]));
            }

            return $data;
        }

        return false;
    }

    function checkAndSetMovingSettings(){

        $errorVar = array("movingSettings Class","checkAndSetMovingSettings()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.movingSettings WHERE orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }else{
                // if organization does'nt have a prices -> make one
                $FVPterms = "PLEASE BE AWARE: If a moving company damages or loses  any of your household goods, there are 2 standards for the company’s liability based on the types of rates you pay. BY FEDERAL LAW, THIS FORM MUST CONTAIN A FILLED-IN ESTIMATE OF THE COST OF A MOVE FOR WHICH THE MOVING COMPANY IS LIABLE FOR THE FULL (REPLACEMENT) VALUE OF YOUR GOODS in the event of loss of, or damage to, the goods. This form may also contain an estimate of the cost of a move in which the moving company is liable for FAR LESS than the replacement value of your goods, typically at a lower cost to you. You must select the liability level on the bill of lading/contract or signed agreed estimate for your move. Before selecting a liability level, please make sure you understand the differences in liability levels, and seek further information at the government website <a href='www.protectyourmove.gov'>www.protectyourmove.gov</a>";
                $terms = "This is a binding estimate based on the information provided to us by you. Our company guarantees this price based on the current inventory list. Should your inventory be adapted before or at the time of pick- up, your price may change (Based on the company's tariff for the additional or changed items). Please note that changes to your inventory list and/or desired move date can be made up until 72 hours prior to your original desired move date.";
                $creditcardterms = ' I agree to <span style="font-weight: bold;text-decoration: underline">{companyName}</span> cancellation policy as well, which reads: "Deposits are refundable (less 10% for processing fees and internal resources) for reservations cancelled Inside Seven (7) days of signing this estimate. If the pick-up date reflected on this estimate is within Seven (7) business days of signing, the deposit is non-refundable. Moves may be postponed for up to 24 months, if you choose to cancel your move outside of <span style="font-weight: bold;text-decoration: underline">{companyName}</span> cancellation policy, your deposit may be applicable towards any move in the next 24 months. All cancellation or refund requests MUST be sent in writing via email to <span style="font-weight: bold;text-decoration: underline">{companyName}</span> . Any cancellation or refund request received after normal business hours, after 6pm Eastern Time, will be deemed received on the next business day (Federal holidays and Weekends excluded)".<br>
I agree that any deposit provided to <span style="font-weight: bold;text-decoration: underline">{companyName}</span> can be utilized for a future move within a 12 month period of placing a reservation, should I request to cancel.<br>';
                $binds[] = array(':terms', $terms, PDO::PARAM_STR);
                $binds[] = array(':FVPTerms', $FVPterms, PDO::PARAM_STR);
                $binds[] = array(':estimateTermsCreditCard', $creditcardterms, PDO::PARAM_STR);
                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.movingSettings(orgId,estimateTerms,estimateTermsCreditCard,FVPTerms) VALUES(:orgId,:terms,:estimateTermsCreditCard,:FVPTerms)",$binds,$errorVar);
                if(!$setIt){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return false;
    }

    function setMovingSettings($data){

        $errorVar = array("movingSettings Class","setMovingSettings()",4,"Notes",array());

        $binds = array();


        $data = $this->checkDataBeforeSend($data);
        foreach ($data as $k => $v){
            if($k == "estimateTerms" || $k == "FVPTerms" || $k == "estimateTermsCreditCard"){
                $binds[] = [":".$k,$v,PDO::PARAM_STR];
            }else{
                $binds[] = [":".$k,$v,PDO::PARAM_INT];
            }
        }

        $binds[] = [":orgId",$this->orgId,PDO::PARAM_INT];
        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.movingSettings SET estimateEmailAddressId=:estimateEmailAddressId,estimateEmailId=:estimateEmailId,sendEstimateEmail=:sendEstimateEmail,calculateBy=:calculateBy,cflbsratio=:cflbsratio,blockDuplicatesLeadsBasedPhone=:blockDuplicatesLeadsBasedPhone,blockDuplicatesLeadsBasedEmail=:blockDuplicatesLeadsBasedEmail,seniorDiscount=:seniorDiscount,couponDiscount=:couponDiscount,veteranDiscount=:veteranDiscount,calculateFuel=:calculateFuel,estimateTerms=:estimateTerms,estimateTermsCreditCard=:estimateTermsCreditCard,jobBoardFeature=:jobBoardFeature,FVPTerms=:FVPTerms WHERE orgId=:orgId",$binds,$errorVar);
        if(!$setIt) {
            return false;
        }else{
            return true;
        }

        return false;
    }

    function setJobAcceptanceTerms($jobAcceptanceTerms){

        $errorVar = array("movingSettings Class","setJobAcceptanceTerms()",4,"Notes",array());

        $binds = array();
        $binds[] = [":orgId",$this->orgId,PDO::PARAM_INT];
        $binds[] = [":jobAcceptanceTerms",$jobAcceptanceTerms,PDO::PARAM_STR];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.movingSettings SET jobAcceptanceTerms=:jobAcceptanceTerms WHERE orgId=:orgId",$binds,$errorVar);
        if(!$setIt) {
            return false;
        }else{
            return true;
        }

        return false;
    }

    function convertWeightToLBS($weightInCf = 0){

        $orgMoveSettings = $this->getData();

        if (!isset($orgMoveSettings['cflbsratio'])){
            $orgMoveSettings['cflbsratio'] = 7;
        }

        $weight = $weightInCf*$orgMoveSettings['cflbsratio'];

        return $weight;
    }

    private function checkDataBeforeSend(&$data){
        if ($data['cflbsratio'] < 0){
            $data['cflbsratio'] = 0;
        }
        if ($data['seniorDiscount'] < 0 || $data['seniorDiscount'] > 100){
            $data['seniorDiscount'] = 0;
        }
        if ($data['couponDiscount'] < 0 || $data['couponDiscount'] > 100){
            $data['couponDiscount'] = 0;
        }
        if ($data['veteranDiscount'] < 0 || $data['veteranDiscount'] > 100){
            $data['veteranDiscount'] = 0;
        }
        if ($data['calculateFuel'] < 0 || $data['calculateFuel'] > 100){
            $data['calculateFuel'] = 0;
        }
        return $data;
    }

    public function updateTariff($tariff = []){
        $errorVar = array("movingSettings Class","updateTariff()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':tariff', base64_encode(serialize($tariff)), PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.movingSettings SET tariff=:tariff WHERE orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }
        return false;
    }
}