<?php

class storage
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    function getStorages(){

        $errorVar = array("storage Class","getStorages()",4,"Notes",array());

        $storages = [];

        $binds = array();

        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,title,isActive FROM networkleads_moving_db.storage WHERE orgId=:orgId AND isDeleted=0 ORDER BY id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while($storage = $GLOBALS['connector']->fetch($getIt)){
                $storages[] = $storage;
            }
        }

        return $storages;

    }

    function getStorage($id){

        $errorVar = array("storage Class","getStorage()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.storage WHERE orgId=:orgId AND isDeleted=0 AND id=:id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            $storage = $GLOBALS['connector']->fetch($getIt);
            return $storage;

        }

        return false;

    }

    function deleteStorage($id){

        $errorVar = array("storage Class","deleteStorage()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.storage SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function activeStorage($id,$active){
        $errorVar = array("storage Class","activeStorage()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':active', $active, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.storage SET isActive=:active WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function addStorage($userId,$title,$address,$state,$city,$zip,$phone,$contact,$email,$pricePer100,$pricePer1000,$pricePerCrate,$tax,$lateFee,$flatRate){

        $errorVar = array("storage Class","addStorage()",4,"Notes",array());

        $pricePer100 = $this->checkBeforeSend($pricePer100);
        $pricePer1000 = $this->checkBeforeSend($pricePer1000);
        $pricePerCrate = $this->checkBeforeSend($pricePerCrate);
        $tax = $this->checkBeforeSend($tax);
        $lateFee = $this->checkBeforeSend($lateFee);
        $flatRate = $this->checkBeforeSend($flatRate);

        $binds = array();

        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':contact', $contact, PDO::PARAM_STR);
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':pricePer100', $pricePer100, PDO::PARAM_STR);
        $binds[] = array(':pricePer1000', $pricePer1000, PDO::PARAM_STR);
        $binds[] = array(':pricePerCrate', $pricePerCrate, PDO::PARAM_STR);
        $binds[] = array(':tax', $tax, PDO::PARAM_STR);
        $binds[] = array(':lateFee', $lateFee, PDO::PARAM_STR);
        $binds[] = array(':flatRate', $flatRate, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.storage (title,address,city,state,zip,phone,contact,email,price_daily_per_100,price_daily_per_1000,price_daily_per_crate,tax,lateFeePerDay,isActive,isDeleted,userCreated,orgId,comment,price_flat_rate) VALUES(:title,:address,:city,:state,:zip,:phone,:contact,:email,:pricePer100,:pricePer1000,:pricePerCrate,:tax,:lateFee,1,0,:userId,:orgId,'',:flatRate)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id();
        }

        return false;

    }

    function updateStorage($id,$title,$address,$state,$city,$zip,$phone,$contact,$email,$pricePer100,$pricePer1000,$pricePerCrate,$tax,$lateFee,$comment,$flatRate){

        $errorVar = array("trucks Class","updateTruck()",4,"Notes",array());

        $pricePer100 = $this->checkBeforeSend($pricePer100);
        $pricePer1000 = $this->checkBeforeSend($pricePer1000);
        $pricePerCrate = $this->checkBeforeSend($pricePerCrate);
        $tax = $this->checkBeforeSend($tax);
        $lateFee = $this->checkBeforeSend($lateFee);
        $flatRate = $this->checkBeforeSend($flatRate);

        $binds = array();

        $binds[] = array(':storageId', $id, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':contact', $contact, PDO::PARAM_STR);
        $binds[] = array(':email', $email, PDO::PARAM_STR);
        $binds[] = array(':pricePer100', $pricePer100, PDO::PARAM_STR);
        $binds[] = array(':pricePer1000', $pricePer1000, PDO::PARAM_STR);
        $binds[] = array(':pricePerCrate', $pricePerCrate, PDO::PARAM_STR);
        $binds[] = array(':tax', $tax, PDO::PARAM_STR);
        $binds[] = array(':lateFee', $lateFee, PDO::PARAM_STR);
        $binds[] = array(':comment', $comment, PDO::PARAM_STR);
        $binds[] = array(':flatRate', $flatRate, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.storage SET title=:title,address=:address,city=:city,state=:state,zip=:zip,phone=:phone,contact=:contact,email=:email,price_daily_per_100=:pricePer100,price_daily_per_1000=:pricePer1000,price_daily_per_crate=:pricePerCrate,tax=:tax,lateFeePerDay=:lateFee,comment=:comment,price_flat_rate=:flatRate WHERE id=:storageId AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }


    private function checkBeforeSend(&$var){
        if ($var < 0){
            $var = 0;
        }
        return $var;
    }

}