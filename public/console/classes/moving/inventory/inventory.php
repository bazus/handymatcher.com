<?php

class inventory
{

    private $orgId;

    function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    // ==================== GROUPS ====================
    function getGroups(){

        $errorVar = array("inventory Class","getGroups()",4,"Notes",array());

        $groups = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,title,createDate,orgId FROM networkleads_moving_db.inventoryGroups WHERE (orgId=:orgId OR orgId IS NULL) AND isDeleted=0 ORDER BY id",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while($group = $GLOBALS['connector']->fetch($getIt)){
                if($group["orgId"] == NULL){
                    // A default group that cant be deleted
                    $group["default"] = true;
                }else{
                    $group["default"] = false;
                }
                $group['createDate'] = date("F jS, Y",strtotime($group['createDate']));
                $groups[] = $group;
            }
        }

        return $groups;

    }

    function addGroup($title,$userId,$returnGroupId = false){

        $errorVar = array("inventory Class","addGroup()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.inventoryGroups (title,orgId,createDate,isDeleted,userIdCreated) VALUES(:title,:orgId,NOW(),0,:userId)",$binds,$errorVar);
        if(!$getIt){
            return $GLOBALS['connector']->getLastError();
        }else{
            if($returnGroupId == true){
                return $GLOBALS['connector']->last_insert_id();
            }else {
                return true;
            }
        }

        return false;

    }

    function deleteGroup($id){
        $errorVar = array("inventory Class","deleteGroup()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.inventoryGroups SET isDeleted=1,deleteDate=NOW() WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================== GROUPS ====================


    // ==================== ITEMS ====================
    function getItems($id){

        $errorVar = array("inventory Class","getItems()",4,"Notes",array());

        $items = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT ii.id,ii.title,ii.createDate,ii.cf,ig.orgId FROM networkleads_moving_db.inventoryItems AS ii INNER JOIN networkleads_moving_db.inventoryGroups AS ig ON ig.id=ii.groupId WHERE (ig.orgId=:orgId OR ig.orgId IS NULL) AND ii.groupId=:id AND ii.isDeleted=0 ORDER BY id",$binds,$errorVar);
        if(!$getIt){
            return $GLOBALS['connector']->getLastError();
        }else{
            while($item = $GLOBALS['connector']->fetch($getIt)){
                if($item["orgId"] == NULL){
                    // A default group that cant be deleted
                    $item["default"] = true;
                }else{
                    $item["default"] = false;
                }
                $item['createDate'] = date("F jS, Y",strtotime($item['createDate']));
                $items[] = $item;
            }
        }

        return $items;

    }

    function addItem($groupId,$title,$cf,$userId){

        $errorVar = array("inventory Class","addItem()",4,"Notes",array());

        $cf = $this->checkBeforeSend($cf);

        $binds = array();

        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':groupId', $groupId, PDO::PARAM_INT);
        $binds[] = array(':cf', $cf, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.inventoryItems (title,groupId,cf,createDate,isDeleted,userIdCreated) VALUES(:title,:groupId,:cf,NOW(),0,:userId)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }
    function updateItem($itemId,$value,$cf,$groupId){

        $orgValid = false;

        $errorVar = array("inventory Class","updateItem()-checkForUserOrg",4,"Notes",array());

        $cf = $this->checkBeforeSend($cf);

        $binds = array();

        $binds[] = array(':itemId', $itemId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $checkIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.inventoryItems AS ii INNER JOIN networkleads_moving_db.inventoryGroups AS ig ON ig.id=ii.groupId WHERE ii.id=:itemId AND ig.orgId=:orgId",$binds,$errorVar);

        if(!$checkIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($checkIt) > 0){
                $orgValid = true;
            }
        }

        if($orgValid == true){
                $errorVar = array("organization Class","updateItem()",4,"Notes",array());


                $binds = array();

                $binds[] = array(':itemId', $itemId, PDO::PARAM_STR);
                $binds[] = array(':title', $value, PDO::PARAM_STR);
                $binds[] = array(':cf', $cf, PDO::PARAM_STR);
                $binds[] = array(':groupId', $groupId, PDO::PARAM_INT);

                $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.inventoryItems SET title=:title, cf=:cf WHERE id=:itemId AND groupID=:groupId",$binds,$errorVar);
                if(!$getIt){
                    return false;
                }else{
                    return true;
                }

                return false;

        }

        return false;

    }
    function deleteItem($itemId){

        $errorVar = array("inventory Class","deleteItem()",4,"Notes",array());

        $groupValid = false;

        $binds = array();
        $binds[] = array(':itemId', $itemId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

            $checkIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.inventoryItems AS ii INNER JOIN networkleads_moving_db.inventoryGroups AS ig ON ig.id=ii.groupId WHERE ii.id=:itemId AND ig.orgId=:orgId",$binds,$errorVar);
        if(!$checkIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($checkIt) > 0){
                $groupValid = true;
            }
        }

        if($groupValid == true){
            $binds = array();
            $binds[] = array(':itemId', $itemId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.inventoryItems SET isDeleted=1,deleteDate=NOW() WHERE id=:itemId",$binds,$errorVar);
            if(!$getIt){
                return false;
            }else{
                return true;
            }
        }

        return false;
    }

    function getEstimateItem($itemId){
        $errorVar = array("inventory Class","getEstimateItem()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':itemId', $itemId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT ii.* FROM networkleads_moving_db.inventoryItems AS ii INNER JOIN networkleads_moving_db.inventoryGroups AS ig ON ig.id=ii.groupId WHERE ii.id=:itemId AND (ig.orgId=:orgId OR ig.orgId IS NULL)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->fetch($getIt);
        }
    }
    // ==================== ITEMS ====================

    private function checkBeforeSend(&$var){
        if ($var < 0){
            $var = 0;
        }
        return $var;
    }
}