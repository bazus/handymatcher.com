<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 24/10/2018
 * Time: 20:29
 */
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/passwords.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/timeController.php");

class payments
{
    private $orgId;

    function __construct($orgId){
        $this->orgId = $orgId;
    }

    public function getPaymentsByLeads($leadId = NULL){

        if($leadId === NULL){return false;}
        $errorVar = array("payments Class","getData()",4,"Notes",array());

        $payments = [];
        $resp = [];

        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT *,CTZ(paymentDate,:OFFSET) AS paymentDate FROM networkleads_moving_db.payments WHERE leadId=:leadId and isDeleted=0",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            $passwords = new passwords();
            $totalPayments = 0;

            while($res = $GLOBALS['connector']->fetch($getIt)){

                // calculate VAT
                $vatCalculation = $this->calculateVAT($res["amount"],$res["vatPer"],$res["vatIsInclusive"]);

                $res["vatAmount"] = $vatCalculation["vatAmount"];
                $res["paymentTotal"] = $vatCalculation["paymentTotal"];

                $res['creditCardNumber'] = str_replace(" ","-",$passwords->unHashCC(($res['creditCardNumber'])));;
                $res['creditCardNumber'] = substr($res['creditCardNumber'],-4);

                $totalPayments += $res["totalAmount"];
                $res['amount'] = number_format($res['amount'], 2, '.', ',');
                if(!isset($res["paymentDate"]) || $res["paymentDate"] == null || $res["paymentDate"] == ""){
                    $res['paymentDateText'] = "now";
                }else{
                    //$res['paymentDateText'] = date("M j Y, H:i",strtotime($res["paymentDate"]));
                    $timeController = new timeController();
                    $res["paymentDateText"] = $timeController->convertv2($res['paymentDate'],"m/d/Y H:i");

                }


                $payments[] = $res;
            }
        }

        $resp["paymentData"] = $payments;
        $resp["totalPayments"] = $totalPayments;

        $estimateCalculation = new estimateCalculation($leadId,$this->orgId);
        $estimateCalculationData = $estimateCalculation->getData();

        $paymentsLeft = NULL;
        if($estimateCalculationData!= false){
            if(($estimateCalculationData["totalEstimate"]-$totalPayments) >= 0) {
                $paymentsLeft = $estimateCalculationData["totalEstimate"] - $totalPayments;
            }
        }
        $resp["paymentsLeft"] = number_format($paymentsLeft, 2, '.', ',');


        return $resp;
    }

    public function getCreditCardAuthorizationByLeads($leadId = NULL){

        if($leadId === NULL){return false;}
        $errorVar = array("payments Class","getCreditCardAuthorizationByLeads()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT *,CTZ(signDate,:OFFSET) AS signDate FROM networkleads_db.creditCardAuthorizationForms WHERE leadId=:leadId",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            $timeController = new timeController();
            $ccafs = [];
            while($res = $GLOBALS['connector']->fetch($getIt)){
                $res["signDateText"] = $timeController->convertv2($res['signDate'],"m/d/Y H:i");
                $ccafs[] = $res;
            }
        }

        return $ccafs;
    }

    public function getPayment($paymentId = NULL){

        if($paymentId === NULL){return false;}

        $errorVar = array("payments Class","getPayment()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $paymentId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT p.*,u.fullName,CTZ(p.paymentDate,:OFFSET) AS paymentDate FROM networkleads_moving_db.payments AS p INNER JOIN networkleads_db.users AS u ON u.id=p.userIdCreated INNER JOIN networkleads_db.leads AS SingleLead ON SingleLead.id=p.leadId WHERE p.id=:id AND SingleLead.orgId=:orgId AND p.isDeleted=0",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            $payment = $GLOBALS['connector']->fetch($getIt,true);

            if($payment) {
                // calculate VAT
                $vatCalculation = $this->calculateVAT($payment["amount"],$payment["vatPer"],$payment["vatIsInclusive"]);

                $payment["vatAmount"] = $vatCalculation["vatAmount"];
                $payment["paymentTotal"] = $vatCalculation["paymentTotal"];

                $passwords = new passwords();
                $payment['creditCardNumber'] = str_replace(" ", "-", $passwords->unHashCC(($payment['creditCardNumber'])));;
                $payment['creditCardNumber'] = substr($payment['creditCardNumber'], -4);

                return $payment;
            }
        }
        return false;
    }

    public function getCreditCardAuthorization($ccafId = NULL){

        if($ccafId === NULL){return false;}

        $errorVar = array("payments Class","getCreditCardAuthorization()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $ccafId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT *,CTZ(signDate,:OFFSET) AS signDate FROM networkleads_db.creditCardAuthorizationForms WHERE id=:id AND orgId=:orgId",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            $timeController = new timeController();
            $res = $GLOBALS['connector']->fetch($getIt);
            $res["signDateText"] = $timeController->convertv2($res['signDate'],"m/d/Y H:i");
            return $res;
        }
        return false;
    }

    public function calculateVAT($amount,$vatPer,$vatIsInclusive,$returnCleanNumbers = false)
    {
        // $returnCleanNumbers - return the numbers without number format (5350.55 instead of 5,350.55)

        $vatAmount = 0;
        $paymentTotal = $amount;

        $resp["vatAmount"] = $vatAmount;
        $resp["paymentTotal"] = $paymentTotal;

        if($vatIsInclusive == "1"){
            $vatAmount = $amount-($amount/(1+($vatPer/100)));
        }elseif($vatIsInclusive == "0"){
            $vatAmount = ($amount/100.00)*$vatPer;
            $paymentTotal = $amount+$vatAmount;
        }


        if(!$returnCleanNumbers){
            // return number formatted
            $resp["vatAmount"] = number_format($vatAmount,2,".",",");
            $resp["paymentTotal"] = number_format($paymentTotal,2,".",",");
        }else{
            // return clean number (without ',')
            $resp["vatAmount"] = floatval(number_format($vatAmount,2,".",""));
            $resp["paymentTotal"] = floatval(number_format($paymentTotal,2,".",""));
        }

        return $resp;
    }

    public function saveData($data,$userId,$leadId){

        if($data["description"] == ""){
            $data["description"] = "Payment";
        }

        $errorVar = array("payments Class","saveData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        // convert "5,630.44" to "5630.44"
        $data["amount"] = floatval(preg_replace("/[^-0-9\.]/","",$data["amount"]));

        foreach ($data as $k=>$v){
            $binds[] = [":".$k,$v,PDO::PARAM_STR];
        }

        // calculate VAT
        $vatCalculation = $this->calculateVAT($data["amount"],$data["vatPer"],$data["vatIsInclusive"],true);

        $binds[] = [":totalAmount",$vatCalculation["paymentTotal"],PDO::PARAM_STR];
        $binds[] = [":vatAmount",$vatCalculation["vatAmount"],PDO::PARAM_STR];

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.payments (leadId,userIdCreated,method,description,amount,totalAmount,vatAmount,vatPer,vatIsInclusive,remarks,cardHolder,address,city,zip,country,phone,email,creditCardNumber,securityCode,expDate,creditCardConfirmation) VALUES(:leadId,:userId,:method,:description,:amount,:totalAmount,:vatAmount,:vatPer,:vatIsInclusive,:remarks,:cardHolder,:address,:city,:zip,:country,:phone,:email,:creditCardNumber,:securityCode,:expDate,:creditCardConfirmation)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return $GLOBALS['connector']->last_insert_id($setIt);
        }

        return false;


    }

    public function updateData($data,$id){

        $errorVar = array("payments Class","updateData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $id, PDO::PARAM_INT);
        foreach ($data as $k=>$v){
            $binds[] = [":".$k,$v,PDO::PARAM_STR];
        }

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.payments SET address=:address,city=:city,zip=:zip,country=:country,phone=:phone,email=:email,remarks=:remarks WHERE id=:id",$binds,$errorVar);

        if(!$setIt){
            return $GLOBALS['connector']->getLastError();
            return false;
        }else{
            return true;
        }

        return false;


    }

    public function getTotalPaymentsByDate($orgId,$compare = false,$sDate = false,$eDate = false,$label = ""){

        $errorVar = array("leads Class","getTotalForTheMonth()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT p.amount,p.totalAmount,CTZ(p.paymentDate,:OFFSET) AS paymentDate FROM networkleads_moving_db.payments AS p INNER JOIN networkleads_db.leads AS l on l.id=p.leadId WHERE l.orgId=:orgId AND CTZ(p.paymentDate,:OFFSET)>=:startDate AND CTZ(p.paymentDate,:OFFSET)<=:endDate AND l.isDeleted=0 AND p.isDeleted=0",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch($getIt);
            }else{
                $totalPerThisDate = 0;

                while($x = $GLOBALS['connector']->fetch($getIt)){
                    $totalPerThisDate = $totalPerThisDate+$x['totalAmount'];
                 }

                $data = [];
                $data['totalLeadPayments'] = number_format($totalPerThisDate, 2, '.', ',');
                $data['totalLeadPaymentsIsCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 day"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("-2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of -2 month"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("last day of -2 month"));
                }else if ($label == "Custom"){
                    $data['totalLeadPaymentsChange'] = 0;
                    $data['totalLeadPaymentsIsCustom'] = true;
                    return $data;
                }

                $binds = [];
                $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                $getIt = $GLOBALS['connector']->execute("SELECT p.amount,p.totalAmount,CTZ(p.paymentDate,:OFFSET) AS paymentDate FROM networkleads_moving_db.payments AS p INNER JOIN networkleads_db.leads AS l on l.id=p.leadId WHERE l.orgId=:orgId AND CTZ(paymentDate,:OFFSET)>=:startDate AND CTZ(paymentDate,:OFFSET)<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalPerNewDate = 0;
                    while($x = $GLOBALS['connector']->fetch($getIt)){
                        $totalPerNewDate = $totalPerNewDate+$x['totalAmount'];
                    }
                    if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                        $data['totalLeadPaymentsChange'] = 100;
                    }else if ($totalPerNewDate == $totalPerThisDate) {
                        $data['totalLeadPaymentsChange'] = 0;
                    }else{
                        $data['totalLeadPaymentsChange'] = (($totalPerThisDate-$totalPerNewDate)/$totalPerNewDate)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }

    public function deletePayment($paymentId){

        $errorVar = array("payments Class","deletePayment()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $paymentId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.payments AS p INNER JOIN networkleads_db.leads AS l ON l.id=p.leadId SET p.isDeleted=1 WHERE p.id=:id AND l.orgId=:orgId",$binds,$errorVar);

        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

}