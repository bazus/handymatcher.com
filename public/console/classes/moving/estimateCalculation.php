<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingSettings.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");


/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 28/10/2018
 * Time: 21:42
 */

class estimateCalculation{

    private $leadId;
    private $orgId;
    public $estimateHasCalculation = false;
    public $data = NULL;
    public $calculationData = NULL;

    function __construct($leadId = NULL,$orgId = NULL)
    {
        $this->orgId = $orgId;

        if ($leadId != NULL)
        {
            $errorVar = array("estimate Class", "__construct()", 4, "Notes", array());

            $binds = [];
            $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads WHERE leadId=:leadId", $binds, $errorVar);
            if (!$getIt) {
                return false;
            } else {
                if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                    $getIt = $GLOBALS['connector']->execute("SELECT id,leadId FROM networkleads_moving_db.leads WHERE leadId=:leadId", $binds, $errorVar);
                    $r = $GLOBALS['connector']->fetch($getIt);

                    $this->leadId = $r["id"];
                    $this->leadId = $leadId;

                    $binds = [];
                    $binds[] = array(':id', $this->leadId, PDO::PARAM_INT);

                    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.estimateCalculations WHERE leadId=:id AND isDeleted=0", $binds, $errorVar);
                    if (!$getIt) {
                        return false;
                    } else {
                        if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                            $this->estimateHasCalculation = true;
                        }
                    }
                }
            }
        }
    }

    public function getData($estimateId = NULL){
        // Get the estimate calculation data of the active estimate (only 1 estimate should be active, but let's get the LAST one)

        $errorVar = array("estimateCalculation Class","getData()",4,"Notes",array());

        $binds = array();

        // if $estimeIt is not null - then get the data by the estimate id
        if($estimateId != NULL){
            $binds[] = array(':id', $estimateId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.estimateCalculations WHERE id=:id AND isDeleted=0 AND isActive=1 ORDER BY id DESC LIMIT 1",$binds,$errorVar);
        }else{
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.estimateCalculations WHERE leadId=:leadId AND isDeleted=0 AND isActive=1 ORDER BY id DESC LIMIT 1",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            $data = $GLOBALS['connector']->fetch($getIt);

            if($data == false){return false;}

            try{
                if (isset($data['extraCharges']) && $data['extraCharges']) {
                    $data['extraCharges'] = unserialize($data['extraCharges']);
                } else {
                    $data['extraCharges'] = false;
                }

                if (isset($data['materials']) && $data['materials']) {
                    $data['materials'] = unserialize($data['materials']);
                } else {
                    $data['materials'] = false;
                }

                $this->data = $data;
                if ($data){
                    $calculateTotalEstimate = $this->calculateTotalEstimate($data);
                    $data['totalEstimate'] = $calculateTotalEstimate["totalPrice"];
                }
            }catch(Exception $x){
                unset($x);
                $data['extraCharges'] = false;
            }

            return $data;
        }
        return false;
    }

    public function setData($estimateId = NULL,$data = NULL){

        $errorVar = array("estimateCalculation Class", "setData()", 4, "Notes", array());

        if(isset($data["showEstimateTerms"]) && $data["showEstimateTerms"] == "false"){
            $data["showEstimateTerms"] = false;
        }else{
            $data["showEstimateTerms"] = true;
        }

        if(isset($data["showEstimateSign"]) && $data["showEstimateSign"] == "false"){
            $data["showEstimateSign"] = false;
        }else{
            $data["showEstimateSign"] = true;
        }

        if(isset($data["showEstimateInventory"]) && $data["showEstimateInventory"] == "false"){
            $data["showEstimateInventory"] = false;
        }else{
            $data["showEstimateInventory"] = true;
        }

        if(isset($data["showEstimatePayments"]) && $data["showEstimatePayments"] == "false"){
            $data["showEstimatePayments"] = false;
        }else{
            $data["showEstimatePayments"] = true;
        }

        if(isset($data["valueProtectionAdded"]) && $data["valueProtectionAdded"] == "true"){
            $data["valueProtectionAdded"] = true;
        }else{
            $data["valueProtectionAdded"] = false;
        }

        if(isset($data["valueProtectionType"]) && $data["valueProtectionType"] == "true"){
            $data["valueProtectionType"] = true;
        }else{
            $data["valueProtectionType"] = false;
        }


        // ======= (START) CHECK DATA =======
        if(!$data["initPrice"]){$data["initPrice"] = 0;}
        if(!$data["pricePerCf"]){$data["pricePerCf"] = 0;}
        if(!$data["fuel"]){$data["fuel"] = 0;}
        if(!$data["fuelType"]){$data["fuelType"] = 0;}
        if(!$data["coupon"]){$data["coupon"] = 0;}
        if(!$data["couponType"]){$data["couponType"] = 0;}
        if(!$data["senior"]){$data["senior"] = 0;}
        if(!$data["seniorType"]){$data["seniorType"] = 0;}
        if(!$data["veteranDiscount"]){$data["veteranDiscount"] = 0;}
        if(!$data["veteranDiscountType"]){$data["veteranDiscountType"] = 0;}
        if(!$data["packingPackers"]){$data["packingPackers"] = 0;}
        if(!$data["packingHours"]){$data["packingHours"] = 0;}
        if(!$data["packingPerHour"]){$data["packingPerHour"] = 0;}
        if(!$data["unpackingPackers"]){$data["unpackingPackers"] = 0;}
        if(!$data["unpackingHours"]){$data["unpackingHours"] = 0;}
        if(!$data["unpackingPerHour"]){$data["unpackingPerHour"] = 0;}
        if(!$data["discount"]){$data["discount"] = 0;}
        if(!$data["discountType"]){$data["discountType"] = 0;}
        if(!$data["storageFee"]){$data["storageFee"] = 0;}
        if(!$data["extraCharges"]){$data["extraCharges"] = serialize(array());}
        if(!$data["materials"]){$data["materials"] = serialize(array());}
        if(!$data["attachedFiles"]){$data["attachedFiles"] = serialize(array());}
        if(!$data["agentFee"]){$data["agentFee"] = 0;}
        if(!$data["agentFeeType"]){$data["agentFeeType"] = 0;}
        if(!$data["valueProtectionAOL"]){$data["valueProtectionAOL"] = 0;}
        if(!$data["valueProtectionDA"]){$data["valueProtectionDA"] = 0;}
        if(!$data["valueProtectionCharge"]){$data["valueProtectionCharge"] = 0;}
        if(!$data["calcType"]){$data["calcType"] = 0;}
        if(!$data["estimateComments"]){$data["estimateComments"] = "";}
        // ======= (END) CHECK DATA =======



        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':initPrice', $data["initPrice"], PDO::PARAM_STR);
        $binds[] = array(':pricePerCf', $data["pricePerCf"], PDO::PARAM_STR);
        $binds[] = array(':fuel', $data["fuel"], PDO::PARAM_STR);
        $binds[] = array(':fuelType', $data["fuelType"], PDO::PARAM_STR);
        $binds[] = array(':coupon', $data["coupon"], PDO::PARAM_STR);
        $binds[] = array(':couponType', $data["couponType"], PDO::PARAM_INT);
        $binds[] = array(':senior', $data["senior"], PDO::PARAM_STR);
        $binds[] = array(':seniorType', $data["seniorType"], PDO::PARAM_INT);
        $binds[] = array(':veteranDiscount', $data["veteranDiscount"], PDO::PARAM_STR);
        $binds[] = array(':veteranDiscountType', $data["veteranDiscountType"], PDO::PARAM_INT);
        $binds[] = array(':packingPackers', $data["packingPackers"], PDO::PARAM_STR);
        $binds[] = array(':packingHours', $data["packingHours"], PDO::PARAM_STR);
        $binds[] = array(':packingPerHour', $data["packingPerHour"], PDO::PARAM_STR);
        $binds[] = array(':unpackingPackers', $data["unpackingPackers"], PDO::PARAM_STR);
        $binds[] = array(':unpackingHours', $data["unpackingHours"], PDO::PARAM_STR);
        $binds[] = array(':unpackingPerHour', $data["unpackingPerHour"], PDO::PARAM_STR);
        $binds[] = array(':discount', $data["discount"], PDO::PARAM_STR);
        $binds[] = array(':discountType', $data["discountType"], PDO::PARAM_INT);
        $binds[] = array(':storageFee', $data["storageFee"], PDO::PARAM_STR);
        $binds[] = array(':extraCharges', $data["extraCharges"], PDO::PARAM_STR);
        $binds[] = array(':materials', $data["materials"], PDO::PARAM_STR);
        $binds[] = array(':attachedFiles', $data["attachedFiles"], PDO::PARAM_STR);
        $binds[] = array(':agentFee', $data["agentFee"], PDO::PARAM_STR);
        $binds[] = array(':agentFeeType', $data["agentFeeType"], PDO::PARAM_INT);
        $binds[] = array(':valueProtectionAdded', $data["valueProtectionAdded"], PDO::PARAM_BOOL);
        $binds[] = array(':valueProtectionType', $data["valueProtectionType"], PDO::PARAM_BOOL);
        $binds[] = array(':valueProtectionAOL', $data["valueProtectionAOL"], PDO::PARAM_STR);
        $binds[] = array(':valueProtectionDA', $data["valueProtectionDA"], PDO::PARAM_STR);
        $binds[] = array(':valueProtectionCharge', $data["valueProtectionCharge"], PDO::PARAM_STR);
        $binds[] = array(':calcType', $data["calcType"], PDO::PARAM_INT);
        $binds[] = array(':displayTerms', $data["showEstimateTerms"], PDO::PARAM_BOOL);
        $binds[] = array(':displaySigning', $data["showEstimateSign"], PDO::PARAM_BOOL);
        $binds[] = array(':displayInventory', $data["showEstimateInventory"], PDO::PARAM_BOOL);
        $binds[] = array(':displayPayments', $data["showEstimatePayments"], PDO::PARAM_BOOL);
        $binds[] = array(':estimateComments', $data["estimateComments"], PDO::PARAM_STR);

        if ($estimateId === NULL){

            // Set the company data
            $organization = new organization($this->orgId);
            $organizationData = $organization->getData();

            $companyData = [];
            $companyData["companyLogo"] = $organizationData["logoPath"];
            $companyData["companyName"] = $organizationData["organizationName"];
            $companyData["companyPhone"] = $organizationData["phone"];
            $companyData["companyWebsite"] = $organizationData["website"];
            $companyData["companyAddress"] = $organizationData["address"];
            $companyData["companyCity"] = $organizationData["city"];
            $companyData["companyState"] = $organizationData["state"];
            $companyData["companyZip"] = $organizationData["zip"];
            $companyData["companyDOT"] = $organizationData["DOT"];
            $companyData["companyICCMC"] = $organizationData["ICCMC"];

            $companyData = serialize($companyData);

            $binds[] = array(':companyData', $companyData, PDO::PARAM_STR);

            // Set all estimates for this lead as not active
            if($this->disableAllEstimates()){
                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.estimateCalculations(initPrice,pricePerCf,fuel,fuelType,coupon,couponType,senior,seniorType,veteranDiscount,veteranDiscountType,packingPackers,packingHours,packingPerHour,unpackingPackers,unpackingHours,unpackingPerHour,discount,discountType,storageFee,leadId,extraCharges,materials,attachedFiles,agentFee,agentFeeType,calcType,displayTerms,displaySigning,displayInventory,displayPayments,estimateComments,companyData,valueProtectionAdded,valueProtectionType,valueProtectionAOL,valueProtectionDA,valueProtectionCharge) VALUES(:initPrice,:pricePerCf,:fuel,:fuelType,:coupon,:couponType,:senior,:seniorType,:veteranDiscount,:veteranDiscountType,:packingPackers,:packingHours,:packingPerHour,:unpackingPackers,:unpackingHours,:unpackingPerHour,:discount,:discountType,:storageFee,:leadId,:extraCharges,:materials,:attachedFiles,:agentFee,:agentFeeType,:calcType,:displayTerms,:displaySigning,:displayInventory,:displayPayments,:estimateComments,:companyData,:valueProtectionAdded,:valueProtectionType,:valueProtectionAOL,:valueProtectionDA,:valueProtectionCharge)", $binds, $errorVar);
            }else{
                return false;
            }


        }else{
            $binds[] = array(':id', $estimateId, PDO::PARAM_INT);
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET initPrice=:initPrice,pricePerCf=:pricePerCf,fuel=:fuel,fuelType=:fuelType,coupon=:coupon,couponType=:couponType,senior=:senior,seniorType=:seniorType,veteranDiscount=:veteranDiscount,veteranDiscountType=:veteranDiscountType,packingPackers=:packingPackers,packingHours=:packingHours,packingPerHour=:packingPerHour,unpackingPackers=:unpackingPackers,unpackingHours=:unpackingHours,unpackingPerHour=:unpackingPerHour,discount=:discount,discountType=:discountType,storageFee=:storageFee,extraCharges=:extraCharges,materials=:materials,attachedFiles=:attachedFiles,agentFee=:agentFee,agentFeeType=:agentFeeType,calcType=:calcType,displayTerms=:displayTerms,displaySigning=:displaySigning,displayInventory=:displayInventory,displayPayments=:displayPayments,estimateComments=:estimateComments,valueProtectionAdded=:valueProtectionAdded,valueProtectionType=:valueProtectionType,valueProtectionAOL=:valueProtectionAOL,valueProtectionDA=:valueProtectionDA,valueProtectionCharge=:valueProtectionCharge WHERE id=:id AND leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        }
        if (!$setIt) {
            var_dump($GLOBALS['connector']->getLastError());
            return false;
        } else {
            return true;
        }

    }

    public function getTotalEstimate(){
        // (NOTE) [7.1.19)(NIV) - I am not sure if this function is in use, it's logic is wrong, need to check.
        // I am not updating this function (it will not calculate the materials in the estimate)

        if($this->data === NULL) {
            $this->getData();
        }
        $price = 0;

        if ($this->data !== NULL){

            // initial price * price per cf
            switch($this->data['calcType']){
                case 0: // LBS
                    $price += ($this->data['initPrice'] * $this->data['pricePerCf']);
                    break;
                case 1: // CF
                    $price += ($this->data['initPrice'] * $this->data['pricePerCf']);
                    break;
                case 2: // HOURS
                    $price += ($this->data['initPrice'] * $this->data['pricePerCf']);
                    break;
                case 3: // USD
                    $price += $this->data['initPrice'];
                    break;
            }

            $this->calculationData['initPrice'] = $price;
            $this->calculationData['totalCF'] = $this->data['initPrice'];
            $this->calculationData['perCF'] = $this->data['pricePerCf'];

            // CALCULATE THE 'CLEAN' AMOUNT OF CD/LBS/HOURS
            // for example, if the estimate is '10 CF - also return the amount of LBS
            // for another example, if the estimate is '10 LBS - also return the amount of CF
            $CF = 0;
            $LBS = 0;
            $HOURS = 0;
            $USD = 0;
            if($this->orgId != NULL) {
                $movingSettings = new movingSettings($this->orgId);
                $movingSettingsData = $movingSettings->getData();

                if ($this->data["calcType"] == "0") {
                    // Calculate by LBS
                    $LBS = (float)number_format($this->data["initPrice"], 2, '.', '');

                    if($movingSettingsData["cflbsratio"] == false){
                        // No custom ratio - use 7
                        $CF = (float)number_format(($this->data['initPrice']/7), 2, '.', '');
                    }else{
                        // Has custom ratio - use it
                        $CF = (float)number_format(($this->data['initPrice']/$movingSettingsData["cflbsratio"]), 2, '.', '');
                    }
                }
                if ($this->data["calcType"] == "1") {
                    // Calculate by CF
                    $CF = (float)number_format($this->data["initPrice"], 2, '.', '');

                    if($movingSettingsData["cflbsratio"] == false){
                        // No custom ratio - use 7
                        $LBS = (float)number_format(($this->data['initPrice']*7), 2, '.', '');
                    }else{
                        // Has custom ratio - use it
                        $LBS = (float)number_format(($this->data['initPrice']*$movingSettingsData["cflbsratio"]), 2, '.', '');
                    }
                }
                if ($this->data["calcType"] == "2") {
                    // Calculate by HOURS
                    $HOURS = $this->data["initPrice"];
                }
                if ($this->data["calcType"] == "3") {
                    // Calculate by USD
                    $USD = $this->data["initPrice"];
                }
            }

            $this->calculationData['totalRealLBS'] = $LBS;
            $this->calculationData['totalRealCF'] = $CF;
            $this->calculationData['totalRealHOURS'] = $HOURS;
            $this->calculationData['totalRealUSD'] = $USD;


            // =========== (START) FUEL ===========
            if ($this->data['fuel']) {
                $fuel = 0;
                switch($this->data["fuelType"]){
                    case 0:
                        $fuel = ($price / 100) * $this->data['fuel'];
                        break;
                    case 1:
                        $fuel = $this->data['fuel'];
                        break;
                }

                $this->calculationData['fuel'] = $fuel;
                $price += $fuel;
            }
            // =========== (END) FUEL ===========

            // =========== (START) PACKING ===========
            $this->calculationData['totalPacking'] = 0;
            if ($this->data['packingHours']) {
                $packPrice = ($this->data['packingHours'] * $this->data['packingPerHour']);
                $this->calculationData['packPrice'] = $packPrice;
                $this->calculationData['totalPacking'] += $packPrice;
                $price += $packPrice;
            }

            if ($this->data['unpackingHours']) {
                $unpackPrice = ($this->data['unpackingHours'] * $this->data['unpackingPerHour']);
                $this->calculationData['unpackPrice'] = $unpackPrice;
                $this->calculationData['totalPacking'] += $unpackPrice;
                $price += $unpackPrice;
            }
            // =========== (END) PACKING ===========

            // =========== (START) AGENT FEE ===========
            if ($this->data['agentFee']) {
                $agentFee = ($price /100) * $this->data['agentFee'];
                $this->calculationData['agentFee'] = $agentFee;
                $price += $agentFee;
            }
            // =========== (END) AGENT FEE ===========

            // =========== (START) EXTRA CHARGES ===========
            if ($this->data['extraCharges']) {
                $extraCharges = $this->data['extraCharges'];
                if ($extraCharges) {
                    $this->calculationData['extraCharges'] = [];
                    foreach ($extraCharges AS $charge) {
                        $this->calculationData['extraCharges'][] = ['title'=>$charge['title'],'price'=>$charge['price']];
                        $price += $charge['price'];
                    }
                }
            }
            // =========== (END) EXTRA CHARGES ===========

            // =========== (START) DISCOUNTS ===========
            if ($this->data['discount']) {
                $discount = ($price / 100) * $this->data['discount'];
                $this->calculationData['discount'] = $discount;
                $price -= $discount;
            }

            if ($this->data['coupon']) {
                $coupon = ($price / 100) * $this->data['coupon'];
                $this->calculationData['coupon'] = $coupon;
                $price -= $coupon;
            }

            if ($this->data['senior']) {
                $senior = ($price / 100) * $this->data['senior'];
                $this->calculationData['senior'] = $senior;
                $price -= $senior;
            }

            if ($this->data['veteranDiscount']) {
                $veteran = ($price / 100) * $this->data['veteranDiscount'];
                $this->calculationData['veteran'] = $veteran;
                $price -= $veteran;
            }
            // =========== (END) DISCOUNTS ===========

        }

        return $price;

    }

    public function calculateTotalEstimate($estimateData = NULL){

        if($estimateData === NULL) {return false;}

        $calculationData = [];
        $price = 0;

        if ($estimateData !== NULL){

            // initial price * price per cf
            switch($estimateData['calcType']){
                case 0: // LBS
                    $price += ($estimateData['initPrice'] * $estimateData['pricePerCf']);
                    break;
                case 1: // CF
                    $price += ($estimateData['initPrice'] * $estimateData['pricePerCf']);
                    break;
                case 2: // HOURS
                    $price += ($estimateData['initPrice'] * $estimateData['pricePerCf']);
                    break;
                case 3: // USD
                    $price += $estimateData['initPrice'];
                    break;
            }

            $calculationData['initPrice'] = $price;
            $calculationData['totalCF'] = $estimateData['initPrice'];
            $calculationData['perCF'] = $estimateData['pricePerCf'];

            // CALCULATE THE 'CLEAN' AMOUNT OF CD/LBS/HOURS
            // for example, if the estimate is '10 CF - also return the amount of LBS
            // for another example, if the estimate is '10 LBS - also return the amount of CF
            $CF = 0;
            $LBS = 0;
            $HOURS = 0;
            $USD = 0;
            if($this->orgId != NULL) {
                $movingSettings = new movingSettings($this->orgId);
                $movingSettingsData = $movingSettings->getData();

                if ($estimateData["calcType"] == "0") {
                    // Calculate by LBS
                    $LBS = (float)number_format($estimateData["initPrice"], 2, '.', '');

                    if($movingSettingsData["cflbsratio"] == false){
                        // No custom ratio - use 7
                        $CF = (float)number_format(($estimateData['initPrice']/7), 2, '.', '');
                    }else{
                        // Has custom ratio - use it
                        $CF = (float)number_format(($estimateData['initPrice']/$movingSettingsData["cflbsratio"]), 2, '.', '');
                    }
                }
                if ($estimateData["calcType"] == "1") {
                    // Calculate by CF
                    $CF = (float)number_format($estimateData["initPrice"], 2, '.', '');

                    if($movingSettingsData["cflbsratio"] == false){
                        // No custom ratio - use 7
                        $LBS = (float)number_format(($estimateData['initPrice']*7), 2, '.', '');
                    }else{
                        // Has custom ratio - use it
                        $LBS = (float)number_format(($estimateData['initPrice']*$movingSettingsData["cflbsratio"]), 2, '.', '');
                    }
                }
                if ($estimateData["calcType"] == "2") {
                    // Calculate by HOURS
                    $HOURS = $estimateData["initPrice"];
                }
                if ($estimateData["calcType"] == "3") {
                    // Calculate by USD
                    $USD = $estimateData["initPrice"];
                }
            }

            $calculationData['totalRealLBS'] = $LBS;
            $calculationData['totalRealCF'] = $CF;
            $calculationData['totalRealHOURS'] = $HOURS;
            $calculationData['totalRealUSD'] = $USD;


            if ($estimateData['fuel']) {

                $fuel = 0;
                switch($estimateData["fuelType"]){
                    case 0:
                        $fuel = ($price / 100) * $estimateData['fuel'];
                        break;
                    case 1:
                        $fuel = $estimateData['fuel'];
                        break;
                }

                $calculationData['fuel'] = $fuel;
                $price += $fuel;
            }


            $calculationData['totalPacking'] = 0;
            if ($estimateData['packingHours']) {
                $packPrice = ($estimateData['packingHours'] * $estimateData['packingPerHour']);
                $calculationData['packPrice'] = $packPrice;
                $calculationData['totalPacking'] += $packPrice;
                $price += $packPrice;
            }

            if ($estimateData['unpackingHours']) {
                $unpackPrice = ($estimateData['unpackingHours'] * $estimateData['unpackingPerHour']);
                $calculationData['unpackPrice'] = $unpackPrice;
                $calculationData['totalPacking'] += $unpackPrice;
                $price += $unpackPrice;
            }

            if ($estimateData['agentFee']) {
                if($estimateData["agentFeeType"] == "0"){
                    $agentFee = ($price /100) * $estimateData['agentFee'];
                }else{
                    $agentFee = $estimateData['agentFee'];
                }
                $calculationData['agentFee'] = $agentFee;
                $price += $agentFee;
            }

            if ($estimateData['extraCharges']) {
                $extraCharges = $estimateData['extraCharges'];
                if ($extraCharges) {
                    $calculationData['extraCharges'] = [];
                    foreach ($extraCharges AS $charge) {
                        $calculationData['extraCharges'][] = ['title'=>$charge['title'],'price'=>$charge['price']];
                        $price += $charge['price'];
                    }
                }
            }

            if ($estimateData['materials']) {
                $materials = $estimateData['materials'];
                if ($materials) {
                    $calculationData['materials'] = [];
                    foreach ($materials AS $material) {
                        $calculationData['materials'][] = ['title'=>$material['title'],'amount'=>$material['amount'],'pricePerItem'=>$material['pricePerItem'],'totalPrice'=>$material['totalPrice']];
                        $price += $material['totalPrice'];
                    }
                }
            }


            if ($estimateData['valueProtectionCharge'] !== false) {
                $calculationData['valueProtectionAdded'] = $estimateData['valueProtectionAdded'];
                $calculationData['valueProtectionType'] = $estimateData['valueProtectionType'];
                $calculationData['valueProtectionCharge'] = $estimateData['valueProtectionCharge'];
                $price += $estimateData['valueProtectionCharge'];
            }


            if ($estimateData['discount']) {
                if($estimateData["discountType"] == "0"){
                    $discount = ($price / 100) * $estimateData['discount'];
                }else{
                    $discount = $estimateData['discount'];
                }

                $calculationData['discount'] = $discount;
                $price -= $discount;
            }

            if ($estimateData['coupon']) {
                if($estimateData["couponType"] == "0") {
                    $coupon = ($price / 100) * $estimateData['coupon'];
                }else{
                    $coupon = $estimateData['coupon'];
                }
                $calculationData['coupon'] = $coupon;
                $price -= $coupon;
            }

            if ($estimateData['senior']) {
                if($estimateData["seniorType"] == "0") {
                    $senior = ($price / 100) * $estimateData['senior'];
                }else{
                    $senior = $estimateData['senior'];
                }
                $calculationData['senior'] = $senior;
                $price -= $senior;
            }

            if ($estimateData['veteranDiscount']) {
                if($estimateData["veteranDiscountType"] == "0") {
                    $veteran = ($price / 100) * $estimateData['veteranDiscount'];
                }else{
                    $veteran = $estimateData['veteranDiscount'];
                }
                $calculationData['veteran'] = $veteran;
                $price -= $veteran;
            }

            $calculationData['totalPrice'] = $price;


        }

        return $calculationData;

    }

    public function addSignature($estimateId,$signature,$customerName,$userIP = ""){

        $errorVar = array("estimateCalculation Class", "addSignature()", 4, "Notes", array());

        $binds = [];

        $binds[] = [":id",$estimateId,PDO::PARAM_INT];
        $binds[] = [":leadId",$this->leadId,PDO::PARAM_INT];
        $binds[] = [':customerName',$customerName,PDO::PARAM_STR];
        $binds[] = [':customerSignature',$signature,PDO::PARAM_STR];
        $binds[] = [':customerIP',$userIP,PDO::PARAM_STR];

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET customerName=:customerName,customerSignature=:customerSignature,dateSigned=CURRENT_TIMESTAMP,customerIP=:customerIP WHERE id=:id AND leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            return true;
        }

    }

    public function resetSignature($estimateId){

        $errorVar = array("estimateCalculation Class", "resetSignature()", 4, "Notes", array());

        $binds = [];
        $binds[] = [":id",$estimateId,PDO::PARAM_INT];
        $binds[] = [":leadId",$this->leadId,PDO::PARAM_INT];
        $binds[] = [':customerName',NULL,PDO::PARAM_NULL];
        $binds[] = [":customerSignature",NULL,PDO::PARAM_NULL];
        $binds[] = [":dateSigned",NULL,PDO::PARAM_NULL];

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET customerName=:customerName,customerSignature=:customerSignature,dateSigned=:dateSigned WHERE id=:id AND leadId=:leadId",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            return true;
        }

    }

    public function deleteEstimate($estimateId = NULL,$createNew = true){
        if($estimateId === NULL){return;}

        $errorVar = array("estimateCalculation Class", "deleteEstimate()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':id', $estimateId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET isDeleted=1 WHERE id=:id AND leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        if (!$getIt) {
            return false;
        } else {
            return true;
        }
    }

    public function activateEstimate($estimateId = NULL){
        if($estimateId === NULL){return;}

        // First - disable all the estimates
        $this->disableAllEstimates();

        $errorVar = array("estimateCalculation Class", "activateEstimate()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':id', $estimateId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET isActive=1 WHERE id=:id AND leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        if (!$getIt) {
            return false;
        } else {
            return true;
        }
    }

    public function disableEstimate($estimateId = NULL){
        if($estimateId === NULL){return;}

        $errorVar = array("estimateCalculation Class", "disableEstimate()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':id', $estimateId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET isActive=0 WHERE id=:id AND leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        if (!$getIt) {
            return false;
        } else {
            return true;
        }
    }

    public function disableAllEstimates(){
        $errorVar = array("estimateCalculation Class", "disableAllEstimates()", 4, "Notes", array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.estimateCalculations SET isActive=0 WHERE leadId=:leadId", $binds, $errorVar);
        if (!$getIt) {
            return false;
        } else {
            return true;
        }
    }


}