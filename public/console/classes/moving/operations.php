<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/crew.php");

class operations
{
    private $userId;
    private $orgId;
    private $leadId;

    function __construct($userId,$orgId,$leadId){
        $this->userId = $userId;
        $this->orgId = $orgId;
        $this->leadId = $leadId;
    }

    function getData(){

        $errorVar = array("operations Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET',NULL,PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT CTZ(ml.requestedDeliveryDateStart,:OFFSET) AS requestedDeliveryDateStart,CTZ(ml.boxDeliveryDateStart,:OFFSET) AS boxDeliveryDateStart,CTZ(ml.pickupDateStart,:OFFSET) AS pickupDateStart,CTZ(ml.requestedDeliveryDateEnd,:OFFSET) AS requestedDeliveryDateEnd,CTZ(ml.boxDeliveryDateEnd,:OFFSET) AS boxDeliveryDateEnd,CTZ(ml.pickupDateEnd,:OFFSET) AS pickupDateEnd,ml.includeBoxDeliveryDate,ml.includePickupDate,ml.includeRequestDeliveryDate FROM networkleads_moving_db.leads AS ml WHERE ml.leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return [];
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            return $r;
        }

        return false;

    }

    function getAssigned(){

        $operation = [];
        $operation['trucks'] = [];
        $operation['carriers'] = [];
        $operation['crew'] = [];

        // Get Lead Moving Trucks Assigned To Lead
        $trucksAssigned = $this->getTrucksAssigned();
        for ($i = 0;$i<count($trucksAssigned);$i++){
            $operation['trucks'][] = ["assignId"=>$trucksAssigned[$i]["id"],"name"=>$trucksAssigned[$i]["data"]["title"],"phone"=>null];
        }

        // Get Lead Moving Carriers Assigned To Lead
        $carriersAssigned = $this->getCarriersAssigned();
        for ($i = 0;$i<count($carriersAssigned);$i++){
            $operation['carriers'][] = ["assignId"=>$carriersAssigned[$i]["id"],"name"=>$carriersAssigned[$i]["data"]["name"],"phone"=>$carriersAssigned[$i]["data"]["phone1"]];
        }

        // Get Lead Moving Crew Assigned To Lead
        $crewAssigned = $this->getCrewAssigned();
        for ($i = 0;$i<count($crewAssigned);$i++){
            $operation['crew'][] = ["assignId"=>$crewAssigned[$i]["id"],"name"=>$crewAssigned[$i]["data"]["name"],"type"=>$crewAssigned[$i]["data"]["type"],"phone"=>$crewAssigned[$i]["data"]["phone"]];
        }

        return $operation;
    }

    // ========== TRUCKS, CARRIERS AND CREW ==========
    public function getTrucksAssigned(){

        $trucksList = [];
        $errorVar = array("movingLead Class","getTrucksAssigned()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,leadId,truckId,assignedAt FROM networkleads_moving_db.trucksAssigned WHERE leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        if (!$getIt) {
            // Error
        } else {
            while($truck = $GLOBALS['connector']->fetch($getIt)){
                $trucks = new trucks($this->orgId);
                $truckData = $trucks->getTruck($truck['truckId']);
                if($truckData){
                    $truck["data"] = $truckData;
                    $trucksList[] = $truck;
                }
            }
        }

        return $trucksList;

    }

    public function getCarriersAssigned(){

        $carriersList = [];
        $errorVar = array("movingLead Class","getCarriersAssigned()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,leadId,carrierId,assignedAt FROM networkleads_moving_db.carriersAssigned WHERE leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        if (!$getIt) {
            // Error
        } else {
            while($carrier = $GLOBALS['connector']->fetch($getIt)){
                $carriers = new carriers($this->orgId);
                $carrierData = $carriers->getSingleCarrier($carrier["carrierId"]);
                if($carrierData){
                    $carrier["data"] = $carrierData;
                    $carriersList[] = $carrier;
                }
            }
        }

        return $carriersList;

    }

    public function getCrewAssigned(){

        $crewList = [];
        $errorVar = array("movingLead Class","getCrewAssigned()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id,leadId,crewId,assignedAt FROM networkleads_moving_db.crewAssigned WHERE leadId=:leadId AND isDeleted=0", $binds, $errorVar);
        if (!$getIt) {
            // Error
        } else {
            while($crewAssigned = $GLOBALS['connector']->fetch($getIt)){
                $crew = new crew($this->orgId);
                $crewData = $crew->getSingleCrew($crewAssigned["crewId"]);
                if($crewData){
                    $crewAssigned["data"] = $crewData;
                    $crewList[] = $crewAssigned;
                }

            }
        }

        return $crewList;

    }

    public function assignTrucks($trucks = []){
        if(count($trucks) == 0 ){return;}

        $errorVar = array("movingLead Class","assignTrucks()",4,"Notes",array());

            $status = true;
            foreach ($trucks as $truck) {
                $binds = [];
                $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
                $binds[] = array(':truckId', $truck, PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.trucksAssigned(leadId,truckId) VALUES(:leadId,:truckId)", $binds, $errorVar);
                if (!$setIt) {
                    $status = false;
                } else {
                    //return true;
                }
            }
            return $status;

        return false;

    }

    public function assignCarriers($carriers = []){

        if(count($carriers) == 0 ){return;}

        $errorVar = array("movingLead Class","assignCarriers()",4,"Notes",array());

            $status = true;
            foreach ($carriers as $carrierId) {
                $binds = [];
                $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
                $binds[] = array(':carrierId', $carrierId, PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO  networkleads_moving_db.carriersAssigned(leadId,carrierId) VALUES(:leadId,:carrierId)", $binds, $errorVar);
                if (!$setIt) {
                    $status = false;
                } else {
                    //return true;
                }
            }
            return $status;

        return false;
    }

    public function assignCrew($crew = []){

        if(count($crew) == 0 ){return;}

        $errorVar = array("movingLead Class","assignCrew()",4,"Notes",array());

            $status = true;
            foreach ($crew as $crewId) {
                $binds = [];
                $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
                $binds[] = array(':crewId', $crewId, PDO::PARAM_STR);

                $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.crewAssigned(leadId,crewId) VALUES(:leadId,:crewId)", $binds, $errorVar);
                if (!$setIt) {
                    $status = false;
                } else {
                    //return true;
                }
            }
            return $status;

        return false;

    }

    public function unAssignTruck($truckAssignedId = NULL){
        if($truckAssignedId === NULL ){return;}

        $errorVar = array("movingLead Class","unAssignTruck()",4,"Notes",array());

            $binds = [];
            $binds[] = array(':id', $truckAssignedId, PDO::PARAM_STR);
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.trucksAssigned SET isDeleted=1 WHERE leadId=:leadId AND id=:id", $binds, $errorVar);
            if (!$setIt) {
                return false;
            } else {
                return true;
            }

        return false;

    }

    public function unAssignCarrier($carrierAssignedId = NULL){
        if($carrierAssignedId === NULL ){return;}

        $errorVar = array("movingLead Class","unAssignCarrier()",4,"Notes",array());

            $binds = [];
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
            $binds[] = array(':id', $carrierAssignedId, PDO::PARAM_STR);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.carriersAssigned SET isDeleted=1 WHERE leadId=:leadId AND id=:id", $binds, $errorVar);
            if (!$setIt) {
                return false;
            } else {
                return true;
            }

        return false;

    }

    public function unAssignCrew($crewAssignedId = NULL){
        if($crewAssignedId === NULL ){return;}

        $errorVar = array("movingLead Class","unAssignCrew()",4,"Notes",array());

            $binds = [];
            $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
            $binds[] = array(':id', $crewAssignedId, PDO::PARAM_STR);

            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.crewAssigned SET isDeleted=1 WHERE leadId=:leadId AND id=:id", $binds, $errorVar);
            if (!$setIt) {
                return false;
            } else {
                return true;
            }

        return false;

    }

    public function checkIfCrewAssignedToJob($crewId){
        //return true if crew assigned to another job
        
        $errorVar = array("movingLead Class","checkIfCrewAssignedToJob()",4,"Notes",array());

        $leadData = $this->getData();
        $pickupDateStart = $leadData["pickupDateStart"];
        $pickupDateEnd = $leadData["pickupDateEnd"];

        $binds = [];
        $binds[] = array(':crewId', $crewId, PDO::PARAM_STR);
        $binds[] = array(':pickupDateStart', $pickupDateStart, PDO::PARAM_STR);
        $binds[] = array(':pickupDateEnd', $pickupDateEnd, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR, true);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS ml INNER JOIN networkleads_moving_db.crewAssigned AS ca ON ca.leadId=ml.leadId INNER JOIN networkleads_db.leads AS l ON ml.leadId=l.id WHERE crewId=:crewId AND ca.isDeleted=0 AND l.isDeleted=0 AND (ml.status=3 OR ml.status=4 OR ml.status=5) AND (CTZ(ml.pickupDateStart,:OFFSET)<=:pickupDateEnd AND CTZ(ml.pickupDateEnd,:OFFSET)>=:pickupDateStart)", $binds, $errorVar);
        if (!$getIt) {
            return false;
        } else {
            if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                return true;
            }else {
                return false;
            }
        }

    }
    // ========== TRUCKS, CARRIERS AND CREW ==========

}