<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/materials.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/trucks.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/carriers.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/estimateCalculation.php");

/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 01/10/2018
 * Time: 23:23
 */

class movingLead
{
    protected $leadId;
    protected $orgId;
    private $data = NULL;

    function __construct($leadId,$orgId){
        $this->leadId = $leadId;
        $this->orgId = $orgId;
    }

    public function getData(){

        $errorVar = array("movingLead Class","getData()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT ml.*,lp.providerName,CTZ(ml.boxDeliveryDateStart,:OFFSET) AS boxDeliveryDateStart,CTZ(ml.boxDeliveryDateEnd,:OFFSET) AS boxDeliveryDateEnd,CTZ(ml.requestedDeliveryDateStart,:OFFSET) AS requestedDeliveryDateStart,CTZ(ml.requestedDeliveryDateEnd,:OFFSET) AS requestedDeliveryDateEnd,CTZ(ml.pickupDateStart,:OFFSET) AS pickupDateStart,CTZ(ml.pickupDateEnd,:OFFSET) AS pickupDateEnd FROM networkleads_db.leads AS l INNER JOIN networkleads_moving_db.leads AS ml ON ml.leadId=l.id LEFT JOIN networkleads_db.leadProviders AS lp ON lp.id=l.providerId  WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.id=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $lead = $GLOBALS['connector']->fetch($getIt);
            if(!$lead){return false;}
            if($lead){
                $lead['moveDate'] = date("Y-m-d",strtotime($lead['moveDate']));
            }

            // if no materials set, return an empty array
            if($lead["moveMaterials"] == NULL || !$lead["moveMaterials"]){
                $lead["moveMaterials"] = [];
            }


            // Convert lead status to text
            $lead["statusText"] = "";
            switch($lead["status"]){
                case "0":
                    $lead["statusText"] = "New";
                    break;
                case "1":
                    $lead["statusText"] = "Pending";
                    break;
                case "2":
                    $lead["statusText"] = "Quoted";
                    break;
                case "3":
                    $lead["statusText"] = "Booked";
                    break;
                case "4":
                    $lead["statusText"] = "In progress";
                    break;
                case "5":
                    $lead["statusText"] = "Storage/Transit";
                    break;
                case "6":
                    $lead["statusText"] = "Completed";
                    break;
                case "7":
                    $lead["statusText"] = "Cancelled";
                    break;
            }


            $this->data = $lead;
            return $lead;
        }

        return false;
    }

    public function checkLeadEstimateExists(){
        $errorVar = array("movingLead Class","checkLeadEstimateExists()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads WHERE leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }
        }

        return false;
    }

    public function checkLeadOrganization(){
        $errorVar = array("movingLead Class","checkLeadOrganization()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.leads WHERE id=:leadId AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }
        }

        return false;
    }

    public function updateMovingDetails($fromZip,$toZip,$fromState,$toState,$fromCity,$toCity,$fromAddress,$toAddress,$moveDate,$moveSize,$moveSizeLBS,$fromLevel,$fromFloor,$fromAptNumber,$toAptNumber,$fromAptType,$toAptType,$toLevel,$toFloor,$typeOfMove,$autoTransport,$estimator = "",$needStorage = false,$bindingType = NULL){

        $errorVar = array("movingLead Class","createEstimate()",4,"Notes",array());

        $moveDate = date("Y-m-d H:i:s",strtotime($moveDate));

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':fromZip', $fromZip, PDO::PARAM_STR);
        $binds[] = array(':toZip', $toZip, PDO::PARAM_STR);
        $binds[] = array(':fromState', $fromState, PDO::PARAM_STR);
        $binds[] = array(':toState', $toState, PDO::PARAM_STR);
        $binds[] = array(':fromCity', $fromCity, PDO::PARAM_STR);
        $binds[] = array(':toCity', $toCity, PDO::PARAM_STR);
        $binds[] = array(':fromAddress', $fromAddress, PDO::PARAM_STR);
        $binds[] = array(':toAddress', $toAddress, PDO::PARAM_STR);
        $binds[] = array(':moveDate', $moveDate, PDO::PARAM_STR);
        $binds[] = array(':moveSize', $moveSize, PDO::PARAM_STR);
        $binds[] = array(':moveSizeLBS', $moveSizeLBS, PDO::PARAM_STR);
        $binds[] = array(':fromLevel', $fromLevel, PDO::PARAM_STR);
        $binds[] = array(':fromFloor', $fromFloor, PDO::PARAM_STR);
        $binds[] = array(':fromAptNumber', $fromAptNumber, PDO::PARAM_STR);
        $binds[] = array(':toAptNumber', $toAptNumber, PDO::PARAM_STR);
        $binds[] = array(':fromAptType', $fromAptType, PDO::PARAM_STR);
        $binds[] = array(':toAptType', $toAptType, PDO::PARAM_STR);
        $binds[] = array(':toLevel', $toLevel, PDO::PARAM_STR);
        $binds[] = array(':toFloor', $toFloor, PDO::PARAM_STR);
        $binds[] = array(':typeOfMove', $typeOfMove, PDO::PARAM_INT);
        $binds[] = array(':autoTransport', $autoTransport, PDO::PARAM_STR);
        $binds[] = array(':estimator', $estimator, PDO::PARAM_STR);
        $binds[] = array(':needStorage', filter_var($needStorage, FILTER_VALIDATE_BOOLEAN), PDO::PARAM_BOOL);

        if($bindingType === NULL || $bindingType == ""){
            $binds[] = array(':bindingType', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':bindingType', $bindingType, PDO::PARAM_INT);
        }

        if($this->checkLeadOrganization()){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET fromZip=:fromZip, toZip=:toZip,fromState=:fromState, toState=:toState,fromCity=:fromCity, toCity=:toCity,fromAddress=:fromAddress, toAddress=:toAddress, moveDate=:moveDate, moveSize=:moveSize, moveSizeLBS=:moveSizeLBS,fromLevel=:fromLevel, fromFloor=:fromFloor, fromAptNumber=:fromAptNumber, fromAptType=:fromAptType, toLevel=:toLevel, toFloor=:toFloor, toAptNumber=:toAptNumber, toAptType=:toAptType, typeOfMove=:typeOfMove , autoTransport=:autoTransport , estimator=:estimator , needStorage=:needStorage , bindingType=:bindingType WHERE leadId=:leadId",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                return true;
            }
        }

        return false;

    }

    public function updateInventory($items){

        // Updates the items in estimate inventory
        $errorVar = array("estimate Class","updateEstimate()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':items', $items, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET moveInventory=:items WHERE leadId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateEstimateDetails($estimateData){

        $errorVar = array("Moving Lead Class","updateEstimateDetails()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $this->leadId, PDO::PARAM_INT);
        foreach ($estimateData as $k=>$v){
            $binds[] = [":".$k,$v,PDO::PARAM_STR];
        }
        $binds[] = [":OFFSET",NULL,PDO::PARAM_STR,true];

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET departmentId=:departmentId,priority=:priority,reference=:reference,boxDeliveryDateStart=CTZ_TDB(:boxDeliveryDateStart,:OFFSET),boxDeliveryDateEnd=CTZ_TDB(:boxDeliveryDateEnd,:OFFSET),pickupDateStart=CTZ_TDB(:pickupDateStart,:OFFSET),pickupDateEnd=CTZ_TDB(:pickupDateEnd,:OFFSET),requestedDeliveryDateStart=CTZ_TDB(:requestedDeliveryDateStart,:OFFSET),requestedDeliveryDateEnd=CTZ_TDB(:requestedDeliveryDateEnd,:OFFSET),includeBoxDeliveryDate=:includeBoxDeliveryDate,includePickupDate=:includePickupDate,includeRequestDeliveryDate=:includeRequestDeliveryDate WHERE leadId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateOperationDate($dateS,$dateE,$type){

        $errorVar = array("Moving Lead Class","updateOperationDate()",4,"Notes",array());

        $binds = [];
        $binds[] = [":dateS",$dateS,PDO::PARAM_STR];
        $binds[] = [":dateE",$dateE,PDO::PARAM_STR];
        $binds[] = [':leadId', $this->leadId, PDO::PARAM_INT];
        $type2 = str_replace("Start","End",$type);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET ".$type."=:dateS,".$type2."=:dateE WHERE leadId=:leadId",$binds,$errorVar);
        if (!$setIt){
            return $GLOBALS['connector']->getLastError();
            return false;
        }else{
            return true;
        }
    }

    public function getTotalBookedEstimatesByDate($compare = false,$sDate = false,$eDate = false,$label){

        $errorVar = array("estimate Class","getTotalBookedEstimatesByDate()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS ml INNER JOIN networkleads_db.leads AS l ON l.id=ml.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND (ml.status>=3 AND ml.status!=7) AND CTZ(ml.dateReceived,:OFFSET)>=:startDate AND CTZ(ml.dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                $totalPerThisDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalBookedEstimates'] = $totalPerThisDate;
                $data['totalBookedEstimatesIsCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 day"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("-2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of -2 month"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("last day of -2 month"));
                }else if ($label == "Custom"){
                    $data['totalBookedEstimatesChange'] = 0;
                    $data['totalBookedEstimatesIsCustom'] = true;
                    return $data;
                }

                $binds = [];
                $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS ml INNER JOIN networkleads_db.leads AS l ON l.id=ml.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND (ml.status>=3 AND ml.status!=7) AND CTZ(ml.dateReceived,:OFFSET)>=:startDate AND CTZ(ml.dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);
                    if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                        $data['totalBookedEstimatesChange'] = 100;
                    }else if ($totalPerThisDate == $totalPerNewDate) {
                        $data['totalBookedEstimatesChange'] = 0;
                    }else{
                        $data['totalBookedEstimatesChange'] = (($totalPerThisDate-$totalPerNewDate)/$totalPerNewDate)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }

    public function getTotalFinishedJobsByDate($compare = false,$sDate = false,$eDate = false,$label){

        $errorVar = array("estimate Class","getTotalFinishedJobsByDate()",4,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS ml INNER JOIN networkleads_db.leads AS l ON l.id=ml.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.isBadLead=0 AND ml.status=6 AND CTZ(ml.dateReceived,:OFFSET)>=:startDate AND CTZ(ml.dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                $totalPerThisDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalFinishedJobs'] = $totalPerThisDate;
                $data['totalFinishedJobsIsCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 day"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("-2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of -2 month"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("last day of -2 month"));
                }else if ($label == "Custom"){
                    $data['totalFinishedJobsChange'] = 0;
                    $data['totalFinishedJobsIsCustom'] = true;
                    return $data;
                }

                $binds = [];
                $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_moving_db.leads AS ml INNER JOIN networkleads_db.leads AS l ON l.id=ml.leadId WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.isBadLead=0 AND ml.status=6 AND CTZ(ml.dateReceived,:OFFSET)>=:startDate AND CTZ(ml.dateReceived,:OFFSET)<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);
                    if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                        $data['totalFinishedJobsChange'] = 100;
                    }else if ($totalPerThisDate == $totalPerNewDate) {
                        $data['totalFinishedJobsChange'] = 0;
                    }else{
                        $data['totalFinishedJobsChange'] = (($totalPerThisDate-$totalPerNewDate)/$totalPerNewDate)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }


    public function getEstimates($estimateId = NULL){

        $estimates = [];

        $errorVar = array("movingLead Class","getEstimates()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        if($estimateId === NULL){
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.estimateCalculations WHERE leadId=:leadId AND isDeleted=0 ORDER BY id ASC LIMIT 20",$binds,$errorVar);
        }else{
            $binds[] = array(':id', $estimateId, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_moving_db.estimateCalculations WHERE id=:id AND leadId=:leadId AND isDeleted=0",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{

            $estimateCalculation = new estimateCalculation($this->leadId,$this->orgId);
            while($estimate = $GLOBALS['connector']->fetch($getIt)) {
                if (isset($estimate['extraCharges']) && $estimate['extraCharges']) {
                    $estimate['extraCharges'] = unserialize($estimate['extraCharges']);
                } else {
                    $estimate['extraCharges'] = false;
                }

                if (isset($estimate['materials']) && $estimate['materials']) {
                    $estimate['materials'] = unserialize($estimate['materials']);
                } else {
                    $estimate['materials'] = false;
                }

                if (isset($estimate['attachedFiles']) && $estimate['attachedFiles']) {
                    $estimate['attachedFiles'] = unserialize($estimate['attachedFiles']);
                } else {
                    $estimate['attachedFiles'] = false;
                }

                $dateSigned = $estimate["dateSigned"];

                $estimate['dateSigned'] = date("m/d/Y",strtotime($dateSigned));
                $estimate['timeSigned'] = date("h:i A",strtotime($dateSigned));

                $estimateCalculationData = $estimateCalculation->calculateTotalEstimate($estimate);
                $estimate['estimateCalculationData'] = $estimateCalculationData;
                $estimate['totalEstimate'] = $estimateCalculationData["totalPrice"];

                $estimates[] = $estimate;
            }
        }


        return $estimates;
    }

    public function setLeadStatus($status){

        $errorVar = array("movingLead Class","setLeadStatus()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':status', $status, PDO::PARAM_INT);

        if($this->checkLeadOrganization()){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET status=:status WHERE leadId=:leadId",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    public function getLeadStatus(){

        $errorVar = array("movingLead Class","getLeadStatus()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        if($this->checkLeadOrganization()){
            $getIt = $GLOBALS['connector']->execute("SELECT status FROM networkleads_moving_db.leads WHERE leadId=:leadId",$binds,$errorVar);
            if(!$getIt){
                return false;
            }else{
                $leadStatus = $GLOBALS['connector']->fetch($getIt);
                return array_values($leadStatus);
            }
        }

        return false;

    }


    public function setLeadWeight($weight){
        // NOTE : the weight must be saved in LBS

        $errorVar = array("estimate Class","setLeadWeight()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':id', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':weight', $weight, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET moveSizeLBS=:weight WHERE leadId=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function getInventoryTotalWeight(){
        if($this->data === NULL) {
            $leadData = $this->getData();
        }else{
            $leadData = $this->data;
        }
        $moveInventory = unserialize($leadData["moveInventory"]);

        if($moveInventory == false){return 0;}

        $totalCF = 0;
        foreach ($moveInventory as $item){
            if (isset($item['showCF']) && $item['showCF'] == "true") {
                $totalCF += ($item["cf"] * $item['amount']);
            }else if (!isset($item['showCF'])){
                $totalCF += ($item["cf"] * $item['amount']);
            }
        }

        return $totalCF;
    }

    public function setUpdateInfo($fromState,$toState,$fromCity,$toCity,$fromZip,$toZip,$fromAddress,$toAddress,$fromLevel,$toLevel,$fromFloor,$toFloor,$fromApartment,$toApartment,$moveDate,$needStorage){

        $errorVar = array("movingLead Class","setUpdateInfo()",4,"Notes",array());

        $moveDate = date("Y-m-d",strtotime($moveDate));

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':fState', $fromState, PDO::PARAM_STR);
        $binds[] = array(':tState', $toState, PDO::PARAM_STR);
        $binds[] = array(':fCity', $fromCity, PDO::PARAM_STR);
        $binds[] = array(':tCity', $toCity, PDO::PARAM_STR);
        $binds[] = array(':fZip', $fromZip, PDO::PARAM_STR);
        $binds[] = array(':tZip', $toZip, PDO::PARAM_STR);
        $binds[] = array(':fAddress', $fromAddress, PDO::PARAM_STR);
        $binds[] = array(':tAddress', $toAddress, PDO::PARAM_STR);
        $binds[] = array(':fLevel', $fromLevel, PDO::PARAM_STR);
        $binds[] = array(':tLevel', $toLevel, PDO::PARAM_STR);
        $binds[] = array(':fFloor', $fromFloor, PDO::PARAM_STR);
        $binds[] = array(':tFloor', $toFloor, PDO::PARAM_STR);
        $binds[] = array(':fApart', $fromApartment, PDO::PARAM_STR);
        $binds[] = array(':tApart', $toApartment, PDO::PARAM_STR);
        $binds[] = array(':moveDate', $moveDate, PDO::PARAM_STR);
        $binds[] = array(':needStorage', $needStorage, PDO::PARAM_STR);


        if($this->checkLeadOrganization()){
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET fromState=:fState,toState=:tState,fromCity=:fCity,toCity=:tCity,fromZip=:fZip,toZip=:tZip,fromAddress=:fAddress,toAddress=:tAddress,fromLevel=:fLevel,toLevel=:tLevel,fromFloor=:fFloor,toFloor=:tFloor,fromAptNumber=:fApart,toAptNumber=:tApart,moveDate=:moveDate,needStorage=:needStorage WHERE leadId=:leadId",$binds,$errorVar);
            if(!$setIt){
                return false;
            }else{
                return true;
            }
        }

        return false;

    }

    public function getMaterials(){

        $errorVar = array("movingLead Class","getMaterials()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT ml.moveMaterials FROM networkleads_db.leads AS l INNER JOIN networkleads_moving_db.leads AS ml ON ml.leadId=l.id WHERE l.orgId=:orgId AND l.isDeleted=0 AND l.id=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else {
            $movingLead = $GLOBALS['connector']->fetch($getIt);

            if($movingLead["moveMaterials"] == false || $movingLead["moveMaterials"] == ""){
                $moveMaterials = [];
            }else{
                $moveMaterials = unserialize($movingLead["moveMaterials"]);
            }
        }

      return $moveMaterials;
    }

    public function addMaterial($materialId){

        $moveMaterials = $this->getMaterials();

        // check if an item already exists in list, if so - add it's quantity by 1
        $itemExistsInList = false;
        foreach ($moveMaterials as &$singleMoveMaterial){
            if($singleMoveMaterial["id"] == $materialId){
                $itemExistsInList = true;
                $singleMoveMaterial["quantity"]++;
            }
        }
        if(!$itemExistsInList){
            $materials = new materials($this->orgId);
            $materialToAdd = $materials->getMaterial($materialId);

            $singleMaterial = [];
            $singleMaterial["id"] = $materialToAdd["id"];
            $singleMaterial["quantity"] = 1;
            $singleMaterial["price"] = $materialToAdd["localPrice"];

            $moveMaterials[] = $singleMaterial;
        }

        $errorVar = array("estimate Class","updateMaterials()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':moveMaterials', serialize($moveMaterials), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET moveMaterials=:moveMaterials WHERE leadId=:leadId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function decreaseMaterial($materialId){

        $moveMaterials = $this->getMaterials();

        // check if an item already exists in list, if so - add it's quantity by 1
        foreach ($moveMaterials as $k=>$v){
            if($moveMaterials[$k]["id"] == $materialId){

                if($moveMaterials[$k]["quantity"] == 1){
                    unset($moveMaterials[$k]);
                }else{
                    $moveMaterials[$k]["quantity"]--;
                }


            }
        }

        $moveMaterials = array_values($moveMaterials); // 'reindex' array


        $errorVar = array("estimate Class","updateMaterials()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':moveMaterials', serialize($moveMaterials), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET moveMaterials=:moveMaterials WHERE leadId=:leadId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function removeMaterial($materialId){

        $moveMaterials = $this->getMaterials();

        // check if an item already exists in list, if so - add it's quantity by 1
        foreach ($moveMaterials as $k=>&$singleMoveMaterial){
            if($singleMoveMaterial["id"] == $materialId){
                unset($moveMaterials[$k]);
            }
        }

        $moveMaterials = array_values($moveMaterials); // 'reindex' array

        $errorVar = array("estimate Class","updateMaterials()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':moveMaterials', serialize($moveMaterials), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET moveMaterials=:moveMaterials WHERE leadId=:leadId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function updateMaterials($saveArray){

        $errorVar = array("estimate Class","updateMaterials()",4,"Notes",array());

        $binds = [];
        $binds[] = array(':leadId', $this->leadId, PDO::PARAM_INT);
        $binds[] = array(':save', serialize($saveArray), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.leads SET moveMaterials=:save WHERE leadId=:leadId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    public function getTotalLeadsStatusByDate($sDate,$eDate){

        $errorVar = array("movingLead Class","getTotalLeadsStatusByDate()",3,"Notes",array());

        $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        $eDate = date("Y-m-d 23:59:59",strtotime($eDate));

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT ml.status FROM networkleads_moving_db.leads AS ml INNER JOIN networkleads_db.leads AS l ON ml.leadId=l.id WHERE l.orgId=:orgId AND l.isBadLead=0 AND CTZ(l.dateReceived,:OFFSET)>=:startDate AND CTZ(l.dateReceived,:OFFSET)<=:endDate AND l.isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            $stats = [];
            while($status = $GLOBALS['connector']->fetch($getIt)) {
                if(key_exists($status["status"],$stats)){
                    $stats[$status["status"]]++;
                }else{
                    $stats[$status["status"]] = 1;
                }
            }
            return $stats;
        }
        return false;

    }
}