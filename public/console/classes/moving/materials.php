<?php

class materials
{

    private $orgId;

    function __construct($orgId){$this->orgId = $orgId;}

    function getMaterials($ignoreDeleted = false){
        $errorVar = array("materials Class","getMaterials()",4,"Notes",array());

        $materials = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        if ($ignoreDeleted) {
            $getIt = $GLOBALS['connector']->execute("SELECT id,title,cost,localPrice,longPrice FROM networkleads_moving_db.materials WHERE orgId=:orgId ORDER BY id", $binds, $errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT id,title,cost,localPrice,longPrice FROM networkleads_moving_db.materials WHERE orgId=:orgId AND isDeleted=0 ORDER BY id", $binds, $errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            while($material = $GLOBALS['connector']->fetch($getIt,true)){
                $materials[] = $material;
            }
        }
        return $materials;
    }

    function getMaterial($id,$ignoreDeleted = false){
        $errorVar = array("materials Class","getMaterial()",4,"Notes",array());

        $material = [];

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);
        if ($ignoreDeleted) {
            $getIt = $GLOBALS['connector']->execute("SELECT id,title,cost,localPrice,longPrice FROM networkleads_moving_db.materials WHERE orgId=:orgId AND id=:id ORDER BY id", $binds, $errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT id,title,cost,localPrice,longPrice FROM networkleads_moving_db.materials WHERE orgId=:orgId AND isDeleted=0 AND id=:id ORDER BY id", $binds, $errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            $material = $GLOBALS['connector']->fetch($getIt,true);
        }
        return $material;
    }

    function deleteMaterial($id){
        $errorVar = array("materials Class","deleteMaterial()",4,"Notes",array());

        $binds = array();

        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.materials SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    function addMaterial($userId){
        $errorVar = array("trucks Class","addTruck()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_moving_db.materials (title,cost,localPrice,longPrice,isDeleted,orgId,userIdCreated) VALUES('Material Name',0.00,0.00,0.00,0,:orgId,:userId)",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{

            $id = $GLOBALS['connector']->last_insert_id();
            $data = ['id'=>$id,'title'=>'Material Name','cost'=>'0.00','localPrice'=>'0.00','longPrice'=>'0.00'];

            return $data;
        }

        return false;
    }

    function updateMaterial($id,$title,$cost,$local,$long){
        $errorVar = array("materials Class","updateMaterial()",4,"Notes",array());

        $long = $this->checkBeforeSend($long);
        $cost = $this->checkBeforeSend($cost);
        $local = $this->checkBeforeSend($local);

        $binds = array();

        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':id', $id, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':cost', $cost, PDO::PARAM_STR);
        $binds[] = array(':localPrice', $local, PDO::PARAM_STR);
        $binds[] = array(':long', $long, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_moving_db.materials SET title=:title,cost=:cost,localPrice=:localPrice,longPrice=:long WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }

    private function checkBeforeSend(&$var){
        if ($var < 0 || !$var){
            $var = 0;
        }
        return $var;
    }
}