<?php

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (14.8.18) 2018 Niv Apo
 * @license   ---
 */


class department{

    private $departmentId;
    private $organizationId;
    private $departmentData;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($departmentId = "",$organizationId = "") {
        $this->departmentId = $departmentId;
        $this->organizationId = $organizationId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getData() ======================================
    public function getData() {
        $errorVar = array("departments Class","getData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $this->departmentId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.departments WHERE orgId=:orgId AND id=:id AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $res = $GLOBALS['connector']->fetch($getIt);
            $this->departmentData = $res;
            return $res;
        }

        return false;

    }
    // ==================================== START getData() ======================================

    // ==================================== START updateData() ======================================
    public function updateData($title,$address,$city,$state,$zip,$phone,$owner,$description){

        $errorVar = array("department Class","updateData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $this->departmentId, PDO::PARAM_INT);
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':owner', $owner, PDO::PARAM_STR);
        $binds[] = array(':description', $description, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.departments SET title=:title,address=:address,city=:city,state=:state,zip=:zip,phone=:phone,owner=:owner,description=:description WHERE id=:id AND orgId=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START updateData() ======================================

    // ==================================== START deleteDepartment() ======================================
    public function deleteDepartment(){

        $errorVar = array("department Class","deleteDepartment()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $this->departmentId, PDO::PARAM_INT);
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.departments SET isDeleted=1 WHERE id=:id AND orgId=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START deleteDepartment() ======================================

}