<?php

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (9.8.18) 2018 Niv Apo
 * @license   ---
 */

class organizationPack{

    private $myPackId = NULL;
    private $orgsInPack = [];

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($orgId = NULL) {
        $errorVar = array("organizationPack","getOrgsInPack()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $orgId, PDO::PARAM_STR);


        $getIt = $GLOBALS['connector']->execute("SELECT a.packId AS myPackId,b.id,b.organizationName FROM networkleads_db.organizations AS a INNER JOIN networkleads_db.organizations as b ON b.packId=a.packId  WHERE a.id=:orgId",$binds,$errorVar);
        if(!$getIt){

        }else{
            $orgsInPack = [];
            $thePackId = NULL;
            while($org = $GLOBALS['connector']->fetch($getIt)){
                $thePackId = $org["myPackId"];
                $orgsInPack[] = $org;
            }
            $this->myPackId = $thePackId;
            $this->orgsInPack = $orgsInPack;
        }
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getOrgsInPack() ======================================
    public function getOrgsInPack() {
        return $this->orgsInPack;
    }
    // ==================================== START getOrgsInPack() ======================================


    // ==================================== START switchOrg() ======================================
    public function switchToOrg($orgId = NULL) {

        if($this->checkIfOrgIsInMyPack($orgId)){
            // Org is in my pack

            var_dump("OK");

        }else{
            var_dump("NO");
        }


    }
    // ==================================== START switchOrg() ======================================

    // ==================================== START checkIfOrgIsInMyPack() ======================================
    public function checkIfOrgIsInMyPack($orgId = NULL) {

        if($this->myPackId != NULL) {
            $errorVar = array("organizationPack", "checkIfOrgIsInMyPack()", 3, "Notes", array());

            $binds = array();
            $binds[] = array(':id', $orgId, PDO::PARAM_INT);
            $binds[] = array(':myPackId', $this->myPackId, PDO::PARAM_INT);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_db.organizations WHERE id=:id AND packId=:myPackId", $binds, $errorVar);
            if (!$getIt) {

            } else {
                if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                    return true;
                }
            }
        }

        return false;
    }
    // ==================================== START checkIfOrgIsInMyPack() ======================================


}