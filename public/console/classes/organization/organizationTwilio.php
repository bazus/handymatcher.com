<?php

use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/billing.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (15.5.19) 2019 Niv Apo
 * @license   ---
 */


class organizationTwilio{

    private $organizationId = NULL;
    private $userId = NULL;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($organizationId = NULL,$userId = NULL) {
        $this->organizationId = $organizationId;
        $this->userId = $userId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getSubAccount() ======================================
    public function getSubAccount($getById = NULL) {

        $errorVar = array("organizationTwilio Class","getSubAccount()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        if($getById === NULL){
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.twilio_subAccounts WHERE orgId=:orgId",$binds,$errorVar);
        }else{
            $binds[] = array(':id', $getById, PDO::PARAM_INT);
            $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.twilio_subAccounts WHERE orgId=:orgId AND id=:id",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            $subAccounts = $GLOBALS['connector']->fetch($getIt);
            return $subAccounts;
        }

        return false;
    }
    // ==================================== START getSubAccount() ======================================

    // ==================================== START getSubAccountByPhoneId() ======================================
    public function getSubAccountIdByPhoneId($phoneId = NULL) {

        if($phoneId === NULL){
            return false;
        }

        $errorVar = array("organizationTwilio Class","getSubAccountByPhoneId()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':phoneId', $phoneId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT sa.* FROM networkleads_db.twilio_phoneNumbers AS pn INNER JOIN networkleads_db.twilio_subAccounts AS sa ON sa.id=pn.subAccountId WHERE sa.orgId=:orgId AND pn.id=:phoneId AND pn.isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            return $r["id"];
        }

        return false;
    }
    // ==================================== START getSubAccountByPhoneId() ======================================

    // ==================================== START createSubAccount() ======================================
    public function createSubAccount($SID,$token,$accountName) {

        $errorVar = array("organizationTwilio Class","createSubAccount()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);
        $binds[] = array(':token', $token, PDO::PARAM_STR);
        $binds[] = array(':userCreated', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':accountName', $accountName, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.twilio_subAccounts(orgId,SID,token,userCreated,accountName) VALUES(:orgId,:SID,:token,:userCreated,:accountName)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START createSubAccount() ======================================

    // ==================================== START disableSubAccount() ======================================
    public function disableSubAccount($SID,$token) {

        $errorVar = array("organizationTwilio Class","disableSubAccount()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);
        $binds[] = array(':token', $token, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.twilio_subAccounts SET isActive=0 WHERE SID=:SID AND token=:token AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START disableSubAccount() ======================================

    // ==================================== START activateSubAccount() ======================================
    public function activateSubAccount($SID,$token) {

        $errorVar = array("organizationTwilio Class","activateSubAccount()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);
        $binds[] = array(':token', $token, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.twilio_subAccounts SET isActive=1 WHERE SID=:SID AND token=:token AND orgId=:orgId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START activateSubAccount() ======================================

    // ==================================== START getPhoneNumbers() ======================================
    public function getPhoneNumbers($subAccountId = NULL,$getById = NULL) {

        $errorVar = array("organizationTwilio Class","getPhoneNumbers()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        if($subAccountId === NULL){
            $getIt = $GLOBALS['connector']->execute("SELECT pn.* FROM networkleads_db.twilio_phoneNumbers AS pn INNER JOIN networkleads_db.twilio_subAccounts AS sa ON sa.id=pn.subAccountId WHERE sa.orgId=:orgId AND sa.orgId=:orgId AND pn.isDeleted=0", $binds, $errorVar);
        }else {
            $binds[] = array(':subAccountId', $subAccountId, PDO::PARAM_INT);
            if ($getById === NULL) {
                $getIt = $GLOBALS['connector']->execute("SELECT pn.* FROM networkleads_db.twilio_phoneNumbers AS pn INNER JOIN networkleads_db.twilio_subAccounts AS sa ON sa.id=pn.subAccountId WHERE sa.orgId=:orgId AND sa.id=:subAccountId AND pn.isDeleted=0", $binds, $errorVar);
            } else {
                $binds[] = array(':id', $getById, PDO::PARAM_INT);
                $getIt = $GLOBALS['connector']->execute("SELECT pn.* FROM networkleads_db.twilio_phoneNumbers AS pn INNER JOIN networkleads_db.twilio_subAccounts AS sa ON sa.id=pn.subAccountId WHERE pn.id=:id AND sa.orgId=:orgId AND sa.id=:subAccountId AND pn.isDeleted=0", $binds, $errorVar);
            }
        }
        if(!$getIt){
            return false;
        }else{
            if($subAccountId != NULL && $getById != NULL) {
                $r = $GLOBALS['connector']->fetch($getIt);
                return $r;
            }

            $phones = array();
            while($r = $GLOBALS['connector']->fetch($getIt)) {
                $phones[] = $r;
            }
        }

        return $phones;
    }
    // ==================================== START getPhoneNumbers() ======================================

    // ==================================== START addNewPhoneNumber() ======================================
    public function addNewPhoneNumber($subAccountId,$number,$SID,$state = NULL,$postalCode = NULL) {

        $errorVar = array("organizationTwilio Class","addNewPhoneNumber()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':subAccountId', $subAccountId, PDO::PARAM_INT);
        $binds[] = array(':number', $number, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':postalCode', $postalCode, PDO::PARAM_STR);
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.twilio_phoneNumbers(subAccountId,number,SID,state,postalCode) VALUES(:subAccountId,:number,:SID,:state,:postalCode)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START addNewPhoneNumber() ======================================

    // ==================================== START deletePhoneNumber() ======================================
    public function deletePhoneNumber($subAccountId,$SID) {


        $errorVar = array("organizationTwilio Class","deletePhoneNumber()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':subAccountId', $subAccountId, PDO::PARAM_INT);
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.twilio_phoneNumbers SET isDeleted=1,deletedAt=NOW(),deletedBy=:userId WHERE SID=:SID AND subAccountId=:subAccountId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START deletePhoneNumber() ======================================

    // ==================================== START sendSMS() ======================================
    public function sendSMS($fromPhoneNumberId,$toNumber,$msg,$leadId = NULL) {


        // the response array to print
        $resp = [];
        $resp["status"] = false;
        $resp["resp"] = "";


        if($leadId != NULL){
            // If sent to lead - get the phone number by lead id
            $lead = new lead($leadId,$this->organizationId);
            $leadData = $lead->getData();

            if($toNumber == NULL){
                // If there was no phone number passed - get the default phone number of the lead
                $toNumber = $leadData["phone"];
            }

            // === (START) If sent to lead - convert tags ===
            $updateInventoryLink = $_SERVER["YMQ_URL"]."/" . $leadData["secretKey"];

            $msg = str_replace("[inventorylink]", $updateInventoryLink, $msg);

            if (strpos($msg, '[FirstName]') !== false || strpos($msg, '[LastName]') !== false) {
                $msg = str_replace("[FirstName]", $leadData["firstname"], $msg);
                $msg = str_replace("[LastName]", $leadData["lastname"], $msg);
            }

            if (strpos($msg, '[FromCity]') !== false || strpos($msg, '[FromState]') !== false || strpos($msg, '[ToCity]') !== false || strpos($msg, '[ToState]') !== false) {
                $movingLead = new movingLead($leadId,$this->organizationId);
                $movingLeadData = $movingLead->getData();

                if(strpos($msg, 'in [FromCity], [FromState]') !== false && ($movingLeadData["fromCity"] == "" || $movingLeadData["fromState"] == "")){
                    $msg = str_replace("in [FromCity], [FromState]", "in your area", $msg);
                }

                $msg = str_replace("[FromCity]", $movingLeadData["fromCity"], $msg);
                $msg = str_replace("[FromState]", $movingLeadData["fromState"], $msg);
                $msg = str_replace("[ToCity]", $movingLeadData["toCity"], $msg);
                $msg = str_replace("[ToState]", $movingLeadData["toState"], $msg);
            }
            // === (END) If sent to lead - convert tags ===
        }

        // === (START) convert tags ===
        if (strpos($msg, '[CompanyName]') !== false) {
            $organization = new organization($this->organizationId);
            $organizationData = $organization->getData();

            $msg = str_replace("[CompanyName]", $organizationData["organizationName"], $msg);
        }
        // === (END) convert tags ===

        $subAccountData = $this->getSubAccount();
        $phoneNumberData = $this->getPhoneNumbers($subAccountData["id"],$fromPhoneNumberId);

        // Make sure the number format is +18181234567
        $toNumber = $this->unPrettifyPhone($toNumber);

        $resp["fromNumber"] = $phoneNumberData["number"];
        $resp["toNumber"] = $toNumber;

        // ==== (START) check if has balance ====
        // I am checking here because I need to first get the 'from phone' from the account
        $billing = new billing($this->organizationId);
        $doesHaveBalance = $billing->doesHaveBalance(0.01); // The cost for a single outgoing sms

        if($doesHaveBalance == false){
            $resp["status"] = false;
            $resp["resp"] = "balance";
            return $resp;
        }
        // ==== (END) check if has balance ====

        try {
            $client = new Client($subAccountData["SID"], $subAccountData["token"]);
            $twilio = new twilio($client);

            // send the SMS from twilio
            $sendSMS = $twilio->sendSMS($phoneNumberData["number"], $toNumber, $msg);

            $response = [];
            $response["sid"] = $sendSMS->sid;
            $response["status"] = $sendSMS->status;
            $response["accountSid"] = $sendSMS->accountSid;
            $response["messagingServiceSid"] = $sendSMS->messagingServiceSid;
            $response["dateCreated"] = $sendSMS->dateCreated;
            $response["dateUpdated"] = $sendSMS->dateUpdated;
            $response["dateSent"] = $sendSMS->dateSent;
            $response["errorCode"] = $sendSMS->errorCode;
            $response["errorMessage"] = $sendSMS->errorMessage;
            $response["price"] = $sendSMS->price;

            // charge the client for the sms
            $billing->create(0, 0.01, 4, $sendSMS->sid, "", "");

            if($leadId === NULL){
                // save the SMS in our db
                $addNewPhoneNumber = $this->saveNewSMS($sendSMS->sid, $phoneNumberData["number"], $toNumber, $msg, $response);
            }else{
                // save the SMS in our db
                $addNewPhoneNumber = $this->saveNewLeadSMS($leadId, $sendSMS->sid, $phoneNumberData["number"], $toNumber, $msg, $response);
            }

            $resp["status"] = true;
            $resp["resp"] = $addNewPhoneNumber;
        }catch (TwilioException $e){
            $resp["status"] = false;
            $resp["resp"] = $e->getStatusCode();
        }

        return $resp;


    }
    // ==================================== START sendSMS() ======================================

    // ==================================== START saveNewSMS() ======================================
    public function saveNewSMS($SID,$fromNumber,$toNumber,$msg,$allData) {

        $errorVar = array("organizationTwilio Class","saveNewSMS()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);
        $binds[] = array(':fromNumber', $fromNumber, PDO::PARAM_STR);
        $binds[] = array(':toNumber', $toNumber, PDO::PARAM_STR);
        $binds[] = array(':msg', $msg, PDO::PARAM_STR);
        $binds[] = array(':sentByUser', $this->userId, PDO::PARAM_STR);
        $binds[] = array(':allData', serialize($allData), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.twilio_outgoingSMS(SID,fromNumber,toNumber,sentByUser,msg,allData,isOutGoing) VALUES(:SID,:fromNumber,:toNumber,:sentByUser,:msg,:allData,1)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START saveNewSMS() ======================================

    // ==================================== START saveNewLeadSMS() ======================================
    public function saveNewLeadSMS($leadId,$SID,$fromNumber,$toNumber,$msg,$allData) {

        $errorVar = array("organizationTwilio Class","saveNewSMS()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);
        $binds[] = array(':SID', $SID, PDO::PARAM_STR);
        $binds[] = array(':fromNumber', $fromNumber, PDO::PARAM_STR);
        $binds[] = array(':toNumber', $toNumber, PDO::PARAM_STR);
        $binds[] = array(':msg', $msg, PDO::PARAM_STR);
        $binds[] = array(':sentByUser', $this->userId, PDO::PARAM_STR);
        $binds[] = array(':allData', serialize($allData), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.twilio_outgoingLeadsSMS(leadId,SID,fromNumber,toNumber,sentByUser,msg,allData,isOutGoing) VALUES(:leadId,:SID,:fromNumber,:toNumber,:sentByUser,:msg,:allData,1)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START saveNewLeadSMS() ======================================

    // ==================================== START getConversation() ======================================
    public function getConversation($fromPhoneNumber,$toPhoneNumber) {

        $errorVar = array("organizationTwilio Class","getConversation()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_STR);
        $binds[] = array(':fromNumber', $fromPhoneNumber, PDO::PARAM_STR);
        $binds[] = array(':toNumber', $toPhoneNumber, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        //$getIt = $GLOBALS['connector']->execute("SELECT outSMS.id,outSMS.SID,outSMS.msg,outSMS.fromNumber,outSMS.toNumber,outSMS.sentAt,outSMS.isRead,outSMS.sentByUser,outSMS.isOutGoing,users.fullName FROM networkleads_db.twilio_outgoingLeadsSMS AS outSMS LEFT JOIN networkleads_db.users AS users ON users.id=outSMS.sentByUser WHERE ((outSMS.fromNumber=:fromNumber AND outSMS.toNumber=:toNumber) OR (outSMS.fromNumber=:toNumber AND outSMS.toNumber=:fromNumber)) ORDER BY outSMS.id ASC",$binds,$errorVar);
        $getIt = $GLOBALS['connector']->execute("SELECT outSMS.id,outSMS.SID,outSMS.msg,outSMS.fromNumber,outSMS.toNumber,CTZ(outSMS.sentAt,:OFFSET) AS sentAt,outSMS.isRead,outSMS.status,outSMS.sentByUser,outSMS.isOutGoing,users.fullName FROM networkleads_db.twilio_outgoingLeadsSMS AS outSMS INNER JOIN networkleads_db.twilio_phoneNumbers AS tpn ON tpn.number=outSMS.fromNumber INNER JOIN networkleads_db.twilio_subAccounts AS tsa ON tsa.id=tpn.subAccountId LEFT JOIN networkleads_db.users AS users ON users.id=outSMS.sentByUser WHERE tsa.orgId=:orgId AND ((outSMS.fromNumber=:fromNumber AND outSMS.toNumber=:toNumber) OR (outSMS.fromNumber=:toNumber AND outSMS.toNumber=:fromNumber)) UNION ALL SELECT outSMS.id,outSMS.SID,outSMS.msg,outSMS.fromNumber,outSMS.toNumber,CTZ(outSMS.sentAt,:OFFSET) AS sentAt,outSMS.isRead,outSMS.status,outSMS.sentByUser,outSMS.isOutGoing,users.fullName FROM networkleads_db.twilio_outgoingLeadsSMS AS outSMS INNER JOIN networkleads_db.twilio_phoneNumbers AS tpn ON tpn.number=outSMS.toNumber INNER JOIN networkleads_db.twilio_subAccounts AS tsa ON tsa.id=tpn.subAccountId LEFT JOIN networkleads_db.users AS users ON users.id=outSMS.sentByUser WHERE  tsa.orgId=:orgId AND ((outSMS.fromNumber=:fromNumber AND outSMS.toNumber=:toNumber) OR (outSMS.fromNumber=:toNumber AND outSMS.toNumber=:fromNumber)) ORDER BY sentAt",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $conversation = array();
            while($r = $GLOBALS['connector']->fetch($getIt)) {
                $conversation[] = $r;
            }
        }

        return $conversation;
    }
    // ==================================== START getConversation() ======================================
    
    // ==================================== START getLeadNumbers() ======================================
    public function getLeadNumbers($leadId) {

        $errorVar = array("organizationTwilio Class","getLeadNumbers()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':leadId', $leadId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT DISTINCT(outSMS.toNumber) FROM networkleads_db.twilio_outgoingLeadsSMS AS outSMS INNER JOIN networkleads_db.leads AS leads ON leads.id=outSMS.leadId WHERE leads.orgId=:orgId AND outSMS.leadId=:leadId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $phones = array();
            while($r = $GLOBALS['connector']->fetch($getIt)) {
                $phones[] = $r["toNumber"];
            }

            $lead = new lead($leadId,$this->organizationId);
            $leadData = $lead->getData();

            $phone1 =  $this->unPrettifyPhone($leadData["phone"]);
            $phone2 =  $this->unPrettifyPhone($leadData["phone2"]);
            if($phone1 != ""){$phones[] = $phone1;}
            if($phone2 != ""){$phones[] = $phone2;}
        }

        $phones = array_unique($phones);

        return $phones;
    }
    // ==================================== START getLeadNumbers() ======================================

    // ==================================== START unPrettifyPhone() ======================================
    public function unPrettifyPhone($phone_number) {

        $phone_number = preg_replace('/[^0-9+]/', '', $phone_number); //Strip all non number characters
        if (!(substr($phone_number, 0, strlen("+")) === "+") && strlen($phone_number) == 10) {
            $phone_number = "+1" . $phone_number;
        }
        if ((substr($phone_number, 0, strlen("1")) === "1") && strlen($phone_number) == 11) {
            $phone_number = "+" . $phone_number;
        }

        return $phone_number;
    }
    // ==================================== START unPrettifyPhone() ======================================

    // ==================================== START prettifyPhone() ======================================
    public function prettifyPhone($phone_number) {

        // phone should income in the following format: +11234567890

        if ((substr($phone_number, 0, strlen("+1")) === "+1") && strlen($phone_number) == 12) {

            $phone_number = str_replace("+1", "", $phone_number);
            if (strlen($phone_number) == 10) {
                $phone_number = "(" . $phone_number[0] . $phone_number[1] . $phone_number[2] . ") " . $phone_number[3] . $phone_number[4] . $phone_number[5] . " " . $phone_number[6] . $phone_number[7] . $phone_number[8] . $phone_number[9];
            }
            return $phone_number;
        }
        return $phone_number;
    }
    // ==================================== START prettifyPhone() ======================================

    // ==================================== START getTotalUnreadMSGfromNumber() ======================================
    public function getTotalUnreadMSGS($leadId) {

        $errorVar = array("organizationTwilio Class","getTotalUnreadMSGS()",5,"Notes",array());

        $leadNumbers = $this->getLeadNumbers($leadId);

        $totalUnread = 0;
        foreach ($leadNumbers as $leadNumber) {

            $binds = array();
            $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
            $binds[] = array(':fromNumber', $leadNumber, PDO::PARAM_STR);

            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.twilio_outgoingLeadsSMS AS outSMS INNER JOIN networkleads_db.twilio_phoneNumbers AS tpn ON tpn.number=outSMS.toNumber INNER JOIN networkleads_db.twilio_subAccounts AS tsa ON tsa.id=tpn.subAccountId WHERE tsa.orgId=:orgId AND outSMS.fromNumber=:fromNumber AND outSMS.isRead=0 AND outSMS.isOutGoing=0", $binds, $errorVar);
            if (!$getIt) {
                return false;
            } else {
                $totalUnread += (int)$GLOBALS['connector']->fetch_num_rows($getIt);
                if ($totalUnread > 999){
                    // if the amount is bigger then 999 it most probally a bot, anyway we need to make it max 999 so the td in the leads table wont be too big
                    $totalUnread = 999;
                }
            }
        }

        return $totalUnread;
    }
    // ==================================== START getTotalUnreadMSGfromNumber() ======================================

    // ==================================== START markMSGSasRead() ======================================
    public function markMSGSasRead($myNumber,$clientNumber) {

        $errorVar = array("organizationTwilio Class","markMSGSasRead()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':fromNumber', $clientNumber, PDO::PARAM_STR);
        $binds[] = array(':toNumber', $myNumber, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.twilio_outgoingLeadsSMS AS outSMS INNER JOIN networkleads_db.twilio_phoneNumbers AS tpn ON tpn.number=outSMS.toNumber INNER JOIN networkleads_db.twilio_subAccounts AS tsa ON tsa.id=tpn.subAccountId SET outSMS.isRead=1 WHERE tsa.orgId=:orgId AND outSMS.fromNumber=:fromNumber AND outSMS.toNumber=:toNumber",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START markMSGSasRead() ======================================

    // ==================================== START saveIncomingSMS() ======================================
    public function saveIncomingSMS($data) {

        $errorVar = array("organizationTwilio Class","saveIncomingSMS()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':SID', $data["MessageSid"], PDO::PARAM_STR);
        $binds[] = array(':fromNumber', $data["From"], PDO::PARAM_STR);
        $binds[] = array(':toNumber', $data["To"], PDO::PARAM_STR);
        $binds[] = array(':msg', $data["Body"], PDO::PARAM_STR);
        $binds[] = array(':allData', serialize($data), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.twilio_outgoingLeadsSMS(SID,fromNumber,toNumber,msg,allData,isOutGoing) VALUES(:SID,:fromNumber,:toNumber,:msg,:allData,0)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }


        return false;
    }
    // ==================================== START saveIncomingSMS() ======================================

}