<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organizationPack.php");

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author
 * @copyright
 * @license   ---
 */

class organization extends organizationPack{

    private $organizationId;
    private $organizationData = NULL;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($organizationId = "") {
        $this->organizationId = $organizationId;
        parent::__construct($organizationId);
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getData() ======================================
    public function getData() {
        $errorVar = array("organization","getData()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT org.*,l.organizationId,l.DOT,l.ICCMC,l.registration FROM handymatcher_db.organizations AS org LEFT JOIN handymatcher_moving_db.licenses AS l ON l.organizationId=org.id WHERE org.id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $organization = $GLOBALS['connector']->fetch($getIt);
            $organizationTypeId = $organization["organizationTypeId"];

            $organization["organizationType"] = "";

            $binds = array();
            $binds[] = array(':organizationTypeId', $organizationTypeId, PDO::PARAM_STR);

            $getIt2 = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM handymatcher_db.organizationTypes WHERE id=:organizationTypeId",$binds,$errorVar);
            if(!$getIt2){
                return false;
            }else{
                if($GLOBALS['connector']->fetch_num_rows($getIt2) > 0){
                    $getIt3 = $GLOBALS['connector']->execute("SELECT * FROM handymatcher_db.organizationTypes WHERE id=:organizationTypeId",$binds,$errorVar);
                    if(!$getIt3){
                        return false;
                    }else{
                        $r = $GLOBALS['connector']->fetch($getIt3);
                        $organization["organizationType"] = $r["title"];
                    }
                }else{
                    $organization["organizationType"] = $organization["organizationTypeText"];
                }
            }

            $this->organizationData = $organization;
            return $organization;
        }

        return false;
    }
    // ==================================== START getData() ======================================

    // ==================================== START getOrgDataByNameKey() ======================================
    public function getOrgDataByNameKey($orgNameKey) {
        // NOTE : this is not secured (anyone can pass any data to here) - so onluy retrieve the org name,logo,phone,website and address
        $errorVar = array("organization","getOrgDataByNameKey()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationNameKey', $orgNameKey, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT id,organizationName,logoPath,address,city,state,zip,website,phone FROM handymatcher_db.organizations WHERE organizationNameKey=:organizationNameKey",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $organizationData = $GLOBALS['connector']->fetch($getIt);
            return $organizationData;
        }

        return false;
    }
    // ==================================== START getOrgDataByNameKey() ======================================

    // ==================================== START registerOrganizationNameKey() ======================================
    public function registerOrganizationNameKey($organizationNameKey,$generateKeyFromName = false) {

        if($generateKeyFromName == true){
            // Generate key from company name

            // To lower case & trim
            $organizationNameKey = trim(strtolower($organizationNameKey));

            // Remove all non-alpha-numeric and replace spaces with the '-' signs
            $organizationNameKey = preg_replace("/[^A-Za-z0-9 ]/", '', $organizationNameKey);
            $organizationNameKey = str_replace(" ","-",$organizationNameKey);

            // Remove spacial chars from BEGINNING of string
            if(preg_match("/[^A-Za-z0-9 ]/",$organizationNameKey[0])){
                $organizationNameKey = ltrim($organizationNameKey, $organizationNameKey[0]);
            }

            // Remove spacial chars from END of string
            if(preg_match("/[^A-Za-z0-9 ]/",$organizationNameKey[strlen($organizationNameKey)-1])){
                $organizationNameKey = rtrim($organizationNameKey, $organizationNameKey[strlen($organizationNameKey)-1]);
            }
        }else{
            // Clean the key generated in the client side


            // To lower case & trim
            $organizationNameKey = trim(strtolower($organizationNameKey));

            // Remove all non-alpha-numeric (but keep the '-' signs)
            $organizationNameKey = str_replace("-"," ", $organizationNameKey);
            $organizationNameKey = preg_replace("/[^A-Za-z0-9 ]/", '', $organizationNameKey);
            $organizationNameKey = str_replace(" ","-", $organizationNameKey);

            // Remove spacial chars from BEGINNING of string
            if(preg_match("/[^A-Za-z0-9 ]/",$organizationNameKey[0])){
                $organizationNameKey = ltrim($organizationNameKey, $organizationNameKey[0]);
            }

            // Remove spacial chars from END of string
            if(preg_match("/[^A-Za-z0-9 ]/",$organizationNameKey[strlen($organizationNameKey)-1])){
                $organizationNameKey = rtrim($organizationNameKey, $organizationNameKey[strlen($organizationNameKey)-1]);
            }
        }

        $errorVar = array("userLogIn", "registerOrganizationNameKey()", 5, "Notes", array());

        $binds = array();
        $binds[] = array(':id', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':organizationNameKey', $organizationNameKey, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM handymatcher_db.organizations WHERE organizationNameKey=:organizationNameKey AND id!=:id", $binds, $errorVar);
        if (!$getIt) {
            return false;
        } else {
            if ($GLOBALS['connector']->fetch_num_rows($getIt) > 0) {
                $organizationNameKey .= $this->organizationId;
            }
        }

        $errorVar = array("userLogIn", "registerOrganizationNameKey()", 5, "Notes", array());

        $binds = array();
        $binds[] = array(':id', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':organizationNameKey', $organizationNameKey, PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET organizationNameKey=:organizationNameKey WHERE id=:id", $binds, $errorVar);
        if (!$setIt) {
            return false;
        } else {
            return true;
        }

        return false;
    }
    // ==================================== START registerOrganizationNameKey() ======================================

    // ==================================== START canAddUsers() ======================================
    public function canAddUsers($returnValues = false) {

        $totalUsersInOrganization = $this->getTotalUsersInOrganization();

        $errorVar = array("organization","getData()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT pl.usersLimit FROM handymatcher_db.plans AS pl INNER JOIN handymatcher_db.organizations AS org ON org.organizationPackage=pl.id WHERE org.id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            if ($returnValues === true){
                $data = [];
                $data['usersLimit'] = $r['usersLimit'];
                $data['totalUsersInOrganization'] = $totalUsersInOrganization;

                return $data;

            }else {
                if ($totalUsersInOrganization < $r["usersLimit"]) {
                    return true;
                }
            }

        }

        return false;
    }
    // ==================================== START canAddUsers() ======================================

    // ==================================== START isFromOrganizationType() ======================================
    public function isFromOrganizationType($organizationTypes = array()) {
        if($organizationTypes === true){return true;}
        $errorVar = array("organization","isFromOrganizationType()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT organizationTypeId FROM handymatcher_db.organizations WHERE id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $organization = $GLOBALS['connector']->fetch($getIt);
            $organizationTypeId = $organization["organizationTypeId"];

            if (!in_array($organizationTypeId, $organizationTypes)){
                return false;
            }else{
                return true;
            }
        }

        return false;
    }
    // ==================================== START getOrganizationType() ======================================

    // ==================================== START isFeatureAvailableForFreeUser() ======================================
    public function isFeatureAvailableForFreeUser() {

        if($this->organizationData == NULL){
            $this->getData();
        }

        if($this->organizationData["organizationPackage"] > 1){
            return true;
        }


        return false;
    }
    // ==================================== START isFeatureAvailableForFreeUser() ======================================

    // ==================================== START getOrganizationType() ======================================
    public function getOrganizationTypeId() {
        $errorVar = array("organization","getOrganizationType()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT organizationTypeId FROM handymatcher_db.organizations WHERE id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $organization = $GLOBALS['connector']->fetch($getIt);
            $organizationTypeId = $organization["organizationTypeId"];

            return $organizationTypeId;
        }

        return false;
    }
    // ==================================== START getOrganizationType() ======================================

    // ==================================== START updateData() ======================================
    public function updateData($organizationName,$organizationNameKey,$address,$city,$state,$zip,$phone,$website,$facebook,$instagram,$twitter,$linkedin){

        $errorVar = array("organization Class","updateData()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':organizationName', $organizationName, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_STR);
        $binds[] = array(':phone', $phone, PDO::PARAM_STR);
        $binds[] = array(':website', $website, PDO::PARAM_STR);
        $binds[] = array(':facebook', $facebook, PDO::PARAM_STR);
        $binds[] = array(':instagram', $instagram, PDO::PARAM_STR);
        $binds[] = array(':twitter', $twitter, PDO::PARAM_STR);
        $binds[] = array(':linkedin', $linkedin, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET organizationName=:organizationName,address=:address,city=:city,state=:state,zip=:zip,phone=:phone,website=:website,facebookLink=:facebook,instagramLink=:instagram,twitterLink=:twitter,linkedInLink=:linkedin WHERE id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            // Update organizationNameKey
            return $this->registerOrganizationNameKey($organizationNameKey);
        }

        return false;
    }
    // ==================================== START updateData() ======================================

    // ==================================== START getTotalUsersInOrganization() ======================================
    public function getTotalUsersInOrganization() {

        $errorVar = array("organization Class","getTotalUsersInOrganization()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM handymatcher_db.users WHERE organizationId=:organizationId AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $total = $GLOBALS['connector']->fetch_num_rows($getIt);
            return $total;
        }

        return false;
    }
    // ==================================== START getTotalUsersInOrganization() ======================================

    // ==================================== START getUsersOfMyCompany() ======================================
    public function getUsersOfMyCompany($userId = "",$returnOnlyIds = false,$returnOnlyAdmins = false) {

        // $returnOnlyAdmins - will be ignored if $returnOnlyIds is false

        $errorVar = array("userManagement Class","getUsersOfMyCompany()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_INT);

        if($returnOnlyIds == true){
            if($returnOnlyAdmins == true){
                $getIt = $GLOBALS['connector']->execute("SELECT u.id,u.isDeleted,u.revokedDate AS typeTitle FROM handymatcher_db.users AS u LEFT JOIN handymatcher_db.userTypes AS ut ON u.userTypeId=ut.id WHERE u.organizationId=:organizationId AND u.id!=:userId AND u.isActive=1 AND u.isDeleted=0 AND u.isAdmin=1",$binds,$errorVar);
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT u.id,u.isDeleted,u.revokedDate AS typeTitle FROM handymatcher_db.users AS u LEFT JOIN handymatcher_db.userTypes AS ut ON u.userTypeId=ut.id WHERE u.organizationId=:organizationId AND u.id!=:userId AND u.isActive=1 AND u.isDeleted=0",$binds,$errorVar);
            }
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT u.id,u.fullName,u.jobTitle,u.email,u.phoneCountryCodeState,u.phoneNumber,u.signUpDate,u.isActive,u.isAdmin,u.isDeleted,u.revokedDate,u.comment,u.depId,u.profilePicture,u.userTypeId,u.salesBonusPerc,ut.title AS typeTitle FROM handymatcher_db.users AS u LEFT JOIN handymatcher_db.userTypes AS ut ON u.userTypeId=ut.id WHERE u.organizationId=:organizationId AND u.id!=:userId AND u.isDeleted=0",$binds,$errorVar);
        }
        if(!$getIt){
            return false;
        }else{

            $users = array();
            while($user = $GLOBALS['connector']->fetch($getIt)){

                if($returnOnlyIds == true){
                    $users[] = $user["id"];
                }else{
                    if ($user['userTypeId'] == ""){
                        $user['typeTitle'] = "Not Set";
                    }
                    $userClass = new user($user["id"]);
                    $countryCode = $userClass->convertCountryCode($user["phoneCountryCodeState"]);
                    $user["phoneCountryCode"] = $countryCode;
                    $users[] = $user;
                }

            }
            return $users;

        }

        return false;
    }
    // ==================================== END getUsersOfMyCompany() ======================================

    // ==================================== START revokeUser() ======================================
    public function revokeUser($userId,$type) {

        $errorVar = array("userManagement Class","revokeUser()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':dType', $type, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.users SET isDeleted=:dType, revokedDate=NOW() WHERE id=:userId",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END revokeUser() ======================================

    // ==================================== START getDepartments() ======================================
    public function getDepartments() {

        $errorVar = array("userManagement Class","getDepartments()",4,"Notes",array());

        $departments = [];

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT * FROM handymatcher_db.departments WHERE orgId=:orgId AND isDeleted=0",$binds,$errorVar);

        if(!$getIt){
            return false;
        }else{
            while($res = $GLOBALS['connector']->fetch($getIt)){
                $departments[] = $res;
            }
        }

        return $departments;

    }
    // ==================================== END getDepartments() ======================================

    // ==================================== START addDepartment() ======================================
    public function addDepartment($title,$description,$address,$city,$state,$zip,$phone,$owner) {

        $errorVar = array("userManagement Class","addDepartment()",4,"Notes",array());


        $binds = array();
        $binds[] = array(':title', $title, PDO::PARAM_STR);
        $binds[] = array(':des', $description, PDO::PARAM_STR);
        $binds[] = array(':address', $address, PDO::PARAM_STR);
        $binds[] = array(':city', $city, PDO::PARAM_STR);
        $binds[] = array(':state', $state, PDO::PARAM_STR);
        $binds[] = array(':zip', $zip, PDO::PARAM_INT);
        $binds[] = array(':phone', $phone, PDO::PARAM_INT);
        $binds[] = array(':owner', $owner, PDO::PARAM_STR);
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO handymatcher_db.departments(title,description,address,city,state,zip,phone,owner,orgId) VALUES(:title,:des,:address,:city,:state,:zip,:phone,:owner,:orgId)",$binds,$errorVar);
        if(!$getIt){
            echo $GLOBALS['connector']->getLastError();
            return false;
        }else{
            return true;
        }

        return false;

    }
    // ==================================== END addDepartment() ======================================

    // ==================================== START saveNewLogo() ======================================
    public function saveNewLogo($logoPath = "")
    {
        $errorVar = array("userManagement Class","saveNewLogo()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':logoPath', $logoPath, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET logoPath=:logoPath WHERE id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }
        return false;
    }
    // ==================================== END saveNewLogo() ======================================

    // ==================================== START userLogins() ======================================
    public function userLogins($userId)
    {
        $errorVar = array("userManagement Class","saveNewLogo()",4,"Notes",array());

        $logins = [];
        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':userId', $userId, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT CTZ(l.loginTime,:OFFSET) AS loginTime FROM handymatcher_db.logins AS l INNER JOIN users AS u ON l.userId=u.id WHERE u.organizationId=:organizationId AND l.hideLogin=0 AND l.userId=:userId ORDER BY l.loginTime DESC LIMIT 10",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            while($login = $GLOBALS['connector']->fetch($getIt)){
                $login['loginTime'] = date("jS F Y H:i",strtotime($login['loginTime']));
                $logins[] = $login;
            }
            return $logins;
        }
        return false;
    }
    // ==================================== END saveNewLogo() ======================================    ֿ

    // ==================================== START getDefaultMailAccount() ======================================
    public function getDefaultMailAccount()
    {
        $errorVar = array("userManagement Class","getDefaultMailAccount()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT mailAccount FROM handymatcher_db.organizations WHERE id=:organizationId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->fetch($getIt);
        }
        return false;
    }
    // ==================================== END getDefaultMailAccount() ======================================

    // ==================================== START setDefaultMailAccount() ======================================
    public function setDefaultMailAccount($id)
    {
        $errorVar = array("userManagement Class","setDefaultMailAccount()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':mailAccount', $id, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET mailAccount=:mailAccount WHERE id=:organizationId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
        return false;
    }
    // ==================================== END setDefaultMailAccount() ======================================

    // ==================================== START changeOrganizationPlan() ======================================
    public function changeOrganizationPlan($newPlanId = 0) {

        $errorVar = array("billing Class","changeOrganizationPlan()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':organizationPackage', $newPlanId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET organizationPackage=:organizationPackage WHERE id=:id",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START changeOrganizationPlan() ======================================

    // ==================================== START changeOrganizationPlanStatus() ======================================
    public function changeOrganizationPlanStatus($status = 0,$subscriptionEndDate = false) {
        $errorVar = array("billing Class","changeOrganizationPlanStatus()",4,"Notes",array());

        if($status != 1 && $status != 0){return false;}

        $binds = array();
        $binds[] = array(':id', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':organizationPackageStatus', $status, PDO::PARAM_INT);

        if($subscriptionEndDate != false){
            $subscriptionEndDate = date("Y-m-d H:i:s",strtotime($subscriptionEndDate." +7 days"));
            $binds[] = array(':subscriptionEndDate', $subscriptionEndDate, PDO::PARAM_STR);
            $setIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET organizationPackageStatus=:organizationPackageStatus,subscriptionEndDate=:subscriptionEndDate WHERE id=:id", $binds, $errorVar);

        }else {
            $setIt = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET organizationPackageStatus=:organizationPackageStatus WHERE id=:id", $binds, $errorVar);
        }

        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== START changeOrganizationPlanStatus() ======================================

    // ==================================== START getUsersInOrganizationByUserType() ======================================
    public function getUsersInOrganizationByUserType($userTypeId = false) {
        // This function returns an array of all active users in organization by user type id
        if($userTypeId === false){return false;}

        $errorVar = array("organization Class","getUsersInOrganizationByUserType()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':organizationId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':userTypeId', $userTypeId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT id FROM handymatcher_db.users WHERE organizationId=:organizationId AND isDeleted=0 AND isActive=1 AND userTypeId=:userTypeId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $users = array();
            while($user = $GLOBALS['connector']->fetch($getIt)){
                $users[] = $user["id"];
            }
            return $users;
        }

        return false;
    }
    // ==================================== START getUsersInOrganizationByUserType() ======================================

    // ==================================== START updateStripeClientId() ======================================
    public function updateStripeClientId($stripeAccountId = ""){

        $errorVar = array("organization Class","updateStripeClientId()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':organizationId',$this->organizationId,PDO::PARAM_INT);
        $binds[] = array(':stripeAccountId',$stripeAccountId,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("UPDATE handymatcher_db.organizations SET stripeAccountId=:stripeAccountId WHERE id=:organizationId",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

    }
    // ==================================== END turnOffFirstTutorial() ======================================

    // ==================================== START getActiveOrganizations() ======================================
    public function getActiveOrganizations($getOnlyIds = false){

        $errorVar = array("organization Class","getActiveOrganizations()",3,"Notes",array(),false);
        if ($getOnlyIds){
            $query = $GLOBALS['connector']->execute("SELECT id FROM handymatcher_db.organizations WHERE isDeleted=0", NULL, $errorVar);
        }else {
            $query = $GLOBALS['connector']->execute("SELECT * FROM handymatcher_db.organizations WHERE isDeleted=0", NULL, $errorVar);
        }

        if(!$query){
            return $GLOBALS['connector']->getLastError();
        }else{
            $collectedData = [];
            while($data = $GLOBALS['connector']->fetch($query)){
                if ($getOnlyIds){
                    $collectedData[] = $data['id'];
                }else{
                    $collectedData[] = $data;
                }
            }
            return $collectedData;
        }

    }
    // ==================================== END turnOffFirstTutorial() ======================================

    // ==================================== START checkIfCompanyKeyExist() ======================================
    public function checkIfCompanyKeyExist($companyKey = "") {
        // Return true if key dont exists

        $errorVar = array("userLogIn","checkIfCompanyKeyExist()",5,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':companyKey', $companyKey, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM handymatcher_db.organizations WHERE organizationNameKey=:companyKey AND id!=:id",$binds,$errorVar);
        if(!$getIt){

        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) == 0){
                return true;
            }
        }

        return false;
    }
    // ==================================== END checkIfCompanyKeyExist() ======================================
}
