<?php

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (8.11.18) 2018 Niv Apo
 * @license   ---
 */


class stripeBilling{

    private $userId;
    private $organizationId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = "",$organizationId = "") {
        $this->userId = $userId;
        $this->organizationId = $organizationId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START createNewBilling() ======================================
    public function createNewCustomer($description,$stripeToken = NULL,$email = "") {

        $resp = array();
        $resp["status"] = false;
        $resp["customer"] = false;

        $stripeError = false;

        // in welcome mode - create a new customer
        try {
            $customer = \Stripe\Customer::create([
                'description' => $description,
                'source' => $stripeToken,
                'email' => $email
            ]);
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            $resp["status"] = true;
            $resp["customer"] = $customer;
            return $resp;
        }

        return $resp;

    }
    // ==================================== START createNewBilling() ======================================

    // ==================================== START subscribeToPlan() ======================================
    public function subscribeToPlan($customerId,$planId,$coupon = "") {

        $resp = array();
        $resp["status"] = false;

        $stripeError = false;

        try {
            $sub = \Stripe\Subscription::create([
                "customer" => $customerId,
                "trial_from_plan"=>true,
                "items" => [
                    [
                        "plan" => $planId,
                        "quantity" => 1,
                    ],
                ],
                'coupon' => $coupon,
            ]);
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            $resp["status"] = true;
            return $resp;
        }

        return $resp;

    }
    // ==================================== START subscribeToPlan() ======================================

    // ==================================== START updateDefaultPayment() ======================================
    public function updateDefaultPayment($customerId,$source) {

        $resp = array();
        $resp["status"] = false;

        $stripeError = false;

        try {

            \Stripe\Customer::update($customerId, [
                'source' => $source,
            ]);

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            $resp["status"] = true;
            return $resp;
        }

        return $resp;

    }
    // ==================================== START updateDefaultPayment() ======================================

    // ==================================== START getCustomerSubscriptions() ======================================
    public function getCustomerSubscriptions($customerId,$limit = 10) {

        $resp = array();
        $resp["status"] = false;

        $stripeError = false;

        try {
            $customerSubs = \Stripe\Subscription::all(array('customer'=>$customerId,'limit'=>10));
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            return $customerSubs;
        }

        return false;

    }
    // ==================================== START getCustomerSubscriptions() ======================================

    // ==================================== START changePlan() ======================================
    public function changePlan($customerSubscriptionId,$newStripePlanId) {

        $resp = array();
        $resp["status"] = false;

        $stripeError = false;

        try {

            $subscription = \Stripe\Subscription::retrieve($customerSubscriptionId);
            $sub = \Stripe\Subscription::update($subscription->id, [
                'cancel_at_period_end' => false,
                'items' => [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'plan' => $newStripePlanId,
                    ],
                ],
            ]);

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            return true;
        }

        return false;

    }
    // ==================================== START changePlan() ======================================

    // ==================================== START cancelPlan() ======================================
    public function cancelPlan($customerSubscriptionId) {

        $resp = array();
        $resp["status"] = false;

        $stripeError = false;

        try {

            $subscription = \Stripe\Subscription::retrieve($customerSubscriptionId);
            $subscription->cancel_at_period_end = true;
            $subscription->save();

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            return true;
        }

        return false;

    }
    // ==================================== START cancelPlan() ======================================

    // ==================================== START reactivatePlan() ======================================
    public function reactivatePlan($customerSubscriptionId) {

        $resp = array();
        $resp["status"] = false;

        $stripeError = false;

        try {
            $subscription = \Stripe\Subscription::retrieve($customerSubscriptionId);
            $subscription->cancel_at_period_end = false;
            $subscription->save();

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
        }

        if($stripeError == false){
            // No errors
            return true;
        }

        return false;

    }
    // ==================================== START reactivatePlan() ======================================

    // ==================================== START addCreditCard() ======================================
    public function addCreditCard($customerId = NULL,$source = NULL) {

        if($customerId === NULL || $source === NULL){return false;}

        $resp = array();
        $resp["status"] = false;
        $resp["response"] = NULL;

        $stripeError = false;

        try {

            $createSource = \Stripe\Customer::createSource($customerId, [
                'source' => $source,
            ]);

           $this->makeCreditCardDefault($customerId,$createSource->id);

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
            $resp["response"] = $e;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
            $resp["response"] = $e;
        }

        if($stripeError == false){
            // No errors
            $resp["status"] = true;
        }

        // use this for debugging
        // $e->getJsonBody();

        return $resp;

    }
    // ==================================== START addCreditCard() ======================================

    // ==================================== START deleteCreditCard() ======================================
    public function deleteCreditCard($customerId = NULL,$source = NULL) {

        if($customerId === NULL || $source === NULL){return false;}

        $resp = array();
        $resp["status"] = false;
        $resp["response"] = NULL;

        $getDefaultCreditCard = $this->getDefaultCreditCard($customerId);

        if($getDefaultCreditCard["status"] == true) {

            $stripeError = false;
            try {

                $deleteSource = \Stripe\Customer::deleteSource($customerId, $source);


            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $stripeError = true;
                $resp["response"] = $e;
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $stripeError = true;
                $resp["response"] = $e;
            }

            if ($stripeError == false) {
                // No errors
                $resp["status"] = true;
            }

            // use this for debugging
            // $e->getJsonBody();
        }

        return $resp;

    }
    // ==================================== START deleteCreditCard() ======================================

    // ==================================== START deleteCreditCard() ======================================
    public function makeCreditCardDefault($customerId = NULL,$cardId = NULL) {

        if($customerId === NULL || $cardId === NULL){return false;}

        $resp = array();
        $resp["status"] = false;
        $resp["response"] = NULL;

            $stripeError = false;
            try {

                $update = \Stripe\Customer::update($customerId, [
                    'default_source' => $cardId,
                ]);

            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                $stripeError = true;
                $resp["response"] = $e;
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $stripeError = true;
                $resp["response"] = $e;
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $stripeError = true;
                $resp["response"] = $e;
            }

            if ($stripeError == false) {
                // No errors
                $resp["status"] = true;
            }

            // use this for debugging
            // $e->getJsonBody();

        return $resp;

    }
    // ==================================== START deleteCreditCard() ======================================

    // ==================================== START getDefaultCreditCard() ======================================
    public function getDefaultCreditCard($customerId = NULL) {

        if($customerId === NULL){return false;}

        $resp = array();
        $resp["status"] = false;
        $resp["response"] = NULL;

        $stripeError = false;

        try {

            // Note : the default credit card always will be the first object in the array of $cards
            $cards = \Stripe\Customer::allSources($customerId,[
                'object' => 'card',
            ]);

            if(count($cards->data) >= 1){
                $resp["response"] = $cards->data[0];
            }else{
                $resp["response"] = [];
            }

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
            $resp["response"] = $e;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
            $resp["response"] = $e;
        }

        if($stripeError == false){
            // No errors
            $resp["status"] = true;
        }

        return $resp;

    }
    // ==================================== START getDefaultCreditCard() ======================================

    // ==================================== START charge() ======================================
    public function charge($customerId = NULL,$amount = NULL,$description = NULL) {

        if($customerId === NULL || $amount === NULL || $amount <= 0){return false;}

        if($description === NULL){$description = "Network Leads";}

        $resp = array();
        $resp["status"] = false;
        $resp["response"] = NULL;

        $stripeError = false;
        try {

            $cents = intval($amount * 100);

            $charge = \Stripe\Charge::create([
                'amount' => $cents,
                'currency' => 'usd',
                'description' => $description,
                'statement_descriptor' => 'NETWORKLEADS',
                'customer'=>$customerId
            ]);


        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
            $resp["response"] = $e;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
            $resp["response"] = $e;
        }

        if ($stripeError == false) {
            // No errors
            $resp["status"] = true;
            $resp["response"] = $charge;
        }else{

            /*
          $body = $e->getJsonBody();
          $err  = $body['error'];

          print('Status is:' . $e->getHttpStatus() . "\n");
          print('Type is:' . $err['type'] . "\n");
          print('Code is:' . $err['code'] . "\n");
          // param is '' in this case
          print('Param is:' . $err['param'] . "\n");
          print('Message is:' . $err['message'] . "\n");

          */
        }

        // use this for debugging
        // $e->getJsonBody();


        return $resp;

    }
    // ==================================== START charge() ======================================

    // ==================================== START retrieveCharge() ======================================
    public function retrieveCharge($customerId = NULL,$chargeId = NULL) {

        if($customerId === NULL || $chargeId === NULL){return false;}

        $resp = array();
        $resp["status"] = false;
        $resp["response"] = NULL;

        $stripeError = false;
        try {

            $charge = \Stripe\Charge::retrieve($chargeId);


        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $stripeError = true;
            $resp["response"] = $e;
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $stripeError = true;
            $resp["response"] = $e;
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $stripeError = true;
            $resp["response"] = $e;
        }

        if ($stripeError == false) {
            // No errors
            $resp["status"] = true;
            $resp["response"] = $charge;
        }else{

            /*
          $body = $e->getJsonBody();
          $err  = $body['error'];

          print('Status is:' . $e->getHttpStatus() . "\n");
          print('Type is:' . $err['type'] . "\n");
          print('Code is:' . $err['code'] . "\n");
          // param is '' in this case
          print('Param is:' . $err['param'] . "\n");
          print('Message is:' . $err['message'] . "\n");

          */
        }

        // use this for debugging
        // $e->getJsonBody();


        return $resp;

    }
    // ==================================== START retrieveCharge() ======================================

}