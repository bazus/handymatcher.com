<?php

/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (9.10.18) 2018 Niv Apo
 * @license   ---
 */


class billing{

    private $organizationId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($organizationId = "") {
        $this->organizationId = $organizationId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START create() ======================================
    public function create($credit = 0,$debit = 0,$type = 0,$refId = NULL,$notes = "",$other = "",$doNotRecharge = false) {

        // note : $change should be "-" for negative change or "+" for add
        // if you want to charge $10 then $change should be -10

        $response = [];
        $response["status"] = false;
        $response["msg"] = "";


        if(!in_array($type,array(1,2,3,4,5))){
            $response["msg"] = "not a valid type";
            return $response;
        }

        $currentBalance = $this->getCurrentBalance();
        if($currentBalance === false){
            $response["msg"] = "problem with current balance";
            return $response;
        }

        // set new balance
        $newBalance = $currentBalance+$credit;
        $newBalance = $newBalance-$debit;

        if($newBalance < 0){
            $response["msg"] = "no balance available";
            return $response;
        }

        $errorVar = array("billing Class","create()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':credit', $credit, PDO::PARAM_STR);
        $binds[] = array(':debit', $debit, PDO::PARAM_STR);
        $binds[] = array(':currentBalance', $newBalance, PDO::PARAM_STR);
        $binds[] = array(':type', $type, PDO::PARAM_INT);
        if($refId === NULL){
            $binds[] = array(':refId', NULL, PDO::PARAM_NULL);
        }else{
            $binds[] = array(':refId', $refId, PDO::PARAM_INT);
        }
        $binds[] = array(':notes', $notes, PDO::PARAM_STR);
        $binds[] = array(':other', $other, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.balance(orgId,credit,debit,currentBalance,type,refId,notes,other,changeDate) VALUES(:orgId,:credit,:debit,:currentBalance,:type,:refId,:notes,:other,NOW())",$binds,$errorVar);
        if(!$getIt){
            $response["msg"] = "problem with query - ".$GLOBALS['connector']->getLastError();
            return $response;
        }else{

            // if has auto recharge and balance is low - recharge
            $autoRecharge = $this->getAutoRecharge();
            if($autoRecharge != false && $autoRecharge["isOn"] == "1" && $doNotRecharge == false){
                // has auto recharge

                if($newBalance < $autoRecharge["balanceBelow"]){
                    $chargeBalance = $autoRecharge["balanceTo"]-$newBalance;

                    $data = [];
                    $data["organizationId"] = $this->organizationId;
                    $data["amount"] = $chargeBalance;
                    $data["chargeDescription"] = "Auto Recharge Balance";
                    $data["type"] = 2;
                    $data["refId"] = NULL;
                    $data["notes"] = "Auto Recharge";

                    shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/console/actions/system/organization/billing/addFundsBySystem.php " . escapeshellarg(serialize($data)) . " > /dev/null 2>/dev/null &");
                }
            }

            $response["status"] = true;
            return $response;
        }

        return $response;

    }
    // ==================================== START create() ======================================

    // ==================================== START getCurrentBalance() ======================================
    public function getCurrentBalance() {
        $errorVar = array("billing Class","getCurrentBalance()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_db.balance WHERE orgId=:orgId ORDER BY id DESC,changeDate DESC LIMIT 1 FOR UPDATE",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) == 0) {
                return 0;
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT id,currentBalance FROM networkleads_db.balance WHERE orgId=:orgId ORDER BY id DESC,changeDate DESC LIMIT 1 FOR UPDATE", $binds, $errorVar);
                if(!$getIt){
                    return false;
                }else{
                    $r = $GLOBALS['connector']->fetch($getIt);
                    return $r["currentBalance"];
                }
            }
        }

        return false;

    }
    // ==================================== START getCurrentBalance() ======================================

    // ==================================== START getAutoRecharge() ======================================
    public function getAutoRecharge() {
        $errorVar = array("billing Class","getAutoRecharge()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.balanceAutoRecharge WHERE organizationId=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if ($GLOBALS['connector']->fetch_num_rows($getIt) == 0) {
                return false;
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT id,isOn,balanceBelow,balanceTo FROM networkleads_db.balanceAutoRecharge WHERE organizationId=:orgId", $binds, $errorVar);
                if(!$getIt){
                    return false;
                }else{
                    $r = $GLOBALS['connector']->fetch($getIt);
                    return $r;
                }
            }
        }

        return false;

    }
    // ==================================== START getAutoRecharge() ======================================

    // ==================================== START setAutoRecharge() ======================================
    public function setAutoRecharge($isOn = false,$balanceBelow = 10,$balanceTo = 20) {
        $errorVar = array("billing Class","setAutoRecharge()",4,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        if ($isOn == "true") {
            $binds[] = array(':isOn', 1, PDO::PARAM_BOOL);
        } else {
            $binds[] = array(':isOn', 0, PDO::PARAM_BOOL);
        }
        $binds[] = array(':balanceBelow', $balanceBelow, PDO::PARAM_INT);
        $binds[] = array(':balanceTo', $balanceTo, PDO::PARAM_INT);


        $autoRecharge = $this->getAutoRecharge();
        if($autoRecharge == false){
            // No auto recharge available - create a new one
            $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.balanceAutoRecharge(organizationId,isOn,balanceBelow,balanceTo) VALUES(:orgId,:isOn,:balanceBelow,:balanceTo)", $binds, $errorVar);
            if (!$setIt) {
                return false;
            } else {
                return true;
            }
        }else {
            // Update auto recharge
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.balanceAutoRecharge SET isOn=:isOn,balanceBelow=:balanceBelow,balanceTo=:balanceTo WHERE organizationId=:orgId", $binds, $errorVar);
            if (!$setIt) {
                return false;
            } else {
                return true;
            }
        }

        return false;

    }
    // ==================================== START setAutoRecharge() ======================================

    // ==================================== START doesHaveBalance() ======================================
    public function doesHaveBalance($cost = 0) {

        // check if new balance will be above 0
        $currentBalance = $this->getCurrentBalance();
        if(($currentBalance-$cost)>=0){
            return true;
        }

        return false;
    }
    // ==================================== START doesHaveBalance() ======================================

    // ==================================== START getOrganizationBilling() ======================================
    public function getOrganizationBilling($dateS = NULL, $dateE = NULL) {
        $errorVar = array("billing Class","getOrganizationBilling()",4,"Notes",array());

        if ($dateS && $dateE){
            $dateS = date("Y-m-d 00:00:00",strtotime($dateS));
            $dateE = date("Y-m-d 23:59:59",strtotime($dateE));
        }else{
            $dateS = date("Y-m-d 00:00:00",strtotime("today"));
            $dateE = date("Y-m-d 23:59:59",strtotime("today"));
        }

        $binds = array();
        $binds[] = array(':orgId', $this->organizationId, PDO::PARAM_INT);
        $binds[] = array(':dateS', $dateS, PDO::PARAM_STR);
        $binds[] = array(':dateE', $dateE, PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT credit,debit,changeDate,refId,type,other FROM networkleads_db.balance WHERE orgId=:orgId AND (type=1 OR type=2) AND changeDate>=:dateS AND changeDate<=:dateE ORDER BY changeDate DESC",$binds,$errorVar);

        if (!$getIt){
            return false;
        }else{
            $returnData = [];
            while($charge = $GLOBALS['connector']->fetch($getIt)){
                $charge['dateTimestamp'] = strtotime($charge['changeDate']);
                $returnData[] = $charge;
            }
            return $returnData;
        }

        return false;
    }
    // ==================================== START getOrganizationBilling() ======================================


}