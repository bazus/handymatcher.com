<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require_once ($_SERVER['LOCAL_NL_PATH']."/console/services/PHPMailer/src/Exception.php");
require_once ($_SERVER['LOCAL_NL_PATH']."/console/services/PHPMailer/src/PHPMailer.php");
require_once ($_SERVER['LOCAL_NL_PATH']."/console/services/PHPMailer/src/SMTP.php");

require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/mail/mailaccounts.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/googleAPI.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/API/microsoftAPI.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/user/user.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/leads/lead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/moving/movingLead.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/organization/organization.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/passwords.php");

class email
{

    private $userId = NULL;
    private $orgId = NULL;
    private $isValid = false;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId = "",$orgId = NULL) {
        $this->userId = $userId;
        $this->orgId = $orgId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START sendMailFromUs() ======================================
    public function sendMailFromUs($SesClient,$fullName,$from,$to,$subject,$plaincontent,$content,$mailType = false,$excludeDidOpenImage = false,$weeklyReport = false)
    {

        if(!$mailType || $mailType == ""){
            return false;
        }

        // the content without the added image
        $originalContent = $content;

        $token = md5("email".date("Ymdhis",strtotime("now")));
        if(!$excludeDidOpenImage) {
            $content .= '<img src="https://www.network-leads.com/api/didOpen.php?token=' . $token . '" />';
        }

        $resp = array();
        $resp["status"] = false;
        $resp["errors"] = array();
        $resp["msgId"] = false;

        $password = false;
        switch($from){
            case "no-reply@network-leads.com":
                $from = '"Network Leads" <'.$from.'>';
                $password = "password";
                break;
            case "it@networkmoving.com":
                $password = "nivpassword!";
                break;
            case "support@network-leads.com":
                $from = '"Network Leads" <'.$from.'>';
                $password = "password";
                break;
        }

        if(!$password){return false;}

        // This address must be verified with Amazon SES.
        $sender_email = $from;

        $recipient_emails = $to; // $to is array

        // Specify a configuration set. If you do not want to use a configuration
        // set, comment the following variable, and the
        // 'ConfigurationSetName' => $configuration_set argument below.
        //$configuration_set = 'ConfigSet';

        $plaintext_body = $plaincontent ;
        $html_body =  $content;
        $char_set = 'UTF-8';

        try {
            $result = $SesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => $recipient_emails,
                ],
                'ReplyToAddresses' => [$sender_email],
                'Source' => $sender_email,
                'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $html_body,
                        ],
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                ]//,
                // If you aren't using a configuration set, comment or delete the
                // following line
                //'ConfigurationSetName' => $configuration_set,
            ]);

            $messageId = $result['MessageId'];
            $resp["status"] = true;
            $resp["msgId"] = $messageId;
            $this->saveInDB($this->userId,$from,$to[0],$subject,$originalContent,$resp["status"],$token,NULL,true,$weeklyReport);
        } catch (AwsException $e) {
            // output error message if fails
            $resp["status"] = false;
            $resp["errors"][] = $e->getMessage();
            $resp["errors"][] = $e->getAwsErrorMessage();
            $this->saveInDB($this->userId,$from,$to[0],$subject,$originalContent,$resp["status"],$token,NULL,true,$weeklyReport,$resp["errors"]);
        }


        return $resp;
    }
    // ==================================== END sendMailFromUs() ======================================

    // ==================================== START sendMailFromMailAccount() ======================================
    public function sendMailFromMailAccount($userId,$mailAccountId,$to,$subject,$content,$orgId = false,$leadId = NULL,$fromSystem = false,$returnDebug = false)
    {
        $response = [];
        $response['status'] = false;
        $response['response'] = "";
        $response['errors'] = array();
        $response['responseCode'] = NULL; // 0 = success, 1 = email returned with error, 2 = unsubscribed email, 3 = limit reached
        $response['didLimitReached'] = false;

        $user = new user($userId,true);
        $orgId = $user->getOrgId();

        if (!$fromSystem) {
            $isLimitValid = $this->isLimitValid($orgId);

            if ($isLimitValid == false) {
                $response['status'] = false;
                $response['responseCode'] = 3;
                $response['errors'][] = "Email Limit Reached";
                $response['didLimitReached'] = true;
                return $response;
            }
        }

        if($leadId != NULL){
            $subject = $this->convertStringPlaceHolders($subject,$this->orgId,$leadId);
            $content = $this->convertStringPlaceHolders($content,$this->orgId,$leadId);
        }

        $passwords = new passwords();

        $mailaccounts = new mailaccounts($userId,$orgId);
        $accountData = $mailaccounts->getAccountById($mailAccountId);

        $fullName = $accountData["fullName"];
        $accountEmail = $accountData["email"];
        $password = $passwords->unHashCC($accountData["password"]);
        $mailServer = $accountData["outgoingmailserver"];
        $port = $accountData["port"];
        $mailType = $accountData["accountType"];
        $otherData = $accountData["otherData"];
        $requiredSSL = $accountData["requiredSSL"];

        switch($mailType){
            case "1": // google
                {
                    $mailServer = "smtp.gmail.com";
                    $port = "587";
                }
                break;
            case "2": // yahoo
                {
                    $mailServer = "smtp.mail.yahoo.com";
                    $port = "587";
                }
                break;
            case "3": // office
                {
                    $mailServer = "smtp.office365.com";
                    $port = "587";
                }
                break;
        }

        // send the email
        $sendMail = $this->sendMail($fullName, $accountEmail, $password, $to, $subject, $content,$mailType,$port,$mailServer,$requiredSSL,$otherData);
        if(!$sendMail){return false;} // Sending failed

        // save the email in DB
        $responseId = $this->saveInDB($this->userId,$accountEmail,$to,$subject,$sendMail["originalContent"],$sendMail["status"],$sendMail["token"],$leadId,$fromSystem,false,$sendMail["debugOutput"],$sendMail["debugCode"],$sendMail["debugMessage"]);

        $response['status'] = $sendMail["status"];
        $response['responseCode'] = $sendMail["responseCode"];

        if($returnDebug == true){
            $response['debugOutput'] = $sendMail["debugOutput"];
        }

        return $response;
    }
    // ==================================== END sendMailFromMailAccount() ======================================

    // ==================================== START sendMail() ======================================
    public function sendMail($fullName = "",$from = "",$password = "",$to = "",$subject = "",$content = "",$mailType = false,$port = false,$mailServer = false,$requiredSSL = false,$otherData = ""){

        if($mailType === false || $mailType === ""){
            return false;
        }

        $response = array();
        $response['token'] = "";
        $response['originalContent'] = "";
        $response['debugOutput'] = "";
        $response['debugCode'] = "";
        $response['debugMessage'] = "";
        $response['status'] = false;
        $response['responseCode'] = NULL;

        // Check that this email is not unsubscribed
        $isEmailUnsibscribed = $this->isEmailUnsibscribed($to);
        if($isEmailUnsibscribed == true){
            // Email is unsubscribed
            shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => "Tried to send email that was unsubscribed [".$from." to ".$to."]","mode"=>"debug"))) . ""); // > /dev/null 2>/dev/null &
            $response['status'] = false;
            $response['responseCode'] = 2;
            return $response;
        }

        // the content without the added image
        $originalContent = $content;

        if($this->orgId != NULL) {
            // Add unsubscribe link to email

            if(isset($to) && $to != ""){
                $unsubscribeURL = 'https://www.network-leads.com/api/unsubscribe.php?orgId='.$this->orgId.'&email='.$to;
            }else{
                $unsubscribeURL = 'https://www.network-leads.com/api/unsubscribe.php?orgId='.$this->orgId;
            }

            $content .= '<div style="margin-top: 13px;" align="center"><a href="'.$unsubscribeURL.'" style="font-size: 12px;line-height: 1.2;word-break: break-word;font-family: Arial,Helvetica Neue,Helvetica,sans-serif;margin: 0;color: #7d7d7d;text-decoration: underline;" target="_blank" title="unsubscribe">Click here to unsubscribe</a></div>';
        }

        mb_convert_encoding($originalContent, "UTF-8"); //AUTO DETECT AND CONVERT
        mb_convert_encoding($content, "UTF-8"); //AUTO DETECT AND CONVERT

        // **************** SEND EMAIL ****************

        $token = md5("email".date("Ymdhis",strtotime("now")));
        $content .= '<img src="https://www.network-leads.com/api/didOpen.php?token='.$token.'" />';

        $response['token'] = $token;
        $response['originalContent'] = $originalContent;

        if($mailType == "5"){
            $otherData = unserialize($otherData);
            $microsoftAPI = new microsoftAPI($otherData);
            $sendEmail = $microsoftAPI->sendEmail($from,$to,"",$subject,"",$content);

            $response['status'] = $sendEmail["status"];
            $response['debugOutput'] = $sendEmail["debugOutput"];
            $response['debugCode'] = $sendEmail["debugCode"];
            $response['debugMessage'] = $sendEmail["debugMessage"];

            if($sendEmail["status"]){$response['responseCode'] = 0;}else{$response['responseCode'] = 1;}
        }

        if($mailType == "4"){

            $otherData = unserialize($otherData);
            $googleAPI = new googleAPI($otherData["refresh_token"]);
            $sendEmail = $googleAPI->sendEmailFromGmail($from,$to,"",$subject,"",$content);

            $response['status'] = $sendEmail["status"];
            $response['debugOutput'] = $sendEmail["debugOutput"];
            $response['debugCode'] = $sendEmail["debugCode"];
            $response['debugMessage'] = $sendEmail["debugMessage"];

            if($sendEmail["status"]){$response['responseCode'] = 0;}else{$response['responseCode'] = 1;}
        }

        if($mailType == "0" || $mailType == "1" || $mailType == "2" || $mailType == "3") {
            $mail = new PHPMailer(true);

            /*
             * For some servers use this specific configuration
            if ($mailServer == "smtp.wemovejax.com") {
                $smtpinfo["socket_options"] = array('ssl' => array('verify_peer_name' => false));
            }
            */

            try {
                $GLOBALS['debug'] = '';

                $mail->Debugoutput = function($str, $level) {
                    $GLOBALS['debug'] .= "$level: $str";
                };

                //Server settings
                $mail->SMTPDebug = SMTP::DEBUG_CONNECTION;          // Enable verbose debug output
                $mail->isSMTP();                                    // Send using SMTP
                $mail->Host       = $mailServer;                    // Set the SMTP server to send through
                $mail->SMTPAuth   = true;                           // Enable SMTP authentication
                $mail->Username   = $from;                          // SMTP username
                $mail->Password   = $password;                      // SMTP password
                if($requiredSSL){
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                }else{
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                }
                $mail->Port       = $port;                          // TCP port to connect to

                //Recipients
                $mail->setFrom($from, $fullName);
                $mail->addAddress($to);                             // Add a recipient
                //$mail->addReplyTo('info@example.com', 'Information');
                //$mail->addCC('cc@example.com');
                //$mail->addBCC('bcc@example.com');

                // Attachments
                //$mail->addAttachment('/var/tmp/file.tar.gz');      // Add attachments
                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name

                // Content
                $mail->isHTML(true);                          // Set email format to HTML
                $mail->Subject = $subject;
                $mail->Body    = $content;
                $mail->AltBody = $content;


                $mail->send();

                $response['status'] = true;
                $response['responseCode'] = 0;
                $response['debugCode'] = 200;
                $response['debugMessage'] = "OK";
            } catch (Exception $e) {

                $response['status'] = false;
                $response['responseCode'] = 1;
                $response['debugCode'] = 500;
                $response['debugMessage'] = "Failed";
            }

            $response['debugOutput'] = $GLOBALS['debug']." --- ".$mail->ErrorInfo;

            /*
            set_time_limit(300);

            require_once "Mail.php";
            require_once('Mail/mime.php');

            $fromEmail = '"'.$fullName.'" <'.$from.'>';

            $headers["To"] = $to;

            $encoded_subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';

            $headers["From"] = $fromEmail;
            $headers["Content-Type"] = "text/html; charset=UTF-8";
            $headers["text_charset"] = "UTF-8";
            $headers["html_charset"] = "UTF-8";
            $headers["MIME-Version"] = "1.0";

            $mime = new Mail_mime();
            $mime->setHTMLBody($content);
            $mime->setSubject($encoded_subject);
            $mime->_build_params['text_encoding'] = '7bit';
            $mime->_build_params['text_charset'] = "UTF-8";
            $mime->_build_params['html_charset'] = "UTF-8";
            $mime->_build_params['Content-Type'] = "text/html; charset=UTF-8";

            $mime_params = array(
                'text_encoding' => '7bit',
                'text_charset'  => 'UTF-8',
                'html_charset'  => 'UTF-8',
                'head_charset'  => 'UTF-8'
            );

            //endcode character set

            $body = $mime->get($mime_params);
            $headers = $mime->headers($headers);

            $recipients = $to;

            $mail_engine = "smtp";

            if($mailServer == "" || $port == ""){return false;}

            $smtpinfo["host"] = $mailServer;
            $smtpinfo["port"] = $port;
            $smtpinfo["auth"] = true;
            $smtpinfo["username"] = $from;
            $smtpinfo["password"] = $password;

            // manually handle this host so it will be able to send them emails
            if ($mailServer == "smtp.wemovejax.com") {
                $smtpinfo["socket_options"] = array('ssl' => array('verify_peer_name' => false));
            }

            $smtpinfo["debug"] = true;

            //send the mail
            $mail = Mail::factory($mail_engine, $smtpinfo);


            ob_start();
            $res = $mail->send($recipients, $headers, $body);
            $debugOutput = ob_get_clean();

            if (PEAR::isError($res)) {
                $response['status'] = false;
                $response['responseCode'] = 1; //"Failed to send email";
            } else {
                $response['status'] = true;
                $response['responseCode'] = 0; //"Successfully sent";
            }
            $response['debugOutput'] = $debugOutput;
            */
        }

            return $response;

    }
    // ==================================== END sendMail() ======================================

    // ==================================== START saveInDB() ======================================
    public function getHTMLContent($title,$title2 = "",$msg,$settings = array())
    {
        $defaults = ["buttonHref"=>false,"buttonText"=>false,"width"=>600,"hideHeader"=>false,"hideFooter"=>false];
        $settings = array_merge($defaults, $settings);

        // Returns the default email html format with the main div inside it

        $button = "";
        if($settings['buttonHref'] != NULL && $settings['buttonText'] != NULL){
            $button = '<a href="'.$settings['buttonHref'].'" style="box-sizing:border-box;border-color:#348eda;font-weight:400;text-decoration:none;display:inline-block;margin:0;color:#ffffff;background-color:#348eda;border:solid 1px #348eda;border-radius:2px;font-size:14px;padding:12px 45px" target="_blank">'.$settings['buttonText'].'</a>';
        }

        if($title != ""){
            $title = '<h2 style="margin:0;margin-bottom:30px;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-weight:300;line-height:1.5;font-size:24px;color:#294661!important">'.$title.'</h2>';
        }

        $h3 = "";
        if($title2 != ""){
            $h3 = '<h3 style="margin:0;margin-bottom:30px;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-weight:300;line-height:1.5;font-size:21px;color:#294661!important">'.$title2.'</h3>';
        }

        $header = "";
        if($settings["hideHeader"] != true){
            $header = '<div style="box-sizing:border-box;width:100%;margin-bottom:30px;margin-top:15px">

                        <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                            <tbody>

                            <tr>

                                <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;text-align:center" valign="top">

                                    <span>
                                        <a href="" style="box-sizing:border-box;color:#348eda;font-weight:400;text-decoration:none" target="_blank">
                                            <img alt="Network Leads" height="22" src="https://d2o9xrcicycxrg.cloudfront.net/images/logo_small.png" style="max-width:100%;border-style:none;width:145px;height:auto" width="123">
                                        </a>
                                    </span>

                                </td>

                            </tr>

                            </tbody>

                        </table>

                    </div>';
        }

        $footer = "";
        if($settings["hideFooter"] != true){
            $footer = '<div style="box-sizing:border-box;clear:both;width:100%">

                        <table class="bottomTable" style="box-sizing: border-box;width: 100%;border-spacing: 0;font-size: 12px;border-collapse: separate !important;">

                            <tbody>

                            <tr>
                                <td style="text-align: center">

                                    <span style="float:none;display:block;text-align:center">
                                        <a href="" style="box-sizing:border-box;color:#348eda;font-weight:400;text-decoration:none;font-size:12px" target="_blank">
                                            <img alt="Network Leads" height="16" src="https://d2o9xrcicycxrg.cloudfront.net/images/logotext_small.png" style="max-width:100%;border-style:none;font-size:12px;width:auto;height:30px" width="89">
                                        </a>
                                    </span>


                                    <p style="color:#348eda;">© <span>Network Leads</span> '.date("Y",strtotime('now')).'</p>

                                    <p style="margin:0;color:#294661;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-weight:300;font-size:12px;margin-bottom:5px">
                                        <a href="https://blog.network-leads.com" title="Network Leads Blog" style="box-sizing:border-box;color:#348eda;font-weight:400;text-decoration:underline;font-size:12px;padding:0 5px" target="_blank">Blog</a>
                                        <a href="https://www.twitter.com/thenetworkleads" title="Network Leads Twitter" style="box-sizing:border-box;color:#348eda;font-weight:400;text-decoration:underline;font-size:12px;padding:0 5px" target="_blank">Twitter</a>
                                        <a href="https://www.facebook.com/thenetworkleads/" title="Network Leads Facebook" style="box-sizing:border-box;color:#348eda;font-weight:400;text-decoration:underline;font-size:12px;padding:0 5px" target="_blank">Facebook</a>
                                        <a href="https://www.linkedin.com/company/network-leads/" title="Network Leads LinkedIn" style="box-sizing:border-box;color:#348eda;font-weight:400;text-decoration:underline;font-size:12px;padding:0 5px" target="_blank">LinkedIn</a>
                                    </p>

                                </td>

                            </tr>

                            </tbody>

                        </table>

                    </div>';
        }

        $content = '<!DOCTYPE html><html>

<head>
    <style>

        .mainDiv{
            font-size:16px;
            background-color:#fdfdfd;
            margin:0;
            padding:0;
            font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;
            line-height:1.5;
            height:100%!important;
            width:100%!important
        }

        .mainTable{
            box-sizing:border-box;border-spacing:0;width:100%;background-color:#fdfdfd;border-collapse:separate!important
            width: 100%;
            background-color: #fdfdfd;
        }

        .bottomTable{
            box-sizing:border-box;
            width:100%;
            border-spacing:0;
            font-size:12px;
            border-collapse:separate !important;
        }


    </style>
</head>
<body>


<div class="mainDiv" style="font-size: 16px;background-color: #fdfdfd;margin: 0;padding: 0;font-family: \'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;line-height: 1.5;height: 100%!important;width: 100%!important;" width="100%" border="0" cellspacing="0" cellpadding="0">

    <table class="mainTable" style="box-sizing: border-box;border-spacing: 0;width: 100%;background-color: #fdfdfd;border-collapse: separate!important  width: 100%;" width="100%" border="0" cellspacing="0" cellpadding="0">

        <tbody>

        <tr>

            <td style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;display:block;width:'.$settings["width"].'px;max-width:'.$settings["width"].'px;margin:0 auto!important" valign="top" width="'.$settings["width"].'">

                <div style="box-sizing:border-box;display:block;max-width:'.$settings["width"].'px;margin:0 auto;padding:10px">
                
'.$header.'


                    <div style="box-sizing:border-box;width:100%;margin-bottom:10px;background:#ffffff;border:1px solid #f0f0f0">

                        <table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                            <tbody>

                            <tr>

                                <td style="box-sizing:border-box;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;padding:30px" valign="top">

<table style="box-sizing:border-box;width:100%;border-spacing:0;border-collapse:separate!important" width="100%">

                                        <tbody>

                                        <tr>

                                            <td style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top" valign="top">

                                                   '.$title.'

                                                   '.$h3.'


                                                <p style="margin:0;margin-bottom:30px;color:#294661;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;font-weight:300">
                                                    '.$msg.'
                                                </p>

                                            </td>

                                        </tr>

                                        <tr>

                                            <td style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top" valign="top">

                                                <table cellpadding="0" cellspacing="0" style="box-sizing:border-box;border-spacing:0;width:100%;border-collapse:separate!important" width="100%">

                                                    <tbody>

                                                    <tr>

                                                        <td align="center" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;padding-bottom:15px" valign="top">

                                                            <table cellpadding="0" cellspacing="0" style="box-sizing:border-box;border-spacing:0;width:auto;border-collapse:separate!important">

                                                                <tbody>

                                                                <tr>

                                                                    <td align="center" bgcolor="#348eda" style="box-sizing:border-box;padding:0;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size:16px;vertical-align:top;background-color:#348eda;border-radius:2px;text-align:center" valign="top">
                                                                        '.$button.'
                                                                    </td>

                                                                </tr>

                                                                </tbody>

                                                            </table>

                                                        </td>

                                                    </tr>

                                                    </tbody>

                                                </table>

                                            </td>

                                        </tr>

                                        </tbody>

                                    </table>
                                </td>

                            </tr>

                            </tbody>

                        </table>

                    </div>



'.$footer.'
                </div>

            </td>


        </tr>

        </tbody>

    </table>
</div>

</body>

</html>
';

        return $content;
    }
    // ==================================== END saveInDB() ======================================

    // ==================================== START saveInDB() ======================================
    public function saveInDB($userSent,$from,$to,$subject,$content,$status,$token,$leadId = NULL,$fromSystem = true,$weeklyReport = false,$debugOutput = "",$debugCode = NULL,$debugMessage = "")
    {
        $errorVar = array("email class","saveInDB()",3,"Notes",array(),false);

        $binds = [];
        if($userSent == NULL){
            $binds[] = array(':userSent',NULL,PDO::PARAM_NULL);
        }else{
            $binds[] = array(':userSent',$userSent,PDO::PARAM_STR);
        }
        $binds[] = array(':fromEmail',$from,PDO::PARAM_STR);
        if(is_array($to)){
            $binds[] = array(':toEmail',$to[0],PDO::PARAM_STR);
        }else{
            $binds[] = array(':toEmail',$to,PDO::PARAM_STR);
        }
        if ($fromSystem){
            $binds[] = [':systemEmail',1,PDO::PARAM_INT];
        }else{
            $binds[] = [':systemEmail',0,PDO::PARAM_INT];
        }
        if ($weeklyReport){
            $binds[] = [':weeklyReport',1,PDO::PARAM_INT];
        }else{
            $binds[] = [':weeklyReport',0,PDO::PARAM_INT];
        }
        $binds[] = array(':subject',$subject,PDO::PARAM_STR);
        $binds[] = array(':leadId',$leadId,PDO::PARAM_INT);
        $binds[] = array(':content',$content,PDO::PARAM_STR);
        $binds[] = array(':status',$status,PDO::PARAM_INT);
        $binds[] = array(':token',$token,PDO::PARAM_STR);
        $binds[] = array(':debug',$debugOutput,PDO::PARAM_STR);
        $binds[] = array(':debugCode',$debugCode,PDO::PARAM_STR);
        $binds[] = array(':debugMessage',$debugMessage,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.outgoingMails(userSent,sentDate,fromEmail,toEmail,subject,content,status,token,leadId,systemEmail,weeklyReport,debug,debugCode,debugMessage) VALUES(:userSent,NOW(),:fromEmail,:toEmail,:subject,:content,:status,:token,:leadId,:systemEmail,:weeklyReport,:debug,:debugCode,:debugMessage)",$binds,$errorVar);
        if(!$query){
            return false;
        }else {
            $logId = $GLOBALS['connector']->last_insert_id($query);

            if($status == "0") {
                // notify us by app
                $msg = "Email not sent [id #" . $logId . "][from " . $from . "]";
                shell_exec("/opt/rh/rh-php70/root/usr/bin/php /var/www/html/networkleads/public_html/current/devs/actions/notify.php " . escapeshellarg(serialize(array("msg" => $msg,"env"=>$_SERVER["ENVIRONMENT"]))) . ""); // > /dev/null 2>/dev/null &
            }
            return $logId;
        }
        return false;
    }
    // ==================================== END saveInDB() ======================================

    // ==================================== START isLimitValid() ======================================
    public function isLimitValid($orgId = false,$returnLimit = false,$freeEmails = 1)
    {
        // get the email sending limit number by the organization plan
        // return an array with 'didReach' true/false if the total of emails sent from this organization from beginning of month is bigger than the limit

        if($orgId == false){return false;}


        // get the first day of this month to calculate how many emails sent since then
        $begginigOfMonth = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        $errorVar = array("email class","saveInDB()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':orgId',$orgId,PDO::PARAM_INT);
        $getIt = $GLOBALS['connector']->execute("SELECT plans.emailSendLimit FROM networkleads_db.organizations AS org INNER JOIN plans ON plans.id=org.organizationPackage WHERE org.id=:orgId",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            $r = $GLOBALS['connector']->fetch($getIt);
            $limit = $r["emailSendLimit"];

            $binds[] = array(':begginigOfMonth',$begginigOfMonth,PDO::PARAM_STR);

            $getIt2 = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS oe INNER JOIN users AS us ON us.id=oe.userSent WHERE us.organizationId=:orgId AND oe.sentDate>=:begginigOfMonth",$binds,$errorVar);
            if(!$getIt2){
                return false;
            }else {
                $totalEmailsSent = $GLOBALS['connector']->fetch_num_rows($getIt2);
                if ($returnLimit){
                    $data = [];

                    $data['totalEmailsSent'] = $totalEmailsSent;
                    $data['emailsLimit'] = $limit;

                    return $data;
                }else {
                    if (($totalEmailsSent+$freeEmails) <= $limit) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    // ==================================== END isLimitValid() ======================================
    
    // ==================================== START isLimitValid() ======================================
    public function convertStringPlaceHolders($string,$orgId = NULL,$leadId = NULL){


        $tagsData = $this->getTagsData($orgId,$leadId);

        if(!isset($_SERVER["YMQ_URL"])){
            $_SERVER["YMQ_URL"] = "https://www.your-moving-quote.com";
        }
        $updateInventoryLink = $_SERVER["YMQ_URL"]."/" . $tagsData["leadData"]["secretKey"];

        $currentDate = date("m/d/Y");
        $currentTime = date("H:i (T)");



        if($tagsData["leadData"]["jobNumber"] != NULL && $tagsData["leadData"]["jobNumber"] != ""){
            $string = str_replace("{leadjobnumber}", $tagsData["leadData"]["jobNumber"], $string);
        }else{
            $string = str_replace("{leadjobnumber}", $tagsData["leadData"]["id"], $string);
        }


        $string = str_replace("{currentdate}", $currentDate, $string);
        $string = str_replace("{currenttime}", $currentTime, $string);
        $string = str_replace("{leadfirstname}", ucfirst($tagsData["leadData"]["firstname"]), $string);
        $string = str_replace("{leadlastname}", ucfirst($tagsData["leadData"]["lastname"]), $string);
        $string = str_replace("{leademail}", $tagsData["leadData"]["email"], $string);
        $string = str_replace("{leadphone}", $tagsData["leadData"]["phone"], $string);
        $string = str_replace("{leadfromzip}", $tagsData["movingLeadData"]["fromZip"], $string);
        $string = str_replace("{leadtozip}", $tagsData["movingLeadData"]["toZip"], $string);
        $string = str_replace("{leadfromcity}", $tagsData["movingLeadData"]["fromCity"], $string);
        $string = str_replace("{leadtocity}", $tagsData["movingLeadData"]["toCity"], $string);
        $string = str_replace("{leadfromstate}", $tagsData["movingLeadData"]["fromState"], $string);
        $string = str_replace("{leadtostate}", $tagsData["movingLeadData"]["toState"], $string);
        $string = str_replace("{leadupdateinventory}", $updateInventoryLink, $string);
        $string = str_replace("{leadmovedate}", date("m/d/Y",strtotime($tagsData["movingLeadData"]['requestedDeliveryDateStart'])), $string);
        $string = str_replace("{companyname}", $tagsData['organizationData']['organizationName'], $string);
        $string = str_replace("{companyphone}", $tagsData['organizationData']['phone'], $string);
        $string = str_replace("{companyaddress}", $tagsData['organizationData']['address'], $string);
        $string = str_replace("{companywebsite}", $tagsData['organizationData']['website'], $string);
        $string = str_replace("{companyusdot}", $tagsData['organizationData']['DOT'], $string);
        $string = str_replace("{companymcc}", $tagsData['organizationData']['ICCMC'], $string);
        $string = str_replace("{facebooklink}", $tagsData['organizationData']['facebookLink'], $string);
        $string = str_replace("{instagramlink}", $tagsData['organizationData']['instagramLink'], $string);
        $string = str_replace("{twitterlink}", $tagsData['organizationData']['twitterLink'], $string);
        $string = str_replace("{linkedinlink}", $tagsData['organizationData']['linkedInLink'], $string);
        $string = str_replace("{username}", $tagsData['userData']['fullName'], $string);
        $string = str_replace("{useremail}", $tagsData['userData']['email'], $string);
        $string = str_replace("{userphone}", $tagsData['userData']['phoneNumber'], $string);


        return $string;
    }
    // ==================================== END isLimitValid() ======================================

    // ==================================== START getTagsData() ======================================
    public function getTagsData($orgId = NULL,$leadId = NULL){

        $lead = new lead($leadId, $orgId);
        $movingLead = new movingLead($leadId, $orgId);
        $organization = new organization($orgId);
        if($this->userId != NULL && $this->userId != "") {
            $user = new user($this->userId);
            $userData = $user->getData();
        }


        $leadData = $lead->getData();
        $movingLeadData = $movingLead->getData();
        $organizationData = $organization->getData();

        $tagsData = [];
        $tagsData["leadData"] = $leadData;
        $tagsData["movingLeadData"] = $movingLeadData;
        $tagsData["organizationData"] = $organizationData;
        $tagsData["userData"] = $userData;

        return $tagsData;

    }
    // ==================================== END getTagsData() ======================================

    // ==================================== START isEmailUnsibscribed() ======================================
    public function isEmailUnsibscribed($email = ""){

        $errorVar = array("email class","isEmailUnsibscribed()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(':email',$email,PDO::PARAM_STR);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_db.unsubscribedList WHERE orgId=:orgId AND email=:email",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else {
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }
        }
        return false;
    }
    // ==================================== END isEmailUnsibscribed() ======================================

}