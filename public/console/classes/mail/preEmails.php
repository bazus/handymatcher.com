<?php
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/mail/email.php");

class preEmails
{
    private $orgId;
    // ==================================== START CONSTRUCTOR ======================================
    function __construct($id) {     $this->orgId = $id;     }

    public function getData($folderId = NULL,$getAll = false){

        $errorVar = array("preEmails","getData()",3,"Notes",array(),false);

        $pre = [];

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        if ($getAll){
            $query = $GLOBALS['connector']->execute("SELECT pe.*,d.title AS dTitle FROM networkleads_db.preEmails AS pe LEFT JOIN networkleads_db.departments AS d ON d.id=pe.department WHERE pe.orgId=:orgId AND pe.isDeleted=0 ORDER BY pe.id DESC", $binds, $errorVar);
        }else {
            if ($folderId == NULL || $folderId == null || $folderId == "NULL" || $folderId == "null") {
                $query = $GLOBALS['connector']->execute("SELECT pe.*,d.title AS dTitle FROM networkleads_db.preEmails AS pe LEFT JOIN networkleads_db.departments AS d ON d.id=pe.department WHERE pe.orgId=:orgId AND pe.isDeleted=0 AND pe.folderId IS NULL ORDER BY pe.id DESC", $binds, $errorVar);
            } else {
                $binds[] = array(':folderId', $folderId, PDO::PARAM_INT);

                $query = $GLOBALS['connector']->execute("SELECT pe.*,d.title AS dTitle FROM networkleads_db.preEmails AS pe LEFT JOIN networkleads_db.departments AS d ON d.id=pe.department WHERE pe.orgId=:orgId AND pe.isDeleted=0 AND pe.folderId=:folderId ORDER BY pe.id DESC", $binds, $errorVar);
            }
        }
        if(!$query){
            return false;
        }else{
            while($preEmails = $GLOBALS['connector']->fetch($query)){
                if ($preEmails['title'] == ""){
                    $preEmails['title'] = $preEmails['subject'];
                }

                // this is a bandaid fix foe a ' problem - this need to be handeled all over the system!!!!!!!!!!
                $preEmails["subject"] = addslashes($preEmails["subject"]);

                $preEmails['content'] = html_entity_decode($preEmails['content']);
                $pre[] = $preEmails;
            }
            return $pre;
        }

        return false;
    }

    public function getFoldersData(){

        $errorVar = array("preEmails","getFoldersData()",3,"Notes",array(),false);

        $folders = [];

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("SELECT id,name FROM networkleads_db.preEmailsFolders WHERE orgId=:orgId AND isDeleted=0 ORDER BY id ASC",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            while($folder = $GLOBALS['connector']->fetch($query)){
                $folders[] = $folder;
            }
            return $folders;
        }

        return false;
    }

    public function getDataById($id){

        $errorVar = array("preEmails","getDataById()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(':id',$id,PDO::PARAM_INT);

        $query = $GLOBALS['connector']->execute("SELECT pe.*,d.title AS dTitle FROM networkleads_db.preEmails AS pe LEFT JOIN networkleads_db.departments AS d ON d.id=pe.department WHERE pe.orgId=:orgId AND pe.isDeleted=0 AND pe.id=:id",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            $preEmails = $GLOBALS['connector']->fetch($query);
            if ($preEmails) {
                $preEmails['content'] = html_entity_decode($preEmails['content']);

                if ($preEmails && $preEmails['title'] == "") {
                    $preEmails['title'] = $preEmails['subject'];
                }
                return $preEmails;
            }else{
                return false;
            }
        }

        return false;
    }

    public function getActiveData($department = NULL){

        $errorVar = array("preEmails","getActiveData()",3,"Notes",array(),false);

        $pre = [];

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(':depId',$department,PDO::PARAM_INT);
        $query = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.preEmails WHERE orgId=:orgId AND (department=:depId OR department IS NULL) AND isDeleted=0",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            while($preEmails = $GLOBALS['connector']->fetch($query)){
                if ($preEmails['title'] == ""){
                    $preEmails['title'] = $preEmails['subject'];
                }
                $pre[] = $preEmails;
            }
        }

        return $pre;
    }

    public function setData($data){

        $errorVar = array("preEmails","setData()",3,"Notes",array(),false);
        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = [":title",$data['title'],PDO::PARAM_STR];
        $binds[] = [":subject",$data['subject'],PDO::PARAM_STR];
        $binds[] = [":content",htmlentities($data['content']),PDO::PARAM_STR];
        $binds[] = [":contentJson",htmlentities($data['contentJson']),PDO::PARAM_STR];
        $binds[] = [":department",$data['department'],PDO::PARAM_STR];
        if ($data['folder'] == NULL || $data['folder'] == null || $data['folder'] == "NULL" || $data['folder'] == "null"){
            $binds[] = [":folderId",NULL,PDO::PARAM_INT];
        }else{
            $binds[] = [":folderId",$data['folder'],PDO::PARAM_INT];
        }
        $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.preEmails(orgId,title,subject,content,contentJson,department,folderId) VALUES(:orgId,:title,:subject,:content,:contentJson,:department,:folderId)",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

        return false;

    }

    public function updateData($id,$content,$contentJson){

        $errorVar = array("preEmails","updateData()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(":id",$id,PDO::PARAM_INT);
        $binds[] = array(":content",htmlentities($content),PDO::PARAM_STR);
        $binds[] = array(":contentJson",htmlentities($contentJson),PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.preEmails SET content=:content,contentJson=:contentJson WHERE id=:id AND orgId=:orgId",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

        return false;

    }

    public function updateInfoData($id,$title,$subject,$department){

        $errorVar = array("preEmails","updateInfoData()",3,"Notes",array(),false);
        if ($department == "null"|| $department == "NULL" || $department == ""){
            $department = NULL;
        }
        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(":id",$id,PDO::PARAM_INT);
        $binds[] = array(":title",$title,PDO::PARAM_STR);
        $binds[] = array(":subject",$subject,PDO::PARAM_STR);
        $binds[] = array(":department",$department,PDO::PARAM_STR);

        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.preEmails SET title=:title,subject=:subject,department=:department WHERE id=:id AND orgId=:orgId",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

        return false;

    }

    public function setAsDeleted($id){

        $errorVar = array("preEmails","setAsDeleted()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(':id',$id,PDO::PARAM_INT);


        $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.preEmails SET isDeleted=1 WHERE id=:id AND orgId=:orgId",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

        return false;

    }

    function getPreEmail($number,$data){
        $resp = array();
        $resp["subject"] = "";
        $resp["content"] = "";

        $organizationLogo = "";
        $userName = "";
        $departmentTitle = "";
        $organizationName = "";
        $organizationPhone = "";
        $organizationWebsite = "";


        if($data["organizationLogo"] != ""){
            $organizationLogo = '<p style="text-align: center;"><img height="130px" src="'.$data["organizationLogo"].'" /></p>';
        }

        if($data["userName"] != ""){
            $userName = $data["userName"].'<br>';
        }

        if($data["departmentTitle"] != ""){
            $departmentTitle = $data["departmentTitle"].'<br>';
        }

        if($data["organizationName"] != ""){
            $organizationName = $data["organizationName"].'<br>';
        }

        if($data["organizationPhone"] != ""){
            $organizationPhone = $data["organizationPhone"].'<br>';
        }

        if($data["organizationWebsite"] != ""){
            $organizationWebsite = '<a href="'.$data["organizationWebsite"].'">'.$data["organizationWebsite"].'</a>';
        }

        if($number == 1){
            $resp["subject"] = "No Response";
            $resp["content"] = $organizationLogo.'<table align=\'CENTER\' border=\'0\' style="margin-right: calc(1%); width: 99%;" width=\'3D"700"\'><tbody><tr><td align=\'3D"=\'><div style="text-align: right;">'.date("l, m/d/Y",strtotime("now")).'</div></td></tr><tr><td><div data-empty="true" style="text-align: right;"><br></div>Dear {leadfirstname},<br><br>We have been unable to contact you so that we can provide you with a moving estimate.<p>You may contact us via phone at any time to let us know that you will not be needing our services.<br>Of course, if you are still interested in our moving expertise, don\'t hesitate to reach out.</p><br><a href="{leadupdateinventory}" rel=\'=3D"noopener\' target=\'3D"_blank"\'><strong>Click Here</strong></a> in order to view or alter your personal information and the inventory list.<br><br>Sincerely,<br><br>'.$userName.$departmentTitle.$organizationName.$organizationPhone.$organizationWebsite.'</td></tr></tbody></table><p><br></p>';
        }

        if($number == 2){


            $jobNumber = $data["leadId"];
            $organizationName = $data["organizationName"];
            $organizationAddress = $data["organizationAddress"];
            $organizationCity = $data["organizationCity"];
            $organizationState = $data["organizationState"];
            $organizationZip = $data["organizationZip"];
            $organizationPhone = $data["organizationPhone"];
            $organizationWebsite = $data["organizationWebsite"];
            $usdot = $data["DOT"];
            $mc = $data["MC"];
            $registration = $data["registration"];
            $representative = $data["userName"];

            $fromAddress = $data["fromAddress"];
            $fromCity = $data["fromCity"];
            $fromState = $data["fromState"];
            $fromZip = $data["fromZip"];

            $toAddress = $data["toAddress"];
            $toCity = $data["toCity"];
            $toState = $data["toState"];
            $toZip = $data["toZip"];

            $estimatedDate = date("m/d/Y",strtotime("now"));
            $moveType = $data["moveType"];
            $moveDistance = $data["moveDistance"];
            $estimatedVolume = $data["estimateData"]["initPrice"]." cf<br>$".$data["estimateData"]["pricePerCf"]." per cf";
            $moveDay = $data["moveDay"];
            $requestedMoveDate = date("m/d/Y",strtotime($data["moveDate"]));

            $basicEstimatePrice = "$9999";
            $fuel = "$999 (%10)";
            $totalEstimate = "10,998.00";


            $itemsTable = '<br><br><table  align="center" border="1" cellpadding="0" cellspacing="0" width="98%"><td align="center" style="font-weight:bold">Qty</td><td align="center" style="font-weight:bold">Item</td><td align="center" style="font-weight:bold">CuFt</td>';

            foreach ($data["estimateItems"] as $estimateItem){
                $itemName = $estimateItem["name"];
                $itemCf = $estimateItem["cf"];
                $itemQty = $estimateItem["amount"];

                $itemsTable .= '<tr>
        <td width="20%" align="center">'.$itemQty.'</td>
        <td width="60%" align="center">'.$itemName.'</td>
        <td width="20%" align="center">'.$itemCf.'</td>
    </tr>';


            }
            $itemsTable .= '</table>';

            $resp["subject"] = "Moving Estimate";

            $content = $organizationLogo.'<table align="center" border="1" cellpadding="0" cellspacing="0" width="98%"><tbody><tr><td colspan="2" width="100%"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><strong>Binding Moving Estimate</strong></td></tr></tbody></table></td></tr><tr><td valign="top" width="48%"><table border="0" cellpadding="3" cellspacing="1" width="100%"><tbody><tr><td width="100%"><strong>'.$organizationName.'</strong></td></tr><tr><td width="100%"><strong>'.$organizationAddress.'</strong></td></tr><tr><td width="100%"><strong>'.$organizationCity.', '.$organizationState.' '.$organizationZip.'</strong></td></tr><tr><td width="100%"><strong>US DOT: '.$usdot.' &nbsp; MC: '.$mc.'</strong></td></tr><tr><td width="100%"><strong>Registration #'.$registration.'</strong></td></tr></tbody></table></td><td valign="top" width="52%"><table border="0" cellpadding="3" cellspacing="1" width="100%"><tbody><tr><td width="49%"><strong>Representative:</strong></td><td width="79%"><strong>'.$representative.'</strong></td></tr><tr><td width="21%"><strong>Phone:</strong></td><td width="79%"><strong>'.$organizationPhone.'</strong></td></tr><tr><td width="21%"><strong>Web:</strong></td><td width="79%"><a href="'.$organizationWebsite.'" target="_blank">'.$organizationWebsite.'</a></td></tr></tbody></table></td></tr></tbody></table><table align="center" border="1" cellpadding="0" cellspacing="0" width="98%"><tbody><tr><td align="center" width="48%"><strong>Moving From</strong></td><td align="center" width="52%"><strong>Moving To</strong></td></tr><tr><td valign="top" width="48%"><table border="0" cellpadding="2" cellspacing="1" width="100%"><tbody><tr><td>'.$fromAddress.'</td></tr><tr><td>'.$fromCity.', '.$fromState.' '.$fromZip.'</td></tr></tbody></table></td><td valign="top" width="52%"><table border="0" cellpadding="2" cellspacing="1" width="100%"><tbody><tr><td>'.$toAddress.'</td></tr><tr><td>'.$toCity.', '.$toState.' '.$toZip.'</td></tr></tbody></table></td></tr></tbody></table><table align="center" border="1" cellpadding="0" cellspacing="0" width="98%"><tbody><tr><td valign="top" width="48%"><table cellpadding="2" cellspacing="1" width="100%"><tbody><tr><td align="CENTER" colspan="2" width="100%"><strong>Relocation Details</strong></td></tr><tr><td width="42%"><strong>Job #:</strong></td><td align="RIGHT" width="58%"><strong>'.$jobNumber.'</strong>&nbsp;</td></tr><tr><td width="50%"><strong>Estimate Date:</strong></td><td width="50%" align="RIGHT"><strong>'.$estimatedDate.'</strong>&nbsp;</td></tr><tr><td width="42%"><strong>Move Type:</strong></td><td width="58%" align="RIGHT"><strong>'.$moveType.'</strong>&nbsp;</td></tr><tr><tr><td width="42%"><strong>Move Distance:</strong></td><td width="58%" align="RIGHT"><strong>'.$moveDistance.'</strong>&nbsp;</td></tr><tr><td width="48%"><strong>Estimated Volume:</strong></td><td width="52%" align="RIGHT"><strong>'.$estimatedVolume.'</strong>&nbsp;</td></tr><tr><td width="42%"><strong>Move Day:</strong></td><td width="58%" align="RIGHT"><strong>'.$moveDay.'</strong>&nbsp;</td></tr><tr><td width="42%"><strong>Requested Move Date:</strong></td><td width="58%" align="RIGHT"><strong>'.$requestedMoveDate.'</strong>&nbsp;</td></tr></tbody></table></td><td valign="top" width="52%"><table cellpadding="2" cellspacing="1" width="100%"><tbody><tr><td align="center" colspan="2" width="100%"><strong>Relocation Estimate</strong>&nbsp;</td></tr><tr><td width="275"><strong>Basic Estimate Price</strong></td><td align="RIGHT" width="76"><strong>'.$basicEstimatePrice.'</strong></td></tr><tr><td width="275"><strong>Fuel Surcharge:</strong></td><td align="RIGHT" width="76"><strong>'.$fuel.'</strong></td></tr><tr><td width="275"><strong>Total Moving Estimate</strong></td><td align="RIGHT" width="76"><strong>'.$totalEstimate.'</strong></td></tr></tbody></table></td></tr></tbody></table>'.$itemsTable;

            $email = new email();
            $resp["content"] = $email->getHTMLContent("","",$content,["width"=>839,"hideHeader"=>true,"hideFooter"=>true]);

        }

        return $resp;
    }


    public function createFolder($name){

        $errorVar = array("preEmails","createFolder()",3,"Notes",array(),false);

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = [":name",$name,PDO::PARAM_STR];

        $query = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.preEmailsFolders(orgId,name) VALUES(:orgId,:name)",$binds,$errorVar);

        if(!$query){
            return false;
        }else{
            return true;
        }

        return false;

    }

    public function changeFolder($folder,$id){

        $errorVar = array("preEmails","changeFolder()",3,"Notes",array(),false);
        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        if ($folder == NULL || $folder == null || $folder == "NULL" || $folder == "null" ){
            $binds[] = array(':id',$id,PDO::PARAM_INT);

            $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.preEmails SET folderId=NULL WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        }else {
            $binds[] = array(':folderId',$folder,PDO::PARAM_INT);
            $binds[] = array(':id',$id,PDO::PARAM_INT);

            $query = $GLOBALS['connector']->execute("UPDATE networkleads_db.preEmails SET folderId=:folderId WHERE id=:id AND orgId=:orgId",$binds,$errorVar);
        }

        if(!$query){
            return false;
        }else{
            return true;
        }

        return false;

    }

}