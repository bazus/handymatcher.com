<?php
/**
 * User
 *
 * PHP Versions  7
 *
 * @category  User
 * @author    Niv Apo <niv@apo.co.il>
 * @copyright (21.8.18) 2018 Niv Apo
 * @license   ---
 */
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/security/passwords.php");


class mailaccounts
{
    protected $userId;
    protected $orgId;

    // ==================================== START CONSTRUCTOR ======================================
    public function __construct($userId,$orgId = false) {
        $this->userId = $userId;
        $this->orgId = $orgId;
    }
    // ==================================== END CONSTRUCTOR ======================================

    // ==================================== START getMyAccounts() ======================================
    public function getMyAccounts($alsoGetMyOrganizationAccounts = false, $getOnlyActive = false,$mailAccount = false){
        $errorVar = array("mailaccounts","getMyAccounts()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        if($alsoGetMyOrganizationAccounts){
            // Get all organization accounts
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            if ($getOnlyActive == true){
                $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,email,dateCreated,isActive,isDeleted,isPrivate,orgId,userId,accountType,requiredSSL,otherData FROM networkleads_db.mailbox_accounts WHERE (orgId=:orgId OR userId=:userId) AND isDeleted=0 AND IF(isPrivate=1,IF(userId=:userId,1,0) = 1,1) AND isActive=1", $binds, $errorVar);
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,email,dateCreated,isActive,isDeleted,isPrivate,orgId,userId,accountType,requiredSSL,otherData FROM networkleads_db.mailbox_accounts WHERE (orgId=:orgId OR userId=:userId) AND isDeleted=0 AND IF(isPrivate=1,IF(userId=:userId,1,0) = 1,1)", $binds, $errorVar);
            }
        }else {
            // Get only user accounts
            if ($getOnlyActive == true){
                $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,email,dateCreated,isActive,isDeleted,isPrivate,orgId,userId,accountType,requiredSSL,otherData FROM networkleads_db.mailbox_accounts WHERE userId=:userId AND isDeleted=0 AND isActive=1", $binds, $errorVar);
            }else{
                $getIt = $GLOBALS['connector']->execute("SELECT id,fullName,email,dateCreated,isActive,isDeleted,isPrivate,orgId,userId,accountType,requiredSSL,otherData FROM networkleads_db.mailbox_accounts WHERE userId=:userId AND isDeleted=0", $binds, $errorVar);
            }
        }
        if(!$getIt){
            return false;
        }else{
            $accounts = array();
            while($account = $GLOBALS['connector']->fetch($getIt)){
                if ($mailAccount){
                    if ($account['id'] == $mailAccount){
                        $account['isDefault'] = true;
                    }else{
                        $account['isDefault'] = false;
                    }
                }else{
                    $account['isDefault'] = false;
                }
                $accounts[] = $account;
            }
            return $accounts;
        }

        return false;

    }
    // ==================================== END getMyAccounts() ======================================

    // ==================================== START getAccountById() ======================================
    public function getAccountById($accountId = ""){
        $errorVar = array("mailaccounts","getAccountById()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.mailbox_accounts WHERE orgId=:orgId AND id=:id AND isDeleted=0 AND IF(isPrivate = 1,IF(userId=:userId,TRUE,FALSE),TRUE)", $binds, $errorVar);
        if(!$getIt){
            var_dump($GLOBALS['connector']->getLastError()); //get error from queries
            return false;
        }else{
            $account = $GLOBALS['connector']->fetch($getIt);
            return $account;
        }

        return false;

    }
    // ==================================== END getAccountById() ======================================

    // ==================================== START doesAccountExistsAndActive() ======================================
    public function doesAccountExistsAndActive($email = "",$accountType = NULL){
        $errorVar = array("mailaccounts","doesAccountExistsAndActive()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':email', $email, PDO::PARAM_INT);

        if($accountType != NULL){
            $binds[] = array(':accountType', $accountType, PDO::PARAM_STR);
            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_db.mailbox_accounts WHERE orgId=:orgId AND userId=:userId AND isDeleted=0 AND email=:email AND accountType=:accountType", $binds, $errorVar);
        }else{
            $getIt = $GLOBALS['connector']->execute("SELECT COUNT(id) FROM networkleads_db.mailbox_accounts WHERE orgId=:orgId AND userId=:userId AND isDeleted=0 AND email=:email", $binds, $errorVar);
        }
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                $binds = array();
                $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
                $binds[] = array(':email', $email, PDO::PARAM_INT);

                $getIt = $GLOBALS['connector']->execute("SELECT id FROM networkleads_db.mailbox_accounts WHERE orgId=:orgId AND userId=:userId AND isDeleted=0 AND email=:email", $binds, $errorVar);
                $r = $GLOBALS['connector']->fetch($getIt);
                return $r["id"];
            }
        }

        return false;

    }
    // ==================================== END doesAccountExistsAndActive() ======================================

    // ==================================== START updateAccountOtherData() ======================================
    public function updateAccountOtherData($accountId = "",$otherData = ""){
        $errorVar = array("mailaccounts","updateAccountOtherData()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':otherData', $otherData, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET otherData=:otherData WHERE id=:id AND orgId=:orgId AND userId=:userId", $binds, $errorVar);
        if(!$getIt){
            return false;
        }else{
            return true;
        }

        return false;

    }
    // ==================================== END updateAccountOtherData() ======================================

    // ==================================== START doesHaveActiveAccount() ======================================
    public function doesHaveActiveAccount(){
        $errorVar = array("mailaccounts","doesHaveActiveAccount()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.mailbox_accounts WHERE userId=:userId AND isActive=1 AND isDeleted=0",$binds,$errorVar);
        if(!$getIt){
            return false;
        }else{
            if($GLOBALS['connector']->fetch_num_rows($getIt) > 0){
                return true;
            }
        }

        return false;

    }
    // ==================================== END doesHaveActiveAccount() ======================================

    // ==================================== START disableAccount() ======================================
    public function disableAccount($accountId = "",$orgId = ""){
        $errorVar = array("mailaccounts","disableAccount()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET isActive=0 WHERE id=:id AND (userId=:userId OR orgId=:orgId)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;

    }
    // ==================================== END disableAccount() ======================================

    // ==================================== START reActivateAccount() ======================================
    public function reActivateAccount($accountId = "",$orgId = ""){
        $errorVar = array("mailaccounts","reActivateAccount()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $orgId, PDO::PARAM_INT);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET isActive=1 WHERE id=:id AND (userId=:userId OR orgId=:orgId)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;

    }
    // ==================================== END reActivateAccount() ======================================

    // ==================================== START deleteAccount() ======================================
    public function deleteAccount($accountId = "",$isAdmin = false){
        $errorVar = array("mailaccounts","deleteAccount()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        if ($isAdmin) {
            $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET isDeleted=1 WHERE id=:id AND orgId=:orgId", $binds, $errorVar);
        }else{
            $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
            $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET isDeleted=1 WHERE id=:id AND userId=:userId", $binds, $errorVar);
        }

        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END deleteAccount() ======================================

    // ==================================== START updateAccountPassword() ======================================
    public function updateAccountPassword($accountId = "",$password = ""){
        $errorVar = array("mailaccounts","updateAccountPassword()",3,"Notes",array());

        $passwords = new passwords();

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':password', $passwords->hashCC($password), PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET password=:password WHERE id=:id AND userId=:userId",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }

        return false;
    }
    // ==================================== END updateAccountPassword() ======================================

    // ==================================== START createAccount() ======================================
    public function createAccount($outgoingmailserver,$fullName,$emailAddress,$emailPassword,$emailPort,$accountType,$otherData = "",$requiredSSL = false){

        $response = array();
        $response['status'] = false;
        $response['accountId'] = false;

        $passwords = new passwords();

        $errorVar = array("mailaccounts","createAccount()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_STR);
        $binds[] = array(':outgoingmailserver', $outgoingmailserver, PDO::PARAM_STR);
        $binds[] = array(':fullName', $fullName, PDO::PARAM_STR);
        $binds[] = array(':email', $emailAddress, PDO::PARAM_STR);
        $binds[] = array(':password', $passwords->hashCC($emailPassword), PDO::PARAM_STR);
        $binds[] = array(':port', $emailPort, PDO::PARAM_STR);
        $binds[] = array(':accountType', $accountType, PDO::PARAM_STR);
        $binds[] = array(':otherData', $otherData, PDO::PARAM_STR);

        $binds[] = array(':requiredSSL', filter_var($requiredSSL, FILTER_VALIDATE_BOOLEAN) , PDO::PARAM_BOOL); // This will convert the string 'true' or 'false' to BOOL true or false

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.mailbox_accounts(userId,orgId,outgoingmailserver,fullName,email,password,port,isActive,isDeleted,dateCreated,accountType,otherData,requiredSSL) VALUES(:userId,:orgId,:outgoingmailserver,:fullName,:email,:password,:port,1,0,NOW(),:accountType,:otherData,:requiredSSL)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            $response['status'] = true;
            $response['accountId'] = $GLOBALS['connector']->last_insert_id();
            return $response;
        }

        return false;

    }
    // ==================================== END createAccount() ======================================

    // ==================================== START togglePrivateAccount() ======================================
    public function togglePrivateAccount($accountId){

        $response = array();
        $response['status'] = false;
        $response['resp'] = false;

        $errorVar = array("mailaccounts","togglePrivateAccount()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':id', $accountId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);


        $setIt = $GLOBALS['connector']->execute("UPDATE networkleads_db.mailbox_accounts SET isPrivate=NOT isPrivate WHERE id=:id AND userId=:userId",$binds,$errorVar);
        if(!$setIt){

        }else{

            $getIt = $GLOBALS['connector']->execute("SELECT isPrivate FROM networkleads_db.mailbox_accounts WHERE id=:id AND userId=:userId",$binds,$errorVar);
            if(!$getIt){

            }else{
                $r = $GLOBALS['connector']->fetch($getIt);
                $response['status'] = true;
                $response['resp'] = $r["isPrivate"];
            }

        }

        return $response;

    }
    // ==================================== END togglePrivateAccount() ======================================
    // ==================================== START getAccountByUserId() ======================================
    public function getAccountByUserId($accountId = ""){
        $errorVar = array("mailaccounts","getAccountByUserId()",3,"Notes",array());

        $binds = array();
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':userId', $this->userId, PDO::PARAM_INT);
        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.mailbox_accounts WHERE orgId=:orgId AND isDeleted=0 AND IF(isPrivate = 1,IF(userId=:userId,TRUE,FALSE),TRUE)", $binds, $errorVar);
        if(!$getIt){
            var_dump($GLOBALS['connector']->getLastError()); //get error from queries
            return false;
        }else{
            $account = $GLOBALS['connector']->fetch($getIt);
            return $account;
        }

        return false;

    }
    // ==================================== END getAccountById() ======================================
}