<?php
/**
 * Created by PhpStorm.
 * User: maortamir
 * Date: 02/12/2018
 * Time: 20:50
 */
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/system/timeController.php");
require_once($_SERVER['LOCAL_NL_PATH']."/console/classes/user/user.php");

class mailCenter
{
    public $orgId;
    function __construct($id){$this->orgId = $id;}

    public function getEmails($userId = null,$searchByUser = null,$sDate,$eDate){

        $errorVar = array("mailCenter","getEmails()",4,"Notes",array());

        $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        $eDate = date("Y-m-d 23:59:59",strtotime($eDate));

        $emails = [];

        $binds = [];
        if ($userId !== null){
            // user is not an admin
            $binds[] = [':userId',$userId,PDO::PARAM_INT];
            $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
            $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
            $binds[] = [':OFFSET',NULL,PDO::PARAM_STR,true];

            $getIt = $GLOBALS['connector']->execute("SELECT id,userSent,CTZ(sentDate,:OFFSET) AS sentDate,leadId,fromEmail,toEmail,subject,content,didOpen,CTZ(openDate,:OFFSET) AS openDate,status,statusMsg,token,systemEmail,weeklyReport FROM networkleads_db.outgoingMails WHERE userSent=:userId AND systemEmail=0 AND CTZ(sentDate,:OFFSET)>=:startDate AND CTZ(sentDate,:OFFSET)<=:endDate ORDER BY id DESC", $binds, $errorVar);
        }else{
            // user is an admin
            if ($searchByUser){
                // admin - showing emails by user selected in top select
                $binds[] = [':orgId',$this->orgId,PDO::PARAM_INT];
                $binds[] = [':userId',$searchByUser,PDO::PARAM_INT];
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = [':OFFSET',NULL,PDO::PARAM_STR,true];

                $getIt = $GLOBALS['connector']->execute("SELECT mail.id,mail.userSent,CTZ(mail.sentDate,:OFFSET) AS sentDate,mail.leadId,mail.fromEmail,mail.toEmail,mail.subject,mail.content,mail.didOpen,CTZ(mail.openDate,:OFFSET) AS openDate,mail.status,mail.statusMsg,mail.token,mail.systemEmail,mail.weeklyReport FROM networkleads_db.outgoingMails AS mail INNER JOIN networkleads_db.users AS u ON u.id=mail.userSent WHERE u.organizationId=:orgId AND mail.systemEmail=0 AND u.id=:userId  AND CTZ(mail.sentDate,:OFFSET)>=:startDate AND CTZ(mail.sentDate,:OFFSET)<=:endDate ORDER BY mail.id DESC", $binds, $errorVar);
            }else{
                // admin - getting all of hes organization results
                $binds[] = [':orgId',$this->orgId,PDO::PARAM_INT];
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = [':OFFSET',NULL,PDO::PARAM_STR,true];

                $getIt = $GLOBALS['connector']->execute("SELECT mail.id,mail.userSent,CTZ(mail.sentDate,:OFFSET) AS sentDate,mail.leadId,mail.fromEmail,mail.toEmail,mail.subject,mail.content,mail.didOpen,CTZ(mail.openDate,:OFFSET) AS openDate,mail.status,mail.statusMsg,mail.token,mail.systemEmail,mail.weeklyReport FROM networkleads_db.outgoingMails AS mail INNER JOIN networkleads_db.users AS u ON u.id=mail.userSent WHERE u.organizationId=:orgId AND mail.systemEmail=0  AND CTZ(mail.sentDate,:OFFSET)>=:startDate AND CTZ(mail.sentDate,:OFFSET)<=:endDate ORDER BY mail.id DESC", $binds, $errorVar);
            }
        }
        if (!$getIt){
            return false;
        }else{

            $timeController = new timeController();
            $emails['emails'] = [];
            // get organization results, format date of sentDate
            while ($email = $GLOBALS['connector']->fetch($getIt)){
                $convertedLastLead = $timeController->convertv2($email['sentDate'],"m/d/Y h:i A");
                $convertedOpened = $timeController->convertv2($email['openDate'],"m/d/Y h:i A");

                $userSentData = NULL;
                if($email["userSent"] != "" && $email["userSent"] != NULL){
                    $user = new user($email["userSent"]);
                    $userData = $user->getData();
                    $userSentData["fullName"] = $userData["fullName"];
                    $userSentData["email"] = $userData["email"];
                }

                $email['sentDate'] = date("F j, Y h:i A",strtotime($email['sentDate']));
                $email['sentDateConverted'] = $convertedLastLead;
                $email['openDate'] = date("F j, Y h:i A",strtotime($email['openDate']));
                $email['openDateText'] = $convertedOpened;
                $email['userSentData'] = $userSentData;
                $emails['emails'][] = $email;
            }
            return $emails;
        }
        return false;

    }

    public function getEmailsToLead($leadId){
        $errorVar = array("mailCenter","getEmailsToLead()",3,"Notes",array());

        $mails = [];

        $binds = [];
        $binds[] = [':leadId',$leadId,PDO::PARAM_INT];
        $binds[] = [':OFFSET',NULL,PDO::PARAM_STR,true];

        $getIt = $GLOBALS['connector']->execute("SELECT om.*,u.fullName,CTZ(om.sentDate,:OFFSET) AS sentDate,CTZ(om.openDate,:OFFSET) AS openDate FROM networkleads_db.outgoingMails AS om LEFT JOIN networkleads_db.users AS u ON u.id=om.userSent WHERE om.leadId=:leadId",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            $timeController = new timeController();
            while($res = $GLOBALS['connector']->fetch($getIt)){
                $res['timeSent'] = $timeController->convert($res['sentDate']);
                if($res['didOpen'] == 1){
                    $res['timeFormatted'] = date("F jS, H:i",strtotime($res['openDate']));
                }
                $mails[] = $res;
            }
            return $mails;
        }
    }
    //not in use
    public function searchInEmails($userId = null,$searchByUser = null,$search = null){

        // $page => the current page, showing 25 results a page
        // $searchByUser => Admin select to show User emails
        // $search => Search in emails by these words

        $errorVar = array("mailCenter","searchEmails()",3,"Notes",array());

//        if ($page == 0){
//            $page = 0;
//        }else{
//            $page = ($page * $this->perPage);
//        }

        $emails = [];

        $searchQuery = "%" . strtolower($search) . "%";

        $binds = [];
//        $binds[] = [":page",$page,PDO::PARAM_INT];
        $binds[] = [':search', $searchQuery, PDO::PARAM_STR];

        if ($userId !== null){
            // user isn't an admin
            $binds[] = [':userId',$userId,PDO::PARAM_INT];

            $getIt = $GLOBALS['connector']->execute("SELECT id,userSent,sentDate,leadId,fromEmail,toEmail,subject,content,didOpen,openDate,status,statusMsg,token,systemEmail,weeklyReport FROM networkleads_db.outgoingMails WHERE userSent=:userId AND systemEmail=0 AND (LOWER(subject) LIKE :search OR LOWER(content) LIKE :search OR LOWER(fromEmail) LIKE :search OR LOWER(toEmail) LIKE :search) ORDER BY id DESC", $binds, $errorVar);
        }else{
            // user is an admin
            $binds[] = [':orgId',$this->orgId,PDO::PARAM_INT];

            if ($searchByUser){
                // admin - showing emails by user selected in top select
                $binds[] = [':userId',$searchByUser,PDO::PARAM_INT];

                $getIt = $GLOBALS['connector']->execute("SELECT mail.* FROM networkleads_db.outgoingMails AS mail INNER JOIN networkleads_db.users AS u ON u.id=mail.userSent WHERE u.organizationId=:orgId AND mail.systemEmail=0 AND u.id=:userId AND (LOWER(mail.subject) LIKE :search OR LOWER(mail.content) LIKE :search OR LOWER(mail.fromEmail) LIKE :search OR LOWER(mail.toEmail) LIKE :search) ORDER BY mail.id DESC", $binds, $errorVar);
            }else{
                // admin - getting all of hes organization results
                $getIt = $GLOBALS['connector']->execute("SELECT mail.* FROM networkleads_db.outgoingMails AS mail INNER JOIN networkleads_db.users AS u ON u.id=mail.userSent WHERE u.organizationId=:orgId AND mail.systemEmail=0 AND (LOWER(mail.subject) LIKE :search OR LOWER(mail.content) LIKE :search OR LOWER(mail.fromEmail) LIKE :search OR LOWER(mail.toEmail) LIKE :search) ORDER BY mail.id DESC", $binds, $errorVar);
            }
        }
        if (!$getIt){
            return false;
        }else{

            $timeController = new timeController();

            // get organization results, format date of sentDate
            while ($email = $GLOBALS['connector']->fetch($getIt)){

                $email['subject'] = str_ireplace($search,"<b>".$search."</b>",$email['subject']);
                $email['fromEmail'] = str_ireplace($search,"<b>".$search."</b>",$email['fromEmail']);
                $email['toEmail'] = str_ireplace($search,"<b>".$search."</b>",$email['toEmail']);

                $convertedLastLead = $timeController->convert($email['sentDate']);

                $email['sentDate'] = date("F j, Y H:i",strtotime($email['sentDate']));
                $email['sentDateConverted'] = $convertedLastLead;

                $emails['emails'][] = $email;
            }

            // counting results to show top right and count the pages in JS, by admin/admin selecting user/normal user
            if ($userId == null) {
                // user is an admin
                $binds = [];
                $binds[] = [':orgId',$this->orgId,PDO::PARAM_INT];
                if ($searchByUser) {
                    $binds[] = [':userId',$searchByUser,PDO::PARAM_INT];

                    $searchQuery = "%" . strtolower($search) . "%";
                    $binds[] = [':search', $searchQuery, PDO::PARAM_STR];

                    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mail INNER JOIN networkleads_db.users AS u ON u.id=mail.userSent WHERE u.organizationId=:orgId AND mail.systemEmail=0 AND u.id=:userId AND (LOWER(mail.subject) LIKE :search OR LOWER(mail.content)LIKE :search OR LOWER(mail.fromEmail) LIKE :search OR LOWER(mail.toEmail) LIKE :search)", $binds, $errorVar);
                }else{
                    $searchQuery = "%" . $search . "%";
                    $binds[] = [':search', $searchQuery, PDO::PARAM_STR];

                    $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails AS mail INNER JOIN networkleads_db.users AS u ON u.id=mail.userSent WHERE u.organizationId=:orgId AND mail.systemEmail=0 AND (LOWER(mail.subject) LIKE :search OR LOWER(mail.content) LIKE :search OR LOWER(mail.fromEmail) LIKE :search OR LOWER(mail.toEmail) LIKE :search)", $binds, $errorVar);
                }
                $emails['totalEmails'] = $GLOBALS['connector']->fetch_num_rows($getIt);
            }else{
                // user isn't an admin
                $searchQuery = "%" . strtolower($search) . "%";

                $binds = [];
                $binds[] = [':userId',$userId,PDO::PARAM_INT];
                $binds[] = [':search', $searchQuery, PDO::PARAM_STR];

                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails WHERE userSent=:userId AND (LOWER(subject)LIKE :search OR LOWER(content) LIKE :search OR LOWER(fromEmail) LIKE :search OR LOWER(toEmail) LIKE :search) AND systemEmail=0 ", $binds, $errorVar);

                $emails['totalEmails'] = $GLOBALS['connector']->fetch_num_rows($getIt);
            }
            return $emails;
        }
        return false;


    }

    public function getEmail($id,$userId){

        $errorVar = array("mailCenter","getEmail()",3,"Notes",array());

        $binds = [];
        $binds[] = [':id',$id,PDO::PARAM_INT];
        $binds[] = [':userId',$userId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT id,userSent,sentDate,leadId,fromEmail,toEmail,subject,content,didOpen,openDate,status,statusMsg,token,systemEmail,weeklyReport FROM networkleads_db.outgoingMails WHERE id=:id AND userSent=:userId",$binds,$errorVar);
        if (!$getIt){
            return false;
        }else{
            return $GLOBALS['connector']->fetch($getIt);
        }
        return false;

    }

    public function getTotalEmailsOfLimit(){

    }

    public function getTotalEmailsbyDate($compare = false,$sDate = false,$eDate = false,$label = "Custom"){

        $errorVar = array("mailCenter","getTotalEmailsbyDate()",5,"Notes",array());

        if ($sDate == false){
            $sDate = date("Y-m-d 00:00:00",strtotime("first day of this month"));
        }else{
            $sDate = date("Y-m-d 00:00:00",strtotime($sDate));
        }
        if ($eDate == false){
            $eDate = date("Y-m-d 23:59:59",strtotime("Today"));
        }else{
            $eDate = date("Y-m-d 23:59:59",strtotime($eDate));
        }

        $binds = [];
        $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
        $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
        $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
        $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails INNER JOIN networkleads_db.users ON  networkleads_db.outgoingMails.userSent = networkleads_db.users.id WHERE networkleads_db.users.organizationId=:orgId AND CTZ(networkleads_db.outgoingMails.sentDate,:OFFSET)>=:startDate AND CTZ(networkleads_db.outgoingMails.sentDate,:OFFSET)<=:endDate",$binds,$errorVar);


        if(!$getIt){
            return false;
        }else{
            if ($compare == false) {
                return $GLOBALS['connector']->fetch_num_rows($getIt);

            }else{
                $totalPerThisDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                $data = [];
                $data['totalEmailsSent'] = $totalPerThisDate;
                $data['totalEmailsSentCustom'] = false;
                if ($label == "Today"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("yesterday"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 day"));
                }else if ($label == "Yesterday"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-2 days"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("-2 days"));
                }else if ($label == "Last Week"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-14 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -7 days"));
                }else if ($label == "Last 30 Days"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("-60 days"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -30 days"));
                }else if ($label == "This Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of last month"));
                    $eDate = date("Y-m-d H:i:s",strtotime("NOW -1 Month"));
                }else if ($label == "Last Month"){
                    $sDate = date("Y-m-d 00:00:00",strtotime("first day of -2 month"));
                    $eDate = date("Y-m-d 23:59:59",strtotime("last day of -2 month"));
                }else if ($label == "Custom"){
                    $data['totalEmailsSentChange'] = 0;
                    $data['totalEmailsSentCustom'] = true;

                    return $data;
                }

                $binds = [];
                $binds[] = array(':orgId', $this->orgId, PDO::PARAM_INT);
                $binds[] = array(':startDate', $sDate, PDO::PARAM_STR);
                $binds[] = array(':endDate', $eDate, PDO::PARAM_STR);
                $binds[] = array(':OFFSET', NULL, PDO::PARAM_STR,true);

                $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.outgoingMails INNER JOIN networkleads_db.users ON networkleads_db.outgoingMails.userSent = networkleads_db.users.id WHERE networkleads_db.users.organizationId=:orgId AND CTZ(networkleads_db.outgoingMails.sentDate,:OFFSET)>=:startDate AND CTZ(networkleads_db.outgoingMails.sentDate,:OFFSET)<=:endDate",$binds,$errorVar);
                if (!$getIt){
                    return false;
                }else{
                    $totalPerNewDate = $GLOBALS['connector']->fetch_num_rows($getIt);

                    if ($totalPerNewDate == 0 && $totalPerThisDate > 0){
                        $data['totalEmailsSentChange'] = 100;
                    }else if ($totalPerThisDate == $totalPerNewDate) {
                        $data['totalEmailsSentChange'] = 0;
                    }else{
                        $data['totalEmailsSentChange'] = (($totalPerThisDate-$totalPerNewDate)/$totalPerNewDate)*100;
                    }
                    return $data;
                }
            }
        }

        return false;

    }

    public function getAllUnsubscribers($page=0){

        $errorVar = array("mailCenter","getAllUnsubscribers()",3,"Notes",array());

        if ($page == 0){
            $startPage = 0;
        }else{
            $startPage = $page;
        }

        $amount = 20;

        $unsubscribers = [];

        $binds = [];
        $binds[] = [':orgId',$this->orgId,PDO::PARAM_INT];
        $binds[] = [":amount",$amount,PDO::PARAM_INT];


        $getIt = $GLOBALS['connector']->execute("SELECT * FROM networkleads_db.unsubscribedList WHERE orgId=:orgId LIMIT $startPage, :amount",$binds,$errorVar);
//        $getIt .= $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.unsubscribedList WHERE orgId=:orgId",$binds,$errorVar);


        if (!$getIt){
            return false;
        } else{
            $timeController = new timeController();

            while ($unsubscriber = $GLOBALS['connector']->fetch($getIt)){
                $unsubscribedTime = $timeController->convertv2($unsubscriber['unsubscribedTime'],"m/d/Y H:i");
                $unsubscriber['unsubscribedTime'] = $unsubscribedTime;
                $unsubscribers[] = $unsubscriber;
            }
            return $unsubscribers;
        }
    }

    public function getNumberOfAllUnsubscribers(){

        $errorVar = array("mailCenter","getNumberOfAllUnsubscribers()",3,"Notes",array());

        $binds = [];
        $binds[] = [':orgId',$this->orgId,PDO::PARAM_INT];

        $getIt = $GLOBALS['connector']->execute("SELECT COUNT(*) FROM networkleads_db.unsubscribedList WHERE orgId=:orgId",$binds,$errorVar);


        if (!$getIt){
            return false;
        } else{
            $unsubscribers = $GLOBALS['connector']->fetch_num_rows($getIt);
            return $unsubscribers;
        }
    }


    public function addMailToBlockList($email = NULL, $reason = ""){

        if($email === NULL){return false;}

        $errorVar = array("mailCenter","addMailToBlockList",5,"Notes",array());

        $binds = [];
        $binds[] = array(':orgId',$this->orgId,PDO::PARAM_INT);
        $binds[] = array(':email',$email,PDO::PARAM_STR);
        $binds[] = array(':reason',$reason,PDO::PARAM_STR);

        $setIt = $GLOBALS['connector']->execute("INSERT INTO networkleads_db.unsubscribedList (orgId,email,reason) VALUES(:orgId,:email,:reason)",$binds,$errorVar);
        if(!$setIt){
            return false;
        }else{
            return true;
        }
    }

}