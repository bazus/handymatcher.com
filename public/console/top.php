<?php
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/classes/security/bouncer.php");
require_once($_SERVER['LOCAL_NL_PATH'] . "/console/actions/init/timeElapsed.php");

?>


<link href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/mobile/top.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

<link rel="shortcut icon" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/favicon.ico" type="image/x-icon">

<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg topSiteNavBar" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom searchForm" action="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/categories/search/search.php" method="get">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search" value="<?php if(isset($_GET['top-search'])){echo trim($_GET['top-search']);} ?>">
                </div>
            </form>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li id="currentTime" style="display: inline-block;margin: 15px;color: #999c9e;" title="Eastern Time (ET)"></li>
            <li id="dialerBtn">📞</li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-list-alt"></i>  <span class="label label-info" id="unReadLeadFollowups"></span>
                </a>

                <div class="dropdown-menu dropdown-messages followUpsDiv">


                    <div class="ContextualPopover-arrowContainer" style=""><div class="ContextualPopover-arrow" style="">

                            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="9" viewBox="0 0 21 9" style="stroke-width: 0.1px;stroke: #b5b5b5;">
                                <g fill="none" fill-rule="evenodd">
                                    <path fill="#8898AA" fill-opacity=".1" d="M1 9.092h19l-6.402-6.74c-1.717-1.806-4.485-1.8-6.196 0L1 9.093zM20.342 8l-6.02-6.336c-2.108-2.22-5.538-2.218-7.645 0L.658 8h19.684z"></path><path fill="#FFF" d="M7.402 2.353c1.711-1.801 4.48-1.807 6.196 0L20 9.093H1l6.402-6.74z"></path>
                                </g>
                            </svg>
                        </div>
                    </div>

                    <span style="text-align: right;display: block;padding: 5px;padding-right: 0px;margin: 3px;">
                        <span class="label float-right" style="cursor: pointer;" onclick="clearLeadsFollowups(this)" data-style="zoom-out">Clear All</span>
                    </span>
                    <ul class="followUpsList">
                        <li id="noFollowUpsLI" style="text-align: center;bottom: 0px;background-color: #ffffff;padding: 10px;width: 100%;">
                            No Followups
                        </li>
                        <li>
                            <div class="followUpsLoadMore" style="display:none;">
                                <div id="followUpsLoadMore">Load More</div>
                                <div id="followUpsLoadMoreWait" style="display: none;">
                                    <i class="fa fa-circle" style="margin-right: 3px;"></i>
                                    <i class="fa fa-circle" style="margin-right: 3px;"></i>
                                    <i class="fa fa-circle" style="margin-right: 3px;"></i>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
            </li>
            <li class="hidden-xs">
                <a href="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/login/actions/logout.php">
                    Log out
                </a>
            </li>
            <li id="rightside_toolbar_open">
                <a class="right-sidebar-toggle">
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
        </ul>

    </nav>
</div>

<script src="<?php echo $_SERVER['LOCAL_NL_URL']; ?>/console/plugins/datetimepicker/moment.js"></script>
<script>
    function loadingState(){
        $(".ibox-content").addClass('sk-loading');
    }
    function stopLoadingState(){
        $(".ibox-content").removeClass('sk-loading');
    }
    var serverDateText = '<?php echo date("Y/m/d H:i:s",strtotime("now")); ?>';
    var systemDate = new Date(serverDateText);

    setInterval(function(){
        // add a second
        systemDate.setSeconds(systemDate.getSeconds() + 1);
        
        var seconds = systemDate.getSeconds();
        var minutes = (systemDate.getMinutes() < 10 ? '0' : '') + systemDate.getMinutes();
        var hour = (systemDate.getHours() < 10 ? '0' : '') + systemDate.getHours();

        if ((seconds % 2) == 0){
            var currentTime = hour+" : "+minutes;
        }else{
            var currentTime = hour+" &nbsp "+minutes;
        }
        $("#currentTime").html(currentTime);
    },1000);

</script>
<!-- =======
== START LEAD FOLLOWUPS ========= -->
<script>
    var BASE_URL = "<?= $_SERVER['LOCAL_NL_URL']; ?>";

    var leadFollowupsStart = 0;
    $( "#followUpsLoadMore" ).click(function(e) {
        e.stopPropagation();

        leadFollowupsStart+=10;
        getLeadsFollowups();
    });
    function getLeadsFollowups(singleLogId,isLive,doNotAlert){

        $( "#followUpsLoadMore" ).css("display","none");
        $( "#followUpsLoadMoreWait" ).css("display","block");


        var strUrl = BASE_URL+'/console/actions/leads/logs/getLeadsFollowups.php';

        jQuery.ajax({
            url: strUrl,
            method:"GET",
            data:{
                start:leadFollowupsStart,
                singleLogId:singleLogId
            },
            async: true
        }).done(function (jsondata) {

            try{
                var data = JSON.parse(jsondata);

                $( "#followUpsLoadMore" ).css("display","block");
                $( "#followUpsLoadMoreWait" ).css("display","none");

                if(data.unread == 0){
                    $( "#unReadLeadFollowups" ).css("display","none");
                }else{
                    $( "#unReadLeadFollowups" ).css("display","unset");
                }

                $( "#unReadLeadFollowups" ).html(data.unread);

                if(data.logs.length > 0){
                    $( "#noFollowUpsLI" ).css("display","none");
                    $( ".followUpsLoadMore" ).css("display","");
                }

                for(var i = 0;i<data.logs.length;i++){
                    var log = data.logs[i];
                    addLeadFollowupItem(log,isLive,doNotAlert);
                }

                // if 'singleLogId' is null (because a user clicked on 'load more'  and did not click on an item) then scroll to the bottom
                if(singleLogId == null){
                    $('.followUpsList').scrollTop($('.followUpsList')[0].scrollHeight);
                }
            }catch (e) {
                Sentry.withScope(function(scope) {
                    Sentry.setExtra("data", jsondata);
                    Sentry.setExtra("comments", "getLeadsFollowups()");
                    Sentry.captureException(e);
                });
            }

        });
    }

    function addLeadFollowupItem(log,isLive,doNotAlert){

        if(isLive == true && doNotAlert != true){
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 10000
            };
            toastr.options.onclick = function () {
                window.open(BASE_URL+"/console/categories/leads/lead.php?leadId="+log.leadId,'_blank');
            };

            toastr.info("Click To Open", log.logText,{iconClass:"toast-newLog"});
        }

        var li = document.createElement("li");
        li.id = "followUpLogItem"+log.id;
        if(log.isSeen == 0){
            li.className = "unreadFollowup";
        }

        var unreadDiv = document.createElement("div");
        unreadDiv.setAttribute("style","width: 3px;height: 47px;border-bottom: 1px solid gainsboro;background-color: gainsboro;float: left;left: 0;");

        if(log.isSeen == 0){
            unreadDiv.className = "unreadDiv";
        }

        var a = document.createElement("a");
        a.href = BASE_URL+"/console/categories/leads/lead.php?leadId="+log.leadId;
        a.target = "_blank";
        a.className = "followupA";
        a.setAttribute("onClick","window.event.stopPropagation();markLeadLogAsRead("+log.id+","+log.leadId+")");

        a.appendChild(unreadDiv);

        var div = document.createElement("div");
        div.className = "dropdown-messages-box";
        div.setAttribute("style","    padding: 7px;");

        var small = document.createElement("small");
        small.setAttribute("style","float:right;");
        small.innerHTML = "#"+log.jobNumber;

        var strong = document.createElement("strong");
        strong.innerHTML = log.logText;
        strong.setAttribute("style","overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 290px;display: inline-block;");

        var span = document.createElement("span");
        span.style.display = "block";
        span.innerHTML = log.logText;

        var small2 = document.createElement("small");
        small2.className = "text-muted";
        small2.style.display = "block";
        small2.innerHTML = log.dateAddedText;
        small2.title = log.dateAdded;

        div.appendChild(small);
        div.appendChild(strong);
        //div2.appendChild(span);
        div.appendChild(small2);

        //div.appendChild(a);
        a.appendChild(div);

        li.appendChild(a);

        if($('#followUpLogItem'+log.id).length == 0) {
            if(isLive == true){
                // this is a live notification of follow up - put it at the top of the list
                $( ".followUpsList li:first" ).before(li);
            }else{
                $( ".followUpsList li:last" ).before(li);
            }
        }else{
            $( "#followUpLogItem"+log.id ).replaceWith(li);
        }

    }

    function markLeadLogAsRead(id,leadId){

        var strUrl = BASE_URL+'/console/actions/leads/logs/markRead.php';

        jQuery.ajax({
            url: strUrl,
            method:"POST",
            data:{
                id:id,
                leadId:leadId
            },
            async: true
        }).done(function (data) {

            data = JSON.parse(data);

            getLeadsFollowups(id);
        });

        }

    function clearLeadsFollowups(obj){
        obj = $(obj);
            obj.ladda();
            obj.ladda("start");
            window.event.stopPropagation();
            var strUrl = BASE_URL+'/console/actions/leads/logs/markAllAsRead.php';

            jQuery.ajax({
                url: strUrl,
                method:"POST",
                data:{},
                async: true
            }).done(function (data) {
                obj.ladda("stop");

                leadFollowupsStart = 0;
                getLeadsFollowups("");
            });
        }
    getLeadsFollowups();
</script>
<style>
    .followUpsLoadMore{
        text-align: center;
        cursor: pointer;
        bottom: 0px;
        position: inherit;
        background-color: #ffffff;
        padding: 10px;
        width: 100%;
        margin-bottom: 1px;
    }
    .dropdown-menu{
        box-shadow: 0px 0px 2px rgba(154, 154, 154, 0.7) !important;
    }


    .followUpsList{
        padding: 0px;
        overflow-y:auto;
        overflow-x:hidden;
        max-height: 255px;
        border-top: 1px solid #e9ecef;
        padding-bottom: 10px;
    }
    .followUpsList li{
        border-right: 1px solid #e9ecef;
        border-left: 1px solid #e9ecef;
        border-bottom: 1px solid #e9ecef;
        margin-top: 1px;
    }
    .followUpsList:first-child{
        border-top: 1px solid #e9ecef;
    }
    .followUpsList li:hover{
        cursor: pointer;
        background-color: aliceblue;
    }
    .followupA{
        padding: 0 !important;
        font-size: 12px !important;
        margin: 0 !important;
        color: unset !important;
    }

    .unreadFollowup{
        background-color: aliceblue !important;
    }

    .unreadFollowup .unreadDiv{
        width: 3px;
        height: 50px;
        border-bottom: 1px solid gainsboro;
        background-color: #1c84c6 !important;
        float: left;
    }

    .ContextualPopover-arrowContainer {
        text-align: right;
        display: -ms-flexbox;
        display: block;
        -ms-flex-item-align: center;
        align-self: center;
        -ms-flex-pack: center;
        justify-content: center;
        box-sizing: border-box;
        margin: 0;
        right: 0;
        outline: 0;
        margin-top: -13px;
    }

    #toast-container > .toast-newLog:before {
        content: "\f022";
    }
    .toast-newLog {
        background-color: #23c6c8
    }

    #toast-container > .toast-newSMS:before {
        content: "\f10b";
    }
    .toast-newSMS {
        background-color: #23c6c8
    }


    #toast-container > .toast-newEvent:before {
        content: "\f073";
    }
    .toast-newEvent {
        background-color: #23c6c8
    }

    .followUpsDiv{
        max-height: 300px;
        width: 400px !important;
        padding: 9px;
        padding-top:0px;
        margin-top: 11px !important;
    }
    @media (max-width: 768px) {
        .followUpsDiv{
            margin-left: auto !important;
            right:0 !important;
            width: 319px !important;
        }
        .followUpsList strong{
            width: 219px !important;
        }
    }

</style>
<!-- ========= END LEAD FOLLOWUPS ========= -->

<!-- ========= START MIXPANEL TRACKING ========= -->
<?php
/*
 * NOT IN USE
 *
if ($_SERVER["ENVIRONMENT"] == "prod") {
    $analytics = array("mixpanel");
    require_once($_SERVER['LOCAL_NL_PATH'] . "/console/services/analytics/index.php");
}
?>
<script>
    mixpanel.add_group('organizations', '<?php echo $bouncer["organizationData"]["organizationName"]; ?>');

    mixpanel.identify("<?php echo $bouncer["credentials"]["userId"]; ?>");
    mixpanel.people.set({
        "$name": "<?php echo $bouncer["userData"]["fullName"]; ?>",
        "$email": "<?php echo $bouncer["userData"]["email"]; ?>",

        "$created": "<?php echo $bouncer["userData"]["signUpDate"]; ?>"

    });
</script>
<?php */ ?>
<!-- ========= END MIXPANEL TRACKING ========= -->
